<GameProjectFile>
  <PropertyGroup Type="Layer" Name="TableRules" ID="cc0f2163-354d-4eb0-9611-798390573f31" Version="2.0.6.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" FrameEvent="" Tag="135" ctype="LayerObjectData">
        <Position X="0.0000" Y="0.0000" />
        <Scale ScaleX="1.0000" ScaleY="1.0000" />
        <AnchorPoint />
        <CColor A="255" R="255" G="255" B="255" />
        <Size X="1280.0000" Y="720.0000" />
        <PrePosition X="0.0000" Y="0.0000" />
        <PreSize X="0.0000" Y="0.0000" />
        <Children>
          <NodeObjectData Name="txt_panel" ActionTag="165011355" FrameEvent="" Tag="2013" ObjectIndex="91" TouchEnable="True" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Position X="652.0000" Y="705.7947" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <Size X="830.0000" Y="27.0000" />
            <PrePosition X="0.5094" Y="0.9803" />
            <PreSize X="0.6484" Y="0.0375" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </NodeObjectData>
          <NodeObjectData Name="txt_room_rules" ActionTag="-194505408" FrameEvent="" Tag="325" ObjectIndex="1" FontSize="22" LabelText="松原快艇" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ctype="TextObjectData">
            <Position X="624.0000" Y="704.7865" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <Size X="88.0000" Y="22.0000" />
            <PrePosition X="0.4875" Y="0.9789" />
            <PreSize X="0.0000" Y="0.0000" />
          </NodeObjectData>
          <NodeObjectData Name="img_title" ActionTag="-2025463858" VisibleForFrame="False" FrameEvent="" Tag="137" ObjectIndex="2" Scale9Width="137" Scale9Height="34" ctype="ImageViewObjectData">
            <Position X="639.4601" Y="489.8903" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <Size X="46.0000" Y="46.0000" />
            <PrePosition X="0.4996" Y="0.6804" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/ImageFile.png" />
          </NodeObjectData>
          <NodeObjectData Name="img_content" ActionTag="-140270431" Alpha="178" VisibleForFrame="False" FrameEvent="" Tag="138" ObjectIndex="3" Scale9Width="140" Scale9Height="30" ctype="ImageViewObjectData">
            <Position X="778.8200" Y="422.7023" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <Size X="46.0000" Y="46.0000" />
            <PrePosition X="0.6085" Y="0.5871" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/ImageFile.png" />
          </NodeObjectData>
          <NodeObjectData Name="pnl_content" ActionTag="-319940281" VisibleForFrame="False" FrameEvent="" Tag="139" ObjectIndex="1" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Position X="428.0099" Y="344.8600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <AnchorPoint />
            <CColor A="255" R="255" G="255" B="255" />
            <Size X="421.0000" Y="102.0000" />
            <PrePosition X="0.3344" Y="0.4790" />
            <PreSize X="0.3289" Y="0.1417" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </NodeObjectData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>