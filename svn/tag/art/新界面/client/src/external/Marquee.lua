 
local Marquee = class("Marquee", function()
    local _Marquee = display.newNode()
    return _Marquee
end)

function Marquee:ctor ()
 
    self. nwidth = 700
    self.m_tabInfoTips = {}
    self.m_nNotifyId = 0
    self._tipIndex = 1
    -- 系统公告列表
	self.m_tabSystemNotice = {}
	self._sysIndex = 1
    -- 公告是否运行
    self.m_bNotifyRunning = false
 
    self.bg = display.newSprite ("shijie/sp_trumpet_bg.png")
    :addTo (self)
    :setVisible (false)
    :setAnchorPoint (cc.p (0, 0.5))

    local stencil = display.newSprite ()
    :setAnchorPoint (cc.p (0, 0.5))
    stencil:setTextureRect (cc.rect (0, 0, 700, 40))

    self._notifyClip = cc.ClippingNode:create (stencil)
    :setAnchorPoint (cc.p (0, 0.5))
    :addTo (self)
    :setInverted (false)

    self._notifyText = cc.Label:createWithTTF ("", "fonts/round_body.ttf", 24)
    :addTo (self._notifyClip)
    :setTextColor (cc.c4b (255, 191, 123, 255))
    :setAnchorPoint (cc.p (0, 0.5))
    :enableOutline (cc.c4b (79, 48, 35, 255), 1)

    math.randomseed(os.time())
   

    return self

end

function Marquee:addGameNotice (str,id)

 --       if string.find(str,"欢迎") then
  --          return
 --       end

        local item = {}
		item.str =  "" ..str..""
		item.color = cc.c4b(255,191,123,255)
		item.autoremove = true
		item.showcount = 0
		item.bNotice = false
		item.id =  math.random(1,10000)
		--self:addNotice(item)
        table.insert(self.m_tabSystemNotice, item)
        self:onChangeNotify(self.m_tabSystemNotice[self._sysIndex])
        --请求公告
	--self:requestNotice()
end


function Marquee:addNotice (item)
    if nil == item then
        return
    end
    table.insert (self.m_tabInfoTips, 1, item)
    if not self.m_bNotifyRunning then
        self:onChangeNotify (self.m_tabInfoTips[self._tipIndex])
    end
end
--跑马灯更新
function Marquee:onChangeNotify (msg)
    self._notifyText:stopAllActions ()
    if not msg or not msg.str or #msg.str == 0 then
        self._notifyText:setString ("")
        self.m_bNotifyRunning = false
        self._tipIndex = 1
        self._sysIndex = 1
        self.bg:setVisible(false)

        return
    end
    self:setVisible (true)
    self.bg:setVisible (true)
    self.m_bNotifyRunning = true
    local msgcolor = msg.color or cc.c4b (255, 191, 123, 255)
    self._notifyText:setVisible (false)
    self._notifyText:setString (msg.str)
    self._notifyText:setTextColor (msgcolor)
    table.remove(self.m_tabSystemNotice,1) 

    if true == msg.autoremove then
        msg.showcount = msg.showcount or 0
        msg.showcount = msg.showcount - 1
        if msg.showcount <= 0 then
            self:removeNoticeById (msg.id)
        end
    end

    local tmpWidth = self._notifyText:getContentSize ().width
    self._notifyText:runAction (
    cc.Sequence:create (
    cc.CallFunc:create (function ()
        self._notifyText:move (self.nwidth, 0)
        self._notifyText:setVisible (true)
    end),
    cc.MoveTo:create (16 + (tmpWidth / 172), cc.p (0 - tmpWidth, 0)),
    cc.CallFunc:create (function ()
        local tipsSize = 0
        local tips = {}
        local index = 1
        if self.m_tabSystemNotice[1] then
            -- 喇叭等
            local tmp = self._tipIndex + 1
            if tmp > #self.m_tabInfoTips then
                tmp = 1
            end
            self._tipIndex = tmp
            self:onChangeNotify (self.m_tabInfoTips[1])
        else
            self:setVisible(false)
        end
    end)
    )
    )
end

function Marquee:getNoticeId()
	local tmp = self.m_nNotifyId
	self.m_nNotifyId = self.m_nNotifyId + 1
	return tmp
end
function Marquee:removeNoticeById (id)
    if nil == id then
        return
    end

    local idx = nil
    for k, v in pairs (self.m_tabInfoTips) do
        if nil ~= v.id and v.id == id then
            idx = k
            break
        end
    end

    if nil ~= idx then
        table.remove (self.m_tabInfoTips, idx)
    end
end
return Marquee