--请求管理器
local RequestManager = class("RequestManager")

local function _callback(c, r, m)
    
    if c then
        c(r, m)
    end
end

--获取抽奖奖品配置
function RequestManager.requestLotteryConfig(callback)

	local url = yl.HTTP_URL .. "/WS/Lottery.ashx"
 	appdf.onHttpJsionTable(url ,"GET","action=LotteryConfig",function(jstable,jsdata)
        
        if type(jstable) == "table" then
            local data = jstable["data"]
            if type(data) == "table" then
                local valid = data["valid"]
                if nil ~= valid and true == valid then
                    local list = data["list"]
                    if type(list) == "table" then
                        for i = 1, #list do
                            --配置转盘
                            local lottery = list[i]

                            GlobalUserItem.dwLotteryQuotas[i] = lottery.ItemQuota
                            GlobalUserItem.cbLotteryTypes[i] = lottery.ItemType
                        end

                        --抽奖已配置
                        GlobalUserItem.bLotteryConfiged = true

                        _callback(callback, 0)

                        return
                    end
                end
            end
        end

        _callback(callback, -1, "数据获取失败")
    end)
end

--获取用户分数信息
function RequestManager.requestUserScoreInfo(callback)

    local ostime = os.time()
	local url = yl.HTTP_URL .. "/WS/MobileInterface.ashx"
 	appdf.onHttpJsionTable(url ,"GET","action=GetScoreInfo&userid=" .. GlobalUserItem.dwUserID .. "&time=".. ostime .. "&signature=".. GlobalUserItem:getSignature(ostime),function(sjstable,sjsdata)
        
        if type(sjstable) == "table" then
            local data = sjstable["data"]
            if type(data) == "table" then
                local valid = data["valid"]
                if true == valid then
                    local score = tonumber(data["Score"]) or 0
                    local bean = tonumber(data["Currency"]) or 0
                    local ingot = tonumber(data["UserMedal"]) or 0
                    local roomcard = tonumber(data["RoomCard"]) or 0

                    local needupdate = false
                    if score ~= GlobalUserItem.lUserScore 
                    	or bean ~= GlobalUserItem.dUserBeans
                    	or ingot ~= GlobalUserItem.lUserIngot
                    	or roomcard ~= GlobalUserItem.lRoomCard then
                    	GlobalUserItem.dUserBeans = bean
                    	GlobalUserItem.lUserScore = score
                    	GlobalUserItem.lUserIngot = ingot
                    	GlobalUserItem.lRoomCard = roomcard
                        needupdate = true
                    end
                    if needupdate then
                        print("update score")
                        --通知更新        
                        local eventListener = cc.EventCustom:new(yl.RY_USERINFO_NOTIFY)
                        eventListener.obj = yl.RY_MSG_USERWEALTH
                        cc.Director:getInstance():getEventDispatcher():dispatchEvent(eventListener)
                    end 

                    _callback(callback, 0)  
                    
                    return      
                end
            end
        end

        _callback(callback, -1, "数据获取失败")
    end)
end

return RequestManager