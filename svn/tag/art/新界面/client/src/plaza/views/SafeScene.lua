local SafeScene = class("SafeScene", cc.load("mvc").ViewBase)
local ExternalFun = require(appdf.EXTERNAL_SRC.."ExternalFun")
function SafeScene:onCreate()
	self:init()
end

function SafeScene:init()
	local csbNode = ExternalFun.loadCSB("plaza/SafeLayer.csb", self)
    self.rootNode = csbNode
	local LoadingBg = csbNode:getChildByName("LoadingBg")
	self._LoadingBar = LoadingBg:getChildByName("LoadingBar")
	self._LoadingBar = cc.ProgressTimer:create(cc.Sprite:create("plaza/loading_bar.png"))
	self._LoadingBar:setType(cc.PROGRESS_TIMER_TYPE_BAR)
	self._LoadingBar:setMidpoint(cc.p(0.0,0.5))
	self._LoadingBar:setBarChangeRate(cc.p(1,0))
    self._LoadingBar:setPosition(cc.p(169.50,19))
    self._LoadingBar:runAction(cc.ProgressTo:create(0.2,10))
    LoadingBg:addChild(self._LoadingBar)
	self._LoadingBar:setLocalZOrder(1)
	self._text = LoadingBg:getChildByName("text")
	self._text:setLocalZOrder(3)
	self._light = LoadingBg:getChildByName("light")
	self._light:setLocalZOrder(2)
	local callfunc = cc.CallFunc:create(function()
		self:updatePercent()
	end)
	self:runAction(cc.Sequence:create(cc.DelayTime:create(0.8),callfunc))
end

function SafeScene:updatePercent()
	
	local dt = 2
	
	if nil ~= self._LoadingBar then
		self._LoadingBar:runAction(cc.ProgressTo:create(dt,100))
		cc.Director:getInstance():getActionManager():removeActionByTag(1, self._light)
		local move =  cc.MoveTo:create(dt*0.95,cc.p(308*(100/100),18.50))
		move:setTag(1)
		self._light:runAction(move)
	end
	
	local callfunc1 = cc.CallFunc:create(function()
					self._text:setString("安全系统已启动")
				end)
	
	local callfunc2 = cc.CallFunc:create(function()
					self._LoadingBar:stopAllActions()
					self._LoadingBar = nil
		
					self:stopAllActions()
					self:removeFromParent()
					self:getApp():enterSceneEx(appdf.CLIENT_SRC.."plaza.views.ClientScene","FADE",0.2)
				end)
	self:stopAllActions()
	self:runAction(cc.Sequence:create(cc.DelayTime:create(2.0),callfunc1,cc.DelayTime:create(0.5),callfunc2))
end

return SafeScene