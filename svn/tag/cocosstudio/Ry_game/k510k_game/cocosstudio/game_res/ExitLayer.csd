<GameFile>
  <PropertyGroup Name="ExitLayer" Type="Node" ID="cb30b990-f696-4e63-9876-97ff05a3bd64" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Children>
          <AbstractNodeData Name="bg" Tag="121" IconVisible="True" CascadeColorEnabled="True" CascadeOpacityEnabled="True" ctype="ImageViewObjectData">
            <Size X="545.0000" Y="290.0000" />
            <Children>
              <AbstractNodeData Name="bt_ensure" Tag="122" IconVisible="True" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="152" Scale9Height="62" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" CascadeColorEnabled="True" CascadeOpacityEnabled="True" FontName="" GlowEnabled="False" BoldEnabled="False" UnderlineEnabled="False" ItalicsEnabled="False" StrikethroughEnabled="False" ctype="ButtonObjectData">
                <Size X="182.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="159.6355" Y="67.0015" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/bt_ensure_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/bt_ensure_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/bt_ensure_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="0" R="0" G="0" B="0" />
                <GlowColor R="0" G="63" B="198" A="255" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_cancel" Tag="123" IconVisible="True" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="152" Scale9Height="62" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" CascadeColorEnabled="True" CascadeOpacityEnabled="True" FontName="" GlowEnabled="False" BoldEnabled="False" UnderlineEnabled="False" ItalicsEnabled="False" StrikethroughEnabled="False" ctype="ButtonObjectData">
                <Size X="182.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="386.8000" Y="67.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/bt_cancel_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/bt_cancel_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="0" R="0" G="0" B="0" />
                <GlowColor R="0" G="63" B="198" A="255" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0002" Y="-0.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/exit_bg.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>