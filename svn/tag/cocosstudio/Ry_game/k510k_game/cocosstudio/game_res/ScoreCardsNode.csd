<GameFile>
  <PropertyGroup Name="ScoreCardsNode" Type="Node" ID="b0faded7-3d32-493e-acfa-0b7d6a339f46" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="161" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="MaskPanel" ActionTag="2030433240" Tag="162" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="1791044686" Tag="163" IconVisible="False" LeftMargin="-425.0000" RightMargin="-425.0000" TopMargin="-200.0000" BottomMargin="-200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="42" RightEage="42" TopEage="147" BottomEage="147" Scale9OriginX="42" Scale9OriginY="147" Scale9Width="44" Scale9Height="153" ctype="PanelObjectData">
            <Size X="850.0000" Y="400.0000" />
            <Children>
              <AbstractNodeData Name="CloseBtn" ActionTag="1183880339" Tag="189" IconVisible="False" LeftMargin="798.7552" RightMargin="-31.7552" TopMargin="-32.4614" BottomMargin="348.4614" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="83.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="840.2552" Y="390.4614" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9885" Y="0.9762" />
                <PreSize X="0.0976" Y="0.2100" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="setting_res/bt_close_2.png" Plist="" />
                <PressedFileData Type="Normal" Path="setting_res/bt_close_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="setting_res/bt_close_1.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/menu_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_1" ActionTag="775228390" Tag="165" IconVisible="False" LeftMargin="-406.9448" RightMargin="255.9448" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-331.4448" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_2" ActionTag="457336710" Tag="166" IconVisible="False" LeftMargin="-312.5776" RightMargin="161.5776" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-237.0776" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_3" ActionTag="1255412951" Tag="167" IconVisible="False" LeftMargin="-218.2105" RightMargin="67.2105" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-142.7105" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_4" ActionTag="1867173069" Tag="168" IconVisible="False" LeftMargin="-123.8431" RightMargin="-27.1569" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-48.3431" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_5" ActionTag="936828057" Tag="169" IconVisible="False" LeftMargin="-29.4760" RightMargin="-121.5240" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.0240" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_6" ActionTag="-579506720" Tag="170" IconVisible="False" LeftMargin="64.8914" RightMargin="-215.8914" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="140.3914" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_7" ActionTag="-1627862440" Tag="171" IconVisible="False" LeftMargin="159.2586" RightMargin="-310.2586" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="234.7586" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_5_8" ActionTag="-153395777" Tag="172" IconVisible="False" LeftMargin="253.6258" RightMargin="-404.6258" TopMargin="-216.8143" BottomMargin="36.8143" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="329.1258" Y="126.8143" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_1" ActionTag="-856231082" Tag="173" IconVisible="False" LeftMargin="-406.9448" RightMargin="255.9448" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-331.4448" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_2" ActionTag="1519323849" Tag="174" IconVisible="False" LeftMargin="-312.5776" RightMargin="161.5776" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-237.0776" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_3" ActionTag="646540916" Tag="175" IconVisible="False" LeftMargin="-218.2103" RightMargin="67.2103" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-142.7103" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_4" ActionTag="644488607" Tag="176" IconVisible="False" LeftMargin="-123.8431" RightMargin="-27.1569" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-48.3431" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_5" ActionTag="-1159362066" Tag="177" IconVisible="False" LeftMargin="-29.4760" RightMargin="-121.5240" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.0240" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_6" ActionTag="1896729293" Tag="178" IconVisible="False" LeftMargin="64.8914" RightMargin="-215.8914" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="140.3914" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_7" ActionTag="189012478" Tag="179" IconVisible="False" LeftMargin="159.2586" RightMargin="-310.2586" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="234.7586" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_10_8" ActionTag="2082384496" Tag="180" IconVisible="False" LeftMargin="253.6258" RightMargin="-404.6258" TopMargin="-88.8409" BottomMargin="-91.1591" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="329.1258" Y="-1.1591" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_1" ActionTag="-1840026033" Tag="181" IconVisible="False" LeftMargin="-406.9448" RightMargin="255.9448" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-331.4448" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_2" ActionTag="1573470336" Tag="182" IconVisible="False" LeftMargin="-312.5777" RightMargin="161.5777" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-237.0777" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_3" ActionTag="-118116122" Tag="183" IconVisible="False" LeftMargin="-218.2104" RightMargin="67.2104" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-142.7104" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_4" ActionTag="1647510967" Tag="184" IconVisible="False" LeftMargin="-123.8432" RightMargin="-27.1568" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-48.3432" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_5" ActionTag="960784459" Tag="185" IconVisible="False" LeftMargin="-29.4760" RightMargin="-121.5240" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.0240" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_6" ActionTag="1714224275" Tag="186" IconVisible="False" LeftMargin="64.8911" RightMargin="-215.8911" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="140.3911" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_7" ActionTag="486826375" Tag="187" IconVisible="False" LeftMargin="159.2584" RightMargin="-310.2584" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="234.7584" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CardModel_k_8" ActionTag="-2089156158" Tag="188" IconVisible="False" LeftMargin="253.6257" RightMargin="-404.6257" TopMargin="39.1324" BottomMargin="-219.1324" LeftEage="49" RightEage="49" TopEage="59" BottomEage="59" Scale9OriginX="49" Scale9OriginY="59" Scale9Width="53" Scale9Height="62" ctype="ImageViewObjectData">
            <Size X="151.0000" Y="180.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="329.1257" Y="-129.1324" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="card_1/card_5.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>