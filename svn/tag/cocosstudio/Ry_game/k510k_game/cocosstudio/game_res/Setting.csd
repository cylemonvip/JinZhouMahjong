<GameFile>
  <PropertyGroup Name="Setting" Type="Node" ID="3316bd58-5d34-482c-aacf-850f69588e21" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-2025852778" Tag="160" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="190" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_setBG" Tag="24" IconVisible="False" LeftMargin="-326.7176" RightMargin="-329.2824" TopMargin="-238.5001" BottomMargin="-238.4999" TouchEnable="True" Scale9Enable="True" LeftEage="216" RightEage="216" TopEage="157" BottomEage="157" Scale9OriginX="216" Scale9OriginY="157" Scale9Width="224" Scale9Height="163" ctype="ImageViewObjectData">
            <Size X="656.0000" Y="477.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.2824" Y="0.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="setting_res/img_setting_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1875097743" Tag="151" IconVisible="False" LeftMargin="-171.9999" RightMargin="67.9999" TopMargin="21.3467" BottomMargin="-58.3467" ctype="SpriteObjectData">
            <Size X="104.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-119.9999" Y="-39.8467" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="setting_res/setting_music.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_close" Tag="25" IconVisible="False" LeftMargin="271.5086" RightMargin="-354.5086" TopMargin="-235.3572" BottomMargin="151.3572" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="62" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="83.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="313.0086" Y="193.3572" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="setting_res/bt_close_1.png" Plist="" />
            <PressedFileData Type="Normal" Path="setting_res/bt_close_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="setting_res/bt_close_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_effect" Tag="32" IconVisible="False" LeftMargin="50.0000" RightMargin="-190.0000" TopMargin="-89.4238" BottomMargin="30.4238" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="110" Scale9Height="37" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="140.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="120.0000" Y="59.9238" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="setting_res/bt_off_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="setting_res/bt_off_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="setting_res/bt_off_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_music" Tag="124" IconVisible="False" LeftMargin="50.0000" RightMargin="-190.0000" TopMargin="10.3468" BottomMargin="-69.3468" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="110" Scale9Height="37" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="140.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="120.0000" Y="-39.8468" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="setting_res/bt_on_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="setting_res/bt_on_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="setting_res/bt_on_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnJieSan" ActionTag="-2005671905" Tag="127" IconVisible="False" LeftMargin="-214.0003" RightMargin="26.0003" TopMargin="100.6835" BottomMargin="-191.6835" TouchEnable="True" FontSize="60" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="158" Scale9Height="69" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="188.0000" Y="91.0000" />
            <Children>
              <AbstractNodeData Name="LZ_Continue_1" ActionTag="343258833" Tag="66" IconVisible="False" LeftMargin="37.4998" RightMargin="37.5002" TopMargin="7.5000" BottomMargin="22.5000" ctype="SpriteObjectData">
                <Size X="113.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="93.9998" Y="53.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5824" />
                <PreSize X="0.6011" Y="0.6703" />
                <FileData Type="Normal" Path="setting_res/jiesan.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-120.0003" Y="-146.1835" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="result_res/LZ_BlueBtn2.png" Plist="" />
            <PressedFileData Type="Normal" Path="result_res/LZ_BlueBtn2.png" Plist="" />
            <NormalFileData Type="Normal" Path="result_res/LZ_BlueBtn1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnZanLi" ActionTag="882244132" Tag="128" IconVisible="False" LeftMargin="26.0000" RightMargin="-214.0000" TopMargin="101.9658" BottomMargin="-192.9658" TouchEnable="True" FontSize="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="158" Scale9Height="69" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="188.0000" Y="91.0000" />
            <Children>
              <AbstractNodeData Name="LZ_Continue_1_0" ActionTag="905989221" Tag="67" IconVisible="False" LeftMargin="45.4998" RightMargin="41.5002" TopMargin="13.2178" BottomMargin="26.7822" ctype="SpriteObjectData">
                <Size X="101.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="95.9998" Y="52.2822" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5106" Y="0.5745" />
                <PreSize X="0.5372" Y="0.5604" />
                <FileData Type="Normal" Path="setting_res/LZ_Leave.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="120.0000" Y="-147.4658" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="result_res/LZ_BlueBtn2.png" Plist="" />
            <PressedFileData Type="Normal" Path="result_res/LZ_BlueBtn2.png" Plist="" />
            <NormalFileData Type="Normal" Path="result_res/LZ_BlueBtn1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_2" ActionTag="37997226" Tag="152" IconVisible="False" LeftMargin="-172.0000" RightMargin="68.0000" TopMargin="-78.4200" BottomMargin="41.4200" ctype="SpriteObjectData">
            <Size X="104.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-120.0000" Y="59.9200" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="setting_res/setting_sound.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>