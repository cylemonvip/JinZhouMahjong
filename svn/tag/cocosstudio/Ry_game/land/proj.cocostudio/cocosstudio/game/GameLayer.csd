<GameFile>
  <PropertyGroup Name="GameLayer" Type="Layer" ID="79acf30e-2fa8-4dd4-b9df-7c2e05a0e98b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="47" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="game_bg_0_1" ActionTag="-447859542" Tag="147" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="440" RightEage="440" TopEage="247" BottomEage="247" Scale9OriginX="440" Scale9OriginY="247" Scale9Width="454" Scale9Height="256" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="PlistSubImage" Path="game_bg_0.png" Plist="public_res/public_res.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_control" ActionTag="-1376718268" Tag="127" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="atlas_count_1" ActionTag="-583149573" Tag="136" IconVisible="False" LeftMargin="320.0000" RightMargin="1014.0000" TopMargin="315.0000" BottomMargin="435.0000" CharWidth="22" CharHeight="34" LabelText="" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="435.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2399" Y="0.5800" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/game_cardnum.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_count_3" ActionTag="30814733" Tag="138" IconVisible="False" LeftMargin="1014.0000" RightMargin="320.0000" TopMargin="315.0000" BottomMargin="435.0000" CharWidth="22" CharHeight="34" LabelText="" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1014.0000" Y="435.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.5800" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/game_cardnum.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="alarm_1" ActionTag="-795414761" Tag="143" IconVisible="False" LeftMargin="270.0000" RightMargin="1004.0000" TopMargin="370.0000" BottomMargin="320.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0000" Y="350.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2249" Y="0.4667" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="alarm_2" ActionTag="-1597187792" Tag="144" IconVisible="False" LeftMargin="190.0000" RightMargin="1084.0000" TopMargin="620.0000" BottomMargin="70.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="220.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1649" Y="0.1333" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="alarm_3" ActionTag="-85489740" Tag="145" IconVisible="False" LeftMargin="1004.0000" RightMargin="270.0000" TopMargin="370.0000" BottomMargin="320.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1034.0000" Y="350.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7751" Y="0.4667" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="outcards_control" Visible="False" ActionTag="495270638" Tag="67" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_layout" ActionTag="25663319" Tag="77" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="game_topMenu_1" ActionTag="376140498" Tag="277" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="332.0000" RightMargin="332.0000" BottomMargin="642.0000" ctype="SpriteObjectData">
                <Size X="670.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="667.0000" Y="750.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="0.5022" Y="0.1440" />
                <FileData Type="PlistSubImage" Path="game_topMenu.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-1586993847" Tag="278" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="597.0000" RightMargin="667.0000" TopMargin="57.5000" BottomMargin="657.5000" FontSize="30" LabelText="叫分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="35.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="667.0000" Y="675.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9000" />
                <PreSize X="0.0525" Y="0.0467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="gamecall_text" ActionTag="1987961410" Tag="279" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="671.6690" RightMargin="662.3310" TopMargin="75.0000" BottomMargin="675.0000" FontSize="30" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="671.6690" Y="675.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.5035" Y="0.9000" />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="back_btn" ActionTag="-2078719511" Tag="67" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="876.1942" RightMargin="365.8058" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="922.1942" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6913" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_leave.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="game_leave_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_leave.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="set_btn" ActionTag="-1459776986" Tag="68" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="766.2726" RightMargin="475.7274" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="812.2726" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6089" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_setting_0.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_setting_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_setting_0.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="tru_btn" ActionTag="1067060848" Tag="69" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="471.0584" RightMargin="770.9416" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="517.0584" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3876" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_trust_0.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_trust_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_trust_0.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="chat_btn" ActionTag="1056259407" Tag="70" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="362.8710" RightMargin="879.1290" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="408.8710" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3065" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_chat_0.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_chat_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_chat_0.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ready_btn" ActionTag="-512199298" Tag="75" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="551.0000" RightMargin="551.0000" TopMargin="421.7500" BottomMargin="249.2500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="202" Scale9Height="57" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="232.0000" Y="79.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3850" />
                <PreSize X="0.1739" Y="0.1053" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="btn_ready_0.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_invite" ActionTag="508243300" Tag="381" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="469.0000" RightMargin="469.0000" TopMargin="315.5000" BottomMargin="345.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="366" Scale9Height="67" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="396.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="390.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5200" />
                <PreSize X="0.2969" Y="0.1187" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="land_bt_invite_0.png" Plist="game/game.plist" />
                <PressedFileData Type="PlistSubImage" Path="land_bt_invite_1.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="land_bt_invite_0.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="userstate_control" ActionTag="-589800948" Tag="75" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="ready1" ActionTag="-955878366" VisibleForFrame="False" Tag="93" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="279.5000" RightMargin="946.5000" TopMargin="199.0000" BottomMargin="499.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="333.5000" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2500" Y="0.7000" />
                <PreSize X="0.0810" Y="0.0693" />
                <FileData Type="PlistSubImage" Path="txt_ready.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ready2" ActionTag="-1040522555" VisibleForFrame="False" Tag="91" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="613.0000" RightMargin="613.0000" TopMargin="566.5000" BottomMargin="131.5000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="157.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2100" />
                <PreSize X="0.0810" Y="0.0693" />
                <FileData Type="PlistSubImage" Path="txt_ready.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ready3" ActionTag="1872747879" VisibleForFrame="False" Tag="92" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="946.5000" RightMargin="279.5000" TopMargin="199.0000" BottomMargin="499.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1000.5000" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.7000" />
                <PreSize X="0.0810" Y="0.0693" />
                <FileData Type="PlistSubImage" Path="txt_ready.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="state_sp1" ActionTag="1890529561" Tag="118" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="316.8400" RightMargin="957.1600" TopMargin="195.0000" BottomMargin="495.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="346.8400" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2600" Y="0.7000" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="state_sp2" ActionTag="-439710818" Tag="76" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="156.7600" RightMargin="1117.2400" TopMargin="459.7500" BottomMargin="230.2500" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="186.7600" Y="260.2500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1400" Y="0.3470" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="state_sp3" ActionTag="-1357661018" Tag="117" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="983.8400" RightMargin="290.1600" TopMargin="195.0000" BottomMargin="495.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1013.8400" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7600" Y="0.7000" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="callscore_control" ActionTag="708489431" Tag="86" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="score_btn1" ActionTag="-2125142203" Tag="87" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="365.2300" RightMargin="778.7700" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="460.2300" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3450" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_score_pass.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_pass.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_btn2" ActionTag="1686241839" Tag="88" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="592.0100" RightMargin="551.9900" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="687.0100" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5150" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_score_one.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_one.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_btn3" ActionTag="-390430517" Tag="89" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="818.7900" RightMargin="325.2100" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="913.7900" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6850" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_score_two.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_two.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_btn4" ActionTag="-1919289208" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1045.5701" RightMargin="98.4299" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1140.5701" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8550" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_score_three.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_three.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ongame_control" ActionTag="1351370147" VisibleForFrame="False" Tag="71" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="pass_btn" ActionTag="-1669409434" Tag="368" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="231.8300" RightMargin="912.1700" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="326.8300" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2450" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_pass.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_pass.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="suggest_btn" ActionTag="1317905002" Tag="73" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="685.3900" RightMargin="458.6100" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="780.3900" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5850" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_tip.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_tip.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="outcard_btn" ActionTag="337477917" Tag="72" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="910.1180" RightMargin="233.8821" TopMargin="420.0504" BottomMargin="251.9496" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5108" ScaleY="0.4718" />
                <Position X="1007.1700" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7550" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_bt_out.png" Plist="game/game.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_out.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="info" ActionTag="1529881482" Tag="426" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="600.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_9" ActionTag="2009002679" Tag="427" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="33.0000" ctype="SpriteObjectData">
                <Size X="1334.0000" Y="117.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="667.0000" />
                <Scale ScaleX="1.0000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.7800" />
                <FileData Type="PlistSubImage" Path="back_tip.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tmpname_text" ActionTag="-1030113918" Tag="479" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="146.7400" RightMargin="1187.2600" TopMargin="118.6350" BottomMargin="31.3650" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="146.7400" Y="31.3650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1100" Y="0.2091" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="game_gold_1" ActionTag="-1367521202" Tag="518" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="302.5702" RightMargin="955.4298" TopMargin="78.8350" BottomMargin="-4.8350" ctype="SpriteObjectData">
                <Size X="76.0000" Y="76.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="340.5702" Y="33.1650" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2553" Y="0.2211" />
                <PreSize X="0.0570" Y="0.5067" />
                <FileData Type="PlistSubImage" Path="game_gold.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_atlas" ActionTag="384441280" Tag="577" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="549.8748" RightMargin="784.1252" TopMargin="118.5000" BottomMargin="31.5000" CharWidth="23" CharHeight="30" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="549.8748" Y="31.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4122" Y="0.2100" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/shuzi.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="game_cellScoreIMG_1" ActionTag="1914105072" Tag="619" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="765.4772" RightMargin="456.5228" TopMargin="91.8350" BottomMargin="8.1650" ctype="SpriteObjectData">
                <Size X="112.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="821.4772" Y="33.1650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6158" Y="0.2211" />
                <PreSize X="0.0840" Y="0.3333" />
                <FileData Type="PlistSubImage" Path="game_cellScoreIMG.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dizhu_atlas" ActionTag="1385333979" Tag="621" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1079.4728" RightMargin="254.5272" TopMargin="118.1700" BottomMargin="31.8300" CharWidth="23" CharHeight="30" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1079.4728" Y="31.8300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8092" Y="0.2122" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/shuzi.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="info_tip" ActionTag="1181117162" Tag="132" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="637.0000" RightMargin="637.0000" TopMargin="-255.0000" BottomMargin="345.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="2.5000" />
                <PreSize X="0.0450" Y="0.4000" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg_clock" ActionTag="1683327183" Tag="61" IconVisible="False" LeftMargin="612.5541" RightMargin="621.4459" TopMargin="-462.8454" BottomMargin="502.8454" ctype="SpriteObjectData">
                <Size X="100.0000" Y="110.0000" />
                <Children>
                  <AbstractNodeData Name="atlas_time" ActionTag="-775731965" Tag="62" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="50.0000" RightMargin="50.0000" TopMargin="51.9970" BottomMargin="58.0030" CharWidth="18" CharHeight="24" LabelText="" StartChar="0" ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="58.0030" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5273" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/game_timenum.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="662.5541" Y="557.8454" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4967" Y="3.7190" />
                <PreSize X="0.0750" Y="0.7333" />
                <FileData Type="PlistSubImage" Path="bg_clock.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.2000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tru_control" ActionTag="-945568001" VisibleForFrame="False" Tag="88" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="Text_11" ActionTag="1644917598" Tag="81" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="546.0000" RightMargin="546.0000" TopMargin="282.5000" BottomMargin="432.5000" FontSize="30" LabelText="点击屏幕取消托管" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="242.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="450.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6000" />
                <PreSize X="0.1814" Y="0.0467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="-497045577" Tag="134" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="972.0000" TopMargin="414.0000" ctype="SpriteObjectData">
                <Size X="362.0000" Y="336.0000" />
                <AnchorPoint ScaleX="1.0000" />
                <Position X="1334.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" />
                <PreSize X="0.2714" Y="0.4480" />
                <FileData Type="PlistSubImage" Path="game_robot-hd.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="CallTimesLayout" ActionTag="180072555" Tag="56" IconVisible="False" LeftMargin="366.9996" RightMargin="367.0004" TopMargin="409.0883" BottomMargin="240.9117" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="600.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="NoAddTimesBtn" ActionTag="2008199760" Tag="55" IconVisible="False" LeftMargin="53.4998" RightMargin="353.5002" TopMargin="12.0000" BottomMargin="12.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="163" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="193.0000" Y="76.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="149.9998" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2500" Y="0.5000" />
                <PreSize X="0.3217" Y="0.7600" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="public_res/no_add_times_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="public_res/no_add_times_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="public_res/no_add_times.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="AddTimesBtn" ActionTag="-1737595906" Tag="57" IconVisible="False" LeftMargin="355.4998" RightMargin="55.5002" TopMargin="12.0000" BottomMargin="12.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="159" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="189.0000" Y="76.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="449.9998" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.5000" />
                <PreSize X="0.3150" Y="0.7600" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="public_res/add_times_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="public_res/add_times_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="public_res/add_times.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="666.9996" Y="240.9117" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3212" />
            <PreSize X="0.4498" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="DoubleSp_2" ActionTag="621011175" Tag="58" IconVisible="False" LeftMargin="121.1007" RightMargin="1090.8993" TopMargin="419.9789" BottomMargin="222.0211" ctype="SpriteObjectData">
            <Size X="122.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="182.1007" Y="276.0211" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1365" Y="0.3680" />
            <PreSize X="0.0915" Y="0.1440" />
            <FileData Type="PlistSubImage" Path="call_point_16.png" Plist="game/animation.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="DoubleSp_1" ActionTag="924180876" Tag="59" IconVisible="False" LeftMargin="263.4040" RightMargin="948.5960" TopMargin="186.1928" BottomMargin="455.8072" ctype="SpriteObjectData">
            <Size X="122.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="324.4040" Y="509.8072" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2432" Y="0.6797" />
            <PreSize X="0.0915" Y="0.1440" />
            <FileData Type="PlistSubImage" Path="call_point_16.png" Plist="game/animation.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="DoubleSp_3" ActionTag="-1902380806" Tag="60" IconVisible="False" LeftMargin="960.6238" RightMargin="251.3762" TopMargin="179.6305" BottomMargin="462.3695" ctype="SpriteObjectData">
            <Size X="122.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1021.6238" Y="516.3695" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7658" Y="0.6885" />
            <PreSize X="0.0915" Y="0.1440" />
            <FileData Type="PlistSubImage" Path="call_point_16.png" Plist="game/animation.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>