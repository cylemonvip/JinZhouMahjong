<GameFile>
  <PropertyGroup Name="GameRoleItem" Type="Node" ID="417134c0-eb7f-4532-a80d-994d7b9909df" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="73" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="head_frame" ActionTag="750210547" Tag="172" IconVisible="False" LeftMargin="-45.0000" RightMargin="-45.0000" TopMargin="-45.0000" BottomMargin="-45.0000" LeftEage="19" RightEage="19" TopEage="19" BottomEage="19" Scale9OriginX="19" Scale9OriginY="19" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="90.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_bg" ActionTag="211112326" Tag="43" IconVisible="False" LeftMargin="-56.0000" RightMargin="-56.0000" TopMargin="47.9994" BottomMargin="-103.9994" ctype="SpriteObjectData">
            <Size X="112.0000" Y="56.0000" />
            <Children>
              <AbstractNodeData Name="NickNameTxt" ActionTag="2070858058" Tag="54" IconVisible="False" LeftMargin="6.5807" RightMargin="5.4193" TopMargin="6.6673" BottomMargin="26.3327" FontSize="20" LabelText="cccccccccc" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.5807" Y="37.8327" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5052" Y="0.6756" />
                <PreSize X="0.8929" Y="0.4107" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreTxt" ActionTag="1854596814" Tag="55" IconVisible="False" LeftMargin="36.3699" RightMargin="19.6301" TopMargin="28.6316" BottomMargin="4.3684" FontSize="20" LabelText="99999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="64.3699" Y="15.8684" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5747" Y="0.2834" />
                <PreSize X="0.5000" Y="0.4107" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-75.9994" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="role_head_bg.png" Plist="game/game.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="IpTxt" ActionTag="1216826694" Tag="34" IconVisible="False" LeftMargin="-28.5000" RightMargin="-28.5000" TopMargin="-12.0000" BottomMargin="-12.0000" FontSize="20" LabelText="ip相同" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="58.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>