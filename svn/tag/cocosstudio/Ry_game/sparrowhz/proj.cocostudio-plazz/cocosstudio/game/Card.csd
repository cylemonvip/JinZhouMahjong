<GameFile>
  <PropertyGroup Name="Card" Type="Node" ID="6fe3bdf4-ddca-4fc2-80d0-4dd44fd52e38" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="70" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="2011035828" Tag="71" IconVisible="False" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-65.0000" BottomMargin="-65.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="86.0000" Y="130.0000" />
            <Children>
              <AbstractNodeData Name="PaiBg" ActionTag="-479571682" Tag="72" IconVisible="False" LeftEage="28" RightEage="28" TopEage="42" BottomEage="42" Scale9OriginX="28" Scale9OriginY="42" Scale9Width="30" Scale9Height="46" ctype="ImageViewObjectData">
                <Size X="86.0000" Y="130.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="43.0000" Y="65.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="PaiNum" ActionTag="-11231075" Tag="73" IconVisible="False" LeftMargin="-0.0001" RightMargin="0.0001" TopMargin="30.0000" LeftEage="28" RightEage="28" TopEage="33" BottomEage="33" Scale9OriginX="28" Scale9OriginY="33" Scale9Width="30" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="86.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="42.9999" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3846" />
                <PreSize X="1.0000" Y="0.7692" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>