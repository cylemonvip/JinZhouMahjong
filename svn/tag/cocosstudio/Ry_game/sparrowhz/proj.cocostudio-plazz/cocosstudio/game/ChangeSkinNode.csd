<GameFile>
  <PropertyGroup Name="ChangeSkinNode" Type="Node" ID="b4dd2d25-778c-4344-9119-77d5bf0731df" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="138" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-465632409" Tag="142" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="-2009896056" Tag="139" IconVisible="False" LeftMargin="-550.0000" RightMargin="-550.0000" TopMargin="-290.0000" BottomMargin="-290.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="335" RightEage="335" TopEage="149" BottomEage="149" Scale9OriginX="335" Scale9OriginY="149" Scale9Width="348" Scale9Height="154" ctype="PanelObjectData">
            <Size X="1100.0000" Y="580.0000" />
            <Children>
              <AbstractNodeData Name="TitleSp" ActionTag="-1226966260" Tag="140" IconVisible="False" LeftMargin="373.1586" RightMargin="378.8414" TopMargin="-44.7350" BottomMargin="560.7350" ctype="SpriteObjectData">
                <Size X="348.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="TitleTxt" ActionTag="-1626808089" Tag="143" IconVisible="False" LeftMargin="105.8610" RightMargin="107.1390" TopMargin="16.5617" BottomMargin="6.4383" FontSize="33" LabelText="更改皮肤" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="135.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="173.3610" Y="26.9383" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4982" Y="0.4209" />
                    <PreSize X="0.3879" Y="0.6406" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="26" G="26" B="26" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="547.1586" Y="592.7350" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4974" Y="1.0220" />
                <PreSize X="0.3164" Y="0.1103" />
                <FileData Type="PlistSubImage" Path="plaza_image_title_bg.png" Plist="game/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ZBTxt" ActionTag="-818037089" Tag="147" IconVisible="False" LeftMargin="358.5397" RightMargin="658.4603" TopMargin="48.8243" BottomMargin="482.1757" FontSize="40" LabelText="桌布" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="83.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0397" Y="506.6757" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="173" G="216" B="230" />
                <PrePosition X="0.3637" Y="0.8736" />
                <PreSize X="0.0755" Y="0.0845" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="PMTxt" ActionTag="-1166233163" Tag="148" IconVisible="False" LeftMargin="860.0283" RightMargin="156.9717" TopMargin="48.8243" BottomMargin="482.1757" FontSize="40" LabelText="牌面" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="83.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="901.5283" Y="506.6757" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="192" B="203" />
                <PrePosition X="0.8196" Y="0.8736" />
                <PreSize X="0.0755" Y="0.0845" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ZBLayoutContainer" ActionTag="-523888501" Tag="97" IconVisible="False" LeftMargin="63.2166" RightMargin="361.7834" TopMargin="103.0087" BottomMargin="96.9913" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="54" RightEage="54" TopEage="24" BottomEage="24" Scale9OriginX="54" Scale9OriginY="24" Scale9Width="58" Scale9Height="26" ctype="PanelObjectData">
                <Size X="675.0000" Y="380.0000" />
                <AnchorPoint />
                <Position X="63.2166" Y="96.9913" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0575" Y="0.1672" />
                <PreSize X="0.6136" Y="0.6552" />
                <FileData Type="PlistSubImage" Path="game_image_select_gang_back.png" Plist="game/plazaScene.plist" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="PMLayoutContainer" ActionTag="1285862769" Tag="98" IconVisible="False" LeftMargin="791.9686" RightMargin="88.0314" TopMargin="103.0089" BottomMargin="96.9912" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="54" RightEage="54" TopEage="24" BottomEage="24" Scale9OriginX="54" Scale9OriginY="24" Scale9Width="58" Scale9Height="26" ctype="PanelObjectData">
                <Size X="220.0000" Y="380.0000" />
                <AnchorPoint />
                <Position X="791.9686" Y="96.9912" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7200" Y="0.1672" />
                <PreSize X="0.2000" Y="0.6552" />
                <FileData Type="PlistSubImage" Path="game_image_select_gang_back.png" Plist="game/plazaScene.plist" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="885837894" Tag="146" IconVisible="False" LeftMargin="1053.8275" RightMargin="-21.8275" TopMargin="-26.3271" BottomMargin="538.3271" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1087.8275" Y="572.3271" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9889" Y="0.9868" />
                <PreSize X="0.0618" Y="0.1172" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="game/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="game/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="plaza_btn_close_normal.png" Plist="game/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="OkBtn" ActionTag="1297828052" Tag="163" IconVisible="False" LeftMargin="468.5812" RightMargin="471.4188" TopMargin="480.5108" BottomMargin="25.4892" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="130" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="548.5812" Y="62.4892" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4987" Y="0.1077" />
                <PreSize X="0.1455" Y="0.1276" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game/common_btn_ok_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game/common_btn_ok_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game/common_btn_ok.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="plaza_guide_back.png" Plist="game/plazaScene.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>