<GameFile>
  <PropertyGroup Name="ClubManagerNode" Type="Node" ID="0cc655b8-1d59-49be-8124-1ce4dcfe2d72" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="116" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-801322750" Tag="117" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="BgImg" ActionTag="1458075714" Tag="118" IconVisible="False" Scale9Enable="True" LeftEage="143" RightEage="143" TopEage="81" BottomEage="81" Scale9OriginX="143" Scale9OriginY="81" Scale9Width="660" Scale9Height="372" ctype="ImageViewObjectData">
                <Size X="1334.0000" Y="750.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="club/btn_pic/clubbg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="ClubInfo" ActionTag="-936375379" Tag="120" IconVisible="False" LeftMargin="21.1775" RightMargin="1112.8225" TopMargin="73.0844" BottomMargin="44.9156" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="632.0000" />
                <Children>
                  <AbstractNodeData Name="HeadImg" ActionTag="-1328539343" Tag="121" IconVisible="False" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="75.1247" BottomMargin="476.8753" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                    <Size X="80.0000" Y="80.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="516.8753" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.8178" />
                    <PreSize X="0.4000" Y="0.1266" />
                    <FileData Type="PlistSubImage" Path="Avatar139.png" Plist="public/im_head.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="NickNameTxt" ActionTag="-1800468836" Tag="122" IconVisible="False" LeftMargin="34.0000" RightMargin="34.0000" TopMargin="15.6909" BottomMargin="593.3091" FontSize="20" LabelText="昵称: 恭喜发财" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="132.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="604.8091" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.9570" />
                    <PreSize X="0.6600" Y="0.0364" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="IDTxt" ActionTag="2047400873" Tag="123" IconVisible="False" LeftMargin="50.9996" RightMargin="51.0004" TopMargin="43.5363" BottomMargin="565.4637" FontSize="20" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="98.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="99.9996" Y="576.9637" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.9129" />
                    <PreSize X="0.4900" Y="0.0364" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="QunZhuTxt" ActionTag="1383728240" Tag="125" IconVisible="False" LeftMargin="14.0000" RightMargin="14.0000" TopMargin="156.8940" BottomMargin="424.1060" IsCustomSize="True" FontSize="15" LabelText="群主: 拉了阿斯蒂芬" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="172.0000" Y="51.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="449.6060" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5000" Y="0.7114" />
                    <PreSize X="0.8600" Y="0.0807" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="VipBtn" ActionTag="1192154966" Tag="127" IconVisible="False" LeftMargin="29.3799" RightMargin="24.6201" TopMargin="231.9644" BottomMargin="342.0356" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="116" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="146.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="Tips" ActionTag="-249754624" Tag="135" IconVisible="False" LeftMargin="124.7112" RightMargin="-5.7112" TopMargin="-8.1229" BottomMargin="39.1229" ctype="SpriteObjectData">
                        <Size X="27.0000" Y="27.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="138.2112" Y="52.6229" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.9467" Y="0.9073" />
                        <PreSize X="0.1849" Y="0.4655" />
                        <FileData Type="PlistSubImage" Path="sp_dot.png" Plist="public/public.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.3799" Y="371.0356" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5119" Y="0.5871" />
                    <PreSize X="0.7300" Y="0.0918" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/btn_pic/club_menmber_manager_btn_disabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/btn_pic/club_menmber_manager_btn_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/btn_pic/club_menmber_manager_btn_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ManagerGameBtn" ActionTag="1919497546" Tag="128" IconVisible="False" LeftMargin="29.3799" RightMargin="24.6201" TopMargin="466.9857" BottomMargin="107.0143" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="116" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="146.0000" Y="58.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.3799" Y="136.0143" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5119" Y="0.2152" />
                    <PreSize X="0.7300" Y="0.0918" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/btn_pic/club_game_manager_btn_disabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/btn_pic/club_game_manager_btn_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/btn_pic/club_game_manager_btn_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="QuickCreateBtn" ActionTag="-797190260" Tag="129" IconVisible="False" LeftMargin="29.3799" RightMargin="24.6201" TopMargin="545.3259" BottomMargin="28.6741" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="116" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="146.0000" Y="58.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.3799" Y="57.6741" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5119" Y="0.0913" />
                    <PreSize X="0.7300" Y="0.0918" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/btn_pic/club_quick_btn_disabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/btn_pic/club_quick_btn_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/btn_pic/club_quick_btn_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ShareBtn" ActionTag="540214077" VisibleForFrame="False" Tag="130" IconVisible="False" LeftMargin="-58.2954" RightMargin="58.2954" TopMargin="940.6816" BottomMargin="-358.6816" TouchEnable="True" FontSize="25" ButtonText="分享房间" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="200.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="41.7046" Y="-333.6816" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2085" Y="-0.5280" />
                    <PreSize X="1.0000" Y="0.0791" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                    <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="HistoryRoomBtn" ActionTag="-406572778" Tag="242" IconVisible="False" LeftMargin="29.3799" RightMargin="24.6201" TopMargin="388.6453" BottomMargin="185.3547" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="116" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="146.0000" Y="58.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.3799" Y="214.3547" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5119" Y="0.3392" />
                    <PreSize X="0.7300" Y="0.0918" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/btn_pic/club_history_room_btn_disabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/btn_pic/club_history_room_btn_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/btn_pic/club_history_room_btn_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="OnlineRoomBtn" ActionTag="421475825" Tag="34" IconVisible="False" LeftMargin="29.3799" RightMargin="24.6201" TopMargin="310.3046" BottomMargin="263.6954" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="116" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="146.0000" Y="58.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="102.3799" Y="292.6954" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5119" Y="0.4631" />
                    <PreSize X="0.7300" Y="0.0918" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/btn_pic/club_current_room_btn_disabled.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/btn_pic/club_current_room_btn_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/btn_pic/club_current_room_btn_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="21.1775" Y="676.9156" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0159" Y="0.9026" />
                <PreSize X="0.1499" Y="0.8427" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="VipScrollView" ActionTag="1263260122" Tag="142" IconVisible="False" LeftMargin="231.3824" RightMargin="22.6176" TopMargin="70.0000" BottomMargin="30.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="-36" Scale9OriginY="-36" Scale9Width="72" Scale9Height="72" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="1080.0000" Y="650.0000" />
                <AnchorPoint />
                <Position X="231.3824" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1735" Y="0.0400" />
                <PreSize X="0.8096" Y="0.8667" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="1080" Height="730" />
              </AbstractNodeData>
              <AbstractNodeData Name="HistoryRoomScrollView" ActionTag="-977045012" Tag="244" IconVisible="False" LeftMargin="231.3824" RightMargin="22.6176" TopMargin="80.0000" BottomMargin="80.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="-36" Scale9OriginY="-36" Scale9Width="72" Scale9Height="72" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="1080.0000" Y="590.0000" />
                <AnchorPoint />
                <Position X="231.3824" Y="80.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1735" Y="0.1067" />
                <PreSize X="0.8096" Y="0.7867" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="1080" Height="650" />
              </AbstractNodeData>
              <AbstractNodeData Name="OnlineRoomScrollView" ActionTag="737118166" Tag="35" IconVisible="False" LeftMargin="231.3824" RightMargin="22.6176" TopMargin="20.0000" BottomMargin="30.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="-36" Scale9OriginY="-36" Scale9Width="72" Scale9Height="72" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="1080.0000" Y="700.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="231.3824" Y="730.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1735" Y="0.9733" />
                <PreSize X="0.8096" Y="0.9333" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="1080" Height="700" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleSetPanel" ActionTag="283099030" Tag="163" IconVisible="False" LeftMargin="231.3824" RightMargin="22.6177" TopMargin="20.0000" BottomMargin="30.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1080.0000" Y="700.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="771.3824" Y="730.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5782" Y="0.9733" />
                <PreSize X="0.8096" Y="0.9333" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="HistoryTitlePanel" ActionTag="1803510624" Tag="170" IconVisible="False" LeftMargin="231.3800" RightMargin="22.6200" TopMargin="15.1384" BottomMargin="674.8616" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="141" RightEage="141" TopEage="140" BottomEage="140" Scale9OriginX="141" Scale9OriginY="140" Scale9Width="148" Scale9Height="145" ctype="PanelObjectData">
                <Size X="1080.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="-156328304" Tag="171" IconVisible="False" LeftMargin="39.8217" RightMargin="980.1783" TopMargin="13.0000" BottomMargin="13.0000" FontSize="30" LabelText="局号" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.8217" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.0646" Y="0.5000" />
                    <PreSize X="0.0556" Y="0.5667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0" ActionTag="1213097880" Tag="172" IconVisible="False" LeftMargin="155.4949" RightMargin="834.5051" TopMargin="13.0000" BottomMargin="13.0000" FontSize="30" LabelText="参与者" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.4949" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.1856" Y="0.5000" />
                    <PreSize X="0.0833" Y="0.5667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0_0" ActionTag="809104802" Tag="173" IconVisible="False" LeftMargin="663.4934" RightMargin="356.5066" TopMargin="13.0000" BottomMargin="13.0000" FontSize="30" LabelText="积分" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="693.4934" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.6421" Y="0.5000" />
                    <PreSize X="0.0556" Y="0.5667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0_0_0" ActionTag="1451088282" Tag="174" IconVisible="False" LeftMargin="827.4941" RightMargin="192.5059" TopMargin="13.0002" BottomMargin="12.9998" FontSize="30" LabelText="时间" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="857.4941" Y="29.9998" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.7940" Y="0.5000" />
                    <PreSize X="0.0556" Y="0.5667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_1_0_1" ActionTag="-567734709" Tag="180" IconVisible="False" LeftMargin="525.4951" RightMargin="524.5049" TopMargin="9.0210" BottomMargin="16.9790" FontSize="30" LabelText="ID" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="30.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="540.4951" Y="33.9790" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5005" Y="0.5663" />
                    <PreSize X="0.0278" Y="0.5667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="771.3800" Y="734.8616" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5782" Y="0.9798" />
                <PreSize X="0.8096" Y="0.0800" />
                <FileData Type="Normal" Path="club/pic/sub_bg.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="DeleteAllRoomBtn" ActionTag="243164049" Tag="117" IconVisible="False" LeftMargin="692.2457" RightMargin="481.7543" TopMargin="680.3003" BottomMargin="29.6997" TouchEnable="True" FontSize="30" ButtonText="一键删除" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="772.2457" Y="49.6997" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5789" Y="0.0663" />
                <PreSize X="0.1199" Y="0.0533" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="1652105241" Tag="119" IconVisible="False" LeftMargin="1264.4136" RightMargin="-1.4136" TopMargin="1.7123" BottomMargin="677.2877" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="41" Scale9Height="49" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="71.0000" Y="71.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1299.9136" Y="712.7877" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9744" Y="0.9504" />
                <PreSize X="0.0532" Y="0.0947" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/back_btn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/back_btn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/back_btn_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RoomModelNode" ActionTag="-1493796860" Tag="59" IconVisible="False" LeftMargin="-432.5772" RightMargin="-647.4228" TopMargin="542.3461" BottomMargin="-742.3461" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="141" RightEage="141" TopEage="140" BottomEage="140" Scale9OriginX="141" Scale9OriginY="140" Scale9Width="148" Scale9Height="145" ctype="PanelObjectData">
            <Size X="1080.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="DetailBtn" ActionTag="883209227" Tag="52" IconVisible="False" LeftMargin="942.0068" RightMargin="5.9932" TopMargin="71.5000" BottomMargin="71.5000" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="102" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="132.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1008.0068" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9333" Y="0.5000" />
                <PreSize X="0.1222" Y="0.2850" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/btnmore_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/btnmore_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/btnmore_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RoomId" ActionTag="-279408765" Tag="175" IconVisible="False" LeftMargin="20.7994" RightMargin="958.2006" TopMargin="83.0000" BottomMargin="83.0000" FontSize="30" LabelText="000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="101.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="71.2994" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.0660" Y="0.5000" />
                <PreSize X="0.0935" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CostFeeTxt" ActionTag="1322433580" Tag="68" IconVisible="False" LeftMargin="28.7988" RightMargin="966.2012" TopMargin="132.9899" BottomMargin="44.0101" FontSize="20" LabelText="(2张房卡)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="85.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="71.2988" Y="55.5101" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.0660" Y="0.2776" />
                <PreSize X="0.0787" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadImg_1" ActionTag="2138377117" Tag="191" IconVisible="False" LeftMargin="154.0909" RightMargin="885.9091" TopMargin="9.0993" BottomMargin="150.9007" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="40.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="174.0909" Y="170.9007" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1612" Y="0.8545" />
                <PreSize X="0.0370" Y="0.2000" />
                <FileData Type="PlistSubImage" Path="Avatar119.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinNameTxt_1" ActionTag="447309981" Tag="176" IconVisible="False" LeftMargin="195.9232" RightMargin="664.0768" TopMargin="18.0993" BottomMargin="152.9007" IsCustomSize="True" FontSize="25" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="220.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="195.9232" Y="167.4007" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.1814" Y="0.8370" />
                <PreSize X="0.2037" Y="0.1450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinNameTxt_2" ActionTag="-1618324691" Tag="177" IconVisible="False" LeftMargin="195.9200" RightMargin="664.0800" TopMargin="62.6720" BottomMargin="108.3280" IsCustomSize="True" FontSize="25" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="220.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="195.9200" Y="122.8280" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.1814" Y="0.6141" />
                <PreSize X="0.2037" Y="0.1450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinNameTxt_3" ActionTag="-266030456" Tag="178" IconVisible="False" LeftMargin="195.9232" RightMargin="664.0768" TopMargin="110.2201" BottomMargin="60.7799" IsCustomSize="True" FontSize="25" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="220.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="195.9232" Y="75.2799" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.1814" Y="0.3764" />
                <PreSize X="0.2037" Y="0.1450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinNameTxt_4" ActionTag="-1799120722" Tag="179" IconVisible="False" LeftMargin="195.9232" RightMargin="664.0768" TopMargin="155.7682" BottomMargin="15.2318" IsCustomSize="True" FontSize="25" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="220.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="195.9232" Y="29.7318" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.1814" Y="0.1487" />
                <PreSize X="0.2037" Y="0.1450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CreateTimeTxt" ActionTag="-1451728930" Tag="63" IconVisible="False" LeftMargin="723.1531" RightMargin="106.8469" TopMargin="49.9999" BottomMargin="50.0001" IsCustomSize="True" FontSize="30" LabelText="2018-09-09 18:36:59" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="250.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="848.1531" Y="100.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.7853" Y="0.5000" />
                <PreSize X="0.2315" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="GotBtn" ActionTag="1841809889" VisibleForFrame="False" Tag="114" IconVisible="False" LeftMargin="989.1036" RightMargin="10.8964" TopMargin="60.0000" BottomMargin="60.0000" TouchEnable="True" FontSize="14" ButtonText="已收" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="80.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1029.1036" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9529" Y="0.5000" />
                <PreSize X="0.0741" Y="0.4000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinIDTxt_1" ActionTag="189188845" Tag="181" IconVisible="False" LeftMargin="487.5277" RightMargin="491.4723" TopMargin="15.5993" BottomMargin="150.4007" FontSize="30" LabelText="000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="101.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="538.0277" Y="167.4007" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.4982" Y="0.8370" />
                <PreSize X="0.0935" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinIDTxt_2" ActionTag="1436964679" Tag="183" IconVisible="False" LeftMargin="487.5277" RightMargin="491.4723" TopMargin="60.1720" BottomMargin="105.8280" FontSize="30" LabelText="000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="101.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="538.0277" Y="122.8280" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.4982" Y="0.6141" />
                <PreSize X="0.0935" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinIDTxt_3" ActionTag="-579891007" Tag="184" IconVisible="False" LeftMargin="487.5277" RightMargin="491.4723" TopMargin="107.7202" BottomMargin="58.2798" FontSize="30" LabelText="000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="101.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="538.0277" Y="75.2798" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.4982" Y="0.3764" />
                <PreSize X="0.0935" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinIDTxt_4" ActionTag="-1241587005" Tag="185" IconVisible="False" LeftMargin="487.5277" RightMargin="491.4723" TopMargin="153.2682" BottomMargin="12.7318" FontSize="30" LabelText="000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="101.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="538.0277" Y="29.7318" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.4982" Y="0.1487" />
                <PreSize X="0.0935" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinScoreTxt_1" ActionTag="-1714845234" Tag="186" IconVisible="False" LeftMargin="680.7087" RightMargin="382.2913" TopMargin="15.5993" BottomMargin="150.4007" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="689.2087" Y="167.4007" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.6382" Y="0.8370" />
                <PreSize X="0.0157" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinScoreTxt_2" ActionTag="107358005" Tag="187" IconVisible="False" LeftMargin="680.7087" RightMargin="382.2913" TopMargin="60.1720" BottomMargin="105.8280" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="689.2087" Y="122.8280" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.6382" Y="0.6141" />
                <PreSize X="0.0157" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinScoreTxt_3" ActionTag="-683952928" Tag="188" IconVisible="False" LeftMargin="680.7087" RightMargin="382.2913" TopMargin="107.7202" BottomMargin="58.2798" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="689.2087" Y="75.2798" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.6382" Y="0.3764" />
                <PreSize X="0.0157" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinScoreTxt_4" ActionTag="-56305342" Tag="189" IconVisible="False" LeftMargin="680.7087" RightMargin="382.2913" TopMargin="153.2682" BottomMargin="12.7318" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="17.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="689.2087" Y="29.7318" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.6382" Y="0.1487" />
                <PreSize X="0.0157" Y="0.1700" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadImg_2" ActionTag="-1244260762" Tag="192" IconVisible="False" LeftMargin="154.0909" RightMargin="885.9091" TopMargin="55.3222" BottomMargin="104.6778" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="40.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="174.0909" Y="124.6778" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1612" Y="0.6234" />
                <PreSize X="0.0370" Y="0.2000" />
                <FileData Type="PlistSubImage" Path="Avatar119.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadImg_3" ActionTag="263047955" Tag="193" IconVisible="False" LeftMargin="154.0909" RightMargin="885.9091" TopMargin="101.5452" BottomMargin="58.4548" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="40.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="174.0909" Y="78.4548" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1612" Y="0.3923" />
                <PreSize X="0.0370" Y="0.2000" />
                <FileData Type="PlistSubImage" Path="Avatar119.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadImg_4" ActionTag="386670740" Tag="194" IconVisible="False" LeftMargin="154.0909" RightMargin="885.9091" TopMargin="147.7681" BottomMargin="12.2319" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="40.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="174.0909" Y="32.2319" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1612" Y="0.1612" />
                <PreSize X="0.0370" Y="0.2000" />
                <FileData Type="PlistSubImage" Path="Avatar119.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="DaYingJiaTxt_1" ActionTag="1471729309" Tag="64" IconVisible="False" LeftMargin="1501.9568" RightMargin="-495.9568" TopMargin="38.9684" BottomMargin="138.0316" FontSize="20" LabelText="(大赢家)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1501.9568" Y="149.5316" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="1.3907" Y="0.7477" />
                <PreSize X="0.0685" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DaYingJiaTxt_2" ActionTag="852584649" Tag="65" IconVisible="False" LeftMargin="1500.6946" RightMargin="-494.6946" TopMargin="93.5375" BottomMargin="83.4625" FontSize="20" LabelText="(大赢家)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1500.6946" Y="94.9625" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="1.3895" Y="0.4748" />
                <PreSize X="0.0685" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DaYingJiaTxt_3" ActionTag="334522500" Tag="66" IconVisible="False" LeftMargin="1510.6946" RightMargin="-504.6946" TopMargin="103.5375" BottomMargin="73.4625" FontSize="20" LabelText="(大赢家)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1510.6946" Y="84.9625" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="1.3988" Y="0.4248" />
                <PreSize X="0.0685" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DaYingJiaTxt_4" ActionTag="-1157717042" Tag="67" IconVisible="False" LeftMargin="1520.6946" RightMargin="-514.6946" TopMargin="113.5375" BottomMargin="63.4625" FontSize="20" LabelText="(大赢家)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1520.6946" Y="74.9625" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="1.4081" Y="0.3748" />
                <PreSize X="0.0685" Y="0.1150" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-432.5772" Y="-742.3461" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="club/pic/sub_bg.png" Plist="" />
            <SingleColor A="255" R="255" G="165" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>