<GameFile>
  <PropertyGroup Name="ClubNode" Type="Node" ID="d488fd0e-b5a7-4da3-9664-d2d528e79372" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="62" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="532901701" Tag="63" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="ImageBg" ActionTag="1755730183" Tag="64" IconVisible="False" LeftMargin="167.0000" RightMargin="167.0000" TopMargin="75.0000" BottomMargin="75.0000" Scale9Enable="True" LeftEage="143" RightEage="143" TopEage="81" BottomEage="81" Scale9OriginX="143" Scale9OriginY="81" Scale9Width="148" Scale9Height="84" ctype="ImageViewObjectData">
                <Size X="1000.0000" Y="600.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7496" Y="0.8000" />
                <FileData Type="Normal" Path="KFMJ_grxx_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleSp" ActionTag="815260922" Tag="70" IconVisible="False" LeftMargin="474.8558" RightMargin="471.1442" TopMargin="38.4719" BottomMargin="634.5281" ctype="SpriteObjectData">
                <Size X="388.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="668.8558" Y="673.0281" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5014" Y="0.8974" />
                <PreSize X="0.2909" Y="0.1027" />
                <FileData Type="Normal" Path="club/join_pic/join_title.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ListView" ActionTag="2055396635" Tag="65" IconVisible="False" LeftMargin="177.0000" RightMargin="177.0000" TopMargin="115.0000" BottomMargin="185.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="358" Scale9Height="353" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="980.0000" Y="450.0000" />
                <AnchorPoint ScaleX="1.0000" />
                <Position X="1157.0000" Y="185.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8673" Y="0.2467" />
                <PreSize X="0.7346" Y="0.6000" />
                <FileData Type="Normal" Path="club/pic/sub_bg.png" Plist="" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="CreateTipPanel" ActionTag="1056736259" VisibleForFrame="False" Tag="71" IconVisible="False" LeftMargin="176.1570" RightMargin="667.8430" TopMargin="115.0000" BottomMargin="185.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                <Size X="490.0000" Y="450.0000" />
                <AnchorPoint />
                <Position X="176.1570" Y="185.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1321" Y="0.2467" />
                <PreSize X="0.3673" Y="0.6000" />
                <FileData Type="Normal" Path="club/btn_pic/record_9scale_bg.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="CreateClubBtn" ActionTag="324926799" Tag="67" IconVisible="False" LeftMargin="366.0000" RightMargin="766.0000" TopMargin="579.2044" BottomMargin="96.7956" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="172" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="202.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="467.0000" Y="133.7956" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3501" Y="0.1784" />
                <PreSize X="0.1514" Y="0.0987" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/club_create_btn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/club_create_btn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/club_create_btn_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NoTips" ActionTag="-212166569" Tag="66" IconVisible="False" LeftMargin="214.2944" RightMargin="219.7056" TopMargin="308.2402" BottomMargin="389.7598" FontSize="45" LabelText="你还没有加入任何俱乐部，赶紧加入一个吧！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="900.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="664.2944" Y="415.7598" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.4980" Y="0.5543" />
                <PreSize X="0.6747" Y="0.0693" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JoinClubBtn" ActionTag="-1581503504" Tag="68" IconVisible="False" LeftMargin="762.5000" RightMargin="362.5000" TopMargin="579.2044" BottomMargin="95.7956" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="179" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="209.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="867.0000" Y="133.2956" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6499" Y="0.1777" />
                <PreSize X="0.1567" Y="0.1000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/club_join_btn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/club_join_btn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/club_join_btn_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="-16957692" Tag="69" IconVisible="False" LeftMargin="1127.8042" RightMargin="138.1958" TopMargin="46.7697" BottomMargin="635.2303" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1161.8042" Y="669.2303" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8709" Y="0.8923" />
                <PreSize X="0.0510" Y="0.0907" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <PressedFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <NormalFileData Type="PlistSubImage" Path="common_btn_close_normal.png" Plist="common.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="0" G="0" B="0" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ClubModel" ActionTag="1737817471" Tag="55" IconVisible="False" LeftMargin="-490.0000" RightMargin="-490.0000" TopMargin="450.0000" BottomMargin="-550.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="141" RightEage="141" TopEage="140" BottomEage="140" Scale9OriginX="141" Scale9OriginY="140" Scale9Width="148" Scale9Height="145" ctype="PanelObjectData">
            <Size X="980.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="HeadImage" ActionTag="524074529" Tag="56" IconVisible="False" LeftMargin="11.8839" RightMargin="888.1161" TopMargin="9.6650" BottomMargin="10.3350" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="51.8839" Y="50.3350" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0529" Y="0.5034" />
                <PreSize X="0.0816" Y="0.8000" />
                <FileData Type="PlistSubImage" Path="Avatar10.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="StatusTxt" ActionTag="1780294269" Tag="57" IconVisible="False" LeftMargin="107.3526" RightMargin="632.6474" TopMargin="59.6449" BottomMargin="17.3551" IsCustomSize="True" FontSize="20" LabelText="状态: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="240.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="107.3526" Y="28.8551" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.1095" Y="0.2886" />
                <PreSize X="0.2449" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="QunZhuNameTxt" ActionTag="1973223391" Tag="58" IconVisible="False" LeftMargin="107.3526" RightMargin="632.6474" TopMargin="14.2867" BottomMargin="62.7133" IsCustomSize="True" FontSize="20" LabelText="群主: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="240.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="107.3526" Y="74.2133" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.1095" Y="0.7421" />
                <PreSize X="0.2449" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ClubNumTxt" ActionTag="364544794" Tag="59" IconVisible="False" LeftMargin="573.9989" RightMargin="166.0011" TopMargin="14.2867" BottomMargin="62.7133" IsCustomSize="True" FontSize="20" LabelText="俱乐部号: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="240.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="573.9989" Y="74.2133" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.5857" Y="0.7421" />
                <PreSize X="0.2449" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DescriptionTxt" ActionTag="422307355" Tag="60" IconVisible="False" LeftMargin="373.2806" RightMargin="166.7194" TopMargin="59.6449" BottomMargin="17.3551" IsCustomSize="True" FontSize="20" LabelText="描述: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="440.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="373.2806" Y="28.8551" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.3809" Y="0.2886" />
                <PreSize X="0.4490" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameTxt" ActionTag="631989621" Tag="85" IconVisible="False" LeftMargin="373.2806" RightMargin="554.7194" TopMargin="14.2867" BottomMargin="62.7133" FontSize="20" LabelText="游戏: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="52.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="373.2806" Y="74.2133" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.3809" Y="0.7421" />
                <PreSize X="0.0531" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="EnterClub" ActionTag="1975908722" Tag="86" IconVisible="False" LeftMargin="818.9966" RightMargin="5.0034" TopMargin="21.5000" BottomMargin="21.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="126" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="156.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="896.9966" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9153" Y="0.5000" />
                <PreSize X="0.1592" Y="0.5700" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/enter_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/enter_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/enter.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position Y="-450.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="club/pic/sub_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>