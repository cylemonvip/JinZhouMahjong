<GameFile>
  <PropertyGroup Name="HzMahjongRuleNode" Type="Node" ID="16ffd60d-00e8-40d7-93d3-a38207e12f88" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="70" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1845352816" Tag="71" IconVisible="False" LeftMargin="-540.0000" RightMargin="-540.0000" TopMargin="-275.0000" BottomMargin="-275.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1080.0000" Y="550.0000" />
            <Children>
              <AbstractNodeData Name="QuanCount" ActionTag="991968127" Tag="78" IconVisible="False" LeftMargin="161.7610" RightMargin="841.2390" TopMargin="36.0016" BottomMargin="479.9984" FontSize="30" LabelText="圈数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2610" Y="496.9984" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.9036" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_1" ActionTag="67559082" Tag="77" IconVisible="False" LeftMargin="258.7016" RightMargin="631.2983" TopMargin="28.8096" BottomMargin="467.1904" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="721853163" Tag="79" IconVisible="False" LeftMargin="45.0000" RightMargin="-19.0000" TopMargin="12.0000" BottomMargin="8.0000" FontSize="30" LabelText="1圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="164.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.4630" />
                    <PreSize X="0.8632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="353.7016" Y="494.1904" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3275" Y="0.8985" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_2" ActionTag="1798503918" Tag="82" IconVisible="False" LeftMargin="498.5792" RightMargin="391.4208" TopMargin="28.3505" BottomMargin="467.6495" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-176031383" Tag="83" IconVisible="False" LeftMargin="45.0000" RightMargin="98.0000" TopMargin="12.0000" BottomMargin="8.0000" FontSize="30" LabelText="8圈" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="47.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.4630" />
                    <PreSize X="0.2474" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="593.5792" Y="494.6495" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5496" Y="0.8994" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_3" ActionTag="2105631491" Tag="86" IconVisible="False" LeftMargin="738.4559" RightMargin="151.5441" TopMargin="25.3846" BottomMargin="470.6154" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-1566285270" Tag="87" IconVisible="False" LeftMargin="45.0000" RightMargin="81.0000" TopMargin="12.0000" BottomMargin="8.0000" FontSize="30" LabelText="10圈" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="64.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.4630" />
                    <PreSize X="0.3368" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="833.4559" Y="497.6154" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7717" Y="0.9048" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="PlayerCount" ActionTag="631884615" Tag="90" IconVisible="False" LeftMargin="161.7610" RightMargin="841.2390" TopMargin="147.7957" BottomMargin="368.2043" FontSize="30" LabelText="人数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2610" Y="385.2043" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.7004" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Rule" ActionTag="1185422420" Tag="95" IconVisible="False" LeftMargin="161.7610" RightMargin="841.2390" TopMargin="253.5901" BottomMargin="262.4099" FontSize="30" LabelText="玩法: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2610" Y="279.4099" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.5080" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RenShuCheckBox_1" ActionTag="107306824" Tag="91" IconVisible="False" LeftMargin="258.7016" RightMargin="631.2983" TopMargin="140.7616" BottomMargin="355.2384" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-1255763356" Tag="92" IconVisible="False" LeftMargin="45.0000" RightMargin="85.0000" TopMargin="12.0000" BottomMargin="8.0000" FontSize="30" LabelText="四人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.4630" />
                    <PreSize X="0.3158" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="353.7016" Y="382.2384" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3275" Y="0.6950" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RenShuCheckBox_2" ActionTag="478569600" Tag="93" IconVisible="False" LeftMargin="498.5792" RightMargin="391.4208" TopMargin="137.7958" BottomMargin="358.2042" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-1424805671" Tag="94" IconVisible="False" LeftMargin="45.0000" RightMargin="85.0000" TopMargin="12.0000" BottomMargin="8.0000" FontSize="30" LabelText="三人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.4630" />
                    <PreSize X="0.3158" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="593.5792" Y="385.2042" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5496" Y="0.7004" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_1" ActionTag="-1116483445" Tag="100" IconVisible="False" LeftMargin="738.4550" RightMargin="201.5450" TopMargin="243.5901" BottomMargin="252.4099" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1116091565" Tag="101" IconVisible="False" LeftMargin="45.0000" RightMargin="5.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="天地胡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.6429" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="808.4550" Y="279.4099" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7486" Y="0.5080" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_2" ActionTag="2073542492" Tag="96" IconVisible="False" LeftMargin="258.7011" RightMargin="681.2990" TopMargin="243.5901" BottomMargin="252.4099" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1404166657" Tag="97" IconVisible="False" LeftMargin="45.0000" RightMargin="35.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="会牌" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.4286" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="328.7011" Y="279.4099" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3044" Y="0.5080" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_3" ActionTag="90677030" Tag="102" IconVisible="False" LeftMargin="258.7011" RightMargin="681.2990" TopMargin="317.0905" BottomMargin="178.9095" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-2087199489" Tag="103" IconVisible="False" LeftMargin="45.0000" RightMargin="-25.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="穷胡加倍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.8571" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="328.7011" Y="205.9095" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3044" Y="0.3744" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_4" ActionTag="-389712138" Tag="108" IconVisible="False" LeftMargin="258.7011" RightMargin="681.2990" TopMargin="390.5910" BottomMargin="105.4090" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="51657511" Tag="109" IconVisible="False" LeftMargin="45.0000" RightMargin="-16.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="封顶150" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="111.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.7929" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="328.7011" Y="132.4090" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3044" Y="0.2407" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_5" ActionTag="-241356873" Tag="98" IconVisible="False" LeftMargin="498.5792" RightMargin="441.4208" TopMargin="243.5901" BottomMargin="252.4099" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-1524695977" Tag="99" IconVisible="False" LeftMargin="45.0000" RightMargin="35.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="三清" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.4286" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="568.5792" Y="279.4099" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5265" Y="0.5080" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_6" ActionTag="-550104170" Tag="104" IconVisible="False" LeftMargin="498.5795" RightMargin="441.4205" TopMargin="317.0905" BottomMargin="178.9095" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1058202745" Tag="105" IconVisible="False" LeftMargin="45.0000" RightMargin="35.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="七对" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.4286" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="568.5795" Y="205.9095" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5265" Y="0.3744" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_7" ActionTag="900976730" Tag="106" IconVisible="False" LeftMargin="738.4550" RightMargin="201.5450" TopMargin="317.0905" BottomMargin="178.9095" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="457078138" Tag="107" IconVisible="False" LeftMargin="45.0000" RightMargin="5.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="清一色" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.6429" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="808.4550" Y="205.9095" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7486" Y="0.3744" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_8" ActionTag="-2063783356" Tag="110" IconVisible="False" LeftMargin="498.5792" RightMargin="441.4208" TopMargin="390.5908" BottomMargin="105.4092" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="140050442" Tag="111" IconVisible="False" LeftMargin="45.0000" RightMargin="-16.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="封顶200" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="111.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.7929" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="568.5792" Y="132.4092" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5265" Y="0.2407" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="SureRuleBtn" ActionTag="-1645443379" Tag="114" IconVisible="False" LeftMargin="800.0000" RightMargin="20.0000" TopMargin="450.0000" BottomMargin="10.0000" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="230" Scale9Height="68" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="260.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="930.0000" Y="55.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8611" Y="0.1000" />
                <PreSize X="0.2407" Y="0.1636" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/sure_rule_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/sure_rule_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/sure_rule.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Line_1" ActionTag="-1521366778" Tag="247" IconVisible="False" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="197.0000" BottomMargin="351.0000" ctype="SpriteObjectData">
                <Size X="964.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.0000" Y="352.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6400" />
                <PreSize X="0.8926" Y="0.0036" />
                <FileData Type="Normal" Path="club/btn_pic/KFMJ_CreateRoom_18.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Line_2" ActionTag="-149102624" Tag="248" IconVisible="False" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="84.0000" BottomMargin="464.0000" ctype="SpriteObjectData">
                <Size X="964.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.0000" Y="465.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8455" />
                <PreSize X="0.8926" Y="0.0036" />
                <FileData Type="Normal" Path="club/btn_pic/KFMJ_CreateRoom_18.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Line_3" ActionTag="1594985867" Tag="249" IconVisible="False" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="299.0000" BottomMargin="249.0000" ctype="SpriteObjectData">
                <Size X="964.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.0000" Y="250.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4545" />
                <PreSize X="0.8926" Y="0.0036" />
                <FileData Type="Normal" Path="club/btn_pic/KFMJ_CreateRoom_18.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Line_4" ActionTag="1762997167" Tag="250" IconVisible="False" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="369.0000" BottomMargin="179.0000" ctype="SpriteObjectData">
                <Size X="964.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.0000" Y="180.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3273" />
                <PreSize X="0.8926" Y="0.0036" />
                <FileData Type="Normal" Path="club/btn_pic/KFMJ_CreateRoom_18.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Line_5" ActionTag="2054918228" Tag="251" IconVisible="False" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="449.0000" BottomMargin="99.0000" ctype="SpriteObjectData">
                <Size X="964.0000" Y="2.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1818" />
                <PreSize X="0.8926" Y="0.0036" />
                <FileData Type="Normal" Path="club/btn_pic/KFMJ_CreateRoom_18.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>