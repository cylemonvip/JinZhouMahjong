<GameFile>
  <PropertyGroup Name="FaqLayer" Type="Layer" ID="0fd6bfea-c7ed-40b9-a1cb-630c098d8643" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="170" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="-2031539996" Tag="106" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_top_bg_1" ActionTag="394712378" Tag="189" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-1.0000" RightMargin="-1.0000" BottomMargin="646.0000" ctype="SpriteObjectData">
            <Size X="1336.0000" Y="104.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="667.0000" Y="750.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="1.0015" Y="0.1387" />
            <FileData Type="PlistSubImage" Path="sp_top_bg.png" Plist="public/public.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_back" ActionTag="559051940" Tag="186" IconVisible="False" LeftMargin="38.0000" RightMargin="1222.0000" TopMargin="13.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="46" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="74.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="75.0000" Y="700.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0562" Y="0.9333" />
            <PreSize X="0.0555" Y="0.0987" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="btn_back_0.png" Plist="public/public.plist" />
            <PressedFileData Type="PlistSubImage" Path="btn_back_1.png" Plist="public/public.plist" />
            <NormalFileData Type="PlistSubImage" Path="btn_back_0.png" Plist="public/public.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_modify_title_3" ActionTag="682117731" Tag="187" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="573.5000" RightMargin="573.5000" TopMargin="19.1005" BottomMargin="677.8995" ctype="SpriteObjectData">
            <Size X="187.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="704.3995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9392" />
            <PreSize X="0.1402" Y="0.0707" />
            <FileData Type="Normal" Path="feedback/sp_faq_title.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_public_frame" ActionTag="742738257" Tag="188" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="22.5000" RightMargin="22.5000" TopMargin="122.5250" BottomMargin="22.4750" ctype="SpriteObjectData">
            <Size X="1289.0000" Y="605.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="324.9750" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4333" />
            <PreSize X="0.9663" Y="0.8067" />
            <FileData Type="PlistSubImage" Path="sp_public_frame_0.png" Plist="public/public.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_public_frame_2_2" ActionTag="-1507379811" Tag="27" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="71.5000" RightMargin="71.5000" TopMargin="146.5250" BottomMargin="46.4750" ctype="SpriteObjectData">
            <Size X="1191.0000" Y="557.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="324.9750" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4333" />
            <PreSize X="0.8928" Y="0.7427" />
            <FileData Type="PlistSubImage" Path="sp_public_frame_2.png" Plist="public/public.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>