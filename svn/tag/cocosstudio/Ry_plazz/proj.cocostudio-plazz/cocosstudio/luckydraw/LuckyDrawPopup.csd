<GameFile>
  <PropertyGroup Name="LuckyDrawPopup" Type="Node" ID="1f99d08e-d8f2-4e2d-a9f6-b9a35ecca034" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="36" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1474251728" Tag="37" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="SpBg" ActionTag="-845113350" Tag="38" IconVisible="False" LeftMargin="358.5000" RightMargin="358.5000" TopMargin="146.5000" BottomMargin="146.5000" ctype="SpriteObjectData">
                <Size X="617.0000" Y="457.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4625" Y="0.6093" />
                <FileData Type="Normal" Path="luckydraw/luckydrawresultbg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpGift" ActionTag="2091785497" Tag="42" IconVisible="False" LeftMargin="673.6428" RightMargin="595.3572" TopMargin="217.8452" BottomMargin="452.1548" ctype="SpriteObjectData">
                <Size X="65.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="706.1428" Y="492.1548" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5293" Y="0.6562" />
                <PreSize X="0.0487" Y="0.1067" />
                <FileData Type="Normal" Path="luckydraw/once_again.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="TxtUserID" ActionTag="-1707077382" Tag="43" IconVisible="False" LeftMargin="516.2313" RightMargin="718.7687" TopMargin="351.1748" BottomMargin="363.8252" FontSize="30" LabelText="123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="516.2313" Y="381.3252" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.3870" Y="0.5084" />
                <PreSize X="0.0742" Y="0.0467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TxtCurrTime" ActionTag="-1644922298" Tag="44" IconVisible="False" LeftMargin="536.1060" RightMargin="553.8940" TopMargin="399.9437" BottomMargin="315.0563" FontSize="30" LabelText="2018.02.11 12:12:21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="244.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="536.1060" Y="332.5563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.4019" Y="0.4434" />
                <PreSize X="0.1829" Y="0.0467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnGot" ActionTag="1827212157" Tag="45" IconVisible="False" LeftMargin="574.7216" RightMargin="577.2784" TopMargin="454.9932" BottomMargin="238.0068" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="152" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="182.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="665.7216" Y="266.5068" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4990" Y="0.3553" />
                <PreSize X="0.1364" Y="0.0760" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="luckydraw/btn_receive_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="luckydraw/btn_receive_1.png" Plist="" />
                <NormalFileData Type="Normal" Path="luckydraw/btn_receive_0.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnClose" ActionTag="1538205922" Tag="14" IconVisible="False" LeftMargin="924.9384" RightMargin="341.0616" TopMargin="124.8696" BottomMargin="557.1304" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="958.9384" Y="591.1304" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7188" Y="0.7882" />
                <PreSize X="0.0510" Y="0.0907" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <PressedFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <NormalFileData Type="PlistSubImage" Path="common_btn_close_normal.png" Plist="common.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>