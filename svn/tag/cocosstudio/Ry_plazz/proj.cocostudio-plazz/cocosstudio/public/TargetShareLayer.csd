<GameFile>
  <PropertyGroup Name="TargetShareLayer" Type="Layer" ID="44453b4e-b335-4dbc-890c-3cad41a0b814" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="82" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="panel_mask" ActionTag="277656328" Tag="93" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="panel_holder" ActionTag="-1554517262" Tag="83" IconVisible="False" LeftMargin="312.0000" RightMargin="312.0000" TopMargin="190.0000" BottomMargin="190.0000" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" LeftEage="234" RightEage="234" TopEage="122" BottomEage="122" Scale9OriginX="234" Scale9OriginY="122" Scale9Width="242" Scale9Height="126" ctype="PanelObjectData">
            <Size X="710.0000" Y="370.0000" />
            <Children>
              <AbstractNodeData Name="btn_wechat" ActionTag="-1365541386" Tag="84" IconVisible="False" LeftMargin="114.5705" RightMargin="415.4295" TopMargin="76.2405" BottomMargin="105.7595" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="166" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="188.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="204.5705" Y="199.7595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2881" Y="0.5399" />
                <PreSize X="0.2535" Y="0.5081" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="wxshare/wxhy_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="wxshare/wxhy_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="wxshare/wxhy.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_cycle" ActionTag="9509775" Tag="88" IconVisible="False" LeftMargin="420.7330" RightMargin="109.2670" TopMargin="76.2405" BottomMargin="105.7595" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="166" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="188.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="510.7330" Y="199.7595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7193" Y="0.5399" />
                <PreSize X="0.2535" Y="0.5081" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="wxshare/wxpyq_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="wxshare/wxpyq_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="wxshare/wxpyq.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5322" Y="0.4933" />
            <FileData Type="Normal" Path="wxshare/wxsharebg.png" Plist="" />
            <SingleColor A="255" R="255" G="255" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>