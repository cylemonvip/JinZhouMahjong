<GameFile>
  <PropertyGroup Name="UserNode" Type="Node" ID="56cf71f8-a445-4866-ba7a-1b07645bd7ca" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="65" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1861979460" Tag="66" IconVisible="False" LeftMargin="-123.0000" RightMargin="-123.0000" TopMargin="-187.0000" BottomMargin="-187.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="81" RightEage="81" TopEage="123" BottomEage="123" Scale9OriginX="81" Scale9OriginY="123" Scale9Width="84" Scale9Height="128" ctype="PanelObjectData">
            <Size X="246.0000" Y="374.0000" />
            <Children>
              <AbstractNodeData Name="HeadNode" ActionTag="-944479632" Tag="67" IconVisible="True" LeftMargin="50.4889" RightMargin="195.5111" TopMargin="50.8643" BottomMargin="323.1357" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="50.4889" Y="323.1357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2052" Y="0.8640" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="-338791048" Tag="68" IconVisible="False" LeftMargin="104.2072" RightMargin="21.7928" TopMargin="22.9551" BottomMargin="328.0449" FontSize="20" LabelText="名称可能很长" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2072" Y="339.5449" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4236" Y="0.9079" />
                <PreSize X="0.4878" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDTxt" ActionTag="562549184" Tag="69" IconVisible="False" LeftMargin="104.2072" RightMargin="43.7928" TopMargin="59.4624" BottomMargin="291.5376" FontSize="20" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2072" Y="303.0376" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.4236" Y="0.8103" />
                <PreSize X="0.3984" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleHuangShangCountTxt" ActionTag="1437450287" Tag="70" IconVisible="False" LeftMargin="29.6361" RightMargin="116.3639" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="皇上次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6361" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.6484" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleNiangNiangCountTxt" ActionTag="1121201752" Tag="71" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="154.6117" BottomMargin="196.3883" FontSize="20" LabelText="娘娘次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="207.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.5559" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleBigXueCountTxt" ActionTag="41647414" Tag="72" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="189.2392" BottomMargin="161.7608" FontSize="20" LabelText="大血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="173.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.4633" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleSmallXueCountTxt" ActionTag="-1766899571" Tag="73" IconVisible="False" LeftMargin="29.6359" RightMargin="116.3641" TopMargin="223.8666" BottomMargin="127.1334" FontSize="20" LabelText="小血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6359" Y="138.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.3707" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HNFlag" ActionTag="-1370410682" Tag="74" IconVisible="False" LeftMargin="75.5000" RightMargin="84.5000" TopMargin="249.4778" BottomMargin="90.5222" ctype="SpriteObjectData">
                <Size X="86.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="118.5000" Y="107.5222" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4817" Y="0.2875" />
                <PreSize X="0.3496" Y="0.0909" />
                <FileData Type="Normal" Path="game/bt_depart_2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HuangShangCountTxt" ActionTag="-1986667409" Tag="75" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.6484" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NiangNiangCountTxt" ActionTag="1309334754" Tag="76" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="154.6117" BottomMargin="196.3883" FontSize="20" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="207.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.5559" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigXueCountTxt" ActionTag="478691638" Tag="77" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="189.2392" BottomMargin="161.7608" FontSize="20" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="173.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.4633" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigXueCountTxt" ActionTag="1335769274" Tag="78" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="223.8666" BottomMargin="127.1334" FontSize="20" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="138.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.3707" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleTotalScore" ActionTag="-1997375152" Tag="79" IconVisible="False" LeftMargin="91.4989" RightMargin="94.5011" TopMargin="292.5727" BottomMargin="58.4273" FontSize="20" LabelText="总结算" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="69.9273" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.1870" />
                <PreSize X="0.2439" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleTotalScore_0" ActionTag="1448666327" Tag="80" IconVisible="False" LeftMargin="115.4989" RightMargin="118.5011" TopMargin="328.5436" BottomMargin="22.4563" FontSize="20" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="33.9563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.0908" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="pri_end_res/gameend_account_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>