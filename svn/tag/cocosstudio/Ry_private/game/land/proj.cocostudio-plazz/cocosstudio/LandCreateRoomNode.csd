<GameFile>
  <PropertyGroup Name="LandCreateRoomNode" Type="Node" ID="83f612f0-b3f1-4590-89a2-34fff6e498d4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1508463358" Tag="2" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="Mask" ActionTag="1889891201" Tag="26" IconVisible="False" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1334.0000" Y="750.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Bg" ActionTag="-16240816" Tag="4" IconVisible="False" LeftMargin="171.0000" RightMargin="171.0000" TopMargin="75.5000" BottomMargin="75.5000" ctype="SpriteObjectData">
                <Size X="992.0000" Y="599.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7436" Y="0.7987" />
                <FileData Type="Normal" Path="landpri_pic/wx_cr_popupbg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Title" ActionTag="452882968" Tag="15" IconVisible="False" LeftMargin="446.4548" RightMargin="445.5452" TopMargin="64.7723" BottomMargin="622.2277" ctype="SpriteObjectData">
                <Size X="442.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.4548" Y="653.7277" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5003" Y="0.8716" />
                <PreSize X="0.3313" Y="0.0840" />
                <FileData Type="Normal" Path="landpri_pic/wx_cr_title.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnBack" ActionTag="-446421336" Tag="3" IconVisible="False" LeftMargin="1114.8088" RightMargin="171.1912" TopMargin="75.8254" BottomMargin="625.1746" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="18" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="48.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1138.8088" Y="649.6746" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8537" Y="0.8662" />
                <PreSize X="0.0360" Y="0.0653" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="landpri_pic/wx_cr_closebtn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="landpri_pic/wx_cr_closebtn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="landpri_pic/wx_cr_closebtn_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_1" ActionTag="1668979866" Tag="5" IconVisible="False" LeftMargin="263.5549" RightMargin="992.4451" TopMargin="197.6938" BottomMargin="521.3062" FontSize="26" LabelText="人数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="302.5549" Y="536.8062" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.2268" Y="0.7157" />
                <PreSize X="0.0585" Y="0.0413" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_2" ActionTag="686063349" Tag="6" IconVisible="False" LeftMargin="263.5547" RightMargin="992.4453" TopMargin="481.3340" BottomMargin="237.6660" FontSize="26" LabelText="局数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="302.5547" Y="253.1660" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.2268" Y="0.3376" />
                <PreSize X="0.0585" Y="0.0413" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_People" ActionTag="1662439886" Tag="8" IconVisible="False" LeftMargin="362.8027" RightMargin="911.1973" TopMargin="181.1391" BottomMargin="507.8609" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1582556588" Tag="7" IconVisible="False" LeftMargin="70.0000" RightMargin="-70.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="三人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.0000" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="392.8027" Y="538.3609" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2945" Y="0.7178" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_Ju_Shu_1" ActionTag="-1797953996" Tag="9" IconVisible="False" LeftMargin="362.8025" RightMargin="911.1975" TopMargin="466.3341" BottomMargin="222.6659" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1185050416" Tag="10" IconVisible="False" LeftMargin="70.0000" RightMargin="-74.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="10局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="64.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.0667" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="costbg" ActionTag="1160338751" Tag="21" IconVisible="False" LeftMargin="135.0000" RightMargin="-175.0000" TopMargin="16.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="185.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="3.0833" Y="0.4918" />
                    <PreSize X="1.6667" Y="0.4918" />
                    <FileData Type="Normal" Path="landpri_pic/wx_cr_costroombg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="CostNum" ActionTag="1381990326" Tag="22" IconVisible="False" LeftMargin="154.0000" RightMargin="-156.0000" TopMargin="19.5000" BottomMargin="18.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="62.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="185.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="3.0833" Y="0.4918" />
                    <PreSize X="1.0333" Y="0.3770" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="392.8025" Y="253.1659" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2945" Y="0.3376" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_Ju_Shu_2" ActionTag="188253517" Tag="11" IconVisible="False" LeftMargin="613.0521" RightMargin="660.9479" TopMargin="466.3341" BottomMargin="222.6659" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-1989900542" Tag="12" IconVisible="False" LeftMargin="70.0000" RightMargin="-74.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="10局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="64.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.0667" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Costbg" ActionTag="883011044" Tag="23" IconVisible="False" LeftMargin="135.0000" RightMargin="-175.0000" TopMargin="16.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="185.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="3.0833" Y="0.4918" />
                    <PreSize X="1.6667" Y="0.4918" />
                    <FileData Type="Normal" Path="landpri_pic/wx_cr_costroombg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="CostNum" ActionTag="156941660" Tag="24" IconVisible="False" LeftMargin="154.0000" RightMargin="-156.0000" TopMargin="19.5000" BottomMargin="18.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="62.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="185.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="3.0833" Y="0.4918" />
                    <PreSize X="1.0333" Y="0.3770" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5128" ScaleY="0.3634" />
                <Position X="643.8201" Y="244.8333" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4826" Y="0.3264" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_Ju_Shu_3" ActionTag="167433815" Tag="13" IconVisible="False" LeftMargin="863.2990" RightMargin="410.7010" TopMargin="466.3341" BottomMargin="222.6659" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-188363630" Tag="14" IconVisible="False" LeftMargin="70.0000" RightMargin="-74.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="10局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="64.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.0667" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Costbg" ActionTag="-128807639" Tag="25" IconVisible="False" LeftMargin="135.0000" RightMargin="-175.0000" TopMargin="16.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="185.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="3.0833" Y="0.4918" />
                    <PreSize X="1.6667" Y="0.4918" />
                    <FileData Type="Normal" Path="landpri_pic/wx_cr_costroombg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="CostNum" ActionTag="-369781713" Tag="26" IconVisible="False" LeftMargin="154.0000" RightMargin="-156.0000" TopMargin="19.5000" BottomMargin="18.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="62.0000" Y="23.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="185.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="3.0833" Y="0.4918" />
                    <PreSize X="1.0333" Y="0.3770" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="893.2990" Y="253.1659" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6696" Y="0.3376" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CreateBtn" ActionTag="-1254160233" Tag="16" IconVisible="False" LeftMargin="917.8715" RightMargin="198.1285" TopMargin="574.9813" BottomMargin="95.0187" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="188" Scale9Height="58" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="218.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1026.8715" Y="135.0187" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7698" Y="0.1800" />
                <PreSize X="0.1634" Y="0.1067" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="landpri_pic/wx_cr_createtbn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="landpri_pic/wx_cr_createtbn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="landpri_pic/wx_cr_createtbn_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CostBg" ActionTag="1080621022" Tag="20" IconVisible="False" LeftMargin="219.1110" RightMargin="906.8890" TopMargin="587.7449" BottomMargin="112.2551" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="82" RightEage="82" TopEage="16" BottomEage="16" Scale9OriginX="82" Scale9OriginY="16" Scale9Width="86" Scale9Height="18" ctype="PanelObjectData">
                <Size X="208.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="FK_sp" ActionTag="-33740274" Tag="19" RotationSkewX="-32.0000" RotationSkewY="-32.0000" IconVisible="False" LeftMargin="-2.6567" RightMargin="155.6567" TopMargin="3.0009" BottomMargin="8.9991" ctype="SpriteObjectData">
                    <Size X="55.0000" Y="38.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="24.8433" Y="27.9991" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1194" Y="0.5600" />
                    <PreSize X="0.2644" Y="0.7600" />
                    <FileData Type="Normal" Path="landpri_pic/sp_roomCardTop.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="RoomCard" ActionTag="-1945818398" Tag="18" IconVisible="False" LeftMargin="129.1181" RightMargin="12.8819" TopMargin="4.1152" BottomMargin="1.8848" FontSize="36" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="66.0000" Y="44.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="162.1181" Y="23.8848" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7794" Y="0.4777" />
                    <PreSize X="0.3173" Y="0.8800" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="77" G="77" B="77" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="323.1110" Y="137.2551" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2422" Y="0.1830" />
                <PreSize X="0.1559" Y="0.0667" />
                <FileData Type="Normal" Path="landpri_pic/wx_cr_roomcardbg.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_3" ActionTag="-1764446555" Tag="27" IconVisible="False" LeftMargin="263.5549" RightMargin="992.4451" TopMargin="326.1805" BottomMargin="392.8195" FontSize="26" LabelText="规则：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="302.5549" Y="408.3195" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.2268" Y="0.5444" />
                <PreSize X="0.0585" Y="0.0413" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_Rule" ActionTag="931235922" Tag="28" IconVisible="False" LeftMargin="613.0507" RightMargin="660.9493" TopMargin="311.1805" BottomMargin="377.8195" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="279746304" Tag="29" IconVisible="False" LeftMargin="70.0000" RightMargin="-314.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="双王或四个2必须叫3分" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="304.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="5.0667" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="643.0507" Y="408.3195" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.5444" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_4" ActionTag="-334259039" Tag="30" IconVisible="False" LeftMargin="211.5549" RightMargin="992.4451" TopMargin="403.7572" BottomMargin="315.2428" FontSize="26" LabelText="最大番数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="276.5549" Y="330.7428" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.2073" Y="0.4410" />
                <PreSize X="0.0975" Y="0.0413" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_MaxFan_1" ActionTag="-2021734499" Tag="31" IconVisible="False" LeftMargin="362.8025" RightMargin="911.1975" TopMargin="391.2573" BottomMargin="297.7427" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-761991229" Tag="32" IconVisible="False" LeftMargin="70.0000" RightMargin="-117.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="3炸封顶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="107.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.7833" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="392.8025" Y="328.2427" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2945" Y="0.4377" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_MaxFan_2" ActionTag="-624032574" Tag="33" IconVisible="False" LeftMargin="613.0507" RightMargin="660.9493" TopMargin="391.2573" BottomMargin="297.7427" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1314797348" Tag="34" IconVisible="False" LeftMargin="70.0000" RightMargin="-117.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="4炸封顶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="107.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.7833" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="643.0507" Y="328.2427" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.4377" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_MaxFan_3" ActionTag="1298391434" Tag="35" IconVisible="False" LeftMargin="863.2991" RightMargin="410.7009" TopMargin="391.2573" BottomMargin="297.7427" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1233933442" Tag="36" IconVisible="False" LeftMargin="70.0000" RightMargin="-117.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="5炸封顶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="107.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="1.7833" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="893.2991" Y="328.2427" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6696" Y="0.4377" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="CB_Rule_Double" ActionTag="-167889758" Tag="40" IconVisible="False" LeftMargin="362.8025" RightMargin="911.1975" TopMargin="311.1805" BottomMargin="377.8195" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="60.0000" Y="61.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="2046007601" Tag="41" IconVisible="False" LeftMargin="70.0000" RightMargin="-160.0000" TopMargin="14.0000" BottomMargin="13.0000" FontSize="30" LabelText="农民可加倍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="70.0000" Y="30.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="1.1667" Y="0.4918" />
                    <PreSize X="2.5000" Y="0.5574" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="392.8025" Y="408.3195" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2945" Y="0.5444" />
                <PreSize X="0.0450" Y="0.0813" />
                <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>