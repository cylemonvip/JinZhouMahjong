<GameFile>
  <PropertyGroup Name="SettingLayer" Type="Layer" ID="61b6fb7e-25f2-475c-870f-acbeae5a2fd4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="60" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="setting_bg" ActionTag="2078718125" Tag="61" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="303.5000" RightMargin="303.5000" TopMargin="134.0000" BottomMargin="134.0000" ctype="SpriteObjectData">
            <Size X="727.0000" Y="482.0000" />
            <Children>
              <AbstractNodeData Name="close_btn" ActionTag="-115474623" Tag="79" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="662.4067" RightMargin="19.5933" TopMargin="16.1384" BottomMargin="419.8616" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="11" BottomEage="11" Scale9OriginX="14" Scale9OriginY="11" Scale9Width="17" Scale9Height="24" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="45.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="684.9067" Y="442.8616" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9421" Y="0.9188" />
                <PreSize X="0.0619" Y="0.0954" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="bt_close_1.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="bt_close_0.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="bt_close_1.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_setting_effect_1" ActionTag="-1457265002" Tag="140" IconVisible="False" LeftMargin="141.0000" RightMargin="408.0000" TopMargin="121.0008" BottomMargin="308.9992" ctype="SpriteObjectData">
                <Size X="178.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="230.0000" Y="334.9992" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3164" Y="0.6950" />
                <PreSize X="0.2448" Y="0.1079" />
                <FileData Type="PlistSubImage" Path="sp_setting_bg.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_setting_effect_1_0" ActionTag="2132533981" Tag="141" IconVisible="False" LeftMargin="141.0000" RightMargin="408.0000" TopMargin="231.0000" BottomMargin="199.0000" ctype="SpriteObjectData">
                <Size X="178.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="230.0000" Y="225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3164" Y="0.4668" />
                <PreSize X="0.2448" Y="0.1079" />
                <FileData Type="PlistSubImage" Path="sp_setting_effect.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="check_bg" ActionTag="1744459741" Tag="142" IconVisible="False" LeftMargin="395.0000" RightMargin="142.0000" TopMargin="104.0000" BottomMargin="292.0000" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="86.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="490.0000" Y="335.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6740" Y="0.6950" />
                <PreSize X="0.2613" Y="0.1784" />
                <NormalBackFileData Type="PlistSubImage" Path="check_setting_close.png" Plist="game/game.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="check_setting_close.png" Plist="game/game.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="check_setting_close.png" Plist="game/game.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="check_setting_open.png" Plist="game/game.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="check_setting_open.png" Plist="game/game.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="check_effect" ActionTag="584912049" Tag="143" IconVisible="False" LeftMargin="395.0000" RightMargin="142.0000" TopMargin="214.0000" BottomMargin="182.0000" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="86.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="490.0000" Y="225.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6740" Y="0.4668" />
                <PreSize X="0.2613" Y="0.1784" />
                <NormalBackFileData Type="PlistSubImage" Path="check_setting_close.png" Plist="game/game.plist" />
                <PressedBackFileData Type="PlistSubImage" Path="check_setting_close.png" Plist="game/game.plist" />
                <DisableBackFileData Type="PlistSubImage" Path="check_setting_close.png" Plist="game/game.plist" />
                <NodeNormalFileData Type="PlistSubImage" Path="check_setting_open.png" Plist="game/game.plist" />
                <NodeDisableFileData Type="PlistSubImage" Path="check_setting_open.png" Plist="game/game.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5450" Y="0.6427" />
            <FileData Type="PlistSubImage" Path="setting_bg.png" Plist="game/game.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>