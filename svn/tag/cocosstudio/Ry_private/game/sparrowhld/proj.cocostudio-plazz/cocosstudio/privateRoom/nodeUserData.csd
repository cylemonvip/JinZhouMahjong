<GameFile>
  <PropertyGroup Name="nodeUserData" Type="Node" ID="1ccb59f9-f3b4-46a4-96d9-f6db2eb04f2e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="71" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="326001193" Tag="322" IconVisible="False" LeftMargin="-157.5000" RightMargin="-157.5000" TopMargin="-260.0000" BottomMargin="-260.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="64" RightEage="64" TopEage="110" BottomEage="110" Scale9OriginX="64" Scale9OriginY="110" Scale9Width="66" Scale9Height="115" ctype="PanelObjectData">
            <Size X="315.0000" Y="520.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="plist/cover2.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="HeadNode" ActionTag="603900632" Tag="612" IconVisible="True" LeftMargin="-83.0230" RightMargin="83.0230" TopMargin="-147.7859" BottomMargin="147.7859" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-83.0230" Y="147.7859" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="HeadImg" ActionTag="1490348207" Tag="253" IconVisible="False" LeftMargin="-118.0011" RightMargin="48.0011" TopMargin="-182.7456" BottomMargin="112.7456" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
            <Size X="70.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-83.0011" Y="147.7456" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="plist/headbg_kk.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_account" ActionTag="-1674625780" Tag="209" IconVisible="False" LeftMargin="-46.2823" RightMargin="-73.7177" TopMargin="-164.8848" BottomMargin="130.8848" FontSize="30" LabelText="测试账号" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="34.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-46.2823" Y="147.8848" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_id" ActionTag="598437441" Tag="90" IconVisible="False" LeftMargin="-127.9990" RightMargin="-93.0010" TopMargin="-97.2188" BottomMargin="63.2188" FontSize="30" LabelText="玩家ID：263598" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="221.0000" Y="34.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-127.9990" Y="80.2188" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="247" G="247" B="158" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="77" G="77" B="77" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_hu" ActionTag="-551613911" Tag="89" IconVisible="False" LeftMargin="-127.9990" RightMargin="-22.0010" TopMargin="-46.4146" BottomMargin="12.4146" FontSize="30" LabelText="胡牌次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9990" Y="29.4146" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_huNum" ActionTag="1725115941" Tag="92" IconVisible="False" LeftMargin="34.5014" RightMargin="-51.5014" TopMargin="-46.4146" BottomMargin="12.4146" FontSize="30" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="17.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0014" Y="29.4146" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gongGang" ActionTag="-1625703066" Tag="86" IconVisible="False" LeftMargin="-127.9990" RightMargin="-22.0010" TopMargin="7.3898" BottomMargin="-41.3898" FontSize="30" LabelText="明杠次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9990" Y="-24.3898" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gongGangNum" ActionTag="-840589994" Tag="93" IconVisible="False" LeftMargin="34.5016" RightMargin="-51.5016" TopMargin="7.3898" BottomMargin="-41.3898" FontSize="30" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="17.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0016" Y="-24.3898" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_anGang" ActionTag="1646956206" Tag="87" IconVisible="False" LeftMargin="-127.9990" RightMargin="-22.0010" TopMargin="61.1941" BottomMargin="-95.1941" FontSize="30" LabelText="暗杠次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9990" Y="-78.1941" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_anGangNum" ActionTag="-1032092274" Tag="94" IconVisible="False" LeftMargin="34.5021" RightMargin="-51.5021" TopMargin="61.1941" BottomMargin="-95.1941" FontSize="30" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="17.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0021" Y="-78.1941" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_ma" ActionTag="-68209943" Tag="88" IconVisible="False" LeftMargin="-127.9990" RightMargin="-22.0010" TopMargin="114.9981" BottomMargin="-148.9981" FontSize="30" LabelText="点炮次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9990" Y="-131.9981" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_maNum" ActionTag="-1087103918" Tag="95" IconVisible="False" LeftMargin="17.5006" RightMargin="-68.5006" TopMargin="114.9981" BottomMargin="-148.9981" FontSize="30" LabelText="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="51.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0006" Y="-131.9981" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gradeTotal" ActionTag="-1726386754" Tag="72" IconVisible="False" LeftMargin="-104.9990" RightMargin="44.9990" TopMargin="189.3798" BottomMargin="-223.3798" FontSize="30" LabelText="战绩" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-74.9990" Y="-206.3798" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gradeTotalNum" ActionTag="-251055276" Tag="91" IconVisible="False" LeftMargin="-19.0927" RightMargin="-65.9073" TopMargin="189.3798" BottomMargin="-223.3798" FontSize="30" LabelText="+1002" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="85.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="23.4073" Y="-206.3798" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_roomHost" ActionTag="512729120" VisibleForFrame="False" Tag="211" IconVisible="False" LeftMargin="74.2653" RightMargin="-147.2653" TopMargin="-245.1964" BottomMargin="212.1964" ctype="SpriteObjectData">
            <Size X="73.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="110.7653" Y="228.6964" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="gameend_account_bank.png" Plist="plist/plazaScene.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_bigWinner" ActionTag="-1061795470" VisibleForFrame="False" Tag="213" IconVisible="False" LeftMargin="-123.5999" RightMargin="31.5999" TopMargin="-245.9776" BottomMargin="193.9776" ctype="SpriteObjectData">
            <Size X="92.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-77.5999" Y="219.9776" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="plist/bigWin.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="zjps" ActionTag="-745835588" VisibleForFrame="False" Tag="1907" IconVisible="False" LeftMargin="93.0745" RightMargin="-150.0745" TopMargin="160.7674" BottomMargin="-219.7674" ctype="SpriteObjectData">
            <Size X="57.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="121.5745" Y="-190.2674" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="plist/best.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>