<GameFile>
  <PropertyGroup Name="CreateRoomResult" Type="Layer" ID="66121fbc-f7f3-452d-b18c-b5f990415394" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="18" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="panel_mask" ActionTag="-1795147967" Tag="32" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_bg" ActionTag="-485898702" Tag="22" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="238.0000" RightMargin="238.0000" TopMargin="145.2250" BottomMargin="135.7750" TouchEnable="True" LeftEage="143" RightEage="143" TopEage="81" BottomEage="81" Scale9OriginX="143" Scale9OriginY="81" Scale9Width="148" Scale9Height="84" ctype="ImageViewObjectData">
            <Size X="858.0000" Y="469.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_2" ActionTag="-1378335273" Tag="77" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="289.5000" RightMargin="289.5000" TopMargin="-18.5392" BottomMargin="424.5392" ctype="SpriteObjectData">
                <Size X="279.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="429.0000" Y="456.0392" />
                <Scale ScaleX="1.1700" ScaleY="1.1700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9724" />
                <PreSize X="0.3252" Y="0.1343" />
                <FileData Type="Normal" Path="systips.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_tips" ActionTag="-1811399833" Tag="33" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="382.5000" RightMargin="382.5000" TopMargin="181.3878" BottomMargin="250.6122" FontSize="30" LabelText="房间号" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="93.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="429.0000" Y="269.1122" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5738" />
                <PreSize X="0.1084" Y="0.0789" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_join" ActionTag="275932609" Tag="24" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="170.9800" RightMargin="497.0200" TopMargin="348.0019" BottomMargin="56.9981" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="265.9800" Y="88.9981" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3100" Y="0.1898" />
                <PreSize X="0.2214" Y="0.1365" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="pri_btn_joingame_0.png" Plist="room/room.plist" />
                <PressedFileData Type="PlistSubImage" Path="pri_btn_joingame_1.png" Plist="room/room.plist" />
                <NormalFileData Type="PlistSubImage" Path="pri_btn_joingame_0.png" Plist="room/room.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_invite" ActionTag="-1527452884" Tag="25" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="497.0200" RightMargin="170.9800" TopMargin="348.0019" BottomMargin="56.9981" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="592.0200" Y="88.9981" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6900" Y="0.1898" />
                <PreSize X="0.2214" Y="0.1365" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="pri_btn_invite_0.png" Plist="room/room.plist" />
                <PressedFileData Type="PlistSubImage" Path="pri_btn_invite_1.png" Plist="room/room.plist" />
                <NormalFileData Type="PlistSubImage" Path="pri_btn_invite_0.png" Plist="room/room.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="370.2750" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4937" />
            <PreSize X="0.6432" Y="0.6253" />
            <FileData Type="Normal" Path="KFMJ_grxx_bg.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>