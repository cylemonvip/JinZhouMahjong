<GameFile>
  <PropertyGroup Name="InviteRewardNode" Type="Node" ID="90685e95-d2d3-4b63-b62f-3ed886b256ec" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="57" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-416128784" Tag="58" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="-1432259882" Tag="59" IconVisible="False" LeftMargin="-367.0000" RightMargin="-367.0000" TopMargin="-199.5000" BottomMargin="-199.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="242" RightEage="242" TopEage="131" BottomEage="131" Scale9OriginX="242" Scale9OriginY="131" Scale9Width="250" Scale9Height="137" ctype="PanelObjectData">
            <Size X="734.0000" Y="399.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_2" ActionTag="83337895" Tag="61" IconVisible="False" LeftMargin="196.0000" RightMargin="190.0000" TopMargin="-42.0000" BottomMargin="377.0000" ctype="SpriteObjectData">
                <Size X="348.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="370.0000" Y="409.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5041" Y="1.0251" />
                <PreSize X="0.4741" Y="0.1604" />
                <FileData Type="PlistSubImage" Path="plaza_image_title_bg.png" Plist="room/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="-225262281" Tag="60" IconVisible="False" LeftMargin="37.3375" RightMargin="297.6625" TopMargin="73.9826" BottomMargin="97.0174" ctype="SpriteObjectData">
                <Size X="399.0000" Y="228.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="236.8375" Y="211.0174" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3227" Y="0.5289" />
                <PreSize X="0.5436" Y="0.5714" />
                <FileData Type="Normal" Path="room/invitedpic/invited_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3" ActionTag="1349889118" Tag="62" IconVisible="False" LeftMargin="66.1472" RightMargin="355.8528" TopMargin="50.0073" BottomMargin="330.9927" ctype="SpriteObjectData">
                <Size X="312.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="222.1472" Y="339.9927" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3027" Y="0.8521" />
                <PreSize X="0.4251" Y="0.0451" />
                <FileData Type="Normal" Path="room/invitedpic/invited_2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_4" ActionTag="-1119129269" VisibleForFrame="False" Tag="63" IconVisible="False" LeftMargin="457.9027" RightMargin="39.0973" TopMargin="258.5368" BottomMargin="28.4632" ctype="SpriteObjectData">
                <Size X="237.0000" Y="112.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="576.4027" Y="84.4632" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7853" Y="0.2117" />
                <PreSize X="0.3229" Y="0.2807" />
                <FileData Type="PlistSubImage" Path="getcard_text_share.png" Plist="room/vip.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="DescTxt" ActionTag="1654275667" Tag="21" IconVisible="False" LeftMargin="448.0591" RightMargin="23.9409" TopMargin="86.5959" BottomMargin="103.4041" IsCustomSize="True" FontSize="20" LabelText="1.点击“分享”，分享给好友下载安装即可绑定！&#xA;2.被推荐的玩家完成2圈房卡麻将后，推荐玩家可获得2元红包奖励！（A邀请B，B邀请C和D，完成2圈游戏，A得2元，B得4元红包）。满15元可以提现!" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="262.0000" Y="209.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="579.0591" Y="312.4041" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7889" Y="0.7830" />
                <PreSize X="0.3569" Y="0.5238" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="77" G="77" B="77" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_5" ActionTag="-650104199" Tag="64" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="349.5285" RightMargin="346.4715" TopMargin="-78.9116" BottomMargin="329.9116" ctype="SpriteObjectData">
                <Size X="38.0000" Y="148.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="368.5285" Y="403.9116" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5021" Y="1.0123" />
                <PreSize X="0.0518" Y="0.3709" />
                <FileData Type="Normal" Path="room/invitedpic/invited_5.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_7" ActionTag="-519126434" Tag="66" IconVisible="False" LeftMargin="100.7328" RightMargin="487.2672" TopMargin="318.1382" BottomMargin="37.8618" ctype="SpriteObjectData">
                <Size X="146.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="173.7328" Y="59.3618" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2367" Y="0.1488" />
                <PreSize X="0.1989" Y="0.1078" />
                <FileData Type="Normal" Path="room/invitedpic/invited_12.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnGet" ActionTag="-476632851" Tag="68" IconVisible="False" LeftMargin="275.6697" RightMargin="324.3303" TopMargin="309.6382" BottomMargin="29.3618" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="104" Scale9Height="38" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="134.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="342.6697" Y="59.3618" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4669" Y="0.1488" />
                <PreSize X="0.1826" Y="0.1504" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="getcard_btn_get_unused.png" Plist="room/vip.plist" />
                <PressedFileData Type="PlistSubImage" Path="getcard_btn_get_press.png" Plist="room/vip.plist" />
                <NormalFileData Type="PlistSubImage" Path="getcard_btn_get_normal.png" Plist="room/vip.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnShare" ActionTag="-575403421" Tag="69" IconVisible="False" LeftMargin="513.5591" RightMargin="89.4409" TopMargin="317.6382" BottomMargin="37.3618" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="101" Scale9Height="22" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="131.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="579.0591" Y="59.3618" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7889" Y="0.1488" />
                <PreSize X="0.1785" Y="0.1103" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="getcard_btn_share_press.png" Plist="room/vip.plist" />
                <PressedFileData Type="PlistSubImage" Path="getcard_btn_share_press.png" Plist="room/vip.plist" />
                <NormalFileData Type="PlistSubImage" Path="getcard_btn_share_normal.png" Plist="room/vip.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="-1274751759" Tag="21" IconVisible="False" LeftMargin="682.1501" RightMargin="-16.1501" TopMargin="-25.4629" BottomMargin="356.4629" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="716.1501" Y="390.4629" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9757" Y="0.9786" />
                <PreSize X="0.0926" Y="0.1704" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="plaza_btn_close_normal.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="InvitedCountTxt" ActionTag="-366555314" Tag="13" IconVisible="False" LeftMargin="248.5163" RightMargin="469.4837" TopMargin="46.0955" BottomMargin="326.9045" FontSize="20" LabelText="0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="16.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="256.5163" Y="339.9045" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.3495" Y="0.8519" />
                <PreSize X="0.0218" Y="0.0652" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="FinishedCountTxt" ActionTag="915032025" Tag="14" IconVisible="False" LeftMargin="382.6472" RightMargin="333.3528" TopMargin="42.5952" BottomMargin="325.4048" FontSize="25" LabelText="0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="18.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="391.6472" Y="340.9048" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.5336" Y="0.8544" />
                <PreSize X="0.0245" Y="0.0777" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RewardTxt" ActionTag="432535265" Tag="19" IconVisible="False" LeftMargin="160.1413" RightMargin="555.8587" TopMargin="324.1382" BottomMargin="43.8618" FontSize="25" LabelText="0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="18.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="169.1413" Y="59.3618" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.2304" Y="0.1488" />
                <PreSize X="0.0245" Y="0.0777" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Contenter" ActionTag="710319909" Tag="20" IconVisible="False" LeftMargin="41.5001" RightMargin="302.4999" TopMargin="122.1868" BottomMargin="104.8132" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="390.0000" Y="172.0000" />
                <AnchorPoint />
                <Position X="41.5001" Y="104.8132" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0565" Y="0.2627" />
                <PreSize X="0.5313" Y="0.4311" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.1700" ScaleY="1.1700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="plaza_common_bg.png" Plist="room/plazaScene.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Item" ActionTag="1533709008" Tag="15" IconVisible="False" LeftMargin="-197.9048" RightMargin="-201.0952" TopMargin="436.2681" BottomMargin="-506.2681" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="128" RightEage="128" TopEage="21" BottomEage="21" Scale9OriginX="128" Scale9OriginY="21" Scale9Width="134" Scale9Height="24" ctype="PanelObjectData">
            <Size X="399.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="PlayCountTxt" ActionTag="1432139663" Tag="17" IconVisible="False" LeftMargin="278.7976" RightMargin="91.2024" TopMargin="21.0000" BottomMargin="21.0000" FontSize="22" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="293.2976" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.7351" Y="0.5000" />
                <PreSize X="0.0727" Y="0.4000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="407027856" Tag="16" IconVisible="False" LeftMargin="7.5000" RightMargin="216.5000" TopMargin="16.5000" BottomMargin="16.5000" IsCustomSize="True" FontSize="22" LabelText="香水" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="175.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="95.0000" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.2381" Y="0.5000" />
                <PreSize X="0.4386" Y="0.5286" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-197.9048" Y="-506.2681" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="getcard_item_bg.png" Plist="room/vip.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>