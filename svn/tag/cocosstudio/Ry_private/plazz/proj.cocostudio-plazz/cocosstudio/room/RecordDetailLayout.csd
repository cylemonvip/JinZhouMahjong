<GameFile>
  <PropertyGroup Name="RecordDetailLayout" Type="Node" ID="d0aa2f65-9114-466a-85bf-068a4f40008f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="49" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-11559639" Tag="53" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="-1406309269" Tag="50" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="249" RightEage="249" TopEage="148" BottomEage="148" Scale9OriginX="249" Scale9OriginY="148" Scale9Width="836" Scale9Height="454" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="NameText1" ActionTag="275486224" Tag="33" IconVisible="False" LeftMargin="376.4500" RightMargin="774.5500" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三拉" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="183.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="376.4500" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2822" Y="0.7989" />
                <PreSize X="0.1372" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText2" ActionTag="1836024632" Tag="34" IconVisible="False" LeftMargin="559.9254" RightMargin="591.0746" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="183.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="559.9254" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4197" Y="0.7989" />
                <PreSize X="0.1372" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText3" ActionTag="-1290192448" Tag="35" IconVisible="False" LeftMargin="744.4007" RightMargin="406.5993" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="183.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="744.4007" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5580" Y="0.7989" />
                <PreSize X="0.1372" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText4" ActionTag="1159738768" Tag="36" IconVisible="False" LeftMargin="928.3760" RightMargin="239.6240" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三打" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="166.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="928.3760" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6959" Y="0.7989" />
                <PreSize X="0.1244" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnBack" ActionTag="-918638689" Tag="17" IconVisible="False" LeftMargin="23.0067" RightMargin="1252.9933" TopMargin="21.8690" BottomMargin="669.1310" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="37" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="58.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0067" Y="698.6310" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0390" Y="0.9315" />
                <PreSize X="0.0435" Y="0.0787" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/btn_back_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/btn_back_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/btn_back_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="room/recorddetail/recordbg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayout" ActionTag="-426199384" Tag="57" IconVisible="False" LeftMargin="-570.2499" RightMargin="-579.7501" TopMargin="500.2107" BottomMargin="-620.2107" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="381" RightEage="381" TopEage="37" BottomEage="37" Scale9OriginX="381" Scale9OriginY="37" Scale9Width="394" Scale9Height="41" ctype="PanelObjectData">
            <Size X="1150.0000" Y="120.0000" />
            <Children>
              <AbstractNodeData Name="FangIDText" ActionTag="2016835688" Tag="58" IconVisible="False" LeftMargin="51.2274" RightMargin="1075.7726" TopMargin="15.0114" BottomMargin="31.9886" FontSize="60" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="23.0000" Y="73.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.7274" Y="68.4886" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0545" Y="0.5707" />
                <PreSize X="0.0200" Y="0.6083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TimeText" ActionTag="-1058833150" Tag="60" IconVisible="False" LeftMargin="114.3290" RightMargin="890.6710" TopMargin="11.0115" BottomMargin="27.9885" IsCustomSize="True" FontSize="22" LabelText="2017-08-29&#xA;18：16：09" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="145.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="186.8290" Y="68.4885" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1625" Y="0.5707" />
                <PreSize X="0.1261" Y="0.6750" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score1" ActionTag="-1500048984" Tag="59" IconVisible="False" LeftMargin="340.5993" RightMargin="749.4007" TopMargin="21.0126" BottomMargin="37.9874" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="370.5993" Y="68.4874" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3223" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score2" ActionTag="-1652902996" Tag="37" IconVisible="False" LeftMargin="528.5731" RightMargin="561.4269" TopMargin="21.0128" BottomMargin="37.9872" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="558.5731" Y="68.4872" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4857" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score3" ActionTag="-1324174194" Tag="38" IconVisible="False" LeftMargin="711.5496" RightMargin="378.4504" TopMargin="21.0126" BottomMargin="37.9874" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="741.5496" Y="68.4874" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6448" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score4" ActionTag="-852027379" Tag="39" IconVisible="False" LeftMargin="887.5254" RightMargin="202.4746" TopMargin="21.0120" BottomMargin="37.9880" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="917.5254" Y="68.4880" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7978" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnReplay" ActionTag="1355914935" Tag="40" IconVisible="False" LeftMargin="984.0503" RightMargin="6.9497" TopMargin="0.6503" BottomMargin="62.3497" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="129" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="159.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1063.5503" Y="90.8497" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9248" Y="0.7571" />
                <PreSize X="0.1383" Y="0.4750" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/recorddetail/btnplayback_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/recorddetail/btnplayback_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/recorddetail/btnplayback_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnShare" ActionTag="638490480" Tag="35" IconVisible="False" LeftMargin="985.5504" RightMargin="8.4496" TopMargin="49.4531" BottomMargin="13.5469" TouchEnable="True" FontSize="44" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="126" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="156.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1063.5504" Y="42.0469" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9248" Y="0.3504" />
                <PreSize X="0.1357" Y="0.4750" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/recorddetail/share_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/recorddetail/share_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/recorddetail/share.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-570.2499" Y="-620.2107" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="room/recorddetail/rcditembg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ReplayInfoLayout" ActionTag="1453193131" Tag="78" IconVisible="False" LeftMargin="118.7892" RightMargin="-668.7892" TopMargin="-365.1618" BottomMargin="285.1618" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="550.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-596237251" Tag="82" IconVisible="False" LeftMargin="150.9361" RightMargin="319.0639" TopMargin="4.9133" BottomMargin="45.0867" Scale9Enable="True" LeftEage="128" RightEage="128" TopEage="21" BottomEage="21" Scale9OriginX="128" Scale9OriginY="21" Scale9Width="134" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="150.9361" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2744" Y="0.7511" />
                <PreSize X="0.1455" Y="0.3750" />
                <FileData Type="PlistSubImage" Path="getcard_item_bg.png" Plist="room/vip.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_24" ActionTag="-839285826" Tag="80" IconVisible="False" LeftMargin="80.6037" RightMargin="403.3963" TopMargin="8.4133" BottomMargin="48.5867" FontSize="20" LabelText="房卡号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.6037" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.2066" Y="0.7511" />
                <PreSize X="0.1200" Y="0.2875" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TFFangka" ActionTag="346224773" Tag="79" IconVisible="False" LeftMargin="150.5180" RightMargin="319.4820" TopMargin="4.9133" BottomMargin="45.0867" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="150.5180" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.2737" Y="0.7511" />
                <PreSize X="0.1455" Y="0.3750" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1_0" ActionTag="1732853645" Tag="83" IconVisible="False" LeftMargin="307.1421" RightMargin="162.8579" TopMargin="4.9133" BottomMargin="45.0867" Scale9Enable="True" LeftEage="128" RightEage="128" TopEage="21" BottomEage="21" Scale9OriginX="128" Scale9OriginY="21" Scale9Width="134" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="307.1421" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5584" Y="0.7511" />
                <PreSize X="0.1455" Y="0.3750" />
                <FileData Type="PlistSubImage" Path="getcard_item_bg.png" Plist="room/vip.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_24_0" ActionTag="-1165223709" Tag="84" IconVisible="False" LeftMargin="258.7562" RightMargin="245.2438" TopMargin="8.4133" BottomMargin="48.5867" FontSize="20" LabelText="标记:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="46.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="281.7562" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.5123" Y="0.7511" />
                <PreSize X="0.0836" Y="0.2875" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TFServerFlag" ActionTag="1745120160" Tag="85" IconVisible="False" LeftMargin="307.7239" RightMargin="162.2761" TopMargin="4.9133" BottomMargin="45.0867" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="307.7239" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.5595" Y="0.7511" />
                <PreSize X="0.1455" Y="0.3750" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1_0_0" ActionTag="153081634" Tag="86" IconVisible="False" LeftMargin="449.3029" RightMargin="20.6971" TopMargin="4.9133" BottomMargin="45.0867" Scale9Enable="True" LeftEage="128" RightEage="128" TopEage="21" BottomEage="21" Scale9OriginX="128" Scale9OriginY="21" Scale9Width="134" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="449.3029" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8169" Y="0.7511" />
                <PreSize X="0.1455" Y="0.3750" />
                <FileData Type="PlistSubImage" Path="getcard_item_bg.png" Plist="room/vip.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_24_0_0" ActionTag="1320979406" Tag="87" IconVisible="False" LeftMargin="400.9152" RightMargin="103.0848" TopMargin="8.4133" BottomMargin="48.5867" FontSize="20" LabelText="局数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="46.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="423.9152" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.7708" Y="0.7511" />
                <PreSize X="0.0836" Y="0.2875" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TFRound" ActionTag="634444448" Tag="88" IconVisible="False" LeftMargin="449.8840" RightMargin="20.1160" TopMargin="4.9133" BottomMargin="45.0867" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="449.8840" Y="60.0867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.8180" Y="0.7511" />
                <PreSize X="0.1455" Y="0.3750" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1_1" ActionTag="-1684786747" Tag="89" IconVisible="False" LeftMargin="149.0919" RightMargin="320.9081" TopMargin="48.3568" BottomMargin="1.6432" Scale9Enable="True" LeftEage="128" RightEage="128" TopEage="21" BottomEage="21" Scale9OriginX="128" Scale9OriginY="21" Scale9Width="134" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="149.0919" Y="16.6432" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2711" Y="0.2080" />
                <PreSize X="0.1455" Y="0.3750" />
                <FileData Type="PlistSubImage" Path="getcard_item_bg.png" Plist="room/vip.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_24_1" ActionTag="1203466700" Tag="90" IconVisible="False" LeftMargin="10.7581" RightMargin="404.2419" TopMargin="51.8568" BottomMargin="5.1432" FontSize="20" LabelText="服务器id(选填):" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="78.2581" Y="16.6432" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.1423" Y="0.2080" />
                <PreSize X="0.2455" Y="0.2875" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TFServerid" ActionTag="-1888525877" Tag="91" IconVisible="False" LeftMargin="148.6729" RightMargin="321.3271" TopMargin="48.3568" BottomMargin="1.6432" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="148.6729" Y="16.6432" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.2703" Y="0.2080" />
                <PreSize X="0.1455" Y="0.3750" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnSure" ActionTag="291801650" Tag="92" IconVisible="False" LeftMargin="470.2551" RightMargin="33.7449" TopMargin="43.6990" BottomMargin="0.3010" TouchEnable="True" FontSize="14" ButtonText="确定" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="493.2551" Y="18.3010" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8968" Y="0.2288" />
                <PreSize X="0.0836" Y="0.4500" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnReset" ActionTag="-2084908648" Tag="93" IconVisible="False" LeftMargin="396.9241" RightMargin="107.0759" TopMargin="43.6989" BottomMargin="0.3011" TouchEnable="True" FontSize="14" ButtonText="重置" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="46.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="419.9241" Y="18.3011" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7635" Y="0.2288" />
                <PreSize X="0.0836" Y="0.4500" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
                <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1_0_1" ActionTag="-1856752230" Tag="94" IconVisible="False" LeftMargin="305.2301" RightMargin="164.7699" TopMargin="47.3979" BottomMargin="2.6021" Scale9Enable="True" LeftEage="128" RightEage="128" TopEage="21" BottomEage="21" Scale9OriginX="128" Scale9OriginY="21" Scale9Width="134" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="305.2301" Y="17.6021" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5550" Y="0.2200" />
                <PreSize X="0.1455" Y="0.3750" />
                <FileData Type="PlistSubImage" Path="getcard_item_bg.png" Plist="room/vip.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_24_0_1" ActionTag="496372154" Tag="95" IconVisible="False" LeftMargin="242.8435" RightMargin="245.1565" TopMargin="50.8990" BottomMargin="6.1010" FontSize="20" LabelText="用户id:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="273.8435" Y="17.6010" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.4979" Y="0.2200" />
                <PreSize X="0.1127" Y="0.2875" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TFUserID" ActionTag="1700463740" Tag="96" IconVisible="False" LeftMargin="305.8112" RightMargin="164.1888" TopMargin="47.3983" BottomMargin="2.6017" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="305.8112" Y="17.6017" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.5560" Y="0.2200" />
                <PreSize X="0.1455" Y="0.3750" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="393.7892" Y="325.1618" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>