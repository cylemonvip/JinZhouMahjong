<GameFile>
  <PropertyGroup Name="RoomIdInputLayer" Type="Layer" ID="01cc8968-0646-4944-866d-94f5edeb9136" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="16" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="-1034745064" Alpha="171" Tag="73" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="179" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="pri_sp_ideditbg" ActionTag="1686743592" Tag="70" IconVisible="False" LeftMargin="167.0000" RightMargin="167.0000" TopMargin="63.9995" BottomMargin="36.0005" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" TopEage="269" BottomEage="187" Scale9OriginY="269" Scale9Width="857" Scale9Height="133" ctype="PanelObjectData">
            <Size X="1000.0000" Y="650.0000" />
            <Children>
              <AbstractNodeData Name="bkk" ActionTag="-1556110619" Tag="76" IconVisible="False" LeftMargin="66.6299" RightMargin="65.3701" TopMargin="180.2512" BottomMargin="99.7488" Scale9Enable="True" LeftEage="252" RightEage="252" TopEage="97" BottomEage="97" Scale9OriginX="252" Scale9OriginY="97" Scale9Width="261" Scale9Height="101" ctype="ImageViewObjectData">
                <Size X="868.0000" Y="370.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="500.6299" Y="469.7488" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5006" Y="0.7227" />
                <PreSize X="0.8680" Y="0.5692" />
                <FileData Type="Normal" Path="room_id_input/bkk.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num1" ActionTag="-195158273" Tag="20" IconVisible="False" LeftMargin="484.0500" RightMargin="382.9500" TopMargin="204.7169" BottomMargin="370.2831" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="550.5500" Y="407.7831" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5505" Y="0.6274" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_1.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_1.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num2" ActionTag="1966220425" Tag="21" IconVisible="False" LeftMargin="628.1642" RightMargin="238.8358" TopMargin="204.7160" BottomMargin="370.2840" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="694.6642" Y="407.7840" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6947" Y="0.6274" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_2.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num3" ActionTag="-569130389" Tag="22" IconVisible="False" LeftMargin="772.2782" RightMargin="94.7218" TopMargin="204.7160" BottomMargin="370.2840" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="838.7782" Y="407.7840" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8388" Y="0.6274" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_3.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_3.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_3.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num4" ActionTag="-604225920" Tag="23" IconVisible="False" LeftMargin="484.0500" RightMargin="382.9500" TopMargin="289.0233" BottomMargin="285.9767" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="550.5500" Y="323.4767" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5505" Y="0.4977" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_4.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_4.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_4.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num5" ActionTag="81640117" Tag="24" IconVisible="False" LeftMargin="628.1642" RightMargin="238.8358" TopMargin="289.0227" BottomMargin="285.9773" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="694.6642" Y="323.4773" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6947" Y="0.4977" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_5.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_5.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_5.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num6" ActionTag="1619923935" Tag="25" IconVisible="False" LeftMargin="772.2783" RightMargin="94.7217" TopMargin="289.0227" BottomMargin="285.9773" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="838.7783" Y="323.4773" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8388" Y="0.4977" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_6.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_6.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_6.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num7" ActionTag="2030805213" Tag="26" IconVisible="False" LeftMargin="484.0500" RightMargin="382.9500" TopMargin="373.3298" BottomMargin="201.6702" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="550.5500" Y="239.1702" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5505" Y="0.3680" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_7.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_7.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_7.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num8" ActionTag="-1843842339" Tag="27" IconVisible="False" LeftMargin="628.1642" RightMargin="238.8358" TopMargin="373.3295" BottomMargin="201.6705" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="694.6642" Y="239.1705" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6947" Y="0.3680" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_8.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_8.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_8.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num9" ActionTag="1852254127" Tag="28" IconVisible="False" LeftMargin="772.2783" RightMargin="94.7217" TopMargin="373.3294" BottomMargin="201.6706" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="838.7783" Y="239.1706" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8388" Y="0.3680" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_9.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_9.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_9.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num0" ActionTag="540766664" Tag="29" IconVisible="False" LeftMargin="628.1642" RightMargin="238.8358" TopMargin="457.6361" BottomMargin="117.3639" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="694.6642" Y="154.8639" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6947" Y="0.2383" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_0.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_0.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_0.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_clear" ActionTag="532765255" Tag="30" IconVisible="False" LeftMargin="484.0500" RightMargin="382.9500" TopMargin="457.6361" BottomMargin="117.3639" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="550.5500" Y="154.8639" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5505" Y="0.2383" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_clear.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_clear.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_clear.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_del" ActionTag="835022667" Tag="31" IconVisible="False" LeftMargin="772.2783" RightMargin="94.7217" TopMargin="457.6361" BottomMargin="117.3639" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="103" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="133.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="838.7783" Y="154.8639" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8388" Y="0.2383" />
                <PreSize X="0.1330" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/btn_del.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/btn_del.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/btn_del.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_0" ActionTag="1620666935" Tag="57" IconVisible="False" LeftMargin="175.6900" RightMargin="795.3100" TopMargin="102.5933" BottomMargin="488.4067" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="190.1900" Y="517.9067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.1902" Y="0.7968" />
                <PreSize X="0.0290" Y="0.0908" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid" ActionTag="-159371142" Tag="56" IconVisible="False" LeftMargin="331.2867" RightMargin="616.7133" TopMargin="769.5485" BottomMargin="-160.5485" CharWidth="52" CharHeight="41" LabelText="1" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="52.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="357.2867" Y="-140.0485" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3573" Y="-0.2155" />
                <PreSize X="0.0520" Y="0.0631" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="room/atlas_roomid.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="-1406303764" Tag="50" IconVisible="False" LeftMargin="937.5473" RightMargin="-5.5473" TopMargin="21.8965" BottomMargin="560.1035" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="971.5473" Y="594.1035" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9715" Y="0.9140" />
                <PreSize X="0.0680" Y="0.1046" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="plaza_btn_close_normal.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_2" ActionTag="-1018522487" Tag="58" IconVisible="False" LeftMargin="421.4888" RightMargin="549.5112" TopMargin="102.5933" BottomMargin="488.4067" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="435.9888" Y="517.9067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4360" Y="0.7968" />
                <PreSize X="0.0290" Y="0.0908" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_1" ActionTag="-384215604" Tag="59" IconVisible="False" LeftMargin="298.5894" RightMargin="672.4106" TopMargin="102.5933" BottomMargin="488.4067" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="313.0894" Y="517.9067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.3131" Y="0.7968" />
                <PreSize X="0.0290" Y="0.0908" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_3" ActionTag="-988490495" Tag="60" IconVisible="False" LeftMargin="544.3883" RightMargin="426.6117" TopMargin="102.5933" BottomMargin="488.4067" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="558.8883" Y="517.9067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.5589" Y="0.7968" />
                <PreSize X="0.0290" Y="0.0908" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_4" ActionTag="-1738655502" Tag="61" IconVisible="False" LeftMargin="667.2876" RightMargin="303.7124" TopMargin="102.5933" BottomMargin="488.4067" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="681.7876" Y="517.9067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.6818" Y="0.7968" />
                <PreSize X="0.0290" Y="0.0908" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_5" ActionTag="1373596364" Tag="62" IconVisible="False" LeftMargin="790.1871" RightMargin="180.8129" TopMargin="102.5933" BottomMargin="488.4067" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="804.6871" Y="517.9067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.8047" Y="0.7968" />
                <PreSize X="0.0290" Y="0.0908" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnOk" ActionTag="-799932946" Tag="71" IconVisible="False" LeftMargin="192.0000" RightMargin="592.0000" TopMargin="540.1610" BottomMargin="34.8390" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="186" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="216.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0000" Y="72.3390" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3000" Y="0.1113" />
                <PreSize X="0.2160" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/bt_service_confirm_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/bt_service_confirm_1.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/bt_service_confirm_0.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RoomInfoLayout" ActionTag="-324489336" Tag="59" IconVisible="False" LeftMargin="65.1933" RightMargin="584.8067" TopMargin="210.2162" BottomMargin="59.7838" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="350.0000" Y="380.0000" />
                <Children>
                  <AbstractNodeData Name="TxtTitle" ActionTag="1567858914" Tag="60" IconVisible="False" LeftMargin="88.6879" RightMargin="161.3121" TopMargin="24.0042" BottomMargin="326.9958" FontSize="25" LabelText="四人麻将" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="138.6879" Y="341.4958" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="26" G="26" B="26" />
                    <PrePosition X="0.3963" Y="0.8987" />
                    <PreSize X="0.2857" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtQuanShu" ActionTag="-572942183" Tag="61" IconVisible="False" LeftMargin="23.3419" RightMargin="230.6581" TopMargin="66.5213" BottomMargin="284.4787" FontSize="25" LabelText="圈数:4圈" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="96.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="23.3419" Y="298.9787" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="26" G="26" B="26" />
                    <PrePosition X="0.0667" Y="0.7868" />
                    <PreSize X="0.2743" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtRenShu" ActionTag="2141076300" Tag="62" IconVisible="False" LeftMargin="260.1747" RightMargin="-6.1747" TopMargin="66.5213" BottomMargin="284.4787" FontSize="25" LabelText="人数:4人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="96.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="260.1747" Y="298.9787" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="26" G="26" B="26" />
                    <PrePosition X="0.7434" Y="0.7868" />
                    <PreSize X="0.2743" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtHostName" ActionTag="-2147104918" Tag="63" IconVisible="False" LeftMargin="23.3417" RightMargin="98.6583" TopMargin="106.9315" BottomMargin="244.0685" IsCustomSize="True" FontSize="25" LabelText="房主:你好你好你好..." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="228.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="23.3417" Y="258.5685" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.0667" Y="0.6804" />
                    <PreSize X="0.6514" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtHostID" ActionTag="1486585060" Tag="64" IconVisible="False" LeftMargin="259.8088" RightMargin="-25.8088" TopMargin="106.9315" BottomMargin="244.0685" FontSize="25" LabelText="ID:555555" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="116.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="259.8088" Y="258.5685" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.7423" Y="0.6804" />
                    <PreSize X="0.3314" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerName_1" ActionTag="1848321594" Tag="65" IconVisible="False" LeftMargin="23.3400" RightMargin="98.6600" TopMargin="147.3416" BottomMargin="203.6584" IsCustomSize="True" FontSize="25" LabelText="房主:cccccc..." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="228.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="23.3400" Y="218.1584" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.0667" Y="0.5741" />
                    <PreSize X="0.6514" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerID_1" ActionTag="347975677" Tag="66" IconVisible="False" LeftMargin="259.8088" RightMargin="-25.8088" TopMargin="147.3416" BottomMargin="203.6584" FontSize="25" LabelText="ID:222222" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="116.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="259.8088" Y="218.1584" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.7423" Y="0.5741" />
                    <PreSize X="0.3314" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerName_2" ActionTag="-1436933232" Tag="67" IconVisible="False" LeftMargin="23.3417" RightMargin="98.6583" TopMargin="187.7518" BottomMargin="163.2482" IsCustomSize="True" FontSize="25" LabelText="玩家:胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="228.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="23.3417" Y="177.7482" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.0667" Y="0.4678" />
                    <PreSize X="0.6514" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerID_2" ActionTag="1454249103" Tag="68" IconVisible="False" LeftMargin="259.8088" RightMargin="-25.8088" TopMargin="187.7518" BottomMargin="163.2482" FontSize="25" LabelText="ID:888888" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="116.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="259.8088" Y="177.7482" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.7423" Y="0.4678" />
                    <PreSize X="0.3314" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerName_3" ActionTag="-988552894" Tag="69" IconVisible="False" LeftMargin="23.3417" RightMargin="98.6583" TopMargin="228.1619" BottomMargin="122.8381" IsCustomSize="True" FontSize="25" LabelText="玩家:胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="228.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="23.3417" Y="137.3381" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.0667" Y="0.3614" />
                    <PreSize X="0.6514" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerID_3" ActionTag="375155794" Tag="70" IconVisible="False" LeftMargin="259.8088" RightMargin="-25.8088" TopMargin="228.1619" BottomMargin="122.8381" FontSize="25" LabelText="ID:999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="116.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="259.8088" Y="137.3381" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.7423" Y="0.3614" />
                    <PreSize X="0.3314" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtRule" ActionTag="-275352873" Tag="71" IconVisible="False" LeftMargin="16.9305" RightMargin="-75.9305" TopMargin="303.1644" BottomMargin="8.8356" IsCustomSize="True" FontSize="25" LabelText="玩法：会牌、三清、穷胡加倍、七对、清一色、天地胡、封顶200" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="409.0000" Y="68.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="16.9305" Y="76.8356" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="26" G="26" B="26" />
                    <PrePosition X="0.0484" Y="0.2022" />
                    <PreSize X="1.1686" Y="0.1789" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerName_4" ActionTag="1127095125" Tag="74" IconVisible="False" LeftMargin="22.5368" RightMargin="99.4632" TopMargin="268.5721" BottomMargin="82.4279" IsCustomSize="True" FontSize="25" LabelText="玩家:胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="228.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="22.5368" Y="96.9279" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.0644" Y="0.2551" />
                    <PreSize X="0.6514" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="TxtPlayerID_4" ActionTag="-1364710396" Tag="75" IconVisible="False" LeftMargin="259.8089" RightMargin="-25.8089" TopMargin="268.5721" BottomMargin="82.4279" FontSize="25" LabelText="ID:999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="116.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="259.8089" Y="96.9279" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="233" G="204" B="150" />
                    <PrePosition X="0.7423" Y="0.2551" />
                    <PreSize X="0.3314" Y="0.0763" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="240.1933" Y="439.7838" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2402" Y="0.6766" />
                <PreSize X="0.3500" Y="0.5846" />
                <SingleColor A="255" R="139" G="105" B="20" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnCancel" ActionTag="-1459441837" Tag="72" IconVisible="False" LeftMargin="580.9192" RightMargin="203.0808" TopMargin="541.1390" BottomMargin="33.8610" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="186" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="216.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5513" ScaleY="0.5521" />
                <Position X="700.0000" Y="75.2685" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7000" Y="0.1158" />
                <PreSize X="0.2160" Y="0.1154" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room_id_input/bt_service_cancel_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="room_id_input/bt_service_cancel_1.png" Plist="" />
                <NormalFileData Type="Normal" Path="room_id_input/bt_service_cancel_0.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="361.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4813" />
            <PreSize X="0.7496" Y="0.8667" />
            <FileData Type="Normal" Path="room_id_input/sp_background.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>