1、
//空闲状态
struct CMD_S_StatusFree
{
	LONGLONG						lTurnScore[GAME_PLAYER];			//历史分数
	LONGLONG						lCollectScore[GAME_PLAYER];			//历史分数
};

2、 #define SUB_S_GAME_START			100								//游戏开始
//发送扑克
struct CMD_S_GameStart
{
	//扑克信息
	WORD				 			wCurrentUser;						//当前玩家
	BYTE							cbCardData[MAX_COUNT-1];			//扑克列表
};

3、客户端上发出牌消息   #define SUB_C_OUT_CARD				1			//用户出牌
//用户出牌
struct CMD_C_OutCard
{
	BYTE							cbCardCount;						//出牌数目
	BYTE							cbCardData[MAX_COUNT];				//扑克数据
};

服务器出牌下发    #define SUB_S_OUT_CARD				101				//用户出牌
//用户出牌
struct CMD_S_OutCard
{
	BYTE							cbCardCount;						//出牌数目
	WORD				 			wCurrentUser;						//当前玩家
	WORD							wOutCardUser;						//出牌玩家
	LONGLONG						TurnScore;							//本轮分数
	BYTE							b510KCardRecord[3][8];				//510K记录
	BYTE							cbCardData[MAX_COUNT-1];			//扑克列表
};

4、当自己出完牌时 把队友牌发给自己显示出来     #define SUB_S_CARD_INFO				103									//扑克信息
//扑克信息
struct CMD_S_CardInfo
{
	BYTE							cbCardCount;						//扑克数目
	BYTE							cbCardData[MAX_COUNT];				//扑克列表
};


5、 用户放弃下发      #define SUB_S_PASS_CARD				102		    //用户放弃
//放弃出牌
struct CMD_S_PassCard
{
	BYTE							cbTurnOver;							//一轮结束
	WORD				 			wCurrentUser;						//当前玩家
	WORD				 			wPassCardUser;						//放弃玩家
	LONGLONG						TurnScore;							//本轮分数
	LONGLONG						PlayerScore[GAME_PLAYER];			//玩家分数
};

6、游戏结束 #define SUB_S_GAME_END				104						//游戏结束
//游戏结束
struct CMD_S_GameEnd
{
	LONGLONG						PlayerScore[GAME_PLAYER];        //游戏分数

	//游戏成绩
	LONGLONG						lGameScore[GAME_PLAYER];			//游戏积分

	//扑克信息
	BYTE							cbCardCount[GAME_PLAYER];			//扑克数目
	BYTE							cbCardData[GAME_PLAYER][MAX_COUNT];	//扑克列表
};

7、游戏场景
//游戏状态
struct CMD_S_StatusPlay
{
	//游戏变量
	WORD							wCurrentUser;						//当前玩家

	//胜利信息
	WORD							wWinCount;							//胜利人数
	WORD							wWinOrder[GAME_PLAYER];				//胜利列表

	//出牌信息
	WORD							wTurnWiner;							//本轮胜者
	BYTE							cbTurnCardType;						//出牌类型
	BYTE							cbTurnCardCount;					//出牌数目
	BYTE							cbTurnCardData[MAX_COUNT];			//出牌数据
	BYTE							cbMagicCardData[MAX_COUNT];			//变幻扑克

	//扑克信息
	BYTE							cbHandCardData[MAX_COUNT];			//手上扑克
	BYTE							cbHandCardCount[GAME_PLAYER];		//扑克数目
};