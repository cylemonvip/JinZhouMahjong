local ClubCreateRoomNode = class("ClubCreateRoomNode", cc.Node)
local ClubHzRuleNode = require("client/src/plaza/views/layer/plaza/ClubHzRuleNode")
local ClubDPRuleNode = require("client/src/plaza/views/layer/plaza/ClubDPRuleNode")
local ClubLandRuleNode = require("client/src/plaza/views/layer/plaza/ClubLandRuleNode")

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")

local RES_CSB = "client/res/club/ClubCreateRoomNode.csb"

function ClubCreateRoomNode:ctor(clubManagerNode)
	self._clubManagerNode = clubManagerNode
	self._clubNum = self._clubManagerNode._clubNum
	self._currInfo = nil
	self:initCSB()
end

function ClubCreateRoomNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode

	local function iconBtnCallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onGameIconCallback(ref)
        end
    end

    --创建面板的父节点
	local createPanel = rootNode:getChildByName("CreatePanel")
	self._createPanel = createPanel

	--游戏列表
	local gameList = rootNode:getChildByName("GameList")

	local gameListData = self._clubManagerNode._clientScene:getApp()._gameList

	for k,v in pairs(gameListData) do
		local iconPath = "client/res/GameList/game_" .. v._KindID .. ".png"
		print(iconPath)

		local iconBtn = ccui.Button:create()
		iconBtn:setName("ICON_BTN_" .. k)
		iconBtn.info = v
		gameList:pushBackCustomItem(iconBtn)

		iconBtn:loadTextureNormal(iconPath, ccui.TextureResType.localType)
		iconBtn:loadTexturePressed(iconPath, ccui.TextureResType.localType)
		iconBtn:addTouchEventListener(iconBtnCallback)
		if k == 1 then
			self:onGameIconCallback(iconBtn)
		end
	end

	--关闭按钮
	local closeBtn = rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btncallback)

end

function ClubCreateRoomNode:close()
	self:removeFromParent()
end

function ClubCreateRoomNode:onGameIconCallback(ref)
	if self._currInfo == ref.info then
		return
	end

	local feeConfig = GlobalUserItem.getFeeConfigByKindId(tonumber(ref.info._KindID))

	if not feeConfig then
		showToast(cc.Director:getInstance():getRunningScene(), "获取房间配置失败，无法设置该游戏规则", 2)
		return 
	end

	self._currInfo = ref.info
	self._createPanel:removeAllChildren()

	if tonumber(self._currInfo._KindID) == yl.KIND_ID_HZ_MAHJONG then
		local clubHzRuleNode = ClubHzRuleNode:create(self)
		local size = self._createPanel:getContentSize()
		clubHzRuleNode:setPosition(cc.p(size.width/2, size.height/2))
		self._createPanel:addChild(clubHzRuleNode)
	elseif tonumber(self._currInfo._KindID) == yl.KIND_ID_LAND then
		local clubLandRuleNode = ClubLandRuleNode:create(self)
		local size = self._createPanel:getContentSize()
		clubLandRuleNode:setPosition(cc.p(size.width/2, size.height/2))
		self._createPanel:addChild(clubLandRuleNode)
	elseif tonumber(self._currInfo._KindID) == yl.KIND_ID_510K then
		local clubDPRuleNode = ClubDPRuleNode:create(self)
		local size = self._createPanel:getContentSize()
		clubDPRuleNode:setPosition(cc.p(size.width/2, size.height/2))
		self._createPanel:addChild(clubDPRuleNode)
	end
end

function ClubCreateRoomNode:onButtonCallback(name, ref)
	if "RootNode" == name or "CloseBtn" == name then
		self:close()
	end
end

return ClubCreateRoomNode