local ClubDPRuleNode = class("ClubDPRuleNode", cc.Node)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")

local RES_CSB = "client/res/club/K510RuleNode.csb"

local CLUB_K510_QUAN_CONFIG_INDEX = "CLUB_K510_QUAN_CONFIG_INDEX"
local CLUB_K510_BIG_OR_SMALL_CONFIG_INDEX = "CLUB_K510_BIG_OR_SMALL_CONFIG_INDEX"

local CBX_INNINGS_QS_START = 100
local CBX_INNINGS_QS_END = 103

function ClubDPRuleNode:ctor(scene)
	self._scene = scene
	self:initCSB()
end

function ClubDPRuleNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode

	local sureRuleBtn = rootNode:getChildByName("SureRuleBtn")
	sureRuleBtn:addTouchEventListener(btncallback)

	--复选按钮
    local cbtlistener = function (sender,eventType)
        self:onSelectedEvent(sender:getTag(), sender)
    end

    local bsIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_K510_BIG_OR_SMALL_CONFIG_INDEX, 1))
	local bsCheckBox = rootNode:getChildByName("BigOrSmallCheckBox")
	bsCheckBox:addEventListener(cbtlistener)
	if bsIndex == 1 then
		bsCheckBox:setSelected(true)
	else
		bsCheckBox:setSelected(false)
	end

	local quanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_K510_QUAN_CONFIG_INDEX, 2))
	local feeConfig = GlobalUserItem.getFeeConfigByKindId(yl.KIND_ID_510K)
	if feeConfig then
		for i = 1, 3 do
			local qsCheckBox = rootNode:getChildByName("QuanShuCheckBox_" .. i)
			local txt = qsCheckBox:getChildByName("Txt")
			txt:setString(feeConfig[i].dwDrawCountLimit .. "圈(房卡X" .. feeConfig[i].lFeeScore .. ")")

			if quanIndex == i then
				qsCheckBox:setSelected(true)
			else
				qsCheckBox:setSelected(false)
			end

			qsCheckBox:setTag(CBX_INNINGS_QS_START + i)
		    qsCheckBox:addEventListener(cbtlistener)
		end
	end
	
end

--根据配置初始化显示
function ClubDPRuleNode:setConfig(config)

end

function ClubDPRuleNode:getCreateConfig()
	for i = 1, 3 do
		local qsCheckBox = self._rootNode:getChildByName("QuanShuCheckBox_" .. i)
		local b = qsCheckBox:isSelected()
		if b then
			cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_QUAN_CONFIG_INDEX, i)
		end
	end

	local bsCheckBox = self._rootNode:getChildByName("BigOrSmallCheckBox")
	local bsb = bsCheckBox:isSelected()
	if bsb then
		cc.UserDefault:getInstance():setStringForKey(CLUB_K510_BIG_OR_SMALL_CONFIG_INDEX, 1)
	else
		cc.UserDefault:getInstance():setStringForKey(CLUB_K510_BIG_OR_SMALL_CONFIG_INDEX, 0)
	end

    local buffer = CCmd_Data:create(174)
    buffer:setcmdinfo(cmd_private.login.MDM_MB_PERSONAL_SERVICE, cmd_private.login.SUB_MB_MODIFY_CLUB_RULE)
    --管理者ID
    buffer:pushdword(GlobalUserItem.dwUserID)
    --俱乐部号
    buffer:pushdword(self._scene._clubManagerNode._clubNum)
    --密码
    buffer:pushstring(GlobalUserItem.szPassword, yl.LEN_PASSWORD)

    buffer:pushbyte(1)
    --具体规则

    --人数
    buffer:pushbyte(4)
    --是否支持大小血
    buffer:pushbool(bsb)

    return buffer
end

function ClubDPRuleNode:getShareMsg()
	
end

function ClubDPRuleNode:onSelectedEvent(tag, sender)
	if tag > CBX_INNINGS_QS_START and tag <= CBX_INNINGS_QS_END then
		for i = 1, 3 do
			local qsCheckBox = self._rootNode:getChildByName("QuanShuCheckBox_" .. i)
			qsCheckBox:setSelected(false)
		end
		sender:setSelected(true)
	end
end

function ClubDPRuleNode:onButtonCallback(name, ref)
	if "RootNode" == name or "CloseBtn" == name then

	elseif "SureRuleBtn" == name then
		local buffer = self:getCreateConfig()
		self._scene._clubManagerNode:sendModifyRule(buffer)
	end
end

return ClubDPRuleNode