local ClubLandRuleNode = class("ClubLandRuleNode", cc.Node)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local cmd_private = appdf.req(appdf.CLIENT_SRC .. "privatemode.header.CMD_Private")

local RES_CSB = "client/res/club/LandRuleNode.csb"

local CLUB_LAND_QUAN_CONFIG_INDEX = "CLUB_LAND_QUAN_CONFIG_INDEX"
local CLUB_LAND_FAN_CONFIG_INDEX = "CLUB_LAND_FAN_CONFIG_INDEX"
local CLUB_LAND_RULE_CONFIG_INDEX_1 = "CLUB_LAND_RULE_CONFIG_INDEX_1"
local CLUB_LAND_RULE_CONFIG_INDEX_2 = "CLUB_LAND_RULE_CONFIG_INDEX_2"

local CBX_INNINGS_QS_START = 100
local CBX_INNINGS_QS_END = 103

local CBX_INNINGS_FAN_START = 120
local CBX_INNINGS_FAN_END = 123

function ClubLandRuleNode:ctor(scene)
	self._scene = scene
	self:initCSB()
end

function ClubLandRuleNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode
	-- rootNode:addTouchEventListener(btncallback)

	local sureRuleBtn = rootNode:getChildByName("SureRuleBtn")
	sureRuleBtn:addTouchEventListener(btncallback)

	--复选按钮
    local cbtlistener = function (sender,eventType)
        self:onSelectedEvent(sender:getTag(), sender)
    end

	--圈数
	local quanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_QUAN_CONFIG_INDEX, 2))
	
	local feeConfig = GlobalUserItem.getFeeConfigByKindId(yl.KIND_ID_LAND)
	dump(feeConfig, "----- ClubHzRuleNode feeConfig -----")

	if feeConfig then
		for i = 1, 3 do
			local qsCheckBox = rootNode:getChildByName("QuanShuCheckBox_" .. i)
			local txt = qsCheckBox:getChildByName("Txt")
			txt:setString(feeConfig[i].dwDrawCountLimit .. "圈(房卡X" .. feeConfig[i].lFeeScore .. ")")

			if quanIndex == i then
				qsCheckBox:setSelected(true)
			else
				qsCheckBox:setSelected(false)
			end

			qsCheckBox:setTag(CBX_INNINGS_QS_START + i)
		    qsCheckBox:addEventListener(cbtlistener)
		end
	end

	local fanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_FAN_CONFIG_INDEX, 2))
	for i = 1, 3 do
		local fanCheckBox = rootNode:getChildByName("FanCheckBox_" .. i)
		fanCheckBox:setTag(CBX_INNINGS_FAN_START + i)
		fanCheckBox:addEventListener(cbtlistener)

		if fanIndex == i then
			fanCheckBox:setSelected(true)
		else
			fanCheckBox:setSelected(false)
		end
	end

	
	local ruleCheckBox_1 = rootNode:getChildByName("RuleCheckBox_1")
	local ruleIndex_1 = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_1, 1))
	if ruleIndex_1 == 1 then
		ruleCheckBox_1:setSelected(true)
	else
		ruleCheckBox_1:setSelected(false)
	end

	local ruleCheckBox_2 = rootNode:getChildByName("RuleCheckBox_1")
	local ruleIndex_2 = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_2, 1))
	if ruleIndex_2 == 1 then
		ruleCheckBox_2:setSelected(true)
	else
		ruleCheckBox_2:setSelected(false)
	end
end

function ClubLandRuleNode:getCreatRoomBuffer(clubManagerNode)
    local buffer = CCmd_Data:create(188)
    buffer:setcmdinfo(cmd_private.game.MDM_GR_PERSONAL_TABLE, cmd_private.game.SUB_GR_CREATE_TABLE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushdword(clubManagerNode._clubNum)

    local feeIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_QUAN_CONFIG_INDEX, 2))
    local feeConfig = GlobalUserItem.getFeeConfigByKindId(yl.KIND_ID_LAND)

    buffer:pushdword(feeConfig[feeIndex].dwDrawCountLimit)
    buffer:pushdword(feeConfig[feeIndex].dwDrawTimeLimit)
    --这个人数不用了
    buffer:pushword(3)
    buffer:pushdword(0)
    buffer:pushstring("", yl.LEN_PASSWORD)

    buffer:pushbyte(0)
    --人数
    buffer:pushbyte(3)
    local ruleIndex_1 = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_1, 1))
    local ruleIndex_2 = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_2, 1))
    local fanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_LAND_FAN_CONFIG_INDEX, 2))

    buffer:pushbool(ruleIndex_1 == 1)
    buffer:pushbool(ruleIndex_2 == 1)

    if fanIndex == 1 then
    	buffer:pushbool(3)
    elseif fanIndex == 2 then
    	buffer:pushbool(4)
    elseif fanIndex == 3 then
    	buffer:pushbool(5)
    end

    return buffer
end

function ClubLandRuleNode:getCreateConfig()
	for i = 1, 3 do
		local qsCheckBox = self._rootNode:getChildByName("QuanShuCheckBox_" .. i)
		local b = qsCheckBox:isSelected()
		if b then
			cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_QUAN_CONFIG_INDEX, i)
		end
	end

	local fanShu = 2
	for i = 1, 3 do
		local fanCheckBox = rootNode:getChildByName("FanCheckBox_" .. i)
		local b = fanCheckBox:isSelected()
		if b then
			fanShu = fanShu + i
			cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_FAN_CONFIG_INDEX, i)
		end
	end

	local ruleCheckBox_1 = rootNode:getChildByName("RuleCheckBox_1")
	local b1 = ruleCheckBox_1:isSelected()
	if b1 then
		cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_1, 1)
	else
		cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_1, 0)
	end

	local ruleCheckBox_2 = rootNode:getChildByName("RuleCheckBox_2")
	local b2 = ruleCheckBox_2:isSelected()
	if b2 then
		cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_2, 1)
	else
		cc.UserDefault:getInstance():setStringForKey(CLUB_LAND_RULE_CONFIG_INDEX_2, 0)
	end

	local buffer = CCmd_Data:create(174)
    buffer:setcmdinfo(cmd_private.login.MDM_MB_PERSONAL_SERVICE, cmd_private.login.SUB_MB_MODIFY_CLUB_RULE)
    --管理者ID
    buffer:pushdword(GlobalUserItem.dwUserID)
    --俱乐部号
    buffer:pushdword(self._scene._clubManagerNode._clubNum)
    --密码
    buffer:pushstring(GlobalUserItem.szPassword, yl.LEN_PASSWORD)

    buffer:pushbyte(1)
    --具体规则

    --人数
    buffer:pushbyte(3)
    buffer:pushbool(b1)
    buffer:pushbool(b2)
    if fanShu ~= 3 and fanShu ~= 4 and fanShu ~= 5 then
    	fanShu = 4
    end
    buffer:pushbyte(fanShu)
	return buffer
end

function ClubLandRuleNode:getShareMsg()
	
end

function ClubLandRuleNode:onSelectedEvent(tag, sender)
	if tag > CBX_INNINGS_QS_START and tag <= CBX_INNINGS_QS_END then
		for i = 1, 3 do
			local qsCheckBox = self._rootNode:getChildByName("QuanShuCheckBox_" .. i)
			qsCheckBox:setSelected(false)
		end
		sender:setSelected(true)
	elseif tag > CBX_INNINGS_FAN_START and tag <= CBX_INNINGS_FAN_END then
		for i = 1, 3 do
			local fanCheckBox = self._rootNode:getChildByName("FanCheckBox_" .. i)
			fanCheckBox:setSelected(false)
		end
		sender:setSelected(true)
	end
end

function ClubLandRuleNode:onButtonCallback(name, ref)
	if "RootNode" == name or "CloseBtn" == name then

	elseif "SureRuleBtn" == name then
		local buffer = self:getCreateConfig()
		self._scene._clubManagerNode:sendModifyRule(buffer)
	end
end

return ClubLandRuleNode