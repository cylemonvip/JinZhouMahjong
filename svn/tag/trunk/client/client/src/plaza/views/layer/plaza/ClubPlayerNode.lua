local ClubPlayerNode = class("ClubPlayerNode", cc.Node)
local ClubApplyDetailPopupNode = require("client/src/plaza/views/layer/plaza/ClubApplyDetailPopupNode")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Utils = appdf.req(appdf.CLIENT_SRC .. "utils.Utils")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")

local RES_CSB = "client/res/club/PlayerNode.csb"

function ClubPlayerNode:ctor(clubManagerNode)
	self._clubManagerNode = clubManagerNode
	self:initCSB()
end

function ClubPlayerNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
            local bpy = ref:getTouchBeganPosition().y
        	local epy = ref:getTouchEndPosition().y
        	if math.abs(bpy-epy) < 1 then
        		self:onButtonCallback(ref:getName(), ref)
        	end
        end
    end

	--Root Node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode
	self._rootNode:setSwallowTouches(false)
	rootNode:addTouchEventListener(btncallback)

	self._tipSp = rootNode:getChildByName("TipSp")
	self._NameTxt = rootNode:getChildByName("NameTxt")
end

function ClubPlayerNode:setData(data)
	self._data = data
	self._tipSp:setVisible(not data.bApply)
	local str = Utils.stringEllipsis(data.szNickName, self._NameTxt:getFontSize(), self._NameTxt:getContentSize().width, "...")
	self._NameTxt:setString(str)

	local headImg = self._rootNode:getChildByName("HeadImg")
	local headImgSize = headImg:getContentSize()

	--将头像的链接保存到本地数据中
	cc.UserDefault:getInstance():setStringForKey("HEAD_URL_" .. data.dwUserID, data.szWeChatImgURL);

	local head = PopupInfoHead:createReplayHead(data.dwGameID, data.szWeChatImgURL, 70)
	head:setPosition(cc.p(headImgSize.width/2, headImgSize.height/2))
	headImg:addChild(head)
end

function ClubPlayerNode:close()
	self:removeFromParent()
end

function ClubPlayerNode:onButtonCallback(name, ref)
	print("ClubPlayerNode touch " .. name)
	if "RootNode" == name then
		local runScene = cc.Director:getInstance():getRunningScene()
		local clubApplyDetailPopupNode = ClubApplyDetailPopupNode:create(self)
		clubApplyDetailPopupNode:setPosition(cc.p(667, 375))
		clubApplyDetailPopupNode:setData(self._data)
		runScene:addChild(clubApplyDetailPopupNode)
	end
end

return ClubPlayerNode