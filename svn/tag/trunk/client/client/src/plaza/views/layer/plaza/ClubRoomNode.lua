local ClubRoomNode = class("ClubRoomNode", cc.Node)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local RES_CSB = "client/res/club/RoomNode.csb"

function ClubRoomNode:ctor(clubManagerNode)
	self._clubManagerNode = clubManagerNode
	self:initCSB()
end

function ClubRoomNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
		elseif tType == ccui.TouchEventType.moved then

        elseif tType == ccui.TouchEventType.ended then
        	local bpy = ref:getTouchBeganPosition().y
        	local epy = ref:getTouchEndPosition().y
        	if math.abs(bpy-epy) < 1 then
        		self:onButtonCallback(ref:getName(), ref)
        	end
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode
	self._rootNode:setSwallowTouches(false)
	rootNode:addTouchEventListener(btncallback)

	local shareBtn = rootNode:getChildByName("ShareBtn")
	shareBtn:addTouchEventListener(btncallback)

	local jieSanBtn = rootNode:getChildByName("JieSanBtn")
	jieSanBtn:addTouchEventListener(btncallback)
    jieSanBtn:setEnabled(self._clubManagerNode._isOwnClub)
end

--根据配置初始化显示
function ClubRoomNode:setData(data)
	self._data = data
	local roomCardTxt = self._rootNode:getChildByName("RoomCardTxt")
	roomCardTxt:setString("房号: " .. data.dwRoomCardId)
	local playerCountTxt = self._rootNode:getChildByName("PlayerCountTxt")
	playerCountTxt:setString("人数：" .. data.cbUserCount .. "/" .. data.cbGameRule[1][2])

	local gameNameTxt = self._rootNode:getChildByName("GameNameTxt")
	local serverId = tonumber(string.sub(tostring(data.dwRoomCardId), 1, 2)) - 10
	local kindId = tonumber(GlobalUserItem.getKindIdByServerId(serverId))
	print("ClubRoomNode kindId = ", kindId)
	local kindStr = ""
	local danwei = "圈"
	if kindId == yl.KIND_ID_HZ_MAHJONG then
		kindStr = "锦州麻将"
		danwei = "圈"
	elseif kindId == yl.KIND_ID_LAND then
		kindStr = "斗地主"
		danwei = "局"
	elseif kindId == yl.KIND_ID_510K then
		kindStr = "打屁"
		danwei = "局"
	else
		kindStr = "未知游戏"
	end
	gameNameTxt:setString("游戏:" .. kindStr)

	local gameStatusTxt = self._rootNode:getChildByName("GameStatusTxt")
	local statusStr = "未知"
	if tostring(data.cbGameStatus) == "0" then --未开始
        statusStr = "未开始"
    elseif tostring(data.cbGameStatus) == "1" then --已开始
        statusStr = "已开始"
        data.cbGameCount = data.cbGameCount + 1
    elseif tostring(data.cbGameStatus) == "2" then --已结束
        statusStr = "已结束"
    end

    gameStatusTxt:setString("状态:" .. statusStr)

    --局数当前进度
    local gameCountTxt = self._rootNode:getChildByName("RoundCountTxt")
    gameCountTxt:setString(danwei .. "数:" .. data.cbGameCount .. "/" .. data.cbAllCount)
end


function ClubRoomNode:getInviteShareMsg()
	local serverId = tonumber(string.sub(tostring(self._data.dwRoomCardId), 1, 2)) - 10
	local kindId = GlobalUserItem.getKindIdByServerId(serverId)
	kindId = kindId or 0

	kindId = tonumber(kindId)
    --规则
    local ruleStr = ""
    --人数
    local person = 0
    --标题
    local title = ""
    if kindId == yl.KIND_ID_HZ_MAHJONG then
    	person = 4
    	title = "九九麻将-锦州俱乐部约战"
        if self._data.cbGameRule[1][3] == 1 then
            ruleStr = ruleStr .. "天地胡"
        end

        if self._data.cbGameRule[1][4] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "会牌"
            else
                ruleStr = ruleStr .. "、会牌"
            end
        end

        if self._data.cbGameRule[1][5] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "穷胡加倍"
            else
                ruleStr = ruleStr .. "、穷胡加倍"    
            end
        end

        if self._data.cbGameRule[1][6] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "封顶150"
            else
                ruleStr = ruleStr .. "、封顶150"
            end
        end

        if self._data.cbGameRule[1][7] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "三清"
            else
                ruleStr = ruleStr .. "、三清"
            end
        end

        if self._data.cbGameRule[1][8] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "七对"
            else
                ruleStr = ruleStr .. "、七对"
            end
        end

        if self._data.cbGameRule[1][9] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "清一色"
            else
                ruleStr = ruleStr .. "、清一色"
            end
        end

        if self._data.cbGameRule[1][10] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "封顶200"
            else
                ruleStr = ruleStr .. "、封顶200"
            end
        end
    elseif kindId == yl.KIND_ID_LAND then
    	person = 3
    	title = "九九斗地主-锦州俱乐部约战"
    elseif kindId == yl.KIND_ID_510K then
    	person = 4
    	title = "九九打屁-锦州俱乐部约战"
    end
	if ruleStr ~= "" then
	    ruleStr = "，玩法：" .. ruleStr
	end

    local shareTxt = "房号:" .. tostring(self._data.dwRoomCardId) .. " 圈数:" .. tostring(self._data.cbAllCount) .. " 人数:" .. tostring(person) .. tostring(ruleStr)

    return {title = title, content = shareTxt .. " 游戏精彩刺激, 一起来玩吧! ", friendContent = shareTxt}
end

function ClubRoomNode:onReloadRecordList()
	self._clubManagerNode:popOnlineRoomByRoomId(self._data.dwRoomCardId)
	self._clubManagerNode:onReloadOnlineRoom()
end


function ClubRoomNode:onButtonCallback(name, ref)
	if "RootNode" == name then
		self._clubManagerNode._clientScene:onStartGameByRoomId(self._data.dwRoomCardId)
		self._clubManagerNode:removeFromParent()
	elseif "ShareBtn" == name then
		PriRoom:getInstance():getPlazaScene():popTargetShare(function(target, bMyFriend)
            local function sharecall( isok )
                if type(isok) == "string" and isok == "true" then
                    showToast(self, "分享成功", 2)
                end
            end
            local msgTab = self:getInviteShareMsg(self._data);
            local serverId = tonumber(string.sub(tostring(self._data.dwRoomCardId), 1, 2)) - 10
			local gameID = GlobalUserItem.getKindIdByServerId(serverId)
            local url = yl.WXShareURL or GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
            MultiPlatform:getInstance():shareToTarget(target, sharecall, msgTab.title, msgTab.content, url, "", false, gameID, self._data.dwRoomCardId, GlobalUserItem.dwUserID)
        end)
	elseif "JieSanBtn" == name then
		PriRoom:getInstance():showPopWait()
		PriRoom:getInstance():setViewFrame(self)
        PriRoom:getInstance():getNetFrame():onDissumeRoom(self._data.dwRoomCardId)
	end
end

return ClubRoomNode