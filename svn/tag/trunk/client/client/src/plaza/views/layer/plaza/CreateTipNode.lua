local CreateTipNode = class("CreateTipNode", cc.Node)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local RES_CSB = "client/res/club/CreateTipNode.csb"
function CreateTipNode:ctor()
	self:initCSB()
end

function CreateTipNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)
	self._csbNode = csbNode

	local function btncallback(ref, tType)
		print("tType = " .. tostring(tType) .. " ref = "  .. tostring(ref))
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	rootNode:addTouchEventListener(btncallback)

	--关闭
	local closeBtn = rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btncallback)

	local copyBtn = rootNode:getChildByName("CopyBtn")
	copyBtn:addTouchEventListener(btncallback)	
end

function CreateTipNode:onButtonCallback(name, ref)
	if "RootNode" == name or "CloseBtn" == name then
		self:removeFromParent()
	elseif "CopyBtn" == name then
		MultiPlatform:getInstance():copyToClipboard("jiujiumj01")
        showToast(cc.Director:getInstance():getRunningScene(), "已复制到剪贴板，快去微信搜索吧", 2)
	end
end

return CreateTipNode