local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local LuckyDrawPopup = class("ChoicesPopupNode", function ()
	return cc.Node:create()
end)

local CSB_RES_PATH = "client/res/luckydraw/LuckyDrawPopup.csb"

function LuckyDrawPopup:ctor(scene, path, date)
	self._scene = scene
	self._path = path
	self._date = date
	self:initCsb()

	self:registerScriptHandler(function(eventType)
		if eventType == "exit" then
			self:onExit()
		elseif eventType == "enter" then
			self:onEnter()
		end
	end)
end

function LuckyDrawPopup:onEnter()

end

function LuckyDrawPopup:onExit()

end

function LuckyDrawPopup:onButtonClickedEvent(name, ref)
	print("LuckyDrawPopup ======= >>> name " .. tostring(name))
	if name == "BtnGot" then
		self:onGotCallback()
	elseif name == "BtnClose" then
		self:removeFromParent()
	end
end

function LuckyDrawPopup:onGotCallback()
	--直接分享到朋友圈
	local function sharecall( isok )
        if type(isok) == "string" and isok == "true" then
            showToast(cc.Director:getInstance():getRunningScene(), "分享成功", 2)
        else
			showToast(cc.Director:getInstance():getRunningScene(), "分享失败", 2)
        end
    end

    yl.addErWeiMa()

	local framesize = cc.Director:getInstance():getOpenGLView():getFrameSize()
	local area = cc.rect(0, 0, framesize.width, framesize.height)
	local imagename = "grade_share.jpg"
	captureScreenWithArea(area, imagename, function(ok, savepath)
        if ok then
        	local savepathA = cc.FileUtils:getInstance():fullPathForFilename("client/res/share_content.png") 
            MultiPlatform:getInstance():shareToTarget(yl.ThirdParty.WECHAT_CIRCLE, sharecall, "99麻将，中奖就是这么简单，一起来玩吧！", "99麻将，中奖就是这么简单，一起来玩吧！", url, savepathA, "true", "", "", GlobalUserItem.dwUserID)         
        end
    end)
end

function LuckyDrawPopup:initCsb()
	local node =  cc.CSLoader:createNode(CSB_RES_PATH)
	self:addChild(node)

	self.rootNode = node:getChildByName("RootNode")
	self.btnGot = self.rootNode:getChildByName("BtnGot")

	local btnCallback = function (ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			self:onButtonClickedEvent(ref:getName(), ref)
		elseif eventType == ccui.TouchEventType.began then
		elseif eventType == ccui.TouchEventType.canceled then
		end
	end

	self.btnGot:addTouchEventListener(btnCallback)

	self.btnClose = self.rootNode:getChildByName("BtnClose")
	self.btnClose:addTouchEventListener(btnCallback)

	self.spGift = self.rootNode:getChildByName("SpGift")
	self.spGift:setTexture(self._path)
	self.txtUserID = self.rootNode:getChildByName("TxtUserID")
	self.txtCurrTime = self.rootNode:getChildByName("TxtCurrTime")

	self.txtUserID:setString("" .. GlobalUserItem.dwGameID)
	self.txtCurrTime:setString("" .. self._date)
	
end

return LuckyDrawPopup