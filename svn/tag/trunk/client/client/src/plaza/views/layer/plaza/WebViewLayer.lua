--
-- Author: zhong
-- Date: 2016-08-02 10:31:30
--
local WebViewLayer = class("WebViewLayer", cc.Layer)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

--访问本地html文件
WebViewLayer.LOAD_FILE = 1 
--访问url
WebViewLayer.LOAD_URL = 2

--scheme
local URL_SCHEME = "ryweb"
local JS_SCHEME = "ryweb://"
local TAG_MASK = 101
local BTN_CLOSE = 102
local BTN_COPY = 103

function WebViewLayer:ctor( scene, isActivity )
    self._scene = scene
    --加载csb资源
    local rootLayer, csbNode = ExternalFun.loadRootCSB("plaza/WebViewLayer.csb", self)
    local touchFunC = function(ref, tType)
        if tType == ccui.TouchEventType.began then
            if BTN_CLOSE == ref:getTag() then
                ref:runAction(cc.ScaleTo:create(0.1, 0.8))
            end

            if GlobalUserItem.bSoundAble then
                AudioEngine.playEffect("client/res/sound/Click.mp3", false)
            end

        elseif tType == ccui.TouchEventType.ended then
            if BTN_CLOSE == ref:getTag() then
                ref:stopAllActions()
                ref:runAction(cc.ScaleTo:create(0.1, 1.0))
            end
            self:onButtonClickedEvent(ref:getTag(), ref)            
        elseif tType == ccui.TouchEventType.canceled then
            if BTN_CLOSE == ref:getTag() then
                ref:stopAllActions()
                ref:runAction(cc.ScaleTo:create(0.1, 1.0))
            end
            
        end
    end

    -- 遮罩
    local mask = csbNode:getChildByName("panel_mask")
    mask:setTag(TAG_MASK)
    mask:addTouchEventListener( touchFunC )

    for i=1, 3 do
        local point = csbNode:getChildByName("Point_" .. i)
        point:setVisible(isActivity)
    end

    -- 背景
    self._pageView = csbNode:getChildByName("PageView")
    --注册事件
    self._pageView:addEventListener(function(sender,event)
        if event==ccui.PageViewEventType.turning then
            local index = sender:getCurrentPageIndex()
            print("index = ", index)
            for i=1, 3 do
                local point = csbNode:getChildByName("Point_" .. i)

                if index == (i-1) then
                    point:setTexture("client/res/plaza/wx_cr_checkpoint.png")
                else
                    point:setTexture("client/res/plaza/wx_cr_checkboxbg.png")
                end
            end
        end
    end)

    self._pageView:setVisible(isActivity)

    -- 退出按钮
    local closeBtn = csbNode:getChildByName("btn_close")
    closeBtn:setTag(BTN_CLOSE)
    closeBtn:addTouchEventListener(touchFunC)

    local gonggaoPanel = csbNode:getChildByName("GonggaoPanel")
    local btn = gonggaoPanel:getChildByName("CopyBtn")
    btn:setTag(BTN_COPY)
    btn:addTouchEventListener(touchFunC)

    gonggaoPanel:setVisible(not isActivity)
    if not isActivity then
        closeBtn:setPositionX(1120)
    end
end

function WebViewLayer:onButtonClickedEvent(tag, ref)
    if BTN_CLOSE == tag or TAG_MASK == tag then
        self._scene:dismissPopWait()
        self:removeFromParent()
    elseif BTN_COPY == tag then
        MultiPlatform:getInstance():copyToClipboard("jiujiumj01")
        showToast(self, "已复制到剪贴板，快去微信搜索吧", 2)
    end
end

function WebViewLayer:getSignature(times)
    print("******  WebViewLayer:getSignature ******")
    print("timevalue-"..times)
    local pstr = "3a5ke4a11fzb5c5e1" .. GlobalUserItem.dwUserID .. times
    pstr = string.lower(md5(pstr))
    print("signature-"..pstr)
    print("******  WebViewLayer:getSignature ******")
    return pstr
end

return WebViewLayer