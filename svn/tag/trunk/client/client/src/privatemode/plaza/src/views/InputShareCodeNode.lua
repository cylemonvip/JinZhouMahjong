local InputShareCodeNode = class("InputShareCodeNode", function ()
	return cc.Node:create()
end)

local CSB_RES_PATH = "client/src/privatemode/plaza/res/room/InputShareCodeNode.csb"

function InputShareCodeNode:ctor(roomRecordLayer)
	self._roomRecordLayer = roomRecordLayer
    self._clientScene = self._roomRecordLayer.scene
	self:initCsb()
end

function InputShareCodeNode:onButtonClickedEvent(name, ref)
	if name == "EnterBtn" then
		-- local textField = self.rootNode:getChildByName("TextField")
        
		local txt = self.selectCount:getText()
        txt = string.match(txt, '%d%d%d%d%d%d')
        local tips = self.rootNode:getChildByName("Tips")
        if txt then
            self.selectCount:setText(txt)
            tips:setVisible(false)
        else
            tips:setVisible(true)
            return
        end
		PriRoom:getInstance():showPopWait()
        PriRoom:getInstance():setViewFrame(self)
        PriRoom:getInstance():getNetFrame():onGetShareReplay(txt)
	elseif name == "CloseBtn" then
		self:removeFromParent()
	end
end

function InputShareCodeNode:replayGame(pData)
	local runScene = cc.Director:getInstance():getRunningScene()
    --如果没有数据，则提示不可回放
    local dwUserID = pData:readdword()
	local replayer = GameplayerManager:getInstance()
    local bSize = pData:readword()
    if bSize <= 0 then
        if nil ~= runScene then
            showToast(runScene, "该局没有记录，无法回放", 2, cc.c4b(250,0,0,255))
        end
    else --将回放数据写入文件
        --判断麻将游戏是否更新到最新
        if not self._clientScene:checkNeedUpdate() then
            if self._rommRecordLayer then
                replayer:setEnterFrom(yl.ENTER_FROM_BALLTE_RECORD)
            else
                replayer:setEnterFrom(yl.ENTER_FROM_CLUB)
            end
            replayer:clear()
            replayer:setViewUserID(dwUserID)
            --解析结果
            local analysisRet = replayer:setOriginData(pData, self._clientScene, nil)
            if not analysisRet then
                showToast(runScene, "回放文件解析错误，请稍后再试！", 2, cc.c4b(250,0,0,255))
            end
        else
            if nil ~= runScene then
                showToast(runScene, "请更新麻将游戏后，再试！", 2, cc.c4b(250,0,0,255))
            end
        end
    end
    PriRoom:getInstance():dismissPopWait()
end

function InputShareCodeNode:initCsb()
	local node =  cc.CSLoader:createNode(CSB_RES_PATH)
	self:addChild(node)

	self.rootNode = node:getChildByName("RootNode")

	local btnCallback = function (ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			self:onButtonClickedEvent(ref:getName(), ref)
		elseif eventType == ccui.TouchEventType.began then
		elseif eventType == ccui.TouchEventType.canceled then
		end
	end

	local enterBtn = self.rootNode:getChildByName("EnterBtn")
	enterBtn:addTouchEventListener(btnCallback)

	local closeBtn = self.rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btnCallback)

    local tips = self.rootNode:getChildByName("Tips")
    tips:setVisible(false)

    local textField = self.rootNode:getChildByName("TextField")
    textField:setVisible(false)

    self.selectCount = ccui.EditBox:create(cc.size(textField:getContentSize().width, textField:getContentSize().height), "client/res/blank.png", ccui.TextureResType.localType)  --输入框尺寸，背景图片
    self.selectCount:setPosition(cc.p(textField:getPositionX(), textField:getPositionY()))
    self.selectCount:setAnchorPoint(cc.p(0.5,0.5))
    self.selectCount:setFontSize(45)
    self.selectCount:setPlaceHolder("请输入分享码")
    self.selectCount:setFontColor(cc.c3b(255,255,255))
    self.selectCount:setReturnType(cc.KEYBOARD_RETURNTYPE_DONE ) --输入键盘返回类型，done，send，go等
    self.selectCount:setInputMode(cc.EDITBOX_INPUT_MODE_NUMERIC) --输入模型，如整数类型，URL，电话号码等，会检测是否符合

    self.selectCount:registerScriptEditBoxHandler(function(eventname,sender) self:editboxHandle(eventname,sender) end) --输入框的事件，主要有光标移进去，光标移出来，以及输入内容改变等
    self.rootNode:addChild(self.selectCount)
    -- self.selectCount:setHACenter() --输入的内容锚点为中心，与anch不同，anch是用来确定控件位置的，而这里是确定输入内容向什么方向展开(。。。说不清了。。自己测试一下)
end

function InputShareCodeNode:editboxHandle( strEventName, sender )
    if strEventName == "began" then

    elseif strEventName == "ended" then
    -- 当编辑框失去焦点并且键盘消失的时候被调用
    
    elseif strEventName == "return" then
        -- 当用户点击编辑框的键盘以外的区域，或者键盘的Return按钮被点击时所调用

    elseif strEventName == "changed" then
        -- 输入内容改变时调用
        print(sender:getText())
    end
end

return InputShareCodeNode