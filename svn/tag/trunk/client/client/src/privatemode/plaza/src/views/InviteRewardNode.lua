local InviteRewardNode = class("RoomRecordLayer", cc.Node)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local ClipText = require(appdf.EXTERNAL_SRC .. "ClipText")
local QueryDialog = appdf.req("client.src.plaza.views.layer.game.QueryDialog")


function InviteRewardNode:ctor(scene)
    self._scene = scene
	-- 加载csb资源
    local rootLayer, csbNode = ExternalFun.loadRootCSB("room/InviteRewardNode.csb", self)-- body
    local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.ended then
            self:onButtonClickedEvent(ref:getName(),ref)
        end
    end

    self.rootNode = csbNode:getChildByName("RootNode")

    local shareBtn  = self.rootNode:getChildByName("BtnShare")
    shareBtn:addTouchEventListener(btncallback)
    self.rewardGetBtn  = self.rootNode:getChildByName("BtnGet")
    self.rewardGetBtn:setEnabled(false)
    self.rewardGetBtn:addTouchEventListener(btncallback)

    
    local maskLayout  = csbNode:getChildByName("MaskLayout")
    maskLayout:addTouchEventListener(btncallback)

    local closeBtn  = self.rootNode:getChildByName("CloseBtn")
    closeBtn:addTouchEventListener(btncallback)

    self.itemLayout  = csbNode:getChildByName("Item")

    --邀请的人数显示
    self.invitedCountTxt = self.rootNode:getChildByName("InvitedCountTxt")
    --完成的人数显示
    self.finishedCountTxt = self.rootNode:getChildByName("FinishedCountTxt")
    --奖励的钱显示
    self.rewardTxt = self.rootNode:getChildByName("RewardTxt")

    local contenter = self.rootNode:getChildByName("Contenter")

    -- 列表
    local m_tableView = cc.TableView:create(contenter:getContentSize())
    m_tableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL) 
    m_tableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    m_tableView:setPosition(contenter:getPosition())
    m_tableView:setDelegate()
    m_tableView:registerScriptHandler(self.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX)
    m_tableView:registerScriptHandler(handler(self, self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
    m_tableView:registerScriptHandler(handler(self, self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
    self.rootNode:addChild(m_tableView, 10)
    self.m_tableView = m_tableView
    

    self:registerScriptHandler(function(eventType)
        if eventType == "enterTransitionFinish" then    
            self:onEnterTransitionFinish()
        elseif eventType == "exitTransitionStart" then 
        elseif eventType == "exit" then
        end
    end)

    self:open()
end

function InviteRewardNode:onEnterTransitionFinish()
    PriRoom:getInstance():showPopWait()
    PriRoom:getInstance():getNetFrame():onQureyUserReward()
end

function InviteRewardNode:reloadInvitedTableView()
    PriRoom:getInstance():dismissPopWait()
    local listinfo = PriRoom:getInstance().m_tabInvited
    local finishCount = 0
    for i = 1, PriRoom:getInstance().m_ShareCount do
        local info = listinfo[i]
        if info.bFinished then
            finishCount = finishCount + 1
        end
    end

    self.invitedCountTxt:setString(PriRoom:getInstance().m_ShareCount)
    self.finishedCountTxt:setString(finishCount)
    self.rewardTxt:setString((finishCount*2) .. "")

    if finishCount*2 >= 15 then
        self.rewardGetBtn:setEnabled(true)
    else
        self.rewardGetBtn:setEnabled(false)
    end

    self.m_tableView:reloadData()
end

function InviteRewardNode:reloadInvitedTableViewAfterGotReward()
    local listinfo = PriRoom:getInstance().m_tabInvited
    local count = 0
    for i = #PriRoom:getInstance().m_tabInvited, 1, -1 do
        local info = PriRoom:getInstance().m_tabInvited[i]
        if info.bFinished or info.szNickName == "" then
            print("删除 " .. i)
            table.remove(PriRoom:getInstance().m_tabInvited, i)
        else
            count = count + 1
        end
    end
    dump(PriRoom:getInstance().m_tabInvited)
    PriRoom:getInstance().m_ShareCount = #PriRoom:getInstance().m_tabInvited
    self.invitedCountTxt:setString(PriRoom:getInstance().m_ShareCount)
    self.finishedCountTxt:setString("0")
    self.rewardTxt:setString("0")

    self.rewardGetBtn:setEnabled(false)

    self.m_tableView:reloadData()
end

function InviteRewardNode:onButtonClickedEvent(name, ref)
	if name == "BtnShare" then
        self._scene:share()
	elseif name == "BtnGet" then
        PriRoom:getInstance():showPopWait()
        PriRoom:getInstance():getNetFrame():onQureyUserRewardCode()
	elseif name == "MaskLayout" then
        self:close()
	elseif name == "CloseBtn" then
        self:close()
	end
end

function InviteRewardNode:close()
    PriRoom:getInstance():setViewFrame(nil)
    self.rootNode:stopAllActions()
    self.rootNode:runAction(
        cc.Sequence:create(
            cc.ScaleTo:create(0.1, 0.1),
            cc.CallFunc:create(
                function()
                    self:removeFromParent()
                end
                )))
end

function InviteRewardNode:open()
    self.rootNode:stopAllActions()
    self.rootNode:setScale(0.1)
    self.rootNode:runAction(cc.ScaleTo:create(0.1, 1))
end

function InviteRewardNode.cellSizeForTable( view, idx )
    return 399, 70
end

function InviteRewardNode:numberOfCellsInTableView( view )
    return PriRoom:getInstance().m_ShareCount
end

function InviteRewardNode:tableCellAtIndex( view, idx )
	local cell = view:dequeueCell()
    if not cell then        
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    local info = PriRoom:getInstance().m_tabInvited[idx + 1]
    local playCount = PriRoom:getInstance().m_tabFinished[idx + 1]
    
    local item = self.itemLayout:clone()
    local nameTxt = item:getChildByName("NameTxt")
    nameTxt:setVisible(false)

    local nickName = ClipText:createClipText(cc.size(215,37), tostring(info.szNickName) , "fonts/round_body.ttf", 22)
    nickName:setTextColor(nameTxt:getTextColor())
    nickName:enableOutline(cc.c4b(0,0,0,255), 1)
    nickName:setAnchorPoint(nameTxt:getAnchorPoint())
    nickName:setPosition(nameTxt:getPosition())
    item:addChild(nickName)

    local playCountTxt = item:getChildByName("PlayCountTxt")
    playCountTxt:setString(info.bFinished and "完成" or "未完成")

	item:setPosition(0, 0)
    cell:addChild(item)

    return cell
end

function InviteRewardNode:recvRewardCodeAndUpdate(cmd_data)
    PriRoom:getInstance():dismissPopWait()
    local dwRewardCode = cmd_data.dwRewardCode
    local str = ""
    if dwRewardCode == "0" then
        str = "金额不足15元，无法领取！"
    else
        str = "您的红包验证码是: " .. dwRewardCode .. "\n请微信关注:99国粹娱乐,关注并将此验证码发送至该公众号即可领取！"
    end
    local QueryDialog = appdf.req("client.src.plaza.views.layer.game.QueryDialog")
    local query = QueryDialog:create(str)
    query:setPosition(cc.p(-appdf.WIDTH/2, appdf.HEIGHT/2))
    query:setCanTouchOutside(false)
    query:addTo(self)

    --获取邀请码成功，清除领取数据，并设置领取按钮不可用
    if dwRewardCode ~= "0" then
        self:reloadInvitedTableViewAfterGotReward()
    end
end

return InviteRewardNode
