--
-- Author: zhong
-- Date: 2016-12-20 11:48:30
--
-- 房间详情界面
local RecordDetailLayer = class("RecordDetailLayer", cc.Layer)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local ClipText = appdf.req(appdf.EXTERNAL_SRC .. "ClipText")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")


-- 详情战绩 是否结束
function RecordDetailLayer:ctor(tabDetail, clientScene, rommRecordLayer)
    self._clientScene = clientScene
    self._rommRecordLayer = rommRecordLayer
    self.m_tabInfoID = tabDetail.dwTableInfoID
    self.m_tabDesc = tabDetail.szDescribeString
    self.m_fileTag = tabDetail.dwFileTag
    self.m_roomCard = tabDetail.dwRoomCardID
    self.m_serverID = tonumber(string.sub(tostring(tabDetail.dwRoomCardID), 1, 2)) - 10
    PriRoom:getInstance().m_tabAllRecord = {}

    self:registerScriptHandler(function(eventType)
        if eventType == "enterTransitionFinish" then    
            self:onEnterTransitionFinish()
        elseif eventType == "exitTransitionStart" then 
        elseif eventType == "exit" then
        end
    end)

    -- 加载csb资源
    local rootLayer, csbNode = ExternalFun.loadRootCSB("room/RecordDetailLayout.csb", self)
    self.m_csbNode = csbNode

    local touchFunC = function(ref, tType)
        if tType == ccui.TouchEventType.began then
            if GlobalUserItem.bSoundAble then
                AudioEngine.playEffect("client/res/sound/Click.mp3", false)
            end
        elseif tType == ccui.TouchEventType.ended then
            self:onButtonClickedEvent(ref:getName(), ref)            
        end
    end

    -- 遮罩
    local mask = csbNode:getChildByName("MaskLayout")
    mask:addTouchEventListener( touchFunC )

    self.rootNode = csbNode:getChildByName("RootNode")
    self.rootNode:setScale(0.1)
    self.rootNode:runAction(cc.ScaleTo:create(0.2, 1.0))
    print("self.m_tabDesc = ", tostring(self.m_tabDesc))

    local nameArray = self:split(self.m_tabDesc, "|")
    dump(nameArray, "------------nameArray 1------------")
    nameArray = self:split(nameArray[2], ",")
    dump(nameArray, "------------nameArray 2------------")
    for i=1, 4 do
        local nameTextName = "NameText" .. i
        local nameText = self.rootNode:getChildByName(nameTextName)
        nameText:setVisible(false)
    end

    for i=1, #nameArray do
        local nameTextName = "NameText" .. i
        local nameText = self.rootNode:getChildByName(nameTextName)
        nameText:setString(nameArray[i])
        nameText:setVisible(true)
    end
    
    local closeBtn = self.rootNode:getChildByName("BtnBack")
    closeBtn:addTouchEventListener( touchFunC )
    --单项
    self.itemLayout = csbNode:getChildByName("ItemLayout")
    print("self.itemLayout === " .. tostring(self.itemLayout))
    
    -- 列表
    local m_tableView = cc.TableView:create(cc.size(1160, 530))
    m_tableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
    m_tableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    m_tableView:setPosition(-575, -340)
    m_tableView:setDelegate()
    m_tableView:registerScriptHandler(self.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX)
    m_tableView:registerScriptHandler(handler(self, self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
    m_tableView:registerScriptHandler(handler(self, self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
    csbNode:addChild(m_tableView, 10)
    -- m_tableView:setBackgroundColor(c3b())
    self.m_tableView = m_tableView
    -- self.m_tableView:reloadData()

    local replayInfoLayout = csbNode:getChildByName("ReplayInfoLayout")
    self.tfFangka = replayInfoLayout:getChildByName("TFFangka")
    self.tfServerFlag = replayInfoLayout:getChildByName("TFServerFlag")
    self.tfRound = replayInfoLayout:getChildByName("TFRound")
    self.tfServerid = replayInfoLayout:getChildByName("TFServerid")
    self.tfUserID = replayInfoLayout:getChildByName("TFUserID")

    if yl.rpRoomCardID ~= nil then
        self.tfFangka:setString(yl.rpRoomCardID)
    end

    if yl.rpFlagID ~= nil then
        self.tfServerFlag:setString(yl.rpFlagID)
    end

    if yl.rpGameRound ~= nil then
        self.tfRound:setString(yl.rpGameRound)
    end
    
    if yl.serverID ~= nil then
        self.tfServerid:setString(yl.serverID)
    end
    
    if yl.rpUserID ~= nil then
        self.tfUserID:setString(yl.rpUserID)
    end
    
    local btnReset = replayInfoLayout:getChildByName("BtnReset")
    btnReset:addTouchEventListener(touchFunC)
    local btnSure = replayInfoLayout:getChildByName("BtnSure")
    btnSure:addTouchEventListener(touchFunC)

    if appdf.isDebug ~= 1 then
        replayInfoLayout:setVisible(false)
    end
end

function RecordDetailLayer:onEnterTransitionFinish()
    PriRoom:getInstance():showPopWait()
    PriRoom:getInstance():setViewFrame(self)
    PriRoom:getInstance():getNetFrame():onQuerySubScoreList(self.m_tabInfoID)
end

function RecordDetailLayer:onReloadDetailRecordList()
    self:onReloadData()
end

function RecordDetailLayer:replayGame(pData)
    local runScene = cc.Director:getInstance():getRunningScene()
    local dwUserID = pData:readdword()
    local replayer = GameplayerManager:getInstance()
    print("RecordDetailLayer replayGame dwUserID = ", dwUserID)
    --如果没有数据，则提示不可回放
    local bSize = pData:readword()
    if bSize <= 0 then
        if nil ~= runScene then
            showToast(runScene, "该局没有记录，无法回放", 2, cc.c4b(250,0,0,255))
        end
    else --将回放数据写入文件
        --判断麻将游戏是否更新到最新
        if not self._clientScene:checkNeedUpdate() then
            
            if self._rommRecordLayer then
                replayer:setEnterFrom(yl.ENTER_FROM_BALLTE_RECORD)
            else
                replayer:setEnterFrom(yl.ENTER_FROM_CLUB)
            end
            replayer:clear()
            replayer:setViewUserID(dwUserID)
            --解析结果
            local analysisRet = replayer:setOriginData(pData, self._clientScene, self.m_roomCard)
            if not analysisRet then
                showToast(runScene, "回放文件解析错误，请稍后再试！", 2, cc.c4b(250,0,0,255))
            end
        else
            if nil ~= runScene then
                showToast(runScene, "请更新麻将游戏后，再试！", 2, cc.c4b(250,0,0,255))
            end
        end
    end
    PriRoom:getInstance():dismissPopWait()
end

function RecordDetailLayer.cellSizeForTable( view, idx )
    return 1150, 120
end

function RecordDetailLayer:numberOfCellsInTableView( view )
    return #PriRoom:getInstance().m_tabAllRecord
end

function RecordDetailLayer:split( str, reps )
    local resultStrList = {}
    string.gsub(str,'[^'..reps..']+',function ( w )
        table.insert(resultStrList,w)
    end)
    return resultStrList
end

function RecordDetailLayer:tableCellAtIndex( view, idx )
    local cell = view:dequeueCell()
    if not cell then        
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    local tabData = PriRoom:getInstance().m_tabAllRecord[idx + 1]
    for k,v in pairs(tabData) do
        print(k,v)
    end
    local item = self.itemLayout:clone()

    dump(tabData)
    --回放按钮
    local replayBtn = item:getChildByName("BtnReplay")
    replayBtn:addTouchEventListener(function(ref, tType)
        if tType == ccui.TouchEventType.ended then
            print("回放参数：", tabData.dwGameRound, self.m_fileTag, self.m_roomCard, self.m_serverID)
            PriRoom:getInstance():showPopWait()
            PriRoom:getInstance():setViewFrame(self)
            PriRoom:getInstance():getNetFrame():onGetReplayFile(tabData.dwGameRound, self.m_fileTag, self.m_roomCard, self.m_serverID)
        end
    end)

    --回放按钮
    local btnShare = item:getChildByName("BtnShare")
    btnShare:addTouchEventListener(function(ref, tType)
        if tType == ccui.TouchEventType.ended then
            print("回放参数：", tabData.dwGameRound, self.m_fileTag, self.m_roomCard, self.m_serverID)
            PriRoom:getInstance():getPlazaScene():popTargetShare(function(target, bMyFriend)
                bMyFriend = bMyFriend or false
                local function sharecall( isok )
                    if type(isok) == "string" and isok == "true" then
                        showToast(self, "分享成功", 2)
                    end
                end

                local shareTxt = "我的回放码是：" .. tabData.dwShareId .. "，欢迎观看！"
                local gameID = 389
                local url = ""--yl.WXShareURL or GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
                MultiPlatform:getInstance():shareToTarget(target, sharecall, "", shareTxt, url, "", false, gameID, "", GlobalUserItem.dwUserID)
            end)
        end
    end)

    local fangIDText = item:getChildByName("FangIDText")
    fangIDText:setString(tabData.dwGameRound)

    for i = 1, 4 do
        local name = "score" .. i
        local scoreText = item:getChildByName(name)
        scoreText:setVisible(false)
    end

    local subStr = self:split(tabData.szDescribeString, ",")
    for i = 1, #subStr do
        local name = "score" .. i
        local scoreText = item:getChildByName(name)
        scoreText:setString(subStr[i])
        scoreText:setVisible(true)
    end

    local tabTime = tabData.sysCreateTime
    local strTime = string.format("%d-%02d-%02d %02d:%02d:%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay, tabTime.wHour, tabTime.wMinute, tabTime.wSecond)
    local timeText = item:getChildByName("TimeText")
    timeText:setString(strTime)
    item:setPosition(0, 0)
    cell:addChild(item)

    return cell
end

function RecordDetailLayer:onReloadData()
    self.m_tableView:reloadData()
end

function RecordDetailLayer:onButtonClickedEvent( name, sender)

    if name == "MaskLayout" or name == "BtnBack" then
        --self._rommRecordLayer 可能为 nil  但是不影响
        PriRoom:getInstance():setViewFrame(self._rommRecordLayer)
        self.m_csbNode:runAction(
            cc.Sequence:create(
                cc.ScaleTo:create(0.1, 0.1),
                cc.CallFunc:create(function()
                    self:removeFromParent()
                end)
                )
            )
    elseif name == "BtnReset" then
        print("BtnReset called")
        yl.rpRoomCardID = nil
        yl.rpFlagID = nil
        yl.rpGameRound = nil
        yl.serverID = nil
        yl.rpUserID = nil
        self.tfFangka:setString("")
        self.tfServerFlag:setString("")
        self.tfRound:setString("")
        self.tfServerid:setString("")
        self.tfUserID:setString("")
    elseif name == "BtnSure" then
        print("BtnSure called")
        yl.rpRoomCardID = tonumber(self.tfFangka:getString())
        yl.rpFlagID = tonumber(self.tfServerFlag:getString())
        yl.rpGameRound = tonumber(self.tfRound:getString())
        yl.serverID = tonumber(self.tfServerid:getString())
        yl.rpUserID = tonumber(self.tfUserID:getString())
    end 
end

return RecordDetailLayer