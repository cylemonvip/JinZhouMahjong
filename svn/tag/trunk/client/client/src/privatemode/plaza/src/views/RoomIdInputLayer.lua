--
-- Author: zhong
-- Date: 2016-12-17 09:39:30
--
-- 私人房 ID 输入界面

local RoomIdInputLayer = class("RoomIdInputLayer", cc.Layer)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Utils = appdf.req(appdf.CLIENT_SRC .. "utils.Utils")
local cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "CMD_Private")

local BTN_CLEAR = 1
local BTN_DEL = 2
local BTN_OK = 3
local BTN_CANCEL = 4

function RoomIdInputLayer:ctor()
    -- 注册触摸事件
    -- ExternalFun.registerTouchEvent(self, true)

    self.roomIDArray = {}

    -- 加载csb资源
    local rootLayer, csbNode = ExternalFun.loadRootCSB("room/RoomIdInputLayer.csb", self)

    self.m_spBg = csbNode:getChildByName("pri_sp_ideditbg")
    self.m_spBg:setScale(0.000001)
    local bg = self.m_spBg

    self:clearRoomInfo()

    local closeBtn = bg:getChildByName("CloseBtn")
    closeBtn:addTouchEventListener(function(ref, tType)
        if tType == ccui.TouchEventType.ended then
            self:runAction(
                cc.Sequence:create(
                    cc.ScaleTo:create(0.1, 0.1),
                    cc.RemoveSelf:create()
                    )
                )
        end
    end)

    self.m_atlasRoomId = bg:getChildByName("atlas_roomid")
    self.m_atlasRoomId:setString("")
    
    -- 房间ID
    for i = 1, 6 do
        local index = i - 1
        local atlasID = bg:getChildByName("atlas_roomid_" .. index)
        atlasID:setString("")
    end

    local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
            if GlobalUserItem.bSoundAble then
                AudioEngine.playEffect("client/res/sound/Click.mp3", false)
            end
            ref:stopAllActions()
            ref:runAction(cc.ScaleTo:create(0.1, 0.9))
        elseif tType == ccui.TouchEventType.ended then
            ref:stopAllActions()
            ref:runAction(cc.ScaleTo:create(0.1, 1.0))
            self:onNumButtonClickedEvent(ref:getTag(),ref)
        elseif tType == ccui.TouchEventType.canceled then
            ref:stopAllActions()
            ref:runAction(cc.ScaleTo:create(0.1, 1.0))
        end
    end
    -- 数字按钮
    for i = 1, 10 do
        local tag = i - 1
        local btn = bg:getChildByName("btn_num" .. tag)
        btn:setTag(tag)
        btn:addTouchEventListener(btncallback)
    end

    local function callback(ref, tType)
        if tType == ccui.TouchEventType.began then
            if GlobalUserItem.bSoundAble then
                AudioEngine.playEffect("client/res/sound/Click.mp3", false)
            end
            if BTN_OK ~= ref:getTag() and BTN_CANCEL ~= ref:getTag() then
                ref:stopAllActions()
                ref:runAction(cc.ScaleTo:create(0.1, 0.9))
            end
            
        elseif tType == ccui.TouchEventType.ended then
            if BTN_OK ~= ref:getTag() and BTN_CANCEL ~= ref:getTag() then
                ref:stopAllActions()
                ref:runAction(cc.ScaleTo:create(0.1, 1.0))
            end
            self:onButtonClickedEvent(ref:getTag(),ref)
        elseif tType == ccui.TouchEventType.canceled then
            if BTN_OK ~= ref:getTag() and BTN_CANCEL ~= ref:getTag() then
                ref:stopAllActions()
                ref:runAction(cc.ScaleTo:create(0.1, 1.0))
            end
        end
    end
    -- 清除按钮
    local btn = bg:getChildByName("btn_clear")
    btn:setTag(BTN_CLEAR)
    btn:addTouchEventListener(callback)

    -- 删除按钮
    btn = bg:getChildByName("btn_del")
    btn:setTag(BTN_DEL)
    btn:addTouchEventListener(callback)

    btn = bg:getChildByName("BtnOk")
    btn:setTag(BTN_OK)
    btn:addTouchEventListener(callback)

    btn = bg:getChildByName("BtnCancel")
    btn:setTag(BTN_CANCEL)
    btn:addTouchEventListener(callback)

    -- 加载动画
    local call = cc.CallFunc:create(function()
        self:setVisible(true)
    end)
    local scale = cc.ScaleTo:create(0.2, 1.0)
    self.m_actShowAct = cc.Sequence:create(call, scale)
    ExternalFun.SAFE_RETAIN(self.m_actShowAct)

    local scale1 = cc.ScaleTo:create(0.2, 0.0001)
    local call1 = cc.CallFunc:create(function( )
        self:setVisible(false)
    end)
    self.m_actHideAct = cc.Sequence:create(scale1, call1)
    ExternalFun.SAFE_RETAIN(self.m_actHideAct)

    self:setVisible(false)
end

function RoomIdInputLayer:clearRoomInfo()
    local roomInfoLayout = self.m_spBg:getChildByName("RoomInfoLayout")

    
    local txtTitle = roomInfoLayout:getChildByName("TxtTitle")

    local txtQuanShu = roomInfoLayout:getChildByName("TxtQuanShu")
    local txtRenShu = roomInfoLayout:getChildByName("TxtRenShu")
    local txtHostName = roomInfoLayout:getChildByName("TxtHostName")
    local txtHostID = roomInfoLayout:getChildByName("TxtHostID")
    local txtPlayerName_1 = roomInfoLayout:getChildByName("TxtPlayerName_1")
    local txtPlayerName_2 = roomInfoLayout:getChildByName("TxtPlayerName_2")
    local txtPlayerName_3 = roomInfoLayout:getChildByName("TxtPlayerName_3")
    local txtPlayerName_4 = roomInfoLayout:getChildByName("TxtPlayerName_4")
    
    local txtPlayerID_1 = roomInfoLayout:getChildByName("TxtPlayerID_1")
    local txtPlayerID_2 = roomInfoLayout:getChildByName("TxtPlayerID_2")
    local txtPlayerID_3 = roomInfoLayout:getChildByName("TxtPlayerID_3")
    local txtPlayerID_4 = roomInfoLayout:getChildByName("TxtPlayerID_4")

    local txtRule = roomInfoLayout:getChildByName("TxtRule")

    txtTitle:setString("")

    txtQuanShu:setString("圈数:")
    txtRenShu:setString("人数:")
    txtHostName:setString("房主:")
    txtHostID:setString("ID:")
    txtPlayerName_1:setString("玩家:")
    txtPlayerName_2:setString("玩家:")
    txtPlayerName_3:setString("玩家:")
    txtPlayerName_4:setString("玩家:")
    txtPlayerID_1:setString("ID:")
    txtPlayerID_2:setString("ID:")
    txtPlayerID_3:setString("ID:")
    txtPlayerID_4:setString("ID:")
    txtRule:setString("玩法:")
end

function RoomIdInputLayer:updateDisplayRoomID()

    local roomIDStr = self.m_atlasRoomId:getString()
    for i = 1,6 do
        local index = i - 1
        local atlasID = self.m_spBg:getChildByName("atlas_roomid_" .. index)
        atlasID:setString("")
    end

    print("roomIDStr len = " .. string.len(roomIDStr))
    for i = 1, string.len(roomIDStr) do
        local index = i - 1
        local atlasID = self.m_spBg:getChildByName("atlas_roomid_" .. index)
        print("roomIDStr[" .. i .. "] = " .. string.sub(roomIDStr,i,i))
        atlasID:setString(string.sub(roomIDStr,i,i))
    end

end

function RoomIdInputLayer:pushARoomID(num)
    table.insert(self.roomIDArray, num) 
    self:updateDisplayRoomID()
end

function RoomIdInputLayer:popARoomID()
    table.remove(self.roomIDArray, #self.roomIDArray)
    self:updateDisplayRoomID()
end

function RoomIdInputLayer:showLayer( var )
    local ani = nil
    if var then
        ani = self.m_actShowAct

        local copyId = string.gsub(GlobalUserItem.szCopyRoomId,"([^0-9])","")
        if "" ~= copyId and 6 == string.len(copyId) then
            self.m_atlasRoomId:setString(copyId)
            self:updateDisplayRoomID()
            PriRoom:getInstance():showPopWait()
            self:runAction(cc.Sequence:create(cc.DelayTime:create(1.5), cc.CallFunc:create(function()
                self:showLayer(false)
                -- PriRoom:getInstance():getNetFrame():onSearchRoom(copyId)
                PriRoom:getInstance().m_client:onStartGameByRoomId(copyId)
                self.m_atlasRoomId:setString("")
                self:updateDisplayRoomID()
                GlobalUserItem.szCopyRoomId = ""
            end)))
        else
            self.m_atlasRoomId:setString("")
            self:updateDisplayRoomID()
        end
    else 
        ani = self.m_actHideAct

        self:clearRoomInfo()
        PriRoom:getInstance():setViewFrame(nil)
    end

    if nil ~= ani then
        self.m_spBg:stopAllActions()
        self.m_spBg:runAction(ani)
    end
end

function RoomIdInputLayer:onExit()
    ExternalFun.SAFE_RELEASE(self.m_actShowAct)
    self.m_actShowAct = nil
    ExternalFun.SAFE_RELEASE(self.m_actHideAct)
    self.m_actHideAct = nil

    self:clearRoomInfo()

    PriRoom:getInstance():setViewFrame(nil)
end

function RoomIdInputLayer:onTouchBegan(touch, event)
    return self:isVisible()
end

function RoomIdInputLayer:onTouchEnded(touch, event)
    local pos = touch:getLocation() 
    local m_spBg = self.m_spBg
    pos = m_spBg:convertToNodeSpace(pos)
    local rec = cc.rect(0, 0, m_spBg:getContentSize().width, m_spBg:getContentSize().height)
    if false == cc.rectContainsPoint(rec, pos) then
        self:showLayer(false)
        local roomid = self.m_atlasRoomId:getString()
        if string.len(roomid) == 6 then
            PriRoom:getInstance():showPopWait()
            -- PriRoom:getInstance():getNetFrame():onSearchRoom(roomid)
            PriRoom:getInstance().m_client:onStartGameByRoomId(roomid)
            self.m_atlasRoomId:setString("")
            self:updateDisplayRoomID()
        end
    end
end

function RoomIdInputLayer:splitStr(szFullString, szSeparator)  
    local nFindStartIndex = 1  
    local nSplitIndex = 1  
    local nSplitArray = {}  
    while true do  
       local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)  
       if not nFindLastIndex then  
        nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))  
        break  
       end  
       nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)  
       nFindStartIndex = nFindLastIndex + string.len(szSeparator)  
       nSplitIndex = nSplitIndex + 1  
    end  
    return nSplitArray  
end  

function RoomIdInputLayer:onRoomInfo(pData)
    local data = ExternalFun.read_netdata(cmd_private.game.CMD_GR_User_Info_Result, pData)
    dump(data, "RoomIdInputLayer:onRoomInfo")
    local roomInfoLayout = self.m_spBg:getChildByName("RoomInfoLayout")

    local txtTitle = roomInfoLayout:getChildByName("TxtTitle")

    local txtQuanShu = roomInfoLayout:getChildByName("TxtQuanShu")
    local txtRenShu = roomInfoLayout:getChildByName("TxtRenShu")
    local txtHostName = roomInfoLayout:getChildByName("TxtHostName")
    local txtHostID = roomInfoLayout:getChildByName("TxtHostID")
    local txtPlayerName_1 = roomInfoLayout:getChildByName("TxtPlayerName_1")
    local txtPlayerName_2 = roomInfoLayout:getChildByName("TxtPlayerName_2")
    local txtPlayerName_3 = roomInfoLayout:getChildByName("TxtPlayerName_3")
    local txtPlayerName_4 = roomInfoLayout:getChildByName("TxtPlayerName_4")
    
    local txtPlayerID_1 = roomInfoLayout:getChildByName("TxtPlayerID_1")
    local txtPlayerID_2 = roomInfoLayout:getChildByName("TxtPlayerID_2")
    local txtPlayerID_3 = roomInfoLayout:getChildByName("TxtPlayerID_3")
    local txtPlayerID_4 = roomInfoLayout:getChildByName("TxtPlayerID_4")

    local txtRule = roomInfoLayout:getChildByName("TxtRule")

    if data.cbGameStatus == 2 then
        showToast(cc.Director:getInstance():getRunningScene() , "房间号不存在，请重新输入", 2)
        return
    end

    txtQuanShu:setString("圈数:" .. data.cbAllCount .. "圈")
    txtRenShu:setString("人数:" .. data.cbUserCount .. "人")

    local roomIdStr = self.m_atlasRoomId:getString()
    local serverId = tonumber(string.sub(tostring(roomIdStr), 1, 2)) - 10

    local nKind = GlobalUserItem.getKindIdByServerId(serverId)
    if tonumber(nKind) == 389 then
        if data.cbGameRule[1][2] == 3 then
            txtTitle:setString("三人麻将")
        else
            txtTitle:setString("四人麻将")
        end
    elseif tonumber(nKind) == 200 then
        txtTitle:setString("斗地主")
    end

    if tonumber(nKind) ~= GlobalUserItem.nCurGameKind then
        local otherGameName = ""
        if tonumber(nKind) == 389 then
            otherGameName = "锦州麻将"
        elseif tonumber(nKind) == 200 then
            otherGameName = "斗地主"
        elseif tonumber(nKind) == 511 then
            otherGameName = "打屁"
        elseif tonumber(nKind) == 27 then
            otherGameName = "牛牛"
        else
            otherGameName = "未知游戏"
        end

        local runScene = cc.Director:getInstance():getRunningScene()
        showToast(runScene, "请先选择<" .. otherGameName .. ">游戏后，再加入房间！！！", 3)
    end

    data.dwGameID[1][1] = data.dwGameID[1][1] ~= 0 and data.dwGameID[1][1] or ""
    data.dwGameID[1][2] = data.dwGameID[1][2] ~= 0 and data.dwGameID[1][2] or ""
    data.dwGameID[1][3] = data.dwGameID[1][3] ~= 0 and data.dwGameID[1][3] or ""
    data.dwGameID[1][4] = data.dwGameID[1][4] ~= 0 and data.dwGameID[1][4] or ""
    data.dwGameID[1][5] = data.dwGameID[1][5] ~= 0 and data.dwGameID[1][5] or ""
    
    txtHostID:setString("ID:" .. tostring(data.dwGameID[1][1]))
    txtPlayerID_1:setString("ID:" .. tostring(data.dwGameID[1][2]))
    txtPlayerID_2:setString("ID:" .. tostring(data.dwGameID[1][3]))
    txtPlayerID_3:setString("ID:" .. tostring(data.dwGameID[1][4]))
    txtPlayerID_4:setString("ID:" .. tostring(data.dwGameID[1][5]))

    --麻将
    local ruleStr = ""
    if GlobalUserItem.nCurGameKind == 389 then
        if data.cbGameRule[1][3] == 1 then
            ruleStr = ruleStr .. "天地胡"
        end

        if data.cbGameRule[1][4] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "会牌"
            else
                ruleStr = ruleStr .. "、会牌"
            end
            
        end

        if data.cbGameRule[1][5] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "穷胡加倍"
            else
                ruleStr = ruleStr .. "、穷胡加倍"    
            end
            
        end

        if data.cbGameRule[1][6] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "封顶150"
            else
                ruleStr = ruleStr .. "、封顶150"
            end
            
        end

        if data.cbGameRule[1][7] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "三清"
            else
                ruleStr = ruleStr .. "、三清"
            end
            
        end

        if data.cbGameRule[1][8] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "七对"
            else
                ruleStr = ruleStr .. "、七对"
            end
            
        end

        if data.cbGameRule[1][9] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "清一色"
            else
                ruleStr = ruleStr .. "、清一色"
            end
            
        end

        if data.cbGameRule[1][10] == 1 then
            if ruleStr == "" then
                ruleStr = ruleStr .. "封顶200"
            else
                ruleStr = ruleStr .. "、封顶200"
            end
            
        end
    end

    txtRule:setString("玩法:" .. ruleStr)

    if data.szNickName ~= nil then
        local list = self:splitStr(data.szNickName, "|")

        list[1] = list[1] or ""
        list[2] = list[2] or ""
        list[3] = list[3] or ""
        list[4] = list[4] or ""
        list[5] = list[5] or ""

        local hostNameStr = Utils.stringEllipsis("房主:" .. tostring(list[1]), txtHostName:getFontSize(), txtHostName:getContentSize().width, "...")
        local playerNameStr_1 = Utils.stringEllipsis("玩家:" .. tostring(list[2]), txtPlayerName_1:getFontSize(), txtPlayerName_1:getContentSize().width, "...")
        local playerNameStr_2 = Utils.stringEllipsis("玩家:" .. tostring(list[3]), txtPlayerName_2:getFontSize(), txtPlayerName_2:getContentSize().width, "...")
        local playerNameStr_3 = Utils.stringEllipsis("玩家:" .. tostring(list[4]), txtPlayerName_3:getFontSize(), txtPlayerName_3:getContentSize().width, "...")
        local playerNameStr_4 = Utils.stringEllipsis("玩家:" .. tostring(list[5]), txtPlayerName_4:getFontSize(), txtPlayerName_4:getContentSize().width, "...")

        txtHostName:setString(hostNameStr)

        txtPlayerName_1:setString(playerNameStr_1)
        txtPlayerName_2:setString(playerNameStr_2)
        txtPlayerName_3:setString(playerNameStr_3)
        txtPlayerName_4:setString(playerNameStr_4)
    end
end

function RoomIdInputLayer:onOkBtnCallback()
    local roomid = self.m_atlasRoomId:getString()
    if string.len(roomid) == 6 then
        --cc.DelayTime:create(1.0), 
        self:runAction(cc.Sequence:create(cc.CallFunc:create(function()
            self:showLayer(false)
            --[[
                加入房间流程: 
                    1.搜索房间信息（连登录服务器socket，后sendSearchRoom() ）
                    2.搜索房间结果返回 onSubSearchRoomResult() 拿到dwTableID 和 dwServerID
                    3.执行priRoom 的 onLoginRoom() 中通过 dwServerID 找对应 ip port数组的下标 然后调用 client的 onStartGame()
                    4.onStartGame() 先关闭游戏服socket 然后调用 onLogonRoom() 登录房间
                    5.onLogonRoom() 通过房间ip port连接服务器
                    6.连接成功，onConnectCompeleted() 中发送 yl.MDM_GR_LOGON,yl.SUB_GR_LOGON_MOBILE
                    7.收到服务器消息 game_cmd.SUB_GR_LOGON_FINISH 执行 onSocketLogonFinish()
                    8.执行 priRoom 的 onLoginPriRoomFinish 进行搜索后 调用sendEnterPrivateGame() 进入私人房
                    9.执行 SitDown 发送要坐的位置 及 游戏选项 SendGameOption(感觉没用)
                    10.状态改变，进入房间
            ]]
            PriRoom:getInstance().m_client:onStartGameByRoomId(roomid)

            -- PriRoom:getInstance():getNetFrame():onSearchRoom(roomid)
            -- self.m_atlasRoomId:setString("")
            -- self:updateDisplayRoomID()
            -- GlobalUserItem.szCopyRoomId = ""
        end)))
    else
        showToast(cc.Director:getInstance():getRunningScene() , "请输入六位房间号", 2)
    end
end


function RoomIdInputLayer:onNumButtonClickedEvent( tag, sender )

    local roomid = self.m_atlasRoomId:getString()
    if string.len(roomid) < 6 then
        roomid = roomid .. tag
        self.m_atlasRoomId:setString(roomid)
        self:updateDisplayRoomID()
    end

    if string.len(roomid) == 6 then
        PriRoom:getInstance():showPopWait()
        PriRoom:getInstance():setViewFrame(self)
        PriRoom:getInstance():getNetFrame():queryTableInfo(roomid, false)
    end
end

function RoomIdInputLayer:onButtonClickedEvent( tag, sender )
    if BTN_CLEAR == tag then
        self.m_atlasRoomId:setString("")
        self:updateDisplayRoomID()
    elseif BTN_DEL == tag then
        local roomid = self.m_atlasRoomId:getString()
        local len = string.len(roomid)
        if len > 0 then
            roomid = string.sub(roomid, 1, len - 1)
        end
        self.m_atlasRoomId:setString(roomid)    
        self:updateDisplayRoomID()    
    elseif BTN_OK == tag then
        self:onOkBtnCallback()
    elseif BTN_CANCEL == tag then
        self:runAction(
            cc.Sequence:create(
                cc.ScaleTo:create(0.1, 0.1),
                cc.RemoveSelf:create()
                )
            )
    end
end

return RoomIdInputLayer