--
-- Author: zhong
-- Date: 2016-12-17 10:32:26
--
-- 房间记录界面

local RoomRecordLayer = class("RoomRecordLayer", cc.Layer)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local ClipText = appdf.req(appdf.EXTERNAL_SRC .. "ClipText")
local RoomDetailLayer = appdf.req(PriRoom.MODULE.PLAZAMODULE .. "views.RoomDetailLayer")
local RecordDetailLayer = appdf.req(PriRoom.MODULE.PLAZAMODULE .. "views.RecordDetailLayer")
local cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "CMD_Private")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")
local InputShareCodeNode = appdf.req(PriRoom.MODULE.PLAZAMODULE .. "views.InputShareCodeNode")


local ROOMDETAIL_NAME = "__pri_room_detail_layer_name__"
function RoomRecordLayer:ctor( scene, param )
    ExternalFun.registerNodeEvent(self)

    if type(param) == 'table' then
        for k,v in pairs(param) do
            print("RoomRecordLayer param ===== >>>> ", k,v)
        end
    end

    --是否为显示战绩
    self._isZhanJiCalled = false
    if param then
        self._isZhanJiCalled = param[1]
    end 

    print("self._isZhanJiCalled = " .. tostring(self._isZhanJiCalled))

    --点击查看详情的数据
    self._clickTabData = nil

    self.scene = scene
    
    -- 加载csb资源
    local rootLayer, csbNode = ExternalFun.loadRootCSB("room/RecordLayer.csb", self)

    local cbtlistener = function (sender,eventType)
        self:onSelectedEvent(sender:getTag(),sender,eventType)
    end

    local myRoomBgSprite = csbNode:getChildByName("MyRoomBgSprite")
    if self._isZhanJiCalled then
        myRoomBgSprite:setVisible(false)
    else 
        myRoomBgSprite:setVisible(true)
    end

    self.itemLayout = csbNode:getChildByName("ItemLayout")
    self.itemLayoutCreate = csbNode:getChildByName("ItemLayout_create")

    local content = csbNode:getChildByName("content")
    -- 列表
    local m_tableView = cc.TableView:create(content:getContentSize())
    m_tableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
    m_tableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
    m_tableView:setPosition(content:getPosition())
    m_tableView:setDelegate()
    m_tableView:registerScriptHandler(self.cellSizeForTable, cc.TABLECELL_SIZE_FOR_INDEX)
    m_tableView:registerScriptHandler(handler(self, self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
    m_tableView:registerScriptHandler(handler(self, self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
    csbNode:addChild(m_tableView)
    self.m_tableView = m_tableView
    content:removeFromParent()

    local btcallback = function(ref, type)
        if type == ccui.TouchEventType.began then
            if GlobalUserItem.bSoundAble then
                AudioEngine.playEffect("client/res/sound/Click.mp3", false)
            end
        elseif type == ccui.TouchEventType.ended then
            if ref:getName() == "BtnBack" then
                PriRoom:getInstance():setViewFrame(nil)
                self.scene._sceneRecord = {}
                self.scene._sceneRecord[1] = yl.SCENE_GAMELIST
                self:removeFromParent()
            elseif ref:getName() == "ScanOtherReplayBtn" then
                local inputShareCodeNode = InputShareCodeNode:create(self)
                inputShareCodeNode:setPosition(yl.WIDTH/2, yl.HEIGHT/2)
                self:addChild(inputShareCodeNode)
            end

        end
    end
    local btnBack = csbNode:getChildByName("BtnBack")
    btnBack:addTouchEventListener(btcallback)

    --查看他人回放
    local scanOtherReplayBtn = csbNode:getChildByName("ScanOtherReplayBtn")
    scanOtherReplayBtn:addTouchEventListener(btcallback)
    
    
    -- 房间信息
    self.m_layRoomDetail = nil
end

function RoomRecordLayer:onSelectedEvent( tag,sender,eventType )
    local sel = sender:isSelected()
    self.m_layCreateRec:setVisible(sel)
    self.m_layJoinRec:setVisible(not sel)

    if not sel and 0 == #(PriRoom:getInstance().m_tabJoinRecord) then
        PriRoom:getInstance():showPopWait()
        PriRoom:getInstance():getNetFrame():onQueryJoinList()
        return
    end
    self.m_tableView:reloadData()
end

function RoomRecordLayer:onEnterTransitionFinish()
    PriRoom:getInstance():showPopWait()
    if self._isZhanJiCalled then
        PriRoom:getInstance():showPopWait()
        PriRoom:getInstance():getNetFrame():onQueryJoinList()
    else
        -- 请求记录列表
        PriRoom:getInstance():getNetFrame():onQueryRoomList()
    end
end

function RoomRecordLayer:onExit()
    -- 清除缓存
    PriRoom:getInstance().m_tabJoinRecord = {}
    PriRoom:getInstance().m_tabCreateRecord = {}
end

function RoomRecordLayer:onReloadRecordList()
    local rd = self:getChildByName(ROOMDETAIL_NAME)
    self.m_tableView:reloadData() 
    if nil ~= rd then
        rd:hide()
    end
end

function RoomRecordLayer:onRoomInfo(pData)
    local cmd_table = ExternalFun.read_netdata(cmd_private.game.CMD_GR_User_Info_Result, pData)
    local popup = self:getChildByName(ROOMDETAIL_NAME)
    if popup then
        popup:setData(cmd_table)
    end
    
end

function RoomRecordLayer.cellSizeForTable( view, idx )
    return 1150,150
end

function RoomRecordLayer:numberOfCellsInTableView( view )
    if self._isZhanJiCalled then
        return #(PriRoom:getInstance().m_tabJoinRecord)
    else
        return #(PriRoom:getInstance().m_tabCreateRecord)
    end
end

function RoomRecordLayer:tableCellAtIndex( view, idx )
    local cell = view:dequeueCell()
    if not cell then        
        cell = cc.TableViewCell:new()
    else
        cell:removeAllChildren()
    end

    if self._isZhanJiCalled then
        local tabData = PriRoom:getInstance().m_tabJoinRecord[idx + 1]
        local item = self:joinRecordItem(tabData, idx)
        item:setPosition(0, 0)
        cell:addChild(item)
    else
        local tabData = PriRoom:getInstance().m_tabCreateRecord[idx + 1]
        local item = self:createRecordItem(tabData, idx)
        item:setPosition(0, 0)
        cell:addChild(item)
    end

    return cell
end

-- 创建记录
function RoomRecordLayer:createRecordItem( tabData , idx)
    local item = self.itemLayoutCreate:clone()
    local indexText = item:getChildByName("IndexText")
    local indexStr = idx + 1
    indexText:setString(indexStr)

    -- 房间ID
    local idText = item:getChildByName("IDText")
    idText:setString("房间号: " .. tabData.szRoomID)

    -- 创建时间
    local tabTime = tabData.sysCreateTime
    local strTime = string.format("%d-%02d-%02d %02d:%02d:%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay, tabTime.wHour, tabTime.wMinute, tabTime.wSecond)
    local timeText = item:getChildByName("TimeText")
    timeText:setString("创建时间: " .. strTime)
     
    local feeType = "张房卡"
    if tabData.cbCardOrBean == 0 then
        feeType = "游戏豆"
    end

    local costText = item:getChildByName("CostText")
    costText:setString("消耗: " .. tabData.lFeeCardOrBeanCount .. feeType)

    local quanShuText = item:getChildByName("QuanShuText")
    quanShuText:setString(tabData.dwPlayTurnCount)

    local jiangLiText = item:getChildByName("JiangLiText")
    jiangLiText:setString(tabData.lScore .. "游戏币")
    
    local statusText = item:getChildByName("StatusText")
    -- 房间状态
    local bOnGame = false
    if tabData.cbIsDisssumRoom == 1 then -- 解散
        statusText:setTextColor(cc.c3b(23,170,255))
        statusText:setString("已解散")
        tabTime = tabData.sysDissumeTime
        strTime = string.format("%d-%02d-%02d %02d:%02d:%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay, tabTime.wHour, tabTime.wMinute, tabTime.wSecond)
    else -- 游戏中
        statusText:setTextColor(cc.c3b(255,21,21))
        statusText:setString("游戏中")
        bOnGame = true
        strTime = ""
    end

    local jieSanTimeText = item:getChildByName("JieSanTimeText")
    jieSanTimeText:setString(strTime)
    
    local btnMore = item:getChildByName("BtnMore")
    local itemFunC = function(ref, tType)
        if tType == ccui.TouchEventType.ended then
            local tabDetail = tabData
            tabDetail.onGame = bOnGame
            local rd = RoomDetailLayer:create(tabDetail)
            rd:setName(ROOMDETAIL_NAME)
            self:addChild(rd)
        end
    end
    btnMore:addTouchEventListener( itemFunC )
    
    return item
end

function RoomRecordLayer:split( str, reps )
    local resultStrList = {}
    string.gsub(str,'[^'..reps..']+',function ( w )
        table.insert(resultStrList,w)
    end)
    return resultStrList
end

-- 参与记录
function RoomRecordLayer:joinRecordItem( tabData , idx)
    local item = self.itemLayout:clone()

    local indexText = item:getChildByName("IndexText")
    local indexStr = idx + 1
    indexText:setString(tostring(indexStr))

    local idText = item:getChildByName("IDText")
    idText:setString("房间号: "..tabData.dwRoomCardID)

    local tabTime = tabData.sysCreateTime
    local strTime = string.format("%d-%02d-%02d %02d:%02d:%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay, tabTime.wHour, tabTime.wMinute, tabTime.wSecond)
    local dzsjText = item:getChildByName("TimeText")
    dzsjText:setString("对战时间: " .. strTime)

    local recordArray = self:split(tabData.szDescribeString, "|")
    local scoreArray = self:split(recordArray[1], ",")
    local nameArray = self:split(recordArray[2], ",")

    for i=1,4 do
        item:getChildByName("NameText" .. i):setVisible(false)
        item:getChildByName("ScoreText" .. i):setVisible(false)
    end

    for i=1,#scoreArray do
        local scoreText = item:getChildByName("ScoreText" .. i)
        scoreText:setVisible(true)
        scoreText:setString(scoreArray[i])

        local nameText = item:getChildByName("NameText" .. i)
        nameText:setVisible(true)
        nameText:setString(nameArray[i])
    end

    local btnMore = item:getChildByName("BtnMore")

    local itemFunC = function(ref, tType)
        if tType == ccui.TouchEventType.ended then
            local tabDetail = tabData
            self._clickTabData = tabData
            local rd = RecordDetailLayer:create(tabDetail, self.scene, self)
            rd:setName(ROOMDETAIL_NAME)
            rd:setPosition(yl.WIDTH/2, yl.HEIGHT/2)
            self:addChild(rd)
        end
    end
    btnMore:addTouchEventListener( itemFunC )
    return item
end

return RoomRecordLayer