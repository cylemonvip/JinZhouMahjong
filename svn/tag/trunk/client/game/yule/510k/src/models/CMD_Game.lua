--[[--
游戏命令
]]

local cmd = cmd or {}

--游戏标识
cmd.KIND_ID                 = 511

--游戏人数
cmd.PLAYER_COUNT            = 4
--非法视图
cmd.INVALID_VIEWID          = 0

cmd.INVALID_CHAIRID         = 65535
--左边玩家视图
cmd.LEFT_VIEWID             = 4
--自己玩家视图
cmd.MY_VIEWID               = 1
--右边玩家视图
cmd.RIGHT_VIEWID            = 2
--上面玩家视图
cmd.TOP_VIEWID            = 3

cmd.DY_EXPRESSION_1_ANI_KEY = "dy_expression_1_ani_key"
cmd.DY_EXPRESSION_2_ANI_KEY = "dy_expression_2_ani_key"
cmd.DY_EXPRESSION_3_ANI_KEY = "dy_expression_3_ani_key"
cmd.DY_EXPRESSION_4_ANI_KEY = "dy_expression_4_ani_key"
cmd.DY_EXPRESSION_5_ANI_KEY = "dy_expression_5_ani_key"
cmd.DY_EXPRESSION_6_ANI_KEY = "dy_expression_6_ani_key"
cmd.DY_EXPRESSION_7_ANI_KEY = "dy_expression_7_ani_key"
cmd.DY_EXPRESSION_8_ANI_KEY = "dy_expression_8_ani_key"

cmd.EFFECT_ROKET_FIRE = "out_cards_effect_roket_fire"
cmd.EFFECT_BOBM_NORMAL = "out_cards_effect_boom_normal"
cmd.EFFECT_SHIP_SEA = "out_cards_effect_ship_sea"


cmd.FULL_COUNT                 = 108                                  --全牌数目
cmd.DISPATCH_COUNT             = 108                                  --发牌数目
cmd.MAX_COUNT                  = 27
cmd.MAX_CARD_COUNT             = 27
cmd.NORMAL_COUNT               = 27
cmd.PUBLIC_CARD_COUNT          = 0                                   --底牌



--游戏状态

cmd.GAME_GAME_FREE            = 0                    --等待开始
cmd.GAME_XUAN_ZHAN            = 100                    --宣战
cmd.GAME_FIND_FRIEND          = 101          --找同盟
cmd.GAME_ASK_FRIEND           = 102          --询问同盟
cmd.GAME_ADD_TIMES            = 103          --加倍
cmd.GAME_SCENE_PLAY           = 104          --游戏进行

--服务器命令结构
cmd.SUB_S_GAME_START        = 100           --游戏开始
cmd.SUB_S_OUT_CARD          = 101           --用户出牌
cmd.SUB_S_PASS_CARD         = 102           --用户放弃
--当自己出完牌时 把队友牌发给自己显示出来
cmd.SUB_S_CARD_INFO         = 103           --队友数据
cmd.SUB_S_GAME_END          = 104           --游戏结束

cmd.SUB_S_RECORD_GAME       = 105           --原本是记录游戏，现在用于游戏结算

-- cmd.SUB_S_XUAN_ZHAN         = 101           --用户宣战
-- cmd.SUB_S_FIND_FRIEND       = 102           --用户找同盟
-- cmd.SUB_S_ASK_FRIEND        = 103           --用户问同盟
-- cmd.SUB_S_ADD_TIMES         = 104           --用户加倍


-- cmd.SUB_S_GAME_CONCLUDE     = 107           --游戏结束
-- cmd.SUB_S_TRUSTEE           = 108           --用户托管
-- cmd.SUB_S_SET_BASESCORE     = 109           --设置基数

-- 倒计时
cmd.TAG_COUNTDOWN_READY     = 1
cmd.TAG_COUNTDOWN_DECLAREWAR = 2
cmd.TAG_COUNTDOWN_FIND_FRIEND   = 3
cmd.TAG_COUNTDOWN_ASK_FRIEND   = 4
cmd.TAG_COUNTDOWN_ADD_TIMES   = 5
cmd.TAG_COUNTDOWN_OUTCARD   = 6
-- 游戏倒计时


cmd.COUNTDOWN_READY         = 30            -- 准备倒计时
cmd.COUNTDOWN_DECLAREWAR = 30                -- 宣战倒计时
cmd.COUNTDOWN_FINDFRIEND = 30                -- 寻找好友倒计时
cmd.COUNTDOWN_ASKFRIEND = 30                 -- 询问好友倒计时
cmd.COUNTDOWN_ADDTIME = 30                   -- 加倍倒计时
cmd.COUNTDOWN_OUTCARD       = 30            -- 出牌倒计时
cmd.COUNTDOWN_HANDOUTTIME   = 30            -- 首出倒计时

--询问好友标志
cmd.FRIEDN_FLAG_DECLAREWAR = 1 ----宣战
cmd.FRIEDN_FLAG_MINGDU = 3  ----明独
cmd.FRIEDN_FLAG_NORMAL = 4  ----正常

-- 游戏胜利方
cmd.kDefault                = -1
cmd.kLanderWin              = 0
cmd.kLanderLose             = 1
cmd.kFarmerWin              = 2
cmd.kFarmerLose             = 3

-- 春天标记
cmd.kFlagDefault            = 0
cmd.kFlagChunTian           = 1
cmd.kFlagFanChunTian        = 2
---------------------------------------------------------------------------------------

------
--服务端消息结构
------

--空闲状态
cmd.CMD_S_StatusFree = 
{
    {k = "bTakeXue", t = "bool"},                              --大小血
    {k = "wPrisistInfo", t = "word", l = {20, 20, 20, 20}}     --爬坡信息
}

--游戏状态
cmd.CMD_S_StatusPlay = 
{
    {k = "bTakeXue", t = "bool"},                              --大小血
    --游戏属性
    {k = "wCurrentUser", t = "word"},                           --当前玩家

    --胜利信息
    {k = "wWinCount", t = "word"},                              --胜利人数
    {k = "wWinOrder", t = "word", l = {cmd.PLAYER_COUNT}},      --胜利列表
    
    --出牌信息
    {k = "wTurnWiner", t = "word"},                             --本轮胜者
    {k = "cbTurnCardType", t = "byte"},                         --出牌类型
    {k = "cbTurnCardCount", t = "byte"},                        --出牌数目
    {k = "cbTurnCardData", t = "byte", l = {cmd.MAX_COUNT}},    --出牌数据
    {k = "cbMagicCardData", t = "byte", l = {cmd.MAX_COUNT}},   --变换扑克
    
    --扑克信息
    {k = "cbHandCardData", t = "byte", l = {cmd.MAX_COUNT}},    --变换扑克
    {k = "cbHandCardCount", t = "byte", l = {cmd.PLAYER_COUNT}},--扑克数目

    {k = "b510KCardRecord", t = "byte", l = {8,8,8}},           --510K记录

    {k = "wPrisistInfo", t = "word", l = {20, 20, 20, 20}},     --爬坡信息

    --历史积分
    {k = "lTurnScore", t = "score"},    --积分信息
    {k = "lPlayerScore", t = "score", l = {cmd.PLAYER_COUNT}}, --积分信息
}

--发送扑克/游戏开始
cmd.CMD_S_GameStart = 
{
    {k = "wCurrentUser", t = "word"},                           --第一个出牌的玩家
    {k = "cbCardData", t = "byte", l = {cmd.MAX_COUNT}},        --自己的牌的点数
}

--用户出牌
cmd.CMD_S_OutCard = 
{
    {k = "cbCardCount", t = "byte"},                            --出牌数目
    {k = "wCurrentUser", t = "word"},                           --当前玩家
    {k = "wOutCardUser", t = "word"},                           --出牌玩家
    {k = "sTurnScore", t = "score"},                            --出牌玩家
    {k = "b510KCardRecord", t = "byte", l = {8,8,8}},           --510K记录
    {k = "cbCardData", t = "byte", l = {cmd.MAX_COUNT}},        --扑克列表
}

--放弃出牌
cmd.CMD_S_PassCard = 
{
    {k = "cbTurnOver", t = "byte"},                             --一轮结束
    {k = "wCurrentUser", t = "word"},                           --当前玩家
    {k = "wPassCardUser", t = "word"},                          --放弃玩家

    {k = "sTurnScore", t = "score"},                            --本轮分数
    {k = "sPlayerScore", t = "score", l = {cmd.PLAYER_COUNT}},  --玩家分数
}

--用户托管
cmd.CMD_S_Trustee = 
{
    {k = "wChair", t = "word"},                             
    {k = "bTrustee", t = "bool"},                          
}

cmd.CMD_S_GameEnd = 
{
    {k = "sPlayerScore", t = "score", l = {cmd.PLAYER_COUNT}},      --玩家分数
    {k = "sCaptureScore", t = "score", l = {cmd.PLAYER_COUNT}},     --抓分情况

    {k = "sGameScore", t = "score", l = {cmd.PLAYER_COUNT}},        --玩家分数
    
    {k = "cbCardCount", t = "byte", l = {cmd.PLAYER_COUNT}},        --具体的牌的点数
    {k = "cbCardData", t = "byte", l = {cmd.MAX_COUNT, cmd.MAX_COUNT, cmd.MAX_COUNT, cmd.MAX_COUNT}},        --具体的牌的点数

    {k = "wWinOrder", t = "word", l = {cmd.PLAYER_COUNT}},      --胜利列表用于皇上，娘娘

    {k = "wPrisistInfo", t = "word", l = {20, 20, 20, 20}}     --爬坡信息
}

cmd.CMD_S_RECORD = 
{
    {k = "cbXiaoXue", t = "byte", l = {cmd.PLAYER_COUNT} },         --大血
    {k = "cbDaXue", t = "byte", l = {cmd.PLAYER_COUNT} },           --小血
    {k = "cbHuangShang", t = "byte", l = {cmd.PLAYER_COUNT} },      --皇上
    {k = "cbNiangNiang", t = "byte", l = {cmd.PLAYER_COUNT} },      --娘娘

    {k = "lAllScore", t = "score", l = {cmd.PLAYER_COUNT} },         --所有分数
    {k = "lRealScore", t = "score", l = {cmd.PLAYER_COUNT} }         --加大小血的分数
    
}

--客户端命令结构
cmd.SUB_C_OUT_CARD          = 1              --用户出牌
cmd.SUB_C_PASS_CARD         = 2              --用户 过



cmd.SUB_C_XUAN_ZHAN        = 1               --用户宣战
cmd.SUB_C_FIND_FRIEND          = 2           --用户选同盟
cmd.SUB_C_ASK_FRIEND         = 3             --用户问同盟
cmd.SUB_C_ADD_TIMES         = 4              --用户加倍                     
                                  
cmd.SUB_C_TRUSTEE           = 7              --用户托管                     

------
--客户端消息结构
------

--用户出牌
cmd.CMD_C_OutCard = 
{
    {k = "cbCardCount", t = "byte"},                            --出牌数目
    {k = "cbCardData", t = "byte", l = {cmd.MAX_COUNT}},        --扑克数据
}

cmd.CMD_C_Trustee = 
{
    {k = "bTrustee", t = "bool"},                            --是否托管
}

return cmd