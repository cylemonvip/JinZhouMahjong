--
-- Author: zhong
-- Date: 2016-11-02 17:28:24
--
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local GameChatLayer = appdf.req(appdf.CLIENT_SRC.."plaza.views.layer.game.GameChatLayer")
local ClipText = appdf.req(appdf.EXTERNAL_SRC .. "ClipText")
local AnimationMgr = appdf.req(appdf.EXTERNAL_SRC .. "AnimationMgr")

local module_pre = "game.yule.510k.src"
local cmd = appdf.req(module_pre .. ".models.CMD_Game")
local Define = appdf.req(module_pre .. ".models.Define")
local CardSprite = appdf.req(module_pre .. ".views.layer.gamecard.CardSprite")
local CardsNode = appdf.req(module_pre .. ".views.layer.gamecard.CardsNode")
local GameLogic = appdf.req(module_pre .. ".models.GameLogic")
local ResultLayer = appdf.req(module_pre .. ".views.layer.ResultLayer")
local ChooseLayer = appdf.req(module_pre .. ".views.layer.ChooseLayer")
local PopupInfoHead = appdf.req(appdf.EXTERNAL_SRC .. "PopupInfoHead")

local ScoreCardsNode = appdf.req(module_pre .. ".views.layer.ScoreCardsNode")


local CardLogic = appdf.req(module_pre .. ".models.CardLogic")

local TAG_ENUM = Define.TAG_ENUM
local TAG_ZORDER = Define.TAG_ZORDER

local posqipaoChat = {
        cc.p(43, 70),
        cc.p(1267, 491),
        cc.p(860, 675),
        cc.p(83, 493)}

local GameViewLayer = class("GameViewLayer",function(scene)
        local gameViewLayer = display.newLayer()
    return gameViewLayer
end)


local TEST_CARD_NUM = cmd.NORMAL_COUNT
GameViewLayer.enum = 
{

    Tag_userNick =1,    

    Tag_userScore=2,

    Tag_GameScore = 10,
    Tag_Buttom = 70 ,

    Tag_Head = 1,

    Tag_FindFriend = 100,
    Tag_CardsNum = 500, --玩家牌数量
    Tag_Cards = 1000,
    Tag_CardsEffct = 2000
}

local timeBgAngle = 
    {
        0,
        -90,
        -180,
        -270,
    }

local tabCardPosition = 
    {
        cc.p(0, -270),
        cc.p(475, 40),
        cc.p(80, 285),
        cc.p(-470, 40),
    }

local ClockPosition = {
    cc.p(380, -103),
    cc.p(360, 100),
    cc.p(0, 180),
    cc.p(-370, 100)
}
--[[
local tabCardPosition = 
    {
        cc.p(0, -270),
        cc.p(0, -270),
        cc.p(0, -270),
        cc.p(0, -270),
    }
--]]
local tabCardsHandCount = 
    {
        cmd.NORMAL_COUNT,
        cmd.NORMAL_COUNT,
        cmd.NORMAL_COUNT,
        cmd.NORMAL_COUNT
    }
--玩家当前得分
local table_curScore ={0,0,0,0}
 

local TAG = GameViewLayer.enum

local RES_PATH = "game/yule/510k/res/"

function GameViewLayer:registerTouchEvent(bSwallow, FixedPriority)
    local function onTouchBegan( touch, event )
        if nil == self.onTouchBegan then
            return false
        end
        return self:onTouchBegan(touch, event)
    end

    local function onTouchMoved(touch, event)
        if nil ~= self.onTouchMoved then
            self:onTouchMoved(touch, event)
        end
    end

    local function onTouchEnded( touch, event )
        if nil ~= self.onTouchEnded then
            self:onTouchEnded(touch, event)
        end       
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    listener:setSwallowTouches(bSwallow)
    self._listener = listener
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithFixedPriority(listener, FixedPriority)
end

function GameViewLayer:ctor(scene)
    --注册node事件
    ExternalFun.registerNodeEvent(self)
    CardsNode.registerTouchEvent(self, true, 5)

    cc.FileUtils:getInstance():addSearchPath(device.writablePath..RES_PATH, true)
    cc.FileUtils:getInstance():addSearchPath(RES_PATH)

    self._scene = scene

    --初始化
    self:paramInit()
    TEST_CARD_NUM = cmd.NORMAL_COUNT
    --加载资源
    self:loadResource()
    self.m_nCountDownView = cmd.MY_VIEWID

    self.chatDetails = {}

    if not self._scene._isReplay then
        self:startHeartBeat()
        self:startQuickMsgTimeDelayUpdate()
    end
end

function GameViewLayer:stopQuickMsgTimeDelayUpdate()
    if self.m_nextSendDelayUpdate then
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.m_nextSendDelayUpdate)
        self.m_nextSendDelayUpdate = nil
    end
end

function GameViewLayer:startQuickMsgTimeDelayUpdate()
    self.m_nextSendDelay = 0
    self.m_nextSendDelayUpdate = cc.Director:getInstance():getScheduler():scheduleScriptFunc(function(dt)
        self.m_nextSendDelay = self.m_nextSendDelay - 1
        if self.m_nextSendDelay <= 0 then
            self.m_nextSendDelay = 0
        end
    end, 1.0, false)
end

function GameViewLayer:paramInit()
    self.m_bSelfDu = false --自己是否为独

    self.m_bHaveBlackFive = false --是否拥有黑桃5
    -- 聊天层
    self.m_chatLayer = nil
    -- 结算层
    self.m_resultLayer = nil

    -- 手牌控制
    self.m_cardControl = nil
    -- 手牌数量
    self.m_tabCardCount = {}
    -- 报警动画
    self.m_tabSpAlarm = {}

    -- 叫分text
    -- self.m_textGameCall = nil
    -- 庄家牌
    self.m_nodeBankerCard = nil
    self.m_tabBankerCard = {}
    -- 准备按钮
    self.m_btnReady = nil
    
    -- 准备标签
    self.m_tabReadySp = {}
    -- 状态标签
    self.m_tabStateSp = {}

    -- 叫分控制
    self.m_callScoreControl = nil
    self.m_nMaxCallScore = 0
    self.m_tabCallScoreBtn = {}

    -- 操作控制
    self.m_onGameControl = nil
    self.m_bt_outCard = nil
    self.m_bt_pass = nil
    self.m_bt_tip = nil
    self.m_bMyCallBanker = false
    self.m_bMyOutCards = false
    --解散
    self.m_bt_dissolve = nil
    --暂离
    self.m_bt_depart = nil
    -- 出牌控制
    self.m_outCardsControl = nil
    -- 能否出牌
    self.m_bCanOutCard = false

    -- 用户信息
    self.m_userinfoControl = nil
    -- 用户头像
    self.m_tabUserHead = {}
    self.m_tabUserHeadPos = {}
    -- 用户信息
    self.m_tabUserItem = {}

    self.m_tabUserItemCopy = {}
    -- 用户昵称
    self.m_tabCacheUserNick = {}
    -- 用户游戏币
    self.m_atlasScore = nil
    -- 底分
    self.m_atlasDiFeng = nil
    -- 一轮提示组合
    self.m_promptIdx = 0
    -- 倒计时
    self.m_time_bg = nil
    self.m_time_num = nil
    self.m_tabTimerPos = {}

    --托管界面bg
    self.m_trusteeshipBg = nil

    -- 扑克
    self.m_tabNodeCards = {}

    -- 火箭
    self.m_actRocketRepeat = nil
    -- 火箭飞行
    self.m_actRocketShoot = nil

    -- 飞机
    self.m_actPlaneRepeat = nil
    -- 飞机飞行
    self.m_actPlaneShoot = nil

    -- 炸弹
    self.m_actBomb = nil
    --菜单按钮
    self.m_menu_bg = nil
    --音效
    self.m_bt_music = nil
    --托管
    -- self.m_bt_trusteeship = nil

    -- self.m_bt_help = nil

    -- self.m_bt_exit = nil


    --历史记录背景框
    self.m_record_bg = nil
    --本轮得分
    self.m_round_outTotalScore = nil

    self.m_wChairId = self._scene:GetMeChairID()
    self.m_wTableId = self._scene:GetMeTableID()
    self.m_cardNum1 = nil

    self.m_tabSelectCards = {}

    self.m_bClickButton = false

    --历史成绩
    self.m_lCellScore = 1

    self.m_lGameScore = {} --游戏输赢分

    self.m_lCollectScore = {1,1,1,1} --汇总成绩

    self.m_cbScore = {0,0,0,0} --玩家当前游戏得分

    self.m_cbBaseTimes = 1 --基础倍数

    self.m_wBankerUser = 0 --庄家用户

    self.m_tabDwUserID = {1,1,1,1}

    self.m_historyScore = {
        {lTurnScore = 0,lCollectScore = 0},
        {lTurnScore = 0,lCollectScore = 0},
        {lTurnScore = 0,lCollectScore = 0},
        {lTurnScore = 0,lCollectScore = 0},
    } --历史积分信息

    self.m_historyUseItem = {}

    self.m_tabCbBaseTimes = {1, 1, 1, 1}

    self.m_nRoundCount = 0 -------玩了多少回合

    self.m_nCurTotalScore = 0

    self.m_bClickSuggest = false

    self.m_bTrustee = false

    self.m_tabVoiceBox = {}

    self.m_tabPopHeadPos = 
    {
        cc.p(43, 46),
        cc.p(1267, 491),
        cc.p(860, 675),
        cc.p(83, 493)
    }

    self.m_tabFlagPos = 
    {
        cc.p(127, 73),
        cc.p(1197, 488),
        cc.p(791, 669),
        cc.p(154, 493)
    }

    self.m_tabPopHeadAnchorPoint =
    {
        cc.p(0, 0),
        cc.p(1, 0.5),
        cc.p(1, 1),
        cc.p(0, 0.5),
    }

    self.m_tabRecordNickName = {}
end

function GameViewLayer:getParentNode()
    return self._scene
end


function GameViewLayer:onTouchBegan(touch, event)
    print("GameViewLayer onTouchBegan")
    --if self.m_bClickSuggest then
        --self.m_bClickSuggest = false
        return true
    --else
        --return false
    --end
end

function GameViewLayer:onTouchMoved(touch, event)
 
end

function GameViewLayer:onTouchEnded(touch, event)
    print("GameViewLayer onTouchEnded")
end

function GameViewLayer:addToRootLayer( node , zorder)
    if nil == node then
        return
    end

    self.m_rootLayer:addChild(node)
    if type(zorder) == "number" then
        node:setLocalZOrder(zorder)
    end    
end

function GameViewLayer:loadResource()
    -- 加载卡牌纹理
    -- cc.Director:getInstance():getTextureCache():addImage("card_res/card.png")
    -- cc.Director:getInstance():getTextureCache():addImage("card_res/cardsmall.png")
    --添加特效plist

    cc.Director:getInstance():getTextureCache():addImage("game/yule/510k/res/effect/LZ_NewPokerType0.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/510k/res/effect/LZ_NewPokerType0.plist")

    cc.Director:getInstance():getTextureCache():addImage("game/yule/510k/res/effect/LZ_NewPokerType1.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/510k/res/effect/LZ_NewPokerType1.plist")

    cc.Director:getInstance():getTextureCache():addImage("game/yule/510k/res/effect/LZ_NewPokerType2.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/510k/res/effect/LZ_NewPokerType2.plist")

    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_1_%d.png", 0, 11, cmd.DY_EXPRESSION_1_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_2_%d.png", 0, 23, cmd.DY_EXPRESSION_2_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_3_%d.png", 0, 6, cmd.DY_EXPRESSION_3_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_4_%d.png", 0, 13, cmd.DY_EXPRESSION_4_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_5_%d.png", 0, 119, cmd.DY_EXPRESSION_5_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_6_%d.png", 0, 11, cmd.DY_EXPRESSION_6_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_7_%d.png", 0, 10, cmd.DY_EXPRESSION_7_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_8_%d.png", 0, 16, cmd.DY_EXPRESSION_8_ANI_KEY, AnimationMgr.LOCAL_RES)

    AnimationMgr.loadAnimationFromFrame("Boom_Boom_%d.png", 1, 14, cmd.EFFECT_BOBM_NORMAL, AnimationMgr.PLIST_RES)
    AnimationMgr.loadAnimationFromFrame("roket_%d.png", 1, 6, cmd.EFFECT_ROKET_FIRE, AnimationMgr.PLIST_RES)

    AnimationMgr.loadAnimationFromFrame("ship_Sea_%d.png", 1, 8, cmd.EFFECT_SHIP_SEA, AnimationMgr.PLIST_RES)
    

    local tabCardEffectIndex = {3, 5, 7, 9,11}
    local tabCardEffectCount = {13, 12, 14, 16,14}
    local cardEffctWidth = 470
    local cardEffctHeight = 156
    --牌效果
    local aniTime = 0.12
    for i = 1,5 do
        cc.Director:getInstance():getTextureCache():addImage("card_res/cardEffect"..tabCardEffectIndex[i]..".png")
        cc.SpriteFrameCache:getInstance():addSpriteFrames("card_res/cardEffect"..tabCardEffectIndex[i]..".plist")
    end
 
    for i = 1,#tabCardEffectIndex do
        if tabCardEffectIndex[i] == 11 then
            aniTime = 0.08
        end
        local frames = {}
        for j = 1,tabCardEffectCount[i] do 
            local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame("cardEffect"..tabCardEffectIndex[i].."_"..j..".png") 
            table.insert(frames, frame)   
        end

        local  animation = cc.Animation:createWithSpriteFrames(frames,aniTime)
        cc.AnimationCache:getInstance():addAnimation(animation, "cardEffectAnim"..tabCardEffectIndex[i])
    end
    --警报灯
    local alertFrames = {}
    for i = 1, 2 do
        local frame = cc.SpriteFrame:create("game_res/alert.png",cc.rect(114*(i-1),0,114,93))
        table.insert(alertFrames, frame)  
    end
    local  alertAnim = cc.Animation:createWithSpriteFrames(alertFrames,0.35)
    cc.AnimationCache:getInstance():addAnimation(alertAnim, "alertAnim")

    

    -- 加载动画纹理

    --语音动画
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game_res/voice_ani.plist")
    local frames = {}
    for i=1, 2 do
        local frameName = "img_voice_" .. i ..".png"
        local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(frameName) 
        table.insert(frames, frame)
    end
    local  animation =cc.Animation:createWithSpriteFrames(frames,0.3)

    cc.AnimationCache:getInstance():addAnimation(animation, Define.VOICE_ANIMATION_KEY)
    
    --播放背景音乐
    ExternalFun.playBackgroudAudio("background.mp3")

    local rootLayer, csbNode = ExternalFun.loadRootCSB("game_res/GameLayer.csb", self)
    self.m_rootLayer = rootLayer
    self.m_csbNode = csbNode
    self.m_csbNode:setAnchorPoint(cc.p(0,0))
    self.m_csbNode:setPosition(cc.p(667,375))
    local function btnEvent( sender, eventType )
        if eventType == ccui.TouchEventType.began then
            ExternalFun.popupTouchFilter(1, false)
        elseif eventType == ccui.TouchEventType.canceled then
            ExternalFun.dismissTouchFilter()
        elseif eventType == ccui.TouchEventType.ended then
            ExternalFun.dismissTouchFilter()
            self:onButtonClickedEvent(sender:getTag(), sender)
            if nil ~= self.m_tabNodeCards then
                self.m_tabNodeCards[1]:onTouchEnded(sender,eventType)
            end   
        end
    end
    local csbNode = self.m_csbNode

    --遮罩
    local mask = self.m_csbNode:getChildByName("Mask")
    mask:setTouchEnabled(true);

    -- 准备标签
    -- 扑克牌
    for i = 1, 4 do
        local bShow = false
        if i == cmd.MY_VIEWID then
            bShow = true
        end
        self.m_tabNodeCards[i] = CardsNode:createEmptyCardsNode(i,true, -128)
        self.m_tabNodeCards[i]:setPosition(tabCardPosition[i])
        self.m_tabNodeCards[i]:setListener(self)
        self.m_csbNode:addChild(self.m_tabNodeCards[i], 1)
    end
    self:initCardsNodeLayout()
    self.m_tabAlertSp = {}
    --3个警报灯
    for i = 2, 4 do
        local alert = csbNode:getChildByName("alert_"..i)
        alert:setVisible(false)
        self.m_tabAlertSp[i] = cc.Sprite:create("game_res/alert.png", cc.rect(0, 0, 114, 93))
        self.m_tabAlertSp[i]:setPosition(cc.p(alert:getPositionX(), alert:getPositionY()))
        self.m_tabAlertSp[i]:setLocalZOrder(TAG_ZORDER.Card_Alert)
        csbNode:addChild(self.m_tabAlertSp[i])
        local animation = cc.AnimationCache:getInstance():getAnimation("alertAnim")
        self.m_tabAlertSp[i]:runAction(cc.RepeatForever:create(cc.Animate:create(animation)))
        self.m_tabAlertSp[i]:setVisible(false)
    end

     -- 邀请按钮
    self.m_btnInvite = csbNode:getChildByName("bt_invite")
    self.m_btnInvite:setTag(TAG_ENUM.BT_INVITE)
    self.m_btnInvite:addTouchEventListener(btnEvent)
    self.m_btnInvite:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)

    if GlobalUserItem.bPrivateRoom then
        self.m_btnInvite:setVisible(false)
        self.m_btnInvite:setEnabled(false)
    end

    self.m_time_bg = csbNode:getChildByName("time_bg")
    self.m_time_bg:setVisible(false)

    self.m_time_num = self.m_time_bg:getChildByName("time_num")

    for i = 1, 4 do

        local head_bg = csbNode:getChildByName(string.format("head_bg_%d", i))
        if i == 1 then
            -- 游戏币
            self.m_atlasScore = head_bg:getChildByName("player_score")
        else
            head_bg:setVisible(false)
        end
        local head_cover = head_bg:getChildByName("head_cover")
        head_cover:setLocalZOrder(TAG.Tag_Head + 1)

        local player_status = head_bg:getChildByName("player_status")
        player_status:setVisible(true)
        player_status:setLocalZOrder(TAG.Tag_Head + 2)
        
        local heart = head_bg:getChildByName("heart")
        heart:setVisible(false)
        heart:setLocalZOrder(TAG.Tag_Head + 5)
        --
        local tip_ready = csbNode:getChildByName(string.format("tip_ready_%d", i))
        tip_ready:setVisible(false)
        local tip_operate = csbNode:getChildByName(string.format("tip_operate_%d", i))
        tip_operate:setVisible(false)

        tip_addTime = csbNode:getChildByName(string.format("tip_addTime_%d", i))
        tip_addTime:setVisible(false)

        local tip_no_addTime = self.m_csbNode:getChildByName(string.format("tip_no_addTime_%d", i))
        tip_no_addTime:setVisible(false)

        local text_curScore = head_bg:getChildByName("text_curScore")
        text_curScore:setString("当前得分:0")

        local img_voice_box = csbNode:getChildByName("img_voice_box_"..i)
        img_voice_box:setVisible(false)
        local img_voice = img_voice_box:getChildByName("img_voice")
        img_voice_box:setLocalZOrder(TAG_ZORDER.VOICE_ZORDER)
        self.m_tabVoiceBox[i] = img_voice_box
        local animation = cc.AnimationCache:getInstance():getAnimation(Define.VOICE_ANIMATION_KEY)
        if nil ~= animation then
            local action = cc.RepeatForever:create(cc.Animate:create(animation))
            img_voice:runAction(action)
        end
    end

    self.m_record_bg = csbNode:getChildByName("record_bg")
    self.m_record_bg:setVisible(false)
    self.m_record_bg:setLocalZOrder(TAG_ZORDER.Card_Control)
    for i = 1, 4 do
        local nickName = self.m_record_bg:getChildByName("nickName_"..i)
        nickName:setVisible(false)
        self.m_tabRecordNickName[i] = ClipText:createClipText(cc.size(110, 30), "xx", nil, 16)
        self.m_tabRecordNickName[i]:setAnchorPoint(cc.p(0, 0.5))
        self.m_tabRecordNickName[i]:setPosition(cc.p(nickName:getPositionX(), nickName:getPositionY()))
        self.m_record_bg:addChild(self.m_tabRecordNickName[i])
    end
    --本轮得分
    self.m_round_outTotalScore = csbNode:getChildByName("round_outTotalScore")
    self.m_round_outTotalScore:setString("0")
    

    --顶部菜单

    --菜单按钮
    self.m_menu_bg = nil
    --音效
    self.m_bt_music = nil
    --
    -- self.m_bt_trusteeship = nil

    -- self.m_bt_help = nil

    -- self.m_bt_exit = nil

    self.m_cardNum1 = csbNode:getChildByName("cardNum1")
    self.m_cardNum1:setVisible(false)

    self.m_menu_bg = csbNode:getChildByName("menu_bg")
    --self.m_menu_bg:setVisible(false)
    self.m_menu_bg:setScale(0.01)
    --
    self.m_bMenuVisible = false
    self.m_bt_menu = csbNode:getChildByName("bt_menu")
    self.m_bt_menu:setTag(TAG_ENUM.BT_SETTING)
    self.m_bt_menu:setSwallowTouches(true)
    self.m_bt_menu:addTouchEventListener(btnEvent)
    self.m_bt_menu:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)

    self.m_bt_record_2 = csbNode:getChildByName("bt_record_2")
    self.m_bt_record_2:setTag(TAG_ENUM.BT_RECORD)
    self.m_bt_record_2:setSwallowTouches(true)
    self.m_bt_record_2:addTouchEventListener(btnEvent)
    self.m_bt_record_2:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)


    --标签
    local str = ""

    --准备按钮
    self.m_btnReady = csbNode:getChildByName("bt_start")
    self.m_btnReady:setTag(TAG_ENUM.BT_READY)
    self.m_btnReady:addTouchEventListener(btnEvent)
    self.m_btnReady:setEnabled(true)
    self.m_btnReady:setVisible(true)
    self.m_btnReady:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)

    self.m_bt_pass = csbNode:getChildByName("bt_pass")
    self.m_bt_pass:setTag(TAG_ENUM.BT_PASS)
    self.m_bt_pass:addTouchEventListener(btnEvent)
    self.m_bt_pass:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)

    --提示按钮
    self.m_bt_tip = csbNode:getChildByName("bt_tip")
    self.m_bt_tip:setTag(TAG_ENUM.BT_SUGGEST)
    self.m_bt_tip:setSwallowTouches(true)
    self.m_bt_tip:addTouchEventListener(btnEvent)
    self.m_bt_tip:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)

    --出牌按钮
    self.m_bt_outCard = csbNode:getChildByName("bt_outCard")
    self.m_bt_outCard:setTag(TAG_ENUM.BT_OUTCARD)
    self.m_bt_outCard:addTouchEventListener(btnEvent)
    self.m_bt_outCard:setSwallowTouches(true)
    self.m_bt_outCard:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)

    --语音
    self.m_bt_voice = csbNode:getChildByName("bt_voice")
    self.m_bt_voice:setTag(TAG_ENUM.BT_VOICE)
    self.m_bt_voice:addTouchEventListener(btnEvent)
    self.m_bt_voice:setLocalZOrder(TAG_ZORDER.Btns_ZORDER)
    self.m_bt_voice:setVisible(false)

    --通用语音按钮
    if not GlobalUserItem.isAntiCheat() then
        self:getParentNode():getParentNode():createVoiceBtn(cc.p(self.m_bt_voice:getPositionX(), self.m_bt_voice:getPositionY()),TAG_ZORDER.Btns_ZORDER,self.m_csbNode)
    end
    
    
    self:hideAddTimesBtns()
    self:hideOutCardsBtns()
    self:hideAskFriendBtns()

    self.m_trusteeshipBg = csbNode:getChildByName("trusteeship_bg")
    self.m_trusteeshipBg:setLocalZOrder(TAG_ZORDER.TRUSTEESHIP_ZORDER)
    self.m_trusteeshipBg:setVisible(false)

    self.m_outCardsControl = cc.LayerColor:create(cc.c4b(0,0,0,0),yl.WIDTH,yl.HEIGHT)
    self.m_outCardsControl:setPosition(cc.p(- yl.WIDTH / 2, - yl.HEIGHT / 2))
    csbNode:addChild(self.m_outCardsControl, TAG_ZORDER.Card_Control)

    self:updateRule()

    self.m_spInfoTip = cc.Sprite:create("game/yule/510k/res/game_res/game_tips_00.png")
    self.m_spInfoTip:setVisible(false)
    self.m_spInfoTip:setPosition(yl.WIDTH * 0.5, 160)
    self:addChild(self.m_spInfoTip, 5)

    --交换位置按钮
    for i = 2, 4 do
        local exchangeSit = csbNode:getChildByName("ExchangeSit_" .. i)
        if i == 2 then
            exchangeSit:setTag(TAG_ENUM.BT_EXCHANGE_2)
        elseif i == 3 then
            exchangeSit:setTag(TAG_ENUM.BT_EXCHANGE_3)
        elseif i == 4 then
            exchangeSit:setTag(TAG_ENUM.BT_EXCHANGE_4)
        end
        
        exchangeSit:addTouchEventListener(btnEvent)
    end 

    local speakBtnPanel = csbNode:getChildByName("SpeakBtnPanel")
    for i=1, 9 do
        local btn = speakBtnPanel:getChildByName("SpeakBtn_" .. i)
        btn.index = i
        btn:addTouchEventListener(function( sender, eventType )
            if eventType == ccui.TouchEventType.began then
            elseif eventType == ccui.TouchEventType.canceled then
            elseif eventType == ccui.TouchEventType.ended then
                if self.m_nextSendDelay <= 0 then
                    local index = sender.index
                    if index and index >= 1 and index <= 9 then
                        local headitem = self.m_tabUserItem[1]
                        local msg = ""
                        if index == 1 then
                            msg = "不要"
                        elseif index == 2 then
                            msg = "让我来"
                        elseif index == 3 then
                            msg = "你管"
                        elseif index == 4 then
                            msg = "炸他"
                        elseif index == 5 then
                            msg = "屁他"
                        elseif index == 6 then
                            msg = "上分"
                        elseif index == 7 then
                            msg = "单"
                        elseif index == 8 then
                            msg = "对"
                        elseif index == 9 then
                            msg = "龙"
                        end
                        --有间隔时间
                        self.m_nextSendDelay = 2
                        if nil ~= self._scene._gameFrame and nil ~= self._scene._gameFrame.sendTextChat then
                            return self._scene._gameFrame:sendTextChat(msg, nil, index + 30)
                        end
                        
                    end
                else
                    local runScene = cc.Director:getInstance():getRunningScene()
                    showToast(runScene, "您的操作过于频繁，请在 " .. self.m_nextSendDelay .. "秒后再试", 2, cc.c4b(250,0,0,255))
                end
            end
        end)
    end

        --聊天泡泡
    self.chatBubble = {}
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/sparrowhz/res/game/plazaScene.plist")

    for i = 1 , cmd.PLAYER_COUNT do
        local strFile = ""
        if i == 2 then
            strFile = "#userinfo_chat_bg_2.png"
        else
            strFile = "#userinfo_chat_bg_1.png"
        end
        self.chatBubble[i] = display.newSprite(strFile, {scale9 = true ,capInsets = cc.rect(30, 30, 40, 150)})
            :setVisible(false)
            :addTo(self, 18)
        if i == 2 then
            self.chatBubble[i]:setAnchorPoint(cc.p(1, 1))
        else
            self.chatBubble[i]:setAnchorPoint(cc.p(0, 1))
        end
        self.chatBubble[i]:setPosition(posqipaoChat[i])
    end

    for i = 1, 9 do
        local papo = csbNode:getChildByName("Papo_" .. i)
        papo:setVisible(false)
    end

    self.sortcardBtn = csbNode:getChildByName("SortcardBtn")
    self.sortcardBtn:setVisible(false)
    self.sortcardBtn:setTag(TAG_ENUM.BT_SORT_CARD)
    self.sortcardBtn:addTouchEventListener(btnEvent)
end

function GameViewLayer:startHeartBeat(beatSpan)
    beatSpan = beatSpan or yl.HEART_BEAT_SPAN
    self:stopHeartBeat()
    self._heartBeatTimer = cc.Director:getInstance():getScheduler():scheduleScriptFunc(function(dt)
        self._scene:sendHeartBeatTimer()
    end, beatSpan, false)
end

function GameViewLayer:stopHeartBeat()
    if self._heartBeatTimer then
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._heartBeatTimer)
        self._heartBeatTimer = nil
    end
end

function GameViewLayer:createAnimation()
    local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.1
    -- 火箭动画
    param.m_strName = Define.ROCKET_ANIMATION_KEY
    local animate = AnimationMgr.getAnimate(param)
    if nil ~= animate then
        local rep = cc.RepeatForever:create(animate)
        self.m_actRocketRepeat = rep
        self.m_actRocketRepeat:retain()
        local moDown = cc.MoveBy:create(0.1, cc.p(0, -20))
        local moBy = cc.MoveBy:create(2.0, cc.p(0, 500))
        local fade = cc.FadeOut:create(2.0)
        local seq = cc.Sequence:create(cc.DelayTime:create(2.0), cc.CallFunc:create(function()

            end), fade)
        local spa = cc.Spawn:create(cc.EaseExponentialIn:create(moBy), seq)
        self.m_actRocketShoot = cc.Sequence:create(cc.CallFunc:create(function( ref )
            ref:runAction(rep)
        end), moDown, spa, cc.RemoveSelf:create(true))
        self.m_actRocketShoot:retain()
    end

    -- 飞机动画    
    param.m_strName = Define.AIRSHIP_ANIMATION_KEY
    local animate = AnimationMgr.getAnimate(param)
    if nil ~= animate then
        local rep = cc.RepeatForever:create(animate)
        self.m_actPlaneRepeat = rep
        self.m_actPlaneRepeat:retain()
        local moTo = cc.MoveTo:create(3.0, cc.p(0, yl.HEIGHT * 0.5))
        local fade = cc.FadeOut:create(1.5)
        local seq = cc.Sequence:create(cc.DelayTime:create(1.5), cc.CallFunc:create(function()
            ExternalFun.playSoundEffect("common_plane.wav")
            end), fade)
        local spa = cc.Spawn:create(moTo, seq)
        self.m_actPlaneShoot = cc.Sequence:create(cc.CallFunc:create(function( ref )
            ref:runAction(rep)
        end), spa, cc.RemoveSelf:create(true))
        self.m_actPlaneShoot:retain()
    end

    -- 炸弹动画
    param.m_strName = Define.BOMB_ANIMATION_KEY
    local animate = AnimationMgr.getAnimate(param)
    if nil ~= animate then
        local fade = cc.FadeOut:create(1.0)
        self.m_actBomb = cc.Sequence:create(animate, fade, cc.RemoveSelf:create(true))
        self.m_actBomb:retain()
    end    
end

function GameViewLayer:unloadResource()

    local tabCardEffectIndex = {3, 5, 7, 9,11}
    for i = 1,5 do
        cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile("card_res/cardEffect"..tabCardEffectIndex[i]..".png")
        cc.Director:getInstance():getTextureCache():removeTextureForKey("card_res/cardEffect"..tabCardEffectIndex[i]..".plist")
    end

    AnimationMgr.removeCachedAnimation(Define.CALLSCORE_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.CALLONE_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.CALLTWO_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.CALLTHREE_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.AIRSHIP_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.ROCKET_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.ALARM_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.BOMB_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation(Define.VOICE_ANIMATION_KEY)
    AnimationMgr.removeCachedAnimation("alertAnim")

    cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile("game_res/voice_ani.plist")
    cc.Director:getInstance():getTextureCache():removeTextureForKey("game_res/voice_ani.png")
    cc.Director:getInstance():getTextureCache():removeTextureForKey("game_res/alert.png")
    -- cc.Director:getInstance():getTextureCache():removeTextureForKey("card_res/card.png")
    -- cc.Director:getInstance():getTextureCache():removeTextureForKey("card_res/cardsmall.png")

    cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile("public_res/public_res.plist")
    cc.Director:getInstance():getTextureCache():removeTextureForKey("public_res/public_res.png")
    cc.Director:getInstance():getTextureCache():removeUnusedTextures()
    cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames()
end
-- 重置
function GameViewLayer:reSetGame()
    self:reSetUserState()
    self.m_time_bg:setVisible(false)
    self.m_time_num:setString("")
    self.m_cardNum1:setVisible(false)

    for i = 1, cmd.PLAYER_COUNT do
        local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", i))
        local player_status = head_bg:getChildByName("player_status")
        player_status:setVisible(true)

        local tip_operate = self.m_csbNode:getChildByName(string.format("tip_operate_%d", i))
        tip_operate:setVisible(false)
        local text_curScore = head_bg:getChildByName("text_curScore")
        text_curScore:setString("当前得分:0")
        local nodeCard = self.m_tabNodeCards[i]
        nodeCard:setCardsCount(cmd.NORMAL_COUNT)
        nodeCard:setVisible(false)
        local heart = head_bg:getChildByName("heart")
        heart:setVisible(false)

        tip_addTime = self.m_csbNode:getChildByName(string.format("tip_addTime_%d", i))
        tip_addTime:setVisible(false)

        self.m_cbScore[i] = 0
    end
    self.m_bMyCallBanker = false
    self.m_bMyOutCards = false

end

function GameViewLayer:setHuangShangFlag(viewId)
    if viewId == nil then
        local sphs = self:getChildByName("NAME_HUANG_SHANG")
        if sphs then
            sphs:removeFromParent()
        end
        return
    end

    self._scene.m_hasHuangShang = true
    --是否存在第一个出完牌的玩家
    local sphs = self:getChildByName("NAME_HUANG_SHANG")
    if sphs then
        sphs:setVisible(true)
        sphs:setPosition(self.m_tabFlagPos[viewId])
    else
        --第一个出完，添加皇上图标
        local falgPath = "game/yule/510k/res/game_res/flag_huangshang.png"
        sphs = cc.Sprite:create(falgPath)
        sphs:setPosition(self.m_tabFlagPos[viewId])
        sphs:setName("NAME_HUANG_SHANG")
        self:addChild(sphs, 15)        
    end
end

function GameViewLayer:setNiangNiangFlag(viewId)
    if viewId == nil then
        local sphs = self:getChildByName("NAME_NIANG_NIANG")
        if sphs then
            sphs:removeFromParent()
        end
        return
    end

    self._scene.m_hasHuangShang = true
    --是否存在第一个出完牌的玩家
    local sphs = self:getChildByName("NAME_NIANG_NIANG")
    if sphs then
        sphs:setVisible(true)
        sphs:setPosition(self.m_tabFlagPos[viewId])
    else
        --第一个出完，添加皇上图标
        local falgPath = "game/yule/510k/res/game_res/flag_niangniang.png"
        sphs = cc.Sprite:create(falgPath)
        sphs:setPosition(self.m_tabFlagPos[viewId])
        sphs:setName("NAME_NIANG_NIANG")
        self:addChild(sphs, 15)        
    end
end

-- 重置(新一局)
function GameViewLayer:reSetForNewGame()
    self:setHuangShangFlag(nil)
    self:setNiangNiangFlag(nil)

    for i = 1, 4 do
        local node = self.m_csbNode:getChildByName("DisplayHandCardNode_" .. i)
        node:removeAllChildren()
    end

    for i = 1, 4 do
        local tip_operate = self.m_csbNode:getChildByName("tip_operate_" .. i)
        tip_operate:setVisible(false)
    end

    -- 清理手牌
    for k,v in pairs(self.m_tabNodeCards) do
        v.m_sortCard = false
        v:removeAllCards()
        if self.m_tabSpAlarm[k] then
            self.m_tabSpAlarm[k]:stopAllActions()
            self.m_tabSpAlarm[k]:setSpriteFrame("blank.png")
        end
    end
    for k,v in pairs(self.m_tabCardCount) do
        v:setString("")
    end
    -- 清理桌面
    self.m_outCardsControl:removeAllChildren()
    -- 庄家叫分
    -- self.m_textGameCall:setString("")
    -- 庄家扑克
    for k,v in pairs(self.m_tabBankerCard) do
        v:setVisible(false)
        v:setCardValue(0)
    end
    -- 用户切换
    for k,v in pairs(self.m_tabUserHead) do
        v:reSet()
    end
end

-- 重置用户状态
function GameViewLayer:reSetUserState()
    for k,v in pairs(self.m_tabReadySp) do
        v:setVisible(false)
    end

    for k,v in pairs(self.m_tabStateSp) do
        v:setSpriteFrame("blank.png")
    end
    -- self.m_btnReady:setEnabled(true)
    -- self.m_btnReady:setVisible(true)
end

-- 重置用户信息
function GameViewLayer:reSetUserInfo()
    local score = self:getParentNode():GetMeUserItem().dBeans or 0
    local str = ""
    if score < 0 then
        str = "." .. score
    else
        str = "" .. score        
    end 
    if string.len(str) > 11 then
        str = string.sub(str, 1, 11)
        str = str .. "///"
    end  
    self.m_atlasScore:setString(str) 
end

function GameViewLayer:onExit()
    if nil ~= self.m_actRocketRepeat then
        self.m_actRocketRepeat:release()
        self.m_actRocketRepeat = nil
    end

    if nil ~= self.m_actRocketShoot then
        self.m_actRocketShoot:release()
        self.m_actRocketShoot = nil
    end

    if nil ~= self.m_actPlaneRepeat then
        self.m_actPlaneRepeat:release()
        self.m_actPlaneRepeat = nil
    end

    if nil ~= self.m_actPlaneShoot then
        self.m_actPlaneShoot:release()
        self.m_actPlaneShoot = nil
    end

    if nil ~= self.m_actBomb then
        self.m_actBomb:release()
        self.m_actBomb = nil
    end
    self:unloadResource()

    self.m_tabUserItem = {}

    self:stopHeartBeat()
    self:stopQuickMsgTimeDelayUpdate()
end

function GameViewLayer:onButtonClickedEvent(tag, ref)   
    local function addBG()
        local bg = ccui.ImageView:create()
        bg:setContentSize(cc.size(yl.WIDTH, yl.HEIGHT))
        bg:setScale9Enabled(true)
        bg:setPosition(yl.WIDTH/2, yl.HEIGHT/2)
        bg:setTouchEnabled(true)
        self:addChild(bg,50)
        bg:addTouchEventListener(function (sender,eventType)
            if eventType == ccui.TouchEventType.ended then
                bg:removeFromParent()

            end
        end)

        return bg
    end
    ExternalFun.playClickEffect()
    if TAG_ENUM.BT_HELP == tag then

    elseif TAG_ENUM.BT_VOICE == tag then

    elseif TAG_ENUM.BT_RECORD == tag then
        --弹出已出牌分
        local scoreCardsNode = ScoreCardsNode:create(self, self._scene.m_allReadyOut510K)
        scoreCardsNode:setPosition(cc.p(667, 375))
        self:addChild(scoreCardsNode, 15)
    elseif TAG_ENUM.BT_CHAT == tag then             --聊天        
        if nil == self.m_chatLayer then
            self.m_chatLayer = GameChatLayer:create(self._scene._gameFrame)
            self:addToRootLayer(self.m_chatLayer, TAG_ZORDER.CHAT_ZORDER)
        end
        self.m_chatLayer:showGameChat(true)
    elseif TAG_ENUM.BT_MUSIC == tag then            --设置音效
         GlobalUserItem.bVoiceAble = not GlobalUserItem.bVoiceAble
                GlobalUserItem.bSoundAble = GlobalUserItem.bVoiceAble

                if GlobalUserItem.bVoiceAble then
                    AudioEngine.resumeMusic()
                    AudioEngine.setMusicVolume(1.0)
                    self.m_bt_music:loadTextures("game_res/bt_musicOn_1.png", "game_res/bt_musicOn_2.png", "game_res/bt_musicOn_1.png")
                else
                    AudioEngine.setMusicVolume(0)
                    AudioEngine.pauseMusic() -- 暂停音乐
                    self.m_bt_music:loadTextures("game_res/bt_musicOff_1.png", "game_res/bt_musicOff_2.png", "game_res/bt_musicOff_1.png")
                end
    elseif TAG_ENUM.BT_SETTING == tag then
        local bMute = false

        local  bg = addBG()

        local csbNode = ExternalFun.loadCSB("game_res/Setting.csb", bg)
        csbNode:setAnchorPoint(0.5,0.5)
        csbNode:setPosition(yl.WIDTH/2,yl.HEIGHT/2)

        local Image_setBG = csbNode:getChildByName("Image_setBG")
        local btnClose = csbNode:getChildByName("bt_close")

        local panelMask = csbNode:getChildByName("Panel_1")

        panelMask:addTouchEventListener(function ( sender , eventType )
            if eventType == ccui.TouchEventType.ended then
                bg:removeFromParent()
            end
        end)

        btnClose:addTouchEventListener(function ( sender , eventType )
            if eventType == ccui.TouchEventType.ended then
                bg:removeFromParent()
            end
        end)

        --静音按钮
        local btn_music = csbNode:getChildByName("btn_music")
        
        if not GlobalUserItem.bVoiceAble then
            btn_music:loadTextures("setting_res/bt_off_1.png", "setting_res/bt_off_2.png", "setting_res/bt_off_1.png")
        else
            btn_music:loadTextures("setting_res/bt_on_1.png", "setting_res/bt_on_2.png", "setting_res/bt_on_1.png")
        end

        local btn_effect = csbNode:getChildByName("btn_effect")
        if not GlobalUserItem.bSoundAble then
            btn_effect:loadTextures("setting_res/bt_off_1.png", "setting_res/bt_off_2.png", "setting_res/bt_off_1.png")
        else
            btn_effect:loadTextures("setting_res/bt_on_1.png", "setting_res/bt_on_2.png", "setting_res/bt_on_1.png")
        end
        
        if (self._tag == 0) and not (GlobalUserItem.bVoiceAble and GlobalUserItem.bSoundAble) then
            self._tag = 1
        end

        btn_music:addTouchEventListener(function( sender,eventType )

            if eventType == ccui.TouchEventType.ended then

                GlobalUserItem.bVoiceAble = not GlobalUserItem.bVoiceAble
                print("GlobalUserItem.bVoiceAble",GlobalUserItem.bVoiceAble)
                GlobalUserItem.setVoiceAble(GlobalUserItem.bVoiceAble)
                if GlobalUserItem.bVoiceAble == true then
                    ExternalFun.playBackgroudAudio("background.mp3")
                end
                if not GlobalUserItem.bVoiceAble then
                    --AudioEngine.setMusicVolume(1.0)   
                    btn_music:loadTextures("setting_res/bt_off_1.png", "setting_res/bt_off_2.png", "setting_res/bt_off_1.png")
                else
                    --AudioEngine.setMusicVolume(0)
                    btn_music:loadTextures("setting_res/bt_on_1.png", "setting_res/bt_on_2.png", "setting_res/bt_on_1.png")
                end
            end
        end)

        btn_effect:addTouchEventListener(function( sender,eventType )
            if eventType == ccui.TouchEventType.ended then

                GlobalUserItem.bSoundAble = not GlobalUserItem.bSoundAble
                print("GlobalUserItem.bSoundAble",GlobalUserItem.bSoundAble)
                GlobalUserItem.setSoundAble(GlobalUserItem.bSoundAble)
                if not GlobalUserItem.bSoundAble then
                    --AudioEngine.resumeMusic() 
                    btn_effect:loadTextures("setting_res/bt_off_1.png", "setting_res/bt_off_2.png", "setting_res/bt_off_1.png")
                else
                    --AudioEngine.pauseMusic() -- 暂停音乐
                    btn_effect:loadTextures("setting_res/bt_on_1.png", "setting_res/bt_on_2.png", "setting_res/bt_on_1.png")
                end

                
            end
        end)

        --解散
        local btnJieSan = csbNode:getChildByName("BtnJieSan")
        btnJieSan:addTouchEventListener(function( sender,eventType )
            if eventType == ccui.TouchEventType.ended then
                bg:removeFromParent()
                 PriRoom:getInstance():queryDismissRoom()
            end
        end)
        --暂离
        local btnZanLi = csbNode:getChildByName("BtnZanLi")
        btnZanLi:addTouchEventListener(function( sender,eventType )
            if eventType == ccui.TouchEventType.ended then
                bg:removeFromParent()
                 self._scene._scene._gameFrame:StandUp(false)
            end
        end)

    elseif TAG_ENUM.BT_DELAREWAR == tag then
        self:hideDeclareWarBtns()
        self._scene:sendDelareWar(1)
    elseif TAG_ENUM.BT_NO_DELAREWAR == tag then
        self:hideDeclareWarBtns()
        self._scene:sendDelareWar(0)
    elseif TAG_ENUM.BT_MENU == tag then
        --self.m_tabNodeCards[cmd.MY_VIEWID]:dragCards({})

        local isVis = self.m_menu_bg:isVisible()
        self.m_bMenuVisible = not self.m_bMenuVisible
        --print("---- TAG_ENUM.BT_MENU ----", self.m_bMenuVisible)
        ---[[
        if self.m_bMenuVisible == true then
            self.m_menu_bg:runAction(cc.ScaleTo:create(0.3, 1.0))
        else
            self.m_menu_bg:runAction(cc.ScaleTo:create(0.3, 0.01))
        end

    elseif TAG_ENUM.BT_DISSOLVE == tag then
        self.m_time_bg:runAction(cc.RotateBy:create(0.5,-90))
        self.m_time_num:runAction(cc.RotateBy:create(0.5,90))
    elseif TAG_ENUM.BT_TRU == tag then          --托管
        self.m_bTrustee = not self.m_bTrustee
        self:onGameTrusteeship(self.m_bTrustee)
        local cbTrustee = 1
        if not self.m_bTrustee then
            cbTrustee = 0
        end
        self._scene:sendTrustees(cbTrustee)
    elseif TAG_ENUM.BT_SET == tag then          --设置

    elseif TAG_ENUM.BT_EXIT == tag then         --退出
        -- self:getParentNode():onQueryExitGame()
        self._scene._gameFrame:StandUp(false)
    elseif TAG_ENUM.BT_READY == tag then        --准备
        self:onClickReady()
    elseif TAG_ENUM.BT_INVITE == tag then       -- 邀请
        GlobalUserItem.bAutoConnect = false
        self:getParentNode():getParentNode():popTargetShare(function(target, bMyFriend)
            bMyFriend = bMyFriend or false
            local function sharecall( isok )
                if type(isok) == "string" and isok == "true" then
                    --showToast(self, "分享成功", 2)
                end
                GlobalUserItem.bAutoConnect = true
            end
            local shareTxt = "510k游戏精彩刺激, 一起来玩吧! "
            local url = GlobalUserItem.szSpreaderURL or yl.HTTP_URL
            if bMyFriend then
                PriRoom:getInstance():getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function( frienddata )
                    dump(frienddata)
                end)
            elseif nil ~= target then
                MultiPlatform:getInstance():shareToTarget(target, sharecall, "510k游戏邀请", shareTxt, url, "")
            end
        end)        
    elseif TAG_ENUM.BT_PASS == tag then         --不出
        self:onPassOutCard()
        self:hideOutCardsBtns()
    elseif TAG_ENUM.BT_SUGGEST == tag then      --提示
        --print("------- BT_SUGGEST -------")
        self.m_bClickSuggest = true
        self:onPromptOut(false)    
    elseif TAG_ENUM.BT_OUTCARD == tag then      --出牌
        local sel = self.m_tabNodeCards[cmd.MY_VIEWID]:getSelectCards()
        --dump(sel,"outCards",6)
        -- 扑克对比
        self:getParentNode():compareWithLastCards(sel, cmd.MY_VIEWID)

        --self.m_onGameControl:setVisible(false)
        self:hideOutCardsBtns()
        local vec = self.m_tabNodeCards[cmd.MY_VIEWID]:outCard(sel)
        self:outCardEffect(cmd.MY_VIEWID, sel, vec)
        self._scene:sendOutCard(sel)
    elseif TAG_ENUM.BT_NO_ASK == tag then
        self._scene:sendAskFriend(0)
        self:hideAskFriendBtns()
    elseif TAG_ENUM.BT_ASK == tag then
        self._scene:sendAskFriend(1)
        self:hideAskFriendBtns()
    elseif TAG_ENUM.BT_NO_ADD == tag then
        self._scene:sendAddTimes(0)
        self:hideAddTimesBtns()
    elseif TAG_ENUM.BT_ADD == tag then
        self._scene:sendAddTimes(1)
        self:hideAddTimesBtns()
    elseif TAG_ENUM.BT_VOICE == tag then
        self._scene:sendAddTimes(1)
        self:hideAddTimesBtns()
    elseif TAG_ENUM.BT_EXCHANGE_2 == tag then
        local meChiardID = self._scene:GetMeChairID()
        local toChairId = (meChiardID + 1) % cmd.PLAYER_COUNT
        print("BT_EXCHANGE_2 called meChiardID = ", meChiardID, "  toChairId = ", toChairId)
        self._scene._gameFrame:QueryChangeDesk(toChairId)

    elseif TAG_ENUM.BT_EXCHANGE_3 == tag then
        local meChiardID = self._scene:GetMeChairID()
        local toChairId = (meChiardID + 2) % cmd.PLAYER_COUNT
        print("BT_EXCHANGE_3 called meChiardID = ", meChiardID, "  toChairId = ", toChairId)
        self._scene._gameFrame:QueryChangeDesk(toChairId)

    elseif TAG_ENUM.BT_EXCHANGE_4 == tag then
        local meChiardID = self._scene:GetMeChairID()
        local toChairId = (meChiardID + 3) % cmd.PLAYER_COUNT
        print("BT_EXCHANGE_4 called meChiardID = ", meChiardID, "  toChairId = ", toChairId)
        self._scene._gameFrame:QueryChangeDesk(toChairId)
    elseif TAG_ENUM.BT_SORT_CARD == tag then
        print("sort card btn called")

        self.m_tabNodeCards[cmd.MY_VIEWID]:sortCard()
    end
end

function GameViewLayer:onClickReady()
    self.m_btnReady:setEnabled(false)
    self.m_btnReady:setVisible(false)
    
    self:getParentNode():sendReady()

    -- 界面清理
    self:reSetForNewGame()

end

-- 出牌效果
-- @param[outViewId]        出牌视图id
-- @param[outCards]         出牌数据
-- @param[vecCards]         扑克精灵
function GameViewLayer:outCardEffect(outViewId, outCards, vecCards)
    local controlSize = self.m_outCardsControl:getContentSize()

    cc.Director:getInstance():getTextureCache():addImage("game/yule/510k/res/effect/LZ_NewPokerType0.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/510k/res/effect/LZ_NewPokerType0.plist")

    cc.Director:getInstance():getTextureCache():addImage("game/yule/510k/res/effect/LZ_NewPokerType1.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/510k/res/effect/LZ_NewPokerType1.plist")

    cc.Director:getInstance():getTextureCache():addImage("game/yule/510k/res/effect/LZ_NewPokerType2.png")
    cc.SpriteFrameCache:getInstance():addSpriteFrames("game/yule/510k/res/effect/LZ_NewPokerType2.plist")


    -- 移除出牌
    self.m_outCardsControl:removeChildByTag(outViewId)
    local holder = cc.Node:create()
    self.m_outCardsControl:addChild(holder)
    holder:setTag(outViewId)

    local outCount = #outCards
    -- 计算牌型
    local cardType = CardLogic.GetCardType(outCards, outCount)

    -- 出牌
    local targetPos = cc.p(0, 0)
    local center = outCount * 0.5
    local scale = 0.5
    local nodeCard = self.m_tabNodeCards[outViewId]
    local holderPos = nodeCard:getParent():convertToWorldSpace(cc.p(nodeCard:getPositionX(), nodeCard:getPositionY()))
    local effectPosX = 0
    local effectPosY = - yl.HEIGHT * 0.1
    holder:setPosition(holderPos)
    --dump(holderPos, "---- outCardEffect Position ----", 6)
    if cmd.MY_VIEWID == outViewId then
        scale = 0.6
        targetPos = holder:convertToNodeSpace(cc.p(controlSize.width * 0.48, controlSize.height * 0.42))
    elseif cmd.LEFT_VIEWID == outViewId then
        center = 0
        holder:setAnchorPoint(cc.p(0, 0.5))
        targetPos = holder:convertToNodeSpace(cc.p(controlSize.width * 0.33 - 80, controlSize.height * 0.58))
        effectPosX = -yl.WIDTH * 0.32
        effectPosY = yl.HEIGHT * 0.1
    elseif cmd.RIGHT_VIEWID == outViewId then
        center = outCount
        holder:setAnchorPoint(cc.p(1, 0.5))
        targetPos = holder:convertToNodeSpace(cc.p(controlSize.width * 0.67 + 80, controlSize.height * 0.58))
        effectPosX = yl.WIDTH * 0.32
        effectPosY = yl.HEIGHT * 0.1
    elseif cmd.TOP_VIEWID == outViewId then
        center = outCount / 2
        holder:setAnchorPoint(cc.p(0.5, 0.5))
        targetPos = holder:convertToNodeSpace(cc.p(controlSize.width * 0.48, controlSize.height * 0.72))
        effectPosX = 0
        effectPosY = yl.HEIGHT * 0.3
    end
    effectPosX = 0
    effectPosY = yl.HEIGHT * 0.05


    --对节点排序
    table.sort( vecCards, function(a, b)
        local av = CardLogic.GetCardFactValue(a:getCardData())
        local bv = CardLogic.GetCardFactValue(b:getCardData())
        return av > bv
    end )

    for k,v in pairs(vecCards) do
        v:retain()
        v:removeFromParent()
        holder:addChild(v)
        v:release()

        v:setLocalZOrder(k)

        v:showCardBack(false)
        local pos = cc.p((k - center) * (CardsNode.CARD_X_DIS + 15) * scale + targetPos.x, targetPos.y)
        local moveTo = cc.MoveTo:create(0.15, pos)
        local spa = cc.Spawn:create(moveTo, cc.ScaleTo:create(0.15, scale))
        v:stopAllActions()
        v:runAction(spa)
    end

    --print("## 出牌类型")
    --print(cardType)
    --print("## 出牌类型")
    local headitem = self.m_tabUserItem[outViewId]
    if nil == headitem then
        print("===== outCardEffect headitem ====",nil)
        return
    end
    local deleyTime = 0.35
    -- 牌型音效
    local bCompare = self:getParentNode().m_bLastCompareRes
    bCompare = false
    print("===== outCardEffect cardType ====",cardType)
    if CardLogic.CT_SINGLE == cardType then
        -- 音效
        local poker = CardLogic.GetCardFactValue(outCards[1])
        if nil ~= poker then
            ExternalFun.playSoundEffect(""..headitem.cbGender.."/"..poker .. ".mp3", headitem.m_userItem) 
        end
    elseif CardLogic.CT_ERROR == cardType then

    elseif CardLogic.CT_DOUBLE == cardType then
        local poker = CardLogic.GetCardFactValue(outCards[1])
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/Dui" .. poker .. ".mp3", headitem.m_userItem) 
    elseif CardLogic.CT_SINGLE_LINE == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/DanLong.mp3", headitem.m_userItem) 
        self:playS_D_T_Effect(1)
    elseif CardLogic.CT_DOUBLE_LINE == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/ShuangLong.mp3", headitem.m_userItem)
        self:playS_D_T_Effect(2)
    elseif CardLogic.CT_THREE_LINE == cardType then
        self:playS_D_T_Effect(3)
        -- ExternalFun.playSoundEffect(""..headitem.cbGender.."/ShuangLong.mp3", headitem.m_userItem)
    elseif CardLogic.CT_BOMB_3_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/SanZhangPao.mp3", headitem.m_userItem)
        
        self:playBoomNormalEffect()
    elseif CardLogic.CT_510K_FALSE == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/510K.mp3", headitem.m_userItem)
        self:playBoomNormalEffect()
    elseif CardLogic.CT_B_S_WANG == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/DaXiaoWang.mp3", headitem.m_userItem)
    elseif CardLogic.CT_510K_TRUE == cardType then
        local color = CardLogic.GetCardFactColor(outCards[1])
        if color == CardLogic.COLOR_FANGKUAI then
            ExternalFun.playSoundEffect(""..headitem.cbGender.."/FangKuai510K.mp3", headitem.m_userItem)
        elseif color == CardLogic.COLOR_MEIHUA then
            ExternalFun.playSoundEffect(""..headitem.cbGender.."/MeiHua510K.mp3", headitem.m_userItem)
        elseif color == CardLogic.COLOR_HEITAO then
            ExternalFun.playSoundEffect(""..headitem.cbGender.."/HeiTao510K.mp3", headitem.m_userItem)
        elseif color == CardLogic.COLOR_HONGTAO then
            ExternalFun.playSoundEffect(""..headitem.cbGender.."/HongTao510K.mp3", headitem.m_userItem)
        end
        self:playBoomNormalEffect()
    elseif CardLogic.CT_S_S_WANG == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/DuiXiaoWang.mp3", headitem.m_userItem)
    elseif CardLogic.CT_B_B_WANG == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/DuiDaWang.mp3", headitem.m_userItem)
    elseif CardLogic.CT_BOMB_4_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/SiZhangPao.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    elseif CardLogic.CT_BOMB_5_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/WuZhangPao.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    elseif CardLogic.CT_THREE_KING == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/SanWang.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    elseif CardLogic.CT_BOMB_6_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/LiuZhangPao.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    elseif CardLogic.CT_BOMB_7_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/QiZhangPao.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    elseif CardLogic.CT_BOMB_ALL_KING_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/SigeWang.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    elseif CardLogic.CT_BOMB_8_CARD == cardType then
        ExternalFun.playSoundEffect(""..headitem.cbGender.."/BaZhangPao.mp3", headitem.m_userItem)
        self:playBoomBigEffect()
    else
        if bCompare then
            -- 音效
            ExternalFun.playSoundEffect(""..headitem.cbGender.."/".."ya" .. math.random(0, 1) .. ".wav", headitem.m_userItem) 
        else
            -- 音效
            ExternalFun.playSoundEffect( ""..headitem.cbGender.."/".."type" .. cardType .. ".wav", headitem.m_userItem)
        end
    end
end

function GameViewLayer:playBoomNormalEffect()
    local sp = cc.Sprite:create()
    sp:setPosition(cc.p(667, 475))
    self:addChild(sp, 15)

    local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.1
    param.m_strName = cmd.EFFECT_BOBM_NORMAL
    local animate = AnimationMgr.getAnimate(param)

    local animaterpt = cc.Sequence:create(animate, cc.RemoveSelf:create())
    sp:runAction(animaterpt)
end

function GameViewLayer:playBoomBigEffect()
    --火箭身
    local roket = cc.Sprite:create()
    roket:setSpriteFrame("roket_0.png")
    roket:setPosition(cc.p(667, 275))
    self:addChild(roket, 16)

    --火焰
    local sp = cc.Sprite:create()
    sp:setPosition(cc.p(675, 255))
    self:addChild(sp, 15)

    local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.1
    param.m_strName = cmd.EFFECT_ROKET_FIRE
    local animate = AnimationMgr.getAnimate(param)

    local animaterpt = cc.Sequence:create(animate, cc.RemoveSelf:create())
    sp:runAction(animaterpt)

    roket:runAction(
        cc.Sequence:create(
            cc.DelayTime:create(0.3), 
            cc.MoveBy:create(0.5, cc.p(0, 700)), 
            cc.RemoveSelf:create()))
end

--单龙，双龙，三龙 value 对应1，2，3
function GameViewLayer:playS_D_T_Effect(value)
    local ship = cc.Sprite:create()
    ship:setSpriteFrame("ship_ship_" .. value .. ".png")
    ship:setPosition(cc.p(934, 475))
    ship:setScale(0.3)
    self:addChild(ship, 16)

    
    local sp = cc.Sprite:create()
    sp:setPosition(cc.p(165, 95))
    ship:addChild(sp, 15)

    if value == 3 then
        sp:setPosition(cc.p(135, 95))
        sp:setRotation(10)
    end

    local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.05
    param.m_strName = cmd.EFFECT_SHIP_SEA
    local animate = AnimationMgr.getAnimate(param)

    local animaterpt = cc.RepeatForever:create(animate)
    sp:runAction(animaterpt)

    ship:runAction(
        cc.Sequence:create(
            cc.Spawn:create(
                cc.ScaleTo:create(0.8, 1),
                cc.MoveBy:create(0.8, cc.p(-500, 0))
                ),
            cc.FadeOut:create(0.2),
            cc.RemoveSelf:create()
            ))
end

function GameViewLayer:onChangePassBtnState( bEnable )
    self.m_bt_pass:setEnabled(bEnable)
    if bEnable then
        self.m_bt_pass:setOpacity(255)
    else
        self.m_bt_pass:setOpacity(125)
    end
end

function GameViewLayer:changeBtnState( targetBtn, bEnable )
    if nil == targetBtn then
        return
    end
    --print("changeBtnState",bEnable)
    targetBtn:setEnabled(bEnable)
    if bEnable then
        targetBtn:setOpacity(255)
    else
        targetBtn:setOpacity(125)
    end
end

function GameViewLayer:onPassOutCard()
    --print("---------- onPassOutCard ------------")
    self:getParentNode():sendOutCard({}, true)
    self.m_tabNodeCards[cmd.MY_VIEWID]:reSetCards()
    ExternalFun.playSoundEffect( ""..self:getParentNode():GetMeUserItem().cbGender.."/".."buyao.mp3", self:getParentNode():GetMeUserItem())
    --self.m_onGameControl:setVisible(false)
    -- 提示
   -- self.m_spInfoTip:setSpriteFrame("blank.png")
    -- 显示不出
    --[[
    local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame("game_nooutcard.png")
    if nil ~= frame then
        self.m_tabStateSp[cmd.MY_VIEWID]:setSpriteFrame(frame)
    end

    -- 音效
    
    --]]
end

function GameViewLayer:getUserNick( viewId )
    if nil ~= self.m_tabUserHead[viewId] then
        return self.m_tabUserHead[viewId].m_userItem.szNickName
    end
    return ""
end

------
-- 扑克代理

-- 扑克状态变更
-- @param[cbCardData]       扑克数据
-- @param[status]           状态(ture:选取、false:非选取)
-- @param[cardsNode]        扑克节点
function GameViewLayer:onCardsStateChange( cbCardData, status, cardsNode )

end

-- 扑克选择
-- @param[selectCards]      选择扑克
-- @param[cardsNode]        扑克节点
function GameViewLayer:onSelectedCards( selectCards, cardsNode )
    -- 出牌对比
    self.m_tabSelectCards = selectCards
    local outCards = self._scene.m_tabCurrentCards
    local outCount = #outCards

    local selectCount = #selectCards
    local selectType = CardLogic.GetCardType(selectCards, selectCount)

    -- print("------------- onSelectedCards selectType -----------",selectType,outCount,self.m_bCanOutCard)
    dump(outCards, "------------- outCards  -----------", 6)
    dump(selectCards, "------------- selectCards  -----------", 6)
    local enable = false
    local opacity = 125

    print("outCount = " .. outCount)
    print("selectCount = " .. selectCount)
    print("self.m_bCanOutCard = " .. tostring(self.m_bCanOutCard))
    print("selectType = " .. tostring(selectType))
    if 0 == outCount then
        if true == self.m_bCanOutCard and CardLogic.CT_ERROR ~= selectType then
            enable = true
            opacity = 255
        end        
    elseif CardLogic.compareCard(outCards, outCount, selectCards, selectCount) and true == self.m_bCanOutCard then
        enable = true
        opacity = 255
    end

    self.m_bt_outCard:setEnabled(enable)
    self.m_bt_outCard:setOpacity(opacity)
end

-- 牌数变动
-- @param[outCards]         出牌数据
-- @param[cardsNode]        扑克节点
function GameViewLayer:onCountChange( count, cardsNode, isOutCard )
    isOutCard = isOutCard or false
    local viewId = cardsNode.m_nViewId
    if nil ~= self.m_tabCardCount[viewId] then
        self.m_tabCardCount[cardsNode.m_nViewId]:setString(count .. "")
    end

    if count <= 2 and nil ~= self.m_tabSpAlarm[viewId] and isOutCard then
        local param = AnimationMgr.getAnimationParam()
        param.m_fDelay = 0.1
        param.m_strName = Define.ALARM_ANIMATION_KEY
        local animate = AnimationMgr.getAnimate(param)
        local rep = cc.RepeatForever:create(animate)
        self.m_tabSpAlarm[viewId]:runAction(rep)
        -- 音效
        ExternalFun.playSoundEffect( "common_alert.wav" )
    end
end

------
-- 扑克代理

-- 提示出牌
-- @param[bOutCard]        是否出牌
function GameViewLayer:onPromptOut( bOutCard )
    bOutCard = bOutCard or false
    if bOutCard then
        local promptCard = self:getParentNode().m_tabPromptCards
        local promptCount = #promptCard
        if promptCount > 0 then
            promptCard = CardLogic.sortCardList(promptCard, promptCount, CardLogic.TP_SORT_DES)

            -- 扑克对比
            self:getParentNode():compareWithLastCards(promptCard, cmd.MY_VIEWID)

            local vec = self.m_tabNodeCards[cmd.MY_VIEWID]:outCard(promptCard)
            self:outCardEffect(cmd.MY_VIEWID, promptCard, vec)
            self:getParentNode():sendOutCard(promptCard)
            --dump(promptCard, "---- 提示自动出牌 ----", 6)
            --self.m_onGameControl:setVisible(false)
        else
            self:onPassOutCard()
            self:hideOutCardsBtns()
        end
    else
        --没有大过的牌
        if #self:getParentNode().m_tabPromptList == 0 then
            self:onPassOutCard()
            self:hideOutCardsBtns()
        else
            print("1 m_promptIdx = ", m_promptIdx)
            if 0 == self.m_promptIdx or self.m_promptIdx > #self:getParentNode().m_tabPromptList then
                self.m_promptIdx = 1
            end
            
            print("2 m_promptIdx = ", m_promptIdx)
            if 0 ~= self.m_promptIdx then
                -- 提示回位
                local sel = self.m_tabNodeCards[cmd.MY_VIEWID]:getSelectCards()
                print("3 ---------- ", #sel, self.m_tabNodeCards[cmd.MY_VIEWID].m_bSuggested, #self:getParentNode().m_tabPromptList)
                if #sel > 0 
                    and self.m_tabNodeCards[cmd.MY_VIEWID].m_bSuggested
                    and #self:getParentNode().m_tabPromptList > 1 then
                    self.m_tabNodeCards[cmd.MY_VIEWID]:suggestShootCards(sel)
                end
                -- 提示扑克
                local prompt = self:getParentNode().m_tabPromptList[self.m_promptIdx]
                print("## 提示扑克")
                for k,v in pairs(prompt) do
                    print(yl.POKER_VALUE[v])
                end
                print("## 提示扑克")
                if table.nums(prompt) > 0 then
                    self.m_tabNodeCards[cmd.MY_VIEWID]:suggestShootCards(prompt)
                else
                    self:onPassOutCard()
                end
                self.m_promptIdx = self.m_promptIdx + 1
            else
                self:onPassOutCard()
                self:hideOutCardsBtns()
            end
        end
    end
end

function GameViewLayer:onGameTrusteeship( bTrusteeship )
    self.m_trusteeshipBg:setVisible(bTrusteeship)
    self:hideOutCardsBtns() 
    if bTrusteeship then
        if self.m_bMyCallBanker then
            self.m_bMyCallBanker = false
            --self.m_callScoreControl:setVisible(false)
           -- self:getParentNode():sendCallScore(255)
           self:hideOutCardsBtns()          
        end

        if self.m_bMyOutCards then
            self.m_bMyOutCards = false
            --self:onPromptOut(true)
        end
    end
end

function GameViewLayer:updateClock( clockId, cbTime)
    self.m_time_num:setString( string.format("%02d", cbTime ))
    if cbTime <= 0 then
        if cmd.TAG_COUNTDOWN_READY == clockId then
            --退出防作弊
            self:getParentNode():getFrame():setEnterAntiCheatRoom(false)
        elseif cmd.TAG_COUNTDOWN_DECLAREWAR == clockId then --宣战阶段时间到直接退出
            -- 私人房无自动托管
            if not GlobalUserItem.bPrivateRoom and self.m_nCountDownView == cmd.MY_VIEWID then
                --self:onGameTrusteeship(true)
                self._scene:onExitTable()
            end
        elseif cmd.TAG_COUNTDOWN_ADD_TIMES == clockId then --宣战阶段时间到直接退出
            -- 私人房无自动托管
            if not GlobalUserItem.bPrivateRoom and self.m_nCountDownView == cmd.MY_VIEWID then
                --self:onGameTrusteeship(true)
                self._scene:onExitTable()
            end            
        elseif cmd.TAG_COUNTDOWN_OUTCARD == clockId then
            -- 私人房无自动托管
            if not GlobalUserItem.bPrivateRoom  and self.m_nCountDownView == cmd.MY_VIEWID then
                self.m_bTrustee = true
                self:onGameTrusteeship(self.m_bTrustee)
                self._scene:sendTrustees(1)
            end
        end
    end
end

function GameViewLayer:OnUpdataClockView( chairId, cbTime )
    self.m_time_bg:setVisible(true)
    local viewId = self._scene:SwitchViewChairID(chairId)

    if viewId ~= cmd.INVALID_CHAIRID then
        self.m_nCountDownView = viewId
    end
    self.m_time_num:setString( string.format("%02d", cbTime ))

    if self:getParentNode():IsValidViewID(viewId) then
        self.m_time_bg:setPosition(ClockPosition[viewId])
    end
end
------------------------------------------------------------------------------------------------------------
--更新
------------------------------------------------------------------------------------------------------------

-- 文本聊天
function GameViewLayer:userChat(wViewChairId, chatString)
    if chatString and #chatString > 0 then
        -- self._chatLayer:showGameChat(false)
        --取消上次
        if self.chatDetails[wViewChairId] then
            self.chatDetails[wViewChairId]:stopAllActions()
            self.chatDetails[wViewChairId]:removeFromParent()
            self.chatDetails[wViewChairId] = nil
        end

        --创建label
        local limWidth = 24*10
        local labCountLength = cc.Label:createWithTTF(chatString,"fonts/round_body.ttf", 24)  
        if labCountLength:getContentSize().width > limWidth then
            self.chatDetails[wViewChairId] = cc.Label:createWithTTF(chatString,"fonts/round_body.ttf", 24, cc.size(limWidth, 0))
        else
            self.chatDetails[wViewChairId] = cc.Label:createWithTTF(chatString,"fonts/round_body.ttf", 24, cc.size(limWidth, 0))
        end

        self.chatDetails[wViewChairId]:setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT)
        self.chatDetails[wViewChairId]:setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_TOP)

        self.chatDetails[wViewChairId]:setLineBreakWithoutSpace(true)
        self.chatDetails[wViewChairId]:setMaxLineWidth(limWidth)
        self.chatDetails[wViewChairId]:setColor(cc.c3b(255, 255, 255))
        self.chatDetails[wViewChairId]:enableOutline(cc.c4b(0,0,0, 255), 1)
        self.chatDetails[wViewChairId]:setAnchorPoint(cc.p(0.5, 1))
        self.chatDetails[wViewChairId]:addTo(self, 19)

        --行数
        local lineNum = self.chatDetails[wViewChairId]:getContentSize().height / 30 

        local qipaoWidth = self.chatDetails[wViewChairId]:getContentSize().width+38
        local qipaoHeight = self.chatDetails[wViewChairId]:getContentSize().height + lineNum * 30
        

        print("qipaoWidth = " .. qipaoWidth)
        if qipaoWidth < 194 then
            qipaoWidth = 194
        end

        print("qipaoHeight = " .. qipaoHeight)
        local dy = 0
        if qipaoHeight <= 60 then
            qipaoHeight = 30
        end

        --改变气泡大小
        self.chatBubble[wViewChairId]:setContentSize(qipaoWidth, qipaoHeight)
            :setVisible(true)

        local size = self.chatBubble[wViewChairId]:getContentSize()
        if wViewChairId == 2 then
            self.chatDetails[wViewChairId]:move(posqipaoChat[wViewChairId].x - size.width/2, posqipaoChat[wViewChairId].y - 10)
        else
            self.chatDetails[wViewChairId]:move(posqipaoChat[wViewChairId].x + size.width/2+10, posqipaoChat[wViewChairId].y - 10)
        end
        self.chatDetails[wViewChairId]:setScale(0)
        self.chatBubble[wViewChairId]:setScale(0)
        self.chatBubble[wViewChairId]:runAction(cc.ScaleTo:create(0.1, 1))
        --动作
        self.chatDetails[wViewChairId]:runAction(cc.Sequence:create(
            cc.ScaleTo:create(0.1, 1),
            cc.DelayTime:create(3),
            cc.CallFunc:create(function(ref)
                self.chatDetails[wViewChairId]:removeFromParent()
                self.chatDetails[wViewChairId] = nil
                self.chatBubble[wViewChairId]:setVisible(false)
            end)))
    end
end

-- 表情聊天
function GameViewLayer:onUserExpression(wViewChairId, wItemIndex, wToViewChairID)
    -- local roleItem = self.m_tabUserHead[viewId]
    -- if nil ~= roleItem then
    --     roleItem:browChat(chatdata.wItemIndex)
    -- end

    if wToViewChairID then --有To的就是互动表情
        print("互动表情 从 " .. wViewChairId .. " 发到 " .. wToViewChairID .. "表情ID: " .. wItemIndex)
        local fileName = string.format("client/res/public/dy_expression/pic/expression_icon_%d.png", (wItemIndex - 100))

        local runScene = cc.Director:getInstance():getRunningScene()

        local sp = cc.Sprite:create(fileName)
        sp:setPosition(self.m_tabPopHeadPos[wViewChairId])
        self:addChild(sp, 15)

        local endPosition = self.m_tabPopHeadPos[wToViewChairID]
        local dx = 0
        local dy = 0
        local needRotate = false
        local delayTime = 0
        local fDelay = 0.15
        if wItemIndex == 101 then
            dx = -30
            dy = 30
            needRotate = false
        elseif wItemIndex == 102 then
            dx = -30
            dy = 30
            needRotate = true
        elseif wItemIndex == 103 then
            needRotate = true
        elseif wItemIndex == 104 then
            dy = 30
            needRotate = false
            delayTime = fDelay * 2
        elseif wItemIndex == 105 then
            needRotate = true
        elseif wItemIndex == 106 then
            needRotate = false
        elseif wItemIndex == 107 then
            dy = 20
            needRotate = true
        elseif wItemIndex == 108 then
            dx = -30
            dy = -30
            needRotate = true
            delayTime = fDelay * 4
        end

        local callfunc = function(  )
            local param = AnimationMgr.getAnimationParam()
            param.m_fDelay = fDelay
            param.m_strName = string.format("dy_expression_%d_ani_key", (wItemIndex - 100))
            local animate = AnimationMgr.getAnimate(param)

            sp:setRotation(0)

            sp:runAction(cc.Sequence:create(cc.DelayTime:create(delayTime), cc.CallFunc:create(function()
                --播放声音
                local sound_path = string.format("client/res/public/dy_expression/sound/expression_%d.mp3", wItemIndex)
                if GlobalUserItem.bSoundAble then
                    AudioEngine.playEffect(sound_path, false)
                end
            end)))

            local animaterpt = cc.Sequence:create(animate, cc.RemoveSelf:create())
            sp:runAction(animaterpt)
        end

        local spawnAction = nil
        if needRotate then
            spawnAction = cc.Spawn:create(
                    cc.RotateBy:create(0.5, 1440),
                    cc.MoveTo:create(0.5, cc.p(endPosition.x + dx, endPosition.y + dy))
                    )
        else
            spawnAction = cc.Spawn:create(
                    cc.MoveTo:create(0.5, cc.p(endPosition.x + dx, endPosition.y + dy))
                    )
        end

        sp:runAction(cc.Sequence:create(
                spawnAction,
                cc.CallFunc:create(callfunc)
                -- cc.RemoveSelf:create()
            ))

        return
    end

    if wItemIndex and wItemIndex >= 0 then
        -- self._chatLayer:showGameChat(false)
        --取消上次
        if self.chatDetails[wViewChairId] then
            self.chatDetails[wViewChairId]:stopAllActions()
            self.chatDetails[wViewChairId]:removeFromParent()
            self.chatDetails[wViewChairId] = nil
        end

        print("face wItemIndex = " .. wItemIndex)
        cc.SpriteFrameCache:getInstance():addSpriteFrames("client/res/Chat/chat.plist")

        self.chatDetails[wViewChairId] = cc.Sprite:create()
            :setAnchorPoint(cc.p(0.5, 0.5))
            :addTo(self, 19)

        
        self.chatDetails[wViewChairId]:setScale(1.0)
        --改变气泡大小
        self.chatBubble[wViewChairId]:setContentSize(90,100)
            :setVisible(false)

        local size = self.chatBubble[wViewChairId]:getContentSize()

        self.chatDetails[wViewChairId]:setPosition(cc.p(self.m_tabPopHeadPos[wViewChairId].x, self.m_tabPopHeadPos[wViewChairId].y + 12))

        ChatNode.playEmojAnimation(self.chatDetails[wViewChairId], cc.p(0,0), wItemIndex)

        self.chatDetails[wViewChairId]:runAction(cc.Sequence:create(
            cc.DelayTime:create(3),
            cc.CallFunc:create(function(ref)
                self.chatDetails[wViewChairId]:removeFromParent()
                self.chatDetails[wViewChairId] = nil
                self.chatBubble[wViewChairId]:setVisible(false)
            end)))
    end


end

-- 语音聊天
function GameViewLayer:onUserVoice(filepath, viewId)
    local roleItem = self.m_tabUserHead[viewId]
    if nil ~= roleItem then
        roleItem:voiceChat(filepath)
    end
end
function GameViewLayer:removeHead(viewId)
    print("removeHead viewId == " , viewId , debug.traceback())
    local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", viewId))
    if head_bg then
        head_bg:setVisible(false)
        head_bg:removeChildByTag(TAG.Tag_Head)
    end
    self:setExchangeBtnStatus(viewId, true)
    self.m_tabUserItem[viewId] = nil
    self:onUserReady(viewId, false)
    self.m_tabNodeCards[viewId]:setVisible(false)
    self.m_historyScore[viewId].lTurnScore = 0
    self.m_historyScore[viewId].lCollectScore = 0
end
-- 用户更新
function GameViewLayer:OnUpdateUser(viewId, userItem, isHost)
    --print(" update user " .. viewId)
    dump(userItem, " ============ GameViewLayer 510k userItem ==========")
    local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", viewId))
    local head = head_bg:getChildByTag(TAG.Tag_Head)
    print(" update user ", viewId, isHost, head)
    if nil == userItem then
        if nil ~= head then
            convertToGraySprite(head.m_head.m_spRender)
        end
        head_bg:setVisible(false)
        self.m_tabUserItem[viewId] = nil
        self:onUserReady(viewId, false)
        self:setExchangeBtnStatus(viewId, true)
        -- self:removeHead(viewId)
        return
    end
    
    print("--- OnUpdateUser  userItem.cbUserStatus ---",userItem.cbUserStatus, userItem.dBeans)
    if userItem.cbUserStatus == yl.US_FREE then
        self:removeHead(viewId)
    elseif userItem.cbUserStatus == yl.US_OFFLINE then
        if nil ~= head then
            convertToGraySprite(head.m_head.m_spRender)
        end
    elseif userItem.cbUserStatus == yl.US_PLAYING then
        if nil ~= head then
            convertToNormalSprite(head.m_head.m_spRender)
        end     
    elseif userItem.cbUserStatus == yl.US_NULL then
        self:removeHead(viewId)
    end

    --[[
    local bHide = ((table.nums(self.m_tabUserHead)) == (self:getParentNode():getFrame():GetChairCount()))
    if not GlobalUserItem.bPrivateRoom then
        self.m_btnInvite:setVisible(not bHide)
        self.m_btnInvite:setEnabled(not bHide)
    end  
    --]]  
    self.m_btnInvite:setVisible(false)
    self.m_btnInvite:setEnabled(false)

    
    

    local bReady = userItem.cbUserStatus == yl.US_READY
    self:onUserReady(viewId, bReady)

    --print(string.format("-------------viewid %d userItem.wChairId %d--------------", viewId ,userItem.wChairID ))
    dump(self.m_tabUserItem[viewId], " ================= self.m_tabUserItem[viewId] ===============")
    if nil == self.m_tabUserItem[viewId] then
        self.m_tabUserItem[viewId] = userItem
        self.m_tabUserItemCopy[viewId] = userItem
        head_bg:setVisible(true)
        self:setExchangeBtnStatus(viewId, false)
        self.m_historyUseItem[viewId] = self.m_tabUserItem[viewId]
        head_bg:removeChildByTag(TAG.Tag_Head)

        local head = PopupInfoHead:createNormal(userItem, 82)
        --head:enableInfoPop(true, popPosition[viewId], popAnchor[viewId])
        head:enableInfoPop(true, self.m_tabPopHeadPos[viewId], self.m_tabPopHeadAnchorPoint[viewId], self._scene._gameFrame)
        head:setTag(TAG.Tag_Head)
        head:setPosition(cc.p(45,45))
        head:setLocalZOrder(TAG.Tag_Head)
        --head:enableHeadFrame(true, {_framefile = "land_headframe.png", _zorder = -1, _scaleRate = 0.75, _posPer = cc.p(0.5, 0.63)})
        head_bg:addChild(head)
        local text_nickName = head_bg:getChildByName("text_nickName")

        text_nickName:setString(userItem.szNickName)
        if GlobalUserItem.isAntiCheat() and viewId ~= cmd.MY_VIEWID then
            text_nickName:setString("玩家"..viewId)
        end
        --玩家当前游戏币数
        local player_score = head_bg:getChildByName("player_score")
        player_score:setString(string.format("%d",userItem.dBeans))
        --玩家当前游戏得分
        -- local text_curScore = head_bg:getChildByName("text_curScore")
        -- text_curScore:setString("当前得分:0")
    else
        self.m_tabUserItem[viewId] = userItem
        head_bg:setVisible(true)
        self:setExchangeBtnStatus(viewId, false)
        local player_score = head_bg:getChildByName("player_score")
        player_score:setString(string.format("%d",userItem.dBeans))
        if nil ~= head and userItem.cbUserStatus ~= yl.US_OFFLINE then
            print("==== head:setVisible  US_PLAYING2====:")
            convertToNormalSprite(head.m_head.m_spRender)
        end 


    end

    head = head_bg:getChildByTag(TAG.Tag_Head)
    if userItem.cbUserStatus == yl.US_FREE then
        self:removeHead(viewId)
    elseif userItem.cbUserStatus == yl.US_OFFLINE then
        if nil ~= head then
            convertToGraySprite(head.m_head.m_spRender)
        end
    elseif userItem.cbUserStatus == yl.US_PLAYING then
        if nil ~= head then
            convertToNormalSprite(head.m_head.m_spRender)
        end     
    elseif userItem.cbUserStatus == yl.US_NULL then
        self:removeHead(viewId)
    end

    if cmd.MY_VIEWID == viewId then
        self:reSetUserInfo()
    end
end

function GameViewLayer:setExchangeBtnStatus(viewId, visible)
    local exchangeSit = nil
    if viewId == 2 then
        exchangeSit = self.m_csbNode:getChildByName("ExchangeSit_2")
    elseif viewId == 3 then
        exchangeSit = self.m_csbNode:getChildByName("ExchangeSit_3")
    elseif viewId == 4 then
        exchangeSit = self.m_csbNode:getChildByName("ExchangeSit_4")
    end

    if exchangeSit then
        exchangeSit:setVisible(visible)
    end
end

function GameViewLayer:onUserReady(viewId, bReady)
    print("onUserReady viewId = ", viewId)
    --用户准备
    local tip_ready = self.m_csbNode:getChildByName(string.format("tip_ready_%d", viewId))
        
    if bReady then
        tip_ready:setVisible(true)
    else
        tip_ready:setVisible(false)
    end
end

function GameViewLayer:onGetCellScore( score )
    score = score or 0
    local str = ""
    if score < 0 then
        str = "." .. score
    else
        str = "" .. score        
    end 
    if string.len(str) > 11 then
        str = string.sub(str, 1, 11)
        str = str .. "///"
    end  
    --self.m_atlasDiFeng:setString(str) 
end

function GameViewLayer:onGetGameFree()
    self:setHuangShangFlag(nil)

    self._scene:OnResetGameEngine()
    self:updateRule()
    --self:openFindRriendLayer()
    --准备按钮
    self.m_btnReady:setEnabled(true)
    self.m_btnReady:setVisible(true)
end

function GameViewLayer:onGetGamePlay()
    self:reSetGame()
    self:reSetForNewGame()
    self.m_btnReady:setEnabled(false)
    self.m_btnReady:setVisible(false)
    self:hideOutCardsBtns()

    for i = 1, 9 do
        local papo = self.m_csbNode:getChildByName("Papo_" .. i)
        papo:setVisible(false)
    end

    
    self.m_tabNodeCards[cmd.MY_VIEWID].m_sortCard = false
end

function GameViewLayer:onGameStart()
    --self:resetTotalScore()
    self:updateRule()
    self.m_round_outTotalScore = self.m_csbNode:getChildByName("round_outTotalScore")
    self.m_round_outTotalScore:setString("0")
    for i = 1 ,cmd.PLAYER_COUNT do 
        tabCardsHandCount [i] =  cmd.NORMAL_COUNT
        local viewId = self._scene:SwitchViewChairID(i - 1)
        local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", viewId))
        local heart = head_bg:getChildByName("heart")
        heart:setVisible(false)
        if viewId ~= cmd.MY_VIEWID then
            self:updateCardsNodeLayout(viewId, 0, false)
            self.m_tabAlertSp[viewId]:setVisible(false)
        else
            self.m_cardNum1:setString(string.format("%d", 0))
            self.m_cardNum1:setVisible(false)
        end

        local tip_operate = self.m_csbNode:getChildByName(string.format("tip_operate_%d", i))
        tip_operate:setVisible(false)

        local tip_addTime = self.m_csbNode:getChildByName(string.format("tip_addTime_%d", i))
        tip_addTime:setVisible(false)

        self.m_outCardsControl:removeChildByTag(i)
        local text_curScore = head_bg:getChildByName("text_curScore")
        text_curScore:setString("当前得分:0")
    end

    self.m_historyUseItem =  self.m_tabUserItem 
    if self.m_nRoundCount == 0 then
        self:initHistoryScore()
    end
    
    self.m_nCurTotalScore = 0

    self.m_nMaxCallScore = 0
    if self._scene.m_wCurrentUser == self.m_wChairId then
        
    end

    local handCards = self.m_tabNodeCards[1]:getHandCards()

    --第一个出牌玩家
    local curView = self._scene:getCurrentUserView()
    local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", curView))
    local heart = head_bg:getChildByName("heart")
    heart:setVisible(true)

end

function GameViewLayer:onGameDeclareWar(wUser,bDeclare)

end

function GameViewLayer:onGameAskFriend(dataBuffer) 

end

function GameViewLayer:onGameAddTimes(dataBuffer) 
    local wAddTimesUser =  dataBuffer.wAddTimesUser
    local addViewId = self._scene:SwitchViewChairID(wAddTimesUser)
    if dataBuffer.bAddTimes then
        local tip_addTime = self.m_csbNode:getChildByName(string.format("tip_addTime_%d", addViewId))
        tip_addTime:setVisible(true)
    else
        local tip_no_addTime = self.m_csbNode:getChildByName(string.format("tip_no_addTime_%d", addViewId))
        tip_no_addTime:setVisible(true)
    end 
end

function GameViewLayer:SetFriendFlag(wChairID, cbFlag)
    if wChairID  == cmd.INVALID_CHAIRID then
        self._scene.m_tab_cbFriendFlag = {}
    else
        self._scene.m_tab_cbFriendFlag[wChairID + 1] = cbFlag
    end
end

function GameViewLayer:SetAddTimes(wChairID, bAddTimes)
    if wChairID  == cmd.INVALID_CHAIRID then
        self._scene.m_tab_cbAddTimesFlag = {}
    else
        if true == bAddTimes then
            self._scene.m_tab_cbAddTimesFlag[wChairID + 1] = 1
        else
            self._scene.m_tab_cbAddTimesFlag[wChairID + 1] = 2
        end
    end
        
end

-- 获取到扑克数据
-- @param[viewId] 界面viewid
-- @param[cards] 扑克数据
-- @param[bReEnter] 是否断线重连
-- @param[pCallBack] 回调函数
function GameViewLayer:onGetGameCard(viewId, cards, bReEnter, pCallBack)
    print("================ onGetGameCard ================",viewId, cards, bReEnter)
    -- if viewId == cmd.MY_VIEWID then
    --     self:findBlackFive(cards)
    -- end
    if bReEnter then
        self.m_tabNodeCards[viewId]:updateCardsNode(cards, (viewId == cmd.MY_VIEWID), false)
    else
        if nil ~= pCallBack then
            pCallBack:retain()
        end
        local call = cc.CallFunc:create(function()
            -- 非自己扑克
            self.m_tabNodeCards[cmd.LEFT_VIEWID]:updateCardsNode(cards, true, true)

            self.m_tabNodeCards[cmd.RIGHT_VIEWID]:updateCardsNode(cards, true, true)
            
            self.m_tabNodeCards[cmd.TOP_VIEWID]:updateCardsNode(cards, true, true)

            -- 自己扑克
            self.m_tabNodeCards[cmd.MY_VIEWID]:updateCardsNode(cards, true, true, pCallBack)

            -- 庄家扑克
            -- 50 525
            -- 50 720
            for k,v in pairs(self.m_tabBankerCard) do
                v:setVisible(true)
            end
        end)
        local call2 = cc.CallFunc:create(function()
            -- 音效
            --ExternalFun.playSoundEffect( "dispatch.wav" )
        end)
        local seq = cc.Sequence:create(call2, cc.DelayTime:create(0.3), call)
        self:stopAllActions()
        self:runAction(seq)
    end
end


-- 用户出牌
-- @param[curViewId]        当前出牌视图id
-- @param[lastViewId]       上局出牌视图id
-- @param[lastOutCards]     上局出牌
-- @param[bRenter]          是否断线重连
function GameViewLayer:onGetOutCard(curViewId, lastViewId, lastOutCards, bReEnter)
    --是否为断线重连
    bReEnter = bReEnter or false
    --删除紧着出牌玩家的桌面上打出的牌
    self.m_outCardsControl:removeChildByTag(curViewId)
    --判断紧接着出牌玩家的viewId是否有效
    if curViewId ~= cmd.INVALID_CHAIRID then
        --判断是否轮到自己出牌
        self.m_bMyOutCards = (curViewId == cmd.MY_VIEWID)
        --遍历上家出的牌 计算出牌总分
        for k,v in pairs (lastOutCards) do
            local turnScore = 0
            local logicValue = CardLogic.GetCardFactValue(v) 
            if logicValue == 0x05 then
                turnScore = 5
            elseif logicValue == 0x0A then
                turnScore = 10
            elseif logicValue == 0x0D then
                turnScore = 10
            end

            --计算本轮出牌的累计总分
            self.m_nCurTotalScore = self.m_nCurTotalScore + turnScore
        end
        --设置本轮出牌的总分
        self.m_round_outTotalScore:setString(string.format("%d", self.m_nCurTotalScore))

        --判断上把是否为自己出牌
        if lastViewId == cmd.MY_VIEWID then
            local handCards = self.m_tabNodeCards[1]:getHandCards()
            self.m_cardNum1:setVisible(false) --true
            self.m_cardNum1:setString(string.format("%d", #handCards))
            --算出牌分
        else
            local outCardsCount = #lastOutCards
            if nil == lastOutCards then
                outCardsCount = 0
            end
        end

        --判断自己是否为紧接着出牌的玩家
        if curViewId == cmd.MY_VIEWID then
            if self.m_trusteeshipBg:isVisible() then--自动提示出牌
                --self:onPromptOut(true)
            else
                --显示过、提示、出牌按钮
                self:presentOutCardsBtns()
                --默认出牌按钮不可用，待选择牌后方可用
                self.m_bt_outCard:setEnabled(false)
                --设置按钮透明度
                self.m_bt_outCard:setOpacity(125)

                --获取提示列表
                local promptList = self:getParentNode().m_tabPromptList
                dump(promptList, "---------- promptList ---------", 6)

                --如果有提示，则说明有可出的牌，否则没有大于上家的牌
                self.m_bCanOutCard = (#promptList > 0)
                self.m_bCanOutCard = true

                --判断能出大于上家的牌
                if not self.m_bCanOutCard then
                    self.m_spInfoTip:setVisible(true)
                else
                    self.m_spInfoTip:setVisible(false)
                    --设置出牌按钮可用
                    self.m_bt_outCard:setEnabled(true)
                    self.m_bt_outCard:setOpacity(255)

                    --获取已选择的手牌
                    local sel = self.m_tabNodeCards[cmd.MY_VIEWID]:getSelectCards()
                    --获取已选择的手牌数量
                    local selCount = #sel
                    --如果大于0，则说明有选择待出牌
                    if selCount > 0 then
                        --获取出牌的类型
                        local selType = CardLogic.GetCardType(sel, selCount)

                        --判断已选的牌是否符合出牌规则
                        if CardLogic.CT_ERROR ~= selType then
                            --获取上家出牌的数量
                            local lastOutCount = #lastOutCards
                            --如果上家出牌数量为0，则表示当前第一个出牌
                            if lastOutCount == 0 then
                                self.m_bt_outCard:setEnabled(true)
                                self.m_bt_outCard:setOpacity(255)
                            elseif lastOutCount > 0 and GameLogic:CompareCard(lastOutCards, lastOutCount, sel, selCount) then --判断是否大于上家的牌，大于则开启按钮
                                self.m_bt_outCard:setEnabled(true)
                                self.m_bt_outCard:setOpacity(255)
                            elseif false == GameLogic:CompareCard(lastOutCards, lastOutCount, sel, selCount) then --如果不大于上家的牌，则屏蔽按钮
                                self.m_bt_outCard:setEnabled(false)
                                self.m_bt_outCard:setOpacity(125)
                            end
                        end
                    else
                        --如果没有选择牌，则屏蔽按钮
                        self.m_bt_outCard:setEnabled(false)
                        self.m_bt_outCard:setOpacity(125)
                    end
                    --self.m_spInfoTip:setSpriteFrame("blank.png")
                end
            end
        end
    end

    -- 非自己出牌，显示出牌动画
    if (lastViewId ~= cmd.MY_VIEWID and #lastOutCards > 0) or (bReEnter) then --or self.m_trusteeshipBg:isVisible() 
        --播放出牌声音
        ExternalFun.playSoundEffect( "sendcard.wav" )
        --出牌动画 lastOutCards: 打出去的牌  bReEnter: 若为重连则不减少牌数
        local vec = self.m_tabNodeCards[lastViewId]:outCard(lastOutCards, bReEnter, bReEnter)

        --出牌效果
        self:outCardEffect(lastViewId, lastOutCards, vec)

        --在机器人的情况下，才可能 lastViewId == cmd.MY_VIEWID 目前已将机器人屏蔽，因此不可能出现改情况
        if lastViewId == cmd.MY_VIEWID then
            local handCards = self.m_tabNodeCards[1]:getHandCards()
            self.m_cardNum1:setVisible(false) --true
            self.m_cardNum1:setString(string.format("%d", #handCards))
        end
    end

    --切换出牌玩家
    self:startToPlay(curViewId)
end

-- 用户pass
-- @param[passViewId]       放弃视图id
function GameViewLayer:onGetPassCard( dataBuffer )
    print("GameViewLayer onGetPassCard ------------- 1")
    local passViewId = self._scene:SwitchViewChairID(dataBuffer.wPassCardUser)
    local curViewId = self._scene:SwitchViewChairID(dataBuffer.wCurrentUser)
    if passViewId ~= cmd.MY_VIEWID then
        local headitem = self.m_tabUserItem[passViewId]
        if nil ~= headitem then
            -- 音效
            ExternalFun.playSoundEffect( ""..headitem.cbGender.."/".."buyao.mp3", headitem.m_userItem)
        end        
    end
    print("GameViewLayer onGetPassCard ------------- 2")
    self.m_outCardsControl:removeChildByTag(passViewId)
    self.m_outCardsControl:removeChildByTag(curViewId)
    local tip_operate = self.m_csbNode:getChildByName(string.format("tip_operate_%d", passViewId))
    tip_operate:setVisible(true)
    print("GameViewLayer onGetPassCard ------------- 3")
    -- 显示不出
    local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame("game_nooutcard.png")
    if nil ~= frame then
        self.m_tabStateSp[passViewId]:setSpriteFrame(frame)
    end
    print("GameViewLayer onGetPassCard ------------- 4")
    if 1 == dataBuffer.cbTurnOver then
        self.m_cbScore[curViewId] = self.m_cbScore[curViewId] + dataBuffer.sPlayerScore[1][dataBuffer.wCurrentUser+1]
        local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", curViewId))
        local text_curScore = head_bg:getChildByName("text_curScore")
        text_curScore:setString(string.format("当前得分:%d", dataBuffer.sPlayerScore[1][dataBuffer.wCurrentUser+1]))

        --获取自己的座位ID
        local meChairdID = self._scene:GetMeChairID()
        local friendChairdID = (meChairdID + 2) % cmd.PLAYER_COUNT

        local aimChairId1 = (meChairdID + 1) % cmd.PLAYER_COUNT
        local aimChairId2 = (meChairdID + 3) % cmd.PLAYER_COUNT

        --两个人的总分
        local selfTotalScore = dataBuffer.sPlayerScore[1][meChairdID+1] + dataBuffer.sPlayerScore[1][friendChairdID+1]
        local otherTotalScore = dataBuffer.sPlayerScore[1][aimChairId1+1] + dataBuffer.sPlayerScore[1][aimChairId2+1]

        self._priView.m_selfScoreTxt:setString(selfTotalScore)
        self._priView.m_otherScoreTxt:setString(otherTotalScore)

        --算总分
        self.m_nCurTotalScore = self.m_nCurTotalScore + dataBuffer.sPlayerScore[1][dataBuffer.wCurrentUser+1]
        self.m_nCurTotalScore = 0
        self.m_round_outTotalScore:setString(string.format("%d", self.m_nCurTotalScore))
        for i = 1, cmd.PLAYER_COUNT do
            self.m_outCardsControl:removeChildByTag(i)
        end
    end  
    print("GameViewLayer onGetPassCard ------------- 5")
end

--展示剩余手牌
function GameViewLayer:displayRestHandCard(cardCounts, cardDatas)
    for i = 1, 4 do
        local count = cardCounts[1][i]
        local viewId = self._scene:SwitchViewChairID(i - 1)
        local nodeName = "DisplayHandCardNode_" .. viewId
        local node = self.m_csbNode:getChildByName(nodeName)
        local dx = 0
        local CARD_X_DIS = 25
        if viewId == 1 then             --自己从左向右显示
            dx = CARD_X_DIS
        elseif viewId == 2 then         --下家从右向左显示
            dx = -CARD_X_DIS
        elseif viewId == 3 then         --对家从右向左显示
            dx = -CARD_X_DIS
        elseif viewId == 4 then         --上家从左向右显示
            dx = CARD_X_DIS
        end

        for j = 1, count do
            local cardData = cardDatas[i][j]
            local spRes = GameLogic.getCardResByValue(cardData)
            local sp = cc.Sprite:create(spRes)
            sp:setScale(0.6)
            sp:setPosition(cc.p(0,0))
            sp.toPos = cc.p((j-1) * dx, 0)
            node:addChild(sp)
            if viewId == 1 or viewId == 4 then
                sp:setLocalZOrder(j)
            else
                sp:setLocalZOrder(count - j)    
            end
            sp:runAction(cc.Sequence:create(cc.MoveTo:create(0.1 + j*0.05, sp.toPos)))
        end
    end
end

-- 游戏结束
function GameViewLayer:onGetGameConclude( data )

    self:displayRestHandCard(data.cbCardCount, data.cbCardData)

    --更新爬坡显示
    local meChiardID = self._scene:GetMeChairID()
    for i = 1, 9 do
        local papoData = data.wPrisistInfo[meChiardID+1][i]
        local papo = self.m_csbNode:getChildByName("Papo_" .. i)
        if papoData == 1 then
            papo:setVisible(true)
            papo:setTexture("game/yule/510k/res/game_res/papo_yes.png")
        elseif papoData == 0 then
            papo:setVisible(true)
            papo:setTexture("game/yule/510k/res/game_res/papo_no.png")
        else
            papo:setVisible(false)
        end
    end

    -- 界面重置
    self.m_bTrustee = false
    self.m_bSelfDu = false 
    self.m_bHaveBlackFive = false
    self.m_nRoundCount = self.m_nRoundCount + 1
    self:reSetGame()
    self:hideOutCardsBtns()
    self:hideAddTimesBtns()
    self:hideAskFriendBtns()
    self:hideDeclareWarBtns()

    -- 显示准备
    self.m_btnReady:setEnabled(true)
    self.m_btnReady:setVisible(true)

    self.m_time_bg:setPosition(cc.p(0, 0))
    --历史记录
    self.m_wBankerUser = self._scene.m_wBankerUser

    for i = 1 ,cmd.PLAYER_COUNT do
        local viewId = self._scene:SwitchViewChairID(i - 1)
        self.m_lCollectScore[i]  =  self.m_historyScore[viewId].lCollectScore + data.sGameScore[1][i]
        local userItem = self.m_historyUseItem[viewId]
        if nil == userItem then
             userItem = self.m_tabUserItemCopy[viewId]
        end
        --self.m_tabDwUserID[viewId]  = userItem.dwUserID
        --历史成绩
        self.m_historyScore[viewId].lTurnScore = data.sGameScore[1][i]
        self.m_historyScore[viewId].lCollectScore = self.m_historyScore[viewId].lCollectScore + data.sGameScore[1][i]

        -- self.m_outCardsControl:removeChildByTag(viewId)
    end
    -- 取消托管
    if self.m_trusteeshipBg:isVisible() then
        self.m_trusteeshipBg:setVisible(false)
    end
    -- 结算
    if nil == self.m_resultLayer then
        self.m_resultLayer = ResultLayer:create(self)
        self.m_resultLayer:setPosition(cc.p(667, 375))
        self:addChild(self.m_resultLayer, 15)
    end

    self.m_resultLayer:setVisible(false)
    self.m_resultLayer:showGameResult(data)

    self:runAction(cc.Sequence:create(cc.DelayTime:create(3), cc.CallFunc:create(function(  )
        self:setHuangShangFlag(nil)
        self:setNiangNiangFlag(nil)
        self.m_resultLayer:setVisible(true)
        for i = 1, cmd.PLAYER_COUNT do
            local viewId = self._scene:SwitchViewChairID(i - 1)
            self.m_outCardsControl:removeChildByTag(viewId)
        end
    end)))

    self:updateHistoryScore()
    self.m_rootLayer:removeChildByName("__effect_ani_name__")
    for i = 1, cmd.PLAYER_COUNT do
        local viewId = self._scene:SwitchViewChairID(i - 1)
        local head_bg = self.m_csbNode:getChildByName(string.format("head_bg_%d", viewId))
        if nil ~= self.m_tabUserItem[viewId] then
            
            userItem = self.m_tabUserItem[i]
            local player_score = head_bg:getChildByName("player_score")
            player_score:setString(string.format("%d",userItem.dBeans))
        else
            player_score:setString(0)
        end
        self.m_cbScore[i] = 0
    end
    --警报灯
    for i = 2, 4 do
        self.m_tabAlertSp[i]:setVisible(false)
    end
end

function GameViewLayer:updateRule()
    local str = "game_res/large_A.png"

    if  GlobalUserItem.bPrivateRoom then

    else
        
    end  
end

function GameViewLayer:updateHistoryScore()
    local  record_bg = self.m_record_bg
    --dump(self.m_historyScore,"-------------- updateHistoryScore ---------------- ",6)
    for i = 1, cmd.PLAYER_COUNT do
        local viewId = self._scene:SwitchViewChairID(i - 1)
        local nickName = record_bg:getChildByName(string.format("nickName_%d", viewId))
        local userItem = self.m_historyUseItem[viewId]
        if nil ~= userItem then
            nickName:setString(self.m_historyUseItem[viewId].szNickName)
            self.m_tabRecordNickName[viewId]:setString(self.m_historyUseItem[viewId].szNickName)
        else
            nickName:setString("xx")
            self.m_tabRecordNickName[viewId]:setString("xx")
        end
        
        local lastRoundScore = record_bg:getChildByName(string.format("lastRoundScore_%d", viewId))
        lastRoundScore:setString(self.m_historyScore[viewId].lTurnScore)

        local totalScore = record_bg:getChildByName(string.format("totalScore_%d", viewId))
        totalScore:setString(self.m_historyScore[viewId].lCollectScore)
    end
end

function GameViewLayer:resetTotalScore()
    local  record_bg = self.m_record_bg

    for i = 1, 4 do
        local viewId = self._scene:SwitchViewChairID(i - 1)
        local totalScore = record_bg:getChildByName(string.format("totalScore_%d", viewId))
        totalScore:setString("0")
    end
end

function GameViewLayer:initHistoryScore()
    local  record_bg = self.m_record_bg
    --print(···)
    for i = 1, 4 do
        local viewId = self._scene:SwitchViewChairID(i - 1)
        local userItem = self.m_historyUseItem[viewId]
        local szNickName = ""
        if nil == userItem then
            szNickName = "玩家"..viewId
        else
            szNickName = userItem.szNickName
        end
        print("szNickName ",i,viewId,szNickName)
        local nickName = record_bg:getChildByName(string.format("nickName_%d", viewId))
        nickName:setString(szNickName)
        self.m_tabRecordNickName[viewId]:setString(szNickName)
        if GlobalUserItem.isAntiCheat() and viewId ~= cmd.MY_VIEWID then
            nickName:setString("玩家"..viewId)
            self.m_tabRecordNickName[viewId]:setString("玩家"..viewId)
        end
        
        local lastRoundScore = record_bg:getChildByName(string.format("lastRoundScore_%d", viewId))
        lastRoundScore:setString(0)

        local totalScore = record_bg:getChildByName(string.format("totalScore_%d", viewId))
        totalScore:setString(0)
    end
end

function GameViewLayer:setOperateTipVisible(viewId,bIsVisible)--不出提示
    print("setOperateTipVisible = " .. viewId .. "  bIsVisible = " .. tostring(bIsVisible))
    local operateTip = self.m_csbNode:getChildByName(string.format("tip_operate_%d", viewId))
    operateTip:setVisible(bIsVisible)
end

function GameViewLayer:hideAskFriendBtns()
end

function GameViewLayer:presentAskFriendBtns()
    if self.m_wChairId == self._scene.m_wBankerUser and self.m_bSelfDu == true  then --and self.m_bHaveBlackFive
        
    else
        
    end
end

function GameViewLayer:startToPlay(viewId)

    --检测 viewId 是否有效
    if viewId > #self.m_tabNodeCards or viewId < 1 then
        print("切换出牌玩家失败  viewId = " .. tostring(viewId))
        return 
    end

    print("切换出牌玩家 viewId = " .. viewId)
    if viewId == cmd.MY_VIEWID then
        self.m_tabNodeCards[viewId].m_turn = true
        self.m_bCanOutCard = true
        self:presentOutCardsBtns()
        self.m_time_bg:setVisible(true)
    else
        self.m_time_bg:setVisible(false)
        self.m_bCanOutCard = false
        self:hideOutCardsBtns()
        self.m_tabNodeCards[viewId].m_turn = false
    end
end

function GameViewLayer:presentDeclareWarBtns()

end

function GameViewLayer:hideDeclareWarBtns()

end

function GameViewLayer:presentOutCardsBtns()
    if self.m_trusteeshipBg:isVisible() then
        return
    end
    --轮到自己出牌前，清除相关数据，此处需要改
    -- self.m_tabNodeCards[cmd.MY_VIEWID]:dragCards(self.m_tabNodeCards[cmd.MY_VIEWID]:filterDragSelectCards(false))
    self.m_tabNodeCards[cmd.MY_VIEWID]:dragCards({})
    self.m_bt_pass:setVisible(true)
    self.m_bt_tip:setVisible(true)
    self.m_bt_outCard:setVisible(true)
end

function GameViewLayer:hideOutCardsBtns()
    self.m_bt_pass:setVisible(false)
    self.m_bt_tip:setVisible(false)
    self.m_bt_outCard:setVisible(false)
end

function GameViewLayer:presentAddTimesBtns()
    
end

function GameViewLayer:hideAddTimesBtns()
    
end

function GameViewLayer:hideAllAddTimesTip()
    for i = 1, cmd.PLAYER_COUNT do
        local tip_addTime = self.m_csbNode:getChildByName(string.format("tip_addTime_%d", i))
        tip_addTime:setVisible(false)

        local tip_no_addTime = self.m_csbNode:getChildByName(string.format("tip_no_addTime_%d", i))
        tip_no_addTime:setVisible(false)
    end
end

function GameViewLayer:setTrusteeBtnEnable(bEnable)
    -- self.m_bt_trusteeship:setEnabled(bEnable)
end



function GameViewLayer:openFindRriendLayer()
    local tChooseLayer = ChooseLayer:create(self)
    tChooseLayer:setPosition(cc.p(680,200))
    tChooseLayer:setTag(TAG.Tag_FindFriend)
    self:addChild(tChooseLayer, Define.TAG_ZORDER.Choose_ZORDER)
end

function GameViewLayer:closeFindRriendLayer( )
    self:removeChildByTag(TAG.Tag_FindFriend)
end

function GameViewLayer:initCardsNodeLayout()
    self.m_tabCardCount = {}
    for index = 2, cmd.PLAYER_COUNT do 
        local NodeCards = self.m_tabNodeCards[index]
        NodeCards:setVisible(false)
        NodeCards.m_cardsHolder:setVisible(false)
        local num = cc.Label:createWithCharMap("game_res/player_cardNum_small.png",29,38,string.byte("0"))
        num:setString(string.format("%d", 0))
        num:setTag(TAG.Tag_CardsNum)
        num:setVisible(false)
        -- table.insert(self.m_tabCardCount, num)
        self.m_tabCardCount[index] = num
        NodeCards:addChild(num, cmd.NORMAL_COUNT + 1)
        for i = 1, cmd.NORMAL_COUNT do
            local card = cc.Sprite:create(GameLogic.getCardBackRes())
            card:setScale(0.6)
            card:setTag(TAG.Tag_Cards + i)
            card:setPosition( GameLogic.tabCardPositionChange[index].x * (i - 1), GameLogic.tabCardPositionChange[index].y * (i - 1))
            card:setVisible(false)

            if 0 == GameLogic.tabCardNumPositionFlag[index] then
                card:setPosition( GameLogic.tabCardPositionChange[index].x * (i - 1), GameLogic.tabCardPositionChange[index].y * (i - 1))
            elseif 1 == GameLogic.tabCardNumPositionFlag[index] then
                local halfNum = math.floor(cmd.NORMAL_COUNT / 2)
                if cmd.NORMAL_COUNT / 2 == math.floor(cmd.NORMAL_COUNT / 2) then --是否为偶数
                    local dy = - GameLogic.CARD_HEIGHT_SMALL / GameLogic.CARD_DISTANCE_UNIT / 2
                    card:setPosition(GameLogic.tabCardPositionChange[index].x * (i - 1), GameLogic.tabCardPositionChange[index].y * (i - 1  - 6) + dy)
                else
                    card:setPosition(GameLogic.tabCardPositionChange[index].x * (i - 1), GameLogic.tabCardPositionChange[index].y * (i - 1  - 6))
                end
            end

            NodeCards:addChild(card, i)
            if i == cmd.NORMAL_COUNT then
                if 0 == GameLogic.tabCardNumPositionFlag[index] then
                    num:setPosition(cc.p(card:getPositionX(),0))
                elseif 1 == GameLogic.tabCardNumPositionFlag[index] then
                    num:setPosition(cc.p(0,card:getPositionY()))
                end
            end   
        end
    end

    
end

function GameViewLayer:updateCardsNodeLayout( viewid, cardCount,visible)
    print("========== updateCardsNodeLayout =========",viewid,cardCount,visible)
    --获取手牌节点
    local NodeCards = self.m_tabNodeCards[viewid]
    --设置为可见
    NodeCards:setVisible(visible)
    --获取当前手牌数量显示节点
    local num = NodeCards:getChildByTag(TAG.Tag_CardsNum)

    if not num then
        num = cc.Label:createWithCharMap("game_res/player_cardNum_small.png",29,38,string.byte("0"))
        num:setString(string.format("%d", 0))
        num:setTag(TAG.Tag_CardsNum)
        num:setVisible(false)
        NodeCards:addChild(num, cmd.NORMAL_COUNT + 1)
    end 
    --大于两张就显示数量，打完就不显示数量
    if  cardCount > 0 then
        num:setVisible(true)
    else
        num:setVisible(false)
    end

    




    --设置手牌数量
    num:setString(string.format("%d", cardCount))

    if viewid == cmd.MY_VIEWID then
        num:setVisible(false)
    end

    --遍历手牌
    for i = 1, cmd.NORMAL_COUNT do
        --获取牌节点
        local card = NodeCards:getChildByTag(TAG.Tag_Cards + i)
        if card ~= nil then
            --超出手牌总数的手牌不显示
            if i > cardCount then
                card:setVisible(false)
            else
                card:setVisible(true)
                --重置手牌位置
                card:setPosition( GameLogic.tabCardPositionChange[viewid].x * (i - 1), GameLogic.tabCardPositionChange[viewid].y * (i - 1))

                --GameLogic.tabCardNumPositionFlag[viewid] == 0 表示横向排布
                if 0 == GameLogic.tabCardNumPositionFlag[viewid] then
                    card:setPosition( GameLogic.tabCardPositionChange[viewid].x * (i - 1), GameLogic.tabCardPositionChange[viewid].y * (i - 1))
                elseif 1 == GameLogic.tabCardNumPositionFlag[viewid] then --GameLogic.tabCardNumPositionFlag[viewid] == 1 表示纵向排布
                    local halfNum = math.floor(cardCount / 2)
                    if math.mod(cardCount, 2) == 0 then --是否为偶数
                        local dy = GameLogic.CARD_HEIGHT_SMALL / GameLogic.CARD_DISTANCE_UNIT
                        if i > halfNum then
                            dy = GameLogic.CARD_HEIGHT_SMALL / GameLogic.CARD_DISTANCE_UNIT
                        end
                        dy = - GameLogic.CARD_HEIGHT_SMALL / GameLogic.CARD_DISTANCE_UNIT / 2
                        card:setPosition(GameLogic.tabCardPositionChange[viewid].x * (i - 1), GameLogic.tabCardPositionChange[viewid].y * (i - 1  - halfNum) + dy)
                    else
                        card:setPosition(GameLogic.tabCardPositionChange[viewid].x * (i - 1), GameLogic.tabCardPositionChange[viewid].y * (i - 1  - halfNum))
                    end
                end

                if i == cardCount then
                    if 0 == GameLogic.tabCardNumPositionFlag[viewid] then
                        num:setPosition(cc.p(card:getPositionX(),0))
                    elseif 1 == GameLogic.tabCardNumPositionFlag[viewid] then
                        num:setPosition(cc.p(0,card:getPositionY()))
                    end
                end   
            end
        end
    end
end

function GameViewLayer:findBlackFive( cards )
    if nil == cards or 0 == #cards then
        return
    end

    for k,v in pairs (cards) do
        if v == 0x45 then
            print("========= HaveBlackFive =========")
            self.m_bHaveBlackFive = true
            break
        end
    end 
end

return GameViewLayer