--
-- Author: zhong
-- Date: 2016-11-11 09:59:23
--
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local ClipText = appdf.req(appdf.EXTERNAL_SRC .. "ClipText")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")

local module_pre = "game.yule.510k.src"
local cmd = appdf.req(module_pre .. ".models.CMD_Game")

local ResultLayer = class("ResultLayer", cc.Layer)

local BT_CONTINUE = 101
local BT_QUIT = 102
local BT_CLOSE = 103

function ResultLayer.getTagSettle()
    return 
    {
        -- 用户名
        m_userName = "",
        -- 文本颜色
        nameColor = cc.c4b(255,255,255,255),
        -- 计算游戏币
        m_settleCoin = "",
        -- 文本颜色
        coinColor = cc.c4b(255,255,255,255),       
        -- 特殊标志
        m_cbFlag = cmd.kFlagDefault,
    }
end

function ResultLayer.getTagGameResult()
    return
    {
        -- 结果
        enResult = cmd.kDefault,
        -- 结算
        settles = 
        {
            ResultLayer.getTagSettle(),
            ResultLayer.getTagSettle(),
            ResultLayer.getTagSettle(),
        } 
    }
end

function ResultLayer:ctor( parent,rs)
    self.m_parent = parent
    self.m_rs = rs
    --注册node事件
    ExternalFun.registerTouchEvent(self, true)

    --加载csb资源
    local csbNode = ExternalFun.loadCSB("game_res/ResultLayer.csb", self)

    local bg = csbNode:getChildByName("bg")
    self.m_spBg = bg

    -- 结算精灵
    self.m_spResultSp = bg:getChildByName("spResult")

    local function btnEvent( sender, eventType )
        if eventType == ccui.TouchEventType.ended then
            self:onBtnClick(sender:getTag(), sender)
        end
    end
    -- 继续按钮
    local btn = bg:getChildByName("bt_again")
    btn:setTag(BT_CONTINUE)
    btn:addTouchEventListener(btnEvent)

    -- 退出按钮
    btn = bg:getChildByName("bt_exit")
    btn:setTag(BT_QUIT)
    btn:addTouchEventListener(btnEvent)
end

function ResultLayer:onTouchBegan(touch, event)
    return self:isVisible()
end

function ResultLayer:onTouchEnded(touch, event)
    local pos = touch:getLocation() 
    local m_spBg = self.m_spBg
    pos = m_spBg:convertToNodeSpace(pos)
    local rec = cc.rect(0, 0, m_spBg:getContentSize().width, m_spBg:getContentSize().height)
    if false == cc.rectContainsPoint(rec, pos) then
        self:hideGameResult()
    end
end

function ResultLayer:onBtnClick(tag, sender)
    if BT_CONTINUE == tag then
        self:hideGameResult()
        self.m_parent:onClickReady()
    elseif BT_QUIT == tag then
        self.m_parent:getParentNode():onQueryExitGame()
    elseif BT_CLOSE == tag then
        self:hideGameResult()
    end
end

function ResultLayer:hideGameResult()
    self:reSet()
    self:setVisible(false)
end

function ResultLayer:showGameResult( rs)
    self.m_rs = rs
    self:reSet()

    local meChairId = self.m_parent._scene:GetMeChairID()
    local friendChairdID = (meChairId + 2) % cmd.PLAYER_COUNT

    local aimChairId1 = (meChairId + 1) % cmd.PLAYER_COUNT
    local aimChairId2 = (meChairId + 3) % cmd.PLAYER_COUNT

    local selfIsWiner = false
    local selfIndex = -1
    local winer = {}
    local loser = {}
    local winerScore = 0
    local loserScore = 0
    for i = 1, cmd.PLAYER_COUNT do
        local isWin = rs.sGameScore[1][i] >= 0
        local viewId = self.m_parent._scene:SwitchViewChairID(i - 1)
        local userItem = self.m_parent.m_historyUseItem[viewId]
        if isWin then
            if not selfIsWiner then
                selfIsWiner = meChairId == (i - 1)
            end
            local item = {}
            item.userItem = userItem
            item.chairId = i-1
            table.insert(winer, item)
            winerScore = winerScore + rs.sPlayerScore[1][i]
        else
            local item = {}
            item.userItem = userItem
            item.chairId = i-1
            table.insert(loser, item)
            loserScore = loserScore + rs.sPlayerScore[1][i]
        end
    end

    local curResult_1 = self.m_spBg:getChildByName("curResult_1")
    curResult_1:setString(winerScore .. "分")

    local curResult_2 = self.m_spBg:getChildByName("curResult_2")
    curResult_2:setString(loserScore .. "分")

    local index = 1
    for k,item in pairs(winer) do
        local headBg = self.m_spBg:getChildByName(string.format("HeadBg_%d", index))
        local headBgSize = headBg:getContentSize()
        --头像
        local head = PopupInfoHead:createNormal(item.userItem, 45)
        head:setPosition(cc.p(headBgSize.width/2, headBgSize.height/2))
        head:setName("NAME_HeadInfo")
        headBg:addChild(head)

        local nameTxt = self.m_spBg:getChildByName(string.format("nickName_%d", index))
        nameTxt:setString(item.userItem.szNickName)

        local falgHN = self.m_spBg:getChildByName(string.format("falg_h_n_%d", index))
        local isHuangShang = rs.wWinOrder[1][1] == (item.chairId)
        local isNiangNiang = rs.wWinOrder[1][4] == (item.chairId)

        if isHuangShang then
            falgHN:setVisible(true)
            falgHN:setTexture("game/yule/510k/res/game_res/flag_huangshang.png")
        elseif isNiangNiang then
            falgHN:setVisible(true)
            falgHN:setTexture("game/yule/510k/res/game_res/flag_niangniang.png")
        else
            falgHN:setVisible(false)
        end

        if meChairId == item.chairId then
            selfIndex = index
        end

        local ddInfoFlag = self.m_spBg:getChildByName(string.format("DdInfoFlag_%d", index))
        ddInfoFlag:setVisible(true)
 
        if friendChairdID == item.chairId then
            ddInfoFlag:setTexture("game/yule/510k/res/game_res/head_league.png")
        else
            ddInfoFlag:setTexture("game/yule/510k/res/game_res/head_enemy.png")
        end

        index = index + 1
    end

    local winerBgImg = self.m_spBg:getChildByName("img_result_1")
    winerBgImg:loadTexture("result_res/win.png")
    -- winerBgImg:setContentSize(cc.size(696,150))

    for k,item in pairs(loser) do
        local headBg = self.m_spBg:getChildByName(string.format("HeadBg_%d", index))
        local headBgSize = headBg:getContentSize()
        --头像
        local head = PopupInfoHead:createNormal(item.userItem, 45)
        head:setPosition(cc.p(headBgSize.width/2, headBgSize.height/2))
        head:setName("NAME_HeadInfo")
        headBg:addChild(head)

        local nameTxt = self.m_spBg:getChildByName(string.format("nickName_%d", index))
        nameTxt:setString(item.userItem.szNickName)

        local falgHN = self.m_spBg:getChildByName(string.format("falg_h_n_%d", index))
        local isHuangShang = rs.wWinOrder[1][1] == (item.chairId)
        local isNiangNiang = rs.wWinOrder[1][4] == (item.chairId)

        if isHuangShang then
            falgHN:setVisible(true)
            falgHN:setTexture("game/yule/510k/res/game_res/flag_huangshang.png")
        elseif isNiangNiang then
            falgHN:setVisible(true)
            falgHN:setTexture("game/yule/510k/res/game_res/flag_niangniang.png")
        else
            falgHN:setVisible(false)
        end

        local ddInfoFlag = self.m_spBg:getChildByName(string.format("DdInfoFlag_%d", index))
        ddInfoFlag:setVisible(true)
        
        if meChairId == item.chairId then
            selfIndex = index
        end

        if friendChairdID == item.chairId then
            ddInfoFlag:setTexture("game/yule/510k/res/game_res/head_league.png")
        else
            ddInfoFlag:setTexture("game/yule/510k/res/game_res/head_enemy.png")
        end

        index = index + 1
    end

    local loserBgImg = self.m_spBg:getChildByName("img_result_2")
    loserBgImg:loadTexture("result_res/lose.png")

    local selfDdInfoFlag = self.m_spBg:getChildByName(string.format("DdInfoFlag_%d", selfIndex))
    selfDdInfoFlag:setVisible(false)

    if selfIsWiner then
        self.m_spBg:loadTexture("result_res/clear_bg_win.png")
    else
        self.m_spBg:loadTexture("result_res/clear_bg_lose.png")
    end
end

function ResultLayer:reSet()
    --删除头像
    for i = 1, cmd.PLAYER_COUNT do
        local headBg = self.m_spBg:getChildByName(string.format("HeadBg_%d", i))
        local head = headBg:getChildByName("NAME_HeadInfo")
        if head then
            head:removeFromParent()
        end
    end

    if true then 
        return
    end
    self.m_spResultSp:setVisible(false)
    for i = 1, 3 do
        -- 昵称
        self.m_tabClipNickName[i]:setString("")
        self.m_tabClipNickName[i]:setTextColor(cc.c4b(255,255,255,255))

        -- 游戏币
        self.m_tabTextCoin[i]:setString("")
        self.m_tabTextCoin[i]:setColor(cc.c4b(255,255,255,255))

        self.m_tabFlag[i]:setString("")
    end
end

return ResultLayer