local ScoreCardsNode = class("", cc.Node)

local cmd = appdf.req("game.yule.510k.src.models.CMD_Game")

local CSB_RES_PATH = "game/yule/510k/res/game_res/ScoreCardsNode.csb"

function ScoreCardsNode:ctor(gameView, record)
	self._gameView = gameView
	--二维数组，下标1为5，下标2为10，下标3为k
	self._record = record
	self:initCsb()
	self:setData(record)
end

function ScoreCardsNode:initCsb()
	local node =  cc.CSLoader:createNode(CSB_RES_PATH)
	self.m_node = node
	self:addChild(node)

	self.maskLayout = node:getChildByName("MaskPanel")

	self.rootNode = node:getChildByName("RootNode")

	self.closeBtn = self.rootNode:getChildByName("CloseBtn")

	local btnCallback = function (ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			self:onButtonClickedEvent(ref:getName(), ref)
		elseif eventType == ccui.TouchEventType.began then
		elseif eventType == ccui.TouchEventType.canceled then
		end
	end

	self.closeBtn:addTouchEventListener(btnCallback)
	self.maskLayout:addTouchEventListener(btnCallback)


end

function ScoreCardsNode:setData(record)
	self._record = record
	dump(record, "========== ScoreCardsNode record ==========")
	for i = 1, 8 do
		local nodeName = "CardModel_5_" .. i
		local card = self.m_node:getChildByName(nodeName)
		if record[1] and record[1][i] and record[1][i] ~= 0 then
			card:loadTexture("game/yule/510k/res/card_1/card_" .. record[1][i] .. ".png", ccui.TextureResType.localType)
			card:setVisible(true)
		else
			card:setVisible(false)
		end
	end

	for i = 1, 8 do
		local nodeName = "CardModel_10_" .. i
		local card = self.m_node:getChildByName(nodeName)
		if record[2] and record[2][i] and record[2][i] ~= 0 then
			card:loadTexture("game/yule/510k/res/card_1/card_" .. record[2][i] .. ".png", ccui.TextureResType.localType)
			card:setVisible(true)
		else
			card:setVisible(false)
		end
	end

	for i = 1, 8 do
		local nodeName = "CardModel_k_" .. i
		local card = self.m_node:getChildByName(nodeName)
		if record[2] and record[3][i] and record[3][i] ~= 0 then
			card:loadTexture("game/yule/510k/res/card_1/card_" .. record[3][i] .. ".png", ccui.TextureResType.localType)
			card:setVisible(true)
		else
			card:setVisible(false)
		end
	end
end

function ScoreCardsNode:onButtonClickedEvent(name, ref)
	if name == "CloseBtn" or name == "MaskPanel" then
		self:close()
	end
end

function ScoreCardsNode:close()
	self:removeFromParent()
end

return ScoreCardsNode