--
-- Author: zhong
-- Date: 2016-06-27 11:36:40
--
local module_pre = "game.yule.510k.src"
local GameLogic = appdf.req(module_pre .. ".models.GameLogic")
local CardSprite = class("CardSprite", cc.Sprite);

--纹理宽高
local CARD_WIDTH = 151
local CARD_HEIGHT = 180
local BACK_Z_ORDER = 2

------
--set/get
function CardSprite:setDispatched( var )
	self.m_bDispatched = var;
end

function CardSprite:getDispatched(  )
	if nil ~= self.m_bDispatched then
		return self.m_bDispatched;
	end
	return false;
end

function CardSprite:getCardData()
	return self.m_cardData
end

--拖动选择
function CardSprite:setCardDragSelect( var )
	self.m_bDragSelect = var
end

function CardSprite:getCardDragSelect()
	return self.m_bDragSelect
end

--弹出
function CardSprite:setCardShoot( var )
	self.m_bShoot = var
end

function CardSprite:getCardShoot()
	return self.m_bShoot
end
------

function CardSprite:ctor()
	self.m_cardData = 0
	self.m_cardValue = 0
	self.m_cardColor = 0
	self.m_bDispatched = false
	self.m_bDragSelect = false
	self.m_bShoot = false
	self.m_bIsBlack = false -- 是否一直暗
end

--创建卡牌
function CardSprite:createCard( cbCardData )
	local sp = CardSprite.new();

	local spRes = GameLogic.getCardResByValue(cbCardData)
	print("spRes = " .. spRes)
	sp:setScale(0.7)
	sp:setTexture(spRes)
	sp.m_cardData = cbCardData;
	sp.m_cardValue = yl.POKER_VALUE[cbCardData] --math.mod(cbCardData, 16)--bit:_and(cbCardData, 0x0F)
	sp.m_cardColor = yl.CARD_COLOR[cbCardData] --math.floor(cbCardData / 16)--bit:_rshift(bit:_and(cbCardData, 0xF0), 4)

	sp:updateSprite();
	--扑克背面
	sp:createBack();

	return sp;
end

--设置卡牌数值
function CardSprite:setCardValue( cbCardData )
	self.m_cardData = cbCardData;
	self.m_cardValue = yl.POKER_VALUE[cbCardData]  --math.mod(cbCardData, 16) --bit:_and(cbCardData, 0x0F)
	self.m_cardColor = yl.CARD_COLOR[cbCardData]  --math.floor(cbCardData / 16) --bit:_rshift(bit:_and(cbCardData, 0xF0), 4)

	self:updateSprite();
end

--更新纹理资源
function CardSprite:updateSprite()
	local m_cardData = self.m_cardData
	local m_cardValue = self.m_cardValue
	local m_cardColor = self.m_cardColor

	self:setTag(m_cardData);
	self:setTexture(GameLogic.getCardResByValue(m_cardData))
end

--显示扑克背面
function CardSprite:showCardBack( var )
	if nil ~= self.m_spBack then
		self.m_spBack:setVisible(var);
	end	
end

--扑克选择效果
function CardSprite:showSelectEffect(bSelect)
	if true == self.m_bIsBlack then
		bSelect = true
	end
	if nil == self.m_pMask then
		self.m_pMask = cc.Sprite:create(GameLogic.getCardResByValue(1))
		if nil ~= self.m_pMask then
			self.m_pMask:setColor(cc.BLACK)
			self.m_pMask:setOpacity(100)
			self.m_pMask:setPosition(CARD_WIDTH * 0.5, CARD_HEIGHT * 0.5)
			self:addChild(self.m_pMask)
		end
	end

	if nil ~= self.m_pMask then
		self.m_pMask:setVisible(bSelect)
	end	
end

--创建背面
function CardSprite:createBack( )
	local cardSize = self:getContentSize();
    local m_spBack = cc.Sprite:create(GameLogic.getCardBackRes());
    -- m_spBack:setScale()
    m_spBack:setPosition(cardSize.width * 0.5, cardSize.height * 0.5);
    m_spBack:setVisible(false);
    self:addChild(m_spBack);
    m_spBack:setLocalZOrder(BACK_Z_ORDER);
    self.m_spBack = m_spBack;
end

return CardSprite;