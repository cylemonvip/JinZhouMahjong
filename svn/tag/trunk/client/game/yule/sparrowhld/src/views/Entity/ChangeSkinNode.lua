local ChangeSkinNode = class("", cc.Node)

local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")
local CSB_RES_PATH = cmd.RES_PATH .. "game/ChangeSkinNode.csb"

function ChangeSkinNode:ctor(gameView)
	self._gameView = gameView
	self:initCsb()
end

function ChangeSkinNode:initCsb()
	local node =  cc.CSLoader:createNode(CSB_RES_PATH)
	self:addChild(node)

	self.maskLayout = node:getChildByName("MaskLayout")

	self.rootNode = node:getChildByName("RootNode")
	self.closeBtn = self.rootNode:getChildByName("CloseBtn")
	self.okBtn = self.rootNode:getChildByName("OkBtn")

	local btnCallback = function (ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			self:onButtonClickedEvent(ref:getName(), ref)
		elseif eventType == ccui.TouchEventType.began then
		elseif eventType == ccui.TouchEventType.canceled then
		end
	end

	self.okBtn:addTouchEventListener(btnCallback)
	self.closeBtn:addTouchEventListener(btnCallback)
	self.maskLayout:addTouchEventListener(btnCallback)

	
	local zbLayoutContainer = self.rootNode:getChildByName("ZBLayoutContainer")
	local pmLayoutContainer = self.rootNode:getChildByName("PMLayoutContainer")

	self.zbPage = ccui.PageView:create()
	self.zbPage:setTouchEnabled(true)
	local zbSize = zbLayoutContainer:getContentSize()
	self.zbPage:setContentSize(cc.size(zbSize.width-4, zbSize.height-4))
	self.zbPage:setPosition(zbLayoutContainer:getPosition())
	self.rootNode:addChild(self.zbPage, 10)

	for i=1,2 do
    	---创建layout，内容添加到layout
        local layout=ccui.Layout:create()
        layout:setContentSize(zbLayoutContainer:getContentSize())
        local zbsp = cc.Sprite:create(cmd.RES_PATH .. "game/background_sparrowHz_" .. (i-1) .. ".png")
        zbsp:setScale(0.5)	
        zbsp:setPosition(337.5, 190)
        layout:addChild(zbsp)
        self.zbPage:addPage(layout)---一个layout 为一个 page内容
    end

    self.pmPage = ccui.PageView:create()
	self.pmPage:setTouchEnabled(true)
	self.pmPage:setContentSize(pmLayoutContainer:getContentSize())
	self.pmPage:setPosition(pmLayoutContainer:getPosition())
	self.rootNode:addChild(self.pmPage, 10)

	for i=1,2 do
    	---创建layout，内容添加到layout
        local layout=ccui.Layout:create()
        layout:setContentSize(zbLayoutContainer:getContentSize())
        local CardUpSP = cc.Sprite:create(cmd.RES_PATH .. "skin/card_back_" .. (i-1) .. ".png")
        CardUpSP:setScale(1.2)	
        CardUpSP:setPosition(111, 92)
        layout:addChild(CardUpSP)

        local CardDownSp = cc.Sprite:create(cmd.RES_PATH .. "skin/card_up_" .. (i-1) .. ".png")
        CardDownSp:setScale(1.2)	
        CardDownSp:setPosition(111, 287)
        layout:addChild(CardDownSp)


        self.pmPage:addPage(layout)---一个layout 为一个 page内容
    end

    --获取本地缓存的当前桌布和牌面
    local zbIndex = cc.UserDefault:getInstance():getStringForKey("ZB_INDEX", 0)
    local pmIndex = cc.UserDefault:getInstance():getStringForKey("PM_INDEX", 0)	

    self.zbPage:setCurrentPageIndex(zbIndex)
    self.pmPage:setCurrentPageIndex(pmIndex)
end

function ChangeSkinNode:onButtonClickedEvent(name, ref)
	if name == "CloseBtn" or name == "MaskLayout" then
		self:close()
	elseif name == "OkBtn" then
		local bpmcgd = self:changeSKin()
		if bpmcgd then
			self._gameView:showTipPopup("您设置的牌面，将在下一局生效！")
		end
		self:close()
	end
end

function ChangeSkinNode:changeSKin()
    local yspmIndex = cc.UserDefault:getInstance():getStringForKey("PM_INDEX", 0)	

	--保存本地缓存的当前桌布和牌面
    cc.UserDefault:getInstance():setStringForKey("ZB_INDEX", self.zbPage:getCurrentPageIndex())
    cc.UserDefault:getInstance():setStringForKey("PM_INDEX", self.pmPage:getCurrentPageIndex())	

    self._gameView:initBackground(self.zbPage:getCurrentPageIndex())

    if tonumber(yspmIndex) ~= tonumber(self.pmPage:getCurrentPageIndex()) then
    	return true
    end

    return false
end

function ChangeSkinNode:close()
	self:removeFromParent()
end

return ChangeSkinNode