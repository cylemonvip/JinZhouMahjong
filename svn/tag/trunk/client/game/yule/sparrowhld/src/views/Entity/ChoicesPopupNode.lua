local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.GameLogic")
local Utils = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.utils.Utils")

local ChoicesPopupNode = class("ChoicesPopupNode", function ()
	return cc.Node:create()
end)

local CSB_RES_PATH = cmd.RES_PATH .. "ChoicesPopupNode.csb"

function ChoicesPopupNode:ctor(aChoice, gameViewLayer)
	self.gameViewLayer = gameViewLayer
	self.aChoice = aChoice
	--第一位是操作码
	self.opType = self.aChoice[1]
	--去掉操作码，只留需要显示的牌
	table.remove(self.aChoice, 1)

	self:initCsb()
	self:initChoice()
	self:initBgLayout() 
end

function ChoicesPopupNode:initCsb()
	self.rootNode =  cc.CSLoader:createNode(CSB_RES_PATH)
	self:addChild(self.rootNode)
end

function ChoicesPopupNode:initChoice()
	local choiceTemp = Utils.deepCopy(self.aChoice)
	table.sort( choiceTemp, function(a, b)
		return a < b
	end )

	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self.gameViewLayer._cardLayer._mjSkin

	local offsetWidth = 86

	--根据当前数据，绘制对应的麻将
	for i, value in ipairs(choiceTemp) do 
		--地面
		local strFileBackup =  skinPath .. "/pai_bg_zheng.png"
		local back = display.newSprite(strFileBackup)
			:move(50 + (i - 1) * offsetWidth, 0)
			:addTo(self.rootNode)

		local index = GameLogic.SwitchToCardIndex(value)
		local value, color  = GameLogic.GetNValueAndNColorByCardIndex(index)
		local strFile = skinPath .. "/card_color_"..color.."_num_"..value..".png"
		local font = cc.Sprite:create(strFile)
		font:move(50 + (i - 1) * offsetWidth, -10)
		font:addTo(self.rootNode)
	end
end

function ChoicesPopupNode:initBgLayout()
	self.bgLayout = self.rootNode:getChildByName("BgLayout")
	self.bgLayout:setContentSize(cc.size(#self.aChoice * 86 + 20, 140))

	self.bgLayout:addTouchEventListener(function (sender, event)
            if event == ccui.TouchEventType.began then
            elseif event == ccui.TouchEventType.moved then
            elseif event == ccui.TouchEventType.ended then
            	self.gameViewLayer._scene:sendOperateCard(self.opType, self.aChoice)
            	self.gameViewLayer._cardLayer:unpopupAllCard()
            	self.gameViewLayer:removeChoicesPopup()
            elseif event == ccui.TouchEventType.canceled then
            end
        end)
end

return ChoicesPopupNode