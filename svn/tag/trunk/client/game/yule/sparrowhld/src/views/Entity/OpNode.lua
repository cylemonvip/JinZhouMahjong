local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.GameLogic")
local Utils = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.utils.Utils")
local AnimationMgr = appdf.req(appdf.EXTERNAL_SRC .. "AnimationMgr")

local OpNode = class("OpNode", function ()
	return cc.Node:create()
end)

local CSB_RES_PATH = cmd.RES_PATH .. "game/OpNode.csb"

function OpNode:ctor(gameViewLayer)
	self.gameViewLayer = gameViewLayer
	self:initCsb()
end

--btnList 需要显示的按钮列表
--viewId 显示在哪个玩家前面
--sureMask 玩家点击的按钮
function OpNode:show(btnList, viewId, sureBtnName)
	viewId = viewId or cmd.MY_VIEWID
	sureBtnName = sureBtnName or ""

	self:setVisible(true)
	local size = self.rootNode:getContentSize()

	local btnCallback = function (ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			ref:runAction(cc.ScaleTo:create(0.1, 1.0))
			self:onButtonClickedEvent(ref:getTag(), ref)
		elseif eventType == ccui.TouchEventType.began then
			ref:runAction(cc.ScaleTo:create(0.1, 0.9))
		elseif eventType == ccui.TouchEventType.canceled then
			ref:runAction(cc.ScaleTo:create(0.1, 1.0))
		end
	end

	--如果是自己 那就是默认值
	local pos = cc.p(850, 200)
	local scale = 1
	local rotation = 0

	if viewId == 2 then
		pos = cc.p(285, 480)
		scale = 0.7
		rotation = 90
	elseif viewId == 1 then
		pos = cc.p(850, 600)
		scale = 0.7
		rotation = 0
	elseif viewId == 4 then
		pos = cc.p(1080, 480)
		scale = 0.7
		rotation = 90
	end

	local index = 1
	local passSp = cc.Sprite:create(cmd.RES_PATH .. "hzpic/btn_pass.png")
	local btn = self.btnDemo:clone()
	local btnSize = btn:getContentSize()
	passSp:setPosition(btnSize.width/2, btnSize.height/2)
	btn:addChild(passSp)
	btn:setTag(self.gameViewLayer.BT_PASS)
	btn:setPosition(cc.p(640 - (index - 1) * 170, 54))
	self.rootNode:addChild(btn)
	btn:setVisible(true)
	btn:setRotation(-rotation)

	--显示操作的手指
	if sureBtnName == "GUO" then
		local param = AnimationMgr.getAnimationParam()
        param.m_fDelay = 0.5
        param.m_strName = cmd.CLICK_PRESS_KEY
        local animate = AnimationMgr.getAnimate(param)


		local finger = cc.Sprite:create("client/res/public/click01.png")
		finger:setAnchorPoint(cc.p(0, 0.69))
		local btnSize = btn:getContentSize()
		finger:setPosition(cc.p(btnSize.width/2, btnSize.height/2))
		btn:addChild(finger)
		finger:runAction(cc.RepeatForever:create(animate))
	end

	--如果非回放，才添加监听
	if not self.gameViewLayer._scene._isReplay then
		btn:addTouchEventListener(btnCallback)
	end

	local sortBtnList = {}
	--排序 吃，碰，杠,胡
	if btnList["CHI"] then
		table.insert(sortBtnList, "CHI")
	end

	if btnList["PENG"] then
		table.insert(sortBtnList, "PENG")
	end

	if btnList["GANG"] then
		table.insert(sortBtnList, "GANG")
	end

	if btnList["HU"] then
		table.insert(sortBtnList, "HU")
	end

	self.btnList = btnList
	index = index + 1
	for k,v in pairs(sortBtnList) do
		local btn = self.btnDemo:clone()
		if v == "CHI" then
			self:createArmature("chi", btn)
			btn:setTag(self.gameViewLayer.BT_CHI)
		elseif v == "PENG" then
			self:createArmature("peng", btn)
			btn:setTag(self.gameViewLayer.BT_BUMP)
		elseif v == "GANG" then
			self:createArmature("gang", btn)
			btn:setTag(self.gameViewLayer.BT_BRIGDE)
		elseif v == "HU" then
			self:createArmature("hu", btn)
			btn:setTag(self.gameViewLayer.BT_WIN)
		end

		--显示操作的手指
		if sureBtnName == v then
			local param = AnimationMgr.getAnimationParam()
	        param.m_fDelay = 0.5
	        param.m_strName = cmd.CLICK_PRESS_KEY
	        local animate = AnimationMgr.getAnimate(param)


			local finger = cc.Sprite:create("client/res/public/click01.png")
			finger:setAnchorPoint(cc.p(0, 0.69))
			local btnSize = btn:getContentSize()
			finger:setPosition(cc.p(btnSize.width/2, btnSize.height/2))
			btn:addChild(finger)
			finger:runAction(cc.RepeatForever:create(animate))
		end
		btn:setVisible(true)
		btn:setPosition(cc.p(640 - (index - 1) * 170, 54))
		self.rootNode:addChild(btn)
		btn:setRotation(-rotation)
		index = index + 1

		--如果非回放，才添加监听
		if not self.gameViewLayer._scene._isReplay then
			btn:addTouchEventListener(btnCallback)
		end
	end

	self:setScale(scale)
	self:setRotation(rotation)
	self:setPosition(pos)
end

function OpNode:onButtonClickedEvent(tag, ref)
	self.gameViewLayer:onButtonClickedEvent(tag, ref)
end


function OpNode:hide()
	self.btnList = {}
	self:setVisible(false)
	self.rootNode:removeAllChildren()
end

function OpNode:initCsb()
	local node =  cc.CSLoader:createNode(CSB_RES_PATH)
	self:addChild(node)

	self.rootNode = node:getChildByName("RootNode")
	self.btnDemo = node:getChildByName("BtnDemo")
	self.btnDemo:setVisible(false)
end

function OpNode:createArmature(action, btn)
	local exportJsonName = "effect_ui_anniujujiao_chipenggang.ExportJson"
	local picName = "effect_ui_anniujujiao_chipenggang0.png"
	local plistName = "effect_ui_anniujujiao_chipenggang0.plist"
	local armatureName = "effect_ui_anniujujiao_chipenggang"
	if action == "hu" then
		exportJsonName = "effect_ui_anniujujiao_hu.ExportJson"
		picName = "effect_ui_anniujujiao_hu0.png"
		plistName = "effect_ui_anniujujiao_hu0.plist"
		action = "Animation1"
		armatureName = "effect_ui_anniujujiao_hu"
	end
	-- 加载动画所用到的数据
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("effect/"..picName,"effect/"..plistName,"effect/"..exportJsonName);
	-- 创建动画对象
	local armature = ccs.Armature:create(armatureName)  
	local btnSize = btn:getContentSize()
	-- 设置位置
	armature:setPosition(btnSize.width/2, btnSize.height/2)                    
	-- 设置动画对象执行的动画名称
	armature:getAnimation():play(action)            
	-- 把动画对象加载到场景内
	btn:addChild(armature)                      
end

return OpNode