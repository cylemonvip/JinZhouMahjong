local GameModel = appdf.req(appdf.CLIENT_SRC.."gamemodel.GameModel")

local GameLayer = class("GameLayer", GameModel)

local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.GameLogic")
local GameViewLayer = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.layer.GameViewLayer")
local ExternalFun =  appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Utils = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.utils.Utils")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")
local UserItem = appdf.req(appdf.CLIENT_SRC.."plaza.models.ClientUserItem")
local game_cmd = appdf.req(appdf.HEADER_SRC .. "CMD_GameServer")

function GameLayer:ctor(frameEngine, scene, param)
    GameLayer.super.ctor(self, frameEngine, scene, param)
    self._lastSendTime = os.time()

    cc.FileUtils:getInstance():addSearchPath(cmd.RES_PATH)
    cc.FileUtils:getInstance():addSearchPath(cmd.RES_PATH .. "/game/")

    cc.FileUtils:getInstance():addSearchPath(device.writablePath..cmd.RES_PATH, true)
    cc.FileUtils:getInstance():addSearchPath(device.writablePath..cmd.RES_PATH .. "/game/", true)
end

function GameLayer:onEnter()
	if self._isReplay then
    	self._replayControlNode =  cc.CSLoader:createNode("client/src/privatemode/plaza/res/room/ReplayerControlNode.csb")
    	self._replayControlNode:setPosition(cc.p(667, 403))
    	self._replayControlNode:addTo(self, 20)

    	local rootNode = self._replayControlNode:getChildByName("RootNode")
    	if GameplayerManager:getInstance():isTest() then
    		rootNode:setTouchEnabled(false)
    	end
    	
    	local txtProgress = rootNode:getChildByName("TxtProgress")
    	local progress = GameplayerManager:getInstance():getProgress()
    	txtProgress:setString("进度: ".. (progress * 100) .. "%")
    	local txtRate = rootNode:getChildByName("TxtRate")
    	local rate = GameplayerManager:getInstance():getRate()
    	txtRate:setString("速率: x"..rate)

    	local btnSlow = rootNode:getChildByName("BtnSlow")
    	local btnQuick = rootNode:getChildByName("BtnQuick")
    	local btnPlay = rootNode:getChildByName("BtnPlay")
    	local btnClose = rootNode:getChildByName("BtnClose")

    	local btnCallback = function(ref, eventType)
			if eventType == ccui.TouchEventType.began then
				ref:setScale(1.2)
	        elseif eventType == ccui.TouchEventType.ended 
	        	or eventType == ccui.TouchEventType.canceled then
	            ref:setScale(1.0)
	            local name = ref:getName()
	            if name == "BtnSlow" then
	            	GameplayerManager:getInstance():slow()
	            	local rate = GameplayerManager:getInstance():getRate()
    				txtRate:setString("速率: x"..rate)
	            elseif name == "BtnQuick" then
	            	GameplayerManager:getInstance():quick()
	            	local rate = GameplayerManager:getInstance():getRate()
    				txtRate:setString("速率: x"..rate)
	            elseif name == "BtnPlay" then
	            	local isPause = GameplayerManager:getInstance():isPause()
	            	if isPause then
	            		GameplayerManager:getInstance():resume()
	            		--需要设置图片
	            		btnPlay:loadTextureNormal("client/src/privatemode/plaza/res/room/replayer/hist_rec_pause.png")
	            		btnPlay:loadTexturePressed("client/src/privatemode/plaza/res/room/replayer/hist_rec_pause.png")
	            		local action = cc.Sequence:create(cc.DelayTime:create(GameplayerManager:getInstance():getReplayDuration()),
	                          cc.CallFunc:create(replayerCallback))
	    				self:runAction(action)
	            	else
	            		GameplayerManager:getInstance():pause()
	            		--需要设置图片
	            		btnPlay:loadTextureNormal("client/src/privatemode/plaza/res/room/replayer/hist_rec_play.png")
	            		btnPlay:loadTexturePressed("client/src/privatemode/plaza/res/room/replayer/hist_rec_play.png")
	            		self:stopAllActions()
	            	end
	            elseif name == "BtnClose" then
	            	self:closeReplay()
	            end
	        end
		end

    	btnSlow:addTouchEventListener(btnCallback)
    	btnQuick:addTouchEventListener(btnCallback)
    	btnPlay:addTouchEventListener(btnCallback)
    	btnClose:addTouchEventListener(btnCallback)
    	
    	--重放的回调
    	function replayerCallback()
    		local subCmdID = nil
    		local dealData = function(cmdIn)

    			subCmdID = cmdIn.subCmdID
    			self._gameStartUserChairId = cmdIn.chairID
    			--如果是操作提示，则再读取下一个操作，以便显示手指
    			if cmdIn.subCmdID == cmd.SUB_S_OPERATE_NOTIFY then
    				local replayCmd = GameplayerManager:getInstance():getNextCmd(true, 1)
    				--如果操作通知的下一个消息是操作结果，则表明进行了“吃”，“碰”，“杠”操作
    				if replayCmd.subCmdID == cmd.SUB_S_OPERATE_RESULT then
    					cmdIn["t"].opMask = replayCmd["t"].cbOperateCode
    				--如果下一个是结算操作，则为“胡”操作
    				elseif replayCmd.subCmdID == cmd.SUB_S_GAME_CONCLUDE then
    					cmdIn["t"].opMask = 64
    				--否则就是“过”
    				else
    					cmdIn["t"].opMask = 0
    				end
    				
    				cmdIn["t"].operateUser = cmdIn.chairID
    				dump(cmdIn, "---------- cmdIn SUB_S_OPERATE_NOTIFY ----------")
    				dump(replayCmd, "---------- replayCmd SUB_S_OPERATE_NOTIFY ----------")
    			end
    			--数据处理
    			self:onEventGameMessage(cmdIn.subCmdID, cmdIn["t"])
    		end

    		local isPause = GameplayerManager:getInstance():isPause()
    		if not isPause then
    			local replayCmd = GameplayerManager:getInstance():getNextCmd()
    			dealData(replayCmd)
    			local progress = GameplayerManager:getInstance():getProgress()
    			txtProgress:setString("进度: ".. (progress * 100) .. "%")
    			--游戏结束就终止
	    		if subCmdID ~= cmd.SUB_S_GAME_CONCLUDE then
	    			local action = cc.Sequence:create(cc.DelayTime:create(GameplayerManager:getInstance():getReplayDuration()),
	                          cc.CallFunc:create(replayerCallback))
	    			self:runAction(action)
	    		else
	    			self._replayControlNode:setVisible(false)
	    		end
	    	else
	    		local action = cc.Sequence:create(cc.DelayTime:create(GameplayerManager:getInstance():getReplayDuration()),
                          cc.CallFunc:create(replayerCallback))
    			self:runAction(action)
    		end
    	end
    	replayerCallback()
    end
end

function GameLayer:CreateView()
    return GameViewLayer:create(self):addTo(self)
end

function GameLayer:OnInitGameEngine()
    GameLayer.super.OnInitGameEngine(self)
    --保存当前玩家的座位id，防止最后一圈被重置
    self.wMeChairID = 0
    self.isGameEnd = false
    -- self.userWithChairId = {}

	self.lCellScore = 0
	self.cbTimeOutCard = 0
	self.cbTimeOperateCard = 0
	self.cbTimeStartGame = 0
	self.wCurrentUser = yl.INVALID_CHAIR
	self.wBankerUser = yl.INVALID_CHAIR
	self.cbPlayStatus = {0, 0, 0, 0}
	self.cbGender = {0, 0, 0, 0}
	self.bTrustee = false
	self.nGameSceneLimit = 0
	self.cbAppearCardData = {} 		--已出现的牌
	self.bMoPaiStatus = false
	self.cbListenPromptOutCard = {}
	self.cbListenCardList = {}
	self.cbActionMask = nil
	self.cbActionCard = nil
	self.bSendCardFinsh = false
	self.cbPlayerCount = 4
	--当前圈数
	self.cbQuanCount = 0
	self.lDetailScore = {}
	self.m_userRecord = {}

	self.cbMaCount = 0
	--房卡需要
	self.wRoomHostViewId = 0
end

function GameLayer:OnResetGameEngine()
    GameLayer.super.OnResetGameEngine(self)
    self._gameView:onResetData()
	self.nGameSceneLimit = 0
	self.bTrustee = false
	self.cbAppearCardData = {} 		--已出现的牌
	self.bMoPaiStatus = false
	self.cbActionMask = nil
	self.cbActionCard = nil
end

--获取gamekind
function GameLayer:getGameKind()
    return cmd.KIND_ID
end

-- 房卡信息层zorder
function GameLayer:priGameLayerZorder()
    return 13
end

--关闭重放
function GameLayer:closeReplay()
 	--清除重放数据
 	self:stopAllActions()
    GameplayerManager:getInstance():clear()
 	self:onExitRoom()
 	--如果从战绩界面来，那就回到战绩界面，否则回到大厅
 	if GameplayerManager:getInstance():isFromBattleRecord() then
 		local param = {}
		table.insert(param, true)
	 	self._scene:onChangeShowMode(PriRoom.LAYTAG.LAYER_MYROOMRECORD, param)
	else
	 	self._scene:onChangeShowMode(yl.SCENE_GAMELIST)
 	end
end

function GameLayer:onExitRoom()
	if not self._isReplay then
		self._gameFrame:onCloseSocket()
	end
    self:stopAllActions()
    self:KillGameClock()
    self:dismissPopWait()
    if not self._isReplay then
    	self._scene:onChangeShowMode(yl.SCENE_GAMELIST)
    end
    -- self._scene:onKeyBack()
end

-- 椅子号转视图位置,注意椅子号从0~nChairCount-1,返回的视图位置从1~nChairCount
function GameLayer:SwitchViewChairID(chair)
    local viewid = yl.INVALID_CHAIR
    local playerCount = nil
    local selfChair = nil
    if self._isReplay then
    	playerCount = GameplayerManager:getInstance():getPlayerCount()
    	selfChair = GameplayerManager:getInstance():getSelfChairID()
    end
    
    --当前桌子人数
    local nChairCount = playerCount or self._gameFrame:GetChairCount()
    --获取自己的椅子号
    local nChairID = selfChair or self:GetMeChairID()

    if not chair then
    	return nil
    end

	if chair ~= yl.INVALID_CHAIR and chair < cmd.GAME_PLAYER then
		--if GlobalUserItem.bPrivateRoom then 		--约战房
		    if nChairCount == 2 then
		    	if chair == 2 then -- 无用ID
		    		viewid = 2
		    	elseif chair == 3 then --无用ID
		    		viewid = 4
		    	elseif chair == nChairID then
		    		viewid = 3
		    	else
		    		viewid = 1
		    	end
			elseif nChairCount == 3 then
				if chair == 3 then -- 无用ID
					viewid = 1
				else
					
					local temp = math.mod(chair + (3 - 2 - nChairID), 3)
					viewid = temp + 2
				end 
				-- viewid = 3 - (self._gameFrame._wChairID - chair)
			elseif nChairCount == 4 then
		        viewid = math.mod(chair + math.floor(nChairCount * 3/2) - nChairID, nChairCount) + 1
		    end
    end
    return viewid
end

 function GameLayer:getRoomHostViewId()
 	return self.wRoomHostViewId
 end

function GameLayer:updateRoomHost()
	assert(GlobalUserItem.bPrivateRoom)
	if self.wRoomHostViewId and self.wRoomHostViewId >= 1 and self.wRoomHostViewId <= 4 then
		self._gameView:setRoomHost(self.wRoomHostViewId)
	end
end

function GameLayer:getUserInfoByChairID(chairId)
	local viewId = self:SwitchViewChairID(chairId)
	return self._gameView.m_sparrowUserItem[viewId]
end

function GameLayer:getMaCount()
	return self.cbMaCount
end

function GameLayer:onGetSitUserNum()
	local num = 0
	for i = 1, cmd.GAME_PLAYER do
		if nil ~= self._gameView.m_sparrowUserItem[i] then
			num = num + 1
		end
	end

    return num
end

-- function GameLayer:onEnterTransitionFinish()
--     self._scene:createVoiceBtn(cc.p(1250, 300))
--     GameLayer.super.onEnterTransitionFinish(self)
-- end

-- 计时器响应
function GameLayer:OnEventGameClockInfo(chair,time,clockId)
    local meChairId = self:GetMeChairID()
    if not self._isReplay then
    	if clockId == cmd.IDI_START_GAME then
	    	--托管
	    	if self.bTrustee and self._gameView.btStart:isVisible() then
	   --  		self._gameView:onButtonClickedEvent(GameViewLayer.BT_START)
	   --  		--托管在上个函数被复原了，在下面重开
				-- self.bTrustee = true
				-- self._gameView.nodePlayer[cmd.MY_VIEWID]:getChildByTag(GameViewLayer.SP_TRUSTEE):setVisible(true)
				-- self._gameView.spTrusteeCover:setVisible(true)
	    	end
	    	-- if chair == meChairI then
	    		--如果游戏结束，那么把这个时间更新到结算界面
	    		if self.isGameEnd then
					self._gameView._resultLayer:onClockUpdate(time)
				end
	    		--超时
				if time <= 0 then
					self._gameFrame:setEnterAntiCheatRoom(false)--退出防作弊，如果有的话
					--如果游戏结束，并且时间到了，那么强制继续游戏，如果没有结束，那么就退出房间
					if self.isGameEnd then
						self._gameView._resultLayer:continueBtnCallback()
					else
						-- self:onExitTable()
					end
				elseif time <= 5 then
		    		self:PlaySound(cmd.RES_PATH.."sound/GAME_WARN.mp3")
				end
	    	-- end
	    elseif clockId == cmd.IDI_OUT_CARD then
	    	if chair == meChairId then
	    		--托管
	    		if self.bTrustee then
					--self._gameView._cardLayer:outCardAuto()
	    		end
	    		--超时
	    		if time <= 0 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				self._gameView._cardLayer:outCardAuto()
	    			end
					
	    		elseif time <= 5 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				self:PlaySound(cmd.RES_PATH.."sound/GAME_WARN.mp3")
	    			end
	    		end
	    	end
	    elseif clockId == cmd.IDI_OPERATE_CARD then
	    	if chair == meChairId then
	    		--托管
	    		if self.bTrustee then
	    -- 			if self._gameView._cardLayer:isUserMustWin() then
					-- 	self._gameView:onButtonClickedEvent(GameViewLayer.BT_WIN)
	    -- 			end
					-- self._gameView:onButtonClickedEvent(GameViewLayer.BT_PASS)
					-- self._gameView._cardLayer:outCardAuto()
	    		end
	    		--超时
	    		if time <= 0 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				if self._gameView._cardLayer:isUserMustWin() then
							self._gameView:onButtonClickedEvent(GameViewLayer.BT_WIN)
		    			end
						self._gameView:onButtonClickedEvent(GameViewLayer.BT_PASS)
						self._gameView._cardLayer:outCardAuto()
	    			end
	    		elseif time <= 5 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				self:PlaySound(cmd.RES_PATH.."sound/GAME_WARN.mp3")
	    			end
	    		end
	    	end
	    end
    end
end

--用户聊天
function GameLayer:onUserChat(chat, wChairId)
    self._gameView:userChat(self:SwitchViewChairID(wChairId), chat.szChatString)
end

--用户表情
function GameLayer:onUserExpression(expression, wChairId, wTotChairID)
	local viewToChair = nil
	if wTotChairID then
		viewToChair = self:SwitchViewChairID(wTotChairID)
	end
    self._gameView:userExpression(self:SwitchViewChairID(wChairId), expression.wItemIndex, viewToChair)
end

function GameLayer:IsValidViewID(viewId)
	local nChairCount = self._gameFrame:GetChairCount()
    if nChairCount == 3 then
        return (viewId >= 2) and (viewId <= 4)
    elseif nChairCount == 4 then
        return (viewId >= 1) and (viewId <= 4)    
    end
    return false
end

function GameLayer:onDyExpression(fromChairID, toChairID, index)
    local from = self:SwitchViewChairID(fromChairID)
    local to = self:SwitchViewChairID(toChairID)
    if self:IsValidViewID(from) and self:IsValidViewID(to) then
        self._gameView:onUserDyExpression(from, to, index)
    end
end

-- 语音播放开始
function GameLayer:onUserVoiceStart( useritem, filepath )
    self._gameView:onUserVoiceStart(self:SwitchViewChairID(useritem.wChairID))
end

-- 语音播放结束
function GameLayer:onUserVoiceEnded( useritem, filepath )
    self._gameView:onUserVoiceEnded(self:SwitchViewChairID(useritem.wChairID))
end

--用户状态发生变化
-- function GameLayer:onEventUserStatus(useritem,newstatus,oldstatus)
	
-- end

-- 场景消息
function GameLayer:onEventGameScene(cbGameStatus, dataBuffer)
	self.m_cbGameStatus = cbGameStatus
	self.nGameSceneLimit = self.nGameSceneLimit + 1
	if self.nGameSceneLimit > 1 then
		--限制只进入场景消息一次
		return true
	end
    
	local wTableId = self:GetMeTableID()
	local wMyChairId = self:GetMeChairID()
	self._gameView:setRoomInfo(wTableId, wMyChairId)
	--初始化用户信息
	local pcount = nil
	if self._isReplay then
		pcount = GameplayerManager:getInstance():getPlayerCount()
	end

	if not self._isReplay then
		local nChairCount = pcount or self._gameFrame:GetChairCount()
		for i = 1, nChairCount do --cmd.GAME_PLAYER
			local wViewChairId = self:SwitchViewChairID(i - 1)
			local userItem = self._gameFrame:getTableUserItem(wTableId, i - 1)
			self._gameView:OnUpdateUser(wViewChairId, userItem)
			if userItem then
				self.cbGender[wViewChairId] = userItem.cbGender
				if PriRoom and GlobalUserItem.bPrivateRoom then
					if userItem.dwUserID == PriRoom:getInstance().m_tabPriData.dwTableOwnerUserID then
						self.wRoomHostViewId = wViewChairId
						if self._gameView._priView.onRefreshInfo then
							--此处只要为了刷新房主
							self._gameView._priView:onRefreshInfo()
						end
					end
				end
			end
		end
	end

	if cbGameStatus == cmd.GAME_SCENE_FREE then
		self._gameView:onSceneFree()
		if not self.isGameEnd then
			self._scene.m_gameAlreadyStart = true
		end

		local cmd_data = ExternalFun.read_netdata(cmd.CMD_S_StatusFree, dataBuffer)

		self.lCellScore = cmd_data.lCellScore
		self.cbTimeOutCard = cmd_data.cbTimeOutCard
		self.cbTimeOperateCard = cmd_data.cbTimeOperateCard
		self.cbTimeStartGame = cmd_data.cbTimeStartGame
		self.cbPlayerCount = cmd_data.cbPlayerCount or 4
		local bGameRule = cmd_data.bGameRule[1]

		PriRoom:getInstance().m_playRuleList["TIANDI"] = bGameRule[1]
		PriRoom:getInstance().m_playRuleList["HUIPAI"] = bGameRule[2]
        PriRoom:getInstance().m_playRuleList["QIONGHU"] = bGameRule[3]
        PriRoom:getInstance().m_playRuleList["FENGDING"] = bGameRule[4]
        PriRoom:getInstance().m_playRuleList["SANQING"] = bGameRule[5]
        
        PriRoom:getInstance().m_playRuleList["DUIHU"] = bGameRule[6]
        PriRoom:getInstance().m_playRuleList["QINGYISE"] = bGameRule[7]
        PriRoom:getInstance().m_playRuleList["FENGDING200"] = bGameRule[8]

        local ruleTxtStr = ""
        if bGameRule[1] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "天地胡"
        	else
        		ruleTxtStr = ruleTxtStr .. "、天地胡"
        	end
        end

        if bGameRule[2] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "死飘"
        	else
        		ruleTxtStr = ruleTxtStr .. "、死飘"
        	end
        end

        if bGameRule[3] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "穷胡"
        	else
        		ruleTxtStr = ruleTxtStr .. "、穷胡"
        	end
        end

        if bGameRule[4] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "封顶150"
        	else
        		ruleTxtStr = ruleTxtStr .. "、封顶150"
        	end
        end

        if bGameRule[5] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "三清"
        	else
        		ruleTxtStr = ruleTxtStr .. "、三清"
        	end
        end

        if bGameRule[6] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "七对"
        	else
        		ruleTxtStr = ruleTxtStr .. "、七对"
        	end
        end

        if bGameRule[7] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "清一色"
        	else
        		ruleTxtStr = ruleTxtStr .. "、清一色"
        	end
        end

        if bGameRule[8] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "封顶200"
        	else
        		ruleTxtStr = ruleTxtStr .. "、封顶200"
        	end
        end

        if ruleTxtStr == "" then
        	ruleTxtStr = ruleTxtStr .. "无"
        end

        local ruleTxt = self._gameView:getChildByName("RuleTxt")
        ruleTxt:setVisible(false)
        ruleTxt:setString("玩法:" .. ruleTxtStr)
        local gameViweRuleTxt = string.gsub(ruleTxtStr, "、", "\n")
        self._gameView.ruleTxt:setString(gameViweRuleTxt)

		self._gameFrame:SetChairCount(self.cbPlayerCount)
		
		self._gameView.btStart:setVisible(true)

		self:SetGameClock(wMyChairId, cmd.IDI_START_GAME, self.cbTimeStartGame) --第一次进入时调用
		self:OnResetGameEngine()
		-- end

		self._gameView._cardLayer.nRemainCardNum = 112
		--设置剩余手牌
	    self._gameView:setRemainCardNum(self._gameView._cardLayer.nRemainCardNum)

		--恢复圈数
		-- 刷新房卡
    	if PriRoom and GlobalUserItem.bPrivateRoom then
	        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
	            self._gameView._priView:setQuanData(cmd_data.cbGameCount)
	        end
	    end
	    --进入房间自动准备
	    self._gameView:onButtonClickedEvent(self._gameView.BT_START,nil)
	    self:sendLocation()
	    --空闲场景加载完成
	    self._gameView:onSceneFreeComplete()
	elseif cbGameStatus == cmd.GAME_SCENE_PLAY then
		self._gameView:onScenePlay()
		self._scene.m_gameAlreadyStart = true
		if not self.isGameEnd then
			self.isGameEnd = false
			--如果游戏开始，但结算界面仍然存在，那么将其删除
			self._gameView._resultLayer:hideLayer(false)
			self._gameView.btStart:setVisible(false)
		end

		self._scene.m_gameInResultLayer = false
		local cmd_data = ExternalFun.read_netdata(cmd.CMD_S_StatusPlay, dataBuffer)
 
		--是否为海底捞
		self.m_isHaiDiLao = cmd_data.bHiDiLao

		self.lCellScore = cmd_data.lCellScore
		self.cbTimeOutCard = cmd_data.cbTimeOutCard
		self.cbTimeOperateCard = cmd_data.cbTimeOperateCard
		self.cbTimeStartGame = cmd_data.cbTimeStartGame
		self.wCurrentUser = cmd_data.wCurrentUser
		self.wBankerUser = cmd_data.wBankerUser
		self.cbPlayerCount = cmd_data.cbPlayerCount or 4
		if not self._isReplay then
			self._gameFrame:SetChairCount(self.cbPlayerCount)
		end
		
		self.cbQuanCount = cmd_data.cbMaCount

		local bGameRule = cmd_data.bGameRule[1]
		local ruleTxtStr = ""
        if bGameRule[1] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "天地胡"
        	else
        		ruleTxtStr = ruleTxtStr .. "、天地胡"
        	end
        end

        if bGameRule[2] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "死飘"
        	else
        		ruleTxtStr = ruleTxtStr .. "、死飘"
        	end
        end

        if bGameRule[3] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "穷胡"
        	else
        		ruleTxtStr = ruleTxtStr .. "、穷胡"
        	end
        end

        if bGameRule[4] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "封顶150"
        	else
        		ruleTxtStr = ruleTxtStr .. "、封顶150"
        	end
        end

        if bGameRule[5] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "三清"
        	else
        		ruleTxtStr = ruleTxtStr .. "、三清"
        	end
        end

        if bGameRule[6] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "七对"
        	else
        		ruleTxtStr = ruleTxtStr .. "、七对"
        	end
        end

        if bGameRule[7] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "清一色"
        	else
        		ruleTxtStr = ruleTxtStr .. "、清一色"
        	end
        end

        if bGameRule[8] then
        	if ruleTxtStr == "" then
        		ruleTxtStr = ruleTxtStr .. "封顶200"
        	else
        		ruleTxtStr = ruleTxtStr .. "、封顶200"
        	end
        end

        if ruleTxtStr == "" then
        	ruleTxtStr = ruleTxtStr .. "无"
        end

        local ruleTxt = self._gameView:getChildByName("RuleTxt")
        ruleTxt:setString("玩法:" .. ruleTxtStr)
        ruleTxt:setVisible(false)

        local gameViweRuleTxt = string.gsub(ruleTxtStr, "、", "\n")
        self._gameView.ruleTxt:setString(gameViweRuleTxt)

		if cmd_data.cbMagicIndex == 34 then
			GameLogic.MAGIC_DATA = -1
		else
			GameLogic.MAGIC_DATA = GameLogic.SwitchToCardData(cmd_data.cbMagicIndex)
		end 
	    
	    GameLogic.MAGIC_INDEX = cmd_data.cbMagicIndex
		self._gameView:setLaiZi(cmd_data.cbMagicIndex)
		--庄家
		self._gameView:setBanker(self:SwitchViewChairID(self.wBankerUser))
		--设置手牌
		local viewCardCount = {}
		for i = 1, cmd.GAME_PLAYER do
			local viewId = self:SwitchViewChairID(i - 1)
			viewCardCount[viewId] = cmd_data.cbCardCount[1][i]
			if viewCardCount[viewId] > 0 then
				self.cbPlayStatus[viewId] = 1
			end
		end
		local cbHandCardData = {}
		for i = 1, cmd.MAX_COUNT do
			local data = cmd_data.cbCardData[1][i]
			if data > 0 then 				--去掉末尾的0
				cbHandCardData[i] = data
			else
				break
			end
		end
		GameLogic.SortCardList(cbHandCardData) 		--排序
		local cbSendCard = cmd_data.cbSendCardData
		if cbSendCard > 0 and self.wCurrentUser == wMyChairId then
			for i = 1, #cbHandCardData do
				if cbHandCardData[i] == cbSendCard then
					table.remove(cbHandCardData, i)				--把刚抓的牌放在最后
					break
				end
			end
			table.insert(cbHandCardData, cbSendCard)
		end
		for i = 1, cmd.GAME_PLAYER do
			self._gameView._cardLayer:setHandCard(i, viewCardCount[i], cbHandCardData)
		end
		self.bSendCardFinsh = true
		--记录已出现牌
		self:insertAppearCard(cbHandCardData)
		--组合牌
		for i = 1, cmd.GAME_PLAYER do
			local wViewChairId = self:SwitchViewChairID(i - 1)
			for j = 1, cmd_data.cbWeaveItemCount[1][i] do
				local cbOperateData = {}
				for v = 1, 4 do
					local data = cmd_data.WeaveItemArray[i][j].cbCardData[1][v]
					if data > 0 then
						table.insert(cbOperateData, data)
					end
				end
				local nShowStatus = GameLogic.SHOW_NULL
				local cbParam = cmd_data.WeaveItemArray[i][j].cbParam
				local cbCenterCard = cmd_data.WeaveItemArray[i][j].cbCenterCard

				if cbParam == GameLogic.WIK_GANERAL then
					if cbOperateData[1] == cbOperateData[2] then 	--碰
						nShowStatus = GameLogic.SHOW_PENG
					else 											--吃
						nShowStatus = GameLogic.SHOW_CHI
						local tmp = {}
						for i = 1, 3 do
							if cbOperateData[i] ~=  cbCenterCard then
								table.insert(tmp, cbOperateData[i])
							end
						end

						table.insert(tmp, 2, cbCenterCard)
						
						-- tmp = {cbOperateData[1], cbOperateData[2], cbOperateData[3]}
						cbOperateData = tmp
					end
				elseif cbParam == GameLogic.WIK_MING_GANG then
					nShowStatus = GameLogic.SHOW_FANG_GANG
				elseif cbParam == GameLogic.WIK_FANG_GANG then
					nShowStatus = GameLogic.SHOW_FANG_GANG
				elseif cbParam == GameLogic.WIK_AN_GANG then
					nShowStatus = GameLogic.SHOW_AN_GANG
				end

				self._gameView._cardLayer:bumpOrBridgeCard(wViewChairId, cbOperateData, nShowStatus)
				--记录已出现牌
				self:insertAppearCard(cbOperateData)
			end
		end
		--设置牌堆
		local wViewHeapHead = self:SwitchViewChairID(cmd_data.wHeapHead)
		local wViewHeapTail = self:SwitchViewChairID(cmd_data.wHeapTail)
		for i = 1, cmd.GAME_PLAYER do
			local viewId = self:SwitchViewChairID(i - 1)
			for j = 1, cmd_data.cbDiscardCount[1][i] do
				--已出的牌
				self._gameView._cardLayer:discard(viewId, cmd_data.cbDiscardCard[i][j])
				--记录已出现牌
				local cbAppearCard = {cmd_data.cbDiscardCard[i][j]}
				self:insertAppearCard(cbAppearCard)
			end

			--牌堆
			self._gameView._cardLayer:setTableCardByHeapInfo(viewId, cmd_data.cbHeapCardInfo[i], wViewHeapHead, wViewHeapTail)
			--托管
			self._gameView:setUserTrustee(viewId, cmd_data.bTrustee[1][i])
			if viewId == cmd.MY_VIEWID then
				self.bTrustee = cmd_data.bTrustee[1][i]
			end
		end

		--刚出的牌
		if cmd_data.cbOutCardData and cmd_data.cbOutCardData > 0 then
			local wOutUserViewId = self:SwitchViewChairID(cmd_data.wOutCardUser)
			self._gameView:gameOutCard(wOutUserViewId, cmd_data.cbOutCardData, true)
		end
		--计时器
		self:SetGameClock(cmd_data.wCurrentUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard) --断线重连时调用


		--提示听牌数据
		self.cbListenPromptOutCard = {}
		self.cbListenCardList = {}
		for i = 1, cmd_data.cbOutCardCount do
			self.cbListenPromptOutCard[i] = cmd_data.cbOutCardDataEx[1][i]
			self.cbListenCardList[i] = {}
			for j = 1, cmd_data.cbHuCardCount[1][i] do
				self.cbListenCardList[i][j] = cmd_data.cbHuCardData[i][j]
			end
		end

		local cbPromptHuCard = self:getListenPromptHuCard(cmd_data.cbOutCardData)
		self._gameView:setListeningCard(cbPromptHuCard)

		--65535 表明无人出牌，即自己摸牌
		local isOtherOp = false
		if cmd_data.wOutCardUser == 65535 then
			isOtherOp = false
		elseif self:GetMeChairID() ~= cmd_data.wOutCardUser then
			isOtherOp = true
		end
		--提示操作
		self._gameView:recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbActionCard, isOtherOp)
		if self.wCurrentUser == wMyChairId then
			self._gameView._cardLayer:promptListenOutCard(self.cbListenPromptOutCard)
		end

		self._gameView._cardLayer.nRemainCardNum = cmd_data.cbLeftCardCount
		--设置剩余手牌
	    self._gameView:setRemainCardNum(self._gameView._cardLayer.nRemainCardNum)

		--恢复圈数
		-- 刷新房卡
    	if PriRoom and GlobalUserItem.bPrivateRoom then
	        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
	        	PriRoom:getInstance().m_tabPriData.dwPlayCount = cmd_data.cbMaCount
	            -- self._gameView._priView:onRefreshInfo()
	            self._gameView._priView:setQuanData(cmd_data.cbMaCount)

	        end
	    end
	    self._gameView:onScenePlayComplete()
	    self:sendLocation()
	else
		return false
	end

    -- 刷新房卡
    if PriRoom and GlobalUserItem.bPrivateRoom then
        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
            self._gameView._priView:onRefreshInfo()
        end
    end

	self:dismissPopWait()

	return true
end

-- 游戏消息
function GameLayer:onEventGameMessage(sub, dataBuffer)
    -- body
	if sub == cmd.SUB_S_GAME_START then 					--游戏开始
		return self:onSubGameStart(dataBuffer)
	elseif sub == cmd.SUB_S_OUT_CARD then 					--用户出牌
		return self:onSubOutCard(dataBuffer)
	elseif sub == cmd.SUB_S_SEND_CARD then 					--发送扑克
		return self:onSubSendCard(dataBuffer)
	elseif sub == cmd.SUB_S_OPERATE_NOTIFY then 			--操作提示
		return self:onSubOperateNotify(dataBuffer)
	elseif sub == cmd.SUB_S_HU_CARD then 					--听牌提示
		return self:onSubListenNotify(dataBuffer)
	elseif sub == cmd.SUB_S_OPERATE_RESULT then 			--操作命令
		return self:onSubOperateResult(dataBuffer)
	elseif sub == cmd.SUB_S_LISTEN_CARD then 				--用户听牌
		return self:onSubListenCard(dataBuffer)
	elseif sub == cmd.SUB_S_TRUSTEE then 					--用户托管
		return self:onSubTrustee(dataBuffer)
	elseif sub == cmd.SUB_S_GAME_CONCLUDE then 				--游戏结束
		return self:onSubGameConclude(dataBuffer)
	elseif sub == cmd.SUB_S_RECORD then 					--游戏记录
		return self:onSubGameRecord(dataBuffer)
	elseif sub == cmd.SUB_S_SET_BASESCORE then 				--设置基数
		self.lCellScore = dataBuffer:readint()
		return true
	else
		assert(false, "default")
	end

	return false
end

function GameLayer:setLaizi(args)
	return args
    -- if args == 9 then
    --   	return 1
    -- elseif args == 18 then
    --  	return 10
    -- elseif args == 27 then
    --  	return 19
    -- elseif args == 32 then
    --  	return 32
    -- else 
        
    -- end
end
function GameLayer:showLaizi(args)
	return args
    -- if args == 8 then
    --   	return 0
    -- elseif args == 17 then
    --  	return 19
    -- elseif args == 26 then
    --  	return 18
    -- elseif args == 33 then
    --  	return 27
    -- else 
        
    -- end
end
--游戏开始
function GameLayer:onSubGameStart(dataBuffer)
	if (self._isReplay and self._gameStartUserChairId == GameplayerManager:getInstance():getSelfChairID()) or not self._isReplay then
		if self._isReplay then
			if self._gameView._priView and self._gameView._priView.onRefreshInfo then
				self._gameView._priView:onRefreshInfo()
			end
		end
		self.m_isHaiDiLao = false
		self._scene.m_gameAlreadyStart = true
		self._scene.m_gameInResultLayer = false
		self.isGameEnd = false
		--如果游戏开始，但结算界面仍然存在，那么将其删除
		self._gameView._resultLayer:hideLayer(false)
		self._gameView.btStart:setVisible(false)

		self._gameView:playStartGameEffect()


	    self.m_cbGameStatus = cmd.GAME_SCENE_PLAY
	    local cmd_data = nil
	    if self._isReplay then
	    	cmd_data = dataBuffer
	    else
	    	cmd_data = ExternalFun.read_netdata(cmd.CMD_S_GameStart, dataBuffer)
	    end
		
		for i = 1, cmd.GAME_PLAYER do
			local viewId = self:SwitchViewChairID(i - 1)
			local head = cmd_data.cbHeapCardInfo[i][1]
			local tail = cmd_data.cbHeapCardInfo[i][2]
		end
		
		self.wBankerUser = cmd_data.wBankerUser
		local wViewBankerUser = self:SwitchViewChairID(self.wBankerUser)
		self._gameView:setBanker(wViewBankerUser)
		local cbCardCount = {0, 0, 0, 0}

		if not self._isReplay then
			-- self.userWithChairId = {}
			for i = 1, cmd.GAME_PLAYER do
				local userItem = self._gameFrame:getTableUserItem(self:GetMeTableID(), i - 1)

				local wViewChairId = self:SwitchViewChairID(i - 1)
				self._gameView:OnUpdateUser(wViewChairId, userItem)
				if userItem then
					self.cbPlayStatus[wViewChairId] = 1
					cbCardCount[wViewChairId] = 13
					if wViewChairId == wViewBankerUser then
						cbCardCount[wViewChairId] = cbCardCount[wViewChairId] + 1
					end
				end
			end
		else --如果是重放，不需要更新头像
			for i = 1, cmd.GAME_PLAYER do
				local wViewChairId = self:SwitchViewChairID(i - 1)
				self.cbPlayStatus[wViewChairId] = 1
				cbCardCount[wViewChairId] = 13
				if wViewChairId == wViewBankerUser then
					cbCardCount[wViewChairId] = cbCardCount[wViewChairId] + 1
				end
				--如果是三人，则viewid = 1 没有牌
				if GameplayerManager:getInstance():getPlayerCount() == 3 then
					cbCardCount[1] = 0
				end
			end
		end

		if self.wBankerUser ~= self:GetMeChairID() then
			cmd_data.cbCardData[1][cmd.MAX_COUNT] = nil
		end

		--本局没有癞子
		if cmd_data.cbMagicIndex == 34 then
			GameLogic.MAGIC_DATA = -1
		else
			GameLogic.MAGIC_DATA = GameLogic.SwitchToCardData(cmd_data.cbMagicIndex)
		end

		GameLogic.MAGIC_INDEX = cmd_data.cbMagicIndex

		--剩余牌的数量
		self._gameView._cardLayer.nRemainCardNum = cmd_data.wCardCount

		self._gameView:setLaiZi(cmd_data.cbMagicIndex)

		--筛子
		local cbSiceCount1 = math.mod(cmd_data.wSiceCount, 256)
		local cbSiceCount2 = math.floor(cmd_data.wSiceCount/256)
		--起始位置
		local wStartChairId = math.mod(self.wBankerUser + cbSiceCount1 + cbSiceCount2 - 1, cmd.GAME_PLAYER)
		local wStartViewId = self:SwitchViewChairID(wStartChairId)
		--起始位置数的起始牌
		local nStartCard = math.min(cbSiceCount1, cbSiceCount2)*2 + 1

		--开始发牌
		self._gameView:gameStart(wStartViewId, nStartCard, cmd_data.cbCardData[1], cbCardCount, cbSiceCount1, cbSiceCount2)

		--记录已出现的牌
		self:insertAppearCard(cmd_data.cbCardData[1])

		self.wCurrentUser = cmd_data.wBankerUser
		self.cbActionMask = cmd_data.cbUserAction
		self.bMoPaiStatus = true
		self.bSendCardFinsh = false
		self:PlaySound(cmd.RES_PATH.."sound/GAME_START.mp3")
		-- 刷新房卡
	    if PriRoom and GlobalUserItem.bPrivateRoom then
	        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
	        	PriRoom:getInstance().m_tabPriData.dwPlayCount = cmd_data.wGameCount
	            -- self._gameView._priView:onRefreshInfo()
	            self._gameView._priView:setQuanData(cmd_data.wGameCount)
	        end
	    end

	    --计时器
		self:SetGameClock(cmd_data.wBankerUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard) --开始游戏时调用，庄家先出牌
	end

	if self._isReplay and self._gameStartUserChairId ~= GameplayerManager:getInstance():getSelfChairID() then
		local cmd_data = dataBuffer

		local cbCardCount = 13
		if cmd_data.wBankerUser == self._gameStartUserChairId then
			cbCardCount = 14
		end
	else
		--友盟统计玩过多少局
		MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.PLAY_COUNT)
	end

	return true
end

--用户出牌
function GameLayer:onSubOutCard(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_OutCard, dataBuffer)
	end

	local wViewId = self:SwitchViewChairID(cmd_data.wOutCardUser)
	self._gameView:gameOutCard(wViewId, cmd_data.cbOutCardData)

	--记录已出现的牌
	if wViewId ~= cmd.MY_VIEWID then
		local cbAppearCard = {cmd_data.cbOutCardData}
		self:insertAppearCard(cbAppearCard)
	end

	self.bMoPaiStatus = false
	self:KillGameClock()
	self._gameView:HideGameBtn()
	self:PlaySound(cmd.RES_PATH.."sound/OUT_CARD.mp3")
	self:playCardDataSound(wViewId, cmd_data.cbOutCardData)
	--轮到下一个
	self.wCurrentUser = cmd_data.wOutCardUser
	local wTurnUser = nil
	if self.wCurrentUser >= self.cbPlayerCount - 1 then
		wTurnUser = 0
	else
		wTurnUser = self.wCurrentUser + 1
	end
	
	local wViewTurnUser = self:SwitchViewChairID(wTurnUser)
	--设置听牌
	self._gameView._cardLayer:promptListenOutCard(nil)
	if wViewId == cmd.MY_VIEWID then
		local cbPromptHuCard = self:getListenPromptHuCard(cmd_data.cbOutCardData)
		self._gameView:setListeningCard(cbPromptHuCard)
		--听牌数据置空
		self.cbListenPromptOutCard = {}
		self.cbListenCardList = {}
	end

	self._gameView:setListeningCard(nil)
	--设置时间
	self:SetGameClock(wTurnUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard)  --玩家出牌的时候调用
	return true
end

--发送扑克(抓牌)
function GameLayer:onSubSendCard(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_SendCard, dataBuffer)
	end

	--判断是否为海底捞
	if tonumber(cmd_data.wSendCardUser) == 65535 then
		self.m_isHaiDiLao = true
	else
		self.m_isHaiDiLao = false
	end

	self.wCurrentUser = cmd_data.wCurrentUser
	local wCurrentViewId = self:SwitchViewChairID(self.wCurrentUser)
	--如果是皮子杠，就不加入手牌
	if cmd_data.bPiZiGang then

	else
		self._gameView:gameSendCard(wCurrentViewId, cmd_data.cbCardData, cmd_data.bTail)
	end

	self:SetGameClock(self.wCurrentUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard) --给玩家发送牌的时候调用

	self._gameView:HideGameBtn()
	--如果当前用户是自己，那就提示
	if not cmd_data.bPiZiGang and self.wCurrentUser == self:GetMeChairID()  then
		self._gameView:recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbCardData, false)
		--自动胡牌
		if cmd_data.cbActionMask >= GameLogic.WIK_CHI_HU and self.bTrustee then
			self._gameView:onButtonClickedEvent(GameViewLayer.BT_WIN)
		end
	end

	--记录已出现的牌
	if not cmd_data.bPiZiGang and wCurrentViewId == cmd.MY_VIEWID then
		local cbAppearCard = {cmd_data.cbCardData}
		self:insertAppearCard(cbAppearCard)
	end

	self.bMoPaiStatus = true
	-- self:PlaySound(cmd.RES_PATH.."sound/PACK_CARD.mp3")
	if not cmd_data.bPiZiGang and cmd_data.bTail then
		self:playCardOperateSound(wOperateViewId, true, nil)
	end
	return true
end

--操作提示
function GameLayer:onSubOperateNotify(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_OperateNotify, dataBuffer)
	end

	--默认是自己的
	cmd_data.wChairID = cmd.MY_VIEWID

	local viewId = nil
	if cmd_data.operateUser then
		viewId = self:SwitchViewChairID(cmd_data.operateUser)
	end

	if self.bSendCardFinsh then 	--发牌完成
		self._gameView:recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbActionCard, true, viewId, cmd_data.opMask)
	else
		self.cbActionMask = cmd_data.cbActionMask
		self.cbActionCard = cmd_data.cbActionCard
	end
	return true
end

--听牌提示
function GameLayer:onSubListenNotify(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_Hu_Data, dataBuffer)
	end

	self.cbListenPromptOutCard = {}
	self.cbListenCardList = {}
	for i = 1, cmd_data.cbOutCardCount do
		self.cbListenPromptOutCard[i] = cmd_data.cbOutCardData[1][i]
		self.cbListenCardList[i] = {}
		for j = 1, cmd_data.cbHuCardCount[1][i] do
			self.cbListenCardList[i][j] = cmd_data.cbHuCardData[i][j]
		end
	end

	return true
end

--操作结果
function GameLayer:onSubOperateResult(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_OperateResult, dataBuffer)
	end
	if cmd_data.cbOperateCode == GameLogic.WIK_NULL then
		assert(false, "没有操作也会进来？")
		return true
	end

	local wOperateViewId = self:SwitchViewChairID(cmd_data.wOperateUser)
	if cmd_data.cbOperateCode < GameLogic.WIK_LISTEN then 		--并非听牌
		local nShowStatus = GameLogic.SHOW_NULL
		local data1 = cmd_data.cbOperateCard[1][1]
		local data2 = cmd_data.cbOperateCard[1][2]
		local data3 = cmd_data.cbOperateCard[1][3]

		local cbOperateData = {}
		local cbRemoveData = {}
		if cmd_data.cbOperateCode == GameLogic.WIK_GANG then
			--皮子牌
			local piziData = GameLogic.getPiZiCardData()
			--如果杠牌是皮子牌，那么手上只移除两张
			if data1 == piziData then
				cbOperateData = {data1, data1, data1}
				cbRemoveData = {data1, data1}
			else
				cbOperateData = {data1, data1, data1, data1}
				cbRemoveData = {data1, data1, data1}
			end
			
			--获取操作的人的手牌数
			local cbCardCount = self._gameView._cardLayer.cbCardCount[wOperateViewId]

			--检查杠的类型
			if math.mod(cbCardCount - 2, 3) == 0 then
				if self._gameView._cardLayer:checkBumpOrBridgeCard(wOperateViewId, data1) then
					nShowStatus = GameLogic.SHOW_MING_GANG
				else
					nShowStatus = GameLogic.SHOW_AN_GANG
				end
			else
				nShowStatus = GameLogic.SHOW_FANG_GANG
			end
		elseif cmd_data.cbOperateCode == GameLogic.WIK_PENG then
			cbOperateData = {data1, data1, data1}
			cbRemoveData = {data1, data1}
			nShowStatus = GameLogic.SHOW_PENG
		elseif cmd_data.cbOperateCode == GameLogic.WIK_RIGHT then
			cbOperateData = {data2, data1, data3} --cmd_data.cbOperateCard[1]
			cbRemoveData = {data2, data3}
			nShowStatus = GameLogic.SHOW_CHI
		elseif cmd_data.cbOperateCode == GameLogic.WIK_CENTER then
			cbOperateData = {data2, data1, data3} --cmd_data.cbOperateCard[1]
			cbRemoveData = {data2, data3}
			nShowStatus = GameLogic.SHOW_CHI
		elseif cmd_data.cbOperateCode == GameLogic.WIK_LEFT then
			cbOperateData = {data2, data1, data3} -- cmd_data.cbOperateCard[1]
			
			cbRemoveData = {data2, data3}
			nShowStatus = GameLogic.SHOW_CHI
		end

		local bAnGang = nShowStatus == GameLogic.SHOW_AN_GANG
		self._gameView._cardLayer:bumpOrBridgeCard(wOperateViewId, cbOperateData, nShowStatus)
		local bRemoveSuccess = false
		if nShowStatus == GameLogic.SHOW_AN_GANG then
			self._gameView._cardLayer:removeHandCard(wOperateViewId, cbOperateData, false)
		elseif nShowStatus == GameLogic.SHOW_MING_GANG then
			self._gameView._cardLayer:removeHandCard(wOperateViewId, {data1}, false)
		else
			self._gameView._cardLayer:removeHandCard(wOperateViewId, cbRemoveData, false)
		end
		self:PlaySound(cmd.RES_PATH.."sound/SEND_CARD.mp3")
		self:playCardOperateSound(wOperateViewId, false, cmd_data.cbOperateCode)

		--记录已出现的牌
		if wOperateViewId ~= cmd.MY_VIEWID then
			if nShowStatus == GameLogic.SHOW_AN_GANG then
				self:insertAppearCard(cbOperateData)
			elseif nShowStatus == GameLogic.SHOW_MING_GANG then
				self:insertAppearCard({data1})
			else
				self:insertAppearCard(cbRemoveData)
			end
		end
		--提示听牌
		if wOperateViewId == cmd.MY_VIEWID and cmd_data.cbOperateCode == GameLogic.WIK_PENG then
			self._gameView._cardLayer:promptListenOutCard(self.cbListenPromptOutCard)
		end
	end
	self._gameView:showOperateFlag(wOperateViewId, cmd_data.cbOperateCode)

	local cbTime = self.cbTimeOutCard - self.cbTimeOperateCard
	self:SetGameClock(cmd_data.wOperateUser, cmd.IDI_OUT_CARD, cbTime > 0 and cbTime or self.cbTimeOutCard) --有碰、杠等操作的时候使用

	--当有两个玩家选择的时候，一个碰一个吃，碰家为先，别人碰了，吃家的操作提示关闭
	self._gameView:HideGameBtn()
	self._gameView:removeChoicesPopup()

	return true
end

--用户听牌
function GameLayer:onSubListenCard(dataBuffer)

	return true
end

--用户托管
function GameLayer:onSubTrustee(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_Trustee, dataBuffer)
	end

	local wViewChairId = self:SwitchViewChairID(cmd_data.wChairID)
	self._gameView:setUserTrustee(wViewChairId, cmd_data.bTrustee)
	if cmd_data.wChairID == self:GetMeChairID() then
		self.bTrustee = cmd_data.bTrustee
	end

	if cmd_data.bTrustee then
		self:PlaySound(cmd.RES_PATH.."sound/GAME_TRUSTEE.mp3")
	else
		self:PlaySound(cmd.RES_PATH.."sound/UNTRUSTEE.mp3")
	end

	return true
end

--游戏结束
function GameLayer:onSubGameConclude(dataBuffer)
	self._gameView:HideGameBtn()
	self._gameView:removeChoicesPopup()
	
	self.isGameEnd = true
	--结束倒计时
	self:KillGameClock()
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_GameConclude, dataBuffer)
	end
	
	if cmd_data.wWinerUser ~= yl.INVALID_CHAIR and cmd_data.wWinerUser >= 0 and cmd_data.wWinerUser <= 3 then
		local viewId = self:SwitchViewChairID(cmd_data.wWinerUser)
		self._gameView:showHuCard(viewId, cmd_data.cbProvideCard)
	end
	
	local bMeWin = nil  	--nil：没人赢，false：有人赢但我没赢，true：我赢
	--剩余牌
	local cbTotalCardData = clone(GameLogic.TotalCardData)
	local cbRemainCard = GameLogic.RemoveCard(cbTotalCardData, self.cbAppearCardData)
	--提示胡牌标记
	for i = 1, cmd.GAME_PLAYER do
		local wViewChairId = self:SwitchViewChairID(i - 1)
		if cmd_data.cbChiHuKind[1][i] >= GameLogic.WIK_CHI_HU then
			bMeWin = false
			self:playCardOperateSound(wOperateViewId, false, GameLogic.WIK_CHI_HU, cmd_data.wProvideUser == cmd_data.wWinerUser)
			self._gameView:showOperateFlag(wViewChairId, GameLogic.WIK_CHI_HU, cmd_data.wProvideUser == cmd_data.wWinerUser)
			if wViewChairId == cmd.MY_VIEWID then
				bMeWin = true
			end
		end
	end
	--显示结算图层
	local resultList = {}
	local cbBpBgData = self._gameView._cardLayer:getBpBgCardData()
	for i = 1, cmd.GAME_PLAYER do
		local wViewChairId = self:SwitchViewChairID(i - 1)
		--总分
		local lScore = cmd_data.lGameScore[1][i]
		--杠的分
		local gScore = cmd_data.lGangScore[1][i]
		--胡牌的分
		local hScore = lScore - gScore

		local user = nil
		if self._isReplay then
			local userData = GameplayerManager:getInstance():getPlayerInfo()
			local aData = userData[i]
			if aData then
				--虚拟信息
				user = UserItem:create()
				user.szNickName = aData.szNickName
				user.dwUserID = aData.dwUserID
				user.cbUserStatus = aData.cbUserStatus
				user.wTableID = aData.wTableID
				user.wChairID = aData.wChairID
			end
		else
			user = self._gameFrame:getTableUserItem(self:GetMeTableID(), i - 1)
		end

		if user then
			local result = {}
			result.userItem = user
			
			result.lScore = lScore
			result.gScore = gScore
			result.hScore = hScore
			result.cbChHuKind = cmd_data.cbChiHuKind[1][i] --dwChiHuRight
			--是否血会庄
			result.wAllMagicUser = cmd_data.wAllMagicUser
			--是否跟庄
			result.bFollowBanker = cmd_data.bFollowBanker[1]
			--组合数目
			result.cbWeaveItemCount = cmd_data.cbWeaveItemCount[1][i]
			--组合扑克
			result.WeaveItemArray = cmd_data.WeaveItemArray[i]

			--没人胡，服务器传的65535
			if cmd_data.wWinerUser >= cmd.GAME_PLAYER then
				result.dwChiHuRight = 0
				result.cbCardData = {}
			else
				--TODO 不一定有人胡牌
				result.dwChiHuRight = cmd_data.dwChiHuRight[cmd_data.wWinerUser+1][1]
				result.cbCardData = {}
			end

			--手牌
			for j = 1, cmd_data.cbCardCount[1][i] do
				result.cbCardData[j] = cmd_data.cbHandCardData[i][j]
			end
			--碰杠牌
			result.cbBpBgCardData = cbBpBgData[wViewChairId]
			--奖码
			result.cbAwardCard = {}

			--插入
			table.insert(resultList, result)
			--剩余牌里删掉对手的牌
			if wViewChairId ~= cmd.MY_VIEWID then
				cbRemainCard = GameLogic.RemoveCard(cbRemainCard, result.cbCardData)
			end

			if result.cbChHuKind >= GameLogic.WIK_CHI_HU then
				local hasQiDuiRule = false
				for k,v in pairs(cmd_data.bGameRule[1]) do
					if v == true and k == 6 then
						hasQiDuiRule = true
					end
				end

				--是否是自摸
				local isZiMo = cmd_data.wProvideUser == cmd_data.wWinerUser
				
				local isGangKaiHua = false
				local isGangLiuLei = false
				local isSiQing = false
				local isTianHu = false
				local isDihu = false
				local isQiongHu = false
				local isQingyise = false
				local isHaidiLao = false
				local isQiDui = false
				local isLou = false
				local isPiao = false
				for k,v in pairs(cmd_data.bGameRuleEnd[1]) do
					if v == true and k == 1 then
						--"杠开花"
						isGangKaiHua = true
					end

					if v == true and k == 2 then
						isGangLiuLei = true
						--"杠流泪"
					end

					if v == true and k == 3 then
						--"三清"
					end

					if v == true and k == 4 then
						isSiQing = true
						--"四清"
					end 

					if v == true and k == 5 then
						--"天地胡"
						if isZiMo then
							isTianHu = true
						else
							isDihu = true
						end
					end 

					if v == true and k == 6 then
						--"封顶150"
					end 

					if v == true and k == 7 then
						--"穷胡翻倍"
						isQiongHu = true
					end 

					if v == true and k == 8 then
						isQingyise = true
						--"清一色"
					end

					if v == true and k == 9 then
						--"封顶200"
					end 

					if v == true and k == 10 then
						isHaidiLao = true
						--"海底捞"
					end 
				end

				local htpType = GameLogic.getHPType(result.dwChiHuRight)
				local typePath = ""
				if htpType == GameLogic.HTP_QIDUI_HU then
					if isZiMo then
						typePath = "zimo1.mp3"
					end
					if hasQiDuiRule then
						typePath = "qixiaodui1.mp3"
						isQiDui = true
					end
				elseif htpType == GameLogic.HTP_HZ_HU then
					typePath = "louhu1.mp3"
					isLou = true
				elseif htpType == GameLogic.HTP_PIAO_HU then
					typePath = "piaohu2.mp3"
					isPiao = true
				elseif htpType == GameLogic.HTP_DAN_DIAN then
					if isZiMo then
						typePath = "zimojia1.mp3"
					else
						typePath = "jiahu1.mp3"
					end
				elseif htpType == GameLogic.HTP_JIA_HU then
					if isZiMo then
						typePath = "zimojia1.mp3"
					else
						typePath = "jiahu1.mp3"
					end
				elseif htpType == GameLogic.HTP_PING_HU then
					if isZiMo then
						typePath = "zimo1.mp3"
					else
						typePath = "hu3.mp3"
					end
				elseif htpType == GameLogic.HTP_QING_YI_SE_HU then
					typePath = "qingyise1.mp3"
				end
				local sound_path = "" 

				if result.userItem.cbGender == 0 then
					sound_path = cmd.RES_PATH .. "sound/GIRL/" .. typePath
				else
					sound_path = cmd.RES_PATH .. "sound/BOY/" .. typePath
				end
				
				if GlobalUserItem.bSoundAble then
					AudioEngine.playEffect(cc.FileUtils:getInstance():fullPathForFilename(sound_path),false)
				end

				if isTianHu then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/tianhu/tianhu.plist", "tianhu%d.png", 1, 16, "tianhu")
				elseif isDihu then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/dihu/dihu.plist", "dihu%d.png", 1, 16, "dihu")
				elseif isSiQing then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/siqing/siqing.plist", "siqing%d.png", 1, 14, "siqing")
				elseif isQingyise then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/qingyise/qiyise.plist", "qiyise%d.png", 1, 15, "qiyise")
				elseif isPiao then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/piao/piao.plist", "piao%d.png", 1, 16, "piao")
				elseif isQiDui then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/qixiaodui/qixiaodui.plist", "qixiaodui%d.png", 1, 16, "qixiaodui")
				elseif isLou then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/piao/piao.plist", "piao%d.png", 1, 16, "piao")
				elseif isGangKaiHua then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/gangshangkaihua/gangkai.plist", "gang_%d.png", 1, 16, "gangkai")
				elseif isGangLiuLei then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/gangliulei/ganglei.plist", "ganglei%d.png", 1, 13, "ganglei")
				elseif isHaidiLao then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/haidilaoyue/haidilaoyue.plist", "haidilaoyue%d.png", 1, 12, "haidilaoyue")
				elseif isQiongHu then
					self._gameView:playHuTypeEffect(result.userItem.wChairID, cmd.RES_PATH .. "effect_atlas/qionghu/qionghu.plist", "qionghu%d.png", 1, 16, "qionghu")
				else
					local viewId = self:SwitchViewChairID(result.userItem.wChairID)
					if isZiMo then
						self._gameView:createArmature("zimo", self, cc.p(667, 403), GameViewLayer.SP_OPERATFLAG, 12)
					else
						self._gameView:createArmature("hu", self, cc.p(657, 393), GameViewLayer.SP_OPERATFLAG, 12)
					end
				end
			end
		end
	end

	local resultListTemp = Utils.deepCopy(resultList)
	local dtime = 2
	if cmd_data.wWinerUser == yl.INVALID_CHAIR then --流局
		self._gameView:playLiujuEffect()
		dtime = 2.5
	end

	--显示结算框
	self:runAction(cc.Sequence:create(cc.DelayTime:create(dtime), cc.CallFunc:create(function(ref)
		self._gameView:removeMoveEffectCard()
		--游戏开始倒计时
		self:SetGameClock(self:GetMeChairID(), cmd.IDI_START_GAME, self.cbTimeStartGame) --游戏倒计时开始时，指针指向自己
		self._gameView._resultLayer:showLayer(resultListTemp, cbAwardCardTotal, cbRemainCard, self.wBankerUser, cmd_data.cbProvideCard, cmd_data.wProvideUser, cmd_data.wWinerUser, self:GetMeChairID(), cmd_data.bGameRule[1], cmd_data.bGameRuleEnd[1]) -- cmd_data.wWinerUser
	end)))
	--播放音效
	if bMeWin then
		self:PlaySound(cmd.RES_PATH.."sound/ZIMO_WIN.mp3")
	else
		self:PlaySound(cmd.RES_PATH.."sound/ZIMO_LOSE.mp3")
	end

	self.cbPlayStatus = {0, 0, 0, 0}
    self.bTrustee = false
    self.bSendCardFinsh = false
	self._gameView:gameConclude()

	self:SetGameClock(self:GetMeChairID(), cmd.IDI_START_GAME, self.cbTimeStartGame) --游戏倒计时结算时，指针指向自己
	--end

	self._gameView:setLaiziCardVisible(false)
	--局面结束时，隐藏操作按钮
	self._gameView:HideGameBtn()
	return true
end

--游戏记录（房卡）
function GameLayer:onSubGameRecord(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_Record, dataBuffer)
	end

	self.m_userRecord = {}
	local nInningsCount = cmd_data.nCount
	for i = 1, self.cbPlayerCount do
		self.m_userRecord[i] = {}
		self.m_userRecord[i].cbHuCount = cmd_data.cbHuCount[1][i]
		self.m_userRecord[i].cbMingGang = cmd_data.cbMingGang[1][i]
		self.m_userRecord[i].cbAnGang = cmd_data.cbAnGang[1][i]
		self.m_userRecord[i].cbMaCount = cmd_data.cbMaCount[1][i]
		self.m_userRecord[i].lDetailScore = {}
		for j = 1, nInningsCount do
			self.m_userRecord[i].lDetailScore[j] = cmd_data.lDetailScore[i][j]
		end
	end
end

--*****************************    普通函数     *********************************--
--发牌完成
function GameLayer:sendCardFinish()	
	--提示操作
	if self.cbActionMask then
		self._gameView:recognizecbActionMask(self.cbActionMask, self.cbActionCard, false)
	end

	--提示听牌
	if self.wBankerUser == self:GetMeChairID() then
		self._gameView._cardLayer:promptListenOutCard(self.cbListenPromptOutCard)
	else
		--非庄家
		local cbHuCardData = self.cbListenCardList[1]
		if cbHuCardData and #cbHuCardData > 0 then
			self._gameView:setListeningCard(cbHuCardData)
		end
	end

	self.bSendCardFinsh = true
end

--解析筛子
function GameLayer:analyseSice(wSiceCount)
	local cbSiceCount1 = math.mod(wSiceCount, 256)
	local cbSiceCount2 = math.floor(wSiceCount/256)
	return cbSiceCount1, cbSiceCount2
end

--设置操作时间
function GameLayer:SetGameOperateClock()
	self:SetGameClock(self:GetMeChairID(), cmd.IDI_OPERATE_CARD, self.cbTimeOperateCard) --设置当前操作者是自己
end

--播放麻将数据音效（哪张）
function GameLayer:playCardDataSound(viewId, cbCardData)
	local strGender = ""
	if self.cbGender[viewId] == 1 then
		strGender = "BOY"
	else
		strGender = "GIRL"
	end
	local color = {"W_", "S_", "T_", "F_"}
	local nCardColor = math.floor(cbCardData/16) + 1
	local nValue = math.mod(cbCardData, 16)
	if cbCardData == GameLogic.MAGIC_DATA then
		nValue = 5
	end
	local strFile = cmd.RES_PATH.."sound/"..strGender.."/"..color[nCardColor]..nValue..".mp3"
	self:PlaySound(strFile)
end
--播放麻将操作音效
function GameLayer:playCardOperateSound(viewId, bTail, operateCode, isZiMo)
	assert(operateCode ~= GameLogic.WIK_NULL)

	local strGender = ""
	if self.cbGender[viewId] == 1 then
		strGender = "BOY"
	else
		strGender = "GIRL"
	end
	local strName = ""
	if bTail then
		-- strName = "REPLACE.mp3"
		return
	else
		if operateCode >= GameLogic.WIK_CHI_HU then
			return
			-- local rNum = math.random(3)
			-- strName = "hu" .. rNum .. ".mp3"
			-- if isZiMo then
			-- 	rNum = math.random(2)
			-- 	strName = "zimo" .. rNum .. ".mp3"
			-- end
		elseif operateCode == GameLogic.WIK_LISTEN then
			strName = "TING.mp3"
		elseif operateCode == GameLogic.WIK_GANG then
			strName = "GANG.mp3"
		elseif operateCode == GameLogic.WIK_PENG then
			strName = "PENG.mp3"
		elseif operateCode <= GameLogic.WIK_RIGHT then
			strName = "CHI.mp3"
		end

		--隐藏显示的Plate牌
		self._gameView:showCardPlate(nil)
		self._gameView:showOperateFlag(nil)

	end
	local strFile = cmd.RES_PATH.."sound/"..strGender.."/"..strName
	self:PlaySound(strFile)
end
--播放随机聊天音效
function GameLayer:playRandomSound(viewId)
	local strGender = ""
	if self.cbGender[viewId] == 1 then
		strGender = "BOY"
	else
		strGender = "GIRL"
	end
	local nRand = math.random(25) - 1
	if nRand <= 6 then
		local num = 6603000 + nRand
		local strName = num..".mp3"
		local strFile = cmd.RES_PATH.."sound/PhraseVoice/"..strGender.."/"..strName
		-- self:PlaySound(strFile)
	end
end

--插入到已出现牌中
function GameLayer:insertAppearCard(cbCardData)
	assert(type(cbCardData) == "table")
	for i = 1, #cbCardData do
		table.insert(self.cbAppearCardData, cbCardData[i])
		--self._gameView:reduceListenCardNum(cbCardData[i])
	end
	table.sort(self.cbAppearCardData)
	local str = ""
	for i = 1, #self.cbAppearCardData do
		str = str..string.format("%x,", self.cbAppearCardData[i])
	end
end

function GameLayer:getDetailScore()
	return self.m_userRecord
end

function GameLayer:getListenPromptOutCard()
	return self.cbListenPromptOutCard
end

function GameLayer:getListenPromptHuCard(cbOutCard)
	if not cbOutCard then
		return nil
	end

	for i = 1, #self.cbListenPromptOutCard do
		if self.cbListenPromptOutCard[i] == cbOutCard then
			assert(#self.cbListenCardList > 0 and self.cbListenCardList[i] and #self.cbListenCardList[i] > 0)
			return self.cbListenCardList[i]
		end
	end

	return nil
end

-- 刷新房卡数据
function GameLayer:updatePriRoom()
    if PriRoom and GlobalUserItem.bPrivateRoom then
        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
            self._gameView._priView:onRefreshInfo()
        end
    end
end

--*****************************    发送消息     *********************************--
--开始游戏
function GameLayer:sendGameStart()
	self:SendUserReady()
	self:OnResetGameEngine()
end
--出牌
function GameLayer:sendOutCard(card)
	-- body
	if card == GameLogic.MAGIC_DATA then
		return false
	end

	if true == self.m_isHaiDiLao then
		showToast(self._scene, "海底捞期间无法出牌，请等待其他玩家操作", 2)
		return
	end

	self._gameView:HideGameBtn()

	local cmd_data = ExternalFun.create_netdata(cmd.CMD_C_OutCard)
	cmd_data:pushbyte(card)
	return self:SendData(cmd.SUB_C_OUT_CARD, cmd_data)
end
--操作扑克
function GameLayer:sendOperateCard(cbOperateCode, cbOperateCard)
	assert(type(cbOperateCard) == "table")

	--听牌数据置空
	self.cbListenPromptOutCard = {}
	self.cbListenCardList = {}
	self._gameView:setListeningCard(nil)
	self._gameView._cardLayer:promptListenOutCard(nil)

	--发送操作
	--local cmd_data = ExternalFun.create_netdata(cmd.CMD_C_OperateCard)
    local cmd_data = CCmd_Data:create(4)
	cmd_data:pushbyte(cbOperateCode)
	for i = 1, 3 do
		cmd_data:pushbyte(cbOperateCard[i])
	end

	self:SendData(cmd.SUB_C_OPERATE_CARD, cmd_data)
end
--用户听牌
-- function GameLayer:sendUserListenCard(bListen)
-- 	local cmd_data = CCmd_Data:create(1)
-- 	cmd_data:pushbool(bListen)
-- 	self:SendData(cmd.SUB_C_LISTEN_CARD, cmd_data)
-- end
--用户托管
function GameLayer:sendUserTrustee(isTrustee)
	if not self.bSendCardFinsh then
		return
	end
	
	local cmd_data = CCmd_Data:create(1)
	--cmd_data:pushbool(not self.bTrustee)
	cmd_data:pushbool(isTrustee)
	self:SendData(cmd.SUB_C_TRUSTEE, cmd_data)
end
--用户补牌
-- function GameLayer:sendUserReplaceCard(card)
-- 	local cmd_data = ExternalFun.create_netdate(cmd.CMD_C_ReplaceCard)
-- 	cmd_data:pushbyte(card)
-- 	self:SendData(cmd.SUB_C_REPLACE_CARD, cmd_data)
-- end
--发送扑克
-- function GameLayer:sendControlCard(cbControlGameCount, cbCardCount, wBankerUser, cbCardData)
-- 	local cmd_data = ExternalFun.create_netdata(cmd.CMD_C_SendCard)
-- 	cmd_data:pushbyte(cbControlGameCount)
-- 	cmd_data:pushbyte(cbCardCount)
-- 	cmd_data:pushword(wBankerUser)
-- 	for i = 1, #cbCardData do
-- 		cmd_data:pushbyte(cbCardData[i])
-- 	end
-- 	self:SendData(cmd.SUB_C_SEND_CARD, cmd_data)
-- end

return GameLayer