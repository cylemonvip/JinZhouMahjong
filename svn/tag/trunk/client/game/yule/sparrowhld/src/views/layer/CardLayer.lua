local CardLayer = class("CardLayer", function(scene)
	local cardLayer = display.newLayer()
	return cardLayer
end)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC.."ExternalFun")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.GameLogic")
local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")

local posTableCard = {cc.p(393, 546), cc.p(292, 179), cc.p(940, 187), cc.p(1044, 593)}
local posHandCard = {cc.p(460, 660), cc.p(180, 260), cc.p(1270, 8), cc.p(1130, 650)}
local posHandDownCard = {cc.p(495, 665), cc.p(180, 670), cc.p(295, 70), cc.p(1155, 670)}
local posDiscard = {cc.p(787, 515), cc.p(434, 515), cc.p(547, 288), cc.p(896, 280)}
local posBpBgCard = {cc.p(1160, 680), cc.p(190, 735), cc.p(0, 65), cc.p(1130, 164)}
local anchorPointHandCard = {cc.p(0, 0), cc.p(0, 0), cc.p(1, 0), cc.p(0, 1)}
local multipleTableCard = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}}
local multipleDownCard = {{1, 0}, {0, -1}, {1, 0}, {0, -1}}
local multipleBpBgCard = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}}

local posPlates = {cc.p(667, 589), cc.p(270, 464), cc.p(667, 184), cc.p(1070, 455)}

local CARD_HEIGHT = 130
local CARD_WIDTH = 86

--local cbCardData = {1, 5, 7, 6, 34, 12, 32, 25, 18, 19, 27, 22, 33, 33}

CardLayer.TAG_BUMPORBRIDGE = 1
CardLayer.TAG_CARD_FONT = 1
CardLayer.TAG_LISTEN_FLAG = 2

CardLayer.ENUM_CARD_NORMAL = nil
CardLayer.ENUM_CARD_POPUP = 1
CardLayer.ENUM_CARD_MOVING = 2
CardLayer.ENUM_CARD_OUT = 3

CardLayer.Z_ORDER_TOP = 50

function CardLayer:onInitData()
	--body
	math.randomseed(os.time())
	self.cbCardData = {}
	--该Table用于储存其他三家的手牌，顺序为viewId -> 1 -> 2 - > 4
	self.otherCbCardData = {}
	self.cbCardCount = {cmd.MAX_COUNT, cmd.MAX_COUNT, cmd.MAX_COUNT, cmd.MAX_COUNT}
	self.nDiscardCount = {0, 0, 0, 0}
	self.nBpBgCount = {0, 0, 0, 0}
	self.bCardGray = {}
	self.cbCardStatus = {}
	self.nCurrentTouchCardTag = 0
	self.currentTag = 0
	self.cbListenList = {}
	self.bWin = false
	self.currentOutCard = 0
	self.nTableTailCardTag = 112
	self.nRemainCardNum = 112
	self.posTemp = cc.p(0, 0)
	self.nZOrderTemp = 0
	self.nMovingIndex = 0
	self.bSpreadCardEnabled = false
	self.bSendOutCardEnabled = false
	self.cbBpBgCardData = {{}, {}, {}, {}}
	self.bpBgCardArray = {}
end

function CardLayer:onResetData()
	
	--body
	for i = 1, cmd.GAME_PLAYER do
		self.cbCardCount[i] = cmd.MAX_COUNT
		self.nDiscardCount[i] = 0
		self.nBpBgCount[i] = 0
		self.nodeDiscard[i]:removeAllChildren()
		self.nodeBpBgCard[i]:removeAllChildren()

		for j = 1, cmd.MAX_COUNT do
			local card = self.nodeHandCard[i]:getChildByTag(j)
			
			
			card:setColor(cc.c3b(255, 255, 255))
			card:setVisible(false)

			if i == cmd.MY_VIEWID then
				local width = CARD_WIDTH
				local height = CARD_HEIGHT
				local fSpacing = width
				local widthTotal = fSpacing*cmd.MAX_COUNT
				local posX = widthTotal - width/2 - fSpacing*(j - 1)
				local posY = height/2
				
				card:setPosition(posX, posY)
				if j == 1 then
					card:setPosition(posX + 20, posY)						--每次抓的牌
				end
			else
				if j == 1 then
					local x, y = card:getPosition()
					if i == 1 or i == 3 then
						card:setPositionX(x + 20)						--每次抓的牌
					elseif i == 2 then
						card:setPositionY(y - 20)						--每次抓的牌
					elseif i == 4 then
						card:setPositionY(y + 20)						--每次抓的牌
					end
				end
			end
			self.nodeHandDownCard[i]:getChildByTag(j):setVisible(false)
		end
	end

	self.bpBgCardArray = {}
	self.cbCardData = {}
	self.bCardGray = {}
	self.cbCardStatus = {}
	self.nCurrentTouchCardTag = 0
	self.nMovingIndex = 0
	self.nZOrderTemp = 0
	self.currentTag = 0
	self.cbListenList = {}
	self.bWin = false
	self.currentOutCard = 0
	self.nTableTailCardTag = 112
	self.nRemainCardNum = 112
	self.posTemp = cc.p(0, 0)
	self:promptListenOutCard(nil)
	self.bSpreadCardEnabled = false
	self.bSendOutCardEnabled = false
	self.cbBpBgCardData = {{}, {}, {}, {}}
	self.beginPoint = nil
end

function CardLayer:ctor(scene)
	self._scene = scene
	self._mjSkin = tonumber(cc.UserDefault:getInstance():getStringForKey("PM_INDEX", 0))
	
	cc.UserDefault:getInstance():setStringForKey("PM_INDEX", 2)
	cc.UserDefault:getInstance():setStringForKey("PM_INIT_INDEX_4", "2")
	
	-- local yspmIndex = cc.UserDefault:getInstance():getStringForKey("PM_INIT_INDEX_4")
	-- if not yspmIndex or yspmIndex == "" then
	-- 	self._mjSkin = 3
	-- 	cc.UserDefault:getInstance():setStringForKey("PM_INIT_INDEX_4", "1")
	-- 	cc.UserDefault:getInstance():setStringForKey("PM_INDEX", 3)
	-- end

	-- if self._mjSkin ~= nil and tonumber(self._mjSkin) == 0 then
	-- 	self._mjSkin = 2
	-- 	cc.UserDefault:getInstance():setStringForKey("PM_INDEX", 2)
	-- end

	self._mjSkin = 2

	self:onInitData()

	self.bpBgCardArray = {}

	ExternalFun.registerTouchEvent(self, true)
	--桌牌
	--self.nodeTableCard = self:createTableCard()
	--手牌
	self.nodeHandCard = self:createHandCard()
	--铺着的手牌
	self.nodeHandDownCard = self:createHandDownCard()
	--弃牌
	self.nodeDiscard = self:createDiscard()
	--碰或杠牌
	self.nodeBpBgCard = self:createBpBgCard()
end

--创建立着的手牌
function CardLayer:createHandCard()
	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._mjSkin
	local res = 
	{
		skinPath .. "/pai_bg_bei.png",
		skinPath .. "/pai_bg_ce.png", 
		skinPath .. "/pai_bg_zheng.png",
		skinPath .. "/pai_bg_ce_r.png"
	}

	if self._scene._scene._isReplay then
		res = 
		{
			skinPath .. "/pai_bg_dian_chu.png",
			skinPath .. "/pai_bg_dao_small.png", 
			skinPath .. "/pai_bg_zheng.png",
			skinPath .. "/pai_bg_dao_small.png"
		}

		posHandCard = {cc.p(460, 655), cc.p(165, 260), cc.p(1270, 8), cc.p(1125, 650)}
	end


	local nodeCard = {}

	for i = 1, cmd.GAME_PLAYER do
		local bVert = i == 1 or i == cmd.MY_VIEWID
		local width = 0
		local height = 0
		local fSpacing = 0
		if i == 1 then
			width = 47
			height = 66
		elseif i == cmd.MY_VIEWID then
			width = CARD_WIDTH
			height = CARD_HEIGHT
		elseif i == 2 then
			width = 25
			height = 50
		elseif i == 4 then
			width = 25
			height = 50
		end
		fSpacing = width

		if self._scene._scene._isReplay then
			if i == 2 or i == 4 then
				width = 47
				height = 39
				fSpacing = height - 10				
			elseif i == 1 then
				width = 40
				height = 58
				fSpacing = width
			end
		end

		local widthTotal = fSpacing*cmd.MAX_COUNT
		local heightTotal = height + fSpacing*(cmd.MAX_COUNT - 1)
		nodeCard[i] = cc.Node:create()
			:move(posHandCard[i])
			:setContentSize(bVert and cc.size(widthTotal, height) or cc.size(width, heightTotal))
			:setAnchorPoint(anchorPointHandCard[i])
			:addTo(self, 1)

		for j = 1, cmd.MAX_COUNT do
			if i == 1 then
				pos = cc.p(width / 2 + fSpacing*(j - 1), height/2)
			elseif i == 2 then
				pos = cc.p(width/2, height/2 + fSpacing*(j - 1))
			elseif i == cmd.MY_VIEWID then
				pos = cc.p(widthTotal - width/2 - fSpacing*(j - 1), height/2)
			elseif i == 4 then
				pos = cc.p(width/2, heightTotal - height/2 - fSpacing*(j - 1))
			end

			local card = cc.Sprite:create(res[i])
				:move(pos)
				:setTag(j)
				:setVisible(false)
				:addTo(nodeCard[i])

			if i == cmd.MY_VIEWID or self._scene._scene._isReplay then
				local cardData = GameLogic.MAGIC_DATA
				card.cardData = cardData
				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(cardData))

				local dy = -15
				local strFile = skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png"

				if self._scene._scene._isReplay and (i == 2) then
					card:setLocalZOrder(30 - j)	--修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
				end

				local font = cc.Sprite:create(strFile)
				font:move(width/2, height/2 + dy)
				font:setTag(1)
				font:addTo(card)

				if cardData == 0x35 then
				    --local lai = cc.Sprite:create(cmd.RES_PATH.."game/lai.png"):move(70.5,112):addTo(card)
				end

				if self._scene._scene._isReplay and (i == 1 or i == 2 or i == 4) then
					local csize = card:getContentSize()
					if i == 1 then
						font:setPosition(cc.p(csize.width/2, csize.height/2 + 8))
						font:setScale(0.45)
					elseif i == 2 then
						font:setPosition(cc.p(csize.width/2, csize.height/2 + 5))
						font:setScale(0.40)
						font:setRotation(90)
					elseif i == 4 then
						font:setPosition(cc.p(csize.width/2, csize.height/2 + 5))
						font:setScale(0.40)
						font:setRotation(-90)
					end
				end

				if j == 1 then
					local x, y = card:getPosition()
					if i == 1 then
						card:setPositionX(x - 20)						--每次抓的牌
					elseif i == 2 then
						card:setPositionY(y - 20)						--每次抓的牌
					elseif i == 4 then
						card:setPositionY(y + 20)						--每次抓的牌
					elseif i == 3 then
						card:setPositionX(x + 20)						--每次抓的牌
					end
				end

				--提示听牌的小标记
				local promptFlag = cc.Sprite:create(cmd.RES_PATH ..  "gamesceneplist/sp_listenPromptFlag.png")
				promptFlag:move(69/2, CARD_HEIGHT + 25)
				promptFlag:setTag(CardLayer.TAG_LISTEN_FLAG)
				promptFlag:setVisible(false)
				promptFlag:addTo(card)
			elseif i == 2 then
				card:setLocalZOrder(30 - j)	--修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
			end
		end
	end
	nodeCard[cmd.MY_VIEWID]:setLocalZOrder(2)

	return nodeCard
end

--给会牌添加标记
function CardLayer:initLaiziCard()
	
end

--创建铺着的手牌
function CardLayer:createHandDownCard()
	local fSpacing = {40, 28, CARD_WIDTH, 28}
	local fontBigFile = "font_big"
	local fontSmallFile = "font_small"
	local fontSmallSideFile = "font_small_side"

	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._mjSkin

	local res = 
	{
		skinPath .. "/pai_bg_dian_chu_bei.png",
		skinPath .. "/pai_bg_ce_bg.png",
		skinPath .. "/pai_bg_bei_kei.png",
		skinPath .. "/pai_bg_ce_bg.png"
	}

	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
			:move(posHandDownCard[i])
			:setAnchorPoint(cc.p(0.5, 0.5))
			:addTo(self, 2)
		for j = 1, cmd.MAX_COUNT do
			local card = cc.Sprite:create(res[i])
				:setVisible(false)
				:setTag(j)
				:addTo(nodeCard[i])
			if i == 1 or i == 3 then
				card:move(fSpacing[i]*j, 0)
			else
				card:move(0, -fSpacing[i]*j)
			end
			if i == 2 then
				--card:setLocalZOrder(30 - j)
			end
		end
	end

	return nodeCard
end

--创建弃牌
function CardLayer:createDiscard()
	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
			:move(posDiscard[i])
			:addTo(self)
	end
	--nodeCard[1]:setLocalZOrder(2)

	return nodeCard
end
--创建碰或杠牌
function CardLayer:createBpBgCard()
	if self._scene._scene._isReplay then
		posBpBgCard = {cc.p(1160, 680), cc.p(190, 735), cc.p(0, 65), cc.p(1150, 164)}
	end
	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
		nodeCard[i]:move(posBpBgCard[i])
		nodeCard[i]:addTo(self)
	end
	nodeCard[cmd.MY_VIEWID]:setLocalZOrder(2)
	nodeCard[4]:setLocalZOrder(1)

	return nodeCard
end
function CardLayer:onTouchBegan(touch, event)
	if not self._scene.ruleDisplayLayout then
		--说明UI还未初始化完成
		return false
	end
	self._scene.ruleDisplayLayout:setVisible(false)

	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end

	local bCanOutCard = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2

	if not bCanOutCard then
		return false
	end

	if self._scene.isChoicesPopuped then
		return false
	end

	local pos = touch:getLocation()
	local parentPosX, parentPosY = self.nodeHandCard[cmd.MY_VIEWID]:getPosition()
	local parentSize = self.nodeHandCard[cmd.MY_VIEWID]:getContentSize()
	local posRelativeCard = cc.p(0, 0)
	posRelativeCard.x = pos.x - (parentPosX - parentSize.width)
	posRelativeCard.y = pos.y - parentPosY

	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		local cardRect = card:getBoundingBox()
		if cc.rectContainsPoint(cardRect, posRelativeCard) and card:isVisible() and not self.bCardGray[i] then
			self.nCurrentTouchCardTag = i
			self.beginPoint = pos
			--缓存
			self.posTemp.x, self.posTemp.y = card:getPosition()
			self.nZOrderTemp = card:getLocalZOrder()
			--将牌补满(ui与值的对齐方式)
			local nCount = 0
			local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
			if num == 2 then
				nCount = self.cbCardCount[cmd.MY_VIEWID]
			elseif num == 1 then
				nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
			else
				assert(false)
			end
			local index = nCount - i + 1

			return true
		end
	end

	return false
end
function CardLayer:onTouchMoved(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end

	local bCanOutCard = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2
	if not bCanOutCard then
		return false
	end

	local pos = touch:getLocation()

	local parentPosX, parentPosY = self.nodeHandCard[cmd.MY_VIEWID]:getPosition()
	local parentSize = self.nodeHandCard[cmd.MY_VIEWID]:getContentSize()
	local posRelativeCard = cc.p(0, 0)
	posRelativeCard.x = pos.x - (parentPosX - parentSize.width)
	posRelativeCard.y = pos.y - parentPosY

	if self.beginPoint and math.pow(pos.x - self.beginPoint.x,2) + math.pow(pos.y - self.beginPoint.y,2)  < 15*15 then
		
	   return true
	end

	--移动
	if self.nCurrentTouchCardTag ~= 0 then
		--将牌补满(ui与值的对齐方式)
		local nCount = 0
		local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
		if num == 2 then
			nCount = self.cbCardCount[cmd.MY_VIEWID]
		elseif num == 1 then
			nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
		else
			assert(false)
		end
		local index = nCount - self.nCurrentTouchCardTag + 1

		self.cbCardStatus[index] = CardLayer.ENUM_CARD_MOVING
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(self.nCurrentTouchCardTag)
		card:setPosition(posRelativeCard)
		card:setLocalZOrder(CardLayer.Z_ORDER_TOP)
		--有则提示听牌
		local cbPromptHuCard = self._scene._scene:getListenPromptHuCard(self.cbCardData[index])
		if self.nMovingIndex ~= index and math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 then
			self._scene:setListeningCard(cbPromptHuCard)
		end
		self.nMovingIndex = index
	end

	return true
end
function CardLayer:onTouchEnded(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end

	local bCanOutCard = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2
	if not bCanOutCard then
		return false
	end
	
	if self.nCurrentTouchCardTag == 0 then
		return true
	end

	local pos = touch:getLocation()

	local parentPosX, parentPosY = self.nodeHandCard[cmd.MY_VIEWID]:getPosition()
	local parentSize = self.nodeHandCard[cmd.MY_VIEWID]:getContentSize()
	local posRelativeCard = cc.p(0, 0)
	posRelativeCard.x = pos.x - (parentPosX - parentSize.width)
	posRelativeCard.y = pos.y - parentPosY

	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		local cardRect = card:getBoundingBox()
		if cc.rectContainsPoint(cardRect, posRelativeCard) and card:isVisible() and not self.bCardGray[i] then
			--将牌补满(ui与值的对齐方式)
			local nCount = 0
			local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
			if num == 2 then
				nCount = self.cbCardCount[cmd.MY_VIEWID]
			elseif num == 1 then
				nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
			else
				assert(false)
			end
			local index = nCount - i + 1		--算出这张牌对应牌值在self.cbCardData里的下标(cbCardData与cbCardStatus保持一致)
			if self.nCurrentTouchCardTag == i then
				if self.cbCardStatus[index] == CardLayer.ENUM_CARD_NORMAL then 		--原始状态
					--恢复
					self.cbCardStatus = {}
					for v = 1, cmd.MAX_COUNT do
						local cardTemp = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(v)
						cardTemp:setPositionY(CARD_HEIGHT/2)
					end
					--弹出
					self.cbCardStatus[index] = CardLayer.ENUM_CARD_POPUP
					card:setPositionY(CARD_HEIGHT/2 + 30)

					self:findSameCardFromDiscard(card.cardData)

					--有则提示听牌
					if math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 then
						local cbPromptHuCard = self._scene._scene:getListenPromptHuCard(self.cbCardData[index])
						self._scene:setListeningCard(cbPromptHuCard)
					end
				elseif self.cbCardStatus[index] == CardLayer.ENUM_CARD_POPUP then 		--弹出状态
					if math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 then
						--出牌"
						if self:touchSendOutCard(self.cbCardData[index], card) then
							-- card:setVisible(false)
							self:removeHandCard(cmd.MY_VIEWID, {self.cbCardData[index]}, true)
						end
					else
						--弹回
					end
					if self.cbCardStatus == nil then
						self.cbCardStatus = {}
					end
					self.cbCardStatus[index] = CardLayer.ENUM_CARD_NORMAL
					card:setPositionY(CARD_HEIGHT/2)
				elseif self.cbCardStatus[index] == CardLayer.ENUM_CARD_MOVING then 		--移动状态
					--恢复
					self.cbCardStatus = {}
					for v = 1, cmd.MAX_COUNT do
						local cardTemp = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(v)
						cardTemp:setPositionY(CARD_HEIGHT/2)
					end
					self.cbCardStatus[index] = CardLayer.ENUM_CARD_POPUP
					card:setPosition(self.posTemp.x, CARD_HEIGHT/2 + 30)
					card:setLocalZOrder(self.nZOrderTemp)
					--判断
					--local rectDiscardPool = cc.rect(324, 218, 686, 283)
					if math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 and pos.y >= 190 then
						--出牌"
						if self:touchSendOutCard(self.cbCardData[index], card) then
							-- card:setVisible(false)
							card:setPosition(self.posTemp.x, CARD_HEIGHT/2)
							self:removeHandCard(cmd.MY_VIEWID, {self.cbCardData[index]}, true)
						end
					end
				elseif self.cbCardStatus[index] == CardLayer.ENUM_CARD_OUT then 		--已出牌状态
					assert(false)
				end
				break
			end
		end
		--规避没点到牌的情况
		if i == cmd.MAX_COUNT then
			local cardTemp = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(self.nCurrentTouchCardTag)
			cardTemp:setPosition(self.posTemp)
			cardTemp:setLocalZOrder(self.nZOrderTemp)
			self.cbCardStatus = {}
		end
	end
	self.nCurrentTouchCardTag = 0
	self.nZOrderTemp = 0
	return true
end

function CardLayer:onTouchCancelled(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end
	
	for j = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(j)
		width = CARD_WIDTH
		height = CARD_HEIGHT
		fSpacing = width
		local widthTotal = fSpacing*cmd.MAX_COUNT
		local posX = widthTotal - width/2 - fSpacing*(j - 1)
		local posY = height/2
		card:setPosition(posX, posY)
		if j == 1 then
			card:setPosition(posX + 20, posY)						--每次抓的牌
		end
	end
end

--弹出指定的牌
function CardLayer:popupCard(cards, op)
	--先收齐点出的牌
	self:unpopupAllCard()

	for i,aCard in ipairs(cards) do
		for j, aCardData in ipairs(self.cbCardData) do
			if aCard == aCardData then
				local bPopup = false
				--弹出
				for k = 1, cmd.MAX_COUNT do
					--将牌补满(ui与值的对齐方式)
					local nCount = 0
					local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
					if num == 2 then
						nCount = self.cbCardCount[cmd.MY_VIEWID]
					elseif num == 1 then
						nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
					else
						assert(false)
					end
					local index = nCount - k + 1
					if index == j then
						local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(k)
						self.cbCardStatus[j] = CardLayer.ENUM_CARD_POPUP
						card:setPositionY(CARD_HEIGHT/2 + 30)
						bPopup = true
					end
				end
				if bPopup and (op == GameLogic.WIK_LEFT or op == GameLogic.WIK_CENTER or op == GameLogic.WIK_RIGHT) then
					break
				end
			end
		end
	end
end

--收入所有牌
function CardLayer:unpopupAllCard()
	for j = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(j)
		width = CARD_WIDTH
		height = CARD_HEIGHT
		fSpacing = width
		local widthTotal = fSpacing*cmd.MAX_COUNT
		local posX = widthTotal - width/2 - fSpacing*(j - 1)
		local posY = height/2
		card:setPosition(posX, posY)

		local nCount = 0
		local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
		if num == 2 then
			nCount = self.cbCardCount[cmd.MY_VIEWID]
		elseif num == 1 then
			nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
		else
			assert(false)
		end

		local index = nCount - j + 1
		self.cbCardStatus[index] = CardLayer.ENUM_CARD_NORMAL
		if j == 1 then
			card:setPosition(posX + 20, posY)						--每次抓的牌
		end
	end
end

--发牌
function CardLayer:sendCard(meData, cardCount, otherCbCardData)
	assert(type(meData) == "table" and type(cardCount) == "table")
	self.bSpreadCardEnabled = true
	--其他三家的牌
	self.otherCbCardData = otherCbCardData
	self.cbCardData = meData
	self.cbCardCount = cardCount

	local fDelayTime = 0.05
	for i = 1, cmd.GAME_PLAYER do
		if cardCount[i] > 0 then
			self:spreadCard(i, fDelayTime, 1)
		end
	end

	local fDelayTimeMax = fDelayTime*cmd.MAX_COUNT + 0.3
	self:runAction(cc.Sequence:create(
		cc.DelayTime:create(fDelayTimeMax),
		cc.CallFunc:create(function(ref)
			self._scene._scene:PlaySound(cmd.RES_PATH.."sound/OUT_CARD.mp3")
			self:spreadCardFinish()
		end)))
end

--伸展牌
function CardLayer:spreadCard(viewId, fDelayTime, nCurrentCount)
	if not self.bSpreadCardEnabled then
		return false
	end
	
	local nodeParent = self.nodeHandDownCard[viewId]
	if nCurrentCount <= self.cbCardCount[viewId] then
		--local posX, posY = nodeParent:getPosition()
		local downCard = nodeParent:getChildByTag(nCurrentCount)
		local downCardSize = downCard:getContentSize()
		for i = 1, nCurrentCount do
			local downCardTemp = nodeParent:getChildByTag(i)
			downCardTemp:setVisible(true)
		end

		if viewId == 1 then
			nodeParent:setPositionX(display.cx - downCardSize.width/2*nCurrentCount - 57 + 135)
		elseif viewId == 3 then
			nodeParent:setPositionX(display.cx - downCardSize.width/2*nCurrentCount - 57)
		else
			nodeParent:setPositionY(display.cy + downCardSize.height/2*nCurrentCount)
		end
		
		self:runAction(cc.Sequence:create(
			cc.DelayTime:create(fDelayTime),
			cc.CallFunc:create(function(ref)
				return self:spreadCard(viewId, fDelayTime, nCurrentCount + 1)
			end)))
	else
		self:runAction(cc.Sequence:create(
			cc.DelayTime:create(0.5),
			cc.CallFunc:create(function()
				for i = 1, nCurrentCount do
					local downCardTemp = nodeParent:getChildByTag(i)
					if downCardTemp then
						downCardTemp:setVisible(false)
					end
				end
			end)))
		
		return true
	end
end

--发完牌
function CardLayer:spreadCardFinish()
	GameLogic.SortCardList(self.cbCardData)
	for i = 1, cmd.GAME_PLAYER do
		self.nRemainCardNum = self.nRemainCardNum - self.cbCardCount[i]
		for j = 1, cmd.MAX_COUNT do
			self.nodeHandDownCard[i]:getChildByTag(j):setVisible(false)
		end
		if self._scene._scene._isReplay then
			local startCmd = GameplayerManager:getInstance():getGameStartData(i)
			if startCmd then
				self:setHandCard(i, self.cbCardCount[i], startCmd["t"].cbCardData[1])
			end
		else
			self:setHandCard(i, self.cbCardCount[i], self.cbCardData)
		end
		
	end

	self._scene:setRemainCardNum(self.nRemainCardNum)
	self._scene:sendCardFinish()
end

--抓牌
function CardLayer:catchCard(viewId, cardData, bTail)
	-- assert(math.mod(self.cbCardCount[viewId], 3) == 1 or bTail, "Can't catch card!")
	
	self._scene._scene:playRandomSound(viewId)

	local HandCard = self.nodeHandCard[viewId]:getChildByTag(1)
	HandCard.cardData = cardData
	HandCard:setVisible(true)
	self.cbCardCount[viewId] = self.cbCardCount[viewId] + 1
	self.nRemainCardNum = self.nRemainCardNum - 1
	self._scene:setRemainCardNum(self.nRemainCardNum)

	--抓牌的数据
	local cbCardDataTemp = nil
	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		cbCardDataTemp = GameplayerManager:getInstance():getPlayerHandCard(viewId)
	end

	if viewId == cmd.MY_VIEWID then
		cbCardDataTemp = self.cbCardData
	end

	if viewId == cmd.MY_VIEWID or self._scene._scene._isReplay then
		if not self._scene._scene._isReplay then
			self:findSameCardFromDiscard(HandCard.cardData)
		end

		local skinPath = cmd.RES_PATH ..  "game/mahjongCard_" .. self._mjSkin

		table.insert(cbCardDataTemp, cardData)
		
		local nValue = math.mod(cardData, 16)
		local nColor = math.floor(cardData/16)
		local strFile = skinPath .. "/card_color_" .. nColor .. "_num_" .. nValue .. ".png"

		local cardFont = HandCard:getChildByTag(CardLayer.TAG_CARD_FONT)
		cardFont:setTexture(strFile)
		HandCard:removeChildByTag(666)

		local huipaFilePath = "#huipaiflag.png"
		local hpscale = 1.0
		local dx = 0
		local dy = 0
		local rotate = 0
		local flpx = false

		if self._scene._scene._isReplay then
			if viewId == 1 then
				huipaFilePath = "#r_m_huipai.png"
				hpscale = 0.5
				dy = 1
			elseif viewId == 2 then
				huipaFilePath = "#r_s_up_huipai.png"
				hpscale = 0.8
				rotate = 90
				flpx = false
				dx = -2
				dy = 2
			elseif viewId == 4 then
				huipaFilePath = "#r_s_up_huipai.png"
				hpscale = 0.8
				rotate = -90
				flpx = false
				dx = 3
				dy = 10
			end
		end

		if cardData == GameLogic.MAGIC_DATA then
			local size = HandCard:getContentSize()
			cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
			display.newSprite(huipaFilePath)
			    :move(size.width/2+dx, size.height/2+dy)
			    :setTag(666)
			    :setScale(hpscale)
			    :setRotation(rotate)
			    :setFlippedY(flpx)
			    :addTo(HandCard)

			HandCard:setColor(cc.c3b(150,150,150))
			local cardFont = HandCard:getChildByTag(CardLayer.TAG_CARD_FONT)
			if cardFont then
				cardFont:setColor(cc.c3b(150,150,150))
			end
		else
			HandCard:setColor(cc.c3b(255,255,255))
			local cardFont = HandCard:getChildByTag(CardLayer.TAG_CARD_FONT)
			if cardFont then
				cardFont:setColor(cc.c3b(255,255,255))
			end
		end

		--假如可以听牌
		local cbPromptCardData = self._scene._scene:getListenPromptOutCard()
		if #cbPromptCardData > 0 then
			self:promptListenOutCard(cbPromptCardData)
		end
	end

	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		GameplayerManager:getInstance():setPlayerHandCard(viewId, cbCardDataTemp)
	end

	if viewId == cmd.MY_VIEWID then
		self.cbCardData = cbCardDataTemp
	end

end

-- 遍历数组
function CardLayer:isInTable(value, tb)
	for k,v in ipairs(tb) do
	  if v == value then
	  	return true;
	  end
	end
	return false;
end

--获取某个元素在table中出现的次数
function CardLayer:getCountOfTab(value, tb)
	local count = 0
	for k,v in ipairs(tb) do
	  if v == value then
	  	count = count + 1
	  end
	end
	return count;
end

--获取可以用来吃的癞子牌
function CardLayer:getEatCardType(index)
	--癞子牌不能用来吃

	--如果是右吃
	local isRightEat = self:isInTable(index-1, self.cbCardData)
	if isRightEat == true and (index-1) ~= GameLogic.MAGIC_DATA  then
		isRightEat = self:isInTable(index-2, self.cbCardData)
		if isRightEat == true and (index-2) ~= GameLogic.MAGIC_DATA then
			return GameLogic.WIK_RIGHT
		end
	end

	--如果是左吃
	local isLeftEat = self:isInTable(index+1, self.cbCardData)
	if isLeftEat == true and (index+1) ~= GameLogic.MAGIC_DATA then
		isLeftEat = self:isInTable(index+2, self.cbCardData)
		if isLeftEat == true and (index+2) ~= GameLogic.MAGIC_DATA then
			return GameLogic.WIK_LEFT
		end
	end

	--如果是中吃
	local isCenterEat = self:isInTable(index-1, self.cbCardData)
	if isCenterEat == true and (index-1) ~= GameLogic.MAGIC_DATA then
		isCenterEat = self:isInTable(index+1, self.cbCardData)
		if isCenterEat == true and (index+1) ~= GameLogic.MAGIC_DATA then
			return GameLogic.WIK_CENTER
		end
	end

	return nil
end

--设置本方牌值
function CardLayer:setHandCard(viewId, cardCount, meData)
	assert(type(meData) == "table")
	if viewId == cmd.MY_VIEWID then
		self.cbCardData = meData
	end
	self.cbCardCount[viewId] = cardCount
	self.bSendOutCardEnabled = true

	--先全部隐藏
	for j = 1, cmd.MAX_COUNT do
		self.nodeHandCard[viewId]:getChildByTag(j):setVisible(false)
		self.nodeHandDownCard[viewId]:getChildByTag(j):setVisible(false)
	end
	--再显示
	if self.cbCardCount[viewId] ~= 0 then
		local nCount = 0
		local num = math.mod(self.cbCardCount[viewId], 3)
		if num == 2 then
			nCount = self.cbCardCount[viewId]
		elseif num == 1 then
			nCount = self.cbCardCount[viewId] + 1
		else
			assert(false)
		end

		local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._mjSkin

		cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
		for j = 1, self.cbCardCount[viewId] do
			local card = self.nodeHandCard[viewId]:getChildByTag(nCount - j + 1)
			card.cardData = meData[j]
			card:setVisible(true)
			if viewId == cmd.MY_VIEWID or self._scene._scene._isReplay then
				local cardFont = card:getChildByTag(CardLayer.TAG_CARD_FONT)

				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(meData[j]))
				if nValue ~= nil and nColor ~= nil then

					local strFile = skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png"

					card:removeChildByTag(666)

					local huipaFilePath = "#huipaiflag.png"
					local hpscale = 1.0
					local dx = 0
					local dy = 0
					local rotate = 0
					local flpx = false

					if self._scene._scene._isReplay then
						if viewId == 1 then
							huipaFilePath = "#r_m_huipai.png"
							hpscale = 0.5
							dy = 1
						elseif viewId == 2 then
							huipaFilePath = "#r_s_up_huipai.png"
							hpscale = 0.8
							rotate = 90
							flpx = true
							dx = -2
							dy = 2
						elseif viewId == 4 then
							huipaFilePath = "#r_s_up_huipai.png"
							hpscale = 0.8
							rotate = -90
							flpx = true
							dx = 3
							dy = 10
						end
					end

					cardFont:setTexture(strFile)

					if meData[j] == GameLogic.MAGIC_DATA then
						local size = card:getContentSize()
						display.newSprite(huipaFilePath)
						    :move(size.width/2 + dx, size.height/2 + dy)
						    :setTag(666)
						    :setScale(hpscale)
						    :setRotation(rotate)
						    :addTo(card)			    
						card:setColor(cc.c3b(150,150,150))
						local cardFont = card:getChildByTag(CardLayer.TAG_CARD_FONT)
						if cardFont then
							cardFont:setColor(cc.c3b(150,150,150))
						end
					else
						card:setColor(cc.c3b(255,255,255))
						local cardFont = card:getChildByTag(CardLayer.TAG_CARD_FONT)
						if cardFont then
							cardFont:setColor(cc.c3b(255,255,255))
						end
					end
				end
			end
		end
	end

end

--删除手上的牌
function CardLayer:removeHandCard(viewId, cardData, bOutCard)
	assert(type(cardData) == "table")
	local cbRemainCount = self.cbCardCount[viewId] - #cardData
	if bOutCard and math.mod(cbRemainCount, 3) ~= 1 then
		return false
	end
	self.cbCardCount[viewId] = cbRemainCount

	local cbCardDataTemp = self.cbCardData

	if viewId == cmd.MY_VIEWID then
		cbCardDataTemp = self.cbCardData
	end

	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		--获取某个视图的手牌
		cbCardDataTemp = GameplayerManager:getInstance():getPlayerHandCard(viewId)
	end

	--如果删除手牌的人是自己
	if viewId == cmd.MY_VIEWID or self._scene._scene._isReplay then
		for i = 1, #cardData do
			for j = #cbCardDataTemp, 1, -1 do
				if cbCardDataTemp[j] == cardData[i] then
					table.remove(cbCardDataTemp, j)
					break
				end
				-- assert(j ~= #cbCardDataTemp, "WithOut this card to remove!")
			end
		end
		GameLogic.SortCardList(cbCardDataTemp)
	end

	if viewId == cmd.MY_VIEWID then 
		self.cbCardData = cbCardDataTemp
	end

	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		--获取某个视图的手牌
		GameplayerManager:getInstance():setPlayerHandCard(viewId, cbCardDataTemp)
	end

	self:setHandCard(viewId, self.cbCardCount[viewId], cbCardDataTemp)

	return true
end

--获取下一张打出去的牌的位置
function CardLayer:getDiscardPosition(viewId)
	local width = 0
	local height = 0
	local fSpacing = 0
	local posX = 0
	local posY = 0
	local pos = cc.p(0, 0)
	local nLimit = 0
	local fBase = 0
	local countTemp = self.nDiscardCount[viewId]
	local bVert = viewId == 1 or viewId == cmd.MY_VIEWID
	if bVert then
		width = 44
		height = 54
		local LineSpan = 45
		if self._mjSkin == 1 then
			width = 36
			height = 44
		elseif self._mjSkin == 3 then
			width = 36
			height = 44
			LineSpan = 51
		end

		fSpacing = width
		nLimit = 7

		while countTemp >= nLimit*2 do   	--超过一层
			fBase = fBase + height - LineSpan
			countTemp = countTemp - nLimit*2
		end

		while countTemp >= nLimit do 		--超过一行
			posY = posY - height
			countTemp = countTemp - nLimit
		end

		local posX = fSpacing*countTemp
		pos = viewId == cmd.MY_VIEWID and cc.p(posX, posY + fBase) or cc.p(-posX, -posY + fBase)
	else
		width = 55
		height = 47
		fSpacing = 35
		if self._mjSkin == 1 then
			width = 49
			height = 33
			fSpacing = 33
		elseif self._mjSkin == 3 then
			width = 50
			height = 31
			fSpacing = 31
		end
		
		nLimit = 7

		while countTemp >= nLimit*2 do 		--超过一层
			fBase = fBase + 14
			countTemp = countTemp - nLimit*2
		end

		while countTemp >= nLimit do 		--超过一行
			posX = posX - width
			countTemp = countTemp - nLimit
		end

		local posY = fSpacing*countTemp
		pos = viewId == 4 and cc.p(-posX, posY + fBase) or cc.p(posX, -posY + fBase)
	end

	local worldPosition = self.nodeDiscard[viewId]:convertToWorldSpace(pos)
	local runScene = cc.Director:getInstance():getRunningScene()
	local nodePosition = self._scene:convertToNodeSpace(worldPosition)
	return nodePosition
end

--牌打到弃牌堆
function CardLayer:discard(viewId, cardData)
	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._mjSkin
	local cardSkinPath = skinPath .. "/40"

	local scaleRate = {0.4, 0.4, 0.4, 0.4}

	local width = 0
	local height = 0
	local fSpacing = 0
	local posX = 0
	local posY = 0
	local pos = cc.p(0, 0)
	local nLimit = 0
	local fBase = 0
	local countTemp = self.nDiscardCount[viewId]
	local bVert = viewId == 1 or viewId == cmd.MY_VIEWID
	if bVert then
		width = 40
		height = 48
		local LineSpan = 0

		fSpacing = width
		nLimit = 7

		while countTemp >= nLimit*2 do   	--超过一层
			fBase = fBase + height - LineSpan
			countTemp = countTemp - nLimit*2
		end

		while countTemp >= nLimit do 		--超过一行
			posY = posY - height
			countTemp = countTemp - nLimit
		end

		local posX = fSpacing*countTemp
		pos = viewId == cmd.MY_VIEWID and cc.p(posX, posY + fBase) or cc.p(-posX, -posY + fBase)
	else
		width = 48
		height = 47
		fSpacing = 39
		
		nLimit = 7

		while countTemp >= nLimit*2 do 		--超过一层
			fBase = fBase + 14
			countTemp = countTemp - nLimit*2
		end

		while countTemp >= nLimit do 		--超过一行
			posX = posX - width
			countTemp = countTemp - nLimit
		end

		local posY = fSpacing*countTemp
		pos = viewId == 4 and cc.p(-posX, posY + fBase) or cc.p(posX, -posY + fBase)
	end

	--local rectX = self:switchToCardRectX(cardData)
	local nValue = math.mod(cardData, 16)
	local nColor = math.floor(cardData/16)

	local cardDownFileName = ""
	if viewId == 1 then
		cardDownFileName = "pai_bg_dian_chu.png"
	elseif viewId == 2 or viewId == 4 then
		cardDownFileName = "pai_bg_dao.png"
	elseif viewId == cmd.MY_VIEWID then
		cardDownFileName = "pai_bg_dian_chu.png"
	end
	--牌底
	local card = cc.Sprite:create(skinPath .. "/" .. cardDownFileName)
	card:move(pos)
	card:setTag(self.nDiscardCount[viewId] + 1)
	card.cardData = cardData
	card:addTo(self.nodeDiscard[viewId])
	--字体
	local strFile = cardSkinPath .. "/40_card_color_"..nColor.."_num_"..nValue..".png"
	local ddy = 0
	if viewId == 1 then
		ddy = 7
	elseif viewId == 2 then
		ddy = 6
	elseif viewId == 3 then
		ddy = 7
	elseif viewId == 4 then
		ddy = 6
	end
	local cardSize = card:getContentSize()
	local cardFont = cc.Sprite:create(strFile)
		:move(cardSize.width/2, cardSize.height/2 + ddy)
		:addTo(card)
	if viewId == 1 then
		cardFont:setRotation(180)	
	elseif viewId == 2 then
		cardFont:setRotation(90)
	elseif viewId == 3 then

	elseif viewId == 4 then
		cardFont:setRotation(-90)
	end

	-- cardFont:setScale(scaleRate[viewId])

	--修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
	if viewId == 1 or viewId == 4 then
		local nOrder = 0
		if self.nDiscardCount[viewId] >= nLimit*6 then
			assert(false)
		elseif self.nDiscardCount[viewId] >= nLimit*4 then
			nOrder = 80 - (self.nDiscardCount[viewId] - nLimit*4)
		elseif self.nDiscardCount[viewId] >= nLimit*2 then
			nOrder = 80 - (self.nDiscardCount[viewId] - nLimit*2)*2
		else
			nOrder = 80 - self.nDiscardCount[viewId]*3
		end
		card:setLocalZOrder(nOrder)
	end
	--计数
	self.nDiscardCount[viewId] = self.nDiscardCount[viewId] + 1
end

--从弃牌堆回收牌（有人要这张牌，碰或杠等）
function CardLayer:recycleDiscard(viewId)
	self.nodeDiscard[viewId]:getChildByTag(self.nDiscardCount[viewId]):removeFromParent()
	self.nDiscardCount[viewId] = self.nDiscardCount[viewId] - 1
end

function CardLayer:stopSameCard()
	for i = 1, 4 do
		local children = self.nodeDiscard[i]:getChildren()
		for j = 1, #children do
			children[j]:stopAllActions()
			children[j]:setColor(cc.c3b(255,255,255))
			local cardFont = children[j]:getChildByTag(CardLayer.TAG_CARD_FONT)
			if cardFont then
				cardFont:stopAllActions()
				cardFont:setColor(cc.c3b(255,255,255))
			end
		end
	end

	for i = 1, #self.bpBgCardArray do
		if self.bpBgCardArray[i] and self.bpBgCardArray[i].cardData then
			self.bpBgCardArray[i]:stopAllActions()
			self.bpBgCardArray[i]:setColor(cc.c3b(255,255,255))
			local cardFont = self.bpBgCardArray[i]:getChildByTag(CardLayer.TAG_CARD_FONT)
			if cardFont then
				cardFont:stopAllActions()
				cardFont:setColor(cc.c3b(255,255,255))
			end
		end
	end
end

--cardData从牌堆里查找相同点数的牌
function CardLayer:findSameCardFromDiscard(cardData)
	self:stopSameCard()
	local sameCardArray = {}
	for i = 1, 4 do
		local children = self.nodeDiscard[i]:getChildren()
		for j = 1, #children do
			if children[j].cardData and children[j].cardData == cardData then
				table.insert(sameCardArray, children[j])
			end
		end
	end

	for i = 1, #self.bpBgCardArray do
		if self.bpBgCardArray[i] and self.bpBgCardArray[i].cardData and self.bpBgCardArray[i].cardData == cardData then
			table.insert(sameCardArray, self.bpBgCardArray[i])
		end
	end
	
	for k,v in pairs(sameCardArray) do
		-- 用持续时间和颜色创建动作，第一个参数为持续时间，后面三个为颜色值  
	  	local action1 = cc.TintTo:create(0.5, 255,228,150)
	  	local action2 = cc.TintTo:create(0.5, 255,255,255)
		v:runAction(cc.RepeatForever:create(cc.Sequence:create(action1:clone(), action2:clone())))
		local cardFont = v:getChildByTag(CardLayer.TAG_CARD_FONT)
		if cardFont then
			cardFont:runAction(cc.RepeatForever:create(cc.Sequence:create(action1:clone(), action2:clone())))
		end
	end
end

--碰或杠
function CardLayer:bumpOrBridgeCard(viewId, cbCardData, nShowStatus)
	assert(type(cbCardData) == "table")
	-- table.sort(cbCardData)
	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._mjSkin

	local resCard = 
	{
		skinPath .. "/pai_bg_shang.png",
		skinPath .. "/pai_bg_dao_small.png",  --pai_bg_dao
		skinPath .. "/pai_bg_dian.png",
		skinPath .. "/pai_bg_dao_small.png" --
	}

	local scaleRate = {
		0.5, 0.42, 0.85, 0.42
	}
	
	local width = 0
	local height = 0
	local widthTotal = 0
	local heightTotal = 0
	local fSpacing = 0
	if viewId == 1 then
		width = 43
		height = 61
		fSpacing = width
	elseif viewId == 3 then
		width = CARD_WIDTH * scaleRate[viewId]
		height = CARD_HEIGHT * scaleRate[viewId]
		fSpacing = width
	else
		width = 47
		height = 39
		fSpacing = 29

		width = width
		height = height
		fSpacing = fSpacing
	end

	local fN = {15, 10, 15, 10}
	local fParentSpacing = fSpacing*3 + fN[viewId]
	local nodeParent = cc.Node:create()
		:move(self.nBpBgCount[viewId]*fParentSpacing*multipleBpBgCard[viewId][1], 
				self.nBpBgCount[viewId]*fParentSpacing*multipleBpBgCard[viewId][2])
		:addTo(self.nodeBpBgCard[viewId])

	if nShowStatus ~= GameLogic.SHOW_CHI then
		--明杠
		if nShowStatus == GameLogic.SHOW_MING_GANG then
			nodeParentMG = self.nodeBpBgCard[viewId]:getChildByTag(cbCardData[1])
			--assert(nodeParentMG, "None of this bump card!")
			if nodeParentMG then
				self.nBpBgCount[viewId] = self.nBpBgCount[viewId] - 1
				nodeParent:removeFromParent()
				nodeParentMG:removeAllChildren()
				nodeParent = nodeParentMG
			end
		end
		nodeParent:setTag(cbCardData[1])
	end

	--个个牌形之间的间隔
	local isInsertPos = false
	for i = 1, #cbCardData do
		--local rectX = self:switchToCardRectX(cbCardData[i])
		--牌底
		local card = display.newSprite(resCard[viewId])
			:move(i*fSpacing*multipleBpBgCard[viewId][1], i*fSpacing*multipleBpBgCard[viewId][2])
			:addTo(nodeParent)
		card.cardData = cbCardData[i]
		table.insert(self.bpBgCardArray, card)
		--字体
		local cardSize = card:getContentSize()
		local nValue = math.mod(cbCardData[i], 16)
		local nColor = math.floor(cbCardData[i]/16)
		local strFile = skinPath .. "/card_color_" .. nColor .. "_num_" .. nValue .. ".png"
		local ddy = 15
		if viewId == 1 then
			ddy = 7
		elseif viewId == 2 then
			ddy = 5
		elseif viewId == cmd.MY_VIEWID then
			ddy = 18
		elseif viewId == 4 then
			ddy = 5
		end

		local cardFont = cc.Sprite:create(strFile)
		cardFont:move(cardSize.width/2, cardSize.height/2 + ddy)
		cardFont:setTag(1)
		cardFont:addTo(card)

		if viewId == 4 then
			card:setLocalZOrder(5 - i)
		end

		if viewId == 1 then
			cardFont:setScale(scaleRate[viewId])
		elseif viewId == 4 then
			cardFont:setRotation(-90)
			cardFont:setScale(scaleRate[viewId])
		elseif viewId == 2 then
			cardFont:setRotation(90)
			cardFont:setScale(scaleRate[viewId])
		else
			card:setScale(scaleRate[viewId])
		end

		if i == 4  then 		--杠
			local moveUp = {17, 10, 23, 10}
			card:move(2*fSpacing*multipleBpBgCard[viewId][1], 2*fSpacing*multipleBpBgCard[viewId][2] + moveUp[viewId])
			card:setLocalZOrder(5)
		end

		--皮子牌
		local piziCardData = GameLogic.getPiZiCardData()

		--皮子牌第三张移动位置
		if (nShowStatus == GameLogic.SHOW_AN_GANG or nShowStatus == GameLogic.SHOW_FANG_GANG) and i == 3 and cbCardData[i] == piziCardData then
			local moveUp = {17, 10, 23, 10}
			--3是自己
			local dx = 0
			local dy = 0
			if viewId == 1 then
				dx = - card:getContentSize().width/2
				dy = 0
			elseif viewId == 3 then
				dx = - card:getContentSize().width/2
				dy = 0
			elseif viewId == 4 then
				dx = 0
				dy = - card:getContentSize().height/2
			elseif viewId == 2 then
				dx = 0
				dy = card:getContentSize().height/2
			end
			card:move(2*fSpacing*multipleBpBgCard[viewId][1] + dx, 2*fSpacing*multipleBpBgCard[viewId][2] + moveUp[viewId] + dy)
			card:setLocalZOrder(5)
		end

		if nShowStatus == GameLogic.SHOW_AN_GANG and viewId ~= cmd.MY_VIEWID then 		--暗杠
			
			local backPath = skinPath .. "/pai_bg_bei_kei.png"
			if viewId == 2 or viewId == 4 then
				backPath = skinPath .. "/pai_bg_ce_bg.png"
			end
			card:setTexture(backPath)
			card:removeChildByTag(1)
		end

		-- if (nShowStatus == GameLogic.SHOW_AN_GANG or nShowStatus == GameLogic.SHOW_MING_GANG) and i == 3 and cbCardData[i] == piziCardData then

		-- end

		

		if nShowStatus == GameLogic.SHOW_AN_GANG and viewId == cmd.MY_VIEWID then
			--如果不是皮子，那么前三张都盖住
			if i ~= 4 and cbCardData[i] ~= piziCardData then
				card:setTexture(skinPath .. "/pai_bg_bei_kei.png")
				card:removeChildByTag(1)
			end

			--如果是皮子杠，那么第三张显示
			if i ~= 3 and cbCardData[i] == piziCardData then
				card:setTexture(skinPath .. "/pai_bg_bei_kei.png")
				card:removeChildByTag(1)
			end
		end

		--添加牌到记录里
		if nShowStatus ~= GameLogic.SHOW_MING_GANG or i == 4 then
			--如果插入的只有一个，那就是碰后的杠
			if nShowStatus == GameLogic.SHOW_MING_GANG then
				local pos = 1
				while pos <= #self.cbBpBgCardData[viewId] do 
					if self.cbBpBgCardData[viewId][pos] == cbCardData[i] then
						table.insert(self.cbBpBgCardData[viewId], pos, cbCardData[i])
						break
					end
					pos = pos + 1
				end
			else
				table.insert(self.cbBpBgCardData[viewId], cbCardData[i])
			end
		end
	end

	if isInsertPos == false then
		if self.cbBpBgCardData[viewId][#self.cbBpBgCardData[viewId]] ~= -2 and self.cbBpBgCardData[viewId][#self.cbBpBgCardData[viewId]] ~= -1 then
			if GameLogic.SHOW_AN_GANG == nShowStatus then
				table.insert(self.cbBpBgCardData[viewId], -2)
			else
				table.insert(self.cbBpBgCardData[viewId], -1)
			end
		end
	end
	
	self.nBpBgCount[viewId] = self.nBpBgCount[viewId] + 1
end

--检查碰、杠牌里是否有这张牌
function CardLayer:checkBumpOrBridgeCard(viewId, cbCardData)
	local card = self.nodeBpBgCard[viewId]:getChildByTag(cbCardData)
	if card then
		return true
	else
		return false
	end
end

function CardLayer:getBpBgCardData()
	return self.cbBpBgCardData
end

function CardLayer:gameEnded()
	self.bSendOutCardEnabled = false
end

function CardLayer:switchToCardRectX(data)
	assert(data, "this card is nil")
	local cardIndex = GameLogic.SwitchToCardIndex(data)
	local rectX = cardIndex == GameLogic.MAGIC_INDEX and 32 or cardIndex
	return rectX
end

--使部分牌变灰（参数为不变灰的）
function CardLayer:makeCardGray(cbOutCardData)
	--assert(#self.cbListenList > 0 and math.mod(#self.cbCardData - 2, 3) == 0)
	local cbCardCount = #self.cbCardData
	--先全部变灰
	for i = cmd.MAX_COUNT, 1, -1 do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		card:setColor(cc.c3b(100, 100, 100))
		self.bCardGray[i] = true
	end
	--将牌补满(ui与值的对齐方式)
	local nCount = 0
	local num = math.mod(cbCardCount, 3)
	if num == 2 then
		nCount = cbCardCount
	elseif num == 1 then
		nCount = cbCardCount + 1
	else
		assert(false)
	end
	--再恢复可打出的牌
	for i = 1, #cbOutCardData do
		for j = 1, nCount do
			if cbOutCardData[i] == self.cbCardData[j] then
				--assert(cbOutCardData[i] ~= GameLogic.MAGIC_DATA, "The magic card can't out!")
				local nTag = nCount - j + 1
				local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(nTag)
				card:setColor(cc.c3b(255, 255, 255))
				self.bCardGray[nTag] = false
				break
			end
		end
	end
end

function CardLayer:promptListeningCard(cardData)
	--assert(#self.cbListenList > 0)

	if nil == cardData then
		assert(self.currentOutCard > 0)
		cardData = self.currentOutCard
	end

	for i = 1, #self.cbListenList do
		if self.cbListenList[i].cbOutCard == cardData then
			self._scene:setListeningCard(self.cbListenList[i].cbListenCard)
			break
		end
	end
end

function CardLayer:startListenCard()
	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		if i == 1 then
			card:setColor(cc.c3b(255, 255, 255))
			self.bCardGray[i] = false
		else
			card:setColor(cc.c3b(100, 100, 100))
			self.bCardGray[i] = true
		end
	end
end

function CardLayer:promptListenOutCard(cbPromptOutData)
	--还原
	local cbCardCount = #self.cbCardData
	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		card:getChildByTag(CardLayer.TAG_LISTEN_FLAG):setVisible(false)
	end
	if cbPromptOutData == nil then
		return 
	end
	--校验
	assert(type(cbPromptOutData) == "table")
	-- assert(math.mod(cbCardCount - 2, 3) == 0, "You can't out card now!")
	for i = 1, #cbPromptOutData do
		if cbPromptOutData[i] ~= GameLogic.MAGIC_DATA then
			for j = 1, cbCardCount do
				if cbPromptOutData[i] == self.cbCardData[j] then
					local nTag = cbCardCount - j + 1
					local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(nTag)
					if card then
						card:getChildByTag(CardLayer.TAG_LISTEN_FLAG):setVisible(false)
					end
				end
			end
		end
	end
end

function CardLayer:setTableCardByHeapInfo(viewId, cbHeapCardInfo, wViewHead, wViewTail)
	local nViewMinTag = (cmd.GAME_PLAYER - viewId)*28 + 1
	local nViewMaxTag = (cmd.GAME_PLAYER - viewId)*28 + 28
	--从右数隐藏几张牌
	local nTagStart1 = nViewMinTag
	local nTagEnd1 = nViewMinTag + cbHeapCardInfo[1] - 1
	for i = nTagStart1, nTagEnd1 do
		--self.nodeTableCard:getChildByTag(i):setVisible(false)
	end
	--从左数隐藏几张牌
	local nTagStart2 = nViewMaxTag - cbHeapCardInfo[2] + 1
	local nTagEnd2 = nViewMaxTag
	for i = nTagStart2, nTagEnd2 do
		--self.nodeTableCard:getChildByTag(i):setVisible(false)
	end

	if viewId == wViewHead then
		self.currentTag = (cmd.GAME_PLAYER - viewId)*28 + cbHeapCardInfo[1] + 1
	elseif viewId == wViewTail then
		self.nTableTailCardTag = (cmd.GAME_PLAYER - viewId)*28 + (28 - cbHeapCardInfo[2])
	end
	--牌堆总共剩余多少牌
	-- self.nRemainCardNum = self.nRemainCardNum - cbHeapCardInfo[1] - cbHeapCardInfo[2]
	self._scene:setRemainCardNum(self.nRemainCardNum)
end

--cbCardData 具体点数
--card 麻将对象
function CardLayer:touchSendOutCard(cbCardData, card)
	if not self.bSendOutCardEnabled then
		return false
	end

	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._mjSkin
	
	self:stopSameCard()
	self.currentOutCard = cbCardData
	self._scene:setListeningCard(nil)
	--发送消息
	local result = self._scene._scene:sendOutCard(cbCardData)

	if card and result then
		local nValue = math.mod(cbCardData, 16)
		local nColor = math.floor(cbCardData/16)
		local strFile = skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png"

		local strFilePP = skinPath .. "/pai_bg_dian.png"

		--底板
		local runScene = cc.Director:getInstance():getRunningScene()
		local worldPosition = card:getParent():convertToWorldSpace(cc.p(card:getPositionX(), card:getPositionY()))
		local nodePosition = self._scene:convertToNodeSpace(worldPosition)


		local moveCard = cc.Sprite:create(strFilePP)
		moveCard:setPosition(nodePosition)
		moveCard:setName("MY_VIEWID_MOVE_CARD")
		self._scene:addChild(moveCard, 15)

		local spPlatecard = cc.Sprite:create(strFile)
		moveCard:addChild(spPlatecard)

		local plateX = 60
		local plateY = 84

		if self._mjSkin == 2 then
			plateY = 88
		elseif self._mjSkin == 3 then
			plateX = 61
			plateY = 88
		end

		spPlatecard:setPosition(moveCard:getContentSize().width/2, moveCard:getContentSize().height/2 + 18)

		--牌移动到桌面
		moveCard:runAction(
			cc.Sequence:create(
				cc.MoveTo:create(0.1, posPlates[cmd.MY_VIEWID]),
				cc.CallFunc:create(function()
					
				end)
				))
	end


	return result
end

function CardLayer:outCardAuto()
	local nCount = #self.cbCardData
	if math.mod(nCount - 2, 3) ~= 0 then
		return false
	end
	for i = nCount, 1, -1 do
		local nTag = nCount - i + 1
		local bOk = not self.bCardGray[nTag]
		if self.cbCardData[i] ~= GameLogic.MAGIC_DATA and bOk then
			self:touchSendOutCard(self.cbCardData[i])
			break
		end
	end

	return true
end

function CardLayer:isMagicData(temp)
	return GameLogic.MAGIC_DATA == temp
end

--获取能吃此牌的手牌
--return a table
function CardLayer:getCanChiCombin(currCard)
	local ret = {}

	--如果是右吃
	local isRightEat = self:isInTable(currCard-1, self.cbCardData)
	if isRightEat == true and self:isMagicData(currCard-1) == false then
		isRightEat = self:isInTable(currCard-2, self.cbCardData)
		if isRightEat == true and self:isMagicData(currCard-2) == false then
			table.insert(ret, {GameLogic.WIK_RIGHT, currCard, currCard - 2, currCard - 1})
		end
	end

	--如果是中吃
	local isCenterEat = self:isInTable(currCard-1, self.cbCardData)
	if isCenterEat == true and self:isMagicData(currCard-1) == false then
		isCenterEat = self:isInTable(currCard+1, self.cbCardData)
		if isCenterEat == true and self:isMagicData(currCard+1) == false then
			table.insert(ret, {GameLogic.WIK_CENTER, currCard, currCard - 1, currCard + 1})
		end
	end

	--如果是左吃
	local isLeftEat = self:isInTable(currCard+1, self.cbCardData)
	if isLeftEat == true and self:isMagicData(currCard+1) == false then
		isLeftEat = self:isInTable(currCard+2, self.cbCardData)
		if isLeftEat == true and self:isMagicData(currCard+2) == false then
			table.insert(ret, {GameLogic.WIK_LEFT, currCard, currCard + 1, currCard + 2})
		end
	end

	return ret
end

--获取能吃此牌的手牌
--return a table
function CardLayer:getCanGangCombin(data, chairId, isOtherOp)
	local ret = {}
	local cbCardCount = #self.cbCardData

	--储存每张牌对应几张
	local cbCardIndex = GameLogic.DataToCardIndex(self.cbCardData)

	--计算出皮子牌
	local piziCardData = GameLogic.getPiZiCardData()

	--如果判断摸起来的牌，能不能和手牌凑成4张
	if data and data > 0 then
		local index = GameLogic.SwitchToCardIndex(data)
		--皮子牌3张可以暗杠，2张可以明杠--摸起来的这张牌是皮子牌
		if (cbCardIndex[index] == 2 and data == piziCardData) or (cbCardIndex[index] == 3 and isOtherOp and data ~= piziCardData ) then
			if data ~= GameLogic.MAGIC_DATA then
				table.insert(ret, {GameLogic.WIK_GANG, data, data, data, data})
			end
		end
	end

	if isOtherOp then
		return ret
	end

	--手牌有4张 暗杠
	for i = 1, GameLogic.NORMAL_INDEX_MAX do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		--普通牌四张可以杠，皮子牌三张可以暗杠
		if cbCardIndex[i] == 4 or (piziCardData == dataTemp and cbCardIndex[i] == 3) then
			if dataTemp ~= GameLogic.MAGIC_DATA then
				table.insert(ret, {GameLogic.WIK_GANG, dataTemp, dataTemp, dataTemp, dataTemp})
			end
			
		end
	end

	--特殊牌红中等
	for i = 28, 34 do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		if cbCardIndex[i] == 4 then
			if dataTemp ~= GameLogic.MAGIC_DATA then
				table.insert(ret, {GameLogic.WIK_GANG, dataTemp, dataTemp, dataTemp, dataTemp})
			end
		end
	end

	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId]

	--碰了之后，手牌里面还有能杠的
	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId]

	local tempUserData = {}
	
	local tempOne = {}
	--按类型分开
	for i, v in ipairs(userData) do
		if i == 1 then
			tempOne = {}
		end
		if v == -1 or v == -2 then
			table.insert(tempUserData, tempOne)
			tempOne = {}
		else
			table.insert(tempOne, v)
		end
	end

	for k,v in pairs(tempUserData) do
		--碰过的牌
		if v[1] and v[1] == v[2] and v[1] == v[3] and v[4] == nil then
			if self:isInTable(v[1], self.cbCardData) then
				table.insert(ret, {GameLogic.WIK_GANG, v[1], v[1], v[1], v[1]})
			end
		end
	end

	--摸到已经碰了的牌，杠
	return ret
end

--检查手里的杠
function CardLayer:getGangCard(data, chairId)
	local cbCardCount = #self.cbCardData

	--储存每张牌对应几张
	local cbCardIndex = GameLogic.DataToCardIndex(self.cbCardData)

	--如果判断摸起来的牌，能不能和手牌凑成4张
	if data and data > 0 then
		local index = GameLogic.SwitchToCardIndex(data)
		if cbCardIndex[index] == 3 then
			return data
		end
	end

	--手牌有4张 暗杠
	for i = 1, GameLogic.NORMAL_INDEX_MAX do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		if cbCardIndex[i] == 4 then
			data = dataTemp
			return data
		end
	end

	--特殊牌红中等
	for i = 28, 34 do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		if cbCardIndex[i] == 4 then
			data = dataTemp
			return data
		end
	end

	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId+1]

	--碰了之后，手牌里面还有能杠的
	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId+1]

	for k,v in pairs(userData) do
		if self:isInTable(v, self.cbCardData) then
			data = v
			return data
		end
	end

	--摸到已经碰了的牌，杠
	return data
end

--用户必须点胡牌
function CardLayer:isUserMustWin()
	-- local cbCardCount = #self.cbCardData
	-- local cbCardDataTemp = clone(self.cbCardData)
	-- GameLogic.SortCardList(cbCardDataTemp)
	-- --有普通牌
	-- local cbNormalCardTypeCount = 0
	-- local cbNormalCardTemp = 0
	-- for i = 1, cbCardCount do
	-- 	if cbCardDataTemp[i] == GameLogic.MAGIC_DATA then
	-- 		break
	-- 	end
	-- 	if cbNormalCardTemp ~= cbCardDataTemp[i] then
	-- 		cbNormalCardTypeCount = cbNormalCardTypeCount + 1
	-- 	end
	-- 	cbNormalCardTemp = cbCardDataTemp[i]
	-- end
	if #self.cbCardData == 2 then
		if self.cbCardData[1] == self.cbCardData[2] and 
			self.cbCardData[2] == GameLogic.MAGIC_DATA then
			return true
		end
	end

	return false
end
--用户可以碰
function CardLayer:isUserCanBump()
	if #self.cbCardData == 4 then
		if self.cbCardData[1] == self.cbCardData[2] and 
			self.cbCardData[3] == self.cbCardData[4] and 
			self.cbCardData[4] == GameLogic.MAGIC_DATA then
			return false
		end
	end

	return true
end

return CardLayer