local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC.."ExternalFun")
local CardLayer = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.layer.CardLayer")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.GameLogic")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local ClipText = appdf.req("client.src.external.ClipText")

local ResultLayer = class("ResultLayer", function(scene)
	local resultLayer = cc.CSLoader:createNode(cmd.RES_PATH.."gameResult/GameResultLayer.csb")
	return resultLayer
end)

ResultLayer.TAG_TEXT_NICKNAME				= 2
ResultLayer.TAG_HEAD 						= 4
ResultLayer.TAG_NODE_CARD					= 5

ResultLayer.WINNER_ORDER					= 1

local posBanker = {cc.p(75, 549), cc.p(75, 441), cc.p(75, 331), cc.p(75, 228)}
local NODE_USER_POS = {cc.p(112, 521), cc.p(112, 414), cc.p(112, 307), cc.p(112, 200)}

function ResultLayer:onInitData()
	--body
	self.winnerIndex = nil
	self.bShield = false
end

function ResultLayer:onResetData()
	--body
	self.winnerIndex = nil
	self.bShield = false
	for i = 1, cmd.GAME_PLAYER do
		self.nodeUser[i]:getChildByName("CardLayout"):removeAllChildren()
	end
end

function ResultLayer:ctor(scene)
	self._scene = scene
	self:onInitData()
	ExternalFun.registerTouchEvent(self, true)

	--分享按钮
	local btRecodeShow = self:getChildByName("Button_recodeShow")
	btRecodeShow:setVisible(true)
	btRecodeShow:addClickEventListener(function(ref)
		self:recodeShow()
	end)

	--继续按钮
	local btContinue = self:getChildByName("Button_continue")
	btContinue:addClickEventListener(function(ref)
		self:continueBtnCallback(ref)
	end)

	--具体数据
	self.nodeUser = {}
	for i = 1, cmd.GAME_PLAYER do
		self.nodeUser[i] = self:getChildByName("FileNode_" .. i)
		self.nodeUser[i]:getChildByName("HeadKuang"):setLocalZOrder(1)
	end
	--庄标志
	self.spBanker = self:getChildByName("sp_banker")

	self:getChildByName("Text_ResultDJS"):setVisible(false)
end

function ResultLayer:continueBtnCallback(ref)
	if self._scene._scene._isReplay or self._scene._scene._gameFrame == nil then
		self._scene._scene:closeReplay()
	else
		self:hideLayer()
		self._scene:onButtonClickedEvent(self._scene.BT_START)
	end
	
end

function ResultLayer:onTouchBegan(touch, event)
	return false
end

function ResultLayer:onClockUpdate(time)
	self:getChildByName("Text_ResultDJS"):setString(time)
	self:getChildByName("Text_ResultDJS"):setVisible(true)
end

function ResultLayer:showLayer(resultList, cbAwardCard, cbRemainCard, wBankerChairId, cbHuCard, wProvideUser, wWinerUser, meCharid, rule, ruleEnd)
	assert(type(resultList) == "table"  and type(cbRemainCard) == "table") --and type(cbAwardCard) == "table"
	cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
	self:getChildByName("Text_ResultDJS"):setVisible(false)

	local txtDate = self:getChildByName("TimeTxt")
	local date=os.date("%Y.%m.%d %H:%M")
	txtDate:setString("日期: " .. tostring(date))

	local hasQiDuiRule = false
	local isFirstConat = true
	local ruleStr = "创建房间的玩法: "
	for k,v in pairs(rule) do
		if v == true and k == 1 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "天地胡"
		end

		if v == true and k == 2 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "死飘"
		end

		if v == true and k == 3 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "穷胡翻倍"
		end

		if v == true and k == 4 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "封顶150"
		end

		if v == true and k == 5 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "三清"
		end 

		if v == true and k == 6 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "七对"
			hasQiDuiRule = true
		end

		if v == true and k == 7 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "清一色"
		end

		if v == true and k == 8 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "封顶200"
		end
	end

	if ruleStr == "创建房间的玩法: " then
		ruleStr = ruleStr .. "没有特殊玩法"
	end

	-- 闭门，杠开花，杠流泪，三清，四清，天胡，地胡，150分封顶，穷胡翻倍
	isFirstConat = true
	local ruleEndStr = "胡牌加翻的算法: "
	for k,v in pairs(ruleEnd) do
		if v == true and k == 1 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "杠开花"
		end

		if v == true and k == 2 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "杠流泪"
		end

		if v == true and k == 3 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "三清"
		end

		if v == true and k == 4 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "四清"
		end 

		if v == true and k == 5 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "天地胡"
		end 

		if v == true and k == 6 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "封顶150"
		end 

		if v == true and k == 7 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "穷胡翻倍"
		end 

		if v == true and k == 8 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "清一色"
		end

		if v == true and k == 9 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "封顶200"
		end 

		if v == true and k == 10 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "海底捞"
		end 
	end

	if ruleEndStr == "胡牌加翻的算法: " then
		ruleEndStr = ruleEndStr .. "没有加翻"
	end

	local spDisHuipaiBg = self:getChildByName("sp_dis_huipai_bg")

	local spDisHuipai = self:getChildByName("sp_dis_huipai")
	if GameLogic.MAGIC_DATA == -1 or GameLogic.MAGIC_INDEX == 34 then
		spDisHuipaiBg:setVisible(false)
		spDisHuipai:setVisible(false)
	else
		spDisHuipaiBg:setVisible(true)
		spDisHuipai:setVisible(true)

		local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(GameLogic.MAGIC_DATA))

	    spDisHuipai:setTexture(cmd.RES_PATH .. "game/mahjongCard_" .. self._scene._cardLayer._mjSkin .. "/card_color_" .. nColor .. "_num_" .. nValue .. ".png")
	end

	local scaleFirst = 0.8
	local scaleOther = 0.45
	local width = 86
	local height = 130

	--胡牌的玩家优先
	--找出胡牌的玩家
	local hasWinner = false
	for i = 1, #resultList do
		if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
			self.winnerIndex = i

			local temp = resultList[1]
			resultList[1] = resultList[i]
			resultList[i] = temp
			hasWinner = true
			break
		end
	end

	--如果没有胡牌的，那就显示自己的
	if not hasWinner then
		for i = 1, #resultList do
			if meCharid == (i - 1) then
				local temp = resultList[1]
				resultList[1] = resultList[meCharid+1]
				resultList[meCharid+1] = temp
				break
			end
		end
	end

	local nBankerOrder = 1
	for i = 1, cmd.GAME_PLAYER do
		local scaleFact = scaleFirst
		if i == 1 then
			scaleFact = scaleFirst
		else
			scaleFact = scaleOther
		end
		width = 86 * scaleFact
		height = 130 * scaleFact


		local order = self:switchToOrder(i)
		if i <= #resultList then
			self.nodeUser[i]:setVisible(true)

			local descriptionTxt = self.nodeUser[i]:getChildByName("DescriptionTxt")
			descriptionTxt:setString("")
			local descriptionStr = ""

			local gameIDTxt = self.nodeUser[i]:getChildByName("GameIDTxt")
			gameIDTxt:setString(resultList[i].userItem.dwGameID)

			--是否为庄家,不是庄家才显示跟庄数据
			if wBankerChairId ~= resultList[i].userItem.wChairID then
				local followCound = 0
				for a, b in pairs(resultList[i].bFollowBanker) do
					if b == true then
						followCound = followCound + 1
					end
				end
				if followCound > 0 then
					descriptionStr = descriptionStr .. "跟庄x" .. followCound .. "  "
				end
			end

			if resultList[i].wAllMagicUser == resultList[i].userItem.wChairID then
				descriptionStr = descriptionStr .. "血会" .. "  "
			end

			local mingGangCount = 0
			local fangGangCount = 0
			local anGangCount = 0
			local dianGangCount = 0

			for a, b in pairs(resultList[i].WeaveItemArray) do
				if b.cbParam == GameLogic.WIK_MING_GANG then --明杠，也即为补杠
					mingGangCount = mingGangCount + 1
				elseif b.cbParam == GameLogic.WIK_FANG_GANG then --放杠，也即为点杠
					fangGangCount = fangGangCount + 1
				elseif b.cbParam == GameLogic.WIK_AN_GANG then --暗杠
					anGangCount = anGangCount + 1
				end
			end

			for l = 1, cmd.GAME_PLAYER do
				if l <= #resultList then
					for a, b in pairs(resultList[l].WeaveItemArray) do
						if b.cbParam == GameLogic.WIK_FANG_GANG then --放杠，也即为点杠,如果提供的人是自己的
							if b.wProvideUser == resultList[i].userItem.wChairID then
								dianGangCount = dianGangCount + 1
							end
						end
					end
				end
			end

			if mingGangCount > 0 then
				descriptionStr = descriptionStr .. "明杠x" .. mingGangCount .. "  "
			end

			if fangGangCount > 0 then
				descriptionStr = descriptionStr .. "接杠x" .. fangGangCount .. "  "	
			end

			if anGangCount > 0 then
				descriptionStr = descriptionStr .. "暗杠x" .. anGangCount .. "  "	
			end

			if dianGangCount > 0 then
				descriptionStr = descriptionStr .. "点杠x" .. dianGangCount .. "  "	
			end

			--是否闭门
			local isBiMen = resultList[i].cbWeaveItemCount == anGangCount
			if isBiMen then
				descriptionStr = descriptionStr .. "闭门" .. "  "
			end

			if wBankerChairId == resultList[i].userItem.wChairID and wWinerUser == yl.INVALID_CHAIR then
				descriptionStr = descriptionStr .. "黄庄-6" .. "  "
			end

			--头像
			local head = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_HEAD)
			if head then
				head:updateHead(resultList[i].userItem)
			else
				if self._scene._scene._isReplay then
					head = PopupInfoHead:createReplayHead(resultList[i].userItem.dwUserID, nil, 96)
				else
					head = PopupInfoHead:createNormal(resultList[i].userItem, 96)
				end
				head:setPosition(48, 48)			--初始位置
				head:enableHeadFrame(false)
				head:enableInfoPop(false)
				head:setTag(ResultLayer.TAG_HEAD)
				self.nodeUser[i]:getChildByName("HeadIcon"):addChild(head)
			end

			
			local scoreTxt = self.nodeUser[i]:getChildByName("ScoreTxt")
			--输赢积分
			local preStr = ""
			if resultList[i].lScore > 0 then
				preStr = "+"
			end
			scoreTxt:setString(preStr .. resultList[i].lScore)

			local gangTxt = self.nodeUser[i]:getChildByName("GangTxt")
			--杠输赢积分
			local preGangStr = ""
			if resultList[i].gScore > 0 then
				preGangStr = "+"
			end
			gangTxt:setString("杠:" .. preGangStr .. resultList[i].gScore)

			--昵称
			local textNickname = self.nodeUser[i]:getChildByName("NickNameTxt")
			textNickname:setString(resultList[i].userItem.szNickName)

			local skinPath = "mahjongCard_" .. self._scene._cardLayer._mjSkin

			local fX = width/2
			local fY = height/2
			local nodeUserCard = self.nodeUser[i]:getChildByName("CardLayout")
			--吃，碰，杠的牌
			for k,v in pairs(resultList[i].WeaveItemArray) do
				local isAllZero = true
				for a,b in pairs(v.cbCardData[1]) do
					if b > 0 then
						isAllZero = false
						local card = nil
						if v.cbParam == GameLogic.WIK_AN_GANG then
							local jieXian = 3
							local totalCount = 4
							--如果是皮子杠
							if b == GameLogic.getPiZiCardData() then
								jieXian = 2
								totalCount = 3
							end

							if a <= jieXian then
								card = cc.Sprite:create(cmd.RES_PATH.."game/" .. skinPath .. "/pai_bg_bei_kei.png")
									:move(fX, fY)
									:addTo(nodeUserCard)
							elseif a == totalCount then
								fX = fX - width
								if totalCount == 3 then
									fxx = fX - width/2
								else
									fxx = fX - width
								end
								
								card = cc.Sprite:create(cmd.RES_PATH.."game/" .. skinPath .. "/pai_bg_dian.png")
									:move(fxx, fY + 16)
									:addTo(nodeUserCard)

								local cardSize = card:getContentSize()
								local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(b))
								cc.Sprite:create("game/" .. skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png")
									:move(cardSize.width/2, cardSize.height/2 + 16)
									:addTo(card)
							end
						else
							card = cc.Sprite:create(cmd.RES_PATH.."game/" .. skinPath .. "/pai_bg_dian.png")
								:move(fX, fY)
								:addTo(nodeUserCard)

							local cardSize = card:getContentSize()
							local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(b))
							local cardFont = cc.Sprite:create("game/" .. skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png")
							cardFont:move(cardSize.width/2, cardSize.height/2 + 16)
							cardFont:addTo(card)
						end
						card:setScale(scaleFact)
						
						fX = fX + width
					end
				end
				if not isAllZero then
					fX = fX + 10
				end
			end

			local huipaiFlagDx = 0
			local huipaiFlagDy = 2

			for j = 1, #resultList[i].cbCardData do  											 	--剩余手牌
				--牌底
				local card = cc.Sprite:create(cmd.RES_PATH.."game/" .. skinPath .. "/pai_bg_dian.png")
					:move(fX, fY)
					:addTo(nodeUserCard)

				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(resultList[i].cbCardData[j]))
				local cardSize = card:getContentSize()
				local cardFont = cc.Sprite:create("game/" .. skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png")
				cardFont:move(cardSize.width/2, cardSize.height/2 + 16)
				cardFont:addTo(card)

				card:removeChildByTag(666)

				if resultList[i].cbCardData[j] == GameLogic.MAGIC_DATA then
					local size = card:getContentSize()
					local sHuiPaiFlag = cc.Sprite:create(cmd.RES_PATH .. "game/huipaiflag.png")
					    :move(size.width/2 + huipaiFlagDx, size.height/2+huipaiFlagDy)
					    :setTag(666)
					    :addTo(card)
				end
				card:setScale(scaleFact)
				fX = fX + width
			end

			local cNode = self.nodeUser[i]:getChildByName("PROVIDEFLAG")
			if cNode then
				cNode:removeFromParent()
			end

			--先判断是否有赢家
			if self.winnerIndex then
				--如果当前遍历的玩家是提供者
				if resultList[i].userItem.wChairID and resultList[i].userItem.wChairID == wProvideUser then
					local isFangpao = false
					--自摸
					if wProvideUser == wWinerUser then
						hupaiFlagPath = cmd.RES_PATH .. "pics/sp_ziMo.png"
						descriptionStr = descriptionStr .. "自摸  "
					else --放炮
						hupaiFlagPath = cmd.RES_PATH .. "pics/r_fangpao.png"
						isFangpao = true
					end

					local hupaiSp = cc.Sprite:create(hupaiFlagPath)
						:setName("PROVIDEFLAG")
						:setVisible(false)
						:addTo(self.nodeUser[i], 20)
					hupaiSp:setPosition(30, 30)
				end
			end

			--胡的那张牌
			if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
				fX = fX + 20
				--牌底
				--local rectX = CardLayer:switchToCardRectX(cbHuCard)

				local huCard = display.newSprite(cmd.RES_PATH.."game/" .. skinPath .. "/pai_bg_dian.png")
					:move(fX, fY)
					:addTo(nodeUserCard)
				--字体
				local huCardSize = huCard:getContentSize()
				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(cbHuCard))
				local cardFont = cc.Sprite:create("game/" .. skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png")
				cardFont:move(huCardSize.width/2, huCardSize.height/2 + 16)
				cardFont:addTo(huCard)

				huCard:setScale(scaleFact)

				if cbHuCard == GameLogic.MAGIC_DATA then
					local size = huCard:getContentSize()
					local sHuiPaiFlag = display.newSprite("#r_s_up_huipai.png")
					    :move(size.width/2 + huipaiFlagDx, size.height/2+huipaiFlagDy)
					    :setTag(666)
					    :addTo(huCard)
				end

				local hupaiFlagPath = ""

				local htpType = GameLogic.getHPType(resultList[i].dwChiHuRight)
				local typePath = ""
				local huDesc = ""
				if htpType == GameLogic.HTP_QIDUI_HU then
					typePath = "r_hppinghu.png"
					if hasQiDuiRule then
						typePath = "r_hpqidui.png"
						huDesc = "七小对  "
					end
				elseif htpType == GameLogic.HTP_HZ_HU then
					typePath = "r_hplou.png"
					huDesc = "娄  "
				elseif htpType == GameLogic.HTP_PIAO_HU then
					typePath = "r_hppiaohu.png"
					huDesc = "飘  "
				elseif htpType == GameLogic.HTP_DAN_DIAN then
					typePath = "r_hpdanza.png"
					huDesc = "单砸  "
				elseif htpType == GameLogic.HTP_JIA_HU then
					typePath = "r_hpjiahu.png"
					huDesc = "夹胡  "
				elseif htpType == GameLogic.HTP_PING_HU then
					typePath = "r_hppinghu.png"
					
				elseif htpType == GameLogic.HTP_QING_YI_SE_HU then
					typePath = "r_hpqingyise.png"
					huDesc = "清一色  "
				end
				descriptionStr = descriptionStr .. huDesc
				-- if typePath ~= "" then
				-- 	spHupaiType:setSpriteFrame(typePath)
				-- 	spHupaiType:setVisible(true)
				-- end
			end
			descriptionTxt:setString(descriptionStr)
			--麻将
			fX = 909-(width+3)*(self._scene._scene.cbMaCount/2) 
				
			--庄家
			if wBankerChairId == resultList[i].userItem.wChairID then
				nBankerOrder = i
			end
		else
			self.nodeUser[i]:setVisible(false)
		end
	end

	--显示创建房间时的详细信息
	local textYXGZ = self:getChildByName("Text_YXGZ")
	textYXGZ:setString(ruleStr)

	local textYXGZ_1 = self:getChildByName("Text_YXGZ_1")
	textYXGZ_1:setString(ruleEndStr)

	--庄家
	if nBankerOrder == 1 then
		self.spBanker:setPosition(cc.p(50, 546))
	elseif nBankerOrder == 2 then
		self.spBanker:setPosition(cc.p(50, 390))
	elseif nBankerOrder == 3 then
		self.spBanker:setPosition(cc.p(50, 280))
	elseif nBankerOrder == 4 then
		self.spBanker:setPosition(cc.p(50, 180))
	end

	self.bShield = true
	self:setVisible(true)
	self:setLocalZOrder(yl.MAX_INT)
end

function ResultLayer:hideLayer()
	if not self:isVisible() then
		return
	end
	self:onResetData()
	self:setVisible(false)
	self._scene.btStart:setVisible(true)
end

--1~4转换到1~4
function ResultLayer:switchToOrder(index)
	assert(index >=1 and index <= cmd.GAME_PLAYER)
	if self.winnerIndex == nil then
		return index
	end
	local nDifference = ResultLayer.WINNER_ORDER - self.winnerIndex - 1
	local order = math.mod(index + nDifference, cmd.GAME_PLAYER) + 1
	return order
end

function ResultLayer:recodeShow()
	local function sharecall( isok )
        if type(isok) == "string" and isok == "true" then
            showToast(self, "分享成功", 2)
        end
        yl.removeErWeiMa()
    end
    local url = GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
    -- 截图分享
    local framesize = cc.Director:getInstance():getOpenGLView():getFrameSize()
    local area = cc.rect(0, 0, framesize.width, framesize.height)
    local imagename = "grade_share.jpg"
    ExternalFun.popupTouchFilter(0, false)
    yl.addErWeiMa()
    captureScreenWithArea(area, imagename, function(ok, savepath)
        ExternalFun.dismissTouchFilter()
        if ok then
            MultiPlatform:getInstance():shareToTarget(yl.ThirdParty.WECHAT, sharecall, "我的约战房战绩", "分享我的约战房战绩", url, savepath, "true", "", "", GlobalUserItem.dwUserID)         
        end
    end)
end

return ResultLayer