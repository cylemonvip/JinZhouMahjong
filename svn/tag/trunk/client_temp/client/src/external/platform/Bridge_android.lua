--
-- Author: zhong
-- Date: 2016-07-29 17:45:30
--
local Bridge_android = {}
local BRIDGE_CLASS = "org/cocos2dx/lua/AppActivity"
local luaj = require "cocos.cocos2d.luaj"

--获取设备id
function Bridge_android.getMachineId()
    local sigs = "()Ljava/lang/String;"    
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"getUUID",{},sigs)
    if not ok then
        print("luaj error:" .. ret)
        return "A501164B366ECFC9E249163873094D50"
    else
        print("The ret is:" .. ret)
        return md5(ret)
    end
end

--获取设备ip
function Bridge_android.getClientIpAdress()
    local sigs = "()Ljava/lang/String;"
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"getHostAdress",{},sigs)
    if not ok then
        print("luaj error:" .. ret)
        return "192.168.1.1"
    else
        print("The ret is:" .. ret)
        return ret
    end
end

--获取外部存储可写文档目录
function Bridge_android.getExtralDocPath()
    local sigs = "()Ljava/lang/String;"
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"getSDCardDocPath",{},sigs)
    if not ok then
        print("luaj error:" .. ret)
        return device.writablePath
    else
        print("The ret is:" .. ret)
        return ret
    end
end

--选择图片
function Bridge_android.triggerPickImg(callback, needClip)
    needClip = needClip or false
    local args = { callback, needClip }
    if nil == callback or type(callback) ~= "function" then
    	print("user default callback fun")

    	local function callbackLua(param)
	        if type(param) == "string" then
	        	print(param)
	        end        
	    end
    	args = { callbackLua, needClip }
    end    
    
    local sigs = "(IZ)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"pickImg",args,sigs)
    if not ok then
        print("luaj error:" .. ret)       
    end
end

--配置支付、登陆相关
function Bridge_android.thirdPartyConfig(thirdparty, configTab)
    print("配置支付、登陆相关 开始")
    for k,v in pairs(configTab) do
        print(k,v)
    end
    print("======================================")

    local args = {thirdparty, cjson.encode(configTab)}
    local sigs = "(ILjava/lang/String;)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"thirdPartyConfig",args,sigs)
    if not ok then
        print("luaj error:" .. ret)        
    end
end

function Bridge_android.configSocial(socialTab)
    local title = socialTab.title
    local content = socialTab.content
    local url = socialTab.url

    local args = {title, content, url}
    local sigs = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"socialShareConfig",args,sigs)
    if not ok then
        print("luaj error:" .. ret)        
    end
end

--第三方登陆
function Bridge_android.thirdPartyLogin(thirdparty, callback)
    local args = { thirdparty, callback }
    local sigs = "(II)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"thirdLogin",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print("第三方登陆 失败 ====================== ")
        print(msg)  
        return false, msg
    else  
        print("第三方登陆 成功 ====================== ")
        print("调起登录日志   " .. debug.traceback())
        -- callback()
        return true
    end
end

--分享
function Bridge_android.startShare(callback)
    local sigs = "(I)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"startShare",{callback},sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return true     
    end
end

--自定义分享
function Bridge_android.customShare( title, content,url, img, imgOnly, callback )
    local sigs = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"customShare",{title, content, url, img, imgOnly, callback},sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return true     
    end
end

-- 分享到指定平台
function Bridge_android.shareToTarget( target, title, content, url, img, imgOnly, callback, gameID, roomID, tag, channelID)
    local sigs = "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"shareToTarget",{target, title, content, url, img, imgOnly, callback, gameID, roomID, tag, channelID},sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return true     
    end
end

--第三方支付
function Bridge_android.thirdPartyPay(thirdparty, payparamTab, callback)
    local args = { thirdparty, cjson.encode(payparamTab), callback }
    local sigs = "(ILjava/lang/String;I)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"thirdPartyPay",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return true     
    end
end

--获取竣付通支付列表
function Bridge_android.getPayList(token, callback)
    local args = { token, callback }
    local sigs = "(Ljava/lang/String;I)V" 
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"getPayList",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return true     
    end
end

function Bridge_android.isPlatformInstalled(thirdparty)
    local args = { thirdparty }
    local sigs = "(I)Z" 
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"isPlatformInstalled",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.saveImgToSystemGallery(filepath, filename)
    local args = { filepath, filename }
    local sigs = "(Ljava/lang/String;Ljava/lang/String;)Z" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"saveImgToSystemGallery",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.checkRecordPermission()
    local args = { }
    local sigs = "()Z" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"isHaveRecordPermission",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.requestLocation( callback )
    local args = { callback }
    local sigs = "(I)V" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"requestLocation", args, sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.metersBetweenLocation( loParam )
    local args = { cjson.encode(loParam) }
    local sigs = "(Ljava/lang/String;)Ljava/lang/String;"
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"metersBetweenLocation", args, sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.requestContact( callback )
    local args = { callback }
    local sigs = "(I)V" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"requestContact", args, sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.openBrowser(url)
    local args = { url }
    local sigs = "(Ljava/lang/String;)V" 
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"openBrowser",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return true     
    end
end

function Bridge_android.copyToClipboard( msg )
    local args = { msg }
    local sigs = "(Ljava/lang/String;)Z" 
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"copyToClipboard",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return 0, msg   
    else  
        return ret     
    end
end

function Bridge_android.pastFromClipboard(callback)
    local args = { callback }
    local sigs = "(I)V"
    local ok,ret  = luaj.callStaticMethod(BRIDGE_CLASS,"pastFromClipboard",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)
        return 0, msg
    else
        return ret
    end
end



--获取别人分享的链接带的房间号
function Bridge_android.getSharedGameInfo()
    local args1 = { }
    local sigs1 = "()Ljava/lang/String;" 
    local args2 = { }
    local sigs2 = "()Ljava/lang/String;" 
    local ok1, gameID = luaj.callStaticMethod(BRIDGE_CLASS, "getSharedGameID", args1, sigs1)
    local ok2, roomID  = luaj.callStaticMethod(BRIDGE_CLASS, "getSharedRoomID", args2, sigs2)
    print("gameID = " .. tostring(gameID) .. " roomID = " .. tostring(roomID))
    if not ok1 or not ok2 then
        local msg = "luaj error:  gameID = " .. tostring(gameID) .. "  roomID = " .. tostring(roomID)
        print(msg)
        return nil, nil
    else
        if gameID == "" or roomID == "" then
            return nil, nil
        else
            return gameID, roomID
        end
    end
end

function Bridge_android.getInviterUserID()
    local args = {}
    local sigs = "()Ljava/lang/String;" 
    local ok, inviterUserID = luaj.callStaticMethod(BRIDGE_CLASS, "getInviterUserID", args, sigs)
    if not ok then
        local msg = "luaoc error:  inviterUserID = " .. tostring(inviterUserID)
        print(msg)
        return "0"
    else
        if inviterUserID then
            return inviterUserID
        else
            return "0"
        end
    end
end

function Bridge_android.getChannelID()
    local args = {}
    local sigs = "()Ljava/lang/String;" 
    local ok, channelID = luaj.callStaticMethod(BRIDGE_CLASS, "getChannelID", args, sigs)
    if not ok then
        local msg = "luaoc error:  channelID = " .. tostring(channelID)
        print(msg)
        return "0"
    else
        if channelID then
            return channelID
        else
            return "0"
        end
    end
end

function Bridge_android.onMWEvent(event, user, count) 
    return true
end

function Bridge_android.initUMeng(appid, channel)
    local args = { appid, channel }
    local sigs = "(Ljava/lang/String;Ljava/lang/String;)V" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"initUMeng",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.countEvent(name)
    local args = { name, GlobalUserItem.dwUserID }
    local sigs = "(Ljava/lang/String;Ljava/lang/String;)V" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"countEvent",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.calculateEvent(name, count)
    local args = { name, GlobalUserItem.dwUserID, count}
    local sigs = "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"calculateEvent",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.logPageViewWithName(name, isEnter)
    local enterFlag = 0
    if isEnter then
        enterFlag = 1
    end

    local args = { name, enterFlag}
    local sigs = "(Ljava/lang/String;Ljava/lang/String;)V" 
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"logPageViewWithName",args,sigs)
    if not ok then
        local msg = "luaj error:" .. ret
        print(msg)  
        return false, msg   
    else  
        return ret
    end
end

function Bridge_android.getClientPower()
    print("Bridge_android getClientPower")
    local sigs = "()Ljava/lang/String;"
    local ok,ret = luaj.callStaticMethod(BRIDGE_CLASS,"getPower",{},sigs)
    if not ok then
        print("luaj error:" .. ret)
        return 100
    else
        print("The ret is:" .. ret)
        return ret
    end
end


return Bridge_android