local GameModel = class("GameModel", function(frameEngine,scene)
        local gameLayer =  display.newLayer()
    return gameLayer
end)


--[[
    此类负责处理游戏服务器与客户端交互
    游戏数据保存放于此 于 onInitData 中初始化
    网络消息放于此
    计时器处理放于此
]]

-- local cmd = appdf.req(appdf.GAME_SRC.."gamemodel.gamename.src.models.CMD_Game")
-- local GameLogic = appdf.req(appdf.GAME_SRC.."gamemodel.gamename.src.models.GameLogic")
-- local GameViewLayer = appdf.req(appdf.GAME_SRC.."gamemodel.gamename.src.views.layer.GameViewLayer")

-- local QueryDialog = appdf.req("app.views.layer.other.QueryDialog")
local QueryDialog = appdf.req("client.src.plaza.views.layer.game.QueryDialog")
local ExternalFun =  appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")
local game_cmd = appdf.req(appdf.HEADER_SRC .. "CMD_GameServer")

-- 初始化界面
function GameModel:ctor(frameEngine,scene, param)
    ExternalFun.registerNodeEvent(self)
    self._delayLoadFlag = false
    self._scene = scene
	self._gameFrame = frameEngine
    self._isReplay = nil
    self._playerCount = 0
    if param then
        self._isReplay = param.isReplay
        self._gameFrame = nil
    end
    print("是否为回放 self._isReplay = " .. tostring(self._isReplay))
    self._replayDuration = yl.REPLAY_DURATION

    --设置搜索路径
    self._gameKind = self:getGameKind()
    self._searchPath = ""
    self:setSearchPath()

    print("==================================== CreateView 创建gameView开始", os.clock())
    self._gameView = self:CreateView()
    print("==================================== CreateView 创建gameView完成 ", os.clock())
    self:OnInitGameEngine()
    self.m_bOnGame = false
    self.m_cbGameStatus = -1
    GlobalUserItem.bAutoConnect = true
    self.cbTimeStartGame = 0

    self._isRecvHeartBeat = true
    self._recvHeartBeatTryTimes = 2

end


function GameModel:stopHeartBeat()
    print("停止心跳")
    self._gameView:stopHeartBeat()
end

function GameModel:startHeartBeat(beatSpan)
    if not self._isReplay then
        print("开启心跳 心跳间隔 = " .. tostring(beatSpan))
        self._gameView:startHeartBeat(beatSpan)
    end
end

function GameModel:gotHeartBeatTimer()
    print("收到心跳 时间：" .. os.time())
    --收到心跳表面连接成功
    self._isRecvHeartBeat = true
    self._recvHeartBeatTryTimes = 2
end

function GameModel:sendHeartBeatTimer(dt)
    --发心跳前判断上一次的心跳是否收到，若收到则继续发，则未收到则说明断开连接
    print("self._isRecvHeartBeat = " .. tostring(self._isRecvHeartBeat))
    print("self._recvHeartBeatTryTimes = " .. tostring(self._recvHeartBeatTryTimes))
    if self._isRecvHeartBeat or self._recvHeartBeatTryTimes > 0 then
        --发心跳前设置未收到心跳
        self._isRecvHeartBeat = false
        self._recvHeartBeatTryTimes = self._recvHeartBeatTryTimes - 1
        --记录当前发送的时间
        self._lastSendTime = os.time()
        print("发送心跳 时间：" .. self._lastSendTime)
        local buffer = CCmd_Data:create(0)
        local heartMain = game_cmd.SUB_GF_DETECT_SOCKET
        print("self._gameKind = " .. self._gameKind .. "   heartMain = " .. heartMain)
        self:send_MDM_GF_FRAME_Data(heartMain, buffer)
    else
        print("长时间未收到服务器消息，与服务器断开连接")
        self:stopHeartBeat()
        self._gameFrame:onCloseSocket()
        self:stopAllActions()
        self:KillGameClock()

        self:gotHeartBeatTimer()
        --重新连接服务器
        self._scene:reconnectGame()
    end
    
end


function GameModel:setSearchPath()
    if nil == self._gameKind then
        return
    end

    local entergame = self._scene:getEnterGameInfo()
    if nil ~= entergame then
        local modulestr = string.gsub(entergame._KindName, "%.", "/")
        self._searchPath = device.writablePath.."game/" .. modulestr .. "/res/"
        cc.FileUtils:getInstance():addSearchPath(self._searchPath)
    end
end

function GameModel:reSetSearchPath()
    --重置搜索路径
    local oldPaths = cc.FileUtils:getInstance():getSearchPaths()
    local newPaths = {}
    for k,v in pairs(oldPaths) do
        if tostring(v) ~= tostring(self._searchPath) then
            table.insert(newPaths, v)
        end
    end
    cc.FileUtils:getInstance():setSearchPaths(newPaths)
end

-- 房卡信息层zorder
function GameModel:priGameLayerZorder()
    if nil ~= self._gameView and nil ~= self._gameView.priGameLayerZorder then
        return self._gameView:priGameLayerZorder()
    end
    return yl.MAX_INT
end

function GameModel:onExit()
    GlobalUserItem.bAutoConnect = true
    self:reSetSearchPath()
    self:release()
end

function GameModel:onReQueryFailure(code, msg)
    self:dismissPopWait()
    if nil ~= msg and type(msg) == "string" then
        showToast(self, msg, 2)
    end
end

function GameModel:onEnterTransitionFinish()
end

--显示等待
function GameModel:showPopWait()
    if self._scene and self._scene.showPopWait then
        self._scene:showPopWait()
    end
end

--关闭等待
function GameModel:dismissPopWait()
    if self._scene and self._scene.dismissPopWait then
        self._scene:dismissPopWait()
    end
end

--初始化游戏数据
function GameModel:OnInitGameEngine()

    self._ClockFun = nil
    self._ClockID = yl.INVALID_ITEM
    self._ClockTime = 0
    self._ClockChair = yl.INVALID_CHAIR
    self._ClockViewChair = yl.INVALID_CHAIR

end

--重置框架
function GameModel:OnResetGameEngine()
    self:KillGameClock()
    self.m_bOnGame = false
end

--退出询问
function GameModel:onQueryExitGame()
    if PriRoom and true == GlobalUserItem.bPrivateRoom then
        PriRoom:getInstance():queryQuitGame(self.m_cbGameStatus)
    else
        if self._queryDialog then
           return
        end

        self._queryDialog = QueryDialog:create("您要退出游戏么？", function(ok)
            if ok == true then
                --退出防作弊
                self._gameFrame:setEnterAntiCheatRoom(false)
                
                self:onExitTable()
            end
            self._queryDialog = nil
        end):setCanTouchOutside(false)
            :addTo(self)
    end
end

function GameModel:standUpAndQuit()
    
end

-- 退出桌子
function GameModel:onExitTable()
    self:stopAllActions()
    self:KillGameClock()


    local MeItem = self:GetMeUserItem()
    if MeItem and MeItem.cbUserStatus > yl.US_FREE then
        local wait = self._gameFrame:StandUp(1)
        if wait then
            self:showPopWait()
            return
        end
    end
    self:dismissPopWait()
    self:onExitRoom()
end

function GameModel:onExitRoom()
    self._gameFrame:onCloseSocket()
    self:stopAllActions()
    self:KillGameClock()
    self:dismissPopWait()
    self._scene:onChangeShowMode(yl.SCENE_ROOMLIST)
end

-- 返回键处理
function  GameModel:onKeyBack()
    self:onQueryExitGame()
    return true
end

-- 获取自己椅子
function GameModel:GetMeChairID()
    return GameplayerManager:getInstance():getSelfChairID() or self._gameFrame:GetChairID()
end

-- 获取自己桌子
function GameModel:GetMeTableID()
   return GameplayerManager:getInstance():getTableID() or self._gameFrame:GetTableID()
end

-- 获取自己
function GameModel:GetMeUserItem()
    return self._gameFrame:GetMeUserItem()
end

--获取桌子上的人数
function GameModel:GetChairCount()
    return GameplayerManager:getInstance():getPlayerCount() or self._gameFrame:GetChairCount()
end

-- 椅子号转视图位置,注意椅子号从0~nChairCount-1,返回的视图位置从1~nChairCount --selfChair 自己的椅子 playerCount 人数
function GameModel:SwitchViewChairID(chair, selfChair, playerCount)
    local viewid = yl.INVALID_CHAIR
    local nChairCount = playerCount or self._gameFrame:GetChairCount()
    local nChairID = selfChair or self:GetMeChairID()
    if chair ~= yl.INVALID_CHAIR and chair < nChairCount then
        viewid = math.mod(chair + math.floor(nChairCount * 3/2) - nChairID, nChairCount) + 1
    end
    print("chair = " .. chair .. " nChairID = " .. nChairID .. " nChairCount = " .. nChairCount)
    return viewid
end

-- 是否合法视图id
function GameModel:IsValidViewID( viewId )
    local nChairCount = self._gameFrame:GetChairCount()
    return (viewId > 0) and (viewId <= nChairCount)
end

-- 设置计时器
function GameModel:SetGameClock(chair,id,time)
    if self._isReplay then
        return
    end
    
    if not self._ClockFun then
        local this = self
        self._ClockFun = cc.Director:getInstance():getScheduler():scheduleScriptFunc(function()
                this:OnClockUpdata()
            end, 1, false)
    end
    print("update chair = " .. chair .. "  GameModel:SetGameClock = " .. debug.traceback())
    self._ClockChair = chair
    self._ClockID = id
    self._ClockTime = time
    self._ClockViewChair = self:SwitchViewChairID(chair)
    self:OnUpdataClockView()
end

function GameModel:GetClockViewID()
    return self._ClockViewChair
end

-- 关闭计时器
function GameModel:KillGameClock(notView)
    print("KillGameClock")
    self._ClockID = yl.INVALID_ITEM
    self._ClockTime = 0
    self._ClockChair = yl.INVALID_CHAIR
    self._ClockViewChair = yl.INVALID_CHAIR
    if self._ClockFun then
        --注销时钟
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._ClockFun) 
        self._ClockFun = nil
    end
    if not notView then
        self:OnUpdataClockView()
    end
end

--计时器更新
function GameModel:OnClockUpdata()
    if  self._ClockID ~= yl.INVALID_ITEM then
        self._ClockTime = self._ClockTime - 1
        local result = self:OnEventGameClockInfo(self._ClockChair,self._ClockTime,self._ClockID)
        if result == true   or self._ClockTime < 1 then
            self:KillGameClock()
        end
    end
    self:OnUpdataClockView()
end

--更新计时器显示
function GameModel:OnUpdataClockView()
    if self._isReplay then
        return
    end
    if self._gameView and self._gameView.OnUpdataClockView then
        local rCount = nil
        if self._isReplay then
            rCount = self:GetMeChairID()
        end
        self._gameView:OnUpdataClockView(self._ClockChair, self._ClockTime, rCount or self._gameFrame:GetChairCount())
    end
end

function GameModel:clearAllHeadInfo()
    
end

--用户状态
function GameModel:onEventUserStatus(useritem,newstatus,oldstatus)
    if not self._gameView or not self._gameView.OnUpdateUser then
        return
    end
    local MyTable = self:GetMeTableID()
    local MyChair = self:GetMeChairID()

    if not MyTable or MyTable == yl.INVLAID_TABLE then
        return
    end

    dump(oldstatus, "----------------  oldstatus  -------------------")
    dump(newstatus, "----------------  newstatus  -------------------")
    dump(useritem, "----------------  useritem  -------------------")
    print("MyTable = ", MyTable)
    --旧的清除
    if oldstatus.wTableID == MyTable then
        local viewid = self:SwitchViewChairID(oldstatus.wChairID)
        if viewid and viewid ~= yl.INVALID_CHAIR then
            local hostID = PriRoom:getInstance().m_tabPriData.dwTableOwnerUserID
            print("清除旧的用户状态 oldstatus.dwUserID = " .. oldstatus.dwUserID)
            print("清除旧的用户状态 hostID = " .. hostID)
            local isHost = (hostID ~= nil and oldstatus.dwUserID == hostID)
            self._gameView:OnUpdateUser(viewid, nil, isHost)
            if PriRoom then
                PriRoom:getInstance():onEventUserState(viewid, useritem, true)
            end
        end
    end

    --更新新状态
    if newstatus.wTableID == MyTable then
        local viewid = self:SwitchViewChairID(newstatus.wChairID)
        print("newstatus.wChairID = ", newstatus.wChairID)
        print("viewid = ", viewid)
        if viewid and viewid ~= yl.INVALID_CHAIR then
            print("刷新新的用户状态")
            local hostID = PriRoom:getInstance().m_tabPriData.dwTableOwnerUserID
            print("hostID = " .. tostring(hostID))
            print("newstatus.dwUserID = " .. tostring(newstatus.dwUserID))
            local isHost = (hostID ~= nil and oldstatus.dwUserID == hostID)
            self._gameView:OnUpdateUser(viewid, useritem, isHost)
            if PriRoom then
                PriRoom:getInstance():onEventUserState(viewid, useritem, false)
            end
        else
            print("")
        end
    end

    --如果是自己的，那么更新UI
    if useritem.dwUserID == GlobalUserItem.dwUserID and newstatus.cbUserStatus > yl.US_FREE and oldstatus.cbUserStatus < yl.US_SIT then
        print("坐下后，更新方向盘")
        --自己坐下，更新方向盘 --cmd.IDI_START_GAME--
        self:SetGameClock(useritem.wChairID, 201, self.cbTimeStartGame) --第一次进入时调用
    end

end

--用户积分
function GameModel:onEventUserScore(useritem)
    if not self._gameView or not self._gameView.OnUpdateUser then
        return
    end
    local MyTable = self:GetMeTableID()
    
    if not MyTable or MyTable == yl.INVLAID_TABLE then
        return
    end 

    if  MyTable == useritem.wTableID then
        local viewid = self:SwitchViewChairID(useritem.wChairID)
        if viewid and viewid ~= yl.INVALID_CHAIR then
            self._gameView:OnUpdateUser(viewid, useritem)
        end
    end 
end

--用户进入
function GameModel:onEventUserEnter(tableid,chairid,useritem)
    print("onEventUserEnter ======== ", tableid,chairid,useritem)
    if not self._gameView or not self._gameView.OnUpdateUser then
        return
    end
    local MyTable = self:GetMeTableID()
    print("MyTable = " , MyTable)
    if not MyTable or MyTable == yl.INVLAID_TABLE then
        return
    end

    if MyTable == tableid then
        local viewid = self:SwitchViewChairID(chairid)
        print("用户进入时刷新头像 viewid = " .. tostring(viewid))
        if viewid and viewid ~= yl.INVALID_CHAIR then
            local hostID = PriRoom:getInstance().m_tabPriData.dwTableOwnerUserID
            print("用户进入时刷新头像 hostID = " .. tostring(hostID))
            print("用户进入时刷新头像 useritem.dwUserID = " .. tostring(useritem.dwUserID))
            local isHost = (hostID ~= nil and useritem.dwUserID == hostID)

            self._gameView:OnUpdateUser(viewid, useritem)

            if PriRoom then
                PriRoom:getInstance():onEventUserState(viewid, useritem, false)
            end
        end
    end
end

--发送准备
function GameModel:SendUserReady(dataBuffer)
    self._gameFrame:SendUserReady(dataBuffer)
end

--发送地理位置
function GameModel:sendLocation()
    self._gameFrame:sendLocation()
end

--发送数据
function GameModel:SendData(sub,dataBuffer)
    if self._gameFrame then
        dataBuffer:setcmdinfo(yl.MDM_GF_GAME, sub)
        return self._gameFrame:sendSocketData(dataBuffer)   
    end

    return false
end

function GameModel:send_MDM_GF_FRAME_Data(sub, dataBuffer)
    if self._gameFrame then
        dataBuffer:setcmdinfo(game_cmd.MDM_GF_FRAME, sub)
        return self._gameFrame:sendSocketData(dataBuffer)   
    end
end

--是否观看
function GameModel:IsLookonMode()
    
end

--播放音效
function GameModel:PlaySound(path)
    if GlobalUserItem.bSoundAble == true then
        AudioEngine.playEffect(path)
    end
end

--获取gamekind
function GameModel:getGameKind()
    return nil
end

-- 创建场景
function GameModel:CreateView()
    -- body
end

-- 场景消息
function GameModel:onEventGameScene(cbGameStatus,dataBuffer)

end

-- 游戏消息
function GameModel:onEventGameMessage(sub,dataBuffer)
    -- body
end

-- 计时器响应
function GameModel:OnEventGameClockInfo(chair,time,clockid)
    -- body
end

-- 私人房解散响应
-- useritem 用户数据
-- cmd_table CMD_GR_RequestReply(回复数据包)
-- 返回是否自定义处理
function GameModel:onCancellApply(useritem, cmd_table)
    print("base onCancellApply")
    return false
end

-- 私人房解散结果
-- 返回是否自定义处理
function GameModel:onCancelResult( cmd_table )
    print("base onCancelResult")
    return false
end

-- 桌子坐下人数
function GameModel:onGetSitUserNum()
    print("base get sit user number")
    return 0
end

-- 根据chairid获取玩家信息
function GameModel:getUserInfoByChairID( chairid )
    
end

return GameModel