local ClubApplyDetailPopupNode = class("ClubApplyDetailPopupNode", cc.Node)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Utils = appdf.req(appdf.CLIENT_SRC .. "utils.Utils")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")

local RES_CSB = "client/res/club/ApplyDetailPopupNode.csb"

function ClubApplyDetailPopupNode:ctor(clubPlayer)
	self._clubPlayer = clubPlayer
	self._operation = false
	self:initCSB()
end

function ClubApplyDetailPopupNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
            self:onButtonCallback(ref:getName(), ref)
        end
    end

    local mask = csbNode:getChildByName("Mask")
    mask:addTouchEventListener(btncallback)

	--Root Node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode
	rootNode:addTouchEventListener(btncallback)

	self._headImg = rootNode:getChildByName("HeadImg")
	self._nameTxt = rootNode:getChildByName("NameTxt")
	self._iDTxt = rootNode:getChildByName("IDTxt")

	self._agreeBtn = rootNode:getChildByName("AgreeBtn")
	self._agreeBtn:addTouchEventListener(btncallback)
	
	self._refuseBtn = rootNode:getChildByName("RefuseBtn")
	self._refuseBtn:addTouchEventListener(btncallback)

	self._deleteBtn = rootNode:getChildByName("DeleteBtn")
	self._deleteBtn:addTouchEventListener(btncallback)

	local closeBtn = rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btncallback)
end

function ClubApplyDetailPopupNode:setData(data)
	self._data = data
	self._nameTxt:setString(data.szNickName)
	self._iDTxt:setString(data.dwGameID)

	dump(data, "----- ClubApplyDetailPopupNode data -----")

	if self._data.bApply then
		self._agreeBtn:setVisible(false)
		self._refuseBtn:setVisible(false)
		self._deleteBtn:setVisible(true)
	else
		self._agreeBtn:setVisible(true)
		self._refuseBtn:setVisible(true)
		self._deleteBtn:setVisible(false)
	end

	local headImgSize = self._headImg:getContentSize()
	local head = PopupInfoHead:createReplayHead(data.dwGameID, data.szWeChatImgURL, 100)
	head:setPosition(cc.p(headImgSize.width/2, headImgSize.height/2))
	self._headImg:addChild(head)
end

function ClubApplyDetailPopupNode:onAgreeJoinResult(cmd_data)
	if cmd_data.iReturnCode == 0 then
		local runScene = cc.Director:getInstance():getRunningScene()
		showToast(runScene, "操作成功", 2)
		--如果是删除，则删除对应的成员显示
		if self._operation == false then
			self._clubPlayer._clubManagerNode:popVipDataByGameId(self._data.dwGameID)
		end
		self:close()
	end
end

function ClubApplyDetailPopupNode:close()
	self:removeFromParent()
end

function ClubApplyDetailPopupNode:onButtonCallback(name, ref)
	if "AgreeBtn" == name then
		--同意
		PriRoom:getInstance():setViewFrame(self)
		self._operation = true
		PriRoom:getInstance():getNetFrame():agreeApplyJoin(true, self._data.dwGameID, self._data.dwClubID)
		self._clubPlayer._tipSp:setVisible(false)
		
	elseif "RefuseBtn" == name or "DeleteBtn" == name then
		--拒绝
		PriRoom:getInstance():setViewFrame(self)
		self._operation = false
		PriRoom:getInstance():getNetFrame():agreeApplyJoin(false, self._data.dwGameID, self._data.dwClubID)
		self._clubPlayer._tipSp:setVisible(false)
	elseif "CloseBtn" == name or "Mask" == name then
		self:close()
	end
end

return ClubApplyDetailPopupNode