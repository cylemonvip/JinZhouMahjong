local ClubHzRuleNode = class("ClubHzRuleNode", cc.Node)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local cmd_private = appdf.req(appdf.CLIENT_SRC .. "privatemode.header.CMD_Private")

local RES_CSB = "client/res/club/HzMahjongRuleNode.csb"

--check box开始和结束
--圈数
local CBX_INNINGS_QS_START = 100   --不包含
local CBX_INNINGS_QS_END = 103 	--包含

--人数
local CBX_INNINGS_RS_START = 120
local CBX_INNINGS_RS_END = 122

--规则
local CBX_INNINGS_RULE_START = 140

local CLUB_MAHJONG_QUAN_CONFIG_INDEX = "CLUB_MAHJONG_QUAN_CONFIG_INDEX"
local CLUB_MAHJONG_PERSON_CONFIG_INDEX = "CLUB_MAHJONG_PERSON_CONFIG_INDEX"

function ClubHzRuleNode:ctor(clubManagerNode)
	self._clubManagerNode = clubManagerNode
	self:initCSB()
end

function ClubHzRuleNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode

	local sureRuleBtn = rootNode:getChildByName("SureRuleBtn")
	sureRuleBtn:addTouchEventListener(btncallback)

	--复选按钮
    local cbtlistener = function (sender,eventType)
        self:onSelectedEvent(sender:getTag(), sender)
    end

	--获取麻将的消耗配置
	local feeConfig = GlobalUserItem.getFeeConfigByKindId(yl.KIND_ID_HZ_MAHJONG)
	--圈数
	local quanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_MAHJONG_QUAN_CONFIG_INDEX, 2))
	--初始化圈数配置
	for i = 1, 3 do
		local qsCheckBox = rootNode:getChildByName("QuanShuCheckBox_" .. i)
		qsCheckBox:setTag(CBX_INNINGS_QS_START + i)
	    qsCheckBox:addEventListener(cbtlistener)

	    if quanIndex == i then
	    	qsCheckBox:setSelected(true)
	    else
	    	qsCheckBox:setSelected(false)
	    end

		local txt = qsCheckBox:getChildByName("Txt")
		txt:setString(feeConfig[i].dwDrawCountLimit .. "圈(房卡X" .. feeConfig[i].lFeeScore .. ")")
	end

	local renIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_MAHJONG_PERSON_CONFIG_INDEX, 1))
	--人数
	for i = 1, 2 do
		local rsCheckBox = rootNode:getChildByName("RenShuCheckBox_" .. i)
		rsCheckBox:setTag(CBX_INNINGS_RS_START + i)
	    rsCheckBox:addEventListener(cbtlistener)

	    if i == renIndex then
	    	rsCheckBox:setSelected(true)
	    else
	    	rsCheckBox:setSelected(false)
	    end
	end

	--玩法
    local rule1 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_1_SELECTED", 1))
    local rule2 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_2_SELECTED", 0))
    local rule3 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_3_SELECTED", 1))
    local rule4 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_4_SELECTED", 1))
    local rule5 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_5_SELECTED", 1))
    local rule6 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_6_SELECTED", 0))
    local rule7 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_7_SELECTED", 0))
    local rule8 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_8_SELECTED", 0))

    print("self._clubManagerNode._clubNum = ", self._clubManagerNode._clubNum)

	--规则
	for i = 1, 8 do
		local ruleCheckBox = rootNode:getChildByName("RuleCheckBox_" .. i)
		ruleCheckBox:setTag(CBX_INNINGS_RULE_START + i)
	    ruleCheckBox:addEventListener(cbtlistener)
	    if i == 1 and rule1 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 2 and rule2 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 3 and rule3 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 4 and rule4 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 5 and rule5 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 6 and rule6 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 7 and rule7 == 1 then
            ruleCheckBox:setSelected(true)
        elseif i == 8 and rule8 == 1 then
            ruleCheckBox:setSelected(true)
        else
            ruleCheckBox:setSelected(false)
        end
	end
end

--根据配置初始化显示
function ClubHzRuleNode:getCreatRoomBuffer(clubManagerNode)
    local buffer = CCmd_Data:create(188)
    buffer:setcmdinfo(cmd_private.game.MDM_GR_PERSONAL_TABLE, cmd_private.game.SUB_GR_CREATE_TABLE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    print("clubManagerNode._clubNum = ", clubManagerNode._clubNum)
    buffer:pushdword(clubManagerNode._clubNum)

    local feeIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_MAHJONG_QUAN_CONFIG_INDEX, 2))
    local feeConfig = GlobalUserItem.getFeeConfigByKindId(yl.KIND_ID_HZ_MAHJONG)
    buffer:pushdword(feeConfig[feeIndex].dwDrawCountLimit)
    buffer:pushdword(feeConfig[feeIndex].dwDrawTimeLimit)

    buffer:pushword(0)
    buffer:pushdword(0)
    buffer:pushstring("", yl.LEN_PASSWORD)

    --单个游戏规则(额外规则)
    buffer:pushbyte(1)

    local cbUserNum = 4
    local renShuIndex = tonumber(cc.UserDefault:getInstance():getStringForKey(CLUB_MAHJONG_PERSON_CONFIG_INDEX, 1))
    if renShuIndex == 1 then
    	cbUserNum = 4
    else
    	cbUserNum = 3
    end
    buffer:pushbyte(cbUserNum)         --人数必须在第2个位置


    local rule1 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_1_SELECTED", 1))
    local rule2 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_2_SELECTED", 0))
    local rule3 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_3_SELECTED", 1))
    local rule4 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_4_SELECTED", 1))
    local rule5 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_5_SELECTED", 1))
    local rule6 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_6_SELECTED", 0))
    local rule7 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_7_SELECTED", 0))
    local rule8 = tonumber(cc.UserDefault:getInstance():getStringForKey("CLUB_MAHJONG_RULE_8_SELECTED", 0))
    buffer:pushbool(rule1 == 1)
    buffer:pushbool(rule2 == 1)
    buffer:pushbool(rule3 == 1)
    buffer:pushbool(rule4 == 1)
    buffer:pushbool(rule5 == 1)
    buffer:pushbool(rule6 == 1)
    buffer:pushbool(rule7 == 1)
    buffer:pushbool(rule8 == 1)

	return buffer
end

function ClubHzRuleNode:getCreateConfig()
    --圈数
    for i = 1, 3 do
		local qsCheckBox = self._rootNode:getChildByName("QuanShuCheckBox_" .. i)
		local b = qsCheckBox:isSelected()
		if b then
			cc.UserDefault:getInstance():setStringForKey(CLUB_MAHJONG_QUAN_CONFIG_INDEX, i)
			print("圈数选择 i = " .. i)
		end
	end

    --人数
	for i = 1, 2 do
		local rsCheckBox = self._rootNode:getChildByName("RenShuCheckBox_" .. i)
		local b = rsCheckBox:isSelected()
		if b then
			cc.UserDefault:getInstance():setStringForKey(CLUB_MAHJONG_PERSON_CONFIG_INDEX, i)
			print("人数选择 i = " .. i)
		end
	end

    --获取麻将的Fee config
    local feeConfig = GlobalUserItem.getRoomOptionByKindId(yl.KIND_ID_HZ_MAHJONG)
    dump(feeConfig, "------  feeConfig  ------")

    --人数
    local personCount = 4
    if personIndex == 2 then
        personCount = 3
    else
        personCount = 4
    end

    -- local buffer = ExternalFun.create_netdata(cmd_private.login.CMD_GP_ModifyClubRule)
    local buffer = CCmd_Data:create(174)
    buffer:setcmdinfo(cmd_private.login.MDM_MB_PERSONAL_SERVICE, cmd_private.login.SUB_MB_MODIFY_CLUB_RULE)
    --管理者ID
    buffer:pushdword(GlobalUserItem.dwUserID)
    --俱乐部号
    buffer:pushdword(self._clubManagerNode._clubNum)
    --密码
    buffer:pushstring(GlobalUserItem.szPassword, yl.LEN_MD5)
    print("修改规则 GlobalUserItem.szPassword = ", GlobalUserItem.szPassword)

    buffer:pushbyte(1)
    --具体规则

    --人数
    buffer:pushbyte(personCount)

    --1.天地胡  2.会牌  3.穷胡  4.封顶150   5.三清   6.七对   7.清一色   8.封顶200

    --规则
	for i = 1, 8 do
		local ruleCheckBox = self._rootNode:getChildByName("RuleCheckBox_" .. i)
		local bSelected = ruleCheckBox:isSelected()
		local key = "CLUB_MAHJONG_RULE_" .. i .. "_SELECTED"
		if bSelected then
			cc.UserDefault:getInstance():setStringForKey(key, 1)
			print("规则选择 i = " .. i)
			buffer:pushbyte(1)
		else
			cc.UserDefault:getInstance():setStringForKey(key, 0)
			buffer:pushbyte(0)
		end
	end

	return buffer
end

function ClubHzRuleNode:onSelectedEvent(tag, sender)
	--圈数
	if tag > CBX_INNINGS_QS_START and tag <= CBX_INNINGS_QS_END then
		for i = 1, 3 do
			local qsCheckBox = self._rootNode:getChildByName("QuanShuCheckBox_" .. i)
			qsCheckBox:setSelected(false)
		end
		sender:setSelected(true)
	elseif tag > CBX_INNINGS_RS_START and tag <= CBX_INNINGS_RS_END then
		for i = 1, 2 do
			local rsCheckBox = self._rootNode:getChildByName("RenShuCheckBox_" .. i)
			rsCheckBox:setSelected(false)
		end
		sender:setSelected(true)
	else
		if tag == (CBX_INNINGS_RULE_START + 8) then
			local ruleCheckBox4 = self._rootNode:getChildByName("RuleCheckBox_4")
			if ruleCheckBox4:isSelected() then
				ruleCheckBox4:setSelected(false)
			end
			sender:setSelected(true)
		elseif tag == (CBX_INNINGS_RULE_START + 4) then
			local ruleCheckBox8 = self._rootNode:getChildByName("RuleCheckBox_8")
			if ruleCheckBox8:isSelected() then
				ruleCheckBox8:setSelected(false)
			end
			sender:setSelected(true)
		end
	end
end

function ClubHzRuleNode:onButtonCallback(name, ref)
	if "RootNode" == name or "CloseBtn" == name then

	elseif "SureRuleBtn" == name then
		local buffer = self:getCreateConfig()
		self._clubManagerNode:sendModifyRule(buffer)
	end
end

return ClubHzRuleNode