local ClubManagerNode = class("ClubManagerNode", cc.Node)


local ClubPlayerNode = require("client/src/plaza/views/layer/plaza/ClubPlayerNode")
local ClubCreateRoomNode = require("client/src/plaza/views/layer/plaza/ClubCreateRoomNode")
local ClubRoomNode = require("client/src/plaza/views/layer/plaza/ClubRoomNode")
local define_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "Define_Private")
local cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "CMD_Private")
local RecordDetailLayer = appdf.req(PriRoom.MODULE.PLAZAMODULE .. "views.RecordDetailLayer")
local Utils = appdf.req(appdf.CLIENT_SRC .. "utils.Utils")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")

local ClubHzRuleNode = require("client/src/plaza/views/layer/plaza/ClubHzRuleNode")
local ClubDPRuleNode = require("client/src/plaza/views/layer/plaza/ClubDPRuleNode")
local ClubLandRuleNode = require("client/src/plaza/views/layer/plaza/ClubLandRuleNode")

local ROOMDETAIL_NAME = "__pri_room_detail_layer_name__"

local RES_CSB = "client/res/club/ClubManagerNode.csb"

local VIP_PANEL = 1
local ONLINE_ROOM_PANEL = 2
local HISTORY_PANEL = 3
local GAME_MANAGER_PANEL = 4


function ClubManagerNode:ctor(info, scene)
	self._info = info
	--是否为自己的俱乐部
	self._isOwnClub = not self._info.bInfoKind
	self._clientScene = scene
	self._clubNum = self._info.dwClubNum
	self:initCSB()
end

function ClubManagerNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--Root Node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode

	self._roomModelNode = csbNode:getChildByName("RoomModelNode")
	

	local clubInfo = rootNode:getChildByName("ClubInfo")

	local headImage = clubInfo:getChildByName("HeadImg")
	local headImgSize = headImage:getContentSize()
	--群主头像
	local head = PopupInfoHead:createReplayHead(GlobalUserItem.dwGameID, self._info.szWeChatImgURL, 80)
	head:setPosition(cc.p(headImgSize.width/2, headImgSize.height/2))
	headImage:addChild(head)
	

	--快速开房
	local quickCreateBtn = clubInfo:getChildByName("QuickCreateBtn")
	quickCreateBtn:addTouchEventListener(btncallback)

	--游戏管理
	self._managerGameBtn = clubInfo:getChildByName("ManagerGameBtn")
	self._managerGameBtn:addTouchEventListener(btncallback)
	self._managerGameBtn:setEnabled(self._isOwnClub)

	--查询俱乐部VIp
	self._vipBtn = clubInfo:getChildByName("VipBtn")
	self._vipBtn:addTouchEventListener(btncallback)
	self._vipBtn:setEnabled(self._isOwnClub)

	local vipTips = self._vipBtn:getChildByName("Tips")
	vipTips:setVisible(self._info.bUserJoining and self._isOwnClub)
	
	--在线人数
	self._onlineRoomBtn = clubInfo:getChildByName("OnlineRoomBtn")
	self._onlineRoomBtn:addTouchEventListener(btncallback)

	--分享按钮
	-- local shareBtn = clubInfo:getChildByName("ShareBtn")
	-- shareBtn:addTouchEventListener(btncallback)

	--历史创建的房间
	self._historyRoomBtn = clubInfo:getChildByName("HistoryRoomBtn")
	self._historyRoomBtn:addTouchEventListener(btncallback)

	self._historyRoomBtn:setEnabled(self._isOwnClub)
	
	--关闭
	local closeBtn = rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btncallback)

	self._vipScrollView = rootNode:getChildByName("VipScrollView")
	self._historyRoomScrollView = rootNode:getChildByName("HistoryRoomScrollView")
	self._onlineRoomScrollView = rootNode:getChildByName("OnlineRoomScrollView")

	self._deleteAllRoom = rootNode:getChildByName("DeleteAllRoomBtn")
	self._deleteAllRoom:addTouchEventListener(btncallback)
	self._deleteAllRoom:setVisible(false)

	self._historyTitlePanel = rootNode:getChildByName("HistoryTitlePanel")
	

	self._ruleSetPanel = rootNode:getChildByName("RuleSetPanel")
	

	local nickNameTxt = clubInfo:getChildByName("NickNameTxt")
	nickNameTxt:setString(self._info.szNickName)
	
	local idTxt = clubInfo:getChildByName("IDTxt")
	idTxt:setString(self._info.dwClubNum)

	local qunZhuTxt = clubInfo:getChildByName("QunZhuTxt")
	qunZhuTxt:setString(self._info.szDescribeString)

	--默认进去就是当前房间
	self:onButtonCallback("OnlineRoomBtn")
end

function ClubManagerNode:refreshVipPanel()
	self:showPanelByIndex(VIP_PANEL)
	self:queryClubVip()
end

function ClubManagerNode:popVipDataByGameId(gameID)
	for k, vip in pairs(self._vipInfoData) do
		if gameID == vip.dwGameID then
			table.remove(self._vipInfoData, k)
			break
		end
	end

	self:onQueryClubVipResult(self._vipInfoData)
end

function ClubManagerNode:showPanelByIndex(index)
	local btnPic = "client/res/club/btn_pic/"
	if index == VIP_PANEL then
		self._vipScrollView:setVisible(true)
		self._historyRoomScrollView:setVisible(false)
		self._historyTitlePanel:setVisible(false)
		self._deleteAllRoom:setVisible(false)
		self._onlineRoomScrollView:setVisible(false)
		self._ruleSetPanel:setVisible(false)
		self._managerGameBtn:loadTextures(btnPic .. "club_game_manager_btn_normal.png", btnPic .. "club_game_manager_btn_press.png", btnPic .. "club_game_manager_btn_disabled.png")
		self._vipBtn:loadTextures(btnPic .. "club_menmber_manager_btn_press.png", btnPic .. "club_menmber_manager_btn_normal.png", btnPic .. "club_menmber_manager_btn_disabled.png")
		self._onlineRoomBtn:loadTextures(btnPic .. "club_current_room_btn_normal.png", btnPic .. "club_current_room_btn_press.png", btnPic .. "club_current_room_btn_disabled.png")
		self._historyRoomBtn:loadTextures(btnPic .. "club_history_room_btn_normal.png", btnPic .. "club_history_room_btn_press.png", btnPic .. "club_history_room_btn_disabled.png")
	elseif index == HISTORY_PANEL then
		self._vipScrollView:setVisible(false)
		self._onlineRoomScrollView:setVisible(false)
		self._historyRoomScrollView:setVisible(true)
		self._historyTitlePanel:setVisible(true)
		self._deleteAllRoom:setVisible(false)
		self._ruleSetPanel:setVisible(false)
		self._managerGameBtn:loadTextures(btnPic .. "club_game_manager_btn_normal.png", btnPic .. "club_game_manager_btn_press.png", btnPic .. "club_game_manager_btn_disabled.png")
		self._vipBtn:loadTextures(btnPic .. "club_menmber_manager_btn_normal.png", btnPic .. "club_menmber_manager_btn_press.png", btnPic .. "club_menmber_manager_btn_disabled.png")
		self._onlineRoomBtn:loadTextures(btnPic .. "club_current_room_btn_normal.png", btnPic .. "club_current_room_btn_press.png", btnPic .. "club_current_room_btn_disabled.png")
		self._historyRoomBtn:loadTextures(btnPic .. "club_history_room_btn_press.png", btnPic .. "club_history_room_btn_normal.png", btnPic .. "club_history_room_btn_disabled.png")
	elseif index == ONLINE_ROOM_PANEL then
		self._vipScrollView:setVisible(false)
		self._historyRoomScrollView:setVisible(false)
		self._historyTitlePanel:setVisible(false)
		self._deleteAllRoom:setVisible(false)
		self._onlineRoomScrollView:setVisible(true)
		self._ruleSetPanel:setVisible(false)
		self._managerGameBtn:loadTextures(btnPic .. "club_game_manager_btn_normal.png", btnPic .. "club_game_manager_btn_press.png", btnPic .. "club_game_manager_btn_disabled.png")
		self._vipBtn:loadTextures(btnPic .. "club_menmber_manager_btn_normal.png", btnPic .. "club_menmber_manager_btn_press.png", btnPic .. "club_menmber_manager_btn_disabled.png")
		self._onlineRoomBtn:loadTextures(btnPic .. "club_current_room_btn_press.png", btnPic .. "club_current_room_btn_normal.png", btnPic .. "club_current_room_btn_disabled.png")
		self._historyRoomBtn:loadTextures(btnPic .. "club_history_room_btn_normal.png", btnPic .. "club_history_room_btn_press.png", btnPic .. "club_history_room_btn_disabled.png")
	elseif index == GAME_MANAGER_PANEL then
		self._vipScrollView:setVisible(false)
		self._historyRoomScrollView:setVisible(false)
		self._historyTitlePanel:setVisible(false)
		self._deleteAllRoom:setVisible(false)
		self._onlineRoomScrollView:setVisible(false)
		self._ruleSetPanel:setVisible(true)
		self._managerGameBtn:loadTextures(btnPic .. "club_game_manager_btn_press.png", btnPic .. "club_game_manager_btn_normal.png", btnPic .. "club_game_manager_btn_disabled.png")
		self._vipBtn:loadTextures(btnPic .. "club_menmber_manager_btn_normal.png", btnPic .. "club_menmber_manager_btn_press.png", btnPic .. "club_menmber_manager_btn_disabled.png")
		self._onlineRoomBtn:loadTextures(btnPic .. "club_current_room_btn_normal.png", btnPic .. "club_current_room_btn_press.png", btnPic .. "club_current_room_btn_disabled.png")
		self._historyRoomBtn:loadTextures(btnPic .. "club_history_room_btn_normal.png", btnPic .. "club_history_room_btn_press.png", btnPic .. "club_history_room_btn_disabled.png")
	else
		self._vipScrollView:setVisible(false)
		self._historyRoomScrollView:setVisible(false)
		self._historyTitlePanel:setVisible(false)
		self._deleteAllRoom:setVisible(false)
		self._onlineRoomScrollView:setVisible(false)
		self._ruleSetPanel:setVisible(false)
		self._managerGameBtn:loadTextures(btnPic .. "club_game_manager_btn_normal.png", btnPic .. "club_game_manager_btn_press.png", btnPic .. "club_game_manager_btn_disabled.png")
		self._vipBtn:loadTextures(btnPic .. "club_menmber_manager_btn_normal.png", btnPic .. "club_menmber_manager_btn_press.png", btnPic .. "club_menmber_manager_btn_disabled.png")
		self._onlineRoomBtn:loadTextures(btnPic .. "club_current_room_btn_normal.png", btnPic .. "club_current_room_btn_press.png", btnPic .. "club_current_room_btn_disabled.png")
		self._historyRoomBtn:loadTextures(btnPic .. "club_history_room_btn_normal.png", btnPic .. "club_history_room_btn_press.png", btnPic .. "club_history_room_btn_disabled.png")
	end
end

--查询俱乐部会员结果
function ClubManagerNode:onQueryClubVipResult(cmd_data)
	dump(cmd_data, "------- onQueryClubVipResult cmd_data --------")

	local firstVip = {}

	for i = #cmd_data, 1, -1 do
		if cmd_data[i].bApply == true then
			table.insert(firstVip, cmd_data[i])
			table.remove(cmd_data, i)
		end
	end

	for k,v in pairs(firstVip) do
		table.insert(cmd_data, 1, v)
	end

	-- dump(cmd_data, "onQueryClubVipResult ---------- onQueryClubVipResult")

	self._vipInfoData = cmd_data
	self._vipScrollView:removeAllChildren()
	
	--横向开始
	local startX = 50
	--横向间隔
	local spanX = 110
	--纵向间隔
	local spanY = -110
	--一行6ge
	local NX = 10

	local innerHeight = (105) * math.ceil(#cmd_data / NX)
	if innerHeight < 600 then
		innerHeight = 650
	end
	innerHeight = innerHeight + 50
	self._vipScrollView:setInnerContainerSize(cc.size(1080, innerHeight))


	for i,v in ipairs(cmd_data) do
		local psx = math.floor((i - 1) % NX) * spanX + startX
		local psy = innerHeight + math.floor((i - 1) / NX) * spanY - 100

		local clubPlayerNode = ClubPlayerNode:create(self)
		clubPlayerNode:setPosition(cc.p(psx, psy))
		self._vipScrollView:addChild(clubPlayerNode)
		dump(v, "------- onQueryClubVipResult v --------")
		clubPlayerNode:setData(v)
	end
end

--查询俱乐部VIP
function ClubManagerNode:queryClubVip()
	PriRoom:getInstance():showPopWait()
	self._vipBtn:getChildByName("Tips"):setVisible(false)
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():queryClubVip()
end

function ClubManagerNode:queryClubOnlineRoom()
	print("queryClubOnlineRoom ------ queryClubOnlineRoom")
	PriRoom:getInstance():showPopWait()
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():queryTableInfo(self._clubNum, true)
end

function ClubManagerNode:onRoomInfo( pData )
	local tCMD = {}
    -- 计算数目
    local len = pData:getlen()
    print("ClubManagerNode onRoomInfo len = ", len, "    LEN_CLUB_ROOM = ", define_private.LEN_CLUB_ONLINE_ROOM)
    local itemcount = math.floor(len/define_private.LEN_CLUB_ONLINE_ROOM)
    print("onQueryClubRoom itemcount = ", itemcount)
    for i = 1, itemcount do
        local cmd_table = ExternalFun.read_netdata(cmd_private.game.CMD_GR_User_Info_Result, pData)
        dump(cmd_table, "--------  CMD_GR_User_Info_Result  ------")
        table.insert(tCMD, cmd_table)
    end

    self:onQueryOnlineRoomResult(tCMD)
end

function ClubManagerNode:onQueryOnlineRoomResult( cmd_data )
	self._onlineRoomData = cmd_data
	--横向开始
	local startX = 170
	--纵向开始
	local startY = 515
	--横向间隔
	local spanX = 250
	--纵向间隔
	local spanY = -232
	--一行4ge
	local NX = 4

	self._onlineRoomScrollView:removeAllChildren()

	local innerHeight = 17 + (200 + 32) * math.ceil(#cmd_data / NX)
	if innerHeight < 650 then
		innerHeight = 650
	end
	self._onlineRoomScrollView:setInnerContainerSize(cc.size(1080, innerHeight))
	for i,v in ipairs(cmd_data) do
		local psx = math.floor((i - 1) % NX) * spanX + startX
		local psy = innerHeight + math.floor((i - 1) / NX) * spanY - 117

		local clubRoomNode = ClubRoomNode:create(self)
		clubRoomNode:setPosition(cc.p(psx, psy))
		self._onlineRoomScrollView:addChild(clubRoomNode)
		clubRoomNode:setData(v)
	end
end

function ClubManagerNode:onQueryRoomResult(cmd_data)
	--横向开始
	local startX = 0
	--纵向间隔
	local spanY = -210
	--一行4ge
	local NX = 1

	self._historyRoomScrollView:removeAllChildren()

	local innerHeight = 210 * math.ceil(#cmd_data / NX)
	if innerHeight < 590 then
		innerHeight = 590
	end
	self._historyRoomScrollView:setInnerContainerSize(cc.size(1080, innerHeight))

	for k,v in pairs(cmd_data) do
		local roomNode = self._roomModelNode:clone()
		local psx = startX
		local psy = innerHeight + k * spanY
		roomNode:setPosition(cc.p(psx, psy))

		--大赢家index
		local daYingJiaIndex = 0
		local maxScoreDaYingJia = 0

		local clubListRoom = clone(v.clubListRoom[1])
	    table.sort(clubListRoom, function(a, b)
	    	return a.lScore > b.lScore
	    end)

	    local maxScore = clubListRoom[1].lScore
		roomNode:getChildByName("RoomId"):setString(v.dwRoomID)

		local detailBtn = roomNode:getChildByName("DetailBtn")
		detailBtn.data = {}
		detailBtn.data.dwTableInfoID = v.dwTableInfoID
		detailBtn.data.dwFileTag = v.dwFileTag
		detailBtn.data.dwRoomCardID = v.dwRoomID
		detailBtn.data.szDescribeString = "0,0,0,0,|"


		detailBtn:addTouchEventListener(function(sender, tType)
			if tType == ccui.TouchEventType.began then
	    		if GlobalUserItem.bSoundAble then
			        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
			    end
	        elseif tType == ccui.TouchEventType.ended then
	        	dump(sender.data, "-------------- sender.data ------------")
	        	PriRoom:getInstance():setViewFrame(self)
	        	self._clickTabData = detailBtn.data
	         	local rd = RecordDetailLayer:create(sender.data, self._clientScene)
	         	rd:setName(ROOMDETAIL_NAME)
			    rd:setPosition(yl.WIDTH/2, yl.HEIGHT/2)
			    self._rootNode:addChild(rd)
	        end
		end)

		for i = 1, 4 do
			--ID
			--头像
			--通过UserID找头像链接
			local headUrl = cc.UserDefault:getInstance():getStringForKey("HEAD_URL_" .. v.clubListRoom[1][i].dwUserID, "");
			print("headUrl = ", headUrl, "   dwUserID = ", v.clubListRoom[1][i].dwUserID)

			local headImage = roomNode:getChildByName("HeadImg_" .. i)
			local headImgSize = headImage:getContentSize()
			--群主头像
			local head = PopupInfoHead:createReplayHead(GlobalUserItem.dwUserID, headUrl, 40)
			head:setPosition(cc.p(headImgSize.width/2, headImgSize.height/2))
			headImage:addChild(head)



			--名称
			local joinNameTxt = roomNode:getChildByName("JoinNameTxt_" .. i)
			local name = v.clubListRoom[1][i].szUserNicname or ""
			--名称
			detailBtn.data.szDescribeString = detailBtn.data.szDescribeString .. name .. ","
			print("detailBtn.data.szDescribeString = ", detailBtn.data.szDescribeString)
			local nameStr = Utils.stringEllipsis(name, joinNameTxt:getFontSize(), joinNameTxt:getContentSize().width, "")
			joinNameTxt:setString(nameStr)

			--ID
			local joinIDTxt = roomNode:getChildByName("JoinIDTxt_" .. i)
			joinIDTxt:setString(v.clubListRoom[1][i].dwUserID)

			--分数
			local joinScoreTxt = roomNode:getChildByName("JoinScoreTxt_" .. i)
			joinScoreTxt:setString(v.clubListRoom[1][i].lScore)

			if v.clubListRoom[1][i].lScore >= maxScore then
				local dyjPosx = joinNameTxt:getPositionX() + joinNameTxt:getContentSize().width

				local daYingJiaTxt = roomNode:getChildByName("DaYingJiaTxt_" .. i)
				daYingJiaTxt:setPosition(cc.p(dyjPosx, joinNameTxt:getPositionY()))
			end
		end

		-- roomNode:getChildByName("GameIDTxt"):setString("玩家ID: " .. v.dwGameID)
		-- local nickName = roomNode:getChildByName("NickNameTxt")

		roomNode:getChildByName("CostFeeTxt"):setString("(消耗" .. v.dwCreateTableFee .. "张房卡)")
		local tabTime = v.sysCreateTime
		local strTime = string.format("%d-%02d-%02d %02d:%02d:%02d", tabTime.wYear, tabTime.wMonth, tabTime.wDay, tabTime.wHour, tabTime.wMinute, tabTime.wSecond)
		roomNode:getChildByName("CreateTimeTxt"):setString(strTime)
		self._historyRoomScrollView:addChild(roomNode)
	end
end

function ClubManagerNode:queryClubRoom(b)
	PriRoom:getInstance():showPopWait()
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():queryClubRoom(b, self._clubNum)
end

function ClubManagerNode:close()
	PriRoom:getInstance():setViewFrame(nil)
	self:removeFromParent()
end

function ClubManagerNode:sendModifyRule( buffer )
	buffer:retain()
	PriRoom:getInstance():showPopWait()
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():modifyClubRule(buffer)
end

function ClubManagerNode:onRoomCreateSuccess(cmd_data)
	PriRoom:getInstance():dismissPopWait()
	showToast(cc.Director:getInstance():getRunningScene(), "房间创建成功", 2)
	
	-- dump(cmd_data, "----------  ClubManagerNode:onRoomCreateSuccess  ----------")
	-- local roomInfo = {}
	-- roomInfo.cbAllCount = cmd_data.dwDrawCountLimit
	-- roomInfo.dwRoomCardId = cmd_data.szServerID
	-- self:pushOnlineRoom(roomInfo)
end

function ClubManagerNode:onClubCreateRoomSuccessLater()
	--默认进去就是当前房间
	self:onButtonCallback("OnlineRoomBtn")
end

function ClubManagerNode:onModifyRoomResult(cmd_data)
	PriRoom:getInstance():dismissPopWait()
	if cmd_data.iReturnCode == 0 then
		showToast(cc.Director:getInstance():getRunningScene(), "设置房间规则成功！", 2)
	else
		showToast(cc.Director:getInstance():getRunningScene(), "设置房间规则失败！", 2)
	end
end

function ClubManagerNode:pushOnlineRoom(room)
	table.insert(self._onlineRoomData, room)
end

function ClubManagerNode:popOnlineRoomByRoomId(roomId)
	for k, roomInfo in pairs(self._onlineRoomData) do
		if roomInfo.dwRoomCardId == roomId then
			print("删除 roomId = " .. roomId .. "  成功")
			table.remove(self._onlineRoomData, k)
			break
		end
	end
end

--刷新在线房间
function ClubManagerNode:onReloadOnlineRoom()
	self:onQueryOnlineRoomResult(self._onlineRoomData)
end

function ClubManagerNode:showRuleSetiingView()
	self._ruleSetPanel:removeAllChildren()
	if tonumber(self._info.wKindID) == yl.KIND_ID_HZ_MAHJONG then
		local clubHzRuleNode = ClubHzRuleNode:create(self)
		local size = self._ruleSetPanel:getContentSize()
		clubHzRuleNode:setPosition(cc.p(size.width/2, size.height/2))
		self._ruleSetPanel:addChild(clubHzRuleNode)
	elseif tonumber(self._info.wKindID) == yl.KIND_ID_LAND then
		local clubLandRuleNode = ClubLandRuleNode:create(self)
		local size = self._ruleSetPanel:getContentSize()
		clubLandRuleNode:setPosition(cc.p(size.width/2, size.height/2))
		self._ruleSetPanel:addChild(clubLandRuleNode)
	elseif tonumber(self._info.wKindID) == yl.KIND_ID_510K then
		local clubDPRuleNode = ClubDPRuleNode:create(self)
		local size = self._ruleSetPanel:getContentSize()
		clubDPRuleNode:setPosition(cc.p(size.width/2, size.height/2))
		self._ruleSetPanel:addChild(clubDPRuleNode)
	end
end

function ClubManagerNode:onButtonCallback(name, ref)
	if "CloseBtn" == name then
		self:close()
	elseif "QuickCreateBtn" == name then
		local buffer = appdf.req("client/src/plaza/views/layer/plaza/ClubHzRuleNode.luac"):getCreatRoomBuffer(self)
		buffer:retain()
		PriRoom:getInstance():showPopWait()
		PriRoom:getInstance():setViewFrame(self)
        PriRoom:getInstance():getNetFrame():createRoom(yl.KIND_ID_HZ_MAHJONG, buffer)
	elseif "QuickCreateLandBtn" == name then
		local buffer = appdf.req("client/src/plaza/views/layer/plaza/ClubLandRuleNode.luac"):getCreatRoomBuffer(self)
		buffer:retain()
		PriRoom:getInstance():showPopWait()
		PriRoom:getInstance():setViewFrame(self)
        PriRoom:getInstance():getNetFrame():createRoom(yl.KIND_ID_LAND, buffer)
	elseif "QuickCreate510KBtn" == name then
		local buffer = appdf.req("client/src/plaza/views/layer/plaza/ClubDPRuleNode.luac"):getCreatRoomBuffer(self)
		buffer:retain()
		PriRoom:getInstance():showPopWait()
		PriRoom:getInstance():setViewFrame(self)
        PriRoom:getInstance():getNetFrame():createRoom(yl.KIND_ID_510K, buffer)
	elseif "OnlineRoomBtn" == name then
		self:showPanelByIndex(ONLINE_ROOM_PANEL)
		self:queryClubOnlineRoom()
	elseif "ManagerGameBtn" == name then
		self:showPanelByIndex(GAME_MANAGER_PANEL)
		self:showRuleSetiingView()
	elseif "VipBtn" == name then
		self:refreshVipPanel()
	elseif "ShareBtn" == name then

	elseif "DeleteAllRoomBtn" == name then
		self:showPanelByIndex(HISTORY_PANEL)
		self:queryClubRoom(true)
	elseif "HistoryRoomBtn" == name then
		self:showPanelByIndex(HISTORY_PANEL)
		self:queryClubRoom(false)
	end
end

return ClubManagerNode