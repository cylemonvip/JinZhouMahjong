local ClubNode = class("ClubNode", cc.Node)
local CreateTipNode = require("client/src/plaza/views/layer/plaza/CreateTipNode")
local InputClubNumNode = require("client/src/plaza/views/layer/plaza/InputClubNumNode")
local ClubManagerNode = require("client/src/plaza/views/layer/plaza/ClubManagerNode")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")

local RES_CSB = "client/res/club/ClubNode.csb"

function ClubNode:ctor(scene)
	self._scene = scene
	self:initCSB()
end

function ClubNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)
	
	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

    --model
    self._clubModel = csbNode:getChildByName("ClubModel")
    

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode
	rootNode:addTouchEventListener(btncallback)

	--滚动view
	self._listView = rootNode:getChildByName("ListView")

	--创建按钮
	local createClubBtn = rootNode:getChildByName("CreateClubBtn")
	createClubBtn:addTouchEventListener(btncallback)

	--加入按钮
	local joinClubBtn = rootNode:getChildByName("JoinClubBtn")
	joinClubBtn:addTouchEventListener(btncallback)

	--关闭
	local closeBtn = rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btncallback)

	self:queryClubInfo()
end

function ClubNode:onQueryClubInfoResult(cmdDatas)
	local startX = 0
	local spanY = 105

	local innerHeight = (100 + 5) * #cmdDatas
	if innerHeight < 450 then
		innerHeight = 450
	end
	self._listView:setInnerContainerSize(cc.size(980, innerHeight))

	for k, info in pairs(cmdDatas) do
		local psx = startX
		local psy = innerHeight + (k - 1) * spanY

		local clubInfoNode = self._clubModel:clone()
		clubInfoNode:setPosition(cc.p(psx, psy))
		clubInfoNode.info = info
		clubInfoNode:getChildByName("QunZhuNameTxt"):setString("昵称:" .. info.szNickName)
		clubInfoNode:getChildByName("ClubNumTxt"):setString("俱乐部号:" .. info.dwClubNum)

		local gameStr = "锦州麻将"
		if info.wKindID == yl.KIND_ID_HZ_MAHJONG then
			gameStr = "锦州麻将"
		elseif info.wKindID == yl.KIND_ID_LAND then
			gameStr = "斗地主"
		elseif info.wKindID == yl.KIND_ID_510K then
			gameStr = "打屁"
		end
		clubInfoNode:getChildByName("GameTxt"):setString("游戏:" .. gameStr)

		local enterClub = clubInfoNode:getChildByName("EnterClub")
		enterClub.info = info
		enterClub:addTouchEventListener(function(ref, tType)
			if tType == ccui.TouchEventType.began then
	    		if GlobalUserItem.bSoundAble then
			        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
			    end
	        elseif tType == ccui.TouchEventType.ended then
				local clubManagerNode = ClubManagerNode:create(ref.info, self._scene)
				clubManagerNode:setPosition(cc.p(667, 375))
				self._scene.m_plazaLayer:addChild(clubManagerNode, 92)
				local px, py = self._scene.m_plazaLayer:getPosition()
				self:close()
	        end
		end)

		--状态
		local status = ""
		if info.bServiceing then
			status = "开启中"
			enterClub:setEnabled(true)
		else
			status = "已关闭"
			enterClub:setEnabled(false)
		end
		clubInfoNode:getChildByName("StatusTxt"):setString("状态:" .. status)
		clubInfoNode:getChildByName("DescriptionTxt"):setString("描述:" .. info.szDescribeString)

		local headImage = clubInfoNode:getChildByName("HeadImage")
		
		local headImgSize = headImage:getContentSize()
		--群主头像
		local head = PopupInfoHead:createReplayHead(GlobalUserItem.dwGameID, info.szWeChatImgURL, 80)
		head:setPosition(cc.p(headImgSize.width/2, headImgSize.height/2))
		headImage:addChild(head)

		self._listView:addChild(clubInfoNode)
	end

	if #cmdDatas > 0 then
		self._rootNode:getChildByName("NoTips"):setVisible(false)
	else
		self._rootNode:getChildByName("NoTips"):setVisible(true)
	end
end

function ClubNode:queryClubInfo()
	PriRoom:getInstance():showPopWait()
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():queryClunInfo()
end

function ClubNode:close()
	self:removeFromParent()
end

function ClubNode:onButtonCallback(name, ref)
	if "CloseBtn" == name then --"RootNode" == name or 
		self:close()
	elseif "CreateClubBtn" == name then
		local createTipNode = CreateTipNode:create()
		createTipNode:setPosition(cc.p(667, 375))
		self._rootNode:addChild(createTipNode)
	elseif "JoinClubBtn" == name then
		local inputClubNumNode = InputClubNumNode:create(self)
		inputClubNumNode:setPosition(cc.p(667, 375))
		self._rootNode:addChild(inputClubNumNode)
	end
end

return ClubNode