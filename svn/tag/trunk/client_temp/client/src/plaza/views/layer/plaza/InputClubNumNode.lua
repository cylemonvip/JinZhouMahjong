local InputClubNumNode = class("InputClubNumNode", cc.Node)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")

local RES_CSB = "client/res/club/InputClubNumNode.csb"
function InputClubNumNode:ctor(clubNode)
	self._clubNode = clubNode
	self:initCSB()
end

function InputClubNumNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	--root node
	local rootNode = csbNode:getChildByName("RootNode")
	self._rootNode = rootNode
	rootNode:addTouchEventListener(btncallback)

	local closeBtn = rootNode:getChildByName("CloseBtn")
	closeBtn:addTouchEventListener(btncallback)	

	--暂时搜索结果
	local displayImageBg = rootNode:getChildByName("DisplayImageBg")

	self._clubInfoPanel = rootNode:getChildByName("ClubInfoPanel")
	self._clubInfoPanel:addTouchEventListener(btncallback)

	--输入俱乐部号
	local inputImageBg = rootNode:getChildByName("InputImageBg")
	self._inputImageBg = inputImageBg

	self._inputNum = ""
	self:setClubNum(self._inputNum)

	--数字按钮
	for i = 0, 9 do
		local name = 'Btn_' .. i
		local btn = inputImageBg:getChildByName(name)
		btn:addTouchEventListener(btncallback)
	end

	--重输
	local btnAbgain = inputImageBg:getChildByName("Btn_again")
	btnAbgain:addTouchEventListener(btncallback)
	--删除
	local btnDel = inputImageBg:getChildByName("Btn_del")
	btnDel:addTouchEventListener(btncallback)

	--初始化自动弹出输入俱乐部号popup
	self._clubInfoPanel:setVisible(false)
end

function InputClubNumNode:setClubNum(num)
	self._inputNum = num
	--已输入数字
	for i = 1, 6 do
		local inputClubNumTxt = self._inputImageBg:getChildByName("ClubNumTxt_" .. i)
		local perNum = string.sub(self._inputNum, i, i)
		inputClubNumTxt:setString(perNum or "")
	end
end

function InputClubNumNode:setClubInfoData(data)
	local headImg = self._clubInfoPanel:getChildByName("HeadImg")
	local nameTxt = self._clubInfoPanel:getChildByName("NameTxt")
	nameTxt:setString("昵称: " .. data.szClubName)
	local qunZhuTxt = self._clubInfoPanel:getChildByName("QunZhuTxt")
	qunZhuTxt:setString("群主: " .. data.szDescribeString)
	local idTxt = self._clubInfoPanel:getChildByName("IDTxt")
	idTxt:setString("ID: " .. data.iReturnCode)
	local countTxt = self._clubInfoPanel:getChildByName("CountTxt")
	
	local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
    		if GlobalUserItem.bSoundAble then
		        AudioEngine.playEffect("client/res/sound/Click.mp3", false)
		    end
        elseif tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref:getName(), ref)
        end
    end

	local joinBtn = self._clubInfoPanel:getChildByName("JoinBtn")
	joinBtn:addTouchEventListener(btncallback)
end

function InputClubNumNode:addInputNum(num)
	if string.len(self._inputNum) < 6 then
		self._inputNum = self._inputNum .. num
		self:setClubNum(self._inputNum)
	end

	if string.len(self._inputNum) == 6 then
		self:search(self._inputNum)
	end
end

function InputClubNumNode:search(clubNum)
	PriRoom:getInstance():showPopWait()
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():applyJoinClub(true, clubNum)
end

function InputClubNumNode:join(clubNum)
	PriRoom:getInstance():showPopWait()
	PriRoom:getInstance():setViewFrame(self)
	PriRoom:getInstance():getNetFrame():applyJoinClub(false, clubNum)
end

function InputClubNumNode:onSearchClubResult(cmd_data)
	local iReturnCode = cmd_data.iReturnCode
	local desc = cmd_data.szDescribeString
	local clubName = cmd_data.szClubName

  	--申请加入
  	if iReturnCode == 0 then
  		local runScene = cc.Director:getInstance():getRunningScene()
	  	if nil ~= runScene then
	  		showToast(runScene, desc, 2)
	  	end
	else
		--不是申请，那就是查询
		self:setClubInfoData(cmd_data)
		self._clubInfoPanel:setVisible(true)	
  	end

  	
end

function InputClubNumNode:desInputNum(isClean)
	if isClean then
		self._inputNum = ""
	elseif string.len(self._inputNum) > 0 then
		self._inputNum = string.sub(self._inputNum, 1, -2)
	end
	self:setClubNum(self._inputNum)
end

function InputClubNumNode:close()
	--如果创建房间面板未弹出，则关闭此界面
	PriRoom:getInstance():setViewFrame(nil)
	self:removeFromParent()
end

function InputClubNumNode:onButtonCallback(name, ref)
	if "RootNode" == name then
		self:close()
	elseif "CreateClubBtn" == name then
		local createTipNode = CreateTipNode:create()
		createTipNode:setPosition(cc.p(667, 375))
		self._rootNode:addChild(createTipNode)
	elseif "Btn_1" == name then
		self:addInputNum(1)
	elseif "Btn_2" == name then
		self:addInputNum(2)
	elseif "Btn_3" == name then
		self:addInputNum(3)
	elseif "Btn_4" == name then
		self:addInputNum(4)
	elseif "Btn_5" == name then
		self:addInputNum(5)
	elseif "Btn_6" == name then
		self:addInputNum(6)
	elseif "Btn_7" == name then
		self:addInputNum(7)
	elseif "Btn_8" == name then
		self:addInputNum(8)
	elseif "Btn_9" == name then
		self:addInputNum(9)
	elseif "Btn_0" == name then
		self:addInputNum(0)
	elseif "Btn_again" == name then
		self:desInputNum(true)
	elseif "Btn_del" == name then
		self:desInputNum(false)
	elseif "JoinBtn" == name then
		self:join(self._inputNum)
	elseif "CloseBtn" == name then
		self:close()
	elseif "ClubInfoPanel" == name then
		self._clubInfoPanel:setVisible(false)
	end
end

return InputClubNumNode