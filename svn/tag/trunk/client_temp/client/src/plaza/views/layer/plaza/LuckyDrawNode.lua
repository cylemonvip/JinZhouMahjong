
local LuckyDrawPopup = appdf.req(appdf.CLIENT_SRC .. "plaza.views.layer.plaza.LuckyDrawPopup")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local LuckyDrawNode = class("ChoicesPopupNode", function ()
	return cc.Node:create()
end)

local CSB_RES_PATH = "client/res/luckydraw/LuckyDrawNode.csb"

local SELECT_WHEEL_POS = {
	cc.p(750, 550),
	cc.p(856, 550),
	cc.p(962, 550),
	cc.p(1068, 550),
	cc.p(1068, 444),
	cc.p(1068, 340),
	cc.p(1068, 234),
	cc.p(962, 234),
	cc.p(856, 234),
	cc.p(750, 234),
	cc.p(750, 340),
	cc.p(750, 444)
}

local LUCKY_DRAW_RES = {
	"client/res/luckydraw/faku_cash2_icon.png",
	"client/res/luckydraw/faku_cash10_icon.png",
	"client/res/luckydraw/faku_card2_icon.png",
	"client/res/luckydraw/faku_iphone8_icon.png",
	"client/res/luckydraw/faku_card1_icon.png",
	"client/res/luckydraw/once_again.png",
	"client/res/luckydraw/faku_cash100_icon.png",
	"client/res/luckydraw/faku_card3_icon.png",
	"client/res/luckydraw/faku_cash5_icon.png",
	"client/res/luckydraw/faku_cash1_icon.png",
	"client/res/luckydraw/faku_card1_icon.png",
	"client/res/luckydraw/once_again.png",
}

function LuckyDrawNode:ctor(scene)
	self._scene = scene
	self._heightLightEnabled = true
	self._heightLightAngle = 0

	self._lastEndIndex = 0
	
	self:initCsb()
	self:setCanClose(true)
	
	self:registerScriptHandler(function(eventType)
		if eventType == "exit" then
			self:onExit()
		elseif eventType == "enter" then
			self:onEnter()
		end
	end)
end

function LuckyDrawNode:pauseHeightLight()
	self._heightLightEnabled = false
	self._heightLightAngle = 0
end

function LuckyDrawNode:resumeHeightLight()
	self._heightLightEnabled = true
end

function LuckyDrawNode:onEnter()
	self._heightLightUpdate = cc.Director:getInstance():getScheduler():scheduleScriptFunc(function(dt)
    	if self._heightLightEnabled then
    		self._heightLightAngle = self._heightLightAngle + 30
    	end
    end, 0.3, false)
end

function LuckyDrawNode:onExit()
	if self._heightLightUpdate then
    	cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._heightLightUpdate)
    	self._heightLightUpdate = nil
    end
end

function LuckyDrawNode:onButtonClickedEvent(name, ref)
	if name == "BtnStart" then
		self:onStartCallback()
	elseif name == "RootNode" then
		self:onColse()
	elseif name == "BtnCopy" then
		MultiPlatform:getInstance():copyToClipboard("jiujiumj01")
        showToast(self._scene, "已复制到剪贴板，快去微信搜索吧", 2)
	end
end

function LuckyDrawNode:onColse()
	if self._canClose then
		yl.isInLuckyDraw = false
		self:removeFromParent()
	end
end

function LuckyDrawNode:onStartCallback()
	--超过10魅力值才能抽奖
	if GlobalUserItem.dwLoveLiness >= 10 then
		self._scene:showPopWait()
		PriRoom:getInstance():setViewFrame(self)
		PriRoom:getInstance():getNetFrame():queryLuckyDraw()
	else
		showToast(self._scene, "您的魅力值不足，先去打几圈吧！", 2)
	end
end

function LuckyDrawNode:setCanClose(b)
	self._canClose = b
	self.btnStart:setEnabled(self._canClose)
end

function LuckyDrawNode:onLuckyDrawResult(cmd_data)
	dump(cmd_data, "onLuckyDrawResult cmd_data")
	self._scene:dismissPopWait()
	self:pauseHeightLight()

	--中奖编号 0 - N
	local luckyIndex = cmd_data.cbLuckyIndex
	--红包的随机金额
	local randomMoney = cmd_data.dLuckyValue
	--剩余的魅力值
	local restLoveLiness = cmd_data.lLoveLiness
	GlobalUserItem.dwLoveLiness = restLoveLiness
	self._scene._goldNewText:setString("" .. restLoveLiness)

	GlobalUserItem.luckyDrawIndex = cmd_data.cbRealIndex
	if luckyIndex == 255 then
		showToast(self._scene, "您的魅力值不足，先去打几圈吧！", 2)
	else
		local date = os.date("%Y.%m.%d %H:%M:%S")
		for i = 1, 12 do
			local fafu = self.item:getChildByName("fafu_" .. (i-1))
			local guang = fafu:getChildByName("guang")
			guang:setVisible(false)
		end
		
		self:setCanClose(false)
		self._currSelectIndex = self._lastEndIndex
		local eindex = 12 - luckyIndex
		self._endSelectIndex = eindex + 12 * 6 --12个一圈，多转6圈

		--上次转的最后停留的地方
		self._lastEndIndex = eindex
		--总间隔
		local moveDelay = 0.5
		--加速 间隔
		local moveDelayAscSpan = 0.08
		--减速 间隔
		local moveDelayDescSpan = 0.05
		local minDelay = 0.02
		local maxDelay = 0.5
		local jiansu = false

		--间隔 span
		local moveDelaySpan = moveDelayAscSpan

		function moveNext ()
			self._currSelectIndex = self._currSelectIndex + 1
			
			local pos = SELECT_WHEEL_POS[self._currSelectIndex % 12 + 1]
			if math.ceil(self._endSelectIndex / 12 * 10) == self._currSelectIndex and not jiansu then
				moveDelaySpan = -moveDelayDescSpan
				jiansu = true
			end

			self.selectWheel:setPosition(pos)
			if self._currSelectIndex < self._endSelectIndex  then
				self:stopAllActions()
				self:runAction(cc.Sequence:create(
					cc.DelayTime:create(moveDelay),
					cc.CallFunc:create(
						function()
							moveDelay = moveDelay - moveDelaySpan
							if moveDelay <= minDelay then
								moveDelay = minDelay
							end

							if moveDelay > maxDelay then
								moveDelay = maxDelay
							end
							moveNext()
						end
						)
					))
			else
				local fafu = self.item:getChildByName("fafu_" .. luckyIndex)
				local guang = fafu:getChildByName("guang")
				guang:setVisible(true)

				if luckyIndex ~= 5 and luckyIndex ~= 11 then
					self:stopAllActions()
					self:runAction(cc.Sequence:create(
						cc.DelayTime:create(3),
						cc.CallFunc:create(
							function()
								self:setCanClose(true)
								local luckyDrawPopup = LuckyDrawPopup:create(self._scene, LUCKY_DRAW_RES[luckyIndex+1], date)
								luckyDrawPopup:setPosition(cc.p(667, 375))
								self._scene.m_plazaLayer:addChild(luckyDrawPopup, yl.MAX_INT + 3)
							end
							)
						))
				else
					self:setCanClose(true)
				end

			end
		end

		moveNext()
	end
end

function LuckyDrawNode:initCsb()
	local node =  cc.CSLoader:createNode(CSB_RES_PATH)
	self:addChild(node)

	self.rootNode = node:getChildByName("RootNode")
	self.btnStart = self.rootNode:getChildByName("BtnStart")

	local btnCallback = function (ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			self:onButtonClickedEvent(ref:getName(), ref)
		elseif eventType == ccui.TouchEventType.began then
		elseif eventType == ccui.TouchEventType.canceled then
		end
	end

	self.btnStart:addTouchEventListener(btnCallback)
	self.rootNode:addTouchEventListener(btnCallback)

	self.selectWheel = self.rootNode:getChildByName("SelectWheel")

	local btnCopy = self.rootNode:getChildByName("BtnCopy")
	btnCopy:addTouchEventListener(btnCallback)

	self.item = self.rootNode:getChildByName("Item")
	
	for i = 1, 12 do
		local fafu = self.item:getChildByName("fafu_" .. (i-1))
		local guang = fafu:getChildByName("guang")
		guang:setScale(0.6)
		guang:runAction(cc.RepeatForever:create(cc.RotateBy:create(1, 360)))
		guang:setVisible(false)
	end

end

return LuckyDrawNode