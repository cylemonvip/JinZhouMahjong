--
-- Author: zhong
-- Date: 2016-12-17 14:07:02
--
-- 斗地主私人房创建界面
local CreateLayerModel = appdf.req(PriRoom.MODULE.PLAZAMODULE .."models.CreateLayerModel")

local PriRoomCreateLayer = class("PriRoomCreateLayer", CreateLayerModel)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Shop = appdf.req(appdf.CLIENT_SRC.."plaza.views.layer.plaza.ShopLayer")

local RES_PATH = "client/src/privatemode/game/qipai/land/res/"

local BTN_MYROOM = 3
local BTN_CREATE = 4
local CBT_BEGIN = 300
local CBT_MAX_FAN_BEGIN = 400

function PriRoomCreateLayer:ctor( scene, isClub )
    PriRoomCreateLayer.super.ctor(self, scene, isClub)

    cc.FileUtils:getInstance():addSearchPath(device.writablePath..RES_PATH, true)
    cc.FileUtils:getInstance():addSearchPath(RES_PATH)
    cc.FileUtils:getInstance():addSearchPath(RES_PATH.."room")

    local rootLayerBase, csbNodeBase = ExternalFun.loadRootCSB("LandCreateRoomNode.csb", self )
    csbNodeBase:setPosition(cc.p(yl.WIDTH/2, yl.HEIGHT/2))

    local baseRoot =csbNodeBase:getChildByName("RootNode")
    self.m_baseRoot = baseRoot
    local btnBack =baseRoot:getChildByName("BtnBack")
    btnBack:setVisible(self._clubNum == nil)
    btnBack:addTouchEventListener(function(ref, tType)
        if tType == ccui.TouchEventType.ended then
            self._scene:onKeyBack()
        end
    end)

    baseRoot:setEnabled(self._clubNum == nil)
    
    local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.ended then
            self:onButtonClickedEvent(ref:getTag(),ref)
        end
    end

    local mask = baseRoot:getChildByName("Mask")
    mask:setVisible(self._clubNum == nil)
    
    -- -- 房卡数
    local costBg = baseRoot:getChildByName("CostBg")
    self.m_txtCardNum = costBg:getChildByName("RoomCard")
    self.m_txtCardNum:setString(GlobalUserItem.lRoomCard .. "")

    dump(PriRoom:getInstance().m_tabFeeConfigList, "m_tabFeeConfigList")

    -- 创建按钮
    local btn = baseRoot:getChildByName("CreateBtn")
    btn:setTag(BTN_CREATE)
    btn:addTouchEventListener(btncallback)

    local cbtlistener = function (sender,eventType)
        self:onSelectedEvent(sender:getTag(),sender)
    end

    local cb_People = baseRoot:getChildByName("CB_People")
    cb_People:setSelected(true)
    cb_People:setTouchEnabled(false);

    self.m_tabCheckBox = {}

    --圈数
    local quanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey("LAND_QUAN_CONFIG_INDEX", 2))
    for i = 1, 3 do
        local config = PriRoom:getInstance().m_tabFeeConfigList[i]
        --局数
        local juShuCheckBox = baseRoot:getChildByName("CB_Ju_Shu_" .. i)
        juShuCheckBox:setTag(CBT_BEGIN + i)
        juShuCheckBox:setTouchEnabled(true);
        juShuCheckBox:addEventListener(cbtlistener)
        if i == quanIndex then
            self.m_tabSelectConfig = config
            juShuCheckBox:setSelected(true)
        else
            juShuCheckBox:setSelected(false)
        end

        juShuCheckBox.index = i
        juShuCheckBox.config = config
        
        self.m_tabCheckBox[CBT_BEGIN + i] = juShuCheckBox

        local juTxt = juShuCheckBox:getChildByName("Txt")
        juTxt:setString(config.dwDrawCountLimit.."局")

        local coatTxt = juShuCheckBox:getChildByName("CostNum")
        coatTxt:setString("房卡x" .. config.lFeeScore)
    end

    local rule1 = tonumber(cc.UserDefault:getInstance():getStringForKey("LAND_RULE_1_CONFIG_INDEX", 0))
    --双王或4个2必叫分
    self._cb_Rule = baseRoot:getChildByName("CB_Rule")
    self._cb_Rule:setSelected(rule1 == 1)
    

    --农民可加倍
    local rule2 = tonumber(cc.UserDefault:getInstance():getStringForKey("LAND_RULE_2_CONFIG_INDEX", 0))
    self._cb_Rule_Double = baseRoot:getChildByName("CB_Rule_Double")
    self._cb_Rule_Double:setSelected(rule2 == 1)

    local cbtMaxFanlistener = function (sender,eventType)
        self:onMaxFanSelectedEvent(sender:getTag(),sender)
    end

    self.m_tabMaxFanCheckBox = {}
    --最大番数
    local fanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey("LAND_FAN_CONFIG_INDEX", 2))
    for i = 1, 3 do
        local cb_MaxFan = baseRoot:getChildByName("CB_MaxFan_" .. i)
        cb_MaxFan:setTag(CBT_MAX_FAN_BEGIN + i)
        cb_MaxFan:setTouchEnabled(true)
        cb_MaxFan:addEventListener(cbtMaxFanlistener)

        self.m_tabMaxFanCheckBox[CBT_MAX_FAN_BEGIN + i] = cb_MaxFan

        if i == fanIndex then
            self.m_currSelectMaxFan = CBT_MAX_FAN_BEGIN + i
            cb_MaxFan:setSelected(true)
        else
            cb_MaxFan:setSelected(false)
        end
    end
end

------
-- 继承/覆盖
------
-- 刷新界面
function PriRoomCreateLayer:onRefreshInfo()
    -- 房卡数更新
    self.m_txtCardNum:setString(GlobalUserItem.lRoomCard .. "")
end

function PriRoomCreateLayer:getCreateRoomBuffer()
    -- 创建登陆
    local buffer = CCmd_Data:create(188)
    buffer:setcmdinfo(self._cmd_pri_game.MDM_GR_PERSONAL_TABLE,self._cmd_pri_game.SUB_GR_CREATE_TABLE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushdword(self._clubNum or 0)
    buffer:pushdword(self.m_tabSelectConfig.dwDrawCountLimit)
    buffer:pushdword(self.m_tabSelectConfig.dwDrawTimeLimit)
    --这个人数不用了
    buffer:pushword(3)
    buffer:pushdword(0)
    buffer:pushstring("", yl.LEN_PASSWORD)

    buffer:pushbyte(0)
    --人数
    buffer:pushbyte(3)
    buffer:pushbool(self._cb_Rule:isSelected())

    if self._cb_Rule:isSelected() then
        cc.UserDefault:getInstance():setStringForKey("LAND_RULE_1_CONFIG_INDEX", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("LAND_RULE_1_CONFIG_INDEX", 0)
    end

    buffer:pushbool(self._cb_Rule_Double:isSelected())
    if self._cb_Rule_Double:isSelected() then
        cc.UserDefault:getInstance():setStringForKey("LAND_RULE_2_CONFIG_INDEX", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("LAND_RULE_2_CONFIG_INDEX", 0)
    end

    if self.m_currSelectMaxFan == (CBT_MAX_FAN_BEGIN + 1) then
        buffer:pushbyte(3)
        cc.UserDefault:getInstance():setStringForKey("LAND_FAN_CONFIG_INDEX", 1)
    elseif self.m_currSelectMaxFan == (CBT_MAX_FAN_BEGIN + 2) then
        buffer:pushbyte(4)
        cc.UserDefault:getInstance():setStringForKey("LAND_FAN_CONFIG_INDEX", 2)
    elseif self.m_currSelectMaxFan == (CBT_MAX_FAN_BEGIN + 3) then
        buffer:pushbyte(5)
        cc.UserDefault:getInstance():setStringForKey("LAND_FAN_CONFIG_INDEX", 3)
    end

    return buffer
end

function PriRoomCreateLayer:getInviteShareMsg( roomDetailInfo )
    local shareTxt = "斗地主约战 房间ID:" .. roomDetailInfo.szRoomID .. " 局数:" .. roomDetailInfo.dwPlayTurnCount
    local friendC = "斗地主房间ID:" .. roomDetailInfo.szRoomID .. " 局数:" .. roomDetailInfo.dwPlayTurnCount
    return {title = "斗地主约战", content = shareTxt .. " 斗地主游戏精彩刺激, 一起来玩吧! ", friendContent = friendC}
end

function PriRoomCreateLayer:onExit()
end

------
-- 继承/覆盖
------

function PriRoomCreateLayer:onButtonClickedEvent( tag, sender)
    if BTN_MYROOM == tag then
        self._scene:onChangeShowMode(PriRoom.LAYTAG.LAYER_MYROOMRECORD)
    elseif BTN_CREATE == tag then 
        if self.m_tabSelectConfig.lFeeScore > GlobalUserItem.lRoomCard then
            local QueryDialog = appdf.req("client.src.plaza.views.layer.game.QueryDialog")
            local query = QueryDialog:create("您的房卡数量不足，请购买房卡！"):setCanTouchOutside(false)
                :addTo(self.m_baseRoot , 10)
            return
        end
        if nil == self.m_tabSelectConfig or table.nums(self.m_tabSelectConfig) == 0 then
            showToast(self, "未选择玩法配置!", 2)
            return
        end
        PriRoom:getInstance():showPopWait()
        -- PriRoom:getInstance():getNetFrame():onCreateRoom()
        local buffer = self:getCreateRoomBuffer()
        buffer:retain()
        PriRoom:getInstance():getNetFrame():createRoom(200, buffer)
    end
end

function PriRoomCreateLayer:onMaxFanSelectedEvent(tag, sender)
    if self.m_currSelectMaxFan == tag then
        sender:setSelected(true)
        return
    end
    self.m_currSelectMaxFan = tag
    for k,v in pairs(self.m_tabMaxFanCheckBox) do
        v:setSelected(false)
    end

    sender:setSelected(true)
end

function PriRoomCreateLayer:onSelectedEvent(tag, sender)
    if self.m_tabSelectConfig == sender.config then
        sender:setSelected(true)
        return
    end

    self.m_tabSelectConfig = sender.config
    for k,v in pairs(self.m_tabCheckBox) do
        v:setSelected(false)
    end

    cc.UserDefault:getInstance():setStringForKey("LAND_QUAN_CONFIG_INDEX", sender.index)
    sender:setSelected(true)
end

return PriRoomCreateLayer