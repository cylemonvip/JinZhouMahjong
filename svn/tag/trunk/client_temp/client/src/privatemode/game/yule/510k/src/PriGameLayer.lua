--
-- Author: zhong
-- Date: 2016-12-29 11:13:57
--
-- 私人房游戏顶层
local PrivateLayerModel = appdf.req(PriRoom.MODULE.PLAZAMODULE .."models.PrivateLayerModel")
local PriGameLayer = class("PriGameLayer", PrivateLayerModel)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local PopupInfoHead = appdf.req(appdf.EXTERNAL_SRC .. "PopupInfoHead")
local ClipText = appdf.req(appdf.EXTERNAL_SRC .. "ClipText")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local BTN_DISMISS = 101
local BTN_INVITE = 102
local BTN_SHARE = 103
local BTN_QUIT = 104
local BTN_ZANLI = 105

local RES_PATH = "client/src/privatemode/game/yule/510k/res/"

function PriGameLayer:ctor( gameLayer )
    cc.FileUtils:getInstance():addSearchPath(device.writablePath..RES_PATH, true)
    cc.FileUtils:getInstance():addSearchPath(RES_PATH)

    PriGameLayer.super.ctor(self, gameLayer)
    -- 加载csb资源
    local rootLayer, csbNode = ExternalFun.loadRootCSB("client/src/privatemode/game/yule/510k/res/game/PrivateGameLayer.csb", self )
    self.m_rootLayer = rootLayer

    local image_bg = csbNode:getChildByName("Image_bg")

    -- 房间ID
    self.m_atlasRoomID = image_bg:getChildByName("room_id")
    self.m_atlasRoomID:setString("000000")

    --对方分数
    self.m_selfScoreTxt = image_bg:getChildByName("SelfScoreTxt")
    self.m_selfScoreTxt:setString("0")

    --自己分数
    self.m_otherScoreTxt = image_bg:getChildByName("OtherScoreTxt")
    self.m_selfScoreTxt:setString("0")

    --时间
    local timeText = image_bg:getChildByName("TimeText")
    local date=os.date("%H:%M");
    timeText:setString(tostring(date))

    --电池
    local dianChiLayout = image_bg:getChildByName("DianChiLayout")
    local valueLayout = dianChiLayout:getChildByName("ValueLayout") 
    local totalLength = 44
    local power = MultiPlatform:getInstance():getClientPower()
    print("power = " .. tostring(power) .. "  signalStrength = " .. tostring(signalStrength))
    valueLayout:setContentSize(cc.size(totalLength * power / 100, 18))

    self.entryId = cc.Director:getInstance():getScheduler():scheduleScriptFunc( 
        function( dt )
            local date=os.date("%H:%M");
            timeText:setString(tostring(date))

            local power = MultiPlatform:getInstance():getClientPower()
            local totalLength = 44
            valueLayout:setContentSize(cc.size(totalLength * power / 100, 18))
        end,
    30, false)

    



    -- 局数
    self.m_curRoundNum = image_bg:getChildByName("curRoundNum")
    self.m_curRoundNum:setString("0")

    self.m_totalRoundNum = image_bg:getChildByName("totalRoundNum")
    self.m_totalRoundNum:setString("0")
    
    local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.ended then
            self:onButtonClickedEvent(ref:getTag(),ref)
        end
    end

    -- 邀请按钮
    self.m_btnInvite = csbNode:getChildByName("bt_invite")
    self.m_btnInvite:setVisible(true)
    self.m_btnInvite:setTag(BTN_INVITE)
    self.m_btnInvite:addTouchEventListener(btncallback)
end

function PriGameLayer:onButtonClickedEvent( tag, sender )
    if BTN_DISMISS == tag then              -- 请求解散游戏
        PriRoom:getInstance():queryDismissRoom()
    elseif BTN_INVITE == tag then
        --GlobalUserItem.bAutoConnect = false
        PriRoom:getInstance():getPlazaScene():popTargetShare(function(target, bMyFriend)
            bMyFriend = bMyFriend or false
            local function sharecall( isok )
                if type(isok) == "string" and isok == "true" then
                    showToast(self, "分享成功", 2)
                end
                GlobalUserItem.bAutoConnect = true
            end
            local shareTxt = "510k约战 房间ID:" .. self.m_atlasRoomID:getString() .. " 局数:" .. PriRoom:getInstance().m_tabPriData.dwDrawCountLimit
            local friendC = "510k房间ID:" .. self.m_atlasRoomID:getString() .. " 局数:" .. PriRoom:getInstance().m_tabPriData.dwDrawCountLimit
            local url = GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
            if bMyFriend then
                PriRoom:getInstance():getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function( frienddata )
                    local serverid = tonumber(PriRoom:getInstance().m_tabPriData.szServerID) or 0                    
                    PriRoom:getInstance():priInviteFriend(frienddata, GlobalUserItem.nCurGameKind, serverid, yl.INVALID_TABLE, friendC)
                end)
            elseif nil ~= target then
                GlobalUserItem.bAutoConnect = false
                MultiPlatform:getInstance():shareToTarget(target, sharecall, "510k约战", shareTxt .. " 510k游戏精彩刺激, 一起来玩吧! ", url, "")
            end
        end)
    elseif BTN_SHARE == tag then
        GlobalUserItem.bAutoConnect = false
        local function sharecall( isok )
            if type(isok) == "string" and isok == "true" then
                showToast(self, "分享成功", 2)
            end
            GlobalUserItem.bAutoConnect = true
        end
        local url = GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
        -- 截图分享
        local framesize = cc.Director:getInstance():getOpenGLView():getFrameSize()
        local area = cc.rect(0, 0, framesize.width, framesize.height)
        local imagename = "grade_share.jpg"

        ExternalFun.popupTouchFilter(0, false)
        captureScreenWithArea(area, imagename, function(ok, savepath)
            ExternalFun.dismissTouchFilter()
            if ok then
                GlobalUserItem.bAutoConnect = false
                MultiPlatform:getInstance():shareToTarget(yl.ThirdParty.WECHAT, sharecall, "我的约战房战绩", "分享我的约战房战绩", url, savepath, "true")      
            end
        end)
    elseif BTN_QUIT == tag then
        GlobalUserItem.bWaitQuit = false
        self._gameLayer:onExitRoom()
    elseif BTN_ZANLI == tag then
        self._gameLayer._scene._gameFrame:StandUp(false)
        -- PriRoom:getInstance():getNetFrame()._gameFrame:setEnterAntiCheatRoom(false)                
        -- PriRoom:getInstance():getNetFrame()._gameFrame:onCloseSocket()
        -- self._gameLayer:onExitRoom()
    end
end

------
-- 继承/覆盖
------
-- 刷新界面
function PriGameLayer:onRefreshInfo()
    -- 房间ID
    self.m_atlasRoomID:setString(PriRoom:getInstance().m_tabPriData.szServerID or "000000")

    -- 局数
    self.m_curRoundNum:setString(""..PriRoom:getInstance().m_tabPriData.dwPlayCount)

    -- 局数
    self.m_totalRoundNum:setString(""..PriRoom:getInstance().m_tabPriData.dwDrawCountLimit)

    self:onRefreshInviteBtn()
end

function PriGameLayer:onRefreshInviteBtn()
    if self._gameLayer.m_cbGameStatus ~= 0 or PriRoom:getInstance().m_tabPriData.dwPlayCount > 0 then --空闲场景
        self.m_btnInvite:setVisible(false)
        return
    end
    -- 邀请按钮
    if nil ~= self._gameLayer.onGetSitUserNum then
        local chairCount = PriRoom:getInstance():getChairCount()
        if self._gameLayer:onGetSitUserNum() == chairCount then
            self.m_btnInvite:setVisible(false)
            return
        end
    end
    self.m_btnInvite:setVisible(true)
end

function PriGameLayer:onPriGameEndFromGameLayer( cmd_table )
    self:removeChildByName("private_end_layer")
    if true then
        --return
    end
    local csbNode = ExternalFun.loadCSB("client/src/privatemode/game/yule/510k/res/game/PrivateGameEndLayer.csb", self.m_rootLayer)
    csbNode:setVisible(false)
    csbNode:setName("private_end_layer")
    local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.ended then
            self:onButtonClickedEvent(ref:getTag(),ref)
        end
    end

    local image_bg = csbNode:getChildByName("Image_bg")

    local hsMaxCount = 0
    local nnMaxCount = 0
    --皇上驾到的玩家椅子号
    local hsjdIndex = 0
    --娘娘附体
    local nnftIndex = 0
    for i=1, 4 do
        local hsCount = cmd_table.cbHuangShang[1][i]
        if hsCount > hsMaxCount then
            hsMaxCount = hsCount
            hsjdIndex = i - 1
        end

        local nnCount = cmd_table.cbNiangNiang[1][i]
        if nnCount > nnMaxCount then
            nnMaxCount = nnCount
            hsjdIndex = i - 1
        end
    end


    -- 玩家成绩
    for i = 1, 4 do
        print("i = " .. i)
        local useritem = self._gameLayer:getUserInfoByChairID(i - 1)

        local userNode = csbNode:getChildByName("UserNode_"..i)
        --名称
        local nameTxt = userNode:getChildByName("NameTxt")
        nameTxt:setString(useritem.szNickName)
        --ID
        local IDTxt = userNode:getChildByName("IDTxt")
        IDTxt:setString(useritem.dwGameID)
        --皇上次数
        local huangShangCountTxt = userNode:getChildByName("HuangShangCountTxt")
        huangShangCountTxt:setString(cmd_table.cbHuangShang[1][i])
        --娘娘次数
        local niangNiangCountTxt = userNode:getChildByName("NiangNiangCountTxt")
        niangNiangCountTxt:setString(cmd_table.cbNiangNiang[1][i])

        --大血次数
        local bigXueCountTxt = userNode:getChildByName("BigXueCountTxt")
        --小血次数
        local smallXueCountTxt = userNode:getChildByName("SmallXueCountTxt")
    
        local bigScore = cmd_table.cbDaXue[1][i] * 2
        local smallScore = cmd_table.cbXiaoXue[1][i] * 1
        if cmd_table.bTakeXue then
            bigXueCountTxt:setString(cmd_table.cbDaXue[1][i])
            smallXueCountTxt:setString(cmd_table.cbXiaoXue[1][i])
        else
            bigXueCountTxt:setString("0")
            smallXueCountTxt:setString("0")
        end        

        --总结算
        local totalScore = userNode:getChildByName("TotalScore")
        local scoreStr = nil
        
        if cmd_table.bTakeXue then
            scoreStr = cmd_table.lRealScore[1][i]
            if cmd_table.lRealScore[1][i] > 0 then
                scoreStr = "+" .. scoreStr
            end
        else
            scoreStr = cmd_table.lAllScore[1][i]
            if cmd_table.lAllScore[1][i] > 0 then
                scoreStr = "+" .. scoreStr
            end
        end

        totalScore:setString(scoreStr)

        --皇上驾到，娘娘附身的图标
        local hnFlag = userNode:getChildByName("HNFlag")
        hnFlag:setScale(0.9)
        hnFlag:setVisible(false)
        -- if hsjdIndex == i-1 then
        --     hnFlag:setTexture("client/src/privatemode/game/yule/510k/res/pri_end_res/bg_label_hsjd.png")
        --     hnFlag:setVisible(true)
        -- end

        -- if nnftIndex == i-1 then
        --     hnFlag:setTexture("client/src/privatemode/game/yule/510k/res/pri_end_res/bg_label_nnft.png")
        --     hnFlag:setVisible(true)
        -- end

        -- if hsjdIndex ~= i-1 and nnftIndex ~= i-1 then
        --     hnFlag:setVisible(false)
        -- end


        local headNode = userNode:getChildByName("HeadNode")
        
        local head = PopupInfoHead:createNormal(useritem, 80)
        head:setPosition(cc.p(0, 0))
        head:setName("NAME_HeadInfo")
        headNode:addChild(head)

        local spRoomHost = userNode:getChildByName("HostFlagImg")
        if PriRoom:getInstance().dwTableOwnerUserID and useritem.dwUserID == PriRoom:getInstance().dwTableOwnerUserID then
            spRoomHost:setVisible(true)
        else
            spRoomHost:setVisible(false)
        end
    end

    local timeTxt = csbNode:getChildByName("TimeTxt")
    local date=os.date("%Y.%m.%d %H:%M")
    timeTxt:setString("日期: " .. tostring(date))

    -- 分享按钮
    local btn = image_bg:getChildByName("btn_share")
    btn:setTag(BTN_SHARE)
    btn:addTouchEventListener(btncallback)

    -- 退出按钮
    btn = image_bg:getChildByName("btn_quit")
    btn:setTag(BTN_QUIT)
    btn:addTouchEventListener(btncallback)

    csbNode:runAction(cc.Sequence:create(cc.DelayTime:create(3.0),
        cc.CallFunc:create(function()
            csbNode:setVisible(true)
        end)))
end

-- 私人房游戏结束
function PriGameLayer:onPriGameEnd( cmd_table )
    local tabUserRecord = self._gameLayer:getDetailScore()
    self:onPriGameEndFromGameLayer(tabUserRecord)
end

function PriGameLayer:onExit()
    cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile("client/src/privatemode/game/yule/510k/res/game/land_game.plist")
    cc.Director:getInstance():getTextureCache():removeTextureForKey("client/src/privatemode/game/yule/510k/res/game/land_game.png")

    if self.entryId then  
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry( self.entryId )  
        self.entryId = nil
    end
end

return PriGameLayer