--
-- Author: Tang
-- Date: 2017-01-08 16:27:52
--
-- 红中麻将私人房创建界面
--local PriRoom = appdf.req("client.src.privatemode.plaza.src.models.PriRoom")
require("client.src.plaza.models.yl")
local CreateLayerModel = appdf.req(PriRoom.MODULE.PLAZAMODULE .."models.CreateLayerModel")

local PriRoomCreateLayer = class("PriRoomCreateLayer", CreateLayerModel)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Shop = appdf.req(appdf.CLIENT_SRC.."plaza.views.layer.plaza.ShopLayer")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")
local RES_PATH = "client/src/privatemode/game/yule/sparrowhld/res/"

local BT_CREATE = 1
local BT_HELP = 2
local BT_PAY = 3

local CBX_INNINGS_1 = 33
local CBX_INNINGS_2 = 34
local CBX_INNINGS_3 = 35

local CBX_USERNUM_3 = 5
local CBX_USERNUM_4 = 6

local CBX_RULE_1 = 41
local CBX_RULE_2 = 42
local CBX_RULE_3 = 43
local CBX_RULE_4 = 44
local CBX_RULE_5 = 45
local CBX_RULE_6 = 46
local CBX_RULE_7 = 47
local CBX_RULE_8 = 48

local BT_MYROOM = 7

local CBX_TianDi = 8;
local CBX_HuiPai = 9;
local CBX_QiongHu = 10;
local CBX_SanQing = 11;
local CBX_FengDing = 12;
local CBX_LaiZi = 13;

local TAG_CLOSE_BTN = 20

function PriRoomCreateLayer:onInitData()
    self.cbMaCount = 0
    self.cbUserNum = 0
end

function PriRoomCreateLayer:onResetData()
    self.cbMaCount = 0
    self.cbUserNum = 0
end

function PriRoomCreateLayer:ctor( scene, isClub )
    self:onInitData()
    self:initData()
    PriRoomCreateLayer.super.ctor(self, scene, isClub)

    -- 加载csb资源
    cc.FileUtils:getInstance():addSearchPath(device.writablePath..RES_PATH, true)
    local rootLayer
    rootLayer, self.m_csbNode = ExternalFun.loadRootCSB(RES_PATH.."privateRoom/RoomCardLayer.csb", self)

    local function btncallback(ref, tType)
        if tType == ccui.TouchEventType.began then
            if GlobalUserItem.bSoundAble then
                AudioEngine.playEffect("client/res/sound/Click.mp3", false)
            end
        elseif tType == ccui.TouchEventType.ended then
            self:onButtonClickedEvent(ref:getTag(),ref)
        end
    end
    -- 创建按钮
    self.m_csbNode:getChildByName("Btn_CreatRoom")
        :setTag(BT_CREATE)
        :addTouchEventListener(btncallback)

    -- 充值按钮
    self.m_csbNode:getChildByName("Btn_ChongZhi")
        :setTag(BT_PAY)
        :addTouchEventListener(btncallback)
    --我的房间
    self.m_csbNode:getChildByName("Btn_MyRoom")
        :setTag(BT_MYROOM)
        :addTouchEventListener(btncallback)

    --复选按钮
    local cbtlistener = function (sender,eventType)
        self:onSelectedEvent(sender:getTag(), sender)
    end
    --扎码数量
    self.cbMaCount = 0

    local cbJuShu1 = self.m_csbNode:getChildByName("CB_JuShu_1")
    cbJuShu1:setTag(CBX_INNINGS_1)
    cbJuShu1:setSelected(false)
    cbJuShu1:addEventListener(cbtlistener)
    local cbJuShu2 = self.m_csbNode:getChildByName("CB_JuShu_2")
    cbJuShu2:setTag(CBX_INNINGS_2)
    cbJuShu2:setSelected(true)
    cbJuShu2:addEventListener(cbtlistener)
    local cbJuShu3 = self.m_csbNode:getChildByName("CB_JuShu_3")
    cbJuShu3:setVisible(false)
    cbJuShu3:setTag(CBX_INNINGS_3)
    cbJuShu3:setSelected(false)
    cbJuShu3:addEventListener(cbtlistener)

    local csSize = cbJuShu1:getContentSize()
    local cfglist = PriRoom:getInstance().m_tabFeeConfigList

    --创建对应的人数文本
    local label1 = cc.Label:createWithTTF(cfglist[1].dwDrawCountLimit .. "圈", RES_PATH.."fonts/fzzqhjt.TTF", 50)
    label1:setTextColor(cc.c4b(105, 59, 6, 255))
    label1:setAnchorPoint(cc.p(0.0, 0.5))
    label1:setPosition(csSize.width, csSize.height/2)
    cbJuShu1:addChild(label1)

    local labelsize = label1:getContentSize()

    local sp1 = display.newSprite("#cr_fangka.png")
    sp1:setAnchorPoint(cc.p(0.0, 0.5))
    sp1:setPosition(csSize.width + label1:getContentSize().width, csSize.height/2)
    cbJuShu1:addChild(sp1)

    local labelx1 = cc.Label:createWithTTF("X" .. cfglist[1].lFeeScore, RES_PATH.."fonts/fzzqhjt.TTF", 36)
    labelx1:setTextColor(cc.c4b(255, 0, 0, 255))
    labelx1:setAnchorPoint(cc.p(0.5, 0.5))
    labelx1:setPosition(csSize.width + label1:getContentSize().width + 80, csSize.height/2)
    cbJuShu1:addChild(labelx1)

    local label2 = cc.Label:createWithTTF(cfglist[2].dwDrawCountLimit .. "圈", RES_PATH.."fonts/fzzqhjt.TTF", 50)
    label2:setTextColor(cc.c4b(105, 59, 6, 255))
    label2:setAnchorPoint(cc.p(0.0, 0.5))
    label2:setPosition(csSize.width, csSize.height/2)
    cbJuShu2:addChild(label2)

    local sp2 = display.newSprite("#cr_fangka.png")
    sp2:setAnchorPoint(cc.p(0.0, 0.5))
    sp2:setPosition(csSize.width + label2:getContentSize().width, csSize.height/2)
    cbJuShu2:addChild(sp2)

    local labelx2 = cc.Label:createWithTTF("X" .. cfglist[2].lFeeScore, RES_PATH.."fonts/fzzqhjt.TTF", 36)
    labelx2:setTextColor(cc.c4b(255, 0, 0, 255))
    labelx2:setAnchorPoint(cc.p(0.5, 0.5))
    labelx2:setPosition(csSize.width + label2:getContentSize().width + 80, csSize.height/2)
    cbJuShu2:addChild(labelx2)

    local label3 = cc.Label:createWithTTF(cfglist[3].dwDrawCountLimit .. "圈", RES_PATH.."fonts/fzzqhjt.TTF", 50)
    label3:setTextColor(cc.c4b(105, 59, 6, 255))
    label3:setAnchorPoint(cc.p(0.0, 0.5))
    label3:setPosition(csSize.width, csSize.height/2)
    cbJuShu3:addChild(label3)

    local sp3 = display.newSprite("#cr_fangka.png")
    sp3:setAnchorPoint(cc.p(0.0, 0.5))
    sp3:setPosition(csSize.width + label3:getContentSize().width, csSize.height/2)
    cbJuShu3:addChild(sp3)

    local labelx3 = cc.Label:createWithTTF("X" .. cfglist[3].lFeeScore, RES_PATH.."fonts/fzzqhjt.TTF", 36)
    labelx3:setTextColor(cc.c4b(255, 0, 0, 255))
    labelx3:setAnchorPoint(cc.p(0.5, 0.5))
    labelx3:setPosition(csSize.width + label3:getContentSize().width + 80, csSize.height/2)
    cbJuShu3:addChild(labelx3)

    --房间规则
    for i = 1, 8 do
        local cbxInnings = self.m_csbNode:getChildByName("CB_WanFa_"..i)
        local visible = true
        cbxInnings:setVisible(visible)
        cbxInnings:setTag(CBX_RULE_1 + (i-1))
        cbxInnings:addEventListener(cbtlistener)
    end    

    self.m_csbNode:getChildByName("CB_RenShu_1")
        :setTag(CBX_USERNUM_4)
        :setSelected(true)
        :addEventListener(cbtlistener)
    self.m_csbNode:getChildByName("CB_RenShu_2")
        :setTag(CBX_USERNUM_3)
        :addEventListener(cbtlistener)
    --自己房卡数目
    self.textCardNum = self.m_csbNode:getChildByName("Text_myRoomCardNum"):setString(GlobalUserItem.lRoomCard .. "")

    local closeBtn = self.m_csbNode:getChildByName("Btn_Close")
    closeBtn:setTag(TAG_CLOSE_BTN)
    closeBtn:addTouchEventListener(btncallback)

    closeBtn:setVisible(self._clubNum == nil)

    --加载已有的选择配置
    --圈数
    local quanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_QUAN_CONFIG_INDEX", 2))

    for i = 1, 3 do
        local cbQuan = self.m_csbNode:getChildByName("CB_JuShu_" .. i)
        if cbQuan then
            if i == quanIndex then
                cbQuan:setSelected(true)
            else
                cbQuan:setSelected(false)
            end
        end
    end

    --人数
    local personIndex = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_PERSON_CONFIG_INDEX", 1))

    for i = 1, 2 do
        local cbRen = self.m_csbNode:getChildByName("CB_RenShu_" .. i)
        if cbRen then
            if i == personIndex then
                cbRen:setSelected(true)
            else
                cbRen:setSelected(false)
            end
        end
    end

    --玩法
    local rule1 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_1_SELECTED", 1))
    local rule2 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_2_SELECTED", 0))
    local rule3 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_3_SELECTED", 0))
    local rule4 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_4_SELECTED", 1))
    local rule5 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_5_SELECTED", 1))
    local rule6 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_6_SELECTED", 0))
    local rule7 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_7_SELECTED", 0))
    local rule8 = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_RULE_8_SELECTED", 0))

    for i = 1, 8 do
        local cbxInnings = self.m_csbNode:getChildByName("CB_WanFa_"..i)
        if cbxInnings then
            if i == 1 and rule1 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 2 and rule2 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 3 and rule3 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 4 and rule4 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 5 and rule5 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 6 and rule6 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 7 and rule7 == 1 then
                cbxInnings:setSelected(true)
            elseif i == 8 and rule8 == 1 then
                cbxInnings:setSelected(true)
            else
                cbxInnings:setSelected(false)
            end
        end
    end
end

function PriRoomCreateLayer:initData()
    local quanIndex = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_QUAN_CONFIG_INDEX", 2))
    local personIndex = tonumber(cc.UserDefault:getInstance():getStringForKey("HLD_PERSON_CONFIG_INDEX", 1))

    if personIndex == 2 then
        --人数
        self.cbUserNum = 3
    else
        --人数
        self.cbUserNum = 4
    end

    --房间配置
    self.m_tabSelectConfig = PriRoom:getInstance().m_tabFeeConfigList[quanIndex]
    self._configIndex = quanIndex

    local lMyTreasure = GlobalUserItem.lRoomCard

    if lMyTreasure < self.m_tabSelectConfig.lFeeScore then
        self.m_bLow = true
    else
        self.m_bLow = false
    end
end

------
-- 继承/覆盖
------
-- 刷新界面
function PriRoomCreateLayer:onRefreshInfo()
    -- 房卡数更新
    self.textCardNum:setString(GlobalUserItem.lRoomCard .. "")
end

function PriRoomCreateLayer:getCreateRoomBuffer()
    --友盟统计消耗房卡
    MultiPlatform:getInstance():calculateEvent(yl.UMENG_KEY.CAST_ROOM_CARD, self.m_tabSelectConfig.lFeeScore)
    if self.cbUserNum == 4 then
        cc.UserDefault:getInstance():setStringForKey("PERSON_CONFIG_INDEX", 1)
        --四人房
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.FOUR_PLAYER_ROOM)

    elseif self.cbUserNum == 3 then
        cc.UserDefault:getInstance():setStringForKey("PERSON_CONFIG_INDEX", 2)
        --三人房
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.THREE_PLAYER_ROOM)
    end

    if self._configIndex == 1 then
        cc.UserDefault:getInstance():setStringForKey("QUAN_CONFIG_INDEX", 1)
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.CREATE_QUAN_1)
    elseif self._configIndex == 2 then
        cc.UserDefault:getInstance():setStringForKey("QUAN_CONFIG_INDEX", 2)
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.CREATE_QUAN_2)
    elseif self._configIndex == 3 then
        cc.UserDefault:getInstance():setStringForKey("QUAN_CONFIG_INDEX", 3)
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.CREATE_QUAN_3)
    end
    
    -- 创建登陆
    local buffer = CCmd_Data:create(188)
    buffer:setcmdinfo(self._cmd_pri_game.MDM_GR_PERSONAL_TABLE,self._cmd_pri_game.SUB_GR_CREATE_TABLE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushdword(self._clubNum or 0)
    buffer:pushdword(self.m_tabSelectConfig.dwDrawCountLimit)
    buffer:pushdword(self.m_tabSelectConfig.dwDrawTimeLimit)
    buffer:pushword(0)
    buffer:pushdword(0)--24
    buffer:pushstring("", yl.LEN_PASSWORD) --66
    --单个游戏规则(额外规则)
    buffer:pushbyte(1)
    buffer:pushbyte(self.cbUserNum)         --人数必须在第2个位置

    local tianDi = self.m_csbNode:getChildByName("CB_WanFa_4"):isSelected()
    local huiPai = self.m_csbNode:getChildByName("CB_WanFa_1"):isSelected()
    local qiongHu = self.m_csbNode:getChildByName("CB_WanFa_3"):isSelected()
    local sanQing = self.m_csbNode:getChildByName("CB_WanFa_2"):isSelected()
    local fengDing = self.m_csbNode:getChildByName("CB_WanFa_5"):isSelected()
    local duihu = self.m_csbNode:getChildByName("CB_WanFa_6"):isSelected()
    local qingYiSe = self.m_csbNode:getChildByName("CB_WanFa_7"):isSelected()
    local fengDing200 = self.m_csbNode:getChildByName("CB_WanFa_8"):isSelected()

    if huiPai == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_1_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_1_SELECTED", 0)
    end

    if sanQing == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_2_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_2_SELECTED", 0)
    end

    if qiongHu == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_3_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_3_SELECTED", 0)
    end

    if tianDi == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_4_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_4_SELECTED", 0)
    end

    if fengDing == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_5_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_5_SELECTED", 0)
    end

    if duihu == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_6_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_6_SELECTED", 0)
    end

    if qingYiSe == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_7_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_7_SELECTED", 0)
    end

    if fengDing200 == true then
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_8_SELECTED", 1)
    else
        cc.UserDefault:getInstance():setStringForKey("HLD_RULE_8_SELECTED", 0)
    end

    print("qingYiSe = " .. tostring(qingYiSe))
    print("fengDing200 = " .. tostring(fengDing200))

    print("tianDi : " .. tostring(tianDi) .. " huiPai : " .. tostring(huiPai) .. " qiongHu : " .. tostring(qiongHu) .. " sanQing : " .. tostring(sanQing) .. " fengDing : " .. tostring(fengDing) .. " duihu : " .. tostring(duihu))
    print("fengDing200 : " .. tostring(fengDing200) .. " qingYiSe : " .. tostring(qingYiSe))
    PriRoom:getInstance().m_playRuleList["TIANDI"] = tianDi
    PriRoom:getInstance().m_playRuleList["HUIPAI"] = huiPai
    PriRoom:getInstance().m_playRuleList["QIONGHU"] = qiongHu
    PriRoom:getInstance().m_playRuleList["SANQING"] = sanQing
    PriRoom:getInstance().m_playRuleList["FENGDING"] = fengDing
    PriRoom:getInstance().m_playRuleList["DUIHU"] = duihu
    PriRoom:getInstance().m_playRuleList["QINGYISE"] = qingYiSe
    PriRoom:getInstance().m_playRuleList["FENGDING200"] = fengDing200


    buffer:pushbool(tianDi)
    buffer:pushbool(huiPai)
    buffer:pushbool(qiongHu)
    buffer:pushbool(fengDing)
    buffer:pushbool(sanQing)
    buffer:pushbool(duihu)
    buffer:pushbool(qingYiSe)
    buffer:pushbool(fengDing200)

    if tianDi then
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.RULE_TIANDIHU)
    end

    if huiPai then
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.RULE_HUI_PAI)
    end

    if qiongHu then
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.RULE_QIONGHU)
    end

    if fengDing then
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.RULE_FENGDING)
    end

    if sanQing then
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.RULE_SANQING)
    end

    if duihu then
        MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.RULE_QIDUI)
    end

    -- for i = 1, 100 - 9 do
    --     buffer:pushbyte(0)
    -- end
    return buffer
end

function PriRoomCreateLayer:getInviteShareMsg( roomDetailInfo )
        local wf = ""

        if PriRoom:getInstance().m_playRuleList["HUIPAI"] then
            if wf == "" then
                wf = wf .. "死飘"
            else
                wf = wf .. "、死飘"
            end    
        end

        if PriRoom:getInstance().m_playRuleList["SANQING"] then
            if wf == "" then
                wf = wf .. "三清"
            else
                wf = wf .. "、三清"
            end
        end

        if PriRoom:getInstance().m_playRuleList["QIONGHU"] then
            if wf == "" then
                wf = wf .. "穷胡"
            else
                wf = wf .. "、穷胡"
            end
        end

        if PriRoom:getInstance().m_playRuleList["TIANDI"] then
            if wf == "" then
                wf = wf .. "天地胡"
            else
                wf = wf .. "、天地胡"
            end
        end

        if PriRoom:getInstance().m_playRuleList["FENGDING"] then
            if wf == "" then
                wf = wf .. "封顶150"
            else
                wf = wf .. "、封顶150"
            end
        end

        if PriRoom:getInstance().m_playRuleList["DUIHU"] then
            if wf == "" then
                wf = wf .. "七对"
            else
                wf = wf .. "、七对"
            end
        end

        if PriRoom:getInstance().m_playRuleList["QINGYISE"] then
            if wf == "" then
                wf = wf .. "清一色"
            else
                wf = wf .. "、清一色"
            end
        end

        if PriRoom:getInstance().m_playRuleList["FENGDING200"] then
            if wf == "" then
                wf = wf .. "封顶200"
            else
                wf = wf .. "、封顶200"
            end
        end

        if wf ~= "" then
            wf = "，玩法：" .. wf
        end
        

    print("九九麻将-锦州约战 人数 = " .. roomDetailInfo.nChairCount)
    print("九九麻将-锦州约战 圈数 = " .. roomDetailInfo.dwPlayTurnCount)
    local shareTxt = "房间号:" .. roomDetailInfo.szRoomID .. " 圈数:" .. (roomDetailInfo.dwPlayTurnCount) .. " 人数:" .. roomDetailInfo.nChairCount .. wf
    local friendC = "房间号:" .. roomDetailInfo.szRoomID .. " 圈数:" .. roomDetailInfo.dwPlayTurnCount .. wf

    return {title = "九九麻将-锦州约战", content = shareTxt .. " 游戏精彩刺激, 一起来玩吧! ", friendContent = friendC}
end

------
-- 继承/覆盖
------

function PriRoomCreateLayer:onButtonClickedEvent(tag, sender)

    if BT_HELP == tag then
        print("帮助")
        self._scene:popHelpLayer(yl.HTTP_URL .. "/Mobile/Introduce.aspx?kindid=389&typeid=1")
    elseif BT_CREATE == tag then
        print("创建房间_tom")
        if self.m_bLow then
            local feeType = "房卡"
            local QueryDialog = appdf.req("client.src.plaza.views.layer.game.QueryDialog")
            local query = QueryDialog:create("您的" .. feeType .. "数量不足，请购买房卡！！"
                , function(ok)
            end, QueryDialog.DEF_TEXT_SIZE, QueryDialog.QUERY_SURE
            ):setCanTouchOutside(false)
                :addTo(self._scene, 12)
            return
        end
        if nil == self.m_tabSelectConfig or table.nums(self.m_tabSelectConfig) == 0 or self.cbUserNum == 0 then
            showToast(self, "未选择玩法配置!", 2)
            return
        end
        PriRoom:getInstance():showPopWait()
        local buffer = self:getCreateRoomBuffer()
        buffer:retain()
        PriRoom:getInstance():getNetFrame():createRoom(390, buffer)
    elseif BT_PAY == tag then

    elseif BT_MYROOM == tag then
        print("我的房间")
        self._scene:onChangeShowMode(PriRoom.LAYTAG.LAYER_MYROOMRECORD)
    elseif TAG_CLOSE_BTN == tag then
        PriRoom:getInstance():setViewFrame(nil)
        self._scene:onKeyBack()
    end
end

function PriRoomCreateLayer:onSelectedEvent(tag, sender)
    print("进", tag)
    if CBX_INNINGS_1 <= tag and tag <= CBX_INNINGS_3 then
        for i = CBX_INNINGS_1, CBX_INNINGS_3 do
            local checkBox = self.m_csbNode:getChildByTag(i)
            if i == tag then
                checkBox:setSelected(true)

                local lMyTreasure = GlobalUserItem.lRoomCard

                if checkBox:isSelected() then
                    local index = i - CBX_INNINGS_1 + 1
                    self.m_tabSelectConfig = PriRoom:getInstance().m_tabFeeConfigList[index]
                    self._configIndex = index
                    print("此房间需要 " .. self.m_tabSelectConfig.lFeeScore .. " 张房卡" .. "  我的房卡数 : " .. lMyTreasure)


                    if lMyTreasure < self.m_tabSelectConfig.lFeeScore then --房卡或游戏豆不足
                        self.m_bLow = true
                    else
                        self.m_bLow = false
                    end
                else
                    -- self.cbInningsCount = 0
                    -- self.m_tabSelectConfig = nil
                end
            else
                checkBox:setSelected(false)
            end
        end
        print("房间人数 : " .. self.cbUserNum .. " 圈数 : " .. self.m_tabSelectConfig.dwDrawCountLimit)
    elseif CBX_USERNUM_3 <= tag and tag <= CBX_USERNUM_4 then
        local checkBox_3 = self.m_csbNode:getChildByTag(CBX_USERNUM_3)
        local checkBox_4 = self.m_csbNode:getChildByTag(CBX_USERNUM_4)
        if tag == CBX_USERNUM_3 then
            self.cbUserNum = 3
            checkBox_4:setSelected(false)
            checkBox_3:setSelected(true)
        else
            self.cbUserNum = 4
            checkBox_3:setSelected(false)
            checkBox_4:setSelected(true)
        end
        print("房间人数 : " .. self.cbUserNum .. " 圈数 : " .. self.m_tabSelectConfig.dwDrawCountLimit)
    elseif CBX_RULE_1 <= tag and tag <= CBX_RULE_8 then
        print("click tag = " .. tag)
        if sender:isSelected() then
            if tag == CBX_RULE_5 or tag == CBX_RULE_8 then
                local cbxRulle5 = self.m_csbNode:getChildByName("CB_WanFa_5")
                local cbxRulle8 = self.m_csbNode:getChildByName("CB_WanFa_8")
                if tag == CBX_RULE_5 and cbxRulle8:isSelected()  then
                    cbxRulle8:setSelected(false)
                elseif tag == CBX_RULE_8 and cbxRulle5:isSelected()  then
                    cbxRulle5:setSelected(false)
                end
            end
        end
    end
    
end

return PriRoomCreateLayer