--
-- Author: zhong
-- Date: 2016-12-03 11:36:14
--
-- 私人房 命令
-- 登陆服务器: private_cmd.login
-- 游戏服务器: private_cmd.game

local private_struct = appdf.req(appdf.CLIENT_SRC .. "privatemode.header.Struct_Private")
local private_define = appdf.req(appdf.CLIENT_SRC .. "privatemode.header.Define_Private")

local private_cmd = {}
private_cmd.struct = private_struct
private_cmd.define = private_define

----------------------------------------------------------------------------------------------
-- 登陆服务器
local login = {}
------
-- 命令
------
-- 私人房间
login.MDM_MB_PERSONAL_SERVICE = 200

login.SUB_MB_QUERY_GAME_SERVER = 204                                    -- 创建房间
login.SUB_MB_QUERY_GAME_SERVER_RESULT = 205                             -- 创建结果
login.SUB_MB_SEARCH_SERVER_TABLE = 206                                  -- 搜索房间
login.SUB_MB_SEARCH_RESULT = 207                                        -- 搜索结果

login.SUB_MB_GET_PERSONAL_PARAMETER = 208                               -- 私人房间配置
login.SUB_MB_PERSONAL_PARAMETER = 209                                   -- 私人房间属性
login.SUB_MB_PERSONAL_FEE_PARAMETER = 212                               -- 私人房费用配置

login.SUB_MB_QUERY_PERSONAL_ROOM_LIST = 210                             -- 私人房列表
login.SUB_MB_QUERY_PERSONAL_ROOM_LIST_RESULT = 211                      -- 私人房列表

login.SUB_MB_DISSUME_SEARCH_SERVER_TABLE = 213                          -- 为解散桌子搜索ID
login.SUB_MB_DISSUME_SEARCH_RESULT = 214                                -- 解散时搜索私人房间返回结果

login.SUB_GR_USER_QUERY_ROOM_SCORE = 216                                -- 私人房间单个玩家请求房间成绩
login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT = 217                         -- 私人房间单个玩家请求房间成绩结果
login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT_FINSIH = 218                  -- 私人房间单个玩家请求房间成绩完成

login.SUB_MB_ROOM_CARD_EXCHANGE_TO_SCORE = 221                          -- 房卡兑换游戏币
login.SUB_GP_EXCHANGE_ROOM_CARD_RESULT = 222                            -- 房卡兑换游戏币结果

login.SUB_GR_USER_QUERY_DETAIL_SCORE = 223                              --私人房间单个玩家请求房间成绩结果

login.SUB_GR_GETREPLAY = 224                                            --请求战绩回放文件
login.SUB_GR_GETREPLAY_RESULT = 225                                     --请求战绩文件结果

login.SUB_GP_ROOMCARDUSERACTIVITY   = 226                               --通知服务器分享成功
login.SUB_GP_QUERY_USER_REWARD   = 227                                  --获取邀请奖励
login.SUB_MB_QUERY_USER_REWARD_RESULT   = 228                           --获取邀请奖励结果

login.SUB_GP_QUERY_USER_REWARD_CODE   = 229                             --获取领取奖励的码
login.SUB_MB_QUERY_USER_REWARD_CODE_RESULT   = 230                      --获取领取奖励的码

login.SUB_MB_UPDATE_ROOM_CARD   = 231                                   --获取当前房卡
login.SUB_MB_UPDATE_ROOM_CARD_RESULT   = 232                            --获取当前房卡结果

login.SUB_MB_LUCKY_DRAW = 233                                           --抽奖
login.SUB_MB_LUCKY_DRAW_RESULT = 234                                    --抽奖结果

login.SUB_MB_APPLY_JOIN_CLUB = 235 --申请加入俱乐部
login.SUB_MB_APPLY_JOIN_CLUB_RESULT = 236 --申请加入俱乐部结果

login.SUB_MB_QUERY_CLUB_VIP = 237 --查询俱乐部会员
login.SUB_MB_QUERY_CLUB_VIP_RESULT = 238 --查询俱乐部会员结果

login.SUB_MB_AGREE_APPLY_JOIN = 239 --通过俱乐部申请
login.SUB_MB_AGREE_JOIN_RESULT = 240 --通过俱乐部申请

login.SUB_MB_QUERY_CLUB_ROOM = 241  --查询创建俱乐部房间的玩家
login.SUB_MB_QUERY_CLUB_ROOM_RESULT = 242  --查询创建俱乐部房间的玩家结果

login.SUB_MB_MODIFY_CLUB_RULE = 243  --更改俱乐部规则
login.SUB_MB_MODIFY_CLUB_RULE_RESULT = 244  --更改俱乐部规则结果

login.SUB_MB_QUERY_CLUB_INFO = 245  --查看俱乐部信息
login.SUB_MB_QUERY_CLUB_INFO_RESULT = 246 --查看俱乐部信息结果



------
-- 消息结构
------
-- 创建房间
login.CMD_MB_QueryGameServer = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    -- 类型ID
    {t = "dword", k = "dwKindID"},
    -- 是否参与 (0 不参与; 1 参与)
    {t = "byte", k = "cbIsJoinGame"},
}

-- 创建结果
login.CMD_MB_QueryGameServerResult = 
{
    -- 房间ID
    {t = "dword", k = "dwServerID"},
    -- 是否可以创建房间
    {t = "bool", k = "bCanCreateRoom"},
    -- 错误描述
    {t = "string", k = "szErrDescrybe"}
}

-- 强制解散搜索房间
login.CMD_MB_SearchServerTable = 
{
    -- 房间ID
    {t = "tchar", k = "szServerID", s = private_define.ROOM_ID_LEN},
}

-- 进入游戏搜索房间
login.CMD_MB_SerchServerTableEnter = 
{
    -- 房间ID
    {t = "tchar", k = "szServerID", s = private_define.ROOM_ID_LEN},
    -- 类型ID
    {t = "dword", k = "dwKindID"},
}

-- 搜索结果
login.CMD_MB_SearchResult = 
{
    -- 房间ID
    {t = "dword", k = "dwServerID"},
    -- 桌子ID
    {t = "dword", k = "dwTableID"},
}

-- 查询私人房间配置
login.CMD_MB_GetPersonalParameter = 
{
    -- 类型ID
    {t = "dword", k = "dwKindID"},
}

-- 查询私人房列表
login.CMD_MB_QeuryPersonalRoomList = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    -- 类型ID
    {t = "dword", k = "dwKindID"},
}

--请求某一大局的小局列表
login.CMD_MB_QUERY_USER_DETAIL_SCORE = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    {t = "dword", k = "dwTableInfoID"},
}

-- 私人房间列表信息
login.CMD_MB_PersonalRoomInfoList = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    -- 配置信息
    {t = "table", k = "PersonalRoomInfo", d = private_struct.tagPersonalRoomInfo, l = {private_define.MAX_CREATE_PERSONAL_ROOM}}
}

-- 解散时搜索结果
login.CMD_MB_DissumeSearchResult = 
{
    -- 房间ID
    {t = "dword", k = "dwServerID"},
    -- 桌子ID
    {t = "dword", k = "dwTableID"},
}

-- 房卡兑换游戏币
login.CMD_GP_ExchangeScoreByRoomCard = 
{
    -- 用户标识
    {t = "dword", k = "dwUserID"},
    -- 房卡数量
    {t = "score", k = "lRoomCard"},
    -- machineid
    {t = "tchar", k = "szMachieID", s = yl.LEN_MACHINE_ID},
}

-- 房卡兑换游戏币结果
login.CMD_GP_ExchangeRoomCardResult = 
{
    -- 成功标识
    {t = "bool", k = "bSuccessed"},
    -- 当前游戏币
    {t = "score", k = "lCurrScore"},
    -- 当前房卡
    {t = "score", k = "lRoomCard"},
    -- 提示内容
    {t = "string", k = "szNotifyContent"}
}

--分享成功，发给服务器
login.CMD_GP_SharedSuccess = 
{
    {k = "bCircle", t = "bool"},
    {k = "cbLuckyIndex", t = "byte"},
    {k = "dwUserID", t = "dword"}
}

--获取分享奖励
login.CMD_GP_RoomCardUserActivity = 
{
    {k = "dwUserID", t = "dword"}
}

--分享获得的信息
login.tagUserRewardInfo = 
{
    {k = "bFinished", t = "bool"},
    {t = "string", k = "szNickName", s = yl.LEN_NICKNAME}, --
}

-- 获取分享奖励的结果
login.CMD_MB_UserRewardInfo = 
{
    -- 分享次数
    {t = "word", k = "wShareCount"},
    -- 受邀请结构体
    {t = "table", k = "rewardInfo", d = login.tagUserRewardInfo, l = {30}}
}

--发送领取奖励
login.CMD_GP_GetRewardCode = 
{
    {k = "dwUserID", t = "dword"}
}

--领取奖励的奖励码
login.CMD_GP_GetRewardCodeResult = 
{
    {k = "dwRewardCode", t = "dword"}
}

--请求战绩回放文件
login.CMD_GP_GetReplay = 
{
    {t = "word", k = "wGameRound"}, --一大圈的第几局
    {t = "dword", k = "dwFlagID"}, --查大圈信息中的文件标识
    {t = "dword", k = "dwRoomCardID"}, --房卡号
    {t = "dword", k = "dwServerID"} --服务器serverid
}

--请求当前房卡
login.CMD_GP_GetRoomCardUserActivity = 
{
    {k = "dwUserID", t = "dword"}
}
--请求当前房卡结果
login.CMD_GP_GetRoomCardUserActivityResult = 
{
    {k = "nRoomCard", t = "int"}
}

--请求抽奖
login.CMD_GP_LuckyDraw = 
{
    {k = "dwUserID", t = "dword"}, --用户id
    {t = "tchar", k = "szPassword", s = yl.LEN_PASSWORD}, ---密码
}

--抽奖结果
login.CMD_GP_LuckyDrawResult = 
{
    {t = "byte", k = "cbLuckyIndex"}, --抽中的索引
    {t = "byte", k = "cbRealIndex"}, --抽中的索引
    {t = "double", k = "dLuckyValue"}, --现金价值
    {t = "score", k = "lLoveLiness"}, --抽奖后的魅力值
}

login.CMD_GP_ApplyJoinClub = 
{
    {t = "dword", k = "dwUserID"}, --自己的userID
    {t = "dword", k = "dwClubNum"} --俱乐部号
}

login.CMD_GP_ApplyClubResult = 
{
    {t = "int", k = "iReturnCode"},  --错误代码
    {t = "string", k = "szClubName", s = 32}, --描述
    {t = "string", k = "szDescribeString", s = 128}, --描述
}

login.CMD_GP_QueryClubVip = 
{
    {t = "dword", k = "dwUserID"}, --自己的userID
}

login.DBO_GP_QueryClubResult =  --585 73 + 512
{
    {t = "bool", k = "bApply"},     --申请状态                                  1
    {t = "dword", k = "dwClubID"},  --俱乐部ID                                  4
    {t = "dword", k = "dwGameID"},  --自己的游戏ID 6位数的                        4
    {t = "dword", k = "dwUserID"},  --自己的游戏ID 6位数的                        4
    {t = "string", k = "szNickName", s = yl.LEN_NICKNAME}, --昵称               32
    {t = "string", k = "szWeChatImgURL", s = yl.LEN_USER_NOTE}, --昵称          256
}

login.CMD_GP_AgreeApplyJoin = 
{
    {t = "bool", k = "bJoinOrCut"},                         --通过还是删除
    {t = "dword", k = "dwManagerID"},                       --房主ID
    {t = "dword", k = "dwApplyUserID"},                     --申请者ID
    {t = "dword", k = "dwClubNum"},                         --俱乐部号
    {t = "tchar", k = "szPassword", s = yl.LEN_PASSWORD}    --俱乐部密码
}

login.DBO_GR_AgreeJoinResult = 
{
    {t = "int", k = "iReturnCode"},  --错误代码
}

login.CMD_GP_QueryClubRoom = 
{
    {t = "bool", k = "bDeleteRecord"},                       --是否是删除创建记录
    {t = "dword", k = "dwClubNum"}                           --俱乐部号
}



login.CMD_GP_QueryClubRoomResult = 
{
    {t = "dword", k = "dwGameID"},                                          --用户ID 4
    {t = "dword", k = "dwCreateTableFee"},                                  --消耗房卡 4
    {t = "dword", k = "dwRoomID"},                                          --房间号
    {t = "dword", k = "dwFileTag"},                                          --回放文件标识
    {t = "dword", k = "dwTableInfoID"},                                      --用于查看回放列表
    {t = "table", k = "sysCreateTime", d = private_struct.SYSTEMTIME},      --创建时间 8
    {t = "string", k = "szNickName", s = 10},                   --昵称 32*2
    {t = "table", k = "clubListRoom", d = private_struct.tagClubListRoom, l = {4}}
}

login.CMD_GP_ModifyClubRule = 
{
    --房主ID
    {t = "dword", k = "dwManagerID"},
    --俱乐部号
    {t = "dword", k = "dwClubNum"},
    -- 密码设置
    {t = "tchar", k = "szPassword", s = yl.LEN_MD5},
    -- 游戏规则，0位标识，0表示未设置，1表示设置设置，后面+自定义规则数据
    {t = "byte", k = "cbGameRule", l = private_define.RULE_LEN},
}

login.CMD_GP_ModifyClubRuleResult = 
{
    {t = "int", k = "iReturnCode"},  --错误代码
}

login.CMD_GP_QueryClubInfo = 
{
    {t = "dword", k = "dwUserID"},
}

login.CMD_GP_QueryClubInfoResult = 
{
    {t = "bool", k = "bInfoKind"}, --0 自己的俱乐部 1 自己加入的俱乐部
    {t = "bool", k = "bServiceing"},--服务状态 关闭，打开
    {t = "bool", k = "bUserJoining"},--是否有玩家加入
    {t = "word", k = "wKindID"}, --游戏种类
    {t = "dword", k = "dwClubNum"},--俱乐部号
    {t = "dword", k = "dwClubGameRule"},--规则
    {t = "string", k = "szNickName", s = yl.LEN_NICKNAME}, --昵称
    {t = "string", k = "szDescribeString", s = yl.LEN_DWELLING_PLACE}, --描述
    {t = "string", k = "szWeChatImgURL", s = yl.LEN_USER_NOTE}, --群主头像
}


private_cmd.login = login
-- 登陆服务器
----------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------
-- 游戏服务器
local game = {}
------
-- 命令
------
-- 私人房间
game.MDM_GR_PERSONAL_TABLE = 210                                        -- 

game.SUB_GR_CREATE_TABLE = 1                                            -- 创建桌子
game.SUB_GR_CREATE_SUCCESS = 2                                          -- 创建成功
game.SUB_GR_CREATE_FAILURE = 3                                          -- 创建失败
game.SUB_GR_CANCEL_TABLE = 4                                            -- 解散桌子
game.SUB_GR_CANCEL_REQUEST = 5                                          -- 请求解散
game.SUB_GR_REQUEST_REPLY = 6                                           -- 请求答复
game.SUB_GR_REQUEST_RESULT = 7                                          -- 请求结果
game.SUB_GR_WAIT_OVER_TIME = 8                                          -- 超时等待
game.SUB_GR_PERSONAL_TABLE_TIP = 9                                      -- 提示信息
game.SUB_GR_PERSONAL_TABLE_END = 10                                     -- 结束消息
game.SUB_GR_HOSTL_DISSUME_TABLE = 11                                    -- 房主强制解散桌子
game.SUB_GR_OFFLINE_SEND_ROOMID = 12                                    --发送房间id，用于下次链接
game.SUB_GR_CANCEL_TABLE_RESULT = 13                                    -- 强制解散结果

game.SUB_GP_QUERY_USER_TABLE_INFO = 14                                  --请求桌子具体信息
game.SUB_GP_QUERY_USER_TABLE_INFO_RESULT = 15                           --请求桌子具体信息结果

game.SUB_GR_CURRECE_ROOMCARD_AND_BEAN = 16                              -- 强制解散桌子后的游戏豆和房卡数量
game.SUB_GR_CHANGE_CHAIR_COUNT = 17                                     -- 改变椅子数量

game.SUB_GR_SHAREREPLAY = 19                                            --通过回放码查看回放




game.SUB_GF_PERSONAL_MESSAGE = 202                                      -- 私人房消息

game.CANCELTABLE_REASON_PLAYER = 0                                      -- 玩家取消
game.CANCELTABLE_REASON_SYSTEM = 1                                      -- 系统取消
game.CANCELTABLE_REASON_ENFOCE = 2                                      -- 强制解散桌子
game.CANCELTABLE_REASON_ERROR = 3                                       -- 错误取消

game.SUB_GR_GetReplay = 17                                              --请求回放
game.SUB_GP_GetReplay_Result = 18                                       --请求回放结果

--请求战绩回放文件
game.CMD_GP_GetReplay = 
{
    {t = "word", k = "wGameRound"}, --一大圈的第几局
    {t = "dword", k = "dwFlagID"}, --查大圈信息中的文件标识
    {t = "dword", k = "dwRoomCardID"}, --房卡号
    {t = "dword", k = "dwServerID"} --服务器serverid
}

------
-- 消息结构
------
-- 创建桌子
game.CMD_GR_CreateTable = 
{
    -- 底分设置
    {t = "score", k = "lCellScore"},
    -- 局数限制
    {t = "dword", k = "dwDrawCountLimit"},
    -- 时间限制
    {t = "dword", k = "dwDrawTimeLimit"},
    -- 参与游戏的人数
    {t = "word", k = "wJoinGamePeopleCount"},
    -- 单独税率
    {t = "dword", k = "dwRoomTax"},
    -- 密码设置
    {t = "tchar", k = "szPassword", s = yl.LEN_PASSWORD},
    -- 游戏规则，0位标识，0表示未设置，1表示设置设置，后面+自定义规则数据
    {t = "byte", k = "cbGameRule", l = private_define.RULE_LEN},
}

-- 创建成功
game.CMD_GR_CreateSuccess = 
{
    -- 房间编号
    {t = "string", k = "szServerID", s = private_define.ROOM_ID_LEN},
    -- 局数限制
    {t = "dword", k = "dwDrawCountLimit"},
    -- 时间限制
    {t = "dword", k = "dwDrawTimeLimit"},
    -- 游戏豆
    {t = "double", k = "dBeans"},
    -- 房卡数量
    {t = "score", k = "lRoomCard"},
    --该房间人数
    {t = "word", k = "wChairCount"},
    --规则，移位运算
    {t = "score", k = "lGameRule"},
}

-- 创建失败
game.CMD_GR_CreateFailure = 
{
    -- 错误代码
    {t = "int", k = "lErrorCode"},
    -- 描述消息
    {t = "string", k = "szDescribeString"},
}

-- 取消桌子
game.CMD_GR_CancelTable = 
{
    -- 取消原因
    {t = "dword", k = "dwReason"},
    -- 描述消息
    {t = "string", k = "szDescribeString"--[[, s = 128]]},
}

-- 请求解散
game.CMD_GR_CancelRequest = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    -- 桌子ID
    {t = "dword", k = "dwTableID"},
    -- 椅子ID
    {t = "dword", k = "dwChairID"},
}

-- 请求答复
game.CMD_GR_RequestReply = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    -- 桌子ID
    {t = "dword", k = "dwTableID"},
    -- 用户答复(1 同意; 0 不同意)
    {t = "byte", k = "cbAgree"},
}

-- 请求结果
game.CMD_GR_RequestResult = 
{
    -- 桌子ID
    {t = "dword", k = "dwTableID"},
    -- 请求结果
    {t = "byte", k = "cbResult"},
}

-- 超时等待
game.CMD_GR_WaitOverTime = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
}

-- 提示信息
game.CMD_GR_PersonalTableTip = 
{
    -- 桌主ID
    {t = "dword", k = "dwTableOwnerUserID"},
    -- 局数限制
    {t = "dword", k = "dwDrawCountLimit"},
    -- 时间限制
    {t = "dword", k = "dwDrawTimeLimit"},
    -- 已玩局数
    {t = "dword", k = "dwPlayCount"},
    -- 已玩时间
    {t = "dword", k = "dwPlayTime"},
    -- 桌子上的总人数
    {t = "word",  k = "wChairCount"},
    -- 游戏底分
    {t = "score", k = "lCellScore"},
    -- 初始分数
    {t = "score", k = "lIniScore"},
    -- 房间编号
    {t = "string", k = "szServerID", s = private_define.ROOM_ID_LEN},
    -- 是否参与游戏
    {t = "byte", k = "cbIsJoinGame"},
    -- 金币场0, 积分场1
    {t = "byte", k = "cbIsGoldOrGameScore"},
}

-- 结束消息
game.CMD_GR_PersonalTableEnd = 
{
    {t = "string", k = "szDescribeString", s = 128},
    {t = "score", k = "lScore", l = {100}},
    -- 特殊信息长度 
    {t = "int", k = "nSpecialInfoLen"},
    -- 特殊信息数据
    --{t = "byte", k = "cbSpecialInfo", l = {200}}
}

--请求桌子具体信息
game.CMD_GR_TableUserInfo = 
{
    {t = "bool", k = "bClubQuery"},         --是否为俱乐部请求
    {t = "dword", k = "dwRoomCardID"}       --如果是俱乐部请求，那就传俱乐部号，如果不是，则传房间号
}

-- 房主强制解散
game.CMD_GR_HostDissumeGame = 
{
    -- 用户ID
    {t = "dword", k = "dwUserID"},
    -- 桌子ID
    {t = "dword", k = "dwTableID"},
}

-- 解散桌子
game.CMD_GR_DissumeTable = 
{
    -- 是否解散成功
    {t = "byte", k = "cbIsDissumSuccess"},
    -- 桌子ID
    {t = "string", k = "szRoomID", s = private_define.ROOM_ID_LEN},
    -- 解散时间
    {t = "table", k = "sysDissumeTime", d = private_struct.SYSTEMTIME},
    -- 用户信息
    {t = "table", k = "PersonalUserScoreInfo", d = private_struct.tagPersonalUserScoreInfo, l = {private_define.PERSONAL_ROOM_CHAIR}}
}

game.tagUserName = 
{
    {t = "string", k = "szNickName", s = yl.LEN_NICKNAME}
}

game.CMD_GR_User_Info_Result = 
{
    --当前状态
    {t = "byte", k = "cbGameStatus"},
    --当前圈/局数
    {t = "byte", k = "cbGameCount"},
    --总圈/局数
    {t = "byte", k = "cbAllCount"},
    --玩家人数
    {t = "byte", k = "cbUserCount"},
    --房间ID
    {t = "dword", k = "dwRoomCardId"},
    --玩法
    {t = "byte", k = "cbGameRule", l = {100} },
    --玩家ID
    {t = "dword", k = "dwGameID", l = {7} },
    --玩家名称
    {t = "string", k = "szNickName", s = yl.LEN_NICKNAME * 7}
}

-- 私人房消息
game.Personal_Room_Message = 
{
    -- 提示信息
    {t = "string", k = "szMessage", s = 260},
    -- 信息类型,暂时无用
    {t = "byte", k = "cbMessageType"},
}

--所在的房间
game.Personal_Room_ID = 
{
    {t = "string",   k = "szRoomID", s = private_define.ROOM_ID_LEN},                         --邀请信息
}

-- 强制解散桌子后的游戏豆和房卡
game.CMD_GR_CurrenceRoomCardAndBeans = 
{
    -- 游戏豆
    {t = "double", k = "dbBeans"},
    -- 房卡
    {t = "score", k = "lRoomCard"},
}

-- 改变椅子数量
game.CMD_GR_ChangeChairCount = 
{
    -- 椅子数量
    {t = "dword", k = "dwChairCount"},
}

game.CMD_GP_ShareReplay = 
{
    {t = "dword", k = "dwShareID"},                                                         --分享码
}

private_cmd.game = game
-- 游戏服务器
----------------------------------------------------------------------------------------------
return private_cmd