--
-- Author: zhong
-- Date: 2016-12-28 16:16:34
--
-- 私人房网络处理
local BaseFrame = appdf.req(appdf.CLIENT_SRC.."plaza.models.BaseFrame")
local PriFrame = class("PriFrame",BaseFrame)
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local cmd_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "CMD_Private")
local define_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "Define_Private")
local struct_private = appdf.req(PriRoom.MODULE.PRIHEADER .. "Struct_Private")

-- 登陆服务器CMD
local cmd_pri_login = cmd_private.login
-- 游戏服务器CMD
local cmd_pri_game = cmd_private.game

PriFrame.OP_CREATEROOM = cmd_pri_login.SUB_MB_QUERY_GAME_SERVER                 -- 创建房间
PriFrame.OP_SEARCHROOM = cmd_pri_login.SUB_MB_SEARCH_SERVER_TABLE               -- 查询房间
PriFrame.OP_ROOMPARAM = cmd_pri_login.SUB_MB_GET_PERSONAL_PARAMETER             -- 私人房配置
PriFrame.OP_QUERYLIST = cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST           -- 私人房列表
PriFrame.OP_DISSUMEROOM = cmd_pri_login.SUB_MB_DISSUME_SEARCH_SERVER_TABLE      -- 解散桌子
PriFrame.OP_QUERYJOINLIST = cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE          -- 查询参与列表
PriFrame.OP_EXCHANGEROOMCARD = cmd_pri_login.SUB_MB_ROOM_CARD_EXCHANGE_TO_SCORE -- 房卡兑换游戏币
PriFrame.OP_QUERYSUBSCORELIST =  cmd_pri_login.SUB_GR_USER_QUERY_DETAIL_SCORE   --请求某一个大局的所有小局信息
PriFrame.OP_QUERY_USER_REWARD = cmd_pri_login.SUB_GP_QUERY_USER_REWARD          --请求邀请奖励

PriFrame.OP_GETREPLAY = cmd_pri_login.SUB_GR_GETREPLAY                          --请求重放文件
PriFrame.OP_GETROOMCARD = cmd_pri_login.SUB_MB_UPDATE_ROOM_CARD                 --请求当前房卡

PriFrame.OP_LUCKY_DRAW = cmd_pri_login.SUB_MB_LUCKY_DRAW                        --抽奖

PriFrame.OP_QUERY_USER_REWARD_CODE = cmd_pri_login.SUB_GP_QUERY_USER_REWARD_CODE--请求邀请奖励

PriFrame.OP_APPLY_JOIN_CLUB = cmd_pri_login.SUB_MB_APPLY_JOIN_CLUB              --申请加入俱乐部
PriFrame.OP_APPLY_JOIN_CLUB_RESULT = cmd_pri_login.SUB_MB_APPLY_JOIN_CLUB_RESULT--申请加入俱乐部结果

PriFrame.OP_QUERY_CLUB_VIP = cmd_pri_login.SUB_MB_QUERY_CLUB_VIP                --查询俱乐部会员
PriFrame.OP_QUERY_CLUB_VIP_RESULT = cmd_pri_login.SUB_MB_QUERY_CLUB_VIP_RESULT  --查询俱乐部会员结果

PriFrame.OP_AGREE_APPLY_JOIN = cmd_pri_login.SUB_MB_AGREE_APPLY_JOIN

PriFrame.OP_QUERY_CLUB_ROOM = cmd_pri_login.SUB_MB_QUERY_CLUB_ROOM --查询创建俱乐部房间的玩家
PriFrame.OP_QUERY_CLUB_ROOM_RESULT = cmd_pri_login.SUB_MB_QUERY_CLUB_ROOM_RESULT --查询创建俱乐部房间的玩家结果

PriFrame.OP_MODIFY_CLUB_RULE = cmd_pri_login.SUB_MB_MODIFY_CLUB_RULE                --更改俱乐部规则
PriFrame.OP_MODIFY_CLUB_RULE_RESULT = cmd_pri_login.SUB_MB_MODIFY_CLUB_RULE_RESULT     --更改俱乐部规则结果

PriFrame.OP_QUERY_CLUB_INFO = cmd_pri_login.SUB_MB_QUERY_CLUB_INFO                --查询现有俱乐部
PriFrame.OP_QUERY_CLUB_INFO_RESULT = cmd_pri_login.SUB_MB_QUERY_CLUB_INFO_RESULT     --查询现有俱乐部结果

function LOGINSERVER(code)
    return { m = cmd_pri_login.MDM_MB_PERSONAL_SERVICE, s = code }
end
function GAMESERVER(code)
    return { m = cmd_pri_game.MDM_GR_PERSONAL_TABLE, s = code }
end

function PriFrame:ctor(view,callbcak)
    PriFrame.super.ctor(self,view,callbcak)
end

-- 创建房间
function PriFrame:onCreateRoom()
    --操作记录
    self._oprateCode = PriFrame.OP_CREATEROOM
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

-- 查询房间
function PriFrame:onSearchRoom( roomId )
    PriRoom:getInstance().m_currentSearchRoomID = roomId
    --操作记录
    self._oprateCode = PriFrame.OP_SEARCHROOM
    self._roomId = roomId or ""
    print("搜索房间 roomId = -" .. roomId .. "-")
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

-- 私人房间配置
function PriFrame:onGetRoomParameter()
    PriRoom:getInstance():showPopWait()
    --操作记录
    self._oprateCode = PriFrame.OP_ROOMPARAM
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

-- 查询私人房列表
function PriFrame:onQueryRoomList()
    --操作记录
    self._oprateCode = PriFrame.OP_QUERYLIST
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

-- 解散房间
function PriFrame:onDissumeRoom( roomId )
    --操作记录
    self._oprateCode = PriFrame.OP_DISSUMEROOM
    self._roomId = roomId or ""
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

-- 查询参与列表
function PriFrame:onQueryJoinList()
    --操作记录
    self._oprateCode = PriFrame.OP_QUERYJOINLIST
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

function PriFrame:onQuerySubScoreList(tableInfoID)
    self._tableInfoID = tableInfoID
    self._oprateCode = PriFrame.OP_QUERYSUBSCORELIST
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

-- 房卡兑换游戏币
function PriFrame:onExchangeScore( lCount )
    --操作记录
    self._oprateCode = PriFrame.OP_EXCHANGEROOMCARD
    self._lExchangeRoomCard = lCount
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"建立连接失败！")
    end
end

--连接结果
function PriFrame:onConnectCompeleted()
    print("================== PriFrame:onConnectCompeleted ================== ==> " .. self._oprateCode)
    if self._oprateCode == PriFrame.OP_CREATEROOM then              -- 创建房间
        self:sendCreateRoom()
    elseif self._oprateCode == PriFrame.OP_SEARCHROOM then          -- 查询房间
        self:sendSearchRoom()
    elseif self._oprateCode == PriFrame.OP_ROOMPARAM then           -- 私人房配置
        self:sendQueryRoomParam()
    elseif self._oprateCode == PriFrame.OP_QUERYLIST then           -- 请求私人房列表
        self:sendQueryRoomList()
    elseif self._oprateCode == PriFrame.OP_DISSUMEROOM then         -- 解散桌子
        self:sendDissumeRoom()
    elseif self._oprateCode == PriFrame.OP_QUERYJOINLIST then       -- 参与列表
        self:sendQueryJoinRoomList()
    elseif self._oprateCode == PriFrame.OP_EXCHANGEROOMCARD then    -- 房卡兑换游戏币
        self:sendExchangeScore()
    elseif self._oprateCode == PriFrame.OP_QUERYSUBSCORELIST then
        self:sendQuerySubScoreList()
    elseif self._oprateCode == PriFrame.OP_QUERY_USER_REWARD then
        self:sendQureyUserReward()
    elseif self._oprateCode == PriFrame.OP_QUERY_USER_REWARD_CODE then
        self:sendQureyUserRewardCode()
    elseif self._oprateCode == PriFrame.OP_GETREPLAY then
        self:sendGetReplayFile()
    elseif self._oprateCode == PriFrame.OP_GETROOMCARD then
        self:sendGetRoomCard()
    elseif self._oprateCode == PriFrame.OP_LUCKY_DRAW then
        self:sendLuckyDraw()
    elseif self._oprateCode == PriFrame.OP_APPLY_JOIN_CLUB then
        self:sendApplyJoinClub()
    elseif self._oprateCode == PriFrame.OP_QUERY_CLUB_VIP then
        self:sendQueryClubVip()
    elseif self._oprateCode == PriFrame.OP_AGREE_APPLY_JOIN then
        self:sendAgreeApplyJoin()
    elseif self._oprateCode == PriFrame.OP_QUERY_CLUB_ROOM then
        self:sendQueryClubRoom()
    elseif self._oprateCode == PriFrame.OP_MODIFY_CLUB_RULE then
        self:sendModifyClubRule()
    elseif self._oprateCode == PriFrame.OP_QUERY_CLUB_INFO then
        self:sendQueryClunInfo()
    else
        self:onCloseSocket()
        if nil ~= self._callBack then
            self._callBack(LOGINSERVER(-1),"未知操作模式！")
        end 
        PriRoom:getInstance():dismissPopWait()
    end
end

-- 发送创建房间
function PriFrame:sendCreateRoom()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QueryGameServer)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_QUERY_GAME_SERVER)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushdword(GlobalUserItem.nCurGameKind)
    local roomOption = GlobalUserItem.getRoomOptionByKindId(GlobalUserItem.nCurGameKind)
    buffer:pushbyte(roomOption.cbIsJoinGame)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送创建房间失败！")
    end
end

-- 发送查询私人房
function PriFrame:sendSearchRoom()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_SerchServerTableEnter)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_SEARCH_SERVER_TABLE)
    buffer:pushstring(self._roomId, define_private.ROOM_ID_LEN)
    buffer:pushdword(GlobalUserItem.nCurGameKind)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送查询房间失败！")
    end
end

-- 发送请求配置
function PriFrame:sendQueryRoomParam()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_GetPersonalParameter)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_GET_PERSONAL_PARAMETER)
    buffer:pushdword(GlobalUserItem.nCurGameKind)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        -- self._callBack(LOGINSERVER(-1),"发送请求配置失败！")
        PriRoom:getInstance():dismissPopWait()
    end
end

-- 发送查询私人房列表
function PriFrame:sendQueryRoomList()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QeuryPersonalRoomList)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushdword(GlobalUserItem.nCurGameKind)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送查询房间列表失败！")
    end
end

-- 发送解散房间
function PriFrame:sendDissumeRoom()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_SearchServerTable)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_DISSUME_SEARCH_SERVER_TABLE)
    buffer:pushstring(self._roomId, define_private.ROOM_ID_LEN)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送解散房间失败！")
    end
end

-- 发送查询参与列表
function PriFrame:sendQueryJoinRoomList()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QeuryPersonalRoomList)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    print("GlobalUserItem.nCurGameKind = " .. GlobalUserItem.nCurGameKind)
    buffer:pushdword(GlobalUserItem.nCurGameKind)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送查询参与列表失败！")
    end
end

--发送查询某一个大局的所有小局列表
function PriFrame:sendQuerySubScoreList()
    if not self._tableInfoID then
        print("无桌子id，无法查询 ====================== 》》》》》")
        return
    end
    print("开始请求大局的某一小局的数据==================== 》》》》》")
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_MB_QUERY_USER_DETAIL_SCORE)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_GR_USER_QUERY_DETAIL_SCORE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushdword(self._tableInfoID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送查询参与列表失败！")
    end
end

-- 发送房卡兑换游戏币
function PriFrame:sendExchangeScore()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_ExchangeScoreByRoomCard)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_ROOM_CARD_EXCHANGE_TO_SCORE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushscore(self._lExchangeRoomCard)
    buffer:pushstring(GlobalUserItem.szMachine,yl.LEN_MACHINE_ID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"发送兑换游戏币失败！")
    end
end

function PriFrame:createRoom(kindId, buffer)
    print("PriFrame createRoom = ", self._gameFrame, self._gameFrame:isSocketServer())
    if nil ~= self._gameFrame then
        if not self._gameFrame:createRoom(kindId, buffer) then
            self._callBack(GAMESERVER(-1),"发送创建房间失败")
        end
    end
end

-- 发送游戏服务器消息
function PriFrame:sendGameServerMsg( buffer )    
    if nil ~= self._gameFrame and self._gameFrame:isSocketServer() then
        if not self._gameFrame:sendSocketData(buffer) then
            self._callBack(GAMESERVER(-1),"发送解散游戏失败！")
        end
    end
end

-- 发送进入私人房
function PriFrame:sendEnterPrivateGame()
    if nil ~= self._gameFrame and self._gameFrame:isSocketServer() then
        local c = yl.INVALID_TABLE,yl.INVALID_CHAIR
        -- 找座椅
        local chaircount = self._gameFrame._wChairCount
        for i = 1, chaircount  do
            local sc = i - 1
            if nil == self._gameFrame:getTableUserItem(PriRoom:getInstance().m_dwTableID, sc) then
                c = sc
                break
            end
        end
        print( "PriFrame:sendEnterPrivateGame ==> private enter " .. PriRoom:getInstance().m_dwTableID .. " ## " .. c)

        self._gameFrame:SitDown(PriRoom:getInstance().m_dwTableID, c)
        self._gameFrame:SendGameOption()
    end
end

function PriFrame:queryTableInfo(tableId, bClubQuery)
    -- 信息记录
    PriRoom:getInstance().m_dwTableID = tableId
    PriRoom:getInstance().m_bClubQuery = bClubQuery or false

    if not bClubQuery then
        -- 发送登陆 取前两位 - 10，如房号8612343 则server id = 86 -10 = 76
        local serverID = tonumber(string.sub(tostring(tableId), 1, 2)) - 10
        self._gameFrame:queryTableInfo(serverID)
    else
        local serverID = GlobalUserItem.getServerIdByKindId(389)
        if serverID ~= nil then
            self._gameFrame:queryTableInfo(serverID)
        end
    end
end

-- 强制解散游戏
function PriFrame:sendDissumeGame( tableId )
    tableId = tableId or 0
    if nil ~= self._gameFrame and self._gameFrame:isSocketServer() then
        local buffer = ExternalFun.create_netdata(cmd_pri_game.CMD_GR_HostDissumeGame)
        buffer:setcmdinfo(cmd_pri_game.MDM_GR_PERSONAL_TABLE,cmd_pri_game.SUB_GR_HOSTL_DISSUME_TABLE)
        buffer:pushdword(GlobalUserItem.dwUserID)
        buffer:pushdword(tableId)
        if not self._gameFrame:sendSocketData(buffer) then
            self._callBack(GAMESERVER(-1),"发送解散游戏失败！")
        end
    end
end

--  请求解散游戏
function PriFrame:sendRequestDissumeGame()
    if nil ~= self._gameFrame and self._gameFrame:isSocketServer() then
        print("game socket sendRequestDissumeGame")
        local buffer = ExternalFun.create_netdata(cmd_pri_game.CMD_GR_CancelRequest)
        buffer:setcmdinfo(cmd_pri_game.MDM_GR_PERSONAL_TABLE,cmd_pri_game.SUB_GR_CANCEL_REQUEST)
        buffer:pushdword(GlobalUserItem.dwUserID)
        buffer:pushdword(self._gameFrame:GetTableID())
        buffer:pushdword(self._gameFrame:GetChairID())
        if not self._gameFrame:sendSocketData(buffer) then
            self._callBack(GAMESERVER(-1),"请求解散游戏失败！")
        end
    end
end

-- 回复请求
function PriFrame:sendRequestReply( cbAgree )
    if nil ~= self._gameFrame and self._gameFrame:isSocketServer() then
        local buffer = ExternalFun.create_netdata(cmd_pri_game.CMD_GR_RequestReply)
        buffer:setcmdinfo(cmd_pri_game.MDM_GR_PERSONAL_TABLE,cmd_pri_game.SUB_GR_REQUEST_REPLY)
        buffer:pushdword(GlobalUserItem.dwUserID)
        buffer:pushdword(self._gameFrame:GetTableID())
        buffer:pushbyte(cbAgree)
        if not self._gameFrame:sendSocketData(buffer) then
            self._callBack(GAMESERVER(-1),"回复请求失败！")
        end
    end
end

-- 请求分享的战绩
function PriFrame:onGetShareReplay(shareID)
    if nil ~= self._gameFrame then
        if not self._gameFrame:onGetShareReplay(shareID) then
            self._callBack(GAMESERVER(-1),"获取回放信息失败")
        end
    end
end

--网络信息(短连接)
function PriFrame:onSocketEvent( main,sub,pData )
    print("PriFrame:onSocketEvent ==> " .. main .. "##" .. sub)
    local needClose = true
    local dissmissPop = true
    if cmd_pri_login.MDM_MB_PERSONAL_SERVICE == main then
        if cmd_pri_login.SUB_MB_SEARCH_RESULT == sub then                           -- 房间搜索结果
            self:onSubSearchRoomResult(pData)
        elseif cmd_pri_login.SUB_MB_DISSUME_SEARCH_RESULT == sub then               -- 解散搜索结果
            self:onSubDissumeSearchReasult(pData)
        elseif cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST_RESULT == sub then     -- 私人房列表
            self:onSubPrivateRoomList(pData)
        elseif cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT == sub then        -- 参与列表
            self:onSubJoinRoomList(pData)
        elseif cmd_pri_login.SUB_MB_PERSONAL_PARAMETER == sub then                  -- 私人房间属性
            print("收到 ----------- SUB_MB_PERSONAL_PARAMETER 209")
            needClose = false
            local roomOption = ExternalFun.read_netdata( struct_private.tagPersonalRoomOption, pData )
            GlobalUserItem.setRoomOptionByKindId(GlobalUserItem.nCurGameKind, roomOption)
        elseif cmd_pri_login.SUB_MB_PERSONAL_FEE_PARAMETER == sub then              -- 私人房费用配置
            print("收到 ----------- SUB_MB_PERSONAL_FEE_PARAMETER 212")
            self:onSubFeeParameter(pData)
        elseif cmd_pri_login.SUB_MB_QUERY_GAME_SERVER_RESULT == sub then            -- 创建结果
            dissmissPop = self:onSubGameServerResult(pData)
        elseif cmd_pri_login.SUB_GP_EXCHANGE_ROOM_CARD_RESULT == sub then           -- 房卡兑换游戏币结果
            self:onSubExchangeRoomCardResult(pData)
        elseif cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT_FINSIH == sub then
            print("收到大局的所有小局数据 =================== 收到大局的所有小局数据")
            self:onQueryRoomScroeResultFinish(pData)
        elseif sub == cmd_pri_login.SUB_MB_QUERY_USER_REWARD_RESULT then
            self:onQureyUserRewardResult(pData)
        elseif sub == cmd_pri_login.SUB_MB_QUERY_USER_REWARD_CODE_RESULT then
            self:onQureyUserRewardCodeResult(pData)
        elseif sub == cmd_pri_login.SUB_GR_GETREPLAY_RESULT then
            self:onGetReplayFileResult(pData)
        elseif sub == cmd_pri_login.SUB_MB_UPDATE_ROOM_CARD_RESULT then
            self:onGetRoomCardResult(pData)
        elseif sub == cmd_pri_login.SUB_MB_LUCKY_DRAW_RESULT then
            self:onLuckyDrawResult(pData)
        elseif sub == cmd_pri_login.SUB_MB_APPLY_JOIN_CLUB_RESULT then
            self:onApplyJoinClub(pData)
        elseif sub == cmd_pri_login.SUB_MB_QUERY_CLUB_VIP_RESULT then
            needClose = self:onQueryClubVip(pData)
        elseif sub == cmd_pri_login.SUB_MB_QUERY_CLUB_ROOM_RESULT then
            self:onQueryClubRoom(pData)
        elseif sub == cmd_pri_login.SUB_MB_AGREE_JOIN_RESULT then
            self:onAgreeJoin(pData)
        elseif sub == cmd_pri_login.SUB_MB_MODIFY_CLUB_RULE_RESULT then
            self:onModifyClubRule(pData)
        elseif sub == cmd_pri_login.SUB_MB_QUERY_CLUB_INFO_RESULT then
            self:onQueryClubInfo(pData)
        end
    end

    if needClose then        
        self:onCloseSocket()
        if dissmissPop then
            PriRoom:getInstance():dismissPopWait()
        end
    end
end

function PriFrame:connectLogonServer()
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        self._callBack(LOGINSERVER(-1),"登录服务器失败")
        return false
    end

    return true
end

--请求大局的小局数据
function PriFrame:onQueryRoomScroeResultFinish(pData)
    -- 计算数目
    local len = pData:getlen()
    print("onQueryRoomScroeResultFinish len = ", len)
    local itemcount = math.floor(len/define_private.LEN_PERSONAL_DETAIL_SCORE)
    for i = 1, itemcount do
        local cmd_table = ExternalFun.read_netdata(struct_private.tagQueryPersonalRoomUserDetailScore, pData)
        dump(cmd_table, "--------------  cmd_table  --------------")
        -- 时间戳
        local tt = cmd_table.sysCreateTime
        cmd_table.sortCreateTimeStmp = os.time({day=tt.wDay, month=tt.wMonth, year=tt.wYear, hour=tt.wHour, min=tt.wMinute, sec=tt.wSecond}) or 0
        table.insert(PriRoom:getInstance().m_tabAllRecord, cmd_table)      
    end

    dump(PriRoom:getInstance().m_tabAllRecord, "=========== PriRoom:getInstance().m_tabAllRecord ============")

    if nil ~= self._callBack then
        self._callBack(LOGINSERVER(cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT_FINSIH))
    end
end

-- 房间搜索结果
function PriFrame:onSubSearchRoomResult( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_SearchResult, pData)
    dump(cmd_table, "CMD_MB_SearchResult", 6)
    if 0 == cmd_table.dwServerID then
        if nil ~= self._callBack then
            print("该房间ID不存在   " .. debug.traceback())
            PriRoom:getInstance().m_tabPriData.szServerID = nil
            self._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_SEARCH_RESULT), "该房间ID不存在, 请重新输入!")
        end
        return
    end
    -- 信息记录
    PriRoom:getInstance().m_dwTableID = cmd_table.dwTableID

    -- 动作定义
    PriRoom:getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_SEARCHROOM
    -- 发送登陆
    PriRoom:getInstance():onLoginRoom(cmd_table.dwServerID)
end

-- 解散搜索结果
function PriFrame:onSubDissumeSearchReasult( pData )
    print("收到解散房间")
    local cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_DissumeSearchResult, pData)
    dump(cmd_table, "CMD_MB_DissumeSearchResult", 6)

    -- 信息记录
    PriRoom:getInstance().m_dwTableID = cmd_table.dwTableID
    -- 动作定义
    PriRoom:getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_DISSUMEROOM
    -- 发送登陆
    PriRoom:getInstance():onLoginRoom(cmd_table.dwServerID)
end

-- 私人房列表
function PriFrame:onSubPrivateRoomList( pData )
    PriRoom:getInstance().m_tabCreateRecord = {}

    local cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_PersonalRoomInfoList, pData)
    local listinfo = cmd_table.PersonalRoomInfo[1]
    for i = 1, define_private.MAX_CREATE_PERSONAL_ROOM do
        local info = listinfo[i]
        if info.szRoomID ~= "" then
            info.lScore = info.lTaxCount--self:getMyReword(info.PersonalUserScoreInfo[1])
            -- 时间戳
            local tt = info.sysDissumeTime
            info.sortTimeStmp = os.time({day=tt.wDay, month=tt.wMonth, year=tt.wYear, hour=tt.wHour, min=tt.wMinute, sec=tt.wSecond})
            tt = info.sysCreateTime
            info.createTimeStmp = os.time({day=tt.wDay, month=tt.wMonth, year=tt.wYear, hour=tt.wHour, min=tt.wMinute, sec=tt.wSecond})
            print("插入创建房间记录 " .. i .. " : " .. tostring(info))
            table.insert(PriRoom:getInstance().m_tabCreateRecord, info)
        else
            break
        end
    end
    table.sort( PriRoom:getInstance().m_tabCreateRecord, function(a, b)
        if a.cbIsDisssumRoom ~= b.cbIsDisssumRoom then
            return a.cbIsDisssumRoom < b.cbIsDisssumRoom
        elseif a.cbIsDisssumRoom == 0 and a.cbIsDisssumRoom == b.cbIsDisssumRoom then
            return a.createTimeStmp > b.createTimeStmp
        else
            return a.sortTimeStmp > b.sortTimeStmp
        end        
    end )
    if nil ~= self._callBack then
        self._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_QUERY_PERSONAL_ROOM_LIST_RESULT))
    end
end

-- 参与列表
function PriFrame:onSubJoinRoomList( pData )
    PriRoom:getInstance().m_tabJoinRecord = {}
    -- 计算数目
    local len = pData:getlen()
    local itemcount = math.floor(len/define_private.LEN_PERSONAL_ROOM_SCORE)
    for i = 1, itemcount do
        local pServer = ExternalFun.read_netdata(struct_private.tagQueryPersonalRoomUserScore, pData)
        local tt = pServer.sysCreateTime
        pServer.sortCreateTimeStmp = os.time({day=tt.wDay, month=tt.wMonth, year=tt.wYear, hour=tt.wHour, min=tt.wMinute, sec=tt.wSecond}) or 0
        table.insert(PriRoom:getInstance().m_tabJoinRecord, pServer)        
    end
    table.sort( PriRoom:getInstance().m_tabJoinRecord, function(a, b)
        if a.bFlagOnGame and b.bFlagOnGame and a.bFlagOnGame ~= b.bFlagOnGame then
            if a.nOnGameOrder == b.nOnGameOrder then
                return a.sortCreateTimeStmp > b.sortCreateTimeStmp
            else
                return a.nOnGameOrder > b.nOnGameOrder
            end
        else
            return a.sortCreateTimeStmp > b.sortCreateTimeStmp
        end
    end )
    if nil ~= self._callBack then
        self._callBack(LOGINSERVER(cmd_pri_login.SUB_GR_USER_QUERY_ROOM_SCORE_RESULT))
    end
end

function PriFrame:getMyReword( list )
    if type(list) ~= "table" then
        return 0
    end
    for k,v in pairs(list) do
        if v["dwUserID"] == GlobalUserItem.dwUserID then
            return (tonumber(v.lScore) or 0)
        end
    end
    return 0
end

-- 私人房费用配置
function PriFrame:onSubFeeParameter( pData )
    PriRoom:getInstance():dismissPopWait()
    print("收到私人房费配置数据 ================== 收到私人房费配置数据")
    PriRoom:getInstance().m_tabFeeConfigList = {}
    local len = pData:getlen()
    local count = math.floor(len/define_private.LEN_PERSONAL_TABLE_PARAMETER)
    for idx = 1, count do
        local param = ExternalFun.read_netdata( struct_private.tagPersonalTableParameter, pData )
        print("插入创建房间配置数据")
        table.insert(PriRoom:getInstance().m_tabFeeConfigList, param)
    end
    table.sort( PriRoom:getInstance().m_tabFeeConfigList, function(a, b)
        return a.dwDrawCountLimit < b.dwDrawCountLimit
    end )
    dump(PriRoom:getInstance().m_tabFeeConfigList, "SUB_MB_PERSONAL_FEE_PARAMETER", 6)
    if nil ~= self._callBack then
        self._callBack(LOGINSERVER(cmd_pri_login.SUB_MB_PERSONAL_FEE_PARAMETER))
    end
end

-- 创建结果
function PriFrame:onSubGameServerResult( pData )
    local cmd_table = ExternalFun.read_netdata( cmd_pri_login.CMD_MB_QueryGameServerResult, pData )
    dump(cmd_table, "CMD_MB_QueryGameServerResult", 6)
    local tips = cmd_table.szErrDescrybe

    if false == cmd_table.bCanCreateRoom then
        if nil ~= self._callBack and type(tips) == "string" then
            self._callBack(LOGINSERVER(-1), tips)
        end
        return true
    end
    if 0 == cmd_table.dwServerID and true == cmd_table.bCanCreateRoom then
        if nil ~= self._callBack and type(tips) == "string" then
            self._callBack(LOGINSERVER(-1), tips)
        end
        return true
    end    
    -- 发送登陆
    PriRoom:getInstance():onLoginRoom(cmd_table.dwServerID)
    return false
end

-- 房卡兑换游戏币结果
function PriFrame:onSubExchangeRoomCardResult( pData )
    local cmd_table = ExternalFun.read_netdata( cmd_pri_login.CMD_GP_ExchangeRoomCardResult, pData )
    dump(cmd_table, "CMD_GP_ExchangeRoomCardResult", 6)
    if true == cmd_table.bSuccessed then
        -- 个人财富
        GlobalUserItem.lUserScore = cmd_table.lCurrScore
        GlobalUserItem.lRoomCard = cmd_table.lRoomCard
        -- 通知更新        
        local eventListener = cc.EventCustom:new(yl.RY_USERINFO_NOTIFY)
        eventListener.obj = yl.RY_MSG_USERWEALTH
        cc.Director:getInstance():getEventDispatcher():dispatchEvent(eventListener)
    end

    if nil ~= self._callBack then
        self._callBack(LOGINSERVER(cmd_pri_login.CMD_GP_ExchangeRoomCardResult), cmd_table.szNotifyContent)
    end
end

--网络消息(长连接)
function PriFrame:onGameSocketEvent( main,sub,pData )    
    if cmd_pri_game.MDM_GR_PERSONAL_TABLE == main then
        if cmd_pri_game.SUB_GR_CREATE_SUCCESS == sub then               -- 创建成功
            self:onSubCreateSuccess(pData)
        elseif cmd_pri_game.SUB_GR_CREATE_FAILURE == sub then           -- 创建失败
            self:onSubCreateFailure(pData)
        elseif cmd_pri_game.SUB_GR_CANCEL_TABLE == sub then             -- 解散桌子
            self:onSubTableCancel(pData)
        elseif cmd_pri_game.SUB_GR_CANCEL_REQUEST == sub then           -- 请求解散
            self:onSubCancelRequest(pData)
        elseif cmd_pri_game.SUB_GR_REQUEST_REPLY == sub then            -- 请求答复
            self:onSubRequestReply(pData)
        elseif cmd_pri_game.SUB_GR_REQUEST_RESULT == sub then           -- 请求结果
            self:onSubReplyResult(pData)
        elseif cmd_pri_game.SUB_GR_WAIT_OVER_TIME == sub then           -- 超时等待
            self:onSubWaitOverTime(pData)
        elseif cmd_pri_game.SUB_GR_PERSONAL_TABLE_TIP == sub then       -- 提示信息/游戏信息
            self:onSubTableTip(pData)
        elseif cmd_pri_game.SUB_GR_PERSONAL_TABLE_END == sub then       -- 结束
            self:onSubTableEnd(pData)
        elseif cmd_pri_game.SUB_GR_CANCEL_TABLE_RESULT == sub then      -- 私人房解散结果
            self:onSubCancelTableResult(pData)
        elseif cmd_pri_game.SUB_GR_CURRECE_ROOMCARD_AND_BEAN == sub then -- 强制解散桌子后的游戏豆和房卡数量
            self:onSubCancelTableScoreInfo(pData)
        elseif cmd_pri_game.SUB_GR_CHANGE_CHAIR_COUNT == sub then       -- 改变椅子数量
            self:onSubChangeChairCount(pData)
        elseif cmd_pri_game.SUB_GF_PERSONAL_MESSAGE == sub then         -- 私人房消息
            local cmd_table = ExternalFun.read_netdata(cmd_pri_game.Personal_Room_Message, pData)
            if nil ~= self._callBack then
                self._callBack(GAMESERVER(-1), cmd_table.szMessage)
            end
            if self._gameFrame:isSocketServer() then
                self._gameFrame:onCloseSocket()
                GlobalUserItem.nCurRoomIndex = -1
            end
        elseif cmd_pri_game.SUB_GR_OFFLINE_SEND_ROOMID == sub then         -- 重连进来收房间号
            print("重连进来收到房间号 ------------ 重连进来收到房间号")
            local cmd_table = ExternalFun.read_netdata(cmd_pri_game.Personal_Room_ID, pData)
            print("cmd_table.szRoomID = " .. cmd_table.szRoomID)
        elseif cmd_pri_game.SUB_GP_QUERY_USER_TABLE_INFO_RESULT == sub then --收到查询桌子信息结果
            PriRoom:getInstance():dismissPopWait()
            print("pData:getlen() = " .. pData:getlen())
            self._gameFrame:onCloseSocket()
            print("priFrame self._callBack = ", self._callBack)
            if nil ~= self._callBack then
                self._callBack(GAMESERVER(cmd_pri_game.SUB_GP_QUERY_USER_TABLE_INFO_RESULT), pData)
            end
        elseif cmd_pri_game.SUB_GP_GetReplay_Result == sub then
            print("SUB_GP_GetReplay_Result ------- SUB_GP_GetReplay_Result")
            self:onGetReplayFileResult(pData)
            self._gameFrame:onCloseSocket()
        end

        --专门针对创建房间后，再次查询房间使用，属于特殊情况
        if cmd_pri_game.SUB_GR_CREATE_SUCCESS == sub then
            if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onClubCreateRoomSuccessLater then
                PriRoom:getInstance()._viewFrame:onClubCreateRoomSuccessLater()
            end
        end
    end
end

-- 创建成功
function PriFrame:onSubCreateSuccess( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CreateSuccess, pData)
    -- 更新私人房数据
    print("更新私人房数据 ----------- 更新私人房数据")
    for k,v in pairs(cmd_table) do
        print("cmd_table ================== ", k, v)
    end
    PriRoom:getInstance().m_tabPriData = {}
    PriRoom:getInstance().m_tabPriData.dwDrawTimeLimit = cmd_table.dwDrawTimeLimit
    PriRoom:getInstance().m_tabPriData.dwDrawCountLimit = cmd_table.dwDrawCountLimit
    PriRoom:getInstance().m_tabPriData.szServerID = cmd_table.szServerID
    PriRoom:getInstance().m_tabPriData.wChairCount = cmd_table.wChairCount
    print("cmd_table.wChairCount = " .. tostring(cmd_table.wChairCount))
    -- 个人财富
    GlobalUserItem.dUserBeans = cmd_table.dBeans
    --消耗的房卡
    local diffRoomCard = GlobalUserItem.lRoomCard - cmd_table.lRoomCard
    GlobalUserItem.lRoomCard = cmd_table.lRoomCard
    MultiPlatform:getInstance():onMWEvent("USE_ROOM_CARD", GlobalUserItem.dwUserID, diffRoomCard)
    -- 通知更新
    local eventListener = cc.EventCustom:new(yl.RY_USERINFO_NOTIFY)
    eventListener.obj = yl.RY_MSG_USERWEALTH
    cc.Director:getInstance():getEventDispatcher():dispatchEvent(eventListener)

    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CREATE_SUCCESS), "创建房间成功", cmd_table, true)
    end

    local roomOption = GlobalUserItem.getRoomOptionByKindId(GlobalUserItem.nCurGameKind)
    if 0 == roomOption.cbIsJoinGame then
        if self._gameFrame:isSocketServer() then
            self._gameFrame:onCloseSocket()
        end
    else
        PriRoom:getInstance().m_nLoginAction = PriRoom.L_ACTION.ACT_SEARCHROOM
    end
end

-- 创建失败
function PriFrame:onSubCreateFailure( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CreateFailure, pData)
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(-1), cmd_table.szDescribeString)
    end
end

-- 解散桌子
function PriFrame:onSubTableCancel( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CancelTable, pData)
    dump(cmd_table, "CMD_GR_CancelTable", 6)

    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CANCEL_TABLE), cmd_table)
    end
end

-- 请求解散
function PriFrame:onSubCancelRequest( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CancelRequest, pData)
    -- 自己不处理
    if cmd_table.dwUserID == GlobalUserItem.dwUserID then
        print("自己请求解散")
        return
    end
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CANCEL_REQUEST), cmd_table)
    end
end

-- 请求答复
function PriFrame:onSubRequestReply( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_RequestReply, pData)
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_REQUEST_REPLY), cmd_table)
    end
end

-- 请求结果
function PriFrame:onSubReplyResult( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_RequestResult, pData)
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_REQUEST_RESULT), cmd_table)
    end
end

-- 超时等待
function PriFrame:onSubWaitOverTime( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_WaitOverTime, pData)
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_WAIT_OVER_TIME), cmd_table)
    end
end

-- 提示信息
function PriFrame:onSubTableTip( pData )
    print("提示信息-----------------提示信息 ==== onSubTableTip")
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_PersonalTableTip, pData)
    PriRoom:getInstance().dwTableOwnerUserID = cmd_table.dwTableOwnerUserID
    dump("cmd_table --------  cmd_table = ", cmd_table)
    PriRoom:getInstance().m_tabPriData = cmd_table
    PriRoom:getInstance():getNetFrame()._gameFrame:SetChairCount(cmd_table.wChairCount)
    PriRoom:getInstance().m_bIsMyRoomOwner = (cmd_table.dwTableOwnerUserID == GlobalUserItem.dwUserID)

    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_PERSONAL_TABLE_TIP), cmd_table)
    end
    dump(PriRoom:getInstance().m_tabPriData, "PriRoom:getInstance().m_tabPriData", 6)
end

-- 结束消息
function PriFrame:onSubTableEnd( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_PersonalTableEnd, pData)
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_PERSONAL_TABLE_END), cmd_table, pData)
    end
end

-- 私人房解散结果
function PriFrame:onSubCancelTableResult( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_DissumeTable, pData)
    if nil ~= self._callBack then
        self._callBack(GAMESERVER(cmd_pri_game.SUB_GR_CANCEL_TABLE_RESULT), cmd_table)
    end
    print("收到解散房间 ----------- 收到解散房间")
    if self._gameFrame:isSocketServer() then
        print("关闭socket")
        self._gameFrame:onCloseSocket()
    end
end

-- 解散后财富信息
function PriFrame:onSubCancelTableScoreInfo( pData )
    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_CurrenceRoomCardAndBeans, pData)
    -- 个人财富
    GlobalUserItem.dUserBeans = cmd_table.dbBeans
    GlobalUserItem.lRoomCard = cmd_table.lRoomCard
    -- 通知更新        
    local eventListener = cc.EventCustom:new(yl.RY_USERINFO_NOTIFY)
    eventListener.obj = yl.RY_MSG_USERWEALTH
    cc.Director:getInstance():getEventDispatcher():dispatchEvent(eventListener)

    if nil ~= self._callBack then
        self._callBack(GAMESERVER(SUB_GR_CURRECE_ROOMCARD_AND_BEAN), cmd_table)
    end
end

-- 改变椅子数量
function PriFrame:onSubChangeChairCount( pData )

    local cmd_table = ExternalFun.read_netdata(cmd_pri_game.CMD_GR_ChangeChairCount, pData)
    if nil ~= cmd_table.dwChairCount then
        print("改变椅子的个数  cmd_table.dwChairCount = " .. cmd_table.dwChairCount)
        self._gameFrame._wChairCount = cmd_table.dwChairCount
    else
        print("椅子数改变失败")
    end
end

function PriFrame:onQureyUserReward()
    --操作记录
    self._oprateCode = PriFrame.OP_QUERY_USER_REWARD
    return self:connectLogonServer()
end

function PriFrame:sendQureyUserReward()
    print("发送获取奖励 ======== baseFrame")
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_RoomCardUserActivity)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_GP_QUERY_USER_REWARD)
    buffer:pushdword(GlobalUserItem.dwUserID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        -- self._callBack(LOGINSERVER(-1),"发送分享成功失败")
    end
end

function PriFrame:onQureyUserRewardResult(pData)
    local cmd_data = ExternalFun.read_netdata(cmd_pri_login.CMD_MB_UserRewardInfo, pData)
    print("请求分享信息 cmd_data = " .. tostring(cmd_data))

    local listinfo = cmd_data.rewardInfo[1]
    for i = 1, 30 do
        local info = listinfo[i]
        print("i: " .. i .. " name: " .. tostring(info.szNickName) .. " finish: " .. tostring(info.bFinished))
    end

    PriRoom:getInstance().m_tabInvited = listinfo
    PriRoom:getInstance().m_ShareCount = cmd_data.wShareCount
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.reloadInvitedTableView then
        PriRoom:getInstance()._viewFrame:reloadInvitedTableView()
    end
end

function PriFrame:onQureyUserRewardCode()
    --操作记录
    self._oprateCode = PriFrame.OP_QUERY_USER_REWARD_CODE
    return self:connectLogonServer()
end

function PriFrame:sendQureyUserRewardCode()
    print("发送获取奖励码 ======== baseFrame")
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_GetRewardCode)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_GP_QUERY_USER_REWARD_CODE)
    buffer:pushdword(GlobalUserItem.dwUserID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        -- self._callBack(LOGINSERVER(-1),"发送分享成功失败")
    end
end

function PriFrame:onQureyUserRewardCodeResult(pData)
    local cmd_data = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_GetRewardCodeResult, pData)
    print("请求分享信息 cmd_data = " .. tostring(cmd_data))
    
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.reloadInvitedTableView then
        PriRoom:getInstance()._viewFrame:recvRewardCodeAndUpdate(cmd_data)
    end
end

function PriFrame:onGetReplayFile(gameRound, flagId, roomCardID, serverID)
    if nil ~= self._gameFrame then
        if not self._gameFrame:onGetReplayFile(gameRound, flagId, roomCardID, serverID) then
            self._callBack(GAMESERVER(-1),"获取回放信息失败")
        end
    end
end

function PriFrame:sendGetReplayFile()
    PriRoom:getInstance():showPopWait()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_GetReplay)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_GR_GETREPLAY)
    --TODO 此处测试固定回放，应该删除
    print(tostring(yl.rpFlagID) .. "  " .. tostring(yl.rpRoomCardID) .. " " .. tostring(yl.rpGameRound) )
    if yl.rpFlagID ~= nil and yl.rpRoomCardID ~= nil and yl.rpGameRound ~= nil and yl.rpFlagID ~= "" and yl.rpRoomCardID ~= "" and yl.rpGameRound ~= "" then
        self._flagID = yl.rpFlagID
        self._roomCardID = yl.rpRoomCardID
        self._gameRound = yl.rpGameRound
        self._serverID = yl.serverID
    end
    

    buffer:pushword(self._gameRound)
    buffer:pushdword(self._flagID)
    buffer:pushdword(self._roomCardID)
    buffer:pushdword(self._serverID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        --self._callBack(LOGINSERVER(-1),"建立请求奖励连接失败！")
        PriRoom:getInstance():dismissPopWait()
    end
end

function PriFrame:onGetReplayFileResult(pData)
    -- local cmd_data = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_GetReplayResult, pData)
    -- dump(cmd_data)
    print("PriRoom:getInstance()._viewFrame = " .. tostring(PriRoom:getInstance()._viewFrame))
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.replayGame then
        PriRoom:getInstance()._viewFrame:replayGame(pData)
    else
        print("未找到重放的类")
    end
end

--------------------------------------------- 转盘 开始 --------------------------------------------
function PriFrame:onLuckyDrawResult( pData )
    local cmd_data = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_LuckyDrawResult, pData)
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onLuckyDrawResult then
        PriRoom:getInstance()._viewFrame:onLuckyDrawResult(cmd_data)
    else
        print("未找到抽奖结果的回调方法")
    end
end

function PriFrame:sendLuckyDraw()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_LuckyDraw)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_LUCKY_DRAW)
    buffer:pushdword(GlobalUserItem.dwUserID)
    buffer:pushstring(GlobalUserItem.szPassword, yl.LEN_MD5)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then

    end
end

function PriFrame:queryLuckyDraw()
    self._oprateCode = PriFrame.OP_LUCKY_DRAW
    return self:connectLogonServer()
end
--------------------------------------------- 转盘 结束 --------------------------------------------
--------------------------------------------- 俱乐部 开始 --------------------------------------------

function PriFrame:onApplyJoinClub(pData)
    PriRoom:getInstance():dismissPopWait()
    local cmd_data = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_ApplyClubResult, pData)
    dump(cmd_data, "-------  onApplyJoinClub  -------")
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onSearchClubResult then
        PriRoom:getInstance()._viewFrame:onSearchClubResult(cmd_data)
    else
        print("未找到接收申请俱乐部结果的类")
    end
end

function PriFrame:sendApplyJoinClub()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_ApplyJoinClub)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_APPLY_JOIN_CLUB)
    if self._isSearchClub then
        buffer:pushdword(0)
    else
        buffer:pushdword(GlobalUserItem.dwUserID)
    end
    buffer:pushdword(self._clubNum)
    print("sendApplyJoinClub ---- sendApplyJoinClub self._clubNum = ", self._clubNum)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        print("发送加入俱乐部失败")
    end
    self._clubNum = nil
    self._isSearchClub = false
end

function PriFrame:applyJoinClub(isSearchClub, clubNum)
    self._oprateCode = PriFrame.OP_APPLY_JOIN_CLUB
    self._clubNum = clubNum
    self._isSearchClub = isSearchClub
    return self:connectLogonServer()
end

function PriFrame:onQueryClubVip(pData)
    -- 计算数目
    local len = pData:getlen()
    print("onQueryClubVip len = ", len, "   LEN_CLUB_VIP = ", define_private.LEN_CLUB_VIP)
    local itemcount = math.floor(len/define_private.LEN_CLUB_VIP)
    print("onQueryClubVip itemcount = ", itemcount)
    for i = 1, itemcount do
        local cmd_table = ExternalFun.read_netdata(cmd_pri_login.DBO_GP_QueryClubResult, pData)
        table.insert(PriRoom:getInstance().m_ClubVip, cmd_table)
    end

    if len == 0 and PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onQueryClubVipResult then
        PriRoom:getInstance():dismissPopWait()
        PriRoom:getInstance()._viewFrame:onQueryClubVipResult(PriRoom:getInstance().m_ClubVip)
        return true
    else
        print("未找到接收查询俱乐部会员结果的类, 或还有未接收完的数据")
        return false
    end
end

function PriFrame:sendQueryClubVip()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_QueryClubVip)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_QUERY_CLUB_VIP)
    buffer:pushdword(GlobalUserItem.dwUserID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        print("发送查询俱乐部会员失败")
    end
    self._clubNum = nil
end

function PriFrame:queryClubVip()
    PriRoom:getInstance().m_ClubVip = {}
    self._oprateCode = PriFrame.OP_QUERY_CLUB_VIP
    return self:connectLogonServer()
end

function PriFrame:onAgreeJoin(pData)
    local cmd_table = ExternalFun.read_netdata(cmd_pri_login.DBO_GR_AgreeJoinResult, pData)
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onAgreeJoinResult then
        PriRoom:getInstance()._viewFrame:onAgreeJoinResult(cmd_table)
    else
        print("未找到接收查询俱乐部会员结果的类")
    end
end

function PriFrame:sendAgreeApplyJoin()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_AgreeApplyJoin)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_AGREE_APPLY_JOIN)
    
    print("self._applyAgree = ", self._applyAgree)
    buffer:pushbool(self._applyAgree)
    buffer:pushdword(GlobalUserItem.dwUserID)
    print("GlobalUserItem.dwUserID = ", GlobalUserItem.dwUserID)
    buffer:pushdword(self._applyGameID)
    print("_applyGameID = ", self._applyGameID)
    buffer:pushdword(self._applyClubNum)
    print("self._applyClubNum = ", self._applyClubNum)
    buffer:pushstring(GlobalUserItem.szPassword, yl.LEN_MD5)
    print("GlobalUserItem.szPassword = ", GlobalUserItem.szPassword)
    
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        print("发送查询俱乐部会员失败")
    end
    self._applyAgree = nil
    self._applyClubNum = nil
    self._applyGameID = nil
end

function PriFrame:agreeApplyJoin(agree, applyGameID, clubNum)
    self._oprateCode = PriFrame.OP_AGREE_APPLY_JOIN
    self._applyAgree = agree
    self._applyGameID = applyGameID
    self._applyClubNum = clubNum
    return self:connectLogonServer()
end

function PriFrame:onQueryClubRoom(pData)
    PriRoom:getInstance():dismissPopWait()

    local tCMD = {}
    -- 计算数目
    local len = pData:getlen()
    print("onQueryClubRoom len = ", len, "   LEN_CLUB_ROOM = ", define_private.LEN_CLUB_ROOM)
    local itemcount = math.floor(len/define_private.LEN_CLUB_ROOM)
    print("onQueryClubRoom itemcount = ", itemcount)
    for i = 1, itemcount do
        local cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_QueryClubRoomResult, pData)
        dump(cmd_table, "--------  onQueryClubRoom  ------")
        table.insert(tCMD, cmd_table)      
    end

    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onQueryRoomResult then
        PriRoom:getInstance()._viewFrame:onQueryRoomResult(tCMD)
    else
        print("未找到接收查询俱乐部会员结果的类")
    end
end

function PriFrame:sendQueryClubRoom()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_QueryClubRoom)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_QUERY_CLUB_ROOM)
    
    buffer:pushbool(self._isDelete)
    buffer:pushdword(self._clubRoomNum)

    print("self._isDelete = ", self._isDelete)
    print("self._clubRoomNum = ", self._clubRoomNum)

    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        print("发送查询俱乐部会员失败")
    end
    self._clubRoomNum = nil
    self._isDelete = false
end

function PriFrame:queryClubRoom(isDelete, clubRoomNum)
    self._oprateCode = PriFrame.OP_QUERY_CLUB_ROOM
    self._isDelete = isDelete
    self._clubRoomNum = clubRoomNum
    return self:connectLogonServer()
end

function PriFrame:onModifyClubRule(pData)
    local cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_ModifyClubRuleResult, pData)
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onModifyRoomResult then
        PriRoom:getInstance()._viewFrame:onModifyRoomResult(cmd_table)
    else
        print("未找到修改俱乐部规则结果")
    end
end

function PriFrame:sendModifyClubRule()
    if not self:sendSocketData(self._modifyClubRuleBuffer) and nil ~= self._callBack then
        PriRoom:getInstance():dismissPopWait()
        print("发送修改规则失败")
    end
    self._modifyClubRuleBuffer:release()
    self._modifyClubRuleBuffer = nil
end

function PriFrame:modifyClubRule(modifyClubRuleBuffer)
    self._oprateCode = PriFrame.OP_MODIFY_CLUB_RULE
    self._modifyClubRuleBuffer = modifyClubRuleBuffer
    return self:connectLogonServer()
end

function PriFrame:onQueryClubInfo(pData)
    local tCMD = {}
    -- 计算数目
    local len = pData:getlen()
    print("onQueryClubInfo len = ", len, "   LEN_CLUB_INFO_ROOM = ", define_private.LEN_CLUB_INFO_ROOM)
    local itemcount = math.floor(len/define_private.LEN_CLUB_INFO_ROOM)
    print("onQueryClubInfo itemcount = ", itemcount)
    for i = 1, itemcount do
        local cmd_table = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_QueryClubInfoResult, pData)
        dump(cmd_table, "--------  onQueryClubInfo  ------")
        table.insert(tCMD, cmd_table)      
    end

    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.onQueryClubInfoResult then
        PriRoom:getInstance()._viewFrame:onQueryClubInfoResult(tCMD)
    else
        print("未找到修改俱乐部规则结果")
    end
end

function PriFrame:sendQueryClunInfo()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_QueryClubInfo)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE, cmd_pri_login.SUB_MB_QUERY_CLUB_INFO)
    
    buffer:pushdword(GlobalUserItem.dwUserID)

    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        PriRoom:getInstance():dismissPopWait()
        print("查询所有俱乐部失败")
    end
end

function PriFrame:queryClunInfo()
    self._oprateCode = PriFrame.OP_QUERY_CLUB_INFO
    return self:connectLogonServer()
end


--------------------------------------------- 俱乐部 结束 --------------------------------------------

function PriFrame:onGetRoomCardResult(pData)
    local cmd_data = ExternalFun.read_netdata(cmd_pri_login.CMD_GP_GetRoomCardUserActivityResult, pData)
    dump(cmd_data, "onGetRoomCardResult")
    if PriRoom:getInstance()._viewFrame and PriRoom:getInstance()._viewFrame.refreshRoomCard then
        PriRoom:getInstance()._viewFrame:refreshRoomCard(cmd_data.nRoomCard)
    else
        print("未找到重放的类")
    end
end

function PriFrame:sendGetRoomCard()
    local buffer = ExternalFun.create_netdata(cmd_pri_login.CMD_GP_GetRoomCardUserActivity)
    buffer:setcmdinfo(cmd_pri_login.MDM_MB_PERSONAL_SERVICE,cmd_pri_login.SUB_MB_UPDATE_ROOM_CARD)
    buffer:pushdword(GlobalUserItem.dwUserID)
    if not self:sendSocketData(buffer) and nil ~= self._callBack then
        -- self._callBack(LOGINSERVER(-1),"发送分享成功失败")
    end
end

function PriFrame:onRefreshRoomCard()
    self._oprateCode = PriFrame.OP_GETROOMCARD
    if not self:onCreateSocket(yl.LOGONSERVER,yl.LOGONPORT) and nil ~= self._callBack then
        -- self._callBack(LOGINSERVER(-1),"建立请求奖励连接失败！")
    end
end

return PriFrame