local Utils = {}

--依据宽度截断字符
function Utils.stringEllipsis(szText, fontSize, maxWidth, replaceStr)
    replaceStr = replaceStr or "...";

    --当前计算宽度
    local width = 0
    --截断结果
    local szResult = "..."
    --完成判断
    local bOK = false
     
    local i = 1
     
    while true do
        local cur = string.sub(szText,i,i)
        local byte = string.byte(cur)
        if byte == nil then
            break
        end
        if byte > 128 then
            if width <= maxWidth then
                width = width + fontSize
                i = i + 3
            else
                bOK = true
                break
            end
        elseif  byte ~= 32 then --区分大小写和数字
            if width <= maxWidth then
                if string.byte('A') <= byte and byte <= string.byte('Z') then
                    width = width + fontSize
                elseif string.byte('a') <= byte and byte <= string.byte('z') then
                    width = width + fontSize
                else
                    width = width + fontSize
                end
                i = i + 1
            else
                bOK = true
                break
            end
        else
            i = i + 1
        end
    end
     
    if i ~= 1 then
        szResult = string.sub(szText, 1, i-1)
        if(bOK) then
            szResult = szResult.. replaceStr
        end
    end

    return szResult
end

return Utils