local CardLogic = {}

CardLogic.TP_SORT_ASC           = 0         --升序
CardLogic.TP_SORT_DES           = 1         --降序


-- 扑克类型
CardLogic.CT_ERROR              = 0         -- 错误类型
CardLogic.CT_SINGLE             = 1         -- 单张
CardLogic.CT_DOUBLE             = 2         -- 对子
CardLogic.CT_SINGLE_LINE        = 3         -- 单龙
CardLogic.CT_DOUBLE_LINE        = 4         -- 双龙
CardLogic.CT_THREE_LINE         = 5         -- 三龙
CardLogic.CT_BOMB_3_CARD        = 6        -- 炸弹x3
CardLogic.CT_510K_FALSE         = 7        -- 非同花510k
CardLogic.CT_B_S_WANG           = 8        -- 大王小王
CardLogic.CT_510K_TRUE          = 9        -- 同花510k
CardLogic.CT_S_S_WANG           = 10        -- 全小王
CardLogic.CT_B_B_WANG           = 11        -- 全大王
CardLogic.CT_BOMB_4_CARD        = 12        -- 炸弹x4
CardLogic.CT_BOMB_5_CARD        = 13        -- 炸弹x5
CardLogic.CT_THREE_KING         = 14        -- 三张王
CardLogic.CT_BOMB_6_CARD        = 15        -- 炸弹x6
CardLogic.CT_BOMB_7_CARD        = 16        -- 炸弹x7
CardLogic.CT_BOMB_ALL_KING_CARD = 17        -- 全王(4个王)
CardLogic.CT_BOMB_8_CARD        = 18        -- 炸弹x8

CardLogic.COLOR_MEIHUA          = 1         --梅花
CardLogic.COLOR_FANGKUAI        = 2         --方块
CardLogic.COLOR_HEITAO          = 3         --黑桃
CardLogic.COLOR_HONGTAO         = 4         --红桃

CardLogic._CardData = {
        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, -- 方块 A - K
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, -- 梅花 A - K
        0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, -- 红桃 A - K
        0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, -- 黑桃 A - K
        0x4E, 0x4F,                                                                   -- 小鬼，大鬼

        0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, -- 方块 A - K
        0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, -- 梅花 A - K
        0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, -- 红桃 A - K
        0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, -- 黑桃 A - K
        0x4E, 0x4F,  

        } -- 小王,大王

-- 创建空扑克数组
function CardLogic.emptyCardList( count )
    local tmp = {}
    for i = 1, count do
        tmp[i] = 0
    end
    return tmp
end

--返回2,3,4,5,6,7,8,9,10,11,12,13,14
function CardLogic.GetCardFactValue(value)
    if value == nil then
        return 0
    end
    local result = value
    if value >= 1 and value <= 13 then
        result = value
    elseif value >= 17 and value <= 29 then
        result = value - 16
    elseif value >= 33 and value <= 45 then
        result = value - 32
    elseif value >= 49 and value <= 61 then
        result = value - 48
    elseif value == 0x4E then
        result = value
    elseif value == 0x4F then
        result = value
    end

    if result == 1 then
        result = 14
    elseif result == 2 then
        result = 15
    end

    return result
end

-- 获取花色(1-5)
--红桃 > 黑桃 > 方块 > 草花
function CardLogic.GetCardFactColor(value)
    local color = 1
    if value >= 1 and value <= 13 then
        color = CardLogic.COLOR_FANGKUAI
    elseif value >= 17 and value <= 29 then
        color = CardLogic.COLOR_MEIHUA
    elseif value >= 33 and value <= 45 then
        color = CardLogic.COLOR_HONGTAO
    elseif value >= 49 and value <= 61 then
        color = CardLogic.COLOR_HEITAO
    elseif value == 0x4E then
        color = 5
    elseif value == 0x4F then
        color = 6
    end

    return color
end




function CardLogic.is5Or10OrK(value)
    local factValue = CardLogic.GetCardFactValue(value)
    return factValue == 5 or factValue == 10 or factValue == 13
end

function CardLogic.filter510K(cards)
    local result = {}
    for k,v in pairs(cards) do
        if CardLogic.is5Or10OrK(v) then
            table.insert(result, v)
        end
    end
    return result
end

--判断是否同花
function CardLogic.isSameColor(cbCardData)
    local result = true
    local tLength = #cbCardData
    local cIndex = 1

    if tLength < 2 then
        return false
    end

    while(cIndex < tLength) do
        local value1 = CardLogic.GetCardFactColor(cbCardData[cIndex])
        local value2 = CardLogic.GetCardFactColor(cbCardData[cIndex + 1])
        if value2 ~= value1 then
            result = false
            break
        end
        cIndex = cIndex + 1
    end

    return result
end

function CardLogic.is510k(cbCardData)
    local result = false
    if CardLogic.GetCardFactValue(cbCardData[1]) == 5 and CardLogic.GetCardFactValue(cbCardData[2]) == 10 and CardLogic.GetCardFactValue(cbCardData[3]) == 13 then
        result = true
    end
    return result
end

function CardLogic.isSameCard(cbCardData)
    local result = true
    local tLength = #cbCardData
    local cIndex = 1

    if tLength < 2 then
        return false
    end

    while(cIndex < tLength) do
        local value1 = cbCardData[cIndex]
        local value2 = cbCardData[cIndex + 1]
        if value2 ~= value1 then
            result = false
            break
        end
        cIndex = cIndex + 1
    end

    return result
end


--是否为单连
function CardLogic.isSingleLine(cbCardData)
    -- print("开始单龙检测")
    table.sort(cbCardData, function(a, b)
        return a < b
    end )

    local result = true
    local tLength = #cbCardData
    local cIndex = 1

    if tLength < 3 then
        -- print("不合法：个数小于3")
        return false
    end

    while(cIndex < tLength) do
        local value1 = cbCardData[cIndex]
        local value2 = cbCardData[cIndex + 1]

        if value2 - value1 ~= 1 or value1 == 15 or value2 == 15 then
            -- print("不合法：value2 = " .. value2 .. "   value1 = " .. value1)
            result = false
            break
        end
        cIndex = cIndex + 1
    end

    return result
end

--双龙判断
function CardLogic.isDoubleLine(cbCardData)
    print("开始双龙检测")
    table.sort(cbCardData, function(a, b)
        return a < b
    end )

    for k,v in pairs(cbCardData) do
        print("排序后", k, v)
    end

    local result = true
    local tLength = #cbCardData
    local cIndex = 1

    if tLength < 6 or tLength % 2 ~= 0 then
        return false
    end

    while(cIndex < tLength) do
        local value1 = cbCardData[cIndex]
        local value2 = cbCardData[cIndex + 1]
        print("value1 = " .. value1 .. "   value2 = " .. value2)
        --判断两个是否相等
        if value2 ~= value1 or value1 == 15 or value2 == 15 then
            result = false
            break
        end

        --判断是否相差 1
        if cIndex ~= 1 then
            local lastValue = cbCardData[cIndex - 1]
            if value1 - lastValue ~= 1 then
                result = false
                break   
            end
        end
        cIndex = cIndex + 2
    end

    return result
end

--三龙判断
function CardLogic.isThreeLine(cbCardData)
    print("开始三龙检测")
    table.sort(cbCardData, function(a, b)
        return a < b
    end )

    local result = true
    local tLength = #cbCardData
    local cIndex = 1

    if tLength < 9 or tLength % 3 ~= 0 then
        return false
    end

    while(cIndex < tLength) do
        local value1 = cbCardData[cIndex]
        local value2 = cbCardData[cIndex + 1]
        local value3 = cbCardData[cIndex + 2]
        --判断两个是否相等
        if value2 ~= value1 or value2 ~= value3 or value1 == 15 or value2 == 15 or value3 == 15 then
            result = false
            break
        end

        --判断是否相差 1
        if cIndex ~= 1 then
            local lastValue = cbCardData[cIndex - 1]
            if value1 - lastValue ~= 1 then
                result = false
                break   
            end
        end
        cIndex = cIndex + 3
    end

    return false--result
end

function CardLogic.isBomb(cbCardData)
    -- print("开始炸弹检测 len = " .. #cbCardData)
    return CardLogic.isSameCard(cbCardData)
end

function CardLogic.isDouble(cbCardData)
    -- print("开始对子检测")
    local result = true
    local tLength = #cbCardData
    local cIndex = 1

    if tLength < 2 then
        return false
    end

    while(cIndex < tLength) do
        local value1 = cbCardData[cIndex]
        local value2 = cbCardData[cIndex + 1]
        if value2 ~= value1 or CardLogic.isSmallOrBigKing(value1) or CardLogic.isSmallOrBigKing(value2)  then
            result = false
            break
        end
        cIndex = cIndex + 1
    end

    return result
end

function CardLogic.isSmallOrBigKing(value)
    return value == 0x4F or value == 0x4E
end

--获取类型
function CardLogic.GetCardType(cbCardData, cbCardCount)
    -- print("GetCardType ", cbCardData, cbCardCount)

    cbCardCount = cbCardCount or #cbCardData
    --保存牌的原始数据
    local cbCardDataOrigin = cbCardData

    local cbCardDataTemp = {}
    --转换为实际点数
    for k,v in pairs(cbCardData) do
        table.insert(cbCardDataTemp, CardLogic.GetCardFactValue(v))
    end

    cbCardData = cbCardDataTemp

    table.sort( cbCardData, function(a, b)
        return a < b
    end )

    -- for k,v in pairs(cbCardData) do
        -- print("转换排序后：",k,v)
    -- end

    local result = CardLogic.CT_ERROR
    --简单牌型
    if cbCardCount == 0 then        --空牌
        result = CardLogic.CT_ERROR
    elseif cbCardCount == 1 then    --单牌
        result = CardLogic.CT_SINGLE
    elseif cbCardCount == 2 then    --1.对牌  2.两个大王  3.两个小王  4.大小王
        --大王，小王
        if (cbCardData[1] == 0x4F and cbCardData[2] == 0x4E) or (cbCardData[1] == 0x4E and cbCardData[2] == 0x4F) then         -- 大小王
            result = CardLogic.CT_B_S_WANG
        elseif cbCardData[1] == 0x4F and cbCardData[2] == 0x4F then     -- 一对大王
            result = CardLogic.CT_B_B_WANG
        elseif cbCardData[1] == 0x4E and cbCardData[2] == 0x4E then     -- 一对小王
            result = CardLogic.CT_S_S_WANG
        elseif CardLogic.isDouble(cbCardData) then
            result = CardLogic.CT_DOUBLE
        end
    elseif cbCardCount == 3 then    --三张一样，连子
        local value1 = cbCardData[1]
        local value2 = cbCardData[2]
        local value3 = cbCardData[3]
        
        if CardLogic.isBomb(cbCardData) then                   -- 三条一样的
            result = CardLogic.CT_BOMB_3_CARD                             -- 三个普通牌
        elseif CardLogic.isSingleLine(cbCardData) then                  -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isSmallOrBigKing(value1) and CardLogic.isSmallOrBigKing(value2) and CardLogic.isSmallOrBigKing(value3) then
            result = CardLogic.CT_THREE_KING
        elseif CardLogic.is510k(cbCardData) then
            if CardLogic.isSameColor(cbCardDataOrigin) then     --同花510k
                result = CardLogic.CT_510K_TRUE
            else
                result = CardLogic.CT_510K_FALSE
            end
        end
    elseif cbCardCount == 4 then    -- 1.4张一样（炸弹） 2.4单龙   3.4个王
        local value1 = cbCardData[1]
        local value2 = cbCardData[2]
        local value3 = cbCardData[3]
        local value4 = cbCardData[4]

        if CardLogic.isBomb(cbCardData) then --4个炸弹
            result = CardLogic.CT_BOMB_4_CARD
        elseif CardLogic.isSingleLine(cbCardData) then --四个单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isSmallOrBigKing(value1) and CardLogic.isSmallOrBigKing(value2) and CardLogic.isSmallOrBigKing(value3) and CardLogic.isSmallOrBigKing(value4) then
            result = CardLogic.CT_BOMB_ALL_KING_CARD
        end
    elseif cbCardCount == 5 then    -- 1.5张一样（炸弹） 2.5单龙
        if CardLogic.isBomb(cbCardData) then --5个炸弹
            result = CardLogic.CT_BOMB_5_CARD
        elseif CardLogic.isSingleLine(cbCardData) then
            result = CardLogic.CT_SINGLE_LINE
        end
    elseif cbCardCount == 6 then    -- 1.6张一样（炸弹） 2.6单龙    3.双龙
        if CardLogic.isBomb(cbCardData) then --6个炸弹
            result = CardLogic.CT_BOMB_6_CARD
        elseif CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 7 then    -- 1.7张一样（炸弹） 2.单龙
        if CardLogic.isBomb(cbCardData) then --7个炸弹
            result = CardLogic.CT_BOMB_7_CARD
        elseif CardLogic.isSingleLine(cbCardData) then
            result = CardLogic.CT_SINGLE_LINE
        end
    elseif cbCardCount == 8 then    -- 1.8张一样（炸弹） 2.单龙   3.双龙
        if CardLogic.isBomb(cbCardData) then --8个炸弹
            result = CardLogic.CT_BOMB_8_CARD
        elseif CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 9 then    --1.单龙   2.三龙
        if CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isThreeLine(cbCardData) then
            result = CardLogic.CT_THREE_LINE
        end
    elseif cbCardCount == 10 then    --1.单龙  2.双龙
        if CardLogic.isSingleLine(cbCardData) then -- 单龙 
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 11 then    --1.单龙
        if CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        end
    elseif cbCardCount == 12 then    --1.单龙    2.三龙     3.双龙
        if CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isThreeLine(cbCardData) then --三龙
            result = CardLogic.CT_THREE_LINE
        elseif CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 13 then    --1.单龙
        if CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        end
    elseif cbCardCount == 14 then    --1.单龙     2.双龙
        if CardLogic.isSingleLine(cbCardData) then -- 单龙
            result = CardLogic.CT_SINGLE_LINE
        elseif CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 15 then    --1.三龙
        if CardLogic.isThreeLine(cbCardData) then --三龙
            result = CardLogic.CT_THREE_LINE
        end
    elseif cbCardCount == 16 then    --1.双龙
        if CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 17 then

    elseif cbCardCount == 18 then
        if CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 19 then

    elseif cbCardCount == 20 then
        if CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 21 then

    elseif cbCardCount == 22 then
        if CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 23 then

    elseif cbCardCount == 24 then
        if CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 25 then

    elseif cbCardCount == 26 then
        if CardLogic.isDoubleLine(cbCardData) then -- 双龙
            result = CardLogic.CT_DOUBLE_LINE
        end
    elseif cbCardCount == 27 then

    end

    return result
end


--扑克排序
-- cbCardData 牌的数组
-- cbCardCount 张数
-- cbSortType 排序方式 0表示从大到小排， 1表示从小到大排
function CardLogic.sortCardList(cbCardData, cbCardCount, cbSortType)
    local cbSortValue = {}
    for i=1, cbCardCount do
        local value = CardLogic.GetCardFactValue(cbCardData[i])
        table.insert(cbSortValue, i, value)
    end

    local sortStandard = 1
    if cbSortType == CardLogic.TP_SORT_DES then --降序
        for i = 1, cbCardCount - 1 do
            for j = 1, cbCardCount - 1 do
                if (cbSortValue[j] < cbSortValue[j+1]) or (cbSortValue[j] == cbSortValue[j+1] and cbCardData[j] < cbCardData[j+1]) then
                    local temp = cbSortValue[j]
                    cbSortValue[j] = cbSortValue[j+1]
                    cbSortValue[j+1] = temp
                    local temp2 = cbCardData[j]
                    cbCardData[j] = cbCardData[j+1]
                    cbCardData[j+1] = temp2
                end
            end
        end
    elseif cbSortType == CardLogic.TP_SORT_ASC then --升序
        for i = 1, cbCardCount - 1 do   
            for j = 1, cbCardCount - 1 do
                if (cbSortValue[j] > cbSortValue[j+1]) or (cbSortValue[j] == cbSortValue[j+1] and cbCardData[j] > cbCardData[j+1]) then
                    local temp = cbSortValue[j]
                    cbSortValue[j] = cbSortValue[j+1]
                    cbSortValue[j+1] = temp
                    local temp2 = cbCardData[j]
                    cbCardData[j] = cbCardData[j+1]
                    cbCardData[j+1] = temp2
                end
            end
        end
    else
        print("无排序方式")
        assert(false)
    end

    return cbCardData
end

--去0
function CardLogic.getValidCardsData(cardsData)
    local newCardsData = {}
    for i = 1, #cardsData do
        if 0 ~= cardsData[i] then
            table.insert(newCardsData, cardsData[i])
        end
    end
    return newCardsData
end

function CardLogic.isNormalType(inType)
    return inType == CardLogic.CT_SINGLE or inType == CardLogic.CT_DOUBLE
            or inType == CardLogic.CT_SINGLE_LINE or inType == CardLogic.CT_DOUBLE_LINE
            or inType == CardLogic.CT_THREE_LINE
end

function CardLogic.isBombType(inType)
    return inType == CardLogic.CT_BOMB_3_CARD or inType == CardLogic.CT_BOMB_4_CARD or inType == CardLogic.CT_BOMB_5_CARD
        or inType == CardLogic.CT_BOMB_6_CARD or inType == CardLogic.CT_BOMB_7_CARD or inType == CardLogic.CT_BOMB_8_CARD
end

function CardLogic.compareColor(aColor, bColor)
    return aColor > bColor
end

function CardLogic.compareCard(cbFirstCard, cbFirstCount, cbNextCard, cbNextCount)
    local result = false;

    cbFirstCard = CardLogic.sortCardList(cbFirstCard, #cbFirstCard, CardLogic.TP_SORT_ASC)
    cbNextCard = CardLogic.sortCardList(cbNextCard, #cbNextCard, CardLogic.TP_SORT_ASC)

    local cbNextType = CardLogic.GetCardType(cbNextCard, cbNextCount)
    local cbFirstType = CardLogic.GetCardType(cbFirstCard, cbFirstCount)
        
    if cbNextType == CardLogic.CT_ERROR then
        return false
    end
    for k,v in pairs(cbFirstCard) do
        print("cbFirstCard ============ ",k,v)
    end

    print()
    
    for k,v in pairs(cbNextCard) do
        print("cbNextCard ============ ",k,v)
    end

    --单牌，对子，单龙，双龙，三连，这五种牌型之间不能互相比较大小。
    if cbNextType == cbFirstType 
        and CardLogic.isNormalType(cbFirstType) and cbFirstCount == cbNextCount then
        --比较最大张
        print(cbNextCard[1], CardLogic.GetCardFactValue(cbNextCard[1]) , cbFirstCard[1], CardLogic.GetCardFactValue(cbFirstCard[1]))
        if CardLogic.GetCardFactValue(cbNextCard[1]) > CardLogic.GetCardFactValue(cbFirstCard[1]) then
            result = true
        end
    else
        --除了5种基本类型，都可以大过5种类型
        if CardLogic.isNormalType(cbFirstType)
            and not CardLogic.isNormalType(cbNextType)then
            --除大双龙压小双龙外，压双龙的最小的牌型是四张炮
            if cbFirstType == CardLogic.CT_DOUBLE_LINE and cbNextType >= CardLogic.CT_BOMB_4_CARD then
                result = true
            --除大三连压小三连外，压三连的最小的牌型是六张炮。
            elseif cbFirstType == CardLogic.CT_THREE_LINE and cbNextType >= CardLogic.CT_BOMB_6_CARD then
                result = true
            elseif cbFirstType ~= CardLogic.CT_DOUBLE_LINE and cbFirstType ~= CardLogic.CT_THREE_LINE then
                result = true
            end
        elseif cbFirstType == CardLogic.CT_510K_TRUE and cbNextType == CardLogic.CT_510K_TRUE then
            local color1 = CardLogic.GetCardFactColor(cbFirstCard[1])
            local color2 = CardLogic.GetCardFactColor(cbNextCard[1])
            result = CardLogic.compareColor(color2, color1)
        elseif cbNextType >= CardLogic.CT_BOMB_3_CARD and cbFirstType >= CardLogic.CT_BOMB_3_CARD then
            if cbFirstType == cbNextType  
                and (cbFirstType ~= CardLogic.CT_B_S_WANG and cbFirstType ~= CardLogic.CT_S_S_WANG 
                    and cbFirstType ~= CardLogic.CT_THREE_KING and cbFirstType ~= CardLogic.CT_B_B_WANG) then
                result = CardLogic.GetCardFactValue(cbNextCard[1]) > CardLogic.GetCardFactValue(cbFirstCard[1])
            else
                result = cbNextType > cbFirstType    
            end
            
        end
    end

    print("当前 选中的类型 cbNextType = " .. tostring(cbNextType) .. "  上家出牌类型 cbFirstType = " .. tostring(cbFirstType) .. "   比较结果: " .. tostring(result))
    return result
end

--[[
    @method searchBeyondBomb: 搜索大于 count 个的炸弹
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondBomb(handCardData, firstCard, count)
    print("开始检测 炸弹 x " .. count)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)
    firstCard = CardLogic.sortCardList(firstCard, #firstCard, CardLogic.TP_SORT_ASC)

    local firstType = CardLogic.GetCardType(firstCard, #firstCard)
    print("上一个类型：", firstType)
    local result = {}
    for i = 1, #handCardData do
        --不能炸弹，也即是相同类型的，对子只能用对子->大
        local cardData = CardLogic.GetCardFactValue(handCardData[i])
        local cardsCount = CardLogic.getCardsCountByFactValue(handCardData, cardData)
        if cardsCount == count then
            local t = {}
            table.insert(t, handCardData[i])
            for j = i + 1, #handCardData do
                if CardLogic.GetCardFactValue(handCardData[i]) == CardLogic.GetCardFactValue(handCardData[j]) then
                    --如果是同类型的炸弹，那要比较大小
                    if CardLogic.isBombType(firstType) and #firstCard == count then
                        if CardLogic.GetCardFactValue(t[#t]) > CardLogic.GetCardFactValue(firstCard[1])   then
                            table.insert(t, handCardData[j])
                        else
                            break
                        end
                    else
                        table.insert(t, handCardData[j])
                    end

                    if #t == count then
                        table.insert(result, t)
                        break
                    end
                else
                    break
                end
            end
        end
    end

    return result
end

--[[
    @method searchBeyondSingle: 搜索大于 maxCard 的单牌
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondSingle(handCardData, first)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    local maxCard = first[#first]
    local result = {}
    for i = 1, #handCardData do
        local cardData = CardLogic.GetCardFactValue(handCardData[i])
        --不能拆分对子或炸弹，也即是相同类型的，单张只能用单张->大
        local cardsCount = CardLogic.getCardsCountByFactValue(handCardData, cardData)
        if cardData > CardLogic.GetCardFactValue(maxCard) and cardsCount == 1 then
            table.insert(result, {handCardData[i]})
        end
    end
    return result
end

--[[
    @method searchBeyondDouble: 搜索大于 maxCard 的对子
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondDouble(handCardData, first)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)

    local result = {}
    for i = 1, #handCardData do
        --不能炸弹，也即是相同类型的，对子只能用对子->大
        local cardData = CardLogic.GetCardFactValue(handCardData[i])
        local cardsCount = CardLogic.getCardsCountByFactValue(handCardData, cardData)

        if cardData > CardLogic.GetCardFactValue(first[#first]) 
            and not CardLogic.isSmallOrBigKing(CardLogic.GetCardFactValue(handCardData[i])) and cardsCount == 2 then
            local t = {}
            table.insert(t, handCardData[i])
            for j = i + 1, #handCardData do
                if CardLogic.GetCardFactValue(handCardData[i]) == CardLogic.GetCardFactValue(handCardData[j]) then
                    table.insert(t, handCardData[j])
                    if #t >= 2 then
                        table.insert(result, t)
                        break
                    end
                else
                    break
                end
            end
        end
    end

    return result
end

--[[
    @method searchBeyondSingleLine: 获取单龙
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondSingleLine(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local count = #first
    local maxCardData = first[#first]
    maxCardData = maxCardData or 0
    maxCardData = CardLogic.GetCardFactValue(maxCardData)
    print("单龙查找，最大值为：" .. maxCardData .. " 长度为：" .. count)

    local result = {}
    for i = 1, #handCardData do
        --如果遍历的牌的数量小于上家牌的数量，则无效
        if #handCardData - (i-1) >= count then
            local curCardData = CardLogic.GetCardFactValue(handCardData[i])
            if curCardData + (count-1) > maxCardData then
                local t = {}
                table.insert(t, handCardData[i])
                for j = i + 1, #handCardData do
                    local secondData = CardLogic.GetCardFactValue(handCardData[j])
                    if secondData - curCardData == 1 and secondData ~= 15 then
                        curCardData = secondData
                        table.insert(t, handCardData[j])
                        if #t == count then
                            table.insert(result, t)
                            break
                        end
                    end
                end
            end
        end
    end

    return result
end

--[[
    @method : 获取双龙
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondDoubleLine(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local count = #first
    local maxCardData = first[#first]
    maxCardData = maxCardData or 0
    maxCardData = CardLogic.GetCardFactValue(maxCardData)
    print("双龙查找，最大值为：" .. maxCardData .. " 长度为：" .. count)

    local result = {}
    for i = 1, #handCardData do
        --如果遍历的牌的数量小于上家牌的数量，则无效
        if #handCardData - (i - 1) >= count then
            local curCardData = CardLogic.GetCardFactValue(handCardData[i])
            if curCardData + (count/2 - 1) > maxCardData then
                local t = {}
                table.insert(t, handCardData[i])
                for j = i + 1, #handCardData do
                    --不足两个时，先凑两个相同的
                    local secondData = CardLogic.GetCardFactValue(handCardData[j])
                    if #t % 2 == 1 and secondData - curCardData == 0 and secondData ~= 15 then
                        curCardData = secondData
                        table.insert(t, handCardData[j])
                        if #t == count then
                            table.insert(result, t)
                            break
                        end
                    elseif #t % 2 == 0 and secondData - CardLogic.GetCardFactValue(t[#t]) == 1 and secondData ~= 15 then
                        curCardData = secondData
                        table.insert(t, handCardData[j])
                    end
                end
            end
        end
    end

    return result
end

--[[
    @method : 获取三龙
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondThreeLine(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local count = #first
    local maxCardData = first[#first]
    maxCardData = maxCardData or 0
    maxCardData = CardLogic.GetCardFactValue(maxCardData)
    print("三龙查找，最大值为：" .. maxCardData .. " 长度为：" .. count)

    local result = {}
    for i = 1, #handCardData do
        --如果遍历的牌的数量小于上家牌的数量，则无效
        if #handCardData - (i - 1) >= count then
            local curCardData = CardLogic.GetCardFactValue(handCardData[i])
            if curCardData + (count/3 - 1) > maxCardData then
                local t = {}
                table.insert(t, handCardData[i])
                for j = i + 1, #handCardData do
                    --不足两个时，先凑两个相同的
                    local secondData = CardLogic.GetCardFactValue(handCardData[j])
                    if #t % 3 ~= 0 and secondData - curCardData == 0 and secondData ~= 15 then
                        curCardData = secondData
                        table.insert(t, handCardData[j])
                        if #t == count then
                            table.insert(result, t)
                            break
                        end
                    elseif #t % 3 == 0 and secondData - CardLogic.GetCardFactValue(t[#t]) == 1 and secondData ~= 15 then
                        curCardData = secondData
                        table.insert(t, handCardData[j])
                    end
                end
            end
        end
    end

    return {}--result
end

function CardLogic.getCardsByFactValue(cards, factValue)
    local result = {}
    for i = 1, #cards do
        local curCardData = CardLogic.GetCardFactValue(cards[i])
        if curCardData == factValue then
            table.insert(result, cards[i])
        end
    end
    return result
end

function CardLogic.getCardsCountByFactValue(cards, factValue)
    local result = CardLogic.getCardsByFactValue(cards, factValue)
    return #result
end

--[[
    @method : 获取510k
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyond510K(handCardData, first)
    print("开始查找510K组合")
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local firstIs510K = CardLogic.is510k(first)
    local firstIs510KTRUE = false
    local firstColor510K = -1
    if firstIs510K then
        firstIs510KTRUE = CardLogic.isSameColor(first)
        --如果是同花510K, 则计算花色
        if firstIs510KTRUE then
            firstColor510K = CardLogic.GetCardFactColor(first[1])
        end
    end

    --是否为大小王，若为大小王，则必须用同花510K
    local isBSKing = CardLogic.GetCardType(first, #first) == CardLogic.CT_B_S_WANG

    local allFiveCards = CardLogic.getCardsByFactValue(handCardData, 5)
    local allTenCards = CardLogic.getCardsByFactValue(handCardData, 10)
    local allKCards = CardLogic.getCardsByFactValue(handCardData, 13)

    local result = {}
    if #allFiveCards ~= 0 and #allTenCards ~= 0 and #allKCards ~= 0 then
        --从5中选一个
        for i = 1, #allFiveCards do
            local t = {}
            local five = allFiveCards[i]
            table.insert(t, five)
            for j = 1, #allTenCards do
                local ten = allTenCards[j]
                if CardLogic.GetCardFactValue(t[#t]) == 5 then
                    table.insert(t, ten)
                    for l = 1, #allKCards do
                        local king = allKCards[l]
                        table.insert(t, king)
                        --如果上家出的510K
                        if firstIs510K or isBSKing then
                            --选择的牌是否为同花
                            local tempTure = CardLogic.isSameColor(t)
                            --如果是同花，那需要比较花色
                            if firstIs510KTRUE then
                                if tempTure then
                                    local colorTemp = CardLogic.GetCardFactColor(t[1])
                                    if colorTemp > firstColor510K then
                                        table.insert(result, t)
                                    end
                                end
                            else
                                if tempTure then
                                    table.insert(result, t)
                                end
                            end
                        else
                            table.insert(result, t)
                        end

                        t = {}
                        table.insert(t, five)
                        table.insert(t, ten)
                    end
                    t = {}
                    table.insert(t, five)
                end
            end
        end
    end

    return result
end

--[[
    @method : 获取大小王
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondBSWANG(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local result = {}
    for i = 1, #handCardData do
        local curCardData = CardLogic.GetCardFactValue(handCardData[i])
        local t = {}
        table.insert(t, handCardData[i])
        if curCardData == 0x4E then
            for j = i + 1, #handCardData do
                if CardLogic.GetCardFactValue(handCardData[j]) == 0x4F then
                    table.insert(t, handCardData[j])
                    if #t == 2 then
                        table.insert(result, t)
                        break
                    end
                end
            end
        end
    end

    return result
end

--[[
    @method : 获取两个小王
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondSSWANG(handCardData, first)
    print("开始检测全小王 === ")
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local result = {}
    for i = 1, #handCardData do
        local curCardData = CardLogic.GetCardFactValue(handCardData[i])
        local t = {}
        table.insert(t, handCardData[i])
        if curCardData == 0x4E then
            for j = i + 1, #handCardData do
                if CardLogic.GetCardFactValue(handCardData[j]) == 0x4E then
                    table.insert(t, handCardData[j])
                    if #t == 2 then
                        table.insert(result, t)
                        break
                    end
                end
            end
        end
    end

    print("全小王检测结果")
    for k,v in pairs(result) do
        for a,b in pairs(v) do
            print(b)
        end
    end
    print("全小王检测结果")
    return result
end

--[[
    @method : 获取三个王
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondTHREEKING(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local result = {}
    for i = 1, #handCardData do
        local curCardData = CardLogic.GetCardFactValue(handCardData[i])
        local t = {}
        if CardLogic.isSmallOrBigKing(curCardData) then
            table.insert(t, handCardData[i])
            for j = i + 1, #handCardData do
                if CardLogic.isSmallOrBigKing(CardLogic.GetCardFactValue(handCardData[j]))then
                    table.insert(t, handCardData[j])
                    if #t == 3 then
                        table.insert(result, t)
                        break
                    end
                end
            end
        end
    end

    return result
end

--[[
    @method : 获取三个王
    @param first: 上家的牌
    ]]
function CardLogic.searchBeyondBBWANG(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local result = {}
    for i = 1, #handCardData do
        local curCardData = CardLogic.GetCardFactValue(handCardData[i])
        local t = {}
        table.insert(t, handCardData[i])
        if curCardData == 0x4F then
            for j = i + 1, #handCardData do
                if CardLogic.GetCardFactValue(handCardData[j]) == 0x4F then
                    table.insert(t, handCardData[j])
                    if #t == 2 then
                        table.insert(result, t)
                        break
                    end
                end
            end
        end
    end

    return result
end

function CardLogic.searchBeyondALLKING(handCardData, first)
    first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_ASC)
    handCardData = CardLogic.sortCardList(handCardData, #handCardData, CardLogic.TP_SORT_ASC)

    local result = {}
    for i = 1, #handCardData do
        local curCardData = CardLogic.GetCardFactValue(handCardData[i])
        local t = {}
        if CardLogic.isSmallOrBigKing(curCardData) then
            table.insert(t, handCardData[i])
            for j = i + 1, #handCardData do
                if CardLogic.isSmallOrBigKing(CardLogic.GetCardFactValue(handCardData[j]))then
                    table.insert(t, handCardData[j])
                    if #t == 4 then
                        table.insert(result, t)
                        break
                    end
                end
            end
        end
    end

    return result
end

function CardLogic.filteRepeat(result)
    local resultTemp = result
    for i = #resultTemp, 1, -1 do
        local first = resultTemp[i]
        first = CardLogic.sortCardList(first, #first, CardLogic.TP_SORT_DES)
        for j = i - 1, 1, -1 do
            local second = resultTemp[j]
            second = CardLogic.sortCardList(second, #second, CardLogic.TP_SORT_DES)

            local firstType = CardLogic.GetCardType(first)
            local secondType = CardLogic.GetCardType(second)
            if firstType == secondType and secondType ~= CardLogic.CT_ERROR and #first == #second then
                local isRepeatCount = 0
                for p = 1, #first do
                    if first[p] == second[p] then
                        isRepeatCount = isRepeatCount + 1
                    end
                end

                --全部重复
                if isRepeatCount == #first then
                    print("删除 ------- 类型为： ", firstType)
                    table.remove(resultTemp, i)
                    break
                end
            end
        end
    end
    return  resultTemp
end

--[[
    cbHandCardData: 手牌
    cbFirstCardData: 上家出牌
]]
function CardLogic.searchOutCard(cbHandCardData, cbFirstCardData)
    for k,v in pairs(cbHandCardData) do
        print("手牌 ===== ", k, v)
    end
    local cbFirstType = CardLogic.GetCardType(cbFirstCardData, #cbFirstCardData)
    local beyondTypeList = CardLogic.getBeyondType(cbFirstType)
    for k,v in pairs(beyondTypeList) do
        print("beyondTypeList ====   ", k, v)
    end
    print("--------------------- 以下类型可大过->类型：" .. CardLogic.getTypeName(cbFirstType) .. "  ------------------------")
    local result = {}
    for k, outType in pairs(beyondTypeList) do
        print("outType = " .. outType)
        if outType == CardLogic.CT_SINGLE then
            local tmp = CardLogic.searchBeyondSingle(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_DOUBLE then
            local tmp = CardLogic.searchBeyondDouble(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_SINGLE_LINE then
            local tmp = CardLogic.searchBeyondSingleLine(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_DOUBLE_LINE then
            local tmp = CardLogic.searchBeyondDoubleLine(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_THREE_LINE then
            local tmp = CardLogic.searchBeyondThreeLine(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_BOMB_3_CARD then
            local tmp = CardLogic.searchBeyondBomb(cbHandCardData, cbFirstCardData, 3)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_510K_FALSE then
            local tmp = CardLogic.searchBeyond510K(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_B_S_WANG then
            local tmp = CardLogic.searchBeyondBSWANG(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_510K_TRUE then
            local tmp = CardLogic.searchBeyond510K(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_S_S_WANG then
            local tmp = CardLogic.searchBeyondSSWANG(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_THREE_KING then
            local tmp = CardLogic.searchBeyondTHREEKING(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_B_B_WANG then
            local tmp = CardLogic.searchBeyondBBWANG(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_BOMB_4_CARD then
            local tmp = CardLogic.searchBeyondBomb(cbHandCardData, cbFirstCardData, 4)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_BOMB_5_CARD then
            local tmp = CardLogic.searchBeyondBomb(cbHandCardData, cbFirstCardData, 5)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_BOMB_6_CARD then
            local tmp = CardLogic.searchBeyondBomb(cbHandCardData, cbFirstCardData, 6)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_BOMB_7_CARD then
            local tmp = CardLogic.searchBeyondBomb(cbHandCardData, cbFirstCardData, 7)
            CardLogic.concatTable(result, tmp)
        elseif outType == CardLogic.CT_BOMB_ALL_KING_CARD then
            local tmp = CardLogic.searchBeyondALLKING(cbHandCardData, cbFirstCardData)
            CardLogic.concatTable(result, tmp)
        elseif outType ==  CardLogic.CT_BOMB_8_CARD then
            local tmp = CardLogic.searchBeyondBomb(cbHandCardData, cbFirstCardData, 8)
            CardLogic.concatTable(result, tmp)
        end
    end

    result = CardLogic.filteRepeat(result)

    -- --如果第一手出牌，先从最小的提示
    -- if cbFirstType == CardLogic.CT_ERROR then
    --     table.sort( result, function(a, b)
    --         local factA = CardLogic.GetCardFactValue(a[#a])
    --         local factB = CardLogic.GetCardFactValue(b[#b])
    --         return factA < factB
    --     end )
    -- end
    

    return result
end

function CardLogic.getBeyondType(inType)
    local result = {}

    for secondType = CardLogic.CT_SINGLE, CardLogic.CT_BOMB_8_CARD do
        if inType == CardLogic.CT_ERROR then
            table.insert(result, secondType)
        else
            --单牌，对子，单龙，双龙，三连，这五种牌型之间不能互相比较大小。
            if secondType == inType 
                and CardLogic.isNormalType(inType) then
                table.insert(result, secondType)
            else
                --除了5种基本类型，都可以大过单张、对子
                if CardLogic.isNormalType(inType)
                    and not CardLogic.isNormalType(secondType)then
                    
                    --除大双龙压小双龙外，压双龙的最小的牌型是四张炮
                    if inType == CardLogic.CT_DOUBLE_LINE and secondType >= CardLogic.CT_BOMB_4_CARD then
                        table.insert(result, secondType)
                    --除大三连压小三连外，压三连的最小的牌型是六张炮。
                    elseif inType == CardLogic.CT_THREE_LINE and secondType >= CardLogic.CT_BOMB_6_CARD then
                        table.insert(result, secondType)
                    elseif inType ~= CardLogic.CT_DOUBLE_LINE and inType ~= CardLogic.CT_THREE_LINE then
                        table.insert(result, secondType)
                    end
                elseif inType == CardLogic.CT_510K_TRUE and secondType == CardLogic.CT_510K_TRUE then
                    table.insert(result, secondType)
                elseif secondType >= CardLogic.CT_BOMB_3_CARD and inType >= CardLogic.CT_BOMB_3_CARD then
                    if inType == secondType 
                        and inType ~= CardLogic.CT_B_S_WANG and inType ~= CardLogic.CT_S_S_WANG 
                        and inType ~= CardLogic.CT_THREE_KING and inType ~= CardLogic.CT_B_B_WANG then
                        table.insert(result, secondType)
                    else
                        if secondType > inType then
                            table.insert(result, secondType)
                        end
                    end
                end
            end    
        end
        
    end
    
    return result
end

function CardLogic.concatTable(result, tmp)
    for i = 1, #tmp do
        table.insert(result, tmp[i])
    end
end

function CardLogic.getTypeName(inType)
    local result = "无"
    if inType == CardLogic.CT_ERROR then
        result = "错误类型"
    elseif inType == CardLogic.CT_SINGLE then
        result = "单张"
    elseif inType == CardLogic.CT_DOUBLE then
        result = "对子"
    elseif inType == CardLogic.CT_SINGLE_LINE then
        result = "单龙"
    elseif inType == CardLogic.CT_DOUBLE_LINE then
        result = "双龙"
    elseif inType == CardLogic.CT_THREE_LINE then
        result = "三龙"
    elseif inType == CardLogic.CT_BOMB_3_CARD then
        result = "炸弹x3"
    elseif inType == CardLogic.CT_510K_FALSE then
        result = "非同花510k"
    elseif inType == CardLogic.CT_B_S_WANG then
        result = "大王小王"
    elseif inType == CardLogic.CT_510K_TRUE then
        result = "同花510k"
    elseif inType == CardLogic.CT_S_S_WANG then
        result = "全小王"
    elseif inType == CardLogic.CT_THREE_KING then
        result = "三张王"
    elseif inType == CardLogic.CT_B_B_WANG then
        result = "全大王"
    elseif inType == CardLogic.CT_BOMB_4_CARD then
        result = "炸弹x4"
    elseif inType == CardLogic.CT_BOMB_5_CARD then
        result = "炸弹x5"
    elseif inType == CardLogic.CT_BOMB_6_CARD then
        result = "炸弹x6"
    elseif inType == CardLogic.CT_BOMB_7_CARD then
        result = "炸弹x7"
    elseif inType == CardLogic.CT_BOMB_ALL_KING_CARD then
        result = "全王(4个王)"
    elseif inType == CardLogic.CT_BOMB_8_CARD then
        result = "炸弹x8"
    end

    return result
end


--红桃 > 黑桃 > 方块 > 草花
-- CardLogic._CardData = {
--         0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, -- 方块 A - K
--         0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, -- 梅花 A - K
--         0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, -- 红桃 A - K
--         0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, -- 黑桃 A - K
--         0x4E, 0x4F,                                                                   -- 小鬼，大鬼

--         0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, -- 方块 A - K
--         0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, -- 梅花 A - K
--         0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, -- 红桃 A - K
--         0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, -- 黑桃 A - K
--         0x4E, 0x4F,  

--         } -- 小王,大王

--  local cardData = {
--      78,
--      49,
--      33,
--      61,
--      13,
--      28,
--      0x2A,
--      0x1A,
--      0x1D,
--      43,
--      27,
--      11,
--      57,
--      41,
--      25,
--      40,
--      24,
--      10,
--      39,
--      23,
--      38,
--      0x25,
--      0x2D,
--      53,
--      21,
--      5,
--      5,
--      52
--  }

--  --挑选出纯5 10 k
-- local true510KList = 
-- {
--     {0x35, 0x3A, 0x3D}, --黑桃 5 10 k
--     {0x05, 0x0A, 0x0D}, --方块 5 10 k
--     {0x15, 0x1A, 0x1D}, --梅花 5 10 k
--     {0x35, 0x0A, 0x1D}, --杂 5 10 k
--     {0x02, 0x12, 0x22}  --三个2
-- }

-- local result = {}

-- local index = 1
-- while(index <= #true510KList) do
--     local first = true510KList[index]
--     local rTemp = CardLogic.searchBeyond510K(cardData, first);
--     if #rTemp > 0 then
--         --每次只取第一个，然后把第一个从手牌删除，最后重新查找
--         table.sort( rTemp[1], function(a, b)
--             return CardLogic.GetCardFactValue(a)  > CardLogic.GetCardFactValue(b)
--         end )
--         CardLogic.concatTable(result, rTemp[1])
--         --重手牌中删除这三个
--         for a,b in pairs(rTemp[1]) do
--             for i = #cardData, 1, -1 do
--                 if b == cardData[i] then
--                     print("删除 b = " .. b)
--                     table.remove(cardData, i)
--                 end
--             end
--         end
--     else
--         index = index + 1
--         print("索引增加 index = " .. index)
--     end
-- end

local first = {13, 45, 45, 61, 61}

local hand = {10, 26, 58}
local ret = CardLogic.searchOutCard(hand, first)
print("结果: " .. tostring(#ret))

for k,v in pairs(ret) do
    print("-------------------------")
    for a,b in pairs(v) do
        print(a, CardLogic.GetCardFactValue(b))
        -- print(b)
    end
end

return CardLogic
