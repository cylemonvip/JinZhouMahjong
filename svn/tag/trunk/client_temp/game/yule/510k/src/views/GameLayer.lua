local GameModel = appdf.req(appdf.CLIENT_SRC.."gamemodel.GameModel")
local GameLayer = class("GameLayer", GameModel)

local module_pre = "game.yule.510k.src"
local ExternalFun =  appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local cmd = appdf.req(module_pre .. ".models.CMD_Game")
local game_cmd = appdf.CLIENT_SRC..".plaza.models.CMD_GameServer"
local GameLogic = appdf.req(module_pre .. ".models.GameLogic")
local GameViewLayer = appdf.req(module_pre .. ".views.layer.GameViewLayer")
local ResultLayer = appdf.req(module_pre .. ".views.layer.ResultLayer")

local CardLogic = appdf.req(module_pre .. ".models.CardLogic")

function GameLayer.registerTouchEvent(node, bSwallow, FixedPriority)
    local function onTouchBegan( touch, event )
        if nil == node.onTouchBegan then
            return false
        end
        return node:onTouchBegan(touch, event)
    end

    local function onTouchMoved(touch, event)
        if nil ~= node.onTouchMoved then
            node:onTouchMoved(touch, event)
        end
    end

    local function onTouchEnded( touch, event )
        if nil ~= node.onTouchEnded then
            node:onTouchEnded(touch, event)
        end       
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    listener:setSwallowTouches(bSwallow)
    node._listener = listener
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    local eventDispatcher = node:getEventDispatcher()
    eventDispatcher:addEventListenerWithFixedPriority(listener, FixedPriority)
end



function GameLayer:ctor( frameEngine,scene )        
    GameLayer.super.ctor(self, frameEngine, scene)
    self:OnInitGameEngine()
    self._roomRule = self._gameFrame._dwServerRule
    self.m_bLeaveGame = false    

    self.m_userRecord = {}

    self.m_takeXue = false

    -- 一轮结束
    self.m_bRoundOver = false
    -- 自己是否是地主
    self.m_bIsMyBanker = false
    -- 提示牌数组
    self.m_tabPromptList = {}
    -- 当前出牌
    self.m_tabCurrentCards = {}
    -- 提示牌
    self.m_tabPromptCards = {}
    -- 比牌结果
    self.m_bLastCompareRes = false
    -- 上轮出牌视图
    self.m_nLastOutViewId = cmd.INVALID_VIEWID
    -- 上轮出牌
    self.m_tabLastCards = {}

    --已出牌分
    self.m_allReadyOut510K = {}

    --是否已经有皇上了
    self.m_hasHuangShang = false

    -- 是否叫分状态进入
    self.m_bCallStateEnter = false

    self.m_gameScenePlay = {}

    self.m_wXuanZhanUser = 0

    self.m_wCurrentUser = 0 

    self.m_cbAskStatus = {}

    self.m_wNoDeclareCount = 0 --没宣战玩家数

    self.m_wNoAskFriendCount = 0 --没宣战玩家数

    self.m_addTimes = 1 --个人翻倍

    self.m_tab_cbFriendFlag = {} --

    self.m_tab_cbAddTimesFlag = {}

    self.m_cbFriendFlag = 0

    self.m_wFriend = {}

    self.m_bCanTrustee = false

    self.m_wLastOutCardUser = cmd.INVALID_CHAIRID

    self.m_wOutCardUser = cmd.INVALID_CHAIRID

    self.m_cbTurnCardCount = {}

    self.m_lCellScore = 1
end

-- 房卡信息层zorder
function GameLayer:priGameLayerZorder()
    return 13
end

--获取gamekind
function GameLayer:getGameKind()
    return cmd.KIND_ID
end

--创建场景
function GameLayer:CreateView()
    return GameViewLayer:create(self)
        :addTo(self)
end

function GameLayer:getParentNode( )
    return self._scene
end

function GameLayer:getFrame( )
    return self._gameFrame
end

function GameLayer:logData(msg)
    if nil ~= self._scene.logData then
        self._scene:logData(msg)
    end
end

function GameLayer:reSetData()
    self.m_allReadyOut510K = {}
    self.m_hasHuangShang = false
    self.m_bIsMyBanker = false
    self.m_tabPromptList = {}
    self.m_tabCurrentCards = {}
    self.m_tabPromptCards = {}
    self.m_bLastCompareRes = false
    self.m_nLastOutViewId = cmd.INVALID_VIEWID
    self.m_tabLastCards = {}    
end

---------------------------------------------------------------------------------------
------继承函数
function GameLayer:onEnterTransitionFinish()
    GameLayer.super.onEnterTransitionFinish(self)
end

function GameLayer:onExit()
    self:KillGameClock()
    self:dismissPopWait()
    GameLayer.super.onExit(self)
end

--退出桌子
function GameLayer:onExitTable()
    self:KillGameClock()
    local MeItem = self:GetMeUserItem()
    if MeItem and MeItem.cbUserStatus > yl.US_FREE then
        self:showPopWait()
        self:runAction(cc.Sequence:create(
            cc.CallFunc:create(
                function () 
                    self._gameFrame:StandUp(1)
                end
                ),
            cc.DelayTime:create(10),
            cc.CallFunc:create(
                function ()
                    print("delay leave")
                    self:onExitRoom()
                end
                )
            )
        )
        return
    end

   self:onExitRoom()
end

--离开房间
function GameLayer:onExitRoom()
    self._scene:onKeyBack()
end

-- 计时器响应
function GameLayer:OnEventGameClockInfo(chair,time,clockId)
    if nil ~= self._gameView and nil ~= self._gameView.updateClock then
        self._gameView:updateClock(clockId, time)
    end
end

-- 设置计时器
function GameLayer:SetGameClock(chair,id,time)
    GameLayer.super.SetGameClock(self,chair,id,time)
end

function GameLayer:clearAllHeadInfo()
    for i = 1, cmd.PLAYER_COUNT do
        local viewId = self:SwitchViewChairID(i-1)
        self._gameView:removeHead(viewId)
    end
end

function GameLayer:onGetSitUserNum()
    -- return table.nums(self._gameView.m_tabUserHead)
    local num = 0
    for i = 1, cmd.PLAYER_COUNT do
        if nil ~= self._gameView.m_tabUserItem[i] then
            num = num + 1
        end
    end

    return num
end

function GameLayer:getUserInfoByChairID( chairid )
    local viewId = self:SwitchViewChairID(chairid)
    return self._gameView.m_tabUserItem[viewId]
end

function GameLayer:OnResetGameEngine()
    self._gameView:reSetGame()
    self:reSetData() 
    GameLayer.super.OnResetGameEngine(self)
end

-- 刷新提示列表
-- @param[cards]        出牌数据
-- @param[handCards]    手牌数据
-- @param[outViewId]    出牌视图id
-- @param[curViewId]    当前视图id
function GameLayer:updatePromptList(cards, handCards, outViewId, curViewId)
    self.m_tabCurrentCards = cards
    self.m_tabPromptList = {}

    local result = {}
    if outViewId == curViewId then
        self.m_tabCurrentCards = {}
        result = CardLogic.searchOutCard(handCards, {})
    else
        result = CardLogic.searchOutCard(handCards, cards)
    end

    self.m_tabPromptList = result

    self.m_tabPromptCards = self.m_tabPromptList[#self.m_tabPromptList] or {}
    self._gameView.m_promptIdx = 0
end

-- 扑克对比
-- @param[cards]        当前出牌
-- @param[outView]      出牌视图id
function GameLayer:compareWithLastCards( cards, outView)
    local bRes = false
    self.m_bLastCompareRes = false
    local outCount = #cards
    if outCount > 0 then
        if outView ~= self.m_nLastOutViewId then
            --返回true，表示cards数据大于m_tagLastCards数据
            self.m_bLastCompareRes = GameLogic:CompareCard(self.m_tabLastCards, #self.m_tabLastCards, cards, outCount)
            self.m_nLastOutViewId = outView
        end
        self.m_tabLastCards = cards
    end
    return bRes
end

------------------------------------------------------------------------------------------------------------
--网络处理
------------------------------------------------------------------------------------------------------------

-- 发送准备
function GameLayer:sendReady()
    self:KillGameClock()
    self._gameFrame:SendUserReady()
end

-- 发送叫分
function GameLayer:sendCallScore( score )
    self:KillGameClock()
    local cmddata = CCmd_Data:create(1)
    cmddata:pushbyte(score)
    self:SendData(cmd.SUB_C_CALL_SCORE,cmddata)
end

--是否宣战
function GameLayer:sendDelareWar(cbDelare)
    local cmddata = CCmd_Data:create(1)
    cmddata:pushbyte(cbDelare)
    self:SendData(cmd.SUB_C_XUAN_ZHAN,cmddata)
end

--
function GameLayer:sendAskFriend(cbAsk)
    local cmddata = CCmd_Data:create(1)
    cmddata:pushbyte(cbAsk)
    self:SendData(cmd.SUB_C_ASK_FRIEND,cmddata)
end

function GameLayer:sendAddTimes(cbAdd)
    local cmddata = CCmd_Data:create(1)
    cmddata:pushbyte(cbAdd)
    self:SendData(cmd.SUB_C_ADD_TIMES,cmddata)
end

-- 发送出牌
function GameLayer:sendOutCard(cards, bPass)
    dump(cards, "--------------- sendOutCard cards --------------")
    print("bPass = " .. tostring(bPass))
    self:KillGameClock()
    if bPass then
        local cmddata = CCmd_Data:create()
        self:SendData(cmd.SUB_C_PASS_CARD, cmddata)
    else
        local cardcount = #cards
        local cmd_data = CCmd_Data:create(1 + cardcount)
        cmd_data:pushbyte(cardcount)
        for i = 1, cardcount do
            cmd_data:pushbyte(cards[i])
        end
        self:SendData(cmd.SUB_C_OUT_CARD, cmd_data)
    end
end

-- 发送叫分
function GameLayer:sendTrustees( cbTrustees )
    local cmddata = CCmd_Data:create(1)
    print("--- sendTrustees ---", cbTrustees)
    --cmddata:pushword(self:GetMeChairID())
    cmddata:pushbyte(cbTrustees)
    self:SendData(cmd.SUB_C_TRUSTEE,cmddata)
end

-- 语音播放开始
function GameLayer:onUserVoiceStart( useritem, filepath )
    local viewid = self:SwitchViewChairID(useritem.wChairID)
    self._gameView.m_tabVoiceBox[viewid]:setVisible(true)
end

-- 语音播放结束
function GameLayer:onUserVoiceEnded( useritem, filepath )
    local viewid = self:SwitchViewChairID(useritem.wChairID)
    self._gameView.m_tabVoiceBox[viewid]:setVisible(false)
end

-- 场景信息
function GameLayer:onEventGameScene(cbGameStatus,dataBuffer)
    print("场景数据:" .. cbGameStatus)
    if self.m_bOnGame then
        return
    end
    self.m_bOnGame = true
    --初始化已有玩家
    for i = 1, cmd.PLAYER_COUNT do
        local userItem = self._gameFrame:getTableUserItem(self._gameFrame:GetTableID(), i-1)
        if nil ~= userItem then
            local wViewChairId = self:SwitchViewChairID(i-1)
            self._gameView:OnUpdateUser(wViewChairId, userItem)
        end
    end
    self._gameView:initHistoryScore()
    self.m_cbGameStatus = cbGameStatus
    if cbGameStatus == cmd.GAME_GAME_FREE then                                 --空闲状态
        self:onEventGameSceneFree(dataBuffer)
    else--if cbGameStatus == cmd.GAME_SCENE_PLAY then                             --游戏状态
        self:onEventGameScenePlay(dataBuffer)
    end
    self:dismissPopWait()
    
end

function GameLayer:onEventGameMessage(sub,dataBuffer)
    if nil == self._gameView then
        return
    end
    print("onEventGameMessage",sub)
    if cmd.SUB_S_GAME_START == sub then                 --游戏开始
        self.m_cbGameStatus = cmd.GAME_XUAN_ZHAN
        self:onSubGameStart(dataBuffer)
    elseif cmd.SUB_S_OUT_CARD == sub then               --用户出牌
        self.m_cbGameStatus = cmd.GAME_SCENE_PLAY
        self:onSubOutCard(dataBuffer)
    elseif cmd.SUB_S_PASS_CARD == sub then              --用户放弃
        self.m_cbGameStatus = cmd.GAME_SCENE_PLAY
        self:onSubPassCard(dataBuffer)
    elseif cmd.SUB_S_CARD_INFO == sub then
        self.m_cbGameStatus = cmd.GAME_SCENE_PLAY
        
    elseif cmd.SUB_S_GAME_END == sub then
        self.m_cbGameStatus = cmd.GAME_SCENE_PLAY
        self:onSubGameConclude(dataBuffer)
        self._gameView:hideAllAddTimesTip()
    elseif sub == cmd.SUB_S_RECORD_GAME then            --总结算
        self:onSubGameRecord(dataBuffer)
    end
end

function GameLayer:onSubGameRecord(dataBuffer)
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_RECORD, dataBuffer)
    self.m_userRecord = cmd_table
    dump(self.m_userRecord, "-------------  self.m_userRecord  -------------")
end

function GameLayer:getDetailScore()
    self.m_userRecord.bTakeXue = self.m_takeXue
    return self.m_userRecord
end


function GameLayer:getCurrentUserView()
    local view = self:SwitchViewChairID(self.m_wCurrentUser)
    print("getCurrentUserView self.m_wCurrentUser view = ", self.m_wCurrentUser, view)
    return view
end

-- 游戏开始
function GameLayer:onSubGameStart(dataBuffer)
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_GameStart, dataBuffer)
    dump(cmd_table, "onSubGameStart", 6)

    --第一个出牌的玩家
    self.m_wCurrentUser = cmd_table.wCurrentUser
    --自己的点数
    local cbCardData = cmd_table.cbCardData

    self.m_bRoundOver = false
    self:reSetData()
    --游戏开始
    self._gameView:onGameStart()

    dump(cbCardData[1], "onSubGameStart cbCardData")

    table.sort( cbCardData[1], function(a, b)
        local dataA = GameLogic.convertToRealValueAndColor(a)
        local dataB = GameLogic.convertToRealValueAndColor(b)
        dump(dataA, "dataA")
        dump(dataB, "dataB")
        if dataA.value == dataB.value then
            return dataA.color > dataB.color
        else
            return dataA.value > dataB.value
        end
    end )

    dump(cbCardData[1], "onSubGameStart cbCardData[1]")
    ---[[
    self._gameView:onGetGameCard(cmd.MY_VIEWID, cbCardData[1], false, cc.CallFunc:create(function()
            print("出牌信息 self.m_wCurrentUser me = ", self.m_wCurrentUser, self._gameFrame:GetChairID())
            self._gameView.sortcardBtn:setVisible(true)
            if self.m_wCurrentUser == self._gameFrame:GetChairID() then
                local handCards = self._gameView.m_tabNodeCards[1]:getHandCards()
                self:updatePromptList({}, handCards, cmd.MY_VIEWID, cmd.MY_VIEWID)

                self._gameView:startToPlay(cmd.MY_VIEWID)
                self._gameView:onChangePassBtnState(false)
            end
            self:KillGameClock()
            self:SetGameClock(cmd_table.wCurrentUser, cmd.TAG_COUNTDOWN_DECLAREWAR, cmd.COUNTDOWN_DECLAREWAR)
        end))

    -- 刷新局数
    if PriRoom and GlobalUserItem.bPrivateRoom then
        local curcount = PriRoom:getInstance().m_tabPriData.dwPlayCount
        PriRoom:getInstance().m_tabPriData.dwPlayCount = PriRoom:getInstance().m_tabPriData.dwPlayCount + 1
        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
            self._gameView._priView:onRefreshInfo()
        end
    end
end

function GameLayer:onSubDeclareWar( dataBuffer )
    
end

function GameLayer:onSubFindFriend( dataBuffer )

end

function GameLayer:onSubAskFriend( dataBuffer )

end

function GameLayer:onSubAddTimes( dataBuffer )
end


function GameLayer:SetFriendFlag(dataBuffer)
    
    self._gameView:SetFriendFlag(cmd.INVALID_VIEWID,0)

    if self.m_cbFriendFlag == cmd.FRIEDN_FLAG_NORMAL then --正常
        for i = 1, cmd.PLAYER_COUNT do
            if i == self:GetMeChairID() or i == self.m_wFriend [self:GetMeChairID() + 1] then
                self._gameView:SetFriendFlag(i - 1,5)
            else
                self._gameView:SetFriendFlag(i - 1,2)
            end
        end
    elseif self.m_cbFriendFlag == cmd.FRIEDN_FLAG_DECLAREWAR then --宣战
        for i = 1, cmd.PLAYER_COUNT do
            if i - 1 == self.m_wXuanZhanUser then
                self._gameView:SetFriendFlag(i - 1,4)
            else
                if self.m_wXuanZhanUser == self:GetMeChairID() then
                    self._gameView:SetFriendFlag(i - 1,2)
                else
                    self._gameView:SetFriendFlag(i - 1,5)
                end
            end
        end
    elseif self.m_cbFriendFlag == cmd.FRIEDN_FLAG_MINGDU then
         for i = 1, cmd.PLAYER_COUNT do
            if i -1 == self.m_wXuanZhanUser then
                self._gameView:SetFriendFlag(i - 1,1)
            else
                if self.m_wXuanZhanUser == self:GetMeChairID() then
                    self._gameView:SetFriendFlag(i - 1,2)
                else
                    self._gameView:SetFriendFlag(i - 1,5)
                end
            end
         end
    else
        for i = 1, cmd.PLAYER_COUNT do
            if i -1 ~= self:GetMeChairID() then
                self._gameView:SetFriendFlag(i - 1,3)
            end
        end
    end
end
-- 用户出牌
function GameLayer:onSubOutCard(dataBuffer)
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_OutCard, dataBuffer)
    dump(cmd_table, " ------------- onSubOutCard -------------- ", 6)

    --紧接着出牌的玩家viewId
    local curView = self:SwitchViewChairID(cmd_table.wCurrentUser)
    --当前出牌的玩家viewId
    local outView = self:SwitchViewChairID(cmd_table.wOutCardUser)
    --当前出牌玩家的userid
    self.m_wOutCardUser = cmd_table.wOutCardUser

    print("出牌 " .. outView .. " current " .. curView)

    --出牌的数据
    local outCard = cmd_table.cbCardData[1]

    self.m_allReadyOut510K = cmd_table.b510KCardRecord

    --出牌的扎那个数
    local outCount = cmd_table.cbCardCount

    outCard = CardLogic.getValidCardsData(outCard)

    --将数据排序
    local carddata = CardLogic.sortCardList(outCard, outCount, CardLogic.TP_SORT_DES)

    --获取有效的牌的数据
    carddata = CardLogic.getValidCardsData(carddata)

    -- 扑克对比，记录当前最大的牌
    -- self:compareWithLastCards(carddata, outView)

    --重置计时器
    self:KillGameClock()

    -- 构造提示
    --获取自己的手牌
    local handCards = self._gameView.m_tabNodeCards[cmd.MY_VIEWID]:getHandCards()
    self.m_tabCurrentCards = carddata

    --如果有紧接着出牌的玩家
    if curView ~= cmd.INVALID_CHAIRID and curView == cmd.MY_VIEWID then
        --切换计时器指向
        self:SetGameClock(cmd_table.wCurrentUser, cmd.TAG_COUNTDOWN_OUTCARD, cmd.COUNTDOWN_OUTCARD)
        dump(carddata, "-------- carddata --------")
        self:updatePromptList(carddata, handCards, outView, curView)
        -- 不出按钮设置为可见
        self._gameView:onChangePassBtnState(true)
    end
    
    --执行出牌
    self._gameView:onGetOutCard(curView, outView, carddata)

    --记录当前出牌玩家userid
    self.m_wLastOutCardUser = cmd_table.wOutCardUser

    -- 设置倒计时
    self._gameView:setOperateTipVisible(outView, false)

    if curView ~= cmd.INVALID_CHAIRID then
        self._gameView:setOperateTipVisible(curView, false)
    end

    --更新牌视图
    if outView ~= cmd.MY_VIEWID then
        --获取出牌玩家的手牌节点
        local m_nodeCard = self._gameView.m_tabNodeCards[outView]
        --获取手牌数量
        local currentCount = m_nodeCard:getCardsCount()
        local lastCount = m_nodeCard:getCardsCount()

        --提示手牌数量小于2张
        if lastCount <= 2 and lastCount > 0 then
            self._gameView.m_tabAlertSp[outView]:setVisible(true)
        else
            self._gameView.m_tabAlertSp[outView]:setVisible(false)
        end

        --更新出牌玩家的手牌信息
        self._gameView:updateCardsNodeLayout(outView, currentCount, true)
    end

    local m_nodeCard = self._gameView.m_tabNodeCards[outView]
    local lastCount = m_nodeCard:getCardsCount()
    if lastCount <= 0 and not self.m_hasHuangShang then
        self._gameView:setHuangShangFlag(outView)
    end
    
end

-- 是否合法视图id
function GameLayer:IsValidViewID( viewId )
    local nChairCount = cmd.PLAYER_COUNT
    return (viewId > 0) and (viewId <= nChairCount)
end

-- 用户放弃
function GameLayer:onSubPassCard(dataBuffer)
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_PassCard, dataBuffer)
    dump(cmd_table, "------------- onSubPassCard CMD_S_PassCard ------------")
    local curView = self:SwitchViewChairID(cmd_table.wCurrentUser)
    local passView = self:SwitchViewChairID(cmd_table.wPassCardUser)
    if self:IsValidViewID(curView) and self:IsValidViewID(passView) then
        print("&& pass " .. passView .. " current " ..  curView)
        local handCards = self._gameView.m_tabNodeCards[cmd.MY_VIEWID]:getHandCards()

        if 1 == cmd_table.cbTurnOver then
            print("一轮结束, 记录分数")
            -- self:compareWithLastCards({}, curView)
            -- 构造提示
            self.m_tabCurrentCards = {}
            self:updatePromptList({}, handCards, curView, curView)

            -- 不出按钮
            self._gameView:onChangePassBtnState(false)
        else
            self:updatePromptList(self.m_tabCurrentCards, handCards, 0, curView)
            self._gameView:onChangePassBtnState(true)
        end

        -- 不出牌
        self._gameView:onGetPassCard(cmd_table)

        self._gameView:onGetOutCard(curView, curView, {})

        

        print("重新设置倒计时")
        -- 设置倒计时
        self:SetGameClock(cmd_table.wCurrentUser, cmd.TAG_COUNTDOWN_OUTCARD, cmd.COUNTDOWN_OUTCARD)
    else
        print("viewid invalid" .. curView .. " ## " .. passView)
    end

    self._gameView:setOperateTipVisible(passView, true)
    if curView ~= cmd.INVALID_CHAIRID then
        self._gameView:setOperateTipVisible(curView, false)
    end
end

-- 游戏结束
function GameLayer:onSubGameConclude(dataBuffer)
    

    --手牌最多的是娘娘
    --获取出牌玩家的手牌节点
    local niangNiangViwdId = 1
    local niangNiangCount = 0

    self._gameView._priView.m_selfScoreTxt:setString(0)
    self._gameView._priView.m_otherScoreTxt:setString(0)

    for i = 1, 4 do
        local m_nodeCard = self._gameView.m_tabNodeCards[i]
        --获取手牌数量
        local currentCount = m_nodeCard:getCardsCount()
        print("rest currentCount = ", currentCount)
        if currentCount > niangNiangCount then
            niangNiangCount = currentCount
            niangNiangViwdId = i
        end
    end

    print("niangNiangViwdId = ", niangNiangViwdId)
    print("niangNiangCount = ", niangNiangCount)

    self._gameView:setNiangNiangFlag(niangNiangViwdId)

    self.m_wNoDeclareCount = 0
    self.m_wNoAskFriendCount = 0
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_GameEnd, dataBuffer)
    dump(cmd_table, "onSubGameConclude", 6)
    
    self._gameView:setTrusteeBtnEnable(false)
    self._gameView:changeBtnState(self._gameView.m_bt_trusteeship,false)
    self:KillGameClock()
    self._gameView.m_time_bg:setVisible(false)
    self._gameView:onGetGameConclude( cmd_table )
    -- 私人房无倒计时
    if not GlobalUserItem.bPrivateRoom then
        -- 设置倒计时
        self:SetGameClock(self:GetMeChairID(), cmd.TAG_COUNTDOWN_READY, cmd.COUNTDOWN_READY)
    end

    self._gameView.sortcardBtn:setVisible(false)
    self:reSetData()
end

function GameLayer:onSubTrustee( dataBuffer )
    
end

function GameLayer:onSubBaseScore( dataBuffer )
    
end

function GameLayer:onEventGameSceneFree( dataBuffer )
    local int64 = Integer64.new()
    print("--------------dataBuffer:getlen() ----------------" ,dataBuffer:getlen())
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_StatusFree, dataBuffer)

    dump(cmd_table, "CMD_S_StatusFree", 6)

    self.m_takeXue = cmd_table.bTakeXue

    -- 空闲消息
    self._gameView:onGetGameFree()

    local empTyCard = CardLogic.emptyCardList(cmd.NORMAL_COUNT)

    -- 私人房无倒计时
    self:KillGameClock()
    if not GlobalUserItem.bPrivateRoom then
        -- 设置倒计时
        self:SetGameClock(self:GetMeChairID(), cmd.TAG_COUNTDOWN_READY, cmd.COUNTDOWN_READY)
    end
    -- 刷新局数
    if PriRoom and GlobalUserItem.bPrivateRoom then
        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
            self._gameView._priView:onRefreshInfo()
        end
    end  

    self._gameView.m_time_bg:setVisible(false)

    --更新爬坡显示
    local meChiardID = self:GetMeChairID()
    for i = 1, 9 do
        local papoData = cmd_table.wPrisistInfo[meChiardID+1][i]
        local papo = self._gameView.m_csbNode:getChildByName("Papo_" .. i)
        if papoData == 1 then
            papo:setTexture("game/yule/510k/res/game_res/papo_yes.png")
        elseif papoData == 0 then
            papo:setTexture("game/yule/510k/res/game_res/papo_no.png")
        else
            papo:setVisible(false)
        end
    end

    self._gameView.sortcardBtn:setVisible(false)
end


function GameLayer:onEventGameScenePlay( dataBuffer )
    local cmd_table = ExternalFun.read_netdata(cmd.CMD_S_StatusPlay, dataBuffer)
    self.m_gameScenePlay = cmd_table
    self.m_takeXue = cmd_table.bTakeXue
    dump(cmd_table, "scene play", 6)

    self._gameView:onGetGamePlay()
    self._gameView.sortcardBtn:setVisible(true)

    local myChairId = self:GetMeChairID()
    self.m_wCurrentUser = cmd_table.wCurrentUser
    -- 用户手牌
    local countlist = cmd_table.cbHandCardCount[1]

    for i = 1, 4 do
        local chair = i - 1
        local cards = {}
        local count = countlist[i]
        local viewId = self:SwitchViewChairID(chair)

        if cmd.MY_VIEWID == viewId then
            if count == 0 then
                cmd_table.cbHandCardData[1] = {}
            end

            cards = CardLogic.getValidCardsData(cmd_table.cbHandCardData[1])
            cards = CardLogic.sortCardList(cards, count, CardLogic.TP_SORT_DES)
        else
            cards = CardLogic.emptyCardList(count)
        end
        self._gameView:onGetGameCard(viewId, cards, true)
        self._gameView:updateCardsNodeLayout( viewId, #cards, true)
    end

    -- cbTurnCardCount 大于 0代表上家有出牌
    if cmd_table.cbTurnCardCount > 0 then
        local outCard = cmd_table.cbTurnCardData[1]
        outCard = CardLogic.getValidCardsData(outCard)
        self.m_tabCurrentCards = CardLogic.sortCardList(outCard, #outCard, CardLogic.TP_SORT_DES)
    else
        self.m_tabCurrentCards = {}
    end

    --紧接着出牌的玩家viewId
    local curView = self:SwitchViewChairID(cmd_table.wCurrentUser)
    local lastViewId = self:SwitchViewChairID(cmd_table.wTurnWiner)
    self._gameView:startToPlay(curView)

    local handCards = CardLogic.getValidCardsData(cmd_table.cbHandCardData[1])
    if cmd_table.cbTurnCardCount <= 0 then
        --如果该自己第一手出牌，那么不能点不出按钮
        self._gameView:onChangePassBtnState(false)

        --当前轮到自己才计算
        if curView == cmd.MY_VIEWID then
            self:updatePromptList({}, handCards, curView, curView)
        end
    else
        --当前轮到自己才计算
        if curView == cmd.MY_VIEWID then
            self:updatePromptList(self.m_tabCurrentCards, handCards, lastViewId, curView)
            self._gameView:onChangePassBtnState(true)
        end

        local lastOutCards = CardLogic.getValidCardsData(cmd_table.cbTurnCardData[1])
        --出牌界面
        self._gameView:onGetOutCard(curView, lastViewId, lastOutCards, true)
    end

    self._gameView.m_nCurTotalScore = cmd_table.lTurnScore
    self._gameView.m_round_outTotalScore:setString(string.format("%d", self._gameView.m_nCurTotalScore))
    

    for i = 1, #cmd_table.lPlayerScore[1] do
        local viewId = self:SwitchViewChairID(i-1)
        local head_bg = self._gameView.m_csbNode:getChildByName(string.format("head_bg_%d", viewId))
        local text_curScore = head_bg:getChildByName("text_curScore")
        text_curScore:setString(string.format("当前得分:%d", cmd_table.lPlayerScore[1][i]))
    end

    local friendChairdID = (myChairId + 2) % cmd.PLAYER_COUNT

    local aimChairId1 = (myChairId + 1) % cmd.PLAYER_COUNT
    local aimChairId2 = (myChairId + 3) % cmd.PLAYER_COUNT

    --两个人的总分
    local selfTotalScore = cmd_table.lPlayerScore[1][myChairId+1] + cmd_table.lPlayerScore[1][friendChairdID+1]
    local otherTotalScore = cmd_table.lPlayerScore[1][aimChairId1+1] + cmd_table.lPlayerScore[1][aimChairId2+1]

    self._gameView._priView.m_selfScoreTxt:setString(selfTotalScore)
    self._gameView._priView.m_otherScoreTxt:setString(otherTotalScore)

    --指针指向待出牌的玩家
    self:SetGameClock(self.m_wCurrentUser, cmd.TAG_COUNTDOWN_OUTCARD, cmd.COUNTDOWN_OUTCARD)
    self._gameView.m_time_bg:setVisible(true)

    --显示皇上
    if cmd_table.wWinCount > 0 then
        local huangShangChairId = cmd_table.wWinOrder[1][1]
        local hsViewId = self:SwitchViewChairID(huangShangChairId)
        self._gameView:setHuangShangFlag(hsViewId)
    end

    --记录已出的牌分
    self.m_allReadyOut510K = cmd_table.b510KCardRecord

    --更新爬坡显示
    local meChiardID = self:GetMeChairID()
    for i = 1, 9 do
        local papoData = cmd_table.wPrisistInfo[meChiardID+1][i]
        local papo = self._gameView.m_csbNode:getChildByName("Papo_" .. i)
        if papoData == 1 then
            papo:setVisible(true)
            papo:setTexture("game/yule/510k/res/game_res/papo_yes.png")
        elseif papoData == 0 then
            papo:setVisible(true)
            papo:setTexture("game/yule/510k/res/game_res/papo_no.png")
        else
            papo:setVisible(false)
        end
    end
end

--去0
-- function GameLayer:getValidCardsData(cardsData)
--     local newCardsData = {}
--     for i = 1, #cardsData do
--         if 0 ~= cardsData[i] then
--             table.insert(newCardsData, cardsData[i])
--         end
--     end
--     return newCardsData
-- end

-- 文本聊天
--用户聊天
function GameLayer:onUserChat(chat, wChairId)
    self._gameView:userChat(self:SwitchViewChairID(wChairId), chat.szChatString)
end

-- 表情聊天
function GameLayer:onUserExpression(expression, wChairId, wTotChairID)
    local viewToChair = nil
    if wTotChairID then
        viewToChair = self:SwitchViewChairID(wTotChairID)
    end
    self._gameView:onUserExpression(self:SwitchViewChairID(wChairId), expression.wItemIndex, viewToChair)
end

-- 语音聊天
function GameLayer:onUserVoice( useritem, filepath)
    local viewid = self:SwitchViewChairID(useritem.wChairID)
    if self:IsValidViewID(viewid) then
        self._gameView:onUserVoice(filepath, viewid)
        return true
    end
    return false
end




------------------------------------------------------------------------------------------------------------
--网络处理
------------------------------------------------------------------------------------------------------------

function GameLayer:getWinDir( score )
    print("## is my Banker")
    print(self.m_bIsMyBanker)
    print("## is my Banker")
    if true == self.m_bIsMyBanker then
        if score > 0 then
            return cmd.kLanderWin
        elseif score < 0 then
            return cmd.kLanderLose
        end
    else
        if score > 0 then
            return cmd.kFarmerWin
        elseif score < 0 then
            return cmd.kFarmerLose
        end
    end
    return cmd.kDefault
end

function GameLayer:SwitchViewChairID(chair)
    local viewid = yl.INVALID_CHAIR
    if chair == cmd.INVALID_CHAIRID or chair == nil  then
        return chair
    end
    local nChairCount = cmd.PLAYER_COUNT
    print("nChairCount = " , nChairCount)
    local nChairID = self:GetMeChairID()
    print("nChairID = " , nChairID)
    local userIndex = 1;
    local startIndex = nChairID
    while (true)
        do
        if startIndex == chair then
            break
        end
            userIndex = userIndex + 1
            startIndex = startIndex + 1
        if startIndex >= nChairCount then
            startIndex = 0
        end
    end
    
    return userIndex
end


return GameLayer