local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")

local GameViewLayer = class("GameViewLayer",function(scene)
	local gameViewLayer =  cc.CSLoader:createNode(cmd.RES_PATH.."game/GameScene.csb")
    return gameViewLayer
end)

require("client/src/plaza/models/yl")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")
local ExternalFun = require(appdf.EXTERNAL_SRC .. "ExternalFun")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.GameLogic")
local CardLayer = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.layer.CardLayer")
local ResultLayer = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.layer.ResultLayer")
local SetLayer = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.layer.SetLayer")
local GameChatLayer = appdf.req(appdf.CLIENT_SRC.."plaza.views.layer.game.GameChatLayer")

local ChatNode = appdf.req(appdf.CLIENT_SRC.."plaza.views.layer.game.ChatNode")

local AnimationMgr = appdf.req(appdf.EXTERNAL_SRC .. "AnimationMgr")
local ChoicesPopupNode = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.Entity.ChoicesPopupNode")
local OpNode = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.Entity.OpNode")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")
local QueryDialog = appdf.req("client.src.plaza.views.layer.game.QueryDialog")

local anchorPointHead = {cc.p(0, 1), cc.p(0, 0.5), cc.p(0, 0), cc.p(1, 0.5)}
local posHead = {cc.p(293, 671), cc.p(85, 433), cc.p(85, 190), cc.p(1249, 433)}
local posReady = {cc.p(310, 0), cc.p(135, 0), cc.p(580, -50), cc.p(-135, 0)}
local posPlate = {cc.p(667, 589), cc.p(270, 464), cc.p(667, 184), cc.p(1070, 455)}
local posqipaoChat = {cc.p(340, 650), cc.p(135, 475), cc.p(135, 230), cc.p(1176, 438)}
local posChat = {cc.p(844, 701), cc.p(262, 507), cc.p(301, 319), cc.p(1052, 535)}
local expressionPos = {cc.p(293, 663), cc.p(85, 425), cc.p(85, 183), cc.p(1249, 425)} 

local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

--对外用
GameViewLayer.posPlates = {cc.p(667, 589), cc.p(270, 464), cc.p(667, 184), cc.p(1070, 455)}

GameViewLayer.BT_CHAT 				= 41				--聊天按钮
GameViewLayer.BT_SET 				= 42				--设置
GameViewLayer.BT_EXIT	 			= 43				--退出按钮
GameViewLayer.BT_TRUSTEE 			= 44				--托管按钮
GameViewLayer.BT_HOWPLAY 			= 45				--玩法按钮
GameViewLayer.BT_JIESAN 			= 78				--玩法按钮

GameViewLayer.BT_SWITCH 			= 2 				--按钮开关按钮
GameViewLayer.BT_START 				= 3 				--开始按钮

GameViewLayer.BT_VOICE 				= 5					--语音按钮（语音关闭）
-- GameViewLayer.BT_VOICEOPEN 			= 55				--语音按钮（语音开启）

GameViewLayer.SP_GAMEBTN 			= 6					--游戏操作按钮
GameViewLayer.BT_CHI 				= 67				--游戏操作按钮吃
GameViewLayer.BT_BUMP 				= 62				--游戏操作按钮碰
GameViewLayer.BT_BRIGDE 			= 63				--游戏操作按钮杠
GameViewLayer.BT_LISTEN 			= 64				--游戏操作按钮听
GameViewLayer.BT_WIN 				= 65				--游戏操作按钮胡
GameViewLayer.BT_PASS 				= 66				--游戏操作按钮过

GameViewLayer.SP_ROOMINFO 			= 7					--房间信息
GameViewLayer.TEXT_ROOMNUM 			= 1					--房间信息房号
GameViewLayer.TEXT_ROOMNAME 		= 2					--房间信息房名
GameViewLayer.TEXT_INDEX 			= 3					--房间信息局数
GameViewLayer.TEXT_INNINGS 			= 4					--房间信息剩多少局

GameViewLayer.SP_CLOCK 				= 9					--计时器
-- GameViewLayer.ASLAB_TIME 			= 1					--计时器时间

GameViewLayer.SP_LISTEN 			= 10				--听牌提示

GameViewLayer.NODEPLAYER_1 			= 11				--玩家节点1
GameViewLayer.NODEPLAYER_2 			= 12				--玩家节点2
GameViewLayer.NODEPLAYER_3 			= 13				--玩家节点3
GameViewLayer.NODEPLAYER_4 			= 14				--玩家节点4
GameViewLayer.SP_HEAD 				= 1					--玩家头像
GameViewLayer.SP_HEADCOVER 			= 2					--玩家头像覆盖层
GameViewLayer.TEXT_NICKNAME 		= 3					--玩家昵称
GameViewLayer.ASLAB_SCORE 			= 4					--玩家游戏币
GameViewLayer.SP_READY 				= 5					--玩家准备标志

GameViewLayer.SP_BANKER 			= 7					--庄家
GameViewLayer.SP_ROOMHOST 			= 8 				--房主

-- GameViewLayer.BT_EXIT	 			= 17				--退出按钮
-- GameViewLayer.BT_TRUSTEE 			= 18				--托管按钮

GameViewLayer.SP_PLATE 				= 19				--牌盘
GameViewLayer.SP_PLATECARD		 	= 1					--排盘中的牌

GameViewLayer.TEXT_REMAINNUM 		= 20				--牌堆剩多少张

GameViewLayer.SP_SICE1 				= 27				--筛子1
GameViewLayer.SP_SICE2 				= 28				--筛子2
GameViewLayer.SP_OPERATFLAG			= 29				--操作标志

GameViewLayer.SP_TRUSTEEBG 			= 1					--托管底图
GameViewLayer.BT_TRUSTEECANCEL 		= 30 				--取消托管

GameViewLayer.TAG_BTN_RULE 			= 229				--规则按钮


local zOrder = 
{
	"LAYER_CARD",
	"LISTEN_BG",
	"CARD_PLATE",
	"BUTTON",
	"NODE_PLAYER",
	"LAYER_TRUSTEE",
	"PRI_ROOM_INFO",
	"MENU_BAR",
	"LAYER_CHAT",
	"LAYER_DISMISS",
	"PRI_ROOM_END",
	"LAYER_RESULT",
}
GameViewLayer.Z_ORDER_ENUM = ExternalFun.declarEnumWithTable(0, zOrder)



function GameViewLayer:onInitData()
	--当前指向的位置
	self._currChairId = yl.INVALID_CHAIR
	self.cbActionCard = 0
	self.cbOutCardTemp = 0
	self.chatDetails = {}
	self.cbAppearCardIndex = {}
	self.m_bNormalState = {}
	--房卡需要
	self.m_sparrowUserItem = {}
	self.hasSomeOneNeed = true
end

function GameViewLayer:onResetData(notRefreshBanker)
	self._currChairId = yl.INVALID_CHAIR
	self._cardLayer:onResetData()

	self._cardLayer:removeFromParent()
	self._cardLayer = CardLayer:create(self):addTo(self, 10)							--牌图层

	self.spListenBg:removeAllChildren()
	self.spListenBg:setVisible(false)
	self.cbOutCardTemp = 0
	self.cbAppearCardIndex = {}
	local spFlag = self:getChildByTag(GameViewLayer.SP_OPERATFLAG)
	if spFlag then
		spFlag:removeFromParent()
	end
	self.spCardPlate:setVisible(false)
	self.spHuCardPlate:setVisible(false)
	self.spTrusteeCover:setVisible(false)
	if not notRefreshBanker then
		for i = 1, cmd.GAME_PLAYER do
			self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_BANKER):setVisible(false)
		end
	end
	self.hasSomeOneNeed = true
	self:setRemainCardNum(112)

	self:removeMoveEffectCard()
	if self.opNode then
		self.opNode:hide()
	end
end

function GameViewLayer:removeMoveEffectCard()
	--删除大牌展示
    local runScene = cc.Director:getInstance():getRunningScene()
    local disCard1 = self:getChildByName("VIEWID_1_MOVE_CARD")
	local disCard2 = self:getChildByName("VIEWID_2_MOVE_CARD")
	local disCard3 = self:getChildByName("MY_VIEWID_MOVE_CARD")
	local disCard4 = self:getChildByName("VIEWID_4_MOVE_CARD")

	if disCard1 then
		disCard1:removeFromParent()
	end

	if disCard2 then
		disCard2:removeFromParent()
	end

	if disCard3 then
		disCard3:removeFromParent()
	end

	if disCard4 then
		disCard4:removeFromParent()
	end
end

function GameViewLayer:startHeartBeat(beatSpan)
	beatSpan = beatSpan or yl.HEART_BEAT_SPAN
	self:stopHeartBeat()
	self._heartBeatTimer = cc.Director:getInstance():getScheduler():scheduleScriptFunc(function(dt)
    	self._scene:sendHeartBeatTimer()
    end, beatSpan, false)
end

function GameViewLayer:stopHeartBeat()
	if self._heartBeatTimer then
    	cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._heartBeatTimer)
    	self._heartBeatTimer = nil
    end
end

function GameViewLayer:onExit()
	self._scene:KillGameClock()
    cc.Director:getInstance():getTextureCache():removeUnusedTextures()
    cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames()
    AnimationMgr.removeCachedAnimation(cmd.VOICE_ANIMATION_KEY)

    cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile("plist/pics.plist")
    cc.SpriteFrameCache:getInstance():removeSpriteFramesFromFile(cmd.RES_PATH .. "game/plazaScene.plist")

    if self.entryId then  
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry( self.entryId )  
        self.entryId = nil
    end

    self:stopHeartBeat()

    --删除大牌展示
    local runScene = cc.Director:getInstance():getRunningScene()
    local disCard1 = self:getChildByName("VIEWID_1_MOVE_CARD")
	local disCard2 = self:getChildByName("VIEWID_2_MOVE_CARD")
	local disCard3 = self:getChildByName("MY_VIEWID_MOVE_CARD")
	local disCard4 = self:getChildByName("VIEWID_4_MOVE_CARD")

	if disCard1 then
		disCard1:removeFromParent()
	end

	if disCard2 then
		disCard2:removeFromParent()
	end

	if disCard3 then
		disCard3:removeFromParent()
	end

	if disCard4 then
		disCard4:removeFromParent()
	end
end

function GameViewLayer:onEnterTransitionFinish()
	
end

function GameViewLayer:delayInit()
	if not self._delayLoadFlag then
		self._delayLoadFlag = true
		self:preloadUI()
		self:initButtons()
		self.opNode = OpNode:create(self)
		self.opNode:setPosition(850, 200)
		self.opNode:hide()
		self:addChild(self.opNode, 20)


		self._resultLayer = ResultLayer:create(self):addTo(self, 14):setVisible(false)	--结算框

		--如果是重放，就无需初始化设置和聊天个界面
		if not self._scene._isReplay then
			ChatNode.loadTextChat(cmd.KIND_ID)
			self.chatNode = ChatNode:create(self._scene._gameFrame):addTo(self, 20)
	    	self.chatNode:hide()
	    	self.chatNode:setPosition(cc.p(667, 375))

	    	self._setLayer = SetLayer:create(self):addTo(self, 20)
		end
	end
end

function GameViewLayer:onScenePlay()
	self:delayInit()
end

function GameViewLayer:onSceneFree()
	self:delayInit()
end

function GameViewLayer:onSceneFreeComplete()
	self:initClock()
end

function GameViewLayer:onScenePlayComplete()
	--初始化计时器
	self:initClock()
	--恢复当前操作的人
	self:clockPointToChairId(self._scene.wCurrentUser)
end

function GameViewLayer:initClock()
	--根据总人数和自己的位置，调整自己的方位
	self:hideAllClockPoint()
	local meChiarId = self._scene:GetMeChairID()
	print("onSceneFreeComplete meChiarId = ", meChiarId)
	local chairCount = self._scene:GetChairCount()
	print("onSceneFreeComplete chairCount = ", chairCount)
	--四人场，座位号就是方位，0-东  1-南  2-西   3-北
	if chairCount == 4 then
		local angle = meChiarId * 90
		self.spClock:setRotation(angle)
	end
end

function GameViewLayer:onEnter()
	--声音
	if GlobalUserItem.bVoiceAble then
		AudioEngine.playMusic("sound/BACK_PLAYING.mp3", true)
	end

	if self._scene._isReplay then
		self:delayInit()
	end
end

local this
function GameViewLayer:ctor(scene)
	cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
	cc.SpriteFrameCache:getInstance():addSpriteFrames(cmd.RES_PATH .. "game/plazaScene.plist")

	this = self
	self.isOtherOp = false
	self.choicesPopupArray = {}
	--是否弹出了选择框
	self.isChoicesPopuped = false

	self.hasSomeOneNeed = true

	self._scene = scene
	self.cbActionMaskType = nil
	
	self:onInitData()

	self._cardLayer = CardLayer:create(self):addTo(self, 10)							--牌图层

	self:initBackground()

	--聊天泡泡
	self.chatBubble = {}

	for i = 1 , cmd.GAME_PLAYER do
		local strFile = ""
		if i == 4 then
			strFile = "#userinfo_chat_bg_2.png"
		else
			strFile = "#userinfo_chat_bg_1.png"
		end
		self.chatBubble[i] = display.newSprite(strFile, {scale9 = true ,capInsets = cc.rect(30, 30, 40, 150)})
			:setVisible(false)
			:addTo(self, 18)
		if i == 4 then
			self.chatBubble[i]:setAnchorPoint(cc.p(1, 1))
		else
			self.chatBubble[i]:setAnchorPoint(cc.p(0, 1))
		end
		self.chatBubble[i]:setPosition(posqipaoChat[i])
	end

	--节点事件
	local function onNodeEvent(eventType)
		if eventType == "enterTransitionFinish" then	-- 进入场景而且过渡动画结束时候触发。			
			self:onEnterTransitionFinish()			
		elseif eventType == "exitTransitionStart" then	-- 退出场景而且开始过渡动画时候触发。
			self:onExitTransitionStart()
		elseif eventType == "enter" then
			self:onEnter()
		elseif eventType == "exit" then
			self:onExit()
		end
	end

	self:registerScriptHandler(onNodeEvent)

	self.nodePlayer = {}
	for i = 1, cmd.GAME_PLAYER do
		self.nodePlayer[i] = self:getChildByTag(GameViewLayer.NODEPLAYER_1 + i - 1)
		self.nodePlayer[i]:setLocalZOrder(1)
		self.nodePlayer[i]:setVisible(false)
		self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_HEADCOVER):setLocalZOrder(1)
		self.nodePlayer[i]:getChildByTag(GameViewLayer.TEXT_NICKNAME):setLocalZOrder(1)

		local children = self.nodePlayer[i]:getChildren()
		for k,v in pairs(children) do
			print("name = ", v:getName())
		end


		local gameIDTxt = self.nodePlayer[i]:getChildByName("GameIDTxt")
		print("gameIDTxt = ", gameIDTxt)
		gameIDTxt:setLocalZOrder(1)
		self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_READY):move(posReady[i]):setLocalZOrder(1)
		local sp_banker = self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_BANKER)
			:setLocalZOrder(1)
			:setVisible(false)
		local sp_roomHost = self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_ROOMHOST)
			:setVisible(false)
		if i == 2 or i == cmd.MY_VIEWID then
			sp_banker:move(44, 55)
			sp_roomHost:move(-79, -24)
		end

		if i == cmd.MY_VIEWID or i == cmd.TOP_VIEWID then
			local nameLabel = self.nodePlayer[i]:getChildByTag(GameViewLayer.TEXT_NICKNAME)
			nameLabel:setAnchorPoint(cc.p(0, 0.5))
			nameLabel:setPosition(cc.p(50, 35))

			local scoreLabel = self.nodePlayer[i]:getChildByTag(GameViewLayer.ASLAB_SCORE)
			scoreLabel:setPosition(cc.p(85, -25))

			local gameIDLabel = self.nodePlayer[i]:getChildByName("GameIDTxt")
			gameIDLabel:setPosition(cc.p(85, 5))
		elseif i == cmd.LEFT_VIEWID then

		elseif i == cmd.RIGHT_VIEWID then

		end

		--如果是重放，则不显示准备
		if self._scene._isReplay then
			self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_READY):setVisible(false)
		end
	end

	self.spListenBg = self:getChildByTag(GameViewLayer.SP_LISTEN)
		:setLocalZOrder(90)
		:setVisible(false)
		:setScale(0.7)
	--托管覆盖层
	self.spTrusteeCover = cc.Layer:create():setVisible(false):addTo(self, 4)
	--阴影层
	display.newSprite(cmd.RES_PATH.."game/sp_trusteeCover.png")
		:move(667, 112)
		:setScaleY(1.6)
		:setTag(GameViewLayer.SP_TRUSTEEBG)
		:addTo(self.spTrusteeCover)

	display.newSprite(cmd.RES_PATH.."game/sp_trusteeMan.png")
		:move(1067, 108)
		:addTo(self.spTrusteeCover)
	self.spTrusteeCover:setTouchEnabled(true)
	self.spTrusteeCover:registerScriptTouchHandler(function(eventType, x, y)
		return self:onTrusteeTouchCallback(eventType, x, y)
	end)
	--牌盘
	self.spCardPlate = self:getChildByTag(GameViewLayer.SP_PLATE):setLocalZOrder(13):setVisible(false)
	self.spHuCardPlate = self:getChildByName("sp_sparrowHuPlate"):setLocalZOrder(100):setVisible(false)

	local plateX = 61
	local plateY = 86
	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._cardLayer._mjSkin
	local strFilePP = skinPath .. "/pai_bg_dian.png"

	display.newSprite(strFilePP)
		:move(61, 74)
		:addTo(self.spCardPlate)

	display.newSprite(strFilePP)
		:move(61, 74)
		:addTo(self.spHuCardPlate)

	display.newSprite(skinPath .. "/card_color_7_num_1.png")
		:move(plateX, plateY)
		:setTag(GameViewLayer.SP_PLATECARD)
		:addTo(self.spCardPlate)

	display.newSprite(skinPath .. "/card_color_7_num_1.png")
		:move(plateX, plateY)
		:setTag(GameViewLayer.SP_PLATECARD)
		:addTo(self.spHuCardPlate)	

	self.spClock = self:getChildByTag(GameViewLayer.SP_CLOCK)

	self.asLabTime = self:getChildByName("AsLab_time"):setString("0")
	self.asLabTime:setPosition(669, 403)
	--初始不显示癞子
	self:setLaiziCardVisible(false)

	local roomResLayer = self._scene._scene:getChildByName("RoomResLayer")
	if roomResLayer then
		roomResLayer:removeFromParent()
	end

	if not self._scene._isReplay then
		self:startHeartBeat()
	end

	if self._scene._isReplay then
    	local selfChairID = nil
    	--初始化玩家
    	local replayUserData = GameplayerManager:getInstance():getPlayerInfo()

    	selfChairID = GameplayerManager:getInstance():getSelfChairID()

    	for k,v in pairs(replayUserData) do
    		local viewId = self._scene:SwitchViewChairID(v.wChairID, selfChairID, GameplayerManager:getInstance():getPlayerCount())
    		local head = PopupInfoHead:createReplayHead(v.dwUserID, v.szWeChatImgUrl, 96)

    		head:setPosition(0, 11)			--初始位置
			head:enableHeadFrame(false)
			head:enableInfoPop(true, posHead[viewId], anchorPointHead[viewId], self._scene._gameFrame)			--点击弹出的位置0
			head:setTag(GameViewLayer.SP_HEAD)
			self.nodePlayer[viewId]:addChild(head)
			self.nodePlayer[viewId]:setVisible(true)
			self.m_bNormalState[viewId] = true
			--昵称
			local strNickname = string.EllipsisByConfig(v.szNickName, 90, string.getConfig("fonts/round_body.ttf", 14))
			self.nodePlayer[viewId]:getChildByTag(GameViewLayer.TEXT_NICKNAME):setString(strNickname)
			self.nodePlayer[viewId]:getChildByName("GameIDTxt"):setString(v.dwGameID)

			self.nodePlayer[viewId]:getChildByTag(GameViewLayer.ASLAB_SCORE):setString(v.lScore)
    	end
    	local shengyuBg = self:getChildByName("Sprite_25")
		shengyuBg:setVisible(false)
	end

	--如果是重放，则初始化时间显示板
	if self._scene._isReplay then
		local selfChairID = GameplayerManager:getInstance():getSelfChairID()
		local nChair = GameplayerManager:getInstance():getPlayerCount()
		self:OnUpdataClockView(selfChairID, 0, nChair, true)

		self.spClock:setVisible(false)
		self.asLabTime:setVisible(false)
	end
end

function GameViewLayer:initBackground(index)
	if index == nil then
		index = cc.UserDefault:getInstance():getStringForKey("ZB_INDEX", 0)
	end
	local background = self:getChildByName("background")
	background:setBackGroundImage(cmd.RES_PATH .. "game/background_sparrowHz_" .. index .. ".png", ccui.TextureResType.localType)
end

function GameViewLayer:setMjSkin(index)
	self._cardLayer:setMJSkin(index)
end

function GameViewLayer:setLaiziCardVisible(b)
	local cardFont = self:getChildByName("sp_laizicard")
	cardFont:setVisible(b)
	local cardFontBg = self:getChildByName("sp_laizicard_bg")
	cardFontBg:setVisible(b)

end

function GameViewLayer:preloadUI()
    --导入动画
    local animationCache = cc.AnimationCache:getInstance()
    for i = 1, 12 do
    	local strColor = ""
    	local index = 0
    	if i <= 6 then
    		strColor = "white"
    		index = i
    	else
    		strColor = "red"
    		index = i - 6
    	end
		local animation = cc.Animation:create()
		animation:setDelayPerUnit(0.1)
		animation:setLoops(1)
		for j = 1, 9 do
			local strFile = cmd.RES_PATH.."Animate_sice_"..strColor..string.format("/sice_%d.png", index)
			local spFrame = cc.SpriteFrame:create(strFile, cc.rect(133*(j - 1), 0, 133, 207))
			animation:addSpriteFrame(spFrame)
		end

		local strName = "sice_"..strColor..string.format("_%d", index)
		animationCache:addAnimation(animation, strName)
	end

    -- 语音动画
    -- AnimationMgr.loadAnimationFromFrame("record_play_ani_%d.png", 1, 3, cmd.VOICE_ANIMATION_KEY)
    AnimationMgr.loadAnimationFromFrame("animate_play_record_%d.png", 1, 3, cmd.VOICE_ANIMATION_KEY)

    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_1_%d.png", 0, 11, cmd.DY_EXPRESSION_1_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_2_%d.png", 0, 23, cmd.DY_EXPRESSION_2_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_3_%d.png", 0, 6, cmd.DY_EXPRESSION_3_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_4_%d.png", 0, 13, cmd.DY_EXPRESSION_4_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_5_%d.png", 0, 19, cmd.DY_EXPRESSION_5_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_6_%d.png", 0, 11, cmd.DY_EXPRESSION_6_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_7_%d.png", 0, 10, cmd.DY_EXPRESSION_7_ANI_KEY, AnimationMgr.LOCAL_RES)
    AnimationMgr.loadAnimationFromFrame("client/res/public/dy_expression/pic/expression_8_%d.png", 0, 16, cmd.DY_EXPRESSION_8_ANI_KEY, AnimationMgr.LOCAL_RES)

    AnimationMgr.loadAnimationFromFrame("client/res/public/click0%d.png", 1, 1, cmd.CLICK_PRESS_KEY, AnimationMgr.LOCAL_RES)
end

function GameViewLayer:initButtons()
	--按钮回调
	local btnCallback = function(ref, eventType)
		if eventType == ccui.TouchEventType.ended then
			self:onButtonClickedEvent(ref:getTag(), ref)
		end
	end

	--桌子操作按钮屏蔽层
	local callbackShield = function(ref)
		local pos = ref:getTouchEndPosition()
        local rectBg = self.spTableBtBg:getBoundingBox()
        if not cc.rectContainsPoint(rectBg, pos)then
        	self:showTableBt(false)
        end
	end
	self.layoutShield = ccui.Layout:create()
		:setContentSize(cc.size(display.width, display.height))
		:setTouchEnabled(false)
		:addTo(self, GameViewLayer.Z_ORDER_ENUM.MENU_BAR)
	self.layoutShield:addClickEventListener(callbackShield)

	local btChat = self:getChildByTag(GameViewLayer.BT_CHAT)	--聊天
	btChat:addTouchEventListener(btnCallback)

	--桌子按钮开关
	self.btSwitch = self:getChildByTag(GameViewLayer.BT_SWITCH)
		:setLocalZOrder(2)
	self.btSwitch:addTouchEventListener(btnCallback)

	
	--开始
	self.btStart = self:getChildByTag(GameViewLayer.BT_START)
		:setLocalZOrder(2)
		:setVisible(false)
	self.btStart:addTouchEventListener(btnCallback)
	
	local btRule = self:getChildByName("BtnRule")		--规则按钮
	btRule:setTag(GameViewLayer.TAG_BTN_RULE)
	btRule:addTouchEventListener(btnCallback)	

	if self._scene._isReplay then
		btRule:setVisible(false)
	else
		btRule:setVisible(true)
	end

	--显示玩法的面板
	self.ruleDisplayLayout = self:getChildByName("RuleDisplayLayout")
	self.ruleDisplayLayout:setVisible(false)
	self.ruleDisplayLayout:setTouchEnabled(false)
	self.ruleDisplayLayout:setLocalZOrder(20)

	self.ruleTxt = self.ruleDisplayLayout:getChildByName("RuleTxt")
	self.ruleTxt:setString("")

	--语音
	local btVoice = self:getChildByTag(GameViewLayer.BT_VOICE)
	btVoice:setLocalZOrder(3)
	--btVoice:setVisible(false)
	btVoice:addTouchEventListener(function(ref, eventType)
		if eventType == ccui.TouchEventType.began then
			self._scene._scene:startVoiceRecord()
        elseif eventType == ccui.TouchEventType.ended
        	or eventType == ccui.TouchEventType.canceled then
            self._scene._scene:stopVoiceRecord()
        end
	end)

	local timeText = self:getChildByName("TimeText")
	local date=os.date("%H:%M");
	timeText:setString(tostring(date))

	local dianChiLayout = self:getChildByName("DianChiLayout")
	local valueLayout = dianChiLayout:getChildByName("ValueLayout") 
	local totalLength = 44
	local power = MultiPlatform:getInstance():getClientPower()

	valueLayout:setContentSize(cc.size(totalLength * power / 100, 18))

	if self._scene._isReplay then
		self.btSwitch:setVisible(false)
		btVoice:setVisible(false)
		btChat:setVisible(false)
		dianChiLayout:setPositionX(1270)
		timeText:setPositionX(1322)
	else
		dianChiLayout:setPositionX(1184)
		timeText:setPositionX(1245)
	end

	self.entryId = cc.Director:getInstance():getScheduler():scheduleScriptFunc( 
		function( dt )
			local date=os.date("%H:%M");
			timeText:setString(tostring(date))

			local power = MultiPlatform:getInstance():getClientPower()
			local totalLength = 44
			valueLayout:setContentSize(cc.size(totalLength * power / 100, 18))
		end,
		30, false)
end

function GameViewLayer:showTableBt(bVisible)
	-- if self.spTableBtBg:isVisible() == bVisible then
	-- 	return false
	-- end

	-- local fSpacing = 334
	-- if bVisible == true then
 --        self.layoutShield:setTouchEnabled(true)
	-- 	self.btSwitch:setVisible(false)
	-- 	self.spTableBtBg:setVisible(true)
	-- 	self.spTableBtBg:runAction(
	-- 		cc.Sequence:create(
	-- 			cc.MoveTo:create(0.1, cc.p(1290, 740)),
	-- 			cc.MoveTo:create(0.4, cc.p(1290, 750))
	-- 			)
	-- 		)
	-- else
 --        self.layoutShield:setTouchEnabled(false)
	-- 	self.spTableBtBg:runAction(cc.Sequence:create(
	-- 		cc.Sequence:create(
	-- 			cc.MoveTo:create(0.1, cc.p(1290, 740)),
	-- 			cc.MoveTo:create(0.4, cc.p(1290, 1050)),
	-- 			cc.CallFunc:create(function(ref)
	-- 				self.btSwitch:setVisible(true)
	-- 				self.spTableBtBg:setVisible(false)
	-- 			end))
	-- 			)
	-- 		)
	-- end

	-- return true
end

--更新用户显示
--isHost 更新的用户是否为房主
function GameViewLayer:OnUpdateUser(viewId, userItem, isHost)
	if not viewId or viewId == yl.INVALID_CHAIR then
		return
	end

	--如果是房主，那么更新房主的显示
	if isHost and self._scene and self._scene._gameView and self._scene._gameView._priView then
		if userItem then
			self._scene._gameView._priView:refreshHost(viewId)
		else
			self._scene._gameView._priView:refreshHost(nil)
		end
	end

	self.m_sparrowUserItem[viewId] = userItem
	--头像
	local head = self.nodePlayer[viewId]:getChildByTag(GameViewLayer.SP_HEAD)
	if not userItem then
		self.nodePlayer[viewId]:setVisible(false)
		if head then
			head:setVisible(false)
		end
	else
		self.nodePlayer[viewId]:setVisible(true)
		self.nodePlayer[viewId]:getChildByTag(GameViewLayer.SP_READY):setVisible(userItem.cbUserStatus == yl.US_READY)

		local ip = self._scene._gameFrame:getUserIp(userItem.dwUserID)
		userItem.dwIpAddress = ip

		--头像
		if not head then
			head = PopupInfoHead:createNormal(userItem, 96)
			if viewId == 1 then
				head:setPosition(0, 12)			--初始位置
			else
				head:setPosition(0, 11)			--初始位置
			end
			
			head:enableHeadFrame(false)
			head:enableInfoPop(true, posHead[viewId], anchorPointHead[viewId], self._scene._gameFrame)			--点击弹出的位置0
			head:setTag(GameViewLayer.SP_HEAD)
			self.nodePlayer[viewId]:addChild(head)

			self.m_bNormalState[viewId] = true
		else
			head:updateHead(userItem)
			--掉线头像变灰
			if userItem.cbUserStatus == yl.US_OFFLINE then
				if self.m_bNormalState[viewId] then
					convertToGraySprite(head.m_head.m_spRender)
				end
				self.m_bNormalState[viewId] = false
			else
				if not self.m_bNormalState[viewId] then
					convertToNormalSprite(head.m_head.m_spRender)
				end
				self.m_bNormalState[viewId] = true
			end
		end
		head:setVisible(true)
		--游戏币
		--local score = userItem.lScore
		--if userItem.lScore < 0 then
			--score = -score
		--end
		local beans = userItem.dBeans or userItem.dUserBeans
		beans = beans or 0
		-- local str = string.format("%.2f", beans)

		-- local strScore = self:numInsertPoint(str)
		--if userItem.lScore < 0 then
			--strScore = "."..strScore
		--end
		self.nodePlayer[viewId]:getChildByTag(GameViewLayer.ASLAB_SCORE):setString(beans)
		--昵称
		local strNickname = string.EllipsisByConfig(userItem.szNickName, 90, string.getConfig("fonts/round_body.ttf", 14))
		self.nodePlayer[viewId]:getChildByTag(GameViewLayer.TEXT_NICKNAME):setString(strNickname)
		self.nodePlayer[viewId]:getChildByName("GameIDTxt"):setString(userItem.dwGameID)
	end

	local hasEqueUserList = {}
	local ipSameList = {}

	for i = 0, 3 do
		--获取在场所有人的userItem
		local userItem = self._scene._gameFrame:getTableUserItem(self._scene._gameFrame:GetTableID(), i)
		local userItemEqueList = {}
		for j = 0, 3 do
			local userItemTwo = self._scene._gameFrame:getTableUserItem(self._scene._gameFrame:GetTableID(), j)
			if userItem and userItemTwo then
				if tonumber(userItem.dwUserID) ~= tonumber(userItemTwo.dwUserID) and userItem.dwIpAddress == userItemTwo.dwIpAddress then
					table.insert(hasEqueUserList, i)
					table.insert(hasEqueUserList, j)
					if #userItemEqueList == 0 then
						local viewid = self._scene:SwitchViewChairID(userItem.wChairID)
						if viewid then
							table.insert(userItemEqueList, userItem.dwIpAddress)
							table.insert(userItemEqueList, self.nodePlayer[viewid])
						end
						
					end
					local viewidTwo = self._scene:SwitchViewChairID(userItemTwo.wChairID)
					if viewidTwo then
						table.insert(userItemEqueList, self.nodePlayer[viewidTwo])
					end
				else
					
				end
				local ipTable = ExternalFun.long2ip(userItem.dwIpAddress)
				local r1 = ipTable.b
				local r2 = ipTable.s
				local r3 = ipTable.m
				local r4 = ipTable.p
				local ipStr = ""
				if nil == r1 or nil == r2 or nil == r3 or nil == r4 then
					
				else
					ipStr = r1 .. "." .. r2 .. "." .. r3 .. "." .. r4
				end
			end
		end
		if #userItemEqueList > 0 then
			table.insert(ipSameList, userItemEqueList)
		end
	end

	for i = 1, 4 do
		local headUI = self.nodePlayer[i]
		local ipTxt = headUI:getChildByName("ipTxt")
		ipTxt:setVisible(false)
		ipTxt:setString("")
	end

	for k,v in pairs(ipSameList) do
		--第一个为具体IP
		local ipStr = v[1]
		local ipTable = ExternalFun.long2ip(ipStr)
		local r1 = ipTable.b
		local r2 = ipTable.s
		local r3 = ipTable.m
		local r4 = ipTable.p
		local ipString = ""
		if nil == r1 or nil == r2 or nil == r3 or nil == r4 then
			
		else
			ipString = r1 .. "." .. r2 .. "." .. r3 .. "." .. r4
		end

		for p, value in pairs(v) do
			if p == 1 then

			else
				local ipTxt = value:getChildByName("ipTxt")
				ipTxt:setVisible(true)
				ipTxt:setLocalZOrder(100)
				ipTxt:setString("IP相同") --ipString
			end
		end
	end
end

--用户聊天
function GameViewLayer:userChat(wViewChairId, chatString)
	if chatString and #chatString > 0 then
		-- self._chatLayer:showGameChat(false)
		--取消上次
		if self.chatDetails[wViewChairId] then
			self.chatDetails[wViewChairId]:stopAllActions()
			self.chatDetails[wViewChairId]:removeFromParent()
			self.chatDetails[wViewChairId] = nil
		end

		--创建label
		local limWidth = 24*10
		local labCountLength = cc.Label:createWithTTF(chatString,"fonts/round_body.ttf", 24)  
		if labCountLength:getContentSize().width > limWidth then
			self.chatDetails[wViewChairId] = cc.Label:createWithTTF(chatString,"fonts/round_body.ttf", 24, cc.size(limWidth, 0))
		else
			self.chatDetails[wViewChairId] = cc.Label:createWithTTF(chatString,"fonts/round_body.ttf", 24, cc.size(limWidth, 0))
		end

		self.chatDetails[wViewChairId]:setHorizontalAlignment(cc.TEXT_ALIGNMENT_LEFT)
		self.chatDetails[wViewChairId]:setVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_TOP)

		self.chatDetails[wViewChairId]:setLineBreakWithoutSpace(true)
		self.chatDetails[wViewChairId]:setMaxLineWidth(limWidth)
		self.chatDetails[wViewChairId]:setColor(cc.c3b(255, 255, 255))
		self.chatDetails[wViewChairId]:enableOutline(cc.c4b(0,0,0, 255), 1)
		self.chatDetails[wViewChairId]:setAnchorPoint(cc.p(0.5, 1))
		self.chatDetails[wViewChairId]:addTo(self, 19)

		--行数
		local lineNum = self.chatDetails[wViewChairId]:getContentSize().height / 30 

		local qipaoWidth = self.chatDetails[wViewChairId]:getContentSize().width+38
		local qipaoHeight = self.chatDetails[wViewChairId]:getContentSize().height + lineNum * 30
		
		if qipaoWidth < 194 then
			qipaoWidth = 194
		end

		local dy = 0
		if qipaoHeight <= 60 then
			qipaoHeight = 30
		end

	    --改变气泡大小
		self.chatBubble[wViewChairId]:setContentSize(qipaoWidth, qipaoHeight)
			:setVisible(true)

		local size = self.chatBubble[wViewChairId]:getContentSize()
		if wViewChairId == 4 then
			self.chatDetails[wViewChairId]:move(posqipaoChat[wViewChairId].x - size.width/2, posqipaoChat[wViewChairId].y - 10)
		else
			self.chatDetails[wViewChairId]:move(posqipaoChat[wViewChairId].x + size.width/2+10, posqipaoChat[wViewChairId].y - 10)
		end
		self.chatDetails[wViewChairId]:setScale(0)
		self.chatBubble[wViewChairId]:setScale(0)
		self.chatBubble[wViewChairId]:runAction(cc.ScaleTo:create(0.1, 1))
		--动作
	    self.chatDetails[wViewChairId]:runAction(cc.Sequence:create(
	    	cc.ScaleTo:create(0.1, 1),
	    	cc.DelayTime:create(3),
	    	cc.CallFunc:create(function(ref)
	    		self.chatDetails[wViewChairId]:removeFromParent()
				self.chatDetails[wViewChairId] = nil
				self.chatBubble[wViewChairId]:setVisible(false)
	    	end)))
    end
end

--用户表情
function GameViewLayer:userExpression(wViewChairId, wItemIndex, wToViewChairID)
	if wToViewChairID then --有To的就是互动表情
		local fileName = string.format("client/res/public/dy_expression/pic/expression_icon_%d.png", (wItemIndex - 100))

		local runScene = cc.Director:getInstance():getRunningScene()

		local sp = cc.Sprite:create(fileName)
		if sp == nil then
			return
		end
		sp:setPosition(posHead[wViewChairId])
		self:addChild(sp, 15)

		local endPosition = posHead[wToViewChairID]
		local dx = 0
		local dy = 0
		local needRotate = false
		local delayTime = 0
		local fDelay = 0.15
		if wItemIndex == 101 then
			dx = -30
			dy = 30
			needRotate = false
		elseif wItemIndex == 102 then
			dx = -30
			dy = 30
			needRotate = true
		elseif wItemIndex == 103 then
			needRotate = true
		elseif wItemIndex == 104 then
			dy = 30
			needRotate = false
			delayTime = fDelay * 2
		elseif wItemIndex == 105 then
			needRotate = true
		elseif wItemIndex == 106 then
			needRotate = false
		elseif wItemIndex == 107 then
			dy = 20
			needRotate = true
		elseif wItemIndex == 108 then
			dx = -30
			dy = -30
			needRotate = true
			delayTime = fDelay * 4
		end

		local callfunc = function(  )
			local param = AnimationMgr.getAnimationParam()
		    param.m_fDelay = fDelay
		    param.m_strName = string.format("dy_expression_%d_ani_key", (wItemIndex - 100))
		    local animate = AnimationMgr.getAnimate(param)

			sp:setRotation(0)

			sp:runAction(cc.Sequence:create(cc.DelayTime:create(delayTime), cc.CallFunc:create(function()
				--播放声音
				local sound_path = string.format("client/res/public/dy_expression/sound/expression_%d.mp3", wItemIndex)
				if GlobalUserItem.bSoundAble then
					AudioEngine.playEffect(sound_path, false)
				end
			end)))

			local animaterpt = cc.Sequence:create(animate, cc.RemoveSelf:create())
			sp:runAction(animaterpt)
		end

		local spawnAction = nil
		if needRotate then
			spawnAction = cc.Spawn:create(
					cc.RotateBy:create(0.5, 1440),
					cc.MoveTo:create(0.5, cc.p(endPosition.x + dx, endPosition.y + dy))
					)
		else
			spawnAction = cc.Spawn:create(
					cc.MoveTo:create(0.5, cc.p(endPosition.x + dx, endPosition.y + dy))
					)
		end

		sp:runAction(cc.Sequence:create(
				spawnAction,
				cc.CallFunc:create(callfunc)
				-- cc.RemoveSelf:create()
			))

		return
	end

	if wItemIndex and wItemIndex >= 0 then
		-- self._chatLayer:showGameChat(false)
		--取消上次
		if self.chatDetails[wViewChairId] then
			self.chatDetails[wViewChairId]:stopAllActions()
			self.chatDetails[wViewChairId]:removeFromParent()
			self.chatDetails[wViewChairId] = nil
		end

		cc.SpriteFrameCache:getInstance():addSpriteFrames("client/res/Chat/chat.plist")

	    self.chatDetails[wViewChairId] = cc.Sprite:create()
			:setAnchorPoint(cc.p(0.5, 0.5))
			:addTo(self, 19)

		
		self.chatDetails[wViewChairId]:setScale(1.0)
	    --改变气泡大小
		self.chatBubble[wViewChairId]:setContentSize(90,100)
			:setVisible(false)

		local size = self.chatBubble[wViewChairId]:getContentSize()
		if wViewChairId == 4 then
			self.chatDetails[wViewChairId]:setPosition(cc.p(posqipaoChat[wViewChairId].x - size.width + 10, posqipaoChat[wViewChairId].y + 60))
		else
			self.chatDetails[wViewChairId]:setPosition(cc.p(posqipaoChat[wViewChairId].x + size.width - 40, posqipaoChat[wViewChairId].y + 60))
		end

		self.chatDetails[wViewChairId]:setPosition(cc.p(expressionPos[wViewChairId].x, expressionPos[wViewChairId].y + 12))

		ChatNode.playEmojAnimation(self.chatDetails[wViewChairId], cc.p(0,0), wItemIndex)

	    self.chatDetails[wViewChairId]:runAction(cc.Sequence:create(
	    	cc.DelayTime:create(3),
	    	cc.CallFunc:create(function(ref)
	    		self.chatDetails[wViewChairId]:removeFromParent()
				self.chatDetails[wViewChairId] = nil
				self.chatBubble[wViewChairId]:setVisible(false)
	    	end)))
    end
end

function GameViewLayer:onUserDyExpression(fromViewID, toViewID, index)
	-- 语音动画
    local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.1
    param.m_strName = cmd.VOICE_ANIMATION_KEY
    local animate = AnimationMgr.getAnimate(param)
    -- local aAnimation = cc.RepeatForever:create(animate)

    local runScene = cc.Director:getInstance():getRunningScene()
    local sp = cc.Sprite:create()
    sp:setPosition(cc.p(667, 375))
    runScene:addChild(sp, 15)

    sp:runAction(animate)
end

function GameViewLayer:onUserVoiceStart(viewId)
	--取消上次
	if self.chatDetails[viewId] then
		self.chatDetails[viewId]:stopAllActions()
		self.chatDetails[viewId]:removeFromParent()
		self.chatDetails[viewId] = nil
	end
    -- 语音动画
    local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.1
    param.m_strName = cmd.VOICE_ANIMATION_KEY
    local animate = AnimationMgr.getAnimate(param)
    self.m_actVoiceAni = cc.RepeatForever:create(animate)

    self.chatDetails[viewId] = display.newSprite("#blank.png")
		:setAnchorPoint(cc.p(0.5, 0.5))
		:addTo(self, 19)
	if viewId == 4 then
		self.chatDetails[viewId]:setRotation(180)
	end


	self.chatBubble[viewId]:setContentSize(200, 70)
			:setVisible(true)

	local size = self.chatBubble[viewId]:getContentSize()
	if viewId == 4 then
		self.chatDetails[viewId]:setPosition(posqipaoChat[viewId].x - size.width/2, posqipaoChat[viewId].y-35)
	else
		self.chatDetails[viewId]:setPosition(posqipaoChat[viewId].x + size.width/2, posqipaoChat[viewId].y-35)
	end
	

	self.chatDetails[viewId]:runAction(self.m_actVoiceAni)

	self:runAction(cc.Sequence:create(
		cc.DelayTime:create(15),
		cc.CallFunc:create(function()
			if self.chatDetails[viewId] then
			    self.chatDetails[viewId]:removeFromParent()
			    self.chatDetails[viewId] = nil
			    self.chatBubble[viewId]:setVisible(false)
			end
		end)
		))
    --改变气泡大小
	self.chatBubble[viewId]:setVisible(true)
end

function GameViewLayer:onUserVoiceEnded(viewId)
	if self.chatDetails[viewId] then
	    self.chatDetails[viewId]:removeFromParent()
	    self.chatDetails[viewId] = nil
	    self.chatBubble[viewId]:setVisible(false)
	end
end

function GameViewLayer:playLiujuEffect()
	local armature = self:getChildByName("effect_ui_duijukaishi01")
	if armature then
		armature:getAnimation():play("Animation1")
		return
	end
	-- 加载动画所用到的数据
	local exportJsonName = "liujueffect/effect_text_draw.ExportJson"
	local picName = "liujueffect/effect_text_draw0.png"
	local plistName = "liujueffect/effect_text_draw0.plist"
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(picName, plistName, exportJsonName);

	local armatureName = "effect_text_draw"
	
	-- 创建动画对象
	armature = ccs.Armature:create(armatureName)  
	armature:setPosition(cc.p(yl.WIDTH/2, yl.HEIGHT/2))               
	-- 设置动画对象执行的动画名称
	armature:getAnimation():play("Animation1")     
	armature:setName("LIUJU_effect_text_draw")
	-- 把动画对象加载到场景内
	self:addChild(armature, 40)                  
end

function GameViewLayer:playStartGameEffect()
	local armature = self:getChildByName("GAMESTART_effect_ui_duijukaishi01")
	if armature then
		armature:getAnimation():play("Animation1")
		return
	end
	-- 加载动画所用到的数据
	local exportJsonName = "gamestarteffect/effect_ui_duijukaishi01.ExportJson"
	local picName = "gamestarteffect/effect_ui_duijukaishi010.png"
	local plistName = "gamestarteffect/effect_ui_duijukaishi010.plist"
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(picName, plistName, exportJsonName);

	picName = "gamestarteffect/effect_ui_duijukaishi011.png"
	plistName = "gamestarteffect/effect_ui_duijukaishi011.plist"
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(picName, plistName, exportJsonName);

	picName = "gamestarteffect/effect_ui_duijukaishi012.png"
	plistName = "gamestarteffect/effect_ui_duijukaishi012.plist"
	ccs.ArmatureDataManager:getInstance():addArmatureFileInfo(picName, plistName, exportJsonName);

	local armatureName = "effect_ui_duijukaishi01"
	
	-- 创建动画对象
	armature = ccs.Armature:create(armatureName)
	armature:setPosition(cc.p(yl.WIDTH/2, yl.HEIGHT/2))
	-- 设置动画对象执行的动画名称
	armature:getAnimation():play("Animation1")
	armature:setName("GAMESTART_effect_ui_duijukaishi01")
	-- 把动画对象加载到场景内
	self:addChild(armature, 40)
end

function GameViewLayer:onButtonClickedEvent(tag, ref)
	if tag == GameViewLayer.BT_START then
		self.btStart:setVisible(false)
		self:showTableBt(false)
		--隐藏结算界面
		self._resultLayer:hideLayer()
		self:onResetData()
		if not PriRoom:getInstance():getPlazaScene().m_gameInResultLayer then
			self._scene:sendGameStart()
		end
	elseif tag == GameViewLayer.BT_SWITCH then
		-- self:showTableBt(true)
		self._setLayer:showLayer()
	elseif tag == GameViewLayer.BT_JIESAN then
		if GlobalUserItem.bPrivateRoom then
			PriRoom:getInstance():queryDismissRoom()
		else
			self._scene:onQueryExitGame()
		end
	elseif tag == GameViewLayer.BT_CHAT then
		self:showTableBt(false)
		self.chatNode:show()
		-- local data2 = {0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x11, 0x12, 0x14, 0x17, 0x19, 0x19, 0x25,
		-- 		0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x11, 0x12, 0x14, 0x17, 0x19, 0x19, 0x25}
		-- self:setListeningCard(data2)

		--self._chatLayer:setLocalZOrder(yl.MAX_INT)
	elseif tag == GameViewLayer.BT_SET then
		self:showTableBt(false)
		self._setLayer:showLayer()
		--self._setLayer:setLocalZOrder(yl.MAX_INT)
	elseif tag == GameViewLayer.BT_HOWPLAY then
		self:showTableBt(false)
        self._scene._scene:popHelpLayer(yl.HTTP_URL .. "/Mobile/Introduce.aspx?kindid=389&typeid=0")
		-- self._scene._scene:popHelpLayer(cmd.KIND_ID, 0)
		-- local data1 = {0x11, 0x08, 0x06, 0x09, 0x08, 0x02, 0x02, 0x07}
		-- local data2 = {0x02, 0x03, 0x04, 0x04, 0x05, 0x06, 0x11, 0x12, 0x14, 0x17, 0x19, 0x19, 0x25, 0x36}
		-- local data3 = {0x22, 0x22, 0x22, 0x19, 0x19}
		-- local data4 = {0x01, 0x03, 0x05, 0x15, 0x16, 0x17, 0x24, 0x24, 0x25, 0x25, 0x25, 0x27, 0x36, 0x29}
		-- local data5 = {1, 1, 1, 6, 7, 8, 9, 18, 19, 20, 33, 34, 35, 53}
		-- for i = 1, cmd.GAME_PLAYER do
		-- 	self._cardLayer:setHandCard(i, 14, data5)
		-- end
	elseif tag == GameViewLayer.BT_EXIT then
		-- self._cardLayer:bumpOrBridgeCard(1, {1, 1, 1}, GameLogic.SHOW_PENG)
		-- self._cardLayer:bumpOrBridgeCard(2, {1, 1, 1, 1}, GameLogic.SHOW_PENG)
		--self._cardLayer:bumpOrBridgeCard(3, {1, 1, 1, 1}, GameLogic.SHOW_AN_GANG)
		-- self._cardLayer:bumpOrBridgeCard(4, {1, 1, 1, 1}, GameLogic.SHOW_FANG_GANG)

		if self._scene._isReplay then
			self._scene:closeReplay()
		elseif GlobalUserItem.bPrivateRoom then
			PriRoom:getInstance():getNetFrame()._gameFrame:setEnterAntiCheatRoom(false)                
	        PriRoom:getInstance():getNetFrame()._gameFrame:onCloseSocket()
	        PriRoom:getInstance():setViewFrame(nil)
	        self._scene:onExitRoom()
	    else
			self._scene:onQueryExitGame()
		end
	elseif tag == GameViewLayer.BT_TRUSTEE then
		self:showTableBt(false)
		self._scene:sendUserTrustee(true)
	elseif tag == GameViewLayer.BT_TRUSTEECANCEL then
		self._scene:sendUserTrustee(false)

   elseif tag == GameViewLayer.BT_CHI then
		--TODO 吃牌选择未完成前注释
		local eatChoices = self._cardLayer:getCanChiCombin(self.cbActionCard)
		if #eatChoices == 0 then
			assert(false, "eatChoices can not == 0")
		elseif #eatChoices == 1 then
			self._scene:sendOperateCard(eatChoices[1][1], {eatChoices[1][2], eatChoices[1][3], eatChoices[1][4]})
		elseif #eatChoices > 1 then
			self:popupChoices(eatChoices)
		end
		--TODO 吃牌选择未完成前注释

		-- local typeTemp = self._cardLayer:getEatCardType(self.cbActionCard)

		-- local cbOperateCard = nil
		
		-- if GameLogic.WIK_RIGHT == typeTemp then  --右吃
		-- 	cbOperateCard = {self.cbActionCard, self.cbActionCard - 2, self.cbActionCard - 1}
		-- elseif GameLogic.WIK_CENTER == typeTemp then  --中吃
		-- 	cbOperateCard = {self.cbActionCard, self.cbActionCard - 1, self.cbActionCard + 1}
		-- elseif GameLogic.WIK_LEFT <= typeTemp then --左吃
		-- 	cbOperateCard = {self.cbActionCard, self.cbActionCard + 1, self.cbActionCard + 2}
		-- end

		-- self._scene:sendOperateCard(typeTemp, cbOperateCard) --TODO 吃牌选择未完成前解开

		self:HideGameBtn()
	elseif tag == GameViewLayer.BT_BUMP then
		--发送碰牌
		local cbOperateCard = {self.cbActionCard, self.cbActionCard, self.cbActionCard}
		self._scene:sendOperateCard(GameLogic.WIK_PENG, cbOperateCard)

		self:HideGameBtn()
	elseif tag == GameViewLayer.BT_BRIGDE then
		local cbGangChoices = self._cardLayer:getCanGangCombin(self.cbActionCard, cmd.MY_VIEWID, self.isOtherOp)
		if #cbGangChoices == 0 then
			showToast(self._scene._scene, "出现未知错误，请截图，并发送给群主，然后杀进程，重连游戏 参数：<" .. tostring(self.cbActionCard) .. "> <" .. tostring(self.isOtherOp) .. ">", 20000)
			assert(false, "gang choices can not == 0")
		elseif #cbGangChoices == 1 then
			self._scene:sendOperateCard(cbGangChoices[1][1], {cbGangChoices[1][2], cbGangChoices[1][3], cbGangChoices[1][4]})
		elseif #cbGangChoices > 1 then
			self:popupChoices(cbGangChoices)
		end

		self:HideGameBtn()
	elseif tag == GameViewLayer.BT_WIN then
		local cbOperateCard = {self.cbActionCard, 0, 0}
		self._scene:sendOperateCard(GameLogic.WIK_CHI_HU, cbOperateCard)

		self:HideGameBtn()
	elseif tag == GameViewLayer.BT_PASS then
		local cbOperateCard = {0, 0, 0}
		self._scene:sendOperateCard(GameLogic.WIK_NULL, cbOperateCard)

		self:HideGameBtn()
	elseif tag == GameViewLayer.TAG_BTN_RULE then
		self.ruleDisplayLayout:setVisible(not self.ruleDisplayLayout:isVisible())
	else
		print("default")
	end

	if tag ~= GameViewLayer.TAG_BTN_RULE then
		self.ruleDisplayLayout:setVisible(false)
	end
end

--弹出选择吃牌或者杠牌的popup选择框
function GameViewLayer:popupChoices(choices)
	self:removeChoicesPopup()

	self.isChoicesPopuped = true
	--麻将的缩放比例
	local scale = 0.8
	local screenSize = cc.Director:getInstance():getVisibleSize()
	--单个麻将的宽
	local mjWidth = 80 * scale
	local mjheight = 150 * scale

	--每个选择框的间隔
	local span = 40 * scale
	local preDis = 10 * scale

	--选择数
	local count = #choices
	local perCount = #choices[1]
	--因为第一个为操作码，不算牌数
	perCount = perCount - 1
	--每个选择框的宽度
	local perWidth = mjWidth * perCount

	--起始坐标
	local stx = 0

	if count % 2 == 0 then
		stx = screenSize.width * 0.5 - (perWidth + span * 0.5) - (count / 2 - 1) * (perWidth + span) - preDis
	else
		stx = screenSize.width * 0.5 - math.floor(count / 2) * (perWidth + span) - perWidth * 0.5 - preDis
	end

	local popupOp = nil
	local popupCards = {}
	for i, v in ipairs(choices) do
		for j,t in ipairs(v) do
			--第一个不是牌，是操作码
			if j ~= 1 then
				--如果是吃牌操作，被吃牌的牌，不纳入弹出的牌, 被吃的牌index=2, 吃的牌index = 3 , 4
				if (v[1] == GameLogic.WIK_LEFT or v[1] == GameLogic.WIK_CENTER or v[1] == GameLogic.WIK_RIGHT) and j == 2 then

				else
					--判断是否已经存在弹出的牌
					local exist = false
					for l, p in ipairs(popupCards) do
						if p == t then
							exist = true
							break
						end
					end

					--如果是吃牌，则不重复显示
					if v[1] == GameLogic.WIK_LEFT or v[1] == GameLogic.WIK_CENTER or v[1] == GameLogic.WIK_RIGHT then
						if exist == false then
							table.insert(popupCards, t)
						end
					else
					--如果是杠牌，则需要重复显示
						table.insert(popupCards, t)
					end
				end
			end
		end
		popupOp = v[1]
	end
	--弹出手牌
	self._cardLayer:popupCard(popupCards, popupOp)

	local choicesTemp = choices
	for i, v in ipairs(choicesTemp) do
		local popup = ChoicesPopupNode:create(v, self)
		local px = stx + (i - 1) * (span + perWidth)
		popup.rootNode:setScale(scale)
		popup:setPosition(px, 240)
		self:addChild(popup, 100)
		table.insert(self.choicesPopupArray, popup)
	end
end

function GameViewLayer:removeChoicesPopup()
	for k,v in pairs(self.choicesPopupArray) do
		v:removeFromParent()
	end
	self.choicesPopupArray = {}
	self.isChoicesPopuped = false

	--所有手牌都向下排列
	self._cardLayer:unpopupAllCard()
end

function GameViewLayer:hideAllClockPoint()
	local dong = self.spClock:getChildByName("Dong")
	dong:stopAllActions()
	dong:setVisible(false)
	local nan = self.spClock:getChildByName("Nan")
	nan:stopAllActions()
	nan:setVisible(false)
	local xi = self.spClock:getChildByName("Xi")
	xi:stopAllActions()
	xi:setVisible(false)
	local bei = self.spClock:getChildByName("Bei")
	bei:stopAllActions()
	bei:setVisible(false)
end

--将指标指向玩家
function GameViewLayer:clockPointToChairId(chairId)
	self._currChairId = chairId
	--隐藏所有的
	self:hideAllClockPoint()

	local dircName = ""
	if chairId == 0 then
		dircName = "Dong"
	elseif chairId == 1 then
		dircName = "Nan"
	elseif chairId == 2 then
		dircName = "Xi"
	elseif chairId == 3 then
		dircName = "Bei"
	end
	if dircName ~= "" and self._scene.isGameEnd == false then
		local dNode = self.spClock:getChildByName(dircName):setVisible(true)

		dNode:runAction(
			cc.RepeatForever:create(
				cc.Sequence:create(
					cc.Show:create(),
					cc.DelayTime:create(0.5),
					cc.Hide:create(),
					cc.DelayTime:create(0.5)
				)
			)
		)

	end
end

--计时器刷新
function GameViewLayer:OnUpdataClockView(chairId, time, nChair, isMustNchair)
	if not chairId or chairId == yl.INVALID_CHAIR or not time then
		self.asLabTime:setString(0)
		self.asLabTime:setPosition(669, 403)
	else
		self.asLabTime:setString(time)
		if tonumber(time) >= 10 and tonumber(time) < 20 then
			self.asLabTime:setPosition(662, 403)
		else
			self.asLabTime:setPosition(669, 403)
		end

		--如果不等于当前的座位号，则需要更新
		if chairId ~= self._currChairId then
			self:clockPointToChairId(chairId)
		end
	end
	--自己的实际位置
	-- local factChairID = self._scene:GetMeChairID()
	-- if not isMustNchair then
	-- 	nChair = self._scene.cbPlayerCount
	-- end

	-- if not chairId or chairId == yl.INVALID_CHAIR or not time then
	-- 	self.asLabTime:setString(0)
	-- 	self.asLabTime:setPosition(669, 403)
	-- else
	-- 	self.asLabTime:setString(time)
	-- 	if tonumber(time) >= 10 and tonumber(time) < 20 then
	-- 		self.asLabTime:setPosition(662, 403)
	-- 	else
	-- 		self.asLabTime:setPosition(669, 403)
	-- 	end

	-- 	if nChair == 3 and factChairID ~= nil then  --3人场
	-- 		self.spClock:setScale(1.2)
	-- 		local res = string.format("direction/direction0%d.png", chairId+1)
	-- 		if factChairID == 0 then --东
	-- 			if chairId == 0 then --出牌的是东
	-- 				--不旋转
	-- 				self.spClock:setRotation(0)
	-- 			elseif chairId == 1 then --出牌的是南
	-- 				self.spClock:setRotation(-120)
	-- 			elseif chairId == 2 then --出牌的是北
	-- 				self.spClock:setRotation(120)
	-- 			end
	-- 		elseif factChairID == 1 then
	-- 			if chairId == 0 then --出牌的是东
	-- 				self.spClock:setRotation(120)
	-- 			elseif chairId == 1 then --出牌的是南
	-- 				self.spClock:setRotation(0)
	-- 			elseif chairId == 2 then --出牌的是北
	-- 				self.spClock:setRotation(-120)
	-- 			end
	-- 		elseif factChairID == 2 then
	-- 			if chairId == 0 then --出牌的是东
	-- 				self.spClock:setRotation(-120)
	-- 			elseif chairId == 1 then --出牌的是南
	-- 				self.spClock:setRotation(120)
	-- 			elseif chairId == 2 then --出牌的是北
	-- 				self.spClock:setRotation(0)
	-- 			end
	-- 		end
	-- 		self.spClock:setTexture(res)
	-- 	elseif nChair == 4 and factChairID ~= nil then --4人场
	-- 		local res = string.format("direction/direction4_0%d.png", chairId+1)
	-- 		self.spClock:setScale(1.0)
	-- 		if factChairID == 0 then --东
	-- 			self.spClock:setRotation(0)
	-- 		elseif factChairID == 1 then
	-- 			self.spClock:setRotation(90)
	-- 		elseif factChairID == 2 then
	-- 			self.spClock:setRotation(180)
	-- 		elseif factChairID == 3 then
	-- 			self.spClock:setRotation(270)
	-- 		end
	-- 		self.spClock:setTexture(res)
	-- 	else
	-- 		assert("错误人数场 ------ cmd.GAME_PLAYER  " .. cmd.GAME_PLAYER)
	-- 	end
	-- end
end

--开始
function GameViewLayer:gameStart(startViewId, wHeapHead, cbCardData, cbCardCount, cbSiceCount1, cbSiceCount2, otherCbCardData)
	self:onResetData(true)
	--self:runSiceAnimate(cbSiceCount1, cbSiceCount2, function()
		self._cardLayer:sendCard(cbCardData, cbCardCount, otherCbCardData)
	--end)
end
--用户出牌
function GameViewLayer:gameOutCard(viewId, card, isReconnected)
	-- local spFlag = self:getChildByTag(GameViewLayer.SP_OPERATFLAG)
	-- if spFlag then
	-- 	spFlag:removeFromParent()
	-- end	

	self.hasSomeOneNeed = true

	--延迟显示
	-- self:runAction(
	-- 	cc.Sequence:create(
	-- 		cc.DelayTime:create(0.1),
	-- 		cc.CallFunc:create(function()
	-- 			if self.hasSomeOneNeed then
	-- 				self:showCardPlate(viewId, card)
	-- 			end
	-- 		end)
	-- 		))

	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._cardLayer._mjSkin

	--不是自己才显示
	if viewId ~= cmd.MY_VIEWID or isReconnected or self._scene._isReplay then
		-- self:showCardPlate(viewId, card)
		--从右手边打出
		local endCard = self._cardLayer.nodeHandCard[viewId]:getChildByTag(1)
		local originX = endCard:getPositionX()
		local originY = endCard:getPositionY()
		if endCard and originX and originY then
			local nValue = math.mod(card, 16)
			local nColor = math.floor(card/16)
			local cardFile = skinPath .. "/card_color_" .. nColor .. "_num_" .. nValue .. ".png"

			local cardBgFile = skinPath .. "/pai_bg_dian.png"

			--底板
			local runScene = cc.Director:getInstance():getRunningScene()
			local worldPosition = endCard:getParent():convertToWorldSpace(cc.p(endCard:getPositionX(), endCard:getPositionY()))
			local nodePosition = self:convertToNodeSpace(worldPosition)

			local moveCard = cc.Sprite:create(cardBgFile)
			moveCard:setPosition(nodePosition)
			local name = "VIEWID_" .. viewId .. "_MOVE_CARD"
			if viewId == cmd.MY_VIEWID then
				name = "MY_VIEWID_MOVE_CARD"
			end
			moveCard:setName(name)
			self:addChild(moveCard, 15)

			local spPlatecard = cc.Sprite:create(cardFile)
			moveCard:addChild(spPlatecard)

			spPlatecard:setPosition(moveCard:getContentSize().width/2, moveCard:getContentSize().height/2 + 18)

			--牌移动到桌面
			moveCard:runAction(
				cc.Sequence:create(
					cc.MoveTo:create(0.1, posPlate[viewId]),
					cc.CallFunc:create(function()
						
					end)
					))
		end
	end
	
	if not isReconnected then
		self._cardLayer:removeHandCard(viewId, {card}, true)
	end

	self.cbOutCardTemp = card
	self.cbOutUserTemp = viewId
end
--用户抓牌
function GameViewLayer:gameSendCard(viewId, card, bTail)
	self.hasSomeOneNeed = false

	--隐藏上把出的牌
	if self.cbOutUserTemp then
		--如果是自己，则把牌平移到牌堆
		if self.cbOutUserTemp == cmd.MY_VIEWID then
			local runScene = cc.Director:getInstance():getRunningScene()
			local disCard = self:getChildByName("MY_VIEWID_MOVE_CARD")
			if disCard then
				--移动到弃牌区
				--目标点
				local mpos = self._cardLayer:getDiscardPosition(self.cbOutUserTemp)
				local m1 = cc.MoveTo:create(0.1, mpos)
				local s1 = cc.ScaleTo:create(0.1, 0.4)

				local outer = self.cbOutUserTemp
				local outCard = self.cbOutCardTemp

				local call = cc.CallFunc:create(function()
					--显示被弃的牌
					--把上一个人打出的牌丢入弃牌堆
					if outer ~= 0 and not bTail then
						self._cardLayer:discard(outer, outCard)
						outer = nil
						outCard = 0
					end
				end)
				local rm = cc.RemoveSelf:create()
				local spawn = cc.Spawn:create(m1, s1)
				local delayt = cc.DelayTime:create(0.5)
				local se = cc.Sequence:create(delayt, spawn, call, rm)
				-- disCard:stopAllActions()
				
				disCard:runAction(se)
			end
		else
			local runScene = cc.Director:getInstance():getRunningScene()

			local disCard = self:getChildByName("VIEWID_" .. self.cbOutUserTemp .. "_MOVE_CARD")
			if disCard then
				--移动到弃牌区
				--目标点
				local mpos = self._cardLayer:getDiscardPosition(self.cbOutUserTemp)
				local m1 = cc.MoveTo:create(0.1, mpos)
				local s1 = cc.ScaleTo:create(0.1, 0.4)

				local outer = self.cbOutUserTemp
				local outCard = self.cbOutCardTemp

				local call = cc.CallFunc:create(function()
					--显示被弃的牌
					--把上一个人打出的牌丢入弃牌堆
					if self.cbOutCardTemp ~= 0 and not bTail then
						self._cardLayer:discard(outer, outCard)
						outer = nil
						outCard = 0
					end
				end)
				local rm = cc.RemoveSelf:create()
				local spawn = cc.Spawn:create(m1, s1)
				local delayt = cc.DelayTime:create(0.5)
				local se = cc.Sequence:create(delayt, spawn, call, rm)
				-- disCard:stopAllActions()
				disCard:runAction(se)
			end
			-- self:showCardPlate(nil)
		end

	end
	

	self:showOperateFlag(nil)
		

	self:removeChoicesPopup()
	--当前的人抓牌
	self._cardLayer:catchCard(viewId, card, bTail)
end
--摇骰子
function GameViewLayer:runSiceAnimate(cbSiceCount1, cbSiceCount2, callback)
	local str1 = string.format("sice_red_%d", cbSiceCount1)
	local str2 = string.format("sice_white_%d", cbSiceCount2)
	local siceX1 = 667 - 320 + math.random(640) - 35
	local siceY1 = 375 - 120 + math.random(240) + 43
	local siceX2 = 667 - 320 + math.random(640) - 35
	local siceY2 = 375 - 120 + math.random(240) + 43
	display.newSprite()
		:move(siceX1, siceY1)
		:setTag(GameViewLayer.SP_SICE1)
		:addTo(self, 0)
		:runAction(cc.Sequence:create(
			self:getAnimate(str1),
			cc.DelayTime:create(1),
			cc.CallFunc:create(function(ref)
				--ref:removeFromParent()
			end)))
	display.newSprite()
		:move(siceX2, siceY2)
		:setTag(GameViewLayer.SP_SICE2)
		:addTo(self, 0)
		:runAction(cc.Sequence:create(
			self:getAnimate(str2),
			cc.DelayTime:create(1),
			cc.CallFunc:create(function(ref)
				--ref:removeFromParent()
				if callback then
					callback()
				end
			end)))
	self._scene:PlaySound(cmd.RES_PATH.."sound/DRAW_SICE.mp3")
end

function GameViewLayer:sendCardFinish()
	local spSice1 = self:getChildByTag(GameViewLayer.SP_SICE1)
	if spSice1 then
		spSice1:removeFromParent()
	end
	local spSice2 = self:getChildByTag(GameViewLayer.SP_SICE2)
	if spSice2 then
		spSice2:removeFromParent()
	end	
	self._scene:sendCardFinish()
end

function GameViewLayer:gameConclude()
    for i = 1, cmd.GAME_PLAYER do
		self:setUserTrustee(i, false)
	end
	self._cardLayer:gameEnded()
end

function GameViewLayer:HideGameBtn()
	if self.opNode ~= nil then
		self.opNode:hide()
	end
end

--识别动作掩码
--cbActionMask 动作掩码
--cbCardData 操作的牌
--isOtherOp 是否是别人打的
function GameViewLayer:recognizecbActionMask(cbActionMask, cbCardData, isOtherOp, viewId, opMask)
	if cbActionMask == GameLogic.WIK_NULL or cbActionMask == 32 then
		assert("false")
		return false
	end

	if self._cardLayer:isUserMustWin() then
		--必须胡牌的情况
	end

	if cbCardData then
		self.cbActionCard = cbCardData
	end

	local btnList = {}
	self.isOtherOp = isOtherOp
	if cbActionMask >= 128 then 				--放炮
		cbActionMask = cbActionMask - 128
		btnList["HU"] = true
	end
	if cbActionMask >= 64 then 					--胡
		cbActionMask = cbActionMask - 64
		btnList["HU"] = true
	end
	if cbActionMask >= 32 then 					--听
		cbActionMask = cbActionMask - 32
		btnList["TING"] = true
	end
	if cbActionMask >= 16 then 					--杠
		cbActionMask = cbActionMask - 16
		btnList["GANG"] = true
	end
	if cbActionMask >= 8 then 					--碰
		cbActionMask = cbActionMask - 8
		if self._cardLayer:isUserCanBump() then
			btnList["PENG"] = true
		end
	end
    if cbActionMask >= 4 then 					--吃
		cbActionMask = cbActionMask - 4
		if self._cardLayer:isUserCanBump() then
			btnList["CHI"] = true
		end
	end

	if cbActionMask >= 2 then
		cbActionMask = cbActionMask - 2
		if self._cardLayer:isUserCanBump() then
			btnList["CHI"] = true
		end
	end

	if cbActionMask >= 1 then
		cbActionMask = cbActionMask - 1
		if self._cardLayer:isUserCanBump() then
			btnList["CHI"] = true
		end
	end

	local opName = ""
	if opMask then
		if opMask >= 128 then 				--放炮
			opMask = opMask - 128
			opName = "HU"
		elseif opMask >= 64 and opName == "" then 					--胡
			opMask = opMask - 64
			opName = "HU"
		elseif opMask >= 32 and opName == "" then 					--听
			opMask = opMask - 32
			opName = "TING"
		elseif opMask >= 16 and opName == "" then 					--杠
			opMask = opMask - 16
			opName = "GANG"
		elseif opMask >= 8 and opName == "" then 					--碰
			opMask = opMask - 8
			opName = "PENG"
		elseif opMask >= 4 and opName == "" then 					--吃
			opMask = opMask - 4
			opName = "CHI"
		elseif opMask >= 2 and opName == "" then
			opMask = opMask - 2
			opName = "CHI"
		elseif opMask >= 1 and opName == "" then
			opMask = opMask - 1
			opName = "CHI"
		elseif opMask == 0 and opName == "" then 					--过
			opName = "GUO" 		
		end
	end
	
	if self._scene._isReplay and not GameplayerManager:getInstance():isTest() then
		self.opNode:hide()
	elseif self._scene._isReplay then
		self.opNode:show(btnList, viewId, opName)
	else
		self.opNode:show(btnList)
	end

	self._scene:SetGameOperateClock() --如果自己可以操作，那么指针指向自己

	return true
end

function GameViewLayer:getAnimate(name, bEndRemove)
	local animation = cc.AnimationCache:getInstance():getAnimation(name)
	local animate = cc.Animate:create(animation)

	if bEndRemove then
		animate = cc.Sequence:create(animate, cc.CallFunc:create(function(ref)
			ref:removeFromParent()
		end))
	end

	return animate
end
--设置听牌提示
function GameViewLayer:setListeningCard(cbCardData)
	if cbCardData == nil then
		self.spListenBg:setVisible(false)
		return
	end
	assert(type(cbCardData) == "table")

	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._cardLayer._mjSkin

	self.spListenBg:removeAllChildren()
	--不提示听牌后设置为false
	self.spListenBg:setVisible(true)

	local cbCardCount = #cbCardData
	local bTooMany = (cbCardCount >= 16)
	--拼接块
	local width = 40
	local height = 58
	local posX = 327
	local fSpacing = 60
	if not bTooMany then
		for i = 1, fSpacing*cbCardCount do
			display.newSprite(cmd.RES_PATH .. "gamesceneplist/sp_listenBg_2.png")
				:move(posX, 46.5)
				:setAnchorPoint(cc.p(0, 0.5))
				:addTo(self.spListenBg)
			posX = posX + 1
			if i > 700 then
				break
			end
		end
	end
	--尾块
	display.newSprite(cmd.RES_PATH .. "gamesceneplist/sp_listenBg_3.png")
		:move(posX, 46.5)
		:setAnchorPoint(cc.p(0, 0.5))
		:addTo(self.spListenBg)
	--可胡牌过多，屏幕摆不下
	if bTooMany then
		local cardBack = display.newSprite(skinPath .. "/pai_bg_dian_chu.png")
		cardBack:move(183 + 40, 46)
		cardBack:addTo(self.spListenBg)

		local cardFont = display.newSprite(skinPath ..  "/card_color_7_num_1.png")
		cardFont:setScale(0.4)
		cardFont:move(width/2, height/2 + 7)
		cardFont:addTo(cardBack)

		local strFilePrompt = ""
		local spListenCount = nil
		if cbCardCount == 28 then 		--所有牌
			strFilePrompt = cmd.RES_PATH .. "gamesceneplist/389_sp_listen_anyCard.png"
		else
			strFilePrompt = cmd.RES_PATH .. "gamesceneplist/389_sp_listen_manyCard.png"
			spListenCount = cc.Label:createWithTTF(cbCardCount.."", "fonts/round_body.ttf", 30)
		end

		local spPrompt = display.newSprite(strFilePrompt)
		spPrompt:move(183 + 110, 46)
		spPrompt:setAnchorPoint(cc.p(0, 0.5))
		spPrompt:addTo(self.spListenBg)
		if spListenCount then
			spListenCount:move(70, 12):addTo(spPrompt)
		end
	end
	--牌、番、数
	self.cbAppearCardIndex = GameLogic.DataToCardIndex(self._scene.cbAppearCardData)
	for i = 1, cbCardCount do
		if bTooMany then
			break
		end
		local tempX = fSpacing*(i - 1)
		--local rectX = self._cardLayer:switchToCardRectX(cbCardData[i])
		local cbCardIndex = GameLogic.SwitchToCardIndex(cbCardData[i])
		local nLeaveCardNum = 4 - self.cbAppearCardIndex[cbCardIndex]
		--牌底
		local card = display.newSprite(skinPath .. "/pai_bg_dian_chu.png")
		card:move(183 + tempX, 46)
		card:addTo(self.spListenBg)
		--字体
		local nValue = math.mod(cbCardData[i], 16)
		local nColor = math.floor(cbCardData[i]/16)
		local strFile = skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png"
		local cardFont = display.newSprite(strFile)
		cardFont:setScale(0.4)
		cardFont:move(width/2, height/2 + 7)
		cardFont:addTo(card)
	end
end

--减少可听牌数
function GameViewLayer:reduceListenCardNum(cbCardData)
	local cbCardIndex = GameLogic.SwitchToCardIndex(cbCardData)
	if #self.cbAppearCardIndex == 0 then
		self.cbAppearCardIndex = GameLogic.DataToCardIndex(self._scene.cbAppearCardData)
	end
	self.cbAppearCardIndex[cbCardIndex] = self.cbAppearCardIndex[cbCardIndex] + 1
	local labelLeaveNum = self.spListenBg:getChildByTag(cbCardIndex)
	if labelLeaveNum then
		local nLeaveCardNum = 4 - self.cbAppearCardIndex[cbCardIndex]
		labelLeaveNum:setString(nLeaveCardNum.."")
	end
end

function GameViewLayer:setBanker(viewId)
	if viewId < 1 or viewId > cmd.GAME_PLAYER then
		return false
	end
	
	local spBanker = self.nodePlayer[viewId]:getChildByTag(GameViewLayer.SP_BANKER)
	spBanker:setVisible(true)
	return true
end

function GameViewLayer:setCurrUser( viewId, chairId )

end

function GameViewLayer:setUserTrustee(viewId, bTrustee)
	if viewId == cmd.MY_VIEWID then
		self.spTrusteeCover:setVisible(bTrustee)
	end
end

--设置房间信息
function GameViewLayer:setRoomInfo(tableId, chairId)
end

function GameViewLayer:onTrusteeTouchCallback(event, x, y)
	if not self.spTrusteeCover:isVisible() then
		return false
	end

	local rect = self.spTrusteeCover:getChildByTag(GameViewLayer.SP_TRUSTEEBG):getBoundingBox()
	if cc.rectContainsPoint(rect, cc.p(x, y)) then
		return true
	else
		return false
	end
end
--设置剩余牌
function GameViewLayer:setRemainCardNum(num)
	local shengyuBg = self:getChildByName("Sprite_25")
	if not GlobalUserItem.bPrivateRoom then
		shengyuBg:setPosition(cc.p(10, 730))
	end

	if self._scene._isReplay then
		shengyuBg:setVisible(false)
	end

	local labelNum = shengyuBg:getChildByName("AtlasLabel_5")
	labelNum:setString(num.."")
	-- if num == 112 then
	-- 	text:setVisible(false)
	-- else
	-- 	text:setVisible(true)
	-- end
end

function GameViewLayer:showHuCard(viewId, cbCardData)
	if nil == viewId then
		self.spHuCardPlate:setVisible(false)
		return
	end 
	self.spHuCardPlate:stopAllActions()
	--local rectX = self._cardLayer:switchToCardRectX(cbCardData)
	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._cardLayer._mjSkin
	local nValue = math.mod(cbCardData, 16)
	local nColor = math.floor(cbCardData/16)
	local strFile = skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png"

	local spPlatecard = self.spHuCardPlate:getChildByTag(GameViewLayer.SP_PLATECARD)
	spPlatecard:setTexture(strFile)
	self.spHuCardPlate:move(posPlate[viewId]):setVisible(true)
	local plateX = 60
	local plateY = 84

	if self._cardLayer._mjSkin == 2 then
		plateY = 88
	elseif self._cardLayer._mjSkin == 3 then
		plateX = 61
		plateY = 88
	end

	spPlatecard:setPosition(plateX, plateY)
end

--牌托
function GameViewLayer:showCardPlate(viewId, cbCardData)
	if nil == viewId then
		self.spCardPlate:setVisible(false)
		local runScene = cc.Director:getInstance():getRunningScene()
		local disCard = self:getChildByName("MY_VIEWID_MOVE_CARD")
		if disCard then
			disCard:removeFromParent()
		end

		local disCard1 = self:getChildByName("VIEWID_1_MOVE_CARD")
		if disCard1 then
			disCard1:removeFromParent()
		end

		local disCard2 = self:getChildByName("VIEWID_2_MOVE_CARD")
		if disCard2 then
			disCard2:removeFromParent()
		end

		local disCard4 = self:getChildByName("VIEWID_4_MOVE_CARD")
		if disCard4 then
			disCard4:removeFromParent()
		end

		return
	end 
	self.spCardPlate:stopAllActions()
	--local rectX = self._cardLayer:switchToCardRectX(cbCardData)
	local skinPath = cmd.RES_PATH .. "game/mahjongCard_" .. self._cardLayer._mjSkin
	local nValue = math.mod(cbCardData, 16)
	local nColor = math.floor(cbCardData/16)
	local strFile = skinPath .. "/card_color_"..nColor.."_num_"..nValue..".png"
	local spPlatecard = self.spCardPlate:getChildByTag(GameViewLayer.SP_PLATECARD)
	spPlatecard:setTexture(strFile)
	self.spCardPlate:move(posPlate[viewId]):setVisible(true)
	local plateX = 60
	local plateY = 84

	if self._cardLayer._mjSkin == 2 then
		plateY = 88
	elseif self._cardLayer._mjSkin == 3 then
		plateX = 61
		plateY = 88
	end

	spPlatecard:setPosition(plateX, plateY)

	
end
--操作效果
function GameViewLayer:showOperateFlag(viewId, operateCode, isZiMo)
	local spFlag = self:getChildByTag(GameViewLayer.SP_OPERATFLAG)
	if spFlag then
		spFlag:removeFromParent()
	end
	if nil == viewId then
		return false
	end
	local action = ""
	if operateCode == GameLogic.WIK_NULL then
		return false
	elseif operateCode == GameLogic.WIK_CHI_HU then
		action = "hu"
		if isZiMo then
			action = "zimo"
		end
	elseif operateCode == GameLogic.WIK_LISTEN then

	elseif operateCode == GameLogic.WIK_GANG then
		action = "gang"
	elseif operateCode == GameLogic.WIK_PENG then
		action = "peng"
	elseif operateCode <= GameLogic.WIK_RIGHT then
		action = "chi"
	end

	if action == "" or action == "zimo" or action == "hu" then
		return false
	end

	self:createArmature(action, self, posPlate[viewId], GameViewLayer.SP_OPERATFLAG, 12)
	return true
end

function GameViewLayer:createArmature(action, parent, pos, tag, zOrder)
	local armature = nil

	if action == "zimo" or action == "hu"  then
		local loadSecond = true
		local exportJsonName = "effect_text_pengchiganghu.ExportJson"
		local picName = "effect_text_pengchiganghu0.png"
		local plistName = "effect_text_pengchiganghu0.plist"
		local armatureName = "effect_text_pengchiganghu"

		if action == "zimo" then
			exportJsonName = "effect_text_zimo01.ExportJson"
			picName = "effect_text_zimo010.png"
			plistName = "effect_text_zimo010.plist"
			armatureName = "effect_text_zimo01"
			action = "Animation1"
			loadSecond = false
		end

		-- 加载动画所用到的数据
		ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("effect/"..picName,"effect/"..plistName,"effect/"..exportJsonName);
		if loadSecond then
			picName = "effect_text_pengchiganghu1.png"
			plistName = "effect_text_pengchiganghu1.plist"
			ccs.ArmatureDataManager:getInstance():addArmatureFileInfo("effect/"..picName,"effect/"..plistName,"effect/"..exportJsonName);
		end
		
		-- 创建动画对象
		armature = ccs.Armature:create(armatureName)  
		armature:setPosition(pos)
		-- armature:setTag(tag)
		-- 设置动画对象执行的动画名称
		armature:getAnimation():play(action)
		-- 把动画对象加载到场景内
		parent:addChild(armature, zOrder)
	else
		--只展示静态图片，不要动画
		local path = "effect/static_pic/game_" .. action .. ".png"
		armature = cc.Sprite:create(path)
		armature:setPosition(pos)
		parent:addChild(armature, zOrder)
	end
	
	

	local dt = 1.0
	if action == "chi" or action == "peng" or action == "gang" then
		dt = 1.0
	elseif action ~= "hu" or action ~= "zimo"  then
		dt = 1.0
	end

	self:runAction(
		cc.Sequence:create(
			cc.DelayTime:create(dt),
			cc.CallFunc:create(
				function()
					armature:runAction(cc.Sequence:create(cc.FadeOut:create(0.5), cc.RemoveSelf:create()))
				end
				)
			)
		)
	
end

function GameViewLayer:playHuTypeEffect(chairId, plistPath, nameFormat, startIndex, endIndex, key)
	local runScene = cc.Director:getInstance():getRunningScene()

	local wViewChairId = self._scene:SwitchViewChairID(chairId)
	local sp = cc.Sprite:create()
	sp:setScale(2)
	sp:setPosition(cc.p(667, 430)) --posPlate[wViewChairId]
	self:addChild(sp, 99)

	cc.SpriteFrameCache:getInstance():addSpriteFrames(plistPath)
	AnimationMgr.loadAnimationFromFrame(nameFormat, startIndex, endIndex, key, AnimationMgr.PLIST_RES)
	local param = AnimationMgr.getAnimationParam()
    param.m_fDelay = 0.1
    param.m_strName = key
    local animate = AnimationMgr.getAnimate(param)

	local animaterpt = cc.Sequence:create(animate, cc.DelayTime:create(1), cc.RemoveSelf:create())
	sp:runAction(animaterpt)
end

--数字中插入点
function GameViewLayer:numInsertPoint(lScore)
	local strRes = ""
	if lScore < 0 then
		strRes = "-"..lScore
	elseif  lScore >= 0 and lScore < 10000 then
		strRes = ""..lScore
	elseif lScore >= 10000 and lScore < 100000000 then

		local num1 = lScore % 10000   > 0   and lScore % 10000  or 0
		local num2 = num1 > 0 and math.floor(num1 / 100) or 0
		if num2>0 then
		       local str1 = num2 < 10 and "0"..num2 or ""..num2
		       strRes = math.floor(lScore/10000).."."..str1.."万"
		else
		       strRes = math.floor(lScore/10000).."万"
		end

	elseif lScore>=100000000 then
		local num1 = lScore%100000000   > 0   and lScore%100000000  or 0
		local num2 = num1>0 and math.floor(num1/1000000) or 0
		if num2>0 then
		       local str1= num2<10 and "0"..num2 or ""..num2
		       strRes = math.floor(lScore/100000000).."."..str1.."亿"
		else
		       strRes = math.floor(lScore/100000000).."亿"
		end
	end
		--todo
	--[[
	assert(lScore >= 0)
	local strRes = ""
	local str = string.format("%d", lScore)
	local len = string.len(str)

	local times = math.floor(len/3)
	local remain = math.mod(len, 3)
	strRes = strRes..string.sub(str, 1, remain)
	for i = 1, times do
		if strRes ~= "" then
			strRes = strRes.."/"
		end
		local index = (i - 1)*3 + remain + 1	--截取起始位置
		strRes = strRes..string.sub(str, index, index + 2)
	end
]]
	return strRes
end


function GameViewLayer:setRoomHost(viewId)
	for i = 1, cmd.GAME_PLAYER do
		self.nodePlayer[i]:getChildByTag(GameViewLayer.SP_ROOMHOST):setVisible(false)
	end
	self.nodePlayer[viewId]:getChildByTag(GameViewLayer.SP_ROOMHOST):setVisible(true)
end

--index 从0开始， 对应GameLogic.LocalCardData
function GameViewLayer:setLaiZi(index)
	if index == 34 then
		return
	end

    local nValue = 0
    local nColor = 0

    if index >= 0 and index <= 8 then
        nValue = index + 1
        nColor = 0
    elseif index >= 9 and index <= 17 then
        nValue = index - 9 + 1
        nColor = 1
    elseif index >= 18 and index <= 26 then
        nValue = index - 18 + 1
        nColor = 2
    elseif index >= 27 and index <= 33 then
    	nValue = index - 27 + 1
    	nColor = 3
    end

    --如果不是选的中，那就显示-1的牌
    if nColor ~= 3 then
    	if nValue ~= 1 then
    		nValue = nValue - 1
    	else
    		nValue = 9
    	end
    end

    self:setLaiziCardVisible(true)

	local strFile = cmd.RES_PATH.."game/font_big/font_"..nColor.."_"..nValue..".png"
	if self._cardLayer._mjSkin == 1 then
		strFile = cmd.RES_PATH.."game/font_big_1/font_"..nColor.."_"..nValue..".png"
	elseif self._cardLayer._mjSkin == 2 then
		strFile = cmd.RES_PATH.."game/font_big_2/font_"..nColor.."_"..nValue..".png"
	elseif self._cardLayer._mjSkin == 3 then
		strFile = cmd.RES_PATH.."game/font_big_3/font_"..nColor.."_"..nValue..".png"
	end

	local cardFont = self:getChildByName("sp_laizicard")
	cardFont:setTexture(strFile)

	local cardFontBg = self:getChildByName("sp_laizicard_bg")
	if self._cardLayer._mjSkin == 1 then
		cardFontBg:setTexture(cmd.RES_PATH.."game/font_big_1/card_up.png")
		cardFontBg:getChildByName("Sprite_1"):setPosition(cc.p(43, 46))
	elseif self._cardLayer._mjSkin == 2 then
		
	end

	local huipaiTx = cardFontBg:getChildByName("HUIPAI_ANIMATION")
	if not huipaiTx then
		-- self:createHuiPaiAnimation(cardFontBg)
	end
end

function GameViewLayer:createHuiPaiAnimation(parent)
	cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/hptx.plist")
    local adAnimition = cc.Sprite:createWithSpriteFrameName("effects_1.png")
    adAnimition:setName("HUIPAI_ANIMATION")
    local pSize = parent:getContentSize()
    adAnimition:setScaleY(1.05)
    adAnimition:setScaleX(0.8)
    adAnimition:setPosition(cc.p(pSize.width/2-7, pSize.height/2+10));
    parent:addChild(adAnimition, 10);  

    local animation = cc.Animation:create()
    local name
    for i=1, 8 do
        name = "effects_"..i..".png"
        --从plist大图中读取图片资源
        animation:addSpriteFrame(cc.SpriteFrameCache:getInstance():getSpriteFrame(name))
    end

    -- animation:setLoops(-1) --设置 -1，表示无限循环
    animation:setDelayPerUnit(0.15)
    -- animation:setRestoreOriginalFrame(true)
    local action = cc.Animate:create(animation)
    adAnimition:runAction(cc.RepeatForever:create(cc.Sequence:create(action)))

end
function GameViewLayer:showTipPopup(tip)
	local query = QueryDialog:create(tip, function(ok)
        if ok == true then
            
        end
    end, 25, 1):setCanTouchOutside(false)
        :addTo(self, 20)
end

return GameViewLayer

