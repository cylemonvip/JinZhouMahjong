--
-- Author: tom
-- Date: 2017-02-27 17:26:42
--
local SetLayer = class("SetLayer", function(scene)
	local setLayer = display.newLayer()
	return setLayer
end)

local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.models.CMD_Game")

local ChangeSkinNode = appdf.req(appdf.GAME_SRC.."yule.sparrowhld.src.views.Entity.ChangeSkinNode")


local TAG_BT_MUSICON = 1
local TAG_BT_MUSICOFF = 2
local TAG_BT_EFFECTON = 3
local TAG_BT_EFFECTOFF = 4
local TAG_BT_EXIT = 5

local TAG_btnEffectMin = 6
local TAG_btnEffectMax = 7
local TAG_btnBgmMin = 8
local TAG_btnBgmMax = 9

local TAG_btnDismiss = 10
local TAG_btnExit = 11

local TAG_btnChangeSKin = 12

function SetLayer:onInitData()
end

function SetLayer:onResetData()
end

local this
function SetLayer:ctor(scene)
	this = self
	self._scene = scene
	self:onInitData()


	self.colorLayer = cc.LayerColor:create(cc.c4b(0, 0, 0, 125))
		:setContentSize(display.width, display.height)
		:addTo(self)
	local this = self
	self.colorLayer:registerScriptTouchHandler(function(eventType, x, y)
		return this:onTouch(eventType, x, y)
	end)

	local funCallback = function(ref)
		this:onButtonCallback(ref:getTag(), ref)
	end
	--UI
	self._csbNode = cc.CSLoader:createNode(cmd.RES_PATH.."game/SetLayer.csb")
		:addTo(self, 1)

	local btnClose = self._csbNode:getChildByName("bt_close")
		:setTag(TAG_BT_EXIT)
	btnClose:addClickEventListener(funCallback)
	self.sp_layerBg = self._csbNode:getChildByName("sp_setLayer_bg")

	--滑动条
	self.effectSlider = self._csbNode:getChildByName("EffectSlider")
	self.effectSlider:setPercent(GlobalUserItem.nSound)
	self.effectSlider:addEventListenerSlider(function(sender, eventType)
        if eventType == ccui.SliderEventType.percentChanged then
        	local percent = self.effectSlider:getPercent()
        	GlobalUserItem.setEffectsVolume(percent)
        	if percent > 0 and not GlobalUserItem.bSoundAble then
        		GlobalUserItem.setSoundAble(true)
        	end

        	if percent <= 0 then
				GlobalUserItem.setSoundAble(false)
        	end
        end
    end)
    
	self.bgmSlider = self._csbNode:getChildByName("BgmSlider")
	self.bgmSlider:setPercent(GlobalUserItem.nMusic)
	self.bgmSlider:addEventListenerSlider(function(sender, eventType)
        if eventType == ccui.SliderEventType.percentChanged then
        	local percent = self.bgmSlider:getPercent()
        	GlobalUserItem.setMusicVolume(percent)
        	if percent > 0 and not GlobalUserItem.bVoiceAble then
        		GlobalUserItem.setVoiceAble(true)
        		AudioEngine.playMusic("sound/BACK_PLAYING.mp3", true)
        	end

        	if percent <= 0 then
        		GlobalUserItem.setVoiceAble(false)
        	end
        end
    end)

    if not GlobalUserItem.bVoiceAble then
    	self.bgmSlider:setPercent(0)
    end

    if not GlobalUserItem.bSoundAble then
    	self.effectSlider:setPercent(0)
    end

	GlobalUserItem.setEffectsVolume(GlobalUserItem.nSound)
    GlobalUserItem.setMusicVolume(GlobalUserItem.nMusic)
	
	local btnEffectMin = self._csbNode:getChildByName("BtnEffectMin")
	btnEffectMin:setTag(TAG_btnEffectMin)
	btnEffectMin:addClickEventListener(funCallback)
	local btnEffectMax = self._csbNode:getChildByName("BtnEffectMax")
	btnEffectMax:setTag(TAG_btnEffectMax)
	btnEffectMax:addClickEventListener(funCallback)
	local btnBgmMin = self._csbNode:getChildByName("BtnBgmMin")
	btnBgmMin:setTag(TAG_btnBgmMin)
	btnBgmMin:addClickEventListener(funCallback)
	local btnBgmMax = self._csbNode:getChildByName("BtnBgmMax")
	btnBgmMax:setTag(TAG_btnBgmMax)
	btnBgmMax:addClickEventListener(funCallback)
	

	local btnDismiss = self._csbNode:getChildByName("BtnDismiss")
	btnDismiss:setTag(TAG_btnDismiss)
	btnDismiss:addClickEventListener(funCallback)

	local btnExit = self._csbNode:getChildByName("BtnExit")
	btnExit:setTag(TAG_btnExit)
	btnExit:addClickEventListener(funCallback)

	-- local btnChangeSkin = self._csbNode:getChildByName("BtnChangeSkin")
	-- btnChangeSkin:setTag(TAG_btnChangeSKin)
	-- btnChangeSkin:addClickEventListener(funCallback)

	if self._scene._scene._isReplay then
		btnDismiss:setVisible(false)
		btnChangeSkin:setVisible(false)
		btnExit:setPositionX(667)
	end

	--获取本地缓存的当前桌布和牌面
    local zbIndex = cc.UserDefault:getInstance():getStringForKey("ZB_INDEX", 0)
    local pmIndex = cc.UserDefault:getInstance():getStringForKey("PM_INDEX", 0)	

	local cbtlistener = function (sender,eventType)
       	self:onSelectedEvent(sender:getName(), sender)
    end

	for i = 0, 1 do
		local cbpzys = self._csbNode:getChildByName("cbpzys_"..i)
		cbpzys.index = i
		cbpzys:addEventListener(cbtlistener)
		if tonumber(zbIndex) == i then
			cbpzys:setSelected(true)
		else
			cbpzys:setSelected(false)
		end
	end

	for i = 0, 1 do
		local cbpmys = self._csbNode:getChildByName("cbpmys_"..i)
		cbpmys.index = i
		cbpmys:addEventListener(cbtlistener)
		if tonumber(pmIndex) == 3 and i == 0 then
			cbpmys:setSelected(true)
		elseif tonumber(pmIndex) == 2 and i == 1 then
			cbpmys:setSelected(true)
		else
			cbpmys:setSelected(false)
		end
	end
	

	-- GlobalUserItem.bVoiceAble
	-- GlobalUserItem.bSoundAble

	--版本号
	-- local textVersion = self._csbNode:getChildByName("Text_version")
	-- local mgr = self._scene._scene._scene:getApp():getVersionMgr()
	-- local nVersion = mgr:getResVersion(cmd.KIND_ID) or "0"
	-- local strVersion = "版本号:"..appdf.BASE_C_VERSION.."."..mgr:getResVersion()
	-- textVersion:setString(strVersion)

	self:setVisible(false)
end

function SetLayer:onSelectedEvent(name, sender)
	if name == "cbpzys_0" or name == "cbpzys_1" or name == "cbpzys_2" then
		for i = 0, 1 do
			local cbpzys = self._csbNode:getChildByName("cbpzys_"..i)
			cbpzys:setSelected(false)
		end
		sender:setSelected(true)
	elseif name == "cbpmys_0" or name == "cbpmys_1" then
		for i = 0, 1 do
			local cbpmys = self._csbNode:getChildByName("cbpmys_"..i)
			cbpmys:setSelected(false)
		end
		sender:setSelected(true)
	end
end

function SetLayer:onButtonCallback(tag, ref)
	if tag == TAG_btnEffectMin then
		GlobalUserItem.setSoundAble(false)
		GlobalUserItem.setEffectsVolume(0)
		self.effectSlider:setPercent(GlobalUserItem.nSound)
	elseif tag == TAG_btnEffectMax then
		GlobalUserItem.setSoundAble(true)
		GlobalUserItem.setEffectsVolume(100)
		self.effectSlider:setPercent(GlobalUserItem.nSound)
	elseif tag == TAG_btnBgmMin then
		GlobalUserItem.setVoiceAble(false)
		GlobalUserItem.setMusicVolume(0)
		self.bgmSlider:setPercent(GlobalUserItem.nMusic)
	elseif tag == TAG_btnBgmMax then
		if not GlobalUserItem.bVoiceAble then
			GlobalUserItem.setVoiceAble(true)
			AudioEngine.playMusic("sound/BACK_PLAYING.mp3", true)
		end
		
		GlobalUserItem.setMusicVolume(100)
		
		self.bgmSlider:setPercent(GlobalUserItem.nMusic)
	elseif tag == TAG_btnDismiss then
		self:hideLayer()
		if GlobalUserItem.bPrivateRoom then
			PriRoom:getInstance():queryDismissRoom()
		else
			self._scene._scene:onQueryExitGame()
		end

	elseif tag == TAG_btnExit then
		self:hideLayer()
		if self._scene._scene._isReplay then
			self._scene._scene:closeReplay()
		elseif GlobalUserItem.bPrivateRoom then
			self._scene._scene._gameFrame:StandUp(false)
	    else
			self._scene._scene:onQueryExitGame()
		end
		
	elseif tag == TAG_BT_MUSICON then
		GlobalUserItem.setVoiceAble(false)
		self.btMusicOn:setVisible(false)
		self.btMusicOff:setVisible(true)
	elseif tag == TAG_BT_MUSICOFF then
		GlobalUserItem.setVoiceAble(true)
		self.btMusicOn:setVisible(true)
		self.btMusicOff:setVisible(false)
		AudioEngine.playMusic("sound/BACK_PLAYING.mp3", true)
	elseif tag == TAG_BT_EFFECTON then
		GlobalUserItem.setSoundAble(false)
		self.btEffectOn:setVisible(false)
		self.btEffectOff:setVisible(true)
	elseif tag == TAG_BT_EFFECTOFF then
		GlobalUserItem.setSoundAble(true)
		self.btEffectOn:setVisible(true)
		self.btEffectOff:setVisible(false)
	elseif tag == TAG_BT_EXIT then
		self:hideLayer(true)
	elseif tag == TAG_btnChangeSKin then
		--更换皮肤
		local changeSkinNode = ChangeSkinNode:create(self._scene)
		changeSkinNode:setPosition(cc.p(667, 375))
		self:getParent():addChild(changeSkinNode, self:getLocalZOrder())
		
		self:hideLayer()
	end
end

function SetLayer:onTouch(eventType, x, y)
	if eventType == "began" then
		return true
	end

	local pos = cc.p(x, y)
    local rectLayerBg = self.sp_layerBg:getBoundingBox()
    if not cc.rectContainsPoint(rectLayerBg, pos) then
    	self:hideLayer(true)
    end

    return true
end

function SetLayer:showLayer()
	self.colorLayer:setTouchEnabled(true)
	self:setVisible(true)
end

function SetLayer:hideLayer(onlyHide)
	self.colorLayer:setTouchEnabled(false)
	self:setVisible(false)
	self:onResetData()

	local pzIndex = 0
	for i = 0, 1 do
		local cbpzys = self._csbNode:getChildByName("cbpzys_"..i)
		if cbpzys:isSelected() then
			pzIndex = i
			break
		end
	end

	local pmIndex = 0
	for i = 0, 1 do
		local cbpmys = self._csbNode:getChildByName("cbpmys_"..i)
		if cbpmys:isSelected() then
			pmIndex = i
		end
	end

	--原始牌面index
	local yspmIndex = cc.UserDefault:getInstance():getStringForKey("PM_INDEX", 0)
	--保存本地缓存的当前桌布和牌面
    cc.UserDefault:getInstance():setStringForKey("ZB_INDEX", pzIndex)
    if tonumber(pmIndex) == 0 then
    	pmIndex = 3
    end
    if tonumber(pmIndex) == 1 then
    	pmIndex = 2
    end
    cc.UserDefault:getInstance():setStringForKey("PM_INDEX", pmIndex)

    self._scene:initBackground(pzIndex)

    if onlyHide and tonumber(yspmIndex) ~= tonumber(pmIndex) then
    	self._scene:showTipPopup("您设置的牌面，将在下一局生效！")
    end

    return false

end

return SetLayer