local cmd = appdf.req(appdf.GAME_SRC.."yule.sparrowhz.src.models.CMD_Game")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC.."ExternalFun")
local CardLayer = appdf.req(appdf.GAME_SRC.."yule.sparrowhz.src.views.layer.CardLayer")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.sparrowhz.src.models.GameLogic")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local ClipText = appdf.req("client.src.external.ClipText")

local ResultLayer = class("ResultLayer", function(scene)
	local resultLayer = cc.CSLoader:createNode(cmd.RES_PATH.."gameResult/GameResultLayer.csb")
	return resultLayer
end)

ResultLayer.TAG_NODE_USER_1					= 1
ResultLayer.TAG_NODE_USER_2					= 2
ResultLayer.TAG_NODE_USER_3					= 3
ResultLayer.TAG_NODE_USER_4					= 4
ResultLayer.TAG_SP_ROOMHOST					= 5
ResultLayer.TAG_SP_BANKER					= 6
ResultLayer.TAG_BT_RECODESHOW				= 8
ResultLayer.TAG_BT_CONTINUE					= 9

ResultLayer.TAG_SP_HEADCOVER				= 1
ResultLayer.TAG_TEXT_NICKNAME				= 2
ResultLayer.TAG_HEAD 						= 4
ResultLayer.TAG_NODE_CARD					= 5

ResultLayer.WINNER_ORDER					= 1

local posBanker = {cc.p(75, 549), cc.p(75, 441), cc.p(75, 331), cc.p(75, 228)}
local NODE_USER_POS = {cc.p(112, 521), cc.p(112, 414), cc.p(112, 307), cc.p(112, 200)}

function ResultLayer:onInitData()
	--body
	self.winnerIndex = nil
	self.bShield = false
end

function ResultLayer:onResetData()
	--body
	self.winnerIndex = nil
	self.bShield = false
	self.nodeAwardCard:removeAllChildren()
	self.nodeRemainCard:removeAllChildren()
	for i = 1, cmd.GAME_PLAYER do
		self.nodeUser[i]:getChildByTag(ResultLayer.TAG_NODE_CARD):removeAllChildren()
	end
end

function ResultLayer:ctor(scene)
	self._scene = scene
	self:onInitData()
	ExternalFun.registerTouchEvent(self, true)

	local btRecodeShow = self:getChildByTag(ResultLayer.TAG_BT_RECODESHOW)
	btRecodeShow:setVisible(false)

	--继续按钮
	local btContinue = self:getChildByTag(ResultLayer.TAG_BT_CONTINUE)
	btContinue:setPosition(cc.p(667, 54))
	--btContinue:setPositionX(display.cx)
	btContinue:addClickEventListener(function(ref)
		self:continueBtnCallback(ref)
	end)

	self.nodeUser = {}
	for i = 1, cmd.GAME_PLAYER do
		self.nodeUser[i] = self:getChildByTag(ResultLayer.TAG_NODE_USER_1 + i - 1)
		self.nodeUser[i]:setLocalZOrder(1)
		self.nodeUser[i]:getChildByTag(ResultLayer.TAG_SP_HEADCOVER):setLocalZOrder(1)
		--个人麻将
		local nodeUserCard = cc.Node:create()
			:setTag(ResultLayer.TAG_NODE_CARD)
			:addTo(self.nodeUser[i])
	end
	--奖码
	self.nodeAwardCard = cc.Node:create():addTo(self)
	--剩余麻将
	self.nodeRemainCard = cc.Node:create():addTo(self)
	--庄标志
	self.spBanker = self:getChildByTag(ResultLayer.TAG_SP_BANKER):setLocalZOrder(1)

	self:getChildByName("Text_ResultDJS"):setVisible(false)
end

function ResultLayer:continueBtnCallback(ref)
	if self._scene._scene._isReplay or self._scene._scene._gameFrame == nil then
		self._scene._scene:closeReplay()
	else
		self:hideLayer()
		self._scene:onButtonClickedEvent(self._scene.BT_START)
	end
	
end

function ResultLayer:onTouchBegan(touch, event)
	return false
end

function ResultLayer:onClockUpdate(time)
	self:getChildByName("Text_ResultDJS"):setString(time)
	self:getChildByName("Text_ResultDJS"):setVisible(true)
end

function ResultLayer:showLayer(resultList, cbAwardCard, cbRemainCard, wBankerChairId, cbHuCard, wProvideUser, wWinerUser, meCharid, rule, ruleEnd)
	assert(type(resultList) == "table"  and type(cbRemainCard) == "table") --and type(cbAwardCard) == "table"
	cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
	self:getChildByName("Text_ResultDJS"):setVisible(false)

	local txtDate = self:getChildByName("TimeTxt")
	local date=os.date("%Y.%m.%d %H:%M")
	txtDate:setString("日期: " .. tostring(date))

	local hasQiDuiRule = false
	local isFirstConat = true
	local ruleStr = "创建房间的玩法: "
	for k,v in pairs(rule) do
		if v == true and k == 1 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "天地胡"
		end

		if v == true and k == 2 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "会牌"
		end

		if v == true and k == 3 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "穷胡翻倍"
		end

		if v == true and k == 4 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "封顶150"
		end

		if v == true and k == 5 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "三清"
		end 

		if v == true and k == 6 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "七对"
			hasQiDuiRule = true
		end

		if v == true and k == 7 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "清一色"
		end

		if v == true and k == 8 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleStr = ruleStr .. "、"
			end
			ruleStr = ruleStr .. "封顶200"
		end
	end

	if ruleStr == "创建房间的玩法: " then
		ruleStr = ruleStr .. "没有特殊玩法"
	end

	-- 闭门，杠开花，杠流泪，三清，四清，天胡，地胡，150分封顶，穷胡翻倍
	isFirstConat = true
	local ruleEndStr = "胡牌加翻的算法: "
	for k,v in pairs(ruleEnd) do
		if v == true and k == 1 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "杠开花"
		end

		if v == true and k == 2 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "杠流泪"
		end

		if v == true and k == 3 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "三清"
		end

		if v == true and k == 4 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "四清"
		end 

		if v == true and k == 5 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "天地胡"
		end 

		if v == true and k == 6 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "封顶150"
		end 

		if v == true and k == 7 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "穷胡翻倍"
		end 

		if v == true and k == 8 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "清一色"
		end

		if v == true and k == 9 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "封顶200"
		end 

		if v == true and k == 10 then
			if isFirstConat then
				isFirstConat = false
			else
				ruleEndStr = ruleEndStr .. "、"
			end
			ruleEndStr = ruleEndStr .. "海底捞"
		end 
	end

	if ruleEndStr == "胡牌加翻的算法: " then
		ruleEndStr = ruleEndStr .. "没有加翻"
	end

	local spDisHuipaiBg = self:getChildByName("sp_dis_huipai_bg")
	if self._scene._cardLayer._mjSkin == 1 then
		spDisHuipaiBg:setTexture("game/yule/sparrowhz/res/game/font_big_1/card_up.png")
		spDisHuipaiBg:getChildByName("Sprite"):setPosition(cc.p(43, 46))
	end

	local spDisHuipai = self:getChildByName("sp_dis_huipai")
	if GameLogic.MAGIC_DATA == -1 or GameLogic.MAGIC_INDEX == 34 then
		spDisHuipaiBg:setVisible(false)
		spDisHuipai:setVisible(false)
	else
		spDisHuipaiBg:setVisible(true)
		spDisHuipai:setVisible(true)

		local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(GameLogic.MAGIC_DATA))
		--如果不是选的中，那就显示-1的牌
	    -- if nColor ~= 3 then
	    -- 	if nValue ~= 1 then
	    -- 		nValue = nValue
	    -- 	else
	    -- 		nValue = 9
	    -- 	end
	    -- end

	    local fontFile = "font_big"

	    if self._scene._cardLayer._mjSkin == 1 then
	    	fontFile = "font_big_1"
	    elseif self._scene._cardLayer._mjSkin == 2 then
	    	fontFile = "font_big_2"
	    end


		spDisHuipai:setTexture("game/" .. fontFile .. "/font_"..nColor.."_"..nValue..".png")
	end

	local width = 44
	local height = 67
	if self._scene._cardLayer._mjSkin == 1 then
		width = 37
		height = 59
	-- elseif self._scene._cardLayer._mjSkin == 3 then
	-- 	width = 37
	-- 	height = 59
	end
	for i = 1, #resultList do
		if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
			self.winnerIndex = i
			break
		end
	end

	local winBackground = self:getChildByName("win_background")
	local spHupaiType = winBackground:getChildByName("sp_hupaitype")
	spHupaiType:setVisible(false)

	local nBankerOrder = 1
	for i = 1, cmd.GAME_PLAYER do
		local order = self:switchToOrder(i)
		if i <= #resultList then
			self.nodeUser[i]:setVisible(true)

			local descriptionTxt = self.nodeUser[i]:getChildByName("Description")
			local descriptionStr = ""

			--是否为庄家,不是庄家才显示跟庄数据
			if wBankerChairId ~= resultList[i].userItem.wChairID then
				local followCound = 0
				for a, b in pairs(resultList[i].bFollowBanker) do
					if b == true then
						followCound = followCound + 1
					end
				end
				if followCound > 0 then
					descriptionStr = descriptionStr .. "跟庄x" .. followCound .. "  "
				end
			end

			if resultList[i].wAllMagicUser == resultList[i].userItem.wChairID then
				descriptionStr = descriptionStr .. "血会" .. "  "
			end

			local mingGangCount = 0
			local fangGangCount = 0
			local anGangCount = 0
			local dianGangCount = 0

			for a, b in pairs(resultList[i].WeaveItemArray) do
				if b.cbParam == GameLogic.WIK_MING_GANG then --明杠，也即为补杠
					mingGangCount = mingGangCount + 1
				elseif b.cbParam == GameLogic.WIK_FANG_GANG then --放杠，也即为点杠
					fangGangCount = fangGangCount + 1
				elseif b.cbParam == GameLogic.WIK_AN_GANG then --暗杠
					anGangCount = anGangCount + 1
				end
			end

			for l = 1, cmd.GAME_PLAYER do
				if l <= #resultList then
					for a, b in pairs(resultList[l].WeaveItemArray) do
						if b.cbParam == GameLogic.WIK_FANG_GANG then --放杠，也即为点杠,如果提供的人是自己的
							if b.wProvideUser == resultList[i].userItem.wChairID then
								dianGangCount = dianGangCount + 1
							end
						end
					end
				end
			end

			if mingGangCount > 0 then
				descriptionStr = descriptionStr .. "明杠x" .. mingGangCount .. "  "
			end

			if fangGangCount > 0 then
				descriptionStr = descriptionStr .. "接杠x" .. fangGangCount .. "  "	
			end

			if anGangCount > 0 then
				descriptionStr = descriptionStr .. "暗杠x" .. anGangCount .. "  "	
			end

			if dianGangCount > 0 then
				descriptionStr = descriptionStr .. "点杠x" .. dianGangCount .. "  "	
			end

			--是否闭门
			local isBiMen = resultList[i].cbWeaveItemCount == anGangCount
			if isBiMen then
				descriptionStr = descriptionStr .. "闭门" .. "  "
			end

			if wBankerChairId == resultList[i].userItem.wChairID and wWinerUser == yl.INVALID_CHAIR then
				descriptionStr = descriptionStr .. "黄庄-6" .. "  "
			end

			descriptionTxt:setString(descriptionStr)

			-- if anGangCount > 0 then
			-- 	descriptionTxt:setPositionY(24)
			-- else
			-- 	descriptionTxt:setPositionY(24)
			-- end

			descriptionTxt:setPositionY(24)
			descriptionTxt:setPositionX(500)

			--头像
			local head = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_HEAD)
			if head then
				head:updateHead(resultList[i].userItem)
			else
				if self._scene._scene._isReplay then
					head = PopupInfoHead:createReplayHead(resultList[i].userItem.dwUserID, nil, 76.8)
				else
					head = PopupInfoHead:createNormal(resultList[i].userItem, 76.8)
				end
				head:setPosition(0, 0)			--初始位置
				head:enableHeadFrame(false)
				head:enableInfoPop(false)
				head:setTag(ResultLayer.TAG_HEAD)
				self.nodeUser[i]:addChild(head)
			end

			local textGangFen = self.nodeUser[i]:getChildByName("Text_gangfen")
			local textHuFen = self.nodeUser[i]:getChildByName("Text_hufen")
			local textZongFen = self.nodeUser[i]:getChildByName("Text_zongfen")

			--输赢积分
			local preStr = ""
			if resultList[i].lScore > 0 then
				preStr = "+"
			end

			local gangPreStr = ""
			if resultList[i].gScore > 0 then
				gangPreStr = "+"
			end

			local huPreStr = ""
			if resultList[i].hScore > 0 then
				huPreStr = "+"
			end
			
			
			textGangFen:setString(gangPreStr .. resultList[i].gScore)
			textHuFen:setString(huPreStr .. resultList[i].hScore)
			textZongFen:setString(preStr .. resultList[i].lScore)

			--昵称
			local textNickname = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_TEXT_NICKNAME)
			textNickname:setVisible(false)
			--textNickname:setString(resultList[i].userItem.szNickName)

			local nick =  ClipText:createClipText(cc.size(101, 23),resultList[i].userItem.szNickName,"fonts/round_body.ttf",19)
			nick:setAnchorPoint(cc.p(0.5,0.5))
			nick:setPosition(cc.p(0,-48.3))
			nick:setTextColor(cc.c3b(254, 255, 157))
			nick.m_text:enableOutline(cc.c4b(0,0,0,255), 1)
			self.nodeUser[i]:addChild(nick)

			local fontSmallFile = "font_small"
			if self._scene._cardLayer._mjSkin == 1 then
				fontSmallFile = "font_small_1"
			elseif self._scene._cardLayer._mjSkin == 2 then
				fontSmallFile = "font_small_2"
			elseif self._scene._cardLayer._mjSkin == 3 then
				fontSmallFile = "font_small_3"
			end

			local fX = 80
			local fY = -22
			local nodeUserCard = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_NODE_CARD)
			--吃，碰，杠的牌
			for k,v in pairs(resultList[i].WeaveItemArray) do
				local isAllZero = true
				for a,b in pairs(v.cbCardData[1]) do
					if b > 0 then
						isAllZero = false
						local card = nil
						if v.cbParam == GameLogic.WIK_AN_GANG then
							local jieXian = 3
							local totalCount = 4
							--如果是皮子杠
							if b == GameLogic.getPiZiCardData() then
								jieXian = 2
								totalCount = 3
							end

							if a <= jieXian then
								card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_back.png")
									:move(fX, fY)
									:addTo(nodeUserCard)
							elseif a == totalCount then
								fX = fX - width
								if totalCount == 3 then
									fxx = fX - width/2
								else
									fxx = fX - width
								end
								
								card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
									:move(fxx, fY + 16)
									:addTo(nodeUserCard)

								local cardSize = card:getContentSize()
								local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(b))
								display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
									:move(cardSize.width/2, cardSize.height/2 + 8)
									:addTo(card)
							end
						else
							card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
								:move(fX, fY)
								:addTo(nodeUserCard)

							local cardSize = card:getContentSize()
							local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(b))
							local cardFont = display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
							cardFont:move(cardSize.width/2, cardSize.height/2 + 8)
							cardFont:addTo(card)
							if self._scene._cardLayer._mjSkin == 3 then
								cardFont:setScale(1.2)
							end
						end

						fX = fX + width
					end
				end
				if not isAllZero then
					fX = fX + 10
				end
			end

			local huipaiFlagDx = 0
			local huipaiFlagDy = 2
			local ddy = 6
			if self._scene._cardLayer._mjSkin == 1 then
				ddy = 7
				huipaiFlagDx = 3
				huipaiFlagDy = -3
			end

			for j = 1, #resultList[i].cbCardData do  											 	--剩余手牌
				--牌底
				local card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
					:move(fX, fY)
					:addTo(nodeUserCard)

				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(resultList[i].cbCardData[j]))
				local cardSize = card:getContentSize()
				local cardFont = display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
				cardFont:move(cardSize.width/2, cardSize.height/2 + ddy)
				cardFont:addTo(card)

				if self._scene._cardLayer._mjSkin == 3 then
					cardFont:setScale(1.2)
				end

				card:removeChildByTag(666)

				if resultList[i].cbCardData[j] == GameLogic.MAGIC_DATA then
					local size = card:getContentSize()
					local sHuiPaiFlag = display.newSprite("#r_s_up_huipai.png")
					    :move(size.width/2 + huipaiFlagDx, size.height/2+huipaiFlagDy)
					    :setTag(666)
					    :addTo(card)    
				end

				fX = fX + width
			end

			local cNode = self.nodeUser[i]:getChildByName("PROVIDEFLAG")
			if cNode then
				cNode:removeFromParent()
			end

			--先判断是否有赢家
			if self.winnerIndex then
				--如果当前遍历的玩家是提供者
				if resultList[i].userItem.wChairID and resultList[i].userItem.wChairID == wProvideUser then
					local isFangpao = false
					--自摸
					if wProvideUser == wWinerUser then
						hupaiFlagPath = "game/yule/sparrowhz/res/pics/sp_ziMo.png"
					else --放炮
						hupaiFlagPath = "game/yule/sparrowhz/res/pics/r_fangpao.png"
						isFangpao = true
					end

					local hupaiSp = display.newSprite(hupaiFlagPath)
						:setName("PROVIDEFLAG")
						:addTo(self.nodeUser[i], 20)

					hupaiSp:setPosition(30, 30)
					if isFangpao then
						hupaiSp:setPosition(1120, -7)
						for a,b in pairs(self.nodeUser) do
							local by = b:getPositionY()
							--和第二个显示换位置
							if math.abs(b:getPositionY() - NODE_USER_POS[2].y) < 1 then
								local ey = self.nodeUser[i]:getPositionY()
								b:setPositionY(ey)
								break;
							end
						end
						self.nodeUser[i]:setPosition(NODE_USER_POS[2])
					end
				end
			end

			--胡的那张牌
			if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
				
				for a,b in pairs(self.nodeUser) do
					if math.abs(b:getPositionY() - NODE_USER_POS[1].y) < 1 then
						local ey = self.nodeUser[i]:getPositionY()
						b:setPositionY(ey)
						break;
					end
				end

				self.nodeUser[i]:setPosition(NODE_USER_POS[1])

				fX = fX + 20
				--牌底
				--local rectX = CardLayer:switchToCardRectX(cbHuCard)
				local huCard = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
					--:setTextureRect(cc.rect(width*rectX, 0, width, height))
					:move(fX, fY)
					:addTo(nodeUserCard)
				--字体
				local huCardSize = huCard:getContentSize()
				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(cbHuCard))
				local cardFont = display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
				cardFont:move(huCardSize.width/2, huCardSize.height/2 + 8)
				cardFont:addTo(huCard)

				if self._scene._cardLayer._mjSkin == 3 then
					cardFont:setScale(1.2)
				end


				if cbHuCard == GameLogic.MAGIC_DATA then
					local size = huCard:getContentSize()
					local sHuiPaiFlag = display.newSprite("#r_s_up_huipai.png")
					    :move(size.width/2 + huipaiFlagDx, size.height/2+huipaiFlagDy)
					    :setTag(666)
					    :addTo(huCard)
				end

				local hupaiFlagPath = ""

				nick:setTextColor(cc.c3b(216, 115,16))

				local winBackground = self:getChildByName("win_background")
				winBackground:setPositionY(self.nodeUser[i]:getPositionY() - 10)

				local spHupaiType = winBackground:getChildByName("sp_hupaitype")
				

				local htpType = GameLogic.getHPType(resultList[i].dwChiHuRight)
				local typePath = ""
				if htpType == GameLogic.HTP_QIDUI_HU then
					typePath = "r_hppinghu.png"
					if hasQiDuiRule then
						typePath = "r_hpqidui.png"
					end
				elseif htpType == GameLogic.HTP_HZ_HU then
					typePath = "r_hplou.png"
				elseif htpType == GameLogic.HTP_PIAO_HU then
					typePath = "r_hppiaohu.png"
				elseif htpType == GameLogic.HTP_DAN_DIAN then
					typePath = "r_hpdanza.png"
				elseif htpType == GameLogic.HTP_JIA_HU then
					typePath = "r_hpjiahu.png"
				elseif htpType == GameLogic.HTP_PING_HU then
					typePath = "r_hppinghu.png"
				elseif htpType == GameLogic.HTP_QING_YI_SE_HU then
					typePath = "r_hpqingyise.png"
				end

				if typePath ~= "" then
					spHupaiType:setSpriteFrame(typePath)
					spHupaiType:setVisible(true)
				end
			end
			--麻将
			fX = 909-(width+3)*(self._scene._scene.cbMaCount/2) 
				
			--庄家
			if wBankerChairId == resultList[i].userItem.wChairID then
				nBankerOrder = i
			end
		else
			self.nodeUser[i]:setVisible(false)
		end
	end

	--显示创建房间时的详细信息
	local textYXGZ = self:getChildByName("Text_YXGZ")
	textYXGZ:setString(ruleStr)
	
	
	local textYXGZ_1 = self:getChildByName("Text_YXGZ_1")
	textYXGZ_1:setString(ruleEndStr)

	--庄家
	self:setBanker(nBankerOrder)

	self.bShield = true
	self:setVisible(true)
	self:setLocalZOrder(yl.MAX_INT)
end

function ResultLayer:hideLayer()
	if not self:isVisible() then
		return
	end
	self:onResetData()
	self:setVisible(false)
	self._scene.btStart:setVisible(true)
end

--1~4转换到1~4
function ResultLayer:switchToOrder(index)
	assert(index >=1 and index <= cmd.GAME_PLAYER)
	if self.winnerIndex == nil then
		return index
	end
	local nDifference = ResultLayer.WINNER_ORDER - self.winnerIndex - 1
	local order = math.mod(index + nDifference, cmd.GAME_PLAYER) + 1
	return order
end

function ResultLayer:setBanker(order)
	assert(order ~= 0)
	self.spBanker:move(posBanker[order])
	self.spBanker:setVisible(false)

	for i = 1, 4 do
		local spBanker = self.nodeUser[i]:getChildByName("SpBanker")
		spBanker:setLocalZOrder(10)
		if i == order then
			spBanker:setVisible(true)
		else
			spBanker:setVisible(false)
		end
	end


end

function ResultLayer:recodeShow()
	if not PriRoom then
		return
	end

    --GlobalUserItem.bAutoConnect = false
    PriRoom:getInstance():getPlazaScene():popTargetShare(function(target, bMyFriend)
        bMyFriend = bMyFriend or false
        local function sharecall( isok )
            if type(isok) == "string" and isok == "true" then
                showToast(self, "战绩炫耀成功", 2)
            end
            --GlobalUserItem.bAutoConnect = true
            yl.removeErWeiMa()
        end

        local url = yl.WXShareURL or GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
        -- 截图分享
        local framesize = cc.Director:getInstance():getOpenGLView():getFrameSize()
        local area = cc.rect(0, 0, framesize.width, framesize.height)
        local imagename = "grade_share.jpg"
        if bMyFriend then
            imagename = "grade_share_" .. os.time() .. ".jpg"
        end
        ExternalFun.popupTouchFilter(0, false)

        yl.addErWeiMa()

        captureScreenWithArea(area, imagename, function(ok, savepath)
            ExternalFun.dismissTouchFilter()
            if ok then
                if bMyFriend then
                    PriRoom:getInstance():getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function( frienddata )
                        PriRoom:getInstance():imageShareToFriend(frienddata, savepath, "分享我的战绩")
                    end)
                elseif nil ~= target then
                    MultiPlatform:getInstance():shareToTarget(target, sharecall, "我的战绩", "分享我的战绩", url, savepath, "true")
                end            
            end
        end)
    end)
end

return ResultLayer