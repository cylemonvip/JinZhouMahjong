<GameFile>
  <PropertyGroup Name="GameLayer" Type="Node" ID="ab717f51-c9f3-457f-bd3c-d6162312394c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="882696396" Tag="218" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="gameBG" Tag="58" IconVisible="False" LeftMargin="-666.7050" RightMargin="-667.2950" TopMargin="-375.0000" BottomMargin="-375.0000" Scale9Width="1334" Scale9Height="750" ctype="ImageViewObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.2950" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/game_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_invite" Tag="76" IconVisible="False" LeftMargin="-250.5995" RightMargin="164.5995" TopMargin="-268.1086" BottomMargin="234.1086" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="56" Scale9Height="12" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="86.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-207.5995" Y="251.1086" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_dissolve_1.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_dissolve_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_dissolve_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_record" Tag="61" IconVisible="False" LeftMargin="533.0396" RightMargin="-562.0396" TopMargin="-463.3824" BottomMargin="441.3824" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="9" RightEage="9" TopEage="11" BottomEage="10" Scale9OriginX="9" Scale9OriginY="11" Scale9Width="11" Scale9Height="1" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="29.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="547.5396" Y="452.3824" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_openRecord_1.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_openRecord_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_openRecord_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_pass" Tag="71" IconVisible="False" LeftMargin="-356.5000" RightMargin="103.5000" TopMargin="57.8010" BottomMargin="-152.8010" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-230.0000" Y="-105.3010" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_pass_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_pass_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_pass_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_tip" Tag="72" IconVisible="False" LeftMargin="-126.5000" RightMargin="-126.5000" TopMargin="57.8010" BottomMargin="-152.8010" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-105.3010" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_tip_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_tip_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_tip_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_outCard" Tag="73" IconVisible="False" LeftMargin="103.5000" RightMargin="-356.5000" TopMargin="57.8010" BottomMargin="-152.8010" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="230.0000" Y="-105.3010" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_outCard_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_outCard_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_outCard_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="cardNum1" Tag="77" IconVisible="False" LeftMargin="-420.3239" RightMargin="312.3239" TopMargin="51.1982" BottomMargin="-103.1982" CharWidth="54" CharHeight="52" LabelText="13" StartChar="0" ctype="TextAtlasObjectData">
            <Size X="108.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-366.3239" Y="-77.1982" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/player_cardNum_big.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_operate_1" Tag="79" IconVisible="False" LeftMargin="-52.5000" RightMargin="-52.5000" TopMargin="80.5000" BottomMargin="-127.5000" Scale9Width="105" Scale9Height="47" ctype="ImageViewObjectData">
            <Size X="105.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-104.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_pass.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_operate_2" Tag="81" IconVisible="False" LeftMargin="317.4400" RightMargin="-422.4400" TopMargin="-128.3300" BottomMargin="81.3300" Scale9Width="105" Scale9Height="47" ctype="ImageViewObjectData">
            <Size X="105.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="369.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_pass.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_operate_3" Tag="82" IconVisible="False" LeftMargin="-52.5000" RightMargin="-52.5000" TopMargin="-202.1400" BottomMargin="155.1400" Scale9Width="105" Scale9Height="47" ctype="ImageViewObjectData">
            <Size X="105.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="178.6400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_pass.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_operate_4" Tag="80" IconVisible="False" LeftMargin="-418.4400" RightMargin="313.4400" TopMargin="-128.3337" BottomMargin="81.3337" Scale9Width="105" Scale9Height="47" ctype="ImageViewObjectData">
            <Size X="105.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-365.9400" Y="104.8337" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_pass.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_addTime_1" Tag="88" IconVisible="False" LeftMargin="-48.0000" RightMargin="-48.0000" TopMargin="80.0000" BottomMargin="-128.0000" Scale9Width="96" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="96.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-104.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_addTime_2" Tag="89" IconVisible="False" LeftMargin="321.9400" RightMargin="-417.9400" TopMargin="-128.8300" BottomMargin="80.8300" Scale9Width="96" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="96.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="369.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_addTime_3" Tag="90" IconVisible="False" LeftMargin="-48.0000" RightMargin="-48.0000" TopMargin="-202.6400" BottomMargin="154.6400" Scale9Width="96" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="96.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="178.6400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_addTime_4" Tag="91" IconVisible="False" LeftMargin="-417.9400" RightMargin="321.9400" TopMargin="-128.8300" BottomMargin="80.8300" Scale9Width="96" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="96.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-369.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_no_addTime_1" Tag="99" IconVisible="False" LeftMargin="-71.0000" RightMargin="-71.0000" TopMargin="80.0000" BottomMargin="-128.0000" Scale9Width="142" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="142.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-104.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_no_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_no_addTime_2" Tag="100" IconVisible="False" LeftMargin="273.9400" RightMargin="-415.9400" TopMargin="-128.8300" BottomMargin="80.8300" Scale9Width="142" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="142.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="344.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_no_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_no_addTime_3" Tag="101" IconVisible="False" LeftMargin="-71.0000" RightMargin="-71.0000" TopMargin="-202.6400" BottomMargin="154.6400" Scale9Width="142" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="142.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="178.6400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_no_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_no_addTime_4" Tag="102" IconVisible="False" LeftMargin="-420.9400" RightMargin="278.9400" TopMargin="-128.8300" BottomMargin="80.8300" Scale9Width="142" Scale9Height="48" ctype="ImageViewObjectData">
            <Size X="142.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-349.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_no_addTimes.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_bg_1" Tag="83" IconVisible="False" LeftMargin="-666.1015" RightMargin="577.1015" TopMargin="283.4445" BottomMargin="-372.4445" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
            <Size X="89.0000" Y="89.0000" />
            <Children>
              <AbstractNodeData Name="head_img" Tag="85" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_default_man.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_cover" Tag="84" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_bg_2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_status" VisibleForFrame="False" Tag="86" IconVisible="False" LeftMargin="-70.2319" RightMargin="134.2319" TopMargin="190.1830" BottomMargin="-152.1830" Scale9Width="25" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-57.7319" Y="-126.6830" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.6487" Y="-1.4234" />
                <PreSize X="0.2809" Y="0.5730" />
                <FileData Type="Normal" Path="game_res/head_enemy.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="gold_img" Tag="87" IconVisible="False" LeftMargin="96.3318" RightMargin="-48.3318" TopMargin="61.5208" BottomMargin="-0.5208" Scale9Width="41" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="41.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="116.8318" Y="13.4792" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.3127" Y="0.1515" />
                <PreSize X="0.4607" Y="0.3146" />
                <FileData Type="Normal" Path="game_res/gold.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score" ActionTag="213657952" Tag="111" IconVisible="False" LeftMargin="140.8317" RightMargin="-95.8317" TopMargin="66.0208" BottomMargin="-1.0208" FontSize="20" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="140.8317" Y="10.9792" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="1.5824" Y="0.1234" />
                <PreSize X="0.4944" Y="0.2697" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score_origin" VisibleForFrame="False" Tag="88" IconVisible="False" LeftMargin="2.8009" RightMargin="-11.8009" TopMargin="90.6255" BottomMargin="-24.6255" CharWidth="14" CharHeight="23" LabelText="1000000" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="2.8009" Y="-13.1255" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0315" Y="-0.1475" />
                <PreSize X="1.1011" Y="0.2584" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/player_gold_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_nickName" Tag="89" IconVisible="False" LeftMargin="4.5000" RightMargin="0.5000" TopMargin="-31.2700" BottomMargin="88.2700" FontSize="27" LabelText="本玩家" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="32.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="4.5000" Y="104.2700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0506" Y="1.1716" />
                <PreSize X="0.9438" Y="0.3596" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_curScore" VisibleForFrame="False" Tag="90" IconVisible="False" LeftMargin="202.6973" RightMargin="-244.6973" TopMargin="63.5208" BottomMargin="-0.5208" FontSize="22" LabelText="当前得分:100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="131.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="202.6973" Y="12.4792" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.2775" Y="0.1402" />
                <PreSize X="1.4719" Y="0.2921" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="heart" Tag="118" IconVisible="False" LeftMargin="67.1594" RightMargin="-17.1594" TopMargin="-12.4873" BottomMargin="67.4873" Scale9Width="39" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="39.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="86.6594" Y="84.4873" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9737" Y="0.9493" />
                <PreSize X="0.4382" Y="0.3820" />
                <FileData Type="Normal" Path="game_res/heart.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-621.6015" Y="-327.9445" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/head_bg_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_bg_2" Tag="91" IconVisible="False" LeftMargin="555.7600" RightMargin="-644.7600" TopMargin="-163.3558" BottomMargin="74.3558" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
            <Size X="89.0000" Y="89.0000" />
            <Children>
              <AbstractNodeData Name="head_img" Tag="92" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_default_man.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_cover" Tag="93" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_bg_2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_status" Tag="94" IconVisible="False" LeftMargin="67.8097" RightMargin="-3.8097" TopMargin="42.2541" BottomMargin="-4.2541" Scale9Width="25" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.3097" Y="21.2459" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9024" Y="0.2387" />
                <PreSize X="0.2809" Y="0.5730" />
                <FileData Type="Normal" Path="game_res/head_enemy.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="gold_img" Tag="95" IconVisible="False" LeftMargin="-20.5000" RightMargin="68.5000" TopMargin="91.0000" BottomMargin="-30.0000" Scale9Width="41" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="41.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-16.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="-0.1798" />
                <PreSize X="0.4607" Y="0.3146" />
                <FileData Type="Normal" Path="game_res/gold.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score" ActionTag="1575456096" Tag="112" IconVisible="False" LeftMargin="24.0000" RightMargin="21.0000" TopMargin="94.9190" BottomMargin="-29.9190" FontSize="20" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="24.0000" Y="-17.9190" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.2697" Y="-0.2013" />
                <PreSize X="0.4944" Y="0.2697" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score_origin" VisibleForFrame="False" Tag="96" IconVisible="False" LeftMargin="23.1102" RightMargin="-32.1102" TopMargin="95.5900" BottomMargin="-29.5900" CharWidth="14" CharHeight="23" LabelText="1000000" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="23.1102" Y="-18.0900" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2597" Y="-0.2033" />
                <PreSize X="1.1011" Y="0.2584" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/player_gold_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_nickName" Tag="97" IconVisible="False" LeftMargin="3.0000" RightMargin="2.0000" TopMargin="-31.2700" BottomMargin="88.2700" FontSize="27" LabelText="本玩家" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.0000" Y="104.2700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5056" Y="1.1716" />
                <PreSize X="0.9438" Y="0.3596" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_curScore" VisibleForFrame="False" Tag="98" IconVisible="False" LeftMargin="-26.7943" RightMargin="-15.2057" TopMargin="120.0700" BottomMargin="-57.0700" FontSize="22" LabelText="当前得分:100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="131.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7057" Y="-44.0700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4349" Y="-0.4952" />
                <PreSize X="1.4719" Y="0.2921" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="heart" Tag="117" IconVisible="False" LeftMargin="-14.5000" RightMargin="64.5000" TopMargin="-13.0000" BottomMargin="68.0000" Scale9Width="39" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="39.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="5.0000" Y="85.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0562" Y="0.9551" />
                <PreSize X="0.4382" Y="0.3820" />
                <FileData Type="Normal" Path="game_res/heart.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.2600" Y="118.8558" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/head_bg_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_bg_3" Tag="99" IconVisible="False" LeftMargin="149.0600" RightMargin="-238.0600" TopMargin="-342.3375" BottomMargin="253.3375" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
            <Size X="89.0000" Y="89.0000" />
            <Children>
              <AbstractNodeData Name="head_img" Tag="100" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_default_man.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_cover" Tag="101" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_bg_2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_status" Tag="102" IconVisible="False" LeftMargin="67.8097" RightMargin="-3.8097" TopMargin="42.2541" BottomMargin="-4.2541" LeftEage="8" RightEage="8" TopEage="16" BottomEage="16" Scale9OriginX="8" Scale9OriginY="16" Scale9Width="9" Scale9Height="19" ctype="ImageViewObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.3097" Y="21.2459" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9024" Y="0.2387" />
                <PreSize X="0.2809" Y="0.5730" />
                <FileData Type="Normal" Path="game_res/head_league.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="gold_img" Tag="103" IconVisible="False" LeftMargin="-20.5000" RightMargin="68.5000" TopMargin="91.0000" BottomMargin="-30.0000" Scale9Width="41" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="41.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-16.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="-0.1798" />
                <PreSize X="0.4607" Y="0.3146" />
                <FileData Type="Normal" Path="game_res/gold.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score" ActionTag="-427152420" Tag="113" IconVisible="False" LeftMargin="24.0000" RightMargin="21.0000" TopMargin="94.9200" BottomMargin="-29.9200" FontSize="20" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="24.0000" Y="-17.9200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.2697" Y="-0.2013" />
                <PreSize X="0.4944" Y="0.2697" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score_origin" VisibleForFrame="False" Tag="104" IconVisible="False" LeftMargin="24.1100" RightMargin="-33.1100" TopMargin="95.5900" BottomMargin="-29.5900" CharWidth="14" CharHeight="23" LabelText="1000000" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="24.1100" Y="-18.0900" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2709" Y="-0.2033" />
                <PreSize X="1.1011" Y="0.2584" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/player_gold_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_nickName" Tag="105" IconVisible="False" LeftMargin="3.0000" RightMargin="2.0000" TopMargin="-31.2700" BottomMargin="88.2700" FontSize="27" LabelText="本玩家" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.0000" Y="104.2700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5056" Y="1.1716" />
                <PreSize X="0.9438" Y="0.3596" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_curScore" VisibleForFrame="False" Tag="106" IconVisible="False" LeftMargin="-26.7943" RightMargin="-15.2057" TopMargin="120.0670" BottomMargin="-57.0670" FontSize="22" LabelText="当前得分:100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="131.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7057" Y="-44.0670" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4349" Y="-0.4951" />
                <PreSize X="1.4719" Y="0.2921" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="heart" Tag="116" IconVisible="False" LeftMargin="-14.5000" RightMargin="64.5000" TopMargin="-13.0000" BottomMargin="68.0000" Scale9Width="39" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="39.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="5.0000" Y="85.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0562" Y="0.9551" />
                <PreSize X="0.4382" Y="0.3820" />
                <FileData Type="Normal" Path="game_res/heart.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="193.5600" Y="297.8375" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/head_bg_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_bg_4" Tag="107" IconVisible="False" LeftMargin="-627.7107" RightMargin="538.7107" TopMargin="-162.3354" BottomMargin="73.3354" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
            <Size X="89.0000" Y="89.0000" />
            <Children>
              <AbstractNodeData Name="head_img" Tag="108" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_default_man.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_cover" Tag="109" IconVisible="False" Scale9Width="89" Scale9Height="89" ctype="ImageViewObjectData">
                <Size X="89.0000" Y="89.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="44.5000" Y="44.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="game_res/head_bg_2.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_status" Tag="110" IconVisible="False" LeftMargin="67.8097" RightMargin="-3.8097" TopMargin="42.2541" BottomMargin="-4.2541" Scale9Width="25" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.3097" Y="21.2459" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9024" Y="0.2387" />
                <PreSize X="0.2809" Y="0.5730" />
                <FileData Type="Normal" Path="game_res/head_enemy.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="gold_img" Tag="111" IconVisible="False" LeftMargin="-20.5000" RightMargin="68.5000" TopMargin="91.0000" BottomMargin="-30.0000" Scale9Width="41" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="41.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-16.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="-0.1798" />
                <PreSize X="0.4607" Y="0.3146" />
                <FileData Type="Normal" Path="game_res/gold.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score" ActionTag="-1468744652" Tag="114" IconVisible="False" LeftMargin="24.0000" RightMargin="21.0000" TopMargin="94.9200" BottomMargin="-29.9200" FontSize="20" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="24.0000" Y="-17.9200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.2697" Y="-0.2013" />
                <PreSize X="0.4944" Y="0.2697" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="player_score_origin" VisibleForFrame="False" Tag="112" IconVisible="False" LeftMargin="24.1101" RightMargin="-19.1101" TopMargin="95.5900" BottomMargin="-29.5900" CharWidth="14" CharHeight="23" LabelText="100000" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="84.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="24.1101" Y="-18.0900" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2709" Y="-0.2033" />
                <PreSize X="0.9438" Y="0.2584" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/player_gold_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_nickName" Tag="113" IconVisible="False" LeftMargin="3.0000" RightMargin="2.0000" TopMargin="-31.2700" BottomMargin="88.2700" FontSize="27" LabelText="本玩家" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.0000" Y="104.2700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5056" Y="1.1716" />
                <PreSize X="0.9438" Y="0.3596" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_curScore" VisibleForFrame="False" Tag="114" IconVisible="False" LeftMargin="-26.7943" RightMargin="-15.2057" TopMargin="120.0670" BottomMargin="-57.0670" FontSize="22" LabelText="当前得分:100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="131.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7057" Y="-44.0670" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4349" Y="-0.4951" />
                <PreSize X="1.4719" Y="0.2921" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="heart" Tag="115" IconVisible="False" LeftMargin="-14.5000" RightMargin="64.5000" TopMargin="-13.0000" BottomMargin="68.0000" Scale9Width="39" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="39.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="5.0000" Y="85.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0562" Y="0.9551" />
                <PreSize X="0.4382" Y="0.3820" />
                <FileData Type="Normal" Path="game_res/heart.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-583.2107" Y="117.8354" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/head_bg_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ExchangeSit_2" ActionTag="1941636943" Tag="116" IconVisible="False" LeftMargin="472.4980" RightMargin="-725.4980" TopMargin="-139.4994" BottomMargin="44.4994" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="598.9980" Y="91.9994" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/speak_btn_10_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/speak_btn_10_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/speak_btn_10.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ExchangeSit_3" ActionTag="-2114532647" Tag="117" IconVisible="False" LeftMargin="66.4988" RightMargin="-319.4988" TopMargin="-344.4998" BottomMargin="249.4998" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="192.9988" Y="296.9998" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/speak_btn_10_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/speak_btn_10_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/speak_btn_10.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ExchangeSit_4" ActionTag="-336915282" Tag="118" IconVisible="False" LeftMargin="-707.4999" RightMargin="454.4999" TopMargin="-168.4999" BottomMargin="73.4999" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-580.9999" Y="120.9999" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/speak_btn_10_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/speak_btn_10_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/speak_btn_10.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="record_bg" Tag="195" IconVisible="False" LeftMargin="341.2312" RightMargin="-620.2312" TopMargin="-631.5035" BottomMargin="482.5035" ctype="SpriteObjectData">
            <Size X="279.0000" Y="149.0000" />
            <Children>
              <AbstractNodeData Name="nickName_1" Tag="196" IconVisible="False" LeftMargin="16.3300" RightMargin="221.6700" TopMargin="62.0099" BottomMargin="68.9901" FontSize="16" LabelText="玩家1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="16.3300" Y="77.9901" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0585" Y="0.5234" />
                <PreSize X="0.1470" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_2" Tag="197" IconVisible="False" LeftMargin="16.3300" RightMargin="221.6700" TopMargin="83.0100" BottomMargin="47.9900" FontSize="16" LabelText="玩家1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="16.3300" Y="56.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0585" Y="0.3825" />
                <PreSize X="0.1470" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_3" Tag="198" IconVisible="False" LeftMargin="16.3300" RightMargin="221.6700" TopMargin="103.0100" BottomMargin="27.9900" FontSize="16" LabelText="玩家1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="16.3300" Y="36.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0585" Y="0.2483" />
                <PreSize X="0.1470" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_4" Tag="199" IconVisible="False" LeftMargin="16.3300" RightMargin="221.6700" TopMargin="122.0100" BottomMargin="8.9900" FontSize="16" LabelText="玩家1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="16.3300" Y="17.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0585" Y="0.1207" />
                <PreSize X="0.1470" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="lastRoundScore_1" Tag="200" IconVisible="False" LeftMargin="133.8300" RightMargin="136.1700" TopMargin="61.0100" BottomMargin="69.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="138.3300" Y="78.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4958" Y="0.5301" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="lastRoundScore_2" Tag="204" IconVisible="False" LeftMargin="133.8300" RightMargin="136.1700" TopMargin="82.0100" BottomMargin="48.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="138.3300" Y="57.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4958" Y="0.3892" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="lastRoundScore_3" Tag="205" IconVisible="False" LeftMargin="133.8300" RightMargin="136.1700" TopMargin="102.0100" BottomMargin="28.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="138.3300" Y="37.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4958" Y="0.2550" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="lastRoundScore_4" Tag="206" IconVisible="False" LeftMargin="133.8300" RightMargin="136.1700" TopMargin="121.0100" BottomMargin="9.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="138.3300" Y="18.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4958" Y="0.1274" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalScore_1" Tag="207" IconVisible="False" LeftMargin="222.8300" RightMargin="47.1700" TopMargin="61.0100" BottomMargin="69.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="227.3300" Y="78.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="255" B="0" />
                <PrePosition X="0.8148" Y="0.5301" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalScore_2" Tag="208" IconVisible="False" LeftMargin="222.8300" RightMargin="47.1700" TopMargin="82.0100" BottomMargin="48.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="227.3300" Y="57.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="255" B="0" />
                <PrePosition X="0.8148" Y="0.3892" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalScore_3" Tag="209" IconVisible="False" LeftMargin="222.8300" RightMargin="47.1700" TopMargin="102.0100" BottomMargin="28.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="227.3300" Y="37.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="255" B="0" />
                <PrePosition X="0.8148" Y="0.2550" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalScore_4" Tag="210" IconVisible="False" LeftMargin="222.8300" RightMargin="47.1700" TopMargin="121.0100" BottomMargin="9.9900" FontSize="16" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="227.3300" Y="18.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="255" B="0" />
                <PrePosition X="0.8148" Y="0.1274" />
                <PreSize X="0.0323" Y="0.1208" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.7312" Y="557.0035" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/record_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_ready_1" Tag="77" IconVisible="False" LeftMargin="-80.0000" RightMargin="-80.0000" TopMargin="82.0000" BottomMargin="-126.0000" Scale9Width="160" Scale9Height="44" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="44.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-104.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_ready.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_ready_2" Tag="78" IconVisible="False" LeftMargin="289.9400" RightMargin="-449.9400" TopMargin="-126.8300" BottomMargin="82.8300" Scale9Width="160" Scale9Height="44" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="44.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="369.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_ready.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_ready_3" Tag="79" IconVisible="False" LeftMargin="-80.0000" RightMargin="-80.0000" TopMargin="-200.6400" BottomMargin="156.6400" Scale9Width="160" Scale9Height="44" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="44.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="178.6400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_ready.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_ready_4" Tag="80" IconVisible="False" LeftMargin="-449.9400" RightMargin="289.9400" TopMargin="-126.8300" BottomMargin="82.8300" Scale9Width="160" Scale9Height="44" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="44.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-369.9400" Y="104.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/tip_ready.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_round_outTotalScore" Tag="81" IconVisible="False" LeftMargin="-145.9900" RightMargin="-68.0100" TopMargin="-31.1501" BottomMargin="-1.8499" Scale9Width="214" Scale9Height="33" ctype="ImageViewObjectData">
            <Size X="214.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-38.9900" Y="14.6501" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/round_outTotalScore.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="round_outTotalScore" Tag="82" IconVisible="False" LeftMargin="71.8100" RightMargin="-128.8100" TopMargin="-27.1501" BottomMargin="2.1501" CharWidth="19" CharHeight="25" LabelText="123" StartChar="0" ctype="TextAtlasObjectData">
            <Size X="57.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="71.8100" Y="14.6501" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/result_num.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="menu_bg" Tag="65" IconVisible="False" LeftMargin="788.3837" RightMargin="-916.3837" TopMargin="-360.3463" BottomMargin="-86.6537" TouchEnable="True" Scale9Width="128" Scale9Height="447" ctype="ImageViewObjectData">
            <Size X="128.0000" Y="447.0000" />
            <Children>
              <AbstractNodeData Name="bt_setting" Tag="66" IconVisible="False" LeftMargin="23.0000" RightMargin="25.0000" TopMargin="95.7500" BottomMargin="271.2500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="58" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="80.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="311.2500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4922" Y="0.6963" />
                <PreSize X="0.6250" Y="0.1790" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/bt_setting_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/bt_setting_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/bt_setting_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_trusteeship" Tag="67" IconVisible="False" LeftMargin="6.0000" RightMargin="8.0000" TopMargin="183.8800" BottomMargin="186.1200" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="84" Scale9Height="55" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="114.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="224.6200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4922" Y="0.5025" />
                <PreSize X="0.8906" Y="0.1723" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/bt_trusteeship_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/bt_trusteeship_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/bt_trusteeship_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_help" Tag="68" IconVisible="False" LeftMargin="6.0000" RightMargin="8.0000" TopMargin="272.2900" BottomMargin="97.7100" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="84" Scale9Height="55" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="114.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="136.2100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4922" Y="0.3047" />
                <PreSize X="0.8906" Y="0.1723" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/bt_help_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/bt_help_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/bt_help_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_exit" Tag="69" IconVisible="False" LeftMargin="6.0000" RightMargin="8.0000" TopMargin="360.8700" BottomMargin="9.1300" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="84" Scale9Height="55" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="114.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="47.6300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4922" Y="0.1066" />
                <PreSize X="0.8906" Y="0.1723" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/bt_exit_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/bt_exit_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/bt_exit_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.9000" />
            <Position X="852.3837" Y="315.6463" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/menu_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_menu" Tag="62" IconVisible="False" LeftMargin="577.8580" RightMargin="-657.8580" TopMargin="-375.5536" BottomMargin="295.5536" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="58" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="617.8580" Y="335.5536" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_setting_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_setting_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_setting_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="time_bg" Tag="75" IconVisible="False" LeftMargin="-54.8052" RightMargin="-54.1948" TopMargin="-48.0000" BottomMargin="-48.0000" Scale9Width="109" Scale9Height="96" ctype="ImageViewObjectData">
            <Size X="109.0000" Y="96.0000" />
            <Children>
              <AbstractNodeData Name="time_num" Tag="76" IconVisible="False" LeftMargin="24.8281" RightMargin="30.1719" TopMargin="24.1659" BottomMargin="32.8341" CharWidth="27" CharHeight="39" LabelText="15" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="54.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="51.8281" Y="52.3341" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4755" Y="0.5451" />
                <PreSize X="0.4954" Y="0.4063" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game_res/time_num.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5028" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/timeBG_1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_start" Tag="83" IconVisible="False" LeftMargin="-126.5000" RightMargin="-126.5000" TopMargin="56.5000" BottomMargin="-151.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="253.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-104.0000" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_start_2.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_start_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_start_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="trusteeship_bg" CanEdit="False" Tag="116" IconVisible="False" LeftMargin="-667.6600" RightMargin="-666.3400" TopMargin="112.5900" BottomMargin="-374.5900" Scale9Width="1334" Scale9Height="262" ctype="ImageViewObjectData">
            <Size X="1334.0000" Y="262.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.6600" Y="-243.5900" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/isTrusteeship.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_voice" Tag="103" IconVisible="False" LeftMargin="579.9072" RightMargin="-651.9072" TopMargin="-13.5890" BottomMargin="-56.4110" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="42" Scale9Height="48" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="72.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="615.9072" Y="-21.4110" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_voice_1.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_voice_2.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_voice_1.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="img_voice_box_1" Tag="186" IconVisible="False" LeftMargin="-593.4455" RightMargin="490.4455" TopMargin="190.0832" BottomMargin="-263.0832" Scale9Width="103" Scale9Height="73" ctype="ImageViewObjectData">
            <Size X="103.0000" Y="73.0000" />
            <Children>
              <AbstractNodeData Name="img_voice" Tag="187" IconVisible="False" LeftMargin="40.5000" RightMargin="39.5000" TopMargin="13.4700" BottomMargin="27.5300" ctype="SpriteObjectData">
                <Size X="23.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0000" Y="43.5300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5049" Y="0.5963" />
                <PreSize X="0.2233" Y="0.4384" />
                <FileData Type="PlistSubImage" Path="img_voice_2.png" Plist="game_res/voice_ani.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-541.9455" Y="-226.5832" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/img_voice_box.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="img_voice_box_2" Tag="188" IconVisible="False" LeftMargin="452.0031" RightMargin="-555.0031" TopMargin="-211.5100" BottomMargin="138.5100" FlipX="True" Scale9Width="103" Scale9Height="73" ctype="ImageViewObjectData">
            <Size X="103.0000" Y="73.0000" />
            <Children>
              <AbstractNodeData Name="img_voice" Tag="189" IconVisible="False" LeftMargin="40.5000" RightMargin="39.5000" TopMargin="13.4700" BottomMargin="27.5300" ctype="SpriteObjectData">
                <Size X="23.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0000" Y="43.5300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5049" Y="0.5963" />
                <PreSize X="0.2233" Y="0.4384" />
                <FileData Type="PlistSubImage" Path="img_voice_2.png" Plist="game_res/voice_ani.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="503.5031" Y="175.0100" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/img_voice_box.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="img_voice_box_3" Tag="190" IconVisible="False" LeftMargin="-25.7125" RightMargin="-77.2875" TopMargin="-369.8594" BottomMargin="296.8594" FlipX="True" Scale9Width="103" Scale9Height="73" ctype="ImageViewObjectData">
            <Size X="103.0000" Y="73.0000" />
            <Children>
              <AbstractNodeData Name="img_voice" Tag="191" IconVisible="False" LeftMargin="40.5000" RightMargin="39.5000" TopMargin="13.4700" BottomMargin="27.5300" ctype="SpriteObjectData">
                <Size X="23.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0000" Y="43.5300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5049" Y="0.5963" />
                <PreSize X="0.2233" Y="0.4384" />
                <FileData Type="PlistSubImage" Path="img_voice_2.png" Plist="game_res/voice_ani.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="25.7875" Y="333.3594" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/img_voice_box.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="img_voice_box_4" Tag="192" IconVisible="False" LeftMargin="-532.6851" RightMargin="429.6851" TopMargin="-213.0532" BottomMargin="140.0532" Scale9Width="103" Scale9Height="73" ctype="ImageViewObjectData">
            <Size X="103.0000" Y="73.0000" />
            <Children>
              <AbstractNodeData Name="img_voice" Tag="193" IconVisible="False" LeftMargin="40.5000" RightMargin="39.5000" TopMargin="13.4700" BottomMargin="27.5300" ctype="SpriteObjectData">
                <Size X="23.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0000" Y="43.5300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5049" Y="0.5963" />
                <PreSize X="0.2233" Y="0.4384" />
                <FileData Type="PlistSubImage" Path="img_voice_2.png" Plist="game_res/voice_ani.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-481.1851" Y="176.5532" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/img_voice_box.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_record_2" Tag="144" IconVisible="False" LeftMargin="417.2469" RightMargin="-517.2469" TopMargin="-357.9242" BottomMargin="327.9242" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="12" RightEage="12" TopEage="11" BottomEage="11" Scale9OriginX="-12" Scale9OriginY="-11" Scale9Width="24" Scale9Height="22" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="100.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="467.2469" Y="342.9242" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="alert_2" Tag="120" IconVisible="True" LeftMargin="493.0900" RightMargin="-493.0900" TopMargin="-193.1900" BottomMargin="193.1900" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="493.0900" Y="193.1900" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="alert_3" Tag="119" IconVisible="True" LeftMargin="-109.8987" RightMargin="109.8987" TopMargin="-280.2067" BottomMargin="280.2067" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-109.8987" Y="280.2067" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="alert_4" Tag="118" IconVisible="True" LeftMargin="-473.0900" RightMargin="473.0900" TopMargin="-193.1900" BottomMargin="193.1900" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-473.0900" Y="193.1900" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="SpeakBtnPanel" ActionTag="-1921278770" Tag="209" IconVisible="False" LeftMargin="-445.0000" RightMargin="-445.0000" TopMargin="320.4656" BottomMargin="-370.4656" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="890.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="SpeakBtn_1" ActionTag="-1869394831" Tag="210" IconVisible="False" LeftMargin="-76.5000" RightMargin="713.5000" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.0000" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0562" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_1_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_1_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_1.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_2" ActionTag="-246909777" Tag="211" IconVisible="False" LeftMargin="33.7500" RightMargin="603.2500" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.2500" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1801" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_2_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_2_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_2.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_3" ActionTag="1000291595" Tag="212" IconVisible="False" LeftMargin="144.0000" RightMargin="493.0000" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="270.5000" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3039" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_3_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_3_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_3.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_4" ActionTag="-28145728" Tag="213" IconVisible="False" LeftMargin="254.2500" RightMargin="382.7500" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="380.7500" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4278" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_4_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_4_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_4.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_5" ActionTag="-1479613058" Tag="214" IconVisible="False" LeftMargin="364.5000" RightMargin="272.5000" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="491.0000" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5517" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_5_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_5_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_5.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_6" ActionTag="-1918195463" Tag="215" IconVisible="False" LeftMargin="474.7500" RightMargin="162.2500" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="601.2500" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6756" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_6_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_6_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_6.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_7" ActionTag="-1542172708" Tag="216" IconVisible="False" LeftMargin="585.0000" RightMargin="52.0000" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="711.5000" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7994" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_7_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_7_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_7.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_8" ActionTag="2077981002" Tag="217" IconVisible="False" LeftMargin="695.2501" RightMargin="-58.2501" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="821.7501" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9233" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_8_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_8_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_8.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpeakBtn_9" ActionTag="1256194402" Tag="218" IconVisible="False" LeftMargin="805.5001" RightMargin="-168.5001" TopMargin="-16.6988" BottomMargin="-28.3012" TouchEnable="True" FontSize="40" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="223" Scale9Height="73" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="253.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="932.0001" Y="19.1988" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0472" Y="0.3840" />
                <PreSize X="0.2843" Y="1.9000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_res/speak_btn_9_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_res/speak_btn_9_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_res/speak_btn_9.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-345.4656" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="SortcardBtn" ActionTag="275771389" Tag="130" IconVisible="False" LeftMargin="552.7072" RightMargin="-676.7072" TopMargin="63.0000" BottomMargin="-145.0000" TouchEnable="True" FontSize="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="94" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="124.0000" Y="82.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="614.7072" Y="-104.0000" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game_res/bt_lipai.png" Plist="" />
            <PressedFileData Type="Normal" Path="game_res/bt_lipai.png" Plist="" />
            <NormalFileData Type="Normal" Path="game_res/bt_lipai.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_1" ActionTag="-867237974" Tag="152" IconVisible="False" LeftMargin="323.2532" RightMargin="-343.2532" TopMargin="-354.8654" BottomMargin="329.8654" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="333.2532" Y="342.3654" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_2" ActionTag="1446759550" Tag="153" IconVisible="False" LeftMargin="352.0032" RightMargin="-372.0032" TopMargin="-354.8654" BottomMargin="329.8654" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="362.0032" Y="342.3654" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_3" ActionTag="248760561" Tag="154" IconVisible="False" LeftMargin="380.7532" RightMargin="-400.7532" TopMargin="-354.8654" BottomMargin="329.8654" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="390.7532" Y="342.3654" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_4" ActionTag="-1548301204" VisibleForFrame="False" Tag="155" IconVisible="False" LeftMargin="321.5994" RightMargin="-341.5994" TopMargin="-459.9689" BottomMargin="434.9689" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="331.5994" Y="447.4689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_5" ActionTag="-1703306234" VisibleForFrame="False" Tag="156" IconVisible="False" LeftMargin="350.3493" RightMargin="-370.3493" TopMargin="-459.9689" BottomMargin="434.9689" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="360.3493" Y="447.4689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_6" ActionTag="1766941445" VisibleForFrame="False" Tag="157" IconVisible="False" LeftMargin="379.0992" RightMargin="-399.0992" TopMargin="-459.9689" BottomMargin="434.9689" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="389.0992" Y="447.4689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_7" ActionTag="288204941" VisibleForFrame="False" Tag="158" IconVisible="False" LeftMargin="321.5994" RightMargin="-341.5994" TopMargin="-431.9689" BottomMargin="406.9689" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="331.5994" Y="419.4689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_8" ActionTag="1042931128" VisibleForFrame="False" Tag="159" IconVisible="False" LeftMargin="350.3493" RightMargin="-370.3493" TopMargin="-431.9689" BottomMargin="406.9689" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="360.3493" Y="419.4689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_yes.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Papo_9" ActionTag="1152626424" VisibleForFrame="False" Tag="160" IconVisible="False" LeftMargin="379.0992" RightMargin="-399.0992" TopMargin="-431.9689" BottomMargin="406.9689" ctype="SpriteObjectData">
            <Size X="20.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="389.0992" Y="419.4689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_res/papo_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="DisplayHandCardNode_3" ActionTag="667934301" Tag="126" IconVisible="True" LeftMargin="91.0668" RightMargin="-91.0668" TopMargin="-253.1244" BottomMargin="253.1244" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="91.0668" Y="253.1244" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="DisplayHandCardNode_2" ActionTag="-1300932858" Tag="127" IconVisible="True" LeftMargin="472.6457" RightMargin="-472.6457" TopMargin="-28.7388" BottomMargin="28.7388" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="472.6457" Y="28.7388" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="DisplayHandCardNode_4" ActionTag="204267060" Tag="128" IconVisible="True" LeftMargin="-439.4384" RightMargin="439.4384" TopMargin="-139.7414" BottomMargin="139.7414" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-439.4384" Y="139.7414" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="DisplayHandCardNode_1" ActionTag="2081074659" Tag="129" IconVisible="True" LeftMargin="-401.7207" RightMargin="401.7207" TopMargin="214.4373" BottomMargin="-214.4373" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-401.7207" Y="-214.4373" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>