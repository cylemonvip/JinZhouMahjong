<GameFile>
  <PropertyGroup Name="ResultLayer" Type="Node" ID="f75258f0-bee8-4ad5-b4b9-668f6c750f02" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="PanelMask" ActionTag="-1429416753" Tag="188" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" Tag="119" IconVisible="False" LeftMargin="-378.0000" RightMargin="-378.0000" TopMargin="-253.0000" BottomMargin="-253.0000" LeftEage="249" RightEage="249" TopEage="166" BottomEage="166" Scale9OriginX="249" Scale9OriginY="166" Scale9Width="258" Scale9Height="174" ctype="ImageViewObjectData">
            <Size X="756.0000" Y="506.0000" />
            <Children>
              <AbstractNodeData Name="Image_win" ActionTag="-967528290" Tag="184" IconVisible="False" LeftMargin="18.6184" RightMargin="41.3816" TopMargin="112.8903" BottomMargin="243.1097" Scale9Enable="True" LeftEage="229" RightEage="229" TopEage="21" BottomEage="21" Scale9OriginX="229" Scale9OriginY="21" Scale9Width="238" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="696.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="366.6184" Y="318.1097" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4849" Y="0.6287" />
                <PreSize X="0.9206" Y="0.2964" />
                <FileData Type="Normal" Path="result_res/Sprite_userBg_win.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_lose" ActionTag="-873185901" Tag="185" IconVisible="False" LeftMargin="18.6184" RightMargin="41.3816" TopMargin="260.7679" BottomMargin="95.2321" Scale9Enable="True" LeftEage="229" RightEage="229" TopEage="21" BottomEage="21" Scale9OriginX="229" Scale9OriginY="21" Scale9Width="238" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="696.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="366.6184" Y="170.2321" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4849" Y="0.3364" />
                <PreSize X="0.9206" Y="0.2964" />
                <FileData Type="Normal" Path="result_res/Sprite_userBg_lose.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_1" Tag="131" IconVisible="False" LeftMargin="114.9502" RightMargin="577.0498" TopMargin="136.2834" BottomMargin="340.7166" FontSize="25" LabelText="玩家1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="114.9502" Y="355.2166" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.1521" Y="0.7020" />
                <PreSize X="0.0847" Y="0.0573" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_2" Tag="133" IconVisible="False" LeftMargin="114.9502" RightMargin="577.0498" TopMargin="210.6494" BottomMargin="266.3506" FontSize="25" LabelText="玩家2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="114.9502" Y="280.8506" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.1521" Y="0.5550" />
                <PreSize X="0.0847" Y="0.0573" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_3" Tag="134" IconVisible="False" LeftMargin="114.9502" RightMargin="577.0498" TopMargin="285.6821" BottomMargin="191.3179" FontSize="25" LabelText="玩家3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="114.9502" Y="205.8179" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.1521" Y="0.4068" />
                <PreSize X="0.0847" Y="0.0573" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="nickName_4" Tag="135" IconVisible="False" LeftMargin="114.9501" RightMargin="577.0499" TopMargin="359.3813" BottomMargin="117.6187" FontSize="25" LabelText="玩家4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="114.9501" Y="132.1187" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="0.1521" Y="0.2611" />
                <PreSize X="0.0847" Y="0.0573" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="img_result_1" Tag="132" IconVisible="False" LeftMargin="440.1683" RightMargin="262.8317" TopMargin="159.7282" BottomMargin="293.2718" Scale9Width="53" Scale9Height="53" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="466.6683" Y="319.7718" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6173" Y="0.6320" />
                <PreSize X="0.0701" Y="0.1047" />
                <FileData Type="Normal" Path="result_res/win.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="img_result_2" Tag="138" IconVisible="False" LeftMargin="440.1683" RightMargin="262.8317" TopMargin="308.0049" BottomMargin="144.9951" Scale9Width="53" Scale9Height="53" ctype="ImageViewObjectData">
                <Size X="53.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="466.6683" Y="171.4951" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6173" Y="0.3389" />
                <PreSize X="0.0701" Y="0.1047" />
                <FileData Type="Normal" Path="result_res/win.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="curResult_1" Tag="147" IconVisible="False" LeftMargin="630.8795" RightMargin="78.1205" TopMargin="169.2283" BottomMargin="302.7717" FontSize="30" LabelText="0分" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="47.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="654.3795" Y="319.7717" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.8656" Y="0.6320" />
                <PreSize X="0.0622" Y="0.0672" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="curResult_2" Tag="149" IconVisible="False" LeftMargin="630.8795" RightMargin="78.1205" TopMargin="321.0049" BottomMargin="150.9951" FontSize="30" LabelText="0分" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="47.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="654.3795" Y="167.9951" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.8656" Y="0.3320" />
                <PreSize X="0.0622" Y="0.0672" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_again" Tag="151" IconVisible="False" LeftMargin="293.8474" RightMargin="294.1526" TopMargin="409.4241" BottomMargin="17.5759" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="57" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="79.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="377.8474" Y="57.0759" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4998" Y="0.1128" />
                <PreSize X="0.2222" Y="0.1561" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="result_res/bt_again_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="result_res/bt_again_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="result_res/bt_again_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_exit" Tag="152" IconVisible="False" LeftMargin="490.6637" RightMargin="97.3363" TopMargin="811.7931" BottomMargin="-384.7931" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="57" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="79.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="574.6637" Y="-345.2931" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="-0.6824" />
                <PreSize X="0.2222" Y="0.1561" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="result_res/bt_exit_1.png" Plist="" />
                <PressedFileData Type="Normal" Path="result_res/bt_exit_2.png" Plist="" />
                <NormalFileData Type="Normal" Path="result_res/bt_exit_1.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadBg_1" ActionTag="2115827344" Tag="34" IconVisible="False" LeftMargin="52.8147" RightMargin="657.1853" TopMargin="125.8304" BottomMargin="334.1696" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.8147" Y="357.1696" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1003" Y="0.7059" />
                <PreSize X="0.0608" Y="0.0909" />
                <FileData Type="Normal" Path="result_res/Sprite_faceBg_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadBg_2" ActionTag="1677739134" Tag="35" IconVisible="False" LeftMargin="52.8147" RightMargin="657.1853" TopMargin="200.4032" BottomMargin="259.5968" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.8147" Y="282.5968" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1003" Y="0.5585" />
                <PreSize X="0.0608" Y="0.0909" />
                <FileData Type="Normal" Path="result_res/Sprite_faceBg_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadBg_3" ActionTag="-398248505" Tag="36" IconVisible="False" LeftMargin="52.8147" RightMargin="657.1853" TopMargin="274.9760" BottomMargin="185.0240" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.8147" Y="208.0240" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1003" Y="0.4111" />
                <PreSize X="0.0608" Y="0.0909" />
                <FileData Type="Normal" Path="result_res/Sprite_faceBg_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadBg_4" ActionTag="-427397259" Tag="37" IconVisible="False" LeftMargin="52.8147" RightMargin="657.1853" TopMargin="349.5488" BottomMargin="110.4512" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.8147" Y="133.4512" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1003" Y="0.2637" />
                <PreSize X="0.0608" Y="0.0909" />
                <FileData Type="Normal" Path="result_res/Sprite_faceBg_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="DdInfoFlag_1" ActionTag="-1170522327" Tag="38" IconVisible="False" LeftMargin="26.2108" RightMargin="704.7892" TopMargin="125.1331" BottomMargin="329.8669" ctype="SpriteObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7108" Y="355.3669" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0512" Y="0.7023" />
                <PreSize X="0.0331" Y="0.1008" />
                <FileData Type="Normal" Path="game_res/head_enemy.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="DdInfoFlag_2" ActionTag="-1715218083" Tag="39" IconVisible="False" LeftMargin="26.2108" RightMargin="704.7892" TopMargin="198.9752" BottomMargin="256.0248" ctype="SpriteObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7108" Y="281.5248" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0512" Y="0.5564" />
                <PreSize X="0.0331" Y="0.1008" />
                <FileData Type="Normal" Path="game_res/head_enemy.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="DdInfoFlag_3" ActionTag="-231778373" Tag="40" IconVisible="False" LeftMargin="26.2108" RightMargin="704.7892" TopMargin="273.5480" BottomMargin="181.4520" ctype="SpriteObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7108" Y="206.9520" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0512" Y="0.4090" />
                <PreSize X="0.0331" Y="0.1008" />
                <FileData Type="Normal" Path="game_res/head_league.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="DdInfoFlag_4" ActionTag="1943847017" Tag="41" IconVisible="False" LeftMargin="26.2108" RightMargin="704.7892" TopMargin="348.1208" BottomMargin="106.8792" ctype="SpriteObjectData">
                <Size X="25.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.7108" Y="132.3792" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0512" Y="0.2616" />
                <PreSize X="0.0331" Y="0.1008" />
                <FileData Type="Normal" Path="game_res/head_league.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="falg_h_n_1" ActionTag="1390276634" Tag="155" IconVisible="False" LeftMargin="265.6620" RightMargin="449.3380" TopMargin="110.0958" BottomMargin="312.9042" ctype="SpriteObjectData">
                <Size X="41.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.1620" Y="354.4042" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3785" Y="0.7004" />
                <PreSize X="0.0542" Y="0.1640" />
                <FileData Type="Normal" Path="game_res/flag_huangshang.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="falg_h_n_2" ActionTag="-256369401" Tag="156" IconVisible="False" LeftMargin="265.6620" RightMargin="449.3380" TopMargin="183.8932" BottomMargin="239.1068" ctype="SpriteObjectData">
                <Size X="41.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.1620" Y="280.6068" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3785" Y="0.5546" />
                <PreSize X="0.0542" Y="0.1640" />
                <FileData Type="Normal" Path="game_res/flag_huangshang.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="falg_h_n_3" ActionTag="266473322" Tag="157" IconVisible="False" LeftMargin="265.6620" RightMargin="449.3380" TopMargin="257.6907" BottomMargin="165.3093" ctype="SpriteObjectData">
                <Size X="41.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.1620" Y="206.8093" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3785" Y="0.4087" />
                <PreSize X="0.0542" Y="0.1640" />
                <FileData Type="Normal" Path="game_res/flag_huangshang.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="falg_h_n_4" ActionTag="-318190355" Tag="158" IconVisible="False" LeftMargin="265.6620" RightMargin="449.3380" TopMargin="331.4881" BottomMargin="91.5119" ctype="SpriteObjectData">
                <Size X="41.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.1620" Y="133.0119" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3785" Y="0.2629" />
                <PreSize X="0.0542" Y="0.1640" />
                <FileData Type="Normal" Path="game_res/flag_huangshang.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="1257038746" Tag="151" IconVisible="False" LeftMargin="224.0149" RightMargin="218.9851" TopMargin="126.6659" BottomMargin="117.3341" ctype="SpriteObjectData">
                <Size X="313.0000" Y="262.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="380.5149" Y="248.3341" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5033" Y="0.4908" />
                <PreSize X="0.0608" Y="0.0909" />
                <FileData Type="Normal" Path="game_res/text_vs.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="result_res/clear_bg_win.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>