<GameFile>
  <PropertyGroup Name="RoomTableLayout" Type="Layer" ID="1f99ae66-a629-45d2-af59-5be803f3023c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="177" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="345476891" Tag="179" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ctype="SpriteObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="roomtable_2" ActionTag="-1615188027" Tag="180" IconVisible="False" LeftMargin="67.6542" RightMargin="1034.3457" TopMargin="245.6654" BottomMargin="299.3346" ctype="SpriteObjectData">
            <Size X="232.0000" Y="205.0000" />
            <Children>
              <AbstractNodeData Name="chair_empty_3" ActionTag="1187148112" Tag="181" IconVisible="False" LeftMargin="-7.5000" RightMargin="188.5000" TopMargin="49.5000" BottomMargin="104.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="18.0000" Y="130.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0776" Y="0.6341" />
                <PreSize X="0.2198" Y="0.2488" />
                <FileData Type="Normal" Path="public_res/chair_empty.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chair_empty_3_0" ActionTag="906661297" Tag="182" IconVisible="False" LeftMargin="186.5000" RightMargin="-5.5000" TopMargin="49.5000" BottomMargin="104.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="212.0000" Y="130.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9138" Y="0.6341" />
                <PreSize X="0.2198" Y="0.2488" />
                <FileData Type="Normal" Path="public_res/chair_empty.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chair_empty_3_0_0" ActionTag="-1592042271" Tag="183" IconVisible="False" LeftMargin="89.5000" RightMargin="91.5000" TopMargin="203.5000" BottomMargin="-49.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="115.0000" Y="-24.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4957" Y="-0.1171" />
                <PreSize X="0.2198" Y="0.2488" />
                <FileData Type="Normal" Path="public_res/chair_empty.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="183.6542" Y="401.8346" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1377" Y="0.5358" />
            <PreSize X="0.1739" Y="0.2733" />
            <FileData Type="Normal" Path="public_res/roomtable.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>