<GameFile>
  <PropertyGroup Name="Player" Type="Layer" ID="5f0fb4aa-b0a2-41c0-89d9-a0626f565963" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="32" ctype="GameLayerObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="sp_headCover" ActionTag="-1631372913" Tag="2" IconVisible="False" LeftMargin="-48.0000" RightMargin="-48.0000" TopMargin="-59.3640" BottomMargin="-36.6360" ctype="SpriteObjectData">
            <Size X="96.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="11.3640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_nickname" ActionTag="-1714128219" Tag="3" IconVisible="False" LeftMargin="-53.1388" RightMargin="-49.8612" TopMargin="36.5133" BottomMargin="-62.5133" FontSize="20" LabelText="身后有尾巴" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="103.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.6388" Y="-49.5133" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Aslab_score" ActionTag="967830280" Tag="4" IconVisible="False" LeftMargin="-9.7334" RightMargin="-6.2666" TopMargin="76.9311" BottomMargin="-102.9311" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="16.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.7334" Y="-89.9311" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="242" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_ready" ActionTag="-729668461" Tag="5" IconVisible="False" LeftMargin="72.7932" RightMargin="-169.7932" TopMargin="-27.8023" BottomMargin="-23.1977" ctype="SpriteObjectData">
            <Size X="97.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="121.2932" Y="2.3023" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gamesceneplist/sp_ready.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_banker" ActionTag="2048453043" Tag="7" IconVisible="False" LeftMargin="-62.0000" RightMargin="24.0000" TopMargin="-75.1947" BottomMargin="38.1947" ctype="SpriteObjectData">
            <Size X="38.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-43.0000" Y="56.6947" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/zhuang_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_host" ActionTag="-1344979282" Tag="8" IconVisible="False" LeftMargin="-94.1984" RightMargin="52.1984" TopMargin="-3.5215" BottomMargin="-37.4785" ctype="SpriteObjectData">
            <Size X="42.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-73.1984" Y="-16.9785" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gamesceneplist/sp_chooseFlag.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ipTxt" ActionTag="-452665102" Tag="137" IconVisible="False" LeftMargin="-47.5009" RightMargin="-47.4991" TopMargin="-24.8493" BottomMargin="-1.1507" FontSize="20" LabelText="192.168.0.1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="95.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0009" Y="11.8493" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="GameIDTxt" ActionTag="-499044939" Tag="82" IconVisible="False" LeftMargin="-34.4203" RightMargin="-31.5797" TopMargin="57.7602" BottomMargin="-83.7602" FontSize="20" LabelText="832321" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="66.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.4203" Y="-70.7602" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>