<GameFile>
  <PropertyGroup Name="GameResultLayer" Type="Scene" ID="5383186a-ba16-4f1b-aa57-a64d26f3f633" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="3" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-1414793136" Tag="215" IconVisible="False" LeftMargin="-99.4225" RightMargin="-100.5775" TopMargin="-75.0000" BottomMargin="-75.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1534.0000" Y="900.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.5775" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5004" Y="0.5000" />
            <PreSize X="1.1499" Y="1.2000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="result_background" ActionTag="-916984261" Tag="261" IconVisible="False" TopMargin="0.7437" BottomMargin="-0.7437" LeftEage="264" RightEage="264" TopEage="158" BottomEage="158" Scale9OriginX="264" Scale9OriginY="158" Scale9Width="272" Scale9Height="164" ctype="ImageViewObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="374.2563" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4990" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="gameResult/game_over_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TitleSp" ActionTag="15130781" Tag="268" IconVisible="False" LeftMargin="626.0002" RightMargin="625.9998" TopMargin="36.0103" BottomMargin="671.9897" ctype="SpriteObjectData">
            <Size X="82.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0002" Y="692.9897" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9240" />
            <PreSize X="0.0615" Y="0.0560" />
            <FileData Type="Normal" Path="gameResult/endtitle.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_banker" ActionTag="-1007326614" Tag="6" IconVisible="False" LeftMargin="30.4736" RightMargin="1265.5264" TopMargin="551.5000" BottomMargin="161.5000" ctype="SpriteObjectData">
            <Size X="38.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="49.4736" Y="180.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0371" Y="0.2400" />
            <PreSize X="0.0285" Y="0.0493" />
            <FileData Type="Normal" Path="game/zhuang_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_recodeShow" ActionTag="-1909096868" Tag="8" IconVisible="False" LeftMargin="407.5001" RightMargin="807.4999" TopMargin="666.3378" BottomMargin="32.6622" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="89" Scale9Height="29" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="119.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="467.0001" Y="58.1622" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3501" Y="0.0775" />
            <PreSize X="0.0892" Y="0.0680" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="gameResult/share_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="gameResult/share_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="gameResult/share.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_continue" ActionTag="680418892" Tag="9" IconVisible="False" LeftMargin="807.5000" RightMargin="407.5000" TopMargin="666.3378" BottomMargin="32.6622" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="89" Scale9Height="29" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="119.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="867.0000" Y="58.1622" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6499" Y="0.0775" />
            <PreSize X="0.0892" Y="0.0680" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="gameResult/start_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="gameResult/start_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="gameResult/start.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_YXGZ" ActionTag="1495928738" VisibleForFrame="False" Tag="121" IconVisible="False" LeftMargin="347.0000" RightMargin="367.0000" TopMargin="98.0341" BottomMargin="614.9659" FontSize="30" LabelText="游戏规则：三清 天地胡 会牌 封顶150 穷胡加倍" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="620.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="657.0000" Y="633.4659" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.4925" Y="0.8446" />
            <PreSize X="0.4648" Y="0.0493" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="77" G="77" B="77" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_YXGZ_1" ActionTag="-1374790502" Tag="122" IconVisible="False" LeftMargin="357.0000" RightMargin="357.0000" TopMargin="-107.4876" BottomMargin="820.4876" FontSize="30" LabelText="游戏规则：三清 天地胡 会牌 封顶150 穷胡加倍" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="620.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="838.9876" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.5000" Y="1.1187" />
            <PreSize X="0.4648" Y="0.0493" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="77" G="77" B="77" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_ResultDJS" ActionTag="1420068867" Tag="195" IconVisible="False" LeftMargin="1264.9678" RightMargin="9.0322" TopMargin="11.2007" BottomMargin="677.7993" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="61.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1294.9678" Y="708.2993" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9707" Y="0.9444" />
            <PreSize X="0.0450" Y="0.0813" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimeTxt" ActionTag="-1380542040" Tag="265" IconVisible="False" LeftMargin="1096.7203" RightMargin="30.2797" TopMargin="703.9167" BottomMargin="20.0833" FontSize="20" LabelText="日期:2018-08-08 18:36" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="207.0000" Y="26.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1303.7203" Y="33.0833" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9773" Y="0.0441" />
            <PreSize X="0.1552" Y="0.0347" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="465039903" Tag="262" IconVisible="False" LeftMargin="67.0000" RightMargin="67.0000" TopMargin="118.0000" BottomMargin="472.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1200.0000" Y="160.0000" />
            <Children>
              <AbstractNodeData Name="HeadIcon" ActionTag="-1211147234" Tag="263" IconVisible="False" LeftMargin="22.7819" RightMargin="1081.2181" TopMargin="34.5263" BottomMargin="29.4737" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="70.7819" Y="77.4737" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0590" Y="0.4842" />
                <PreSize X="0.0800" Y="0.6000" />
                <FileData Type="Normal" Path="Avatar46.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadKuang" ActionTag="-1633101933" Tag="264" IconVisible="False" LeftMargin="23.0000" RightMargin="1081.0000" TopMargin="34.3247" BottomMargin="29.6753" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="71.0000" Y="77.6753" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0592" Y="0.4855" />
                <PreSize X="0.0800" Y="0.6000" />
                <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameIDTxt" ActionTag="1823268172" Tag="265" IconVisible="False" LeftMargin="146.8531" RightMargin="969.1469" TopMargin="0.0045" BottomMargin="130.9955" FontSize="25" LabelText="168322" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="146.8531" Y="145.4955" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1224" Y="0.9093" />
                <PreSize X="0.0700" Y="0.1813" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickNameTxt" ActionTag="1695352584" Tag="266" IconVisible="False" LeftMargin="305.6833" RightMargin="819.3167" TopMargin="0.0045" BottomMargin="130.9955" FontSize="25" LabelText="胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="305.6833" Y="145.4955" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2547" Y="0.9093" />
                <PreSize X="0.0625" Y="0.1813" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DescriptionTxt" ActionTag="-993428760" Tag="267" IconVisible="False" LeftMargin="611.8275" RightMargin="463.1725" TopMargin="0.0044" BottomMargin="130.9956" FontSize="25" LabelText="自摸，夹胡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="125.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="611.8275" Y="145.4956" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5099" Y="0.9093" />
                <PreSize X="0.1042" Y="0.1813" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreTxt" ActionTag="-1667517996" Tag="269" IconVisible="False" LeftMargin="1053.4440" RightMargin="126.5560" TopMargin="-5.4956" BottomMargin="125.4956" FontSize="35" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1053.4440" Y="145.4956" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.8779" Y="0.9093" />
                <PreSize X="0.0167" Y="0.2500" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardLayout" ActionTag="-1129714759" Tag="293" IconVisible="False" LeftMargin="145.0000" RightMargin="-45.0000" TopMargin="43.0000" BottomMargin="7.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1100.0000" Y="110.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="145.0000" Y="62.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1208" Y="0.3875" />
                <PreSize X="0.9167" Y="0.6875" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="GangTxt" ActionTag="-832675318" Tag="52" IconVisible="False" LeftMargin="1145.3263" RightMargin="9.6737" TopMargin="-5.4956" BottomMargin="125.4956" FontSize="35" LabelText="杠:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="45.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1145.3263" Y="145.4956" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.9544" Y="0.9093" />
                <PreSize X="0.0375" Y="0.2500" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" Y="472.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6293" />
            <PreSize X="0.8996" Y="0.2133" />
            <SingleColor A="255" R="255" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="-232417654" Tag="270" IconVisible="False" LeftMargin="67.0000" RightMargin="67.0000" TopMargin="306.0232" BottomMargin="343.9768" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="HeadIcon" ActionTag="154072836" Tag="271" IconVisible="False" LeftMargin="25.5915" RightMargin="1078.4084" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.5915" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0613" Y="0.5000" />
                <PreSize X="0.0800" Y="0.9600" />
                <FileData Type="Normal" Path="Avatar46.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadKuang" ActionTag="-125301622" Tag="272" IconVisible="False" LeftMargin="25.8098" RightMargin="1078.1902" TopMargin="1.8964" BottomMargin="2.1036" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.8098" Y="50.1036" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0615" Y="0.5010" />
                <PreSize X="0.0800" Y="0.9600" />
                <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameIDTxt" ActionTag="965136993" Tag="273" IconVisible="False" LeftMargin="142.8544" RightMargin="973.1456" TopMargin="3.0067" BottomMargin="67.9933" FontSize="25" LabelText="168322" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="142.8544" Y="82.4933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1190" Y="0.8249" />
                <PreSize X="0.0700" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickNameTxt" ActionTag="158089997" Tag="274" IconVisible="False" LeftMargin="301.6821" RightMargin="823.3179" TopMargin="3.0067" BottomMargin="67.9933" FontSize="25" LabelText="胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="301.6821" Y="82.4933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2514" Y="0.8249" />
                <PreSize X="0.0625" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DescriptionTxt" ActionTag="-4993894" Tag="275" IconVisible="False" LeftMargin="541.8373" RightMargin="533.1627" TopMargin="3.0130" BottomMargin="67.9870" FontSize="25" LabelText="自摸，夹胡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="125.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="541.8373" Y="82.4870" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4515" Y="0.8249" />
                <PreSize X="0.1042" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreTxt" ActionTag="-897724953" Tag="276" IconVisible="False" LeftMargin="904.4471" RightMargin="275.5529" TopMargin="9.0001" BottomMargin="50.9999" FontSize="35" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="904.4471" Y="70.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.7537" Y="0.7100" />
                <PreSize X="0.0167" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardLayout" ActionTag="822520410" Tag="294" IconVisible="False" LeftMargin="145.0000" RightMargin="355.0000" TopMargin="39.5000" BottomMargin="0.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="700.0000" Y="60.0000" />
                <AnchorPoint />
                <Position X="145.0000" Y="0.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1208" Y="0.0050" />
                <PreSize X="0.5833" Y="0.6000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="GangTxt" ActionTag="-2038555825" Tag="49" IconVisible="False" LeftMargin="904.4500" RightMargin="250.5500" TopMargin="48.9999" BottomMargin="11.0001" FontSize="35" LabelText="杠:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="45.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="904.4500" Y="31.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.7537" Y="0.3100" />
                <PreSize X="0.0375" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" Y="343.9768" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4586" />
            <PreSize X="0.8996" Y="0.1333" />
            <SingleColor A="255" R="255" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3" ActionTag="542028824" Tag="277" IconVisible="False" LeftMargin="67.0000" RightMargin="67.0000" TopMargin="410.7364" BottomMargin="239.2636" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="HeadIcon" ActionTag="1631395129" Tag="278" IconVisible="False" LeftMargin="25.5915" RightMargin="1078.4084" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.5915" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0613" Y="0.5000" />
                <PreSize X="0.0800" Y="0.9600" />
                <FileData Type="Normal" Path="Avatar46.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadKuang" ActionTag="725727418" Tag="279" IconVisible="False" LeftMargin="25.8098" RightMargin="1078.1902" TopMargin="1.8964" BottomMargin="2.1036" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.8098" Y="50.1036" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0615" Y="0.5010" />
                <PreSize X="0.0800" Y="0.9600" />
                <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameIDTxt" ActionTag="605167298" Tag="280" IconVisible="False" LeftMargin="142.8544" RightMargin="973.1456" TopMargin="3.0067" BottomMargin="67.9933" FontSize="25" LabelText="168322" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="142.8544" Y="82.4933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1190" Y="0.8249" />
                <PreSize X="0.0700" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickNameTxt" ActionTag="1022307161" Tag="281" IconVisible="False" LeftMargin="301.6821" RightMargin="823.3179" TopMargin="3.0067" BottomMargin="67.9933" FontSize="25" LabelText="胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="301.6821" Y="82.4933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2514" Y="0.8249" />
                <PreSize X="0.0625" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DescriptionTxt" ActionTag="-1317703444" Tag="282" IconVisible="False" LeftMargin="541.8373" RightMargin="533.1627" TopMargin="3.0131" BottomMargin="67.9869" FontSize="25" LabelText="自摸，夹胡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="125.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="541.8373" Y="82.4869" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4515" Y="0.8249" />
                <PreSize X="0.1042" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreTxt" ActionTag="368507166" Tag="283" IconVisible="False" LeftMargin="904.4476" RightMargin="275.5524" TopMargin="9.0000" BottomMargin="51.0000" FontSize="35" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="904.4476" Y="71.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.7537" Y="0.7100" />
                <PreSize X="0.0167" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardLayout" ActionTag="-1725997253" Tag="295" IconVisible="False" LeftMargin="145.0000" RightMargin="355.0000" TopMargin="39.5000" BottomMargin="0.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="700.0000" Y="60.0000" />
                <AnchorPoint />
                <Position X="145.0000" Y="0.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1208" Y="0.0050" />
                <PreSize X="0.5833" Y="0.6000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="GangTxt" ActionTag="839768495" Tag="50" IconVisible="False" LeftMargin="904.4500" RightMargin="250.5500" TopMargin="49.0000" BottomMargin="11.0000" FontSize="35" LabelText="杠:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="45.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="904.4500" Y="31.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.7537" Y="0.3100" />
                <PreSize X="0.0375" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" Y="239.2636" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3190" />
            <PreSize X="0.8996" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_4" ActionTag="1664854715" Tag="284" IconVisible="False" LeftMargin="67.0000" RightMargin="67.0000" TopMargin="515.4496" BottomMargin="134.5504" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="HeadIcon" ActionTag="-317350306" Tag="285" IconVisible="False" LeftMargin="25.5915" RightMargin="1078.4084" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.5915" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0613" Y="0.5000" />
                <PreSize X="0.0800" Y="0.9600" />
                <FileData Type="Normal" Path="Avatar46.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadKuang" ActionTag="-61166992" Tag="286" IconVisible="False" LeftMargin="25.8098" RightMargin="1078.1902" TopMargin="1.8964" BottomMargin="2.1036" ctype="SpriteObjectData">
                <Size X="96.0000" Y="96.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.8098" Y="50.1036" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0615" Y="0.5010" />
                <PreSize X="0.0800" Y="0.9600" />
                <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameIDTxt" ActionTag="-1534183940" Tag="287" IconVisible="False" LeftMargin="142.8544" RightMargin="973.1456" TopMargin="3.0067" BottomMargin="67.9933" FontSize="25" LabelText="168322" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="142.8544" Y="82.4933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1190" Y="0.8249" />
                <PreSize X="0.0700" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickNameTxt" ActionTag="1490290950" Tag="288" IconVisible="False" LeftMargin="301.6821" RightMargin="823.3179" TopMargin="3.0067" BottomMargin="67.9933" FontSize="25" LabelText="胡汉三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="301.6821" Y="82.4933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2514" Y="0.8249" />
                <PreSize X="0.0625" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DescriptionTxt" ActionTag="2064562488" Tag="289" IconVisible="False" LeftMargin="541.8373" RightMargin="533.1627" TopMargin="3.0130" BottomMargin="67.9870" FontSize="25" LabelText="自摸，夹胡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="125.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="541.8373" Y="82.4870" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4515" Y="0.8249" />
                <PreSize X="0.1042" Y="0.2900" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreTxt" ActionTag="-34559785" Tag="290" IconVisible="False" LeftMargin="904.4462" RightMargin="275.5538" TopMargin="9.0001" BottomMargin="50.9999" FontSize="35" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="904.4462" Y="70.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.7537" Y="0.7100" />
                <PreSize X="0.0167" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardLayout" ActionTag="75178528" Tag="296" IconVisible="False" LeftMargin="145.0000" RightMargin="355.0000" TopMargin="39.5000" BottomMargin="0.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="700.0000" Y="60.0000" />
                <AnchorPoint />
                <Position X="145.0000" Y="0.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1208" Y="0.0050" />
                <PreSize X="0.5833" Y="0.6000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="GangTxt" ActionTag="-1862962419" Tag="51" IconVisible="False" LeftMargin="904.4489" RightMargin="250.5511" TopMargin="49.0000" BottomMargin="11.0000" FontSize="35" LabelText="杠:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="45.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="904.4489" Y="31.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="224" G="197" B="52" />
                <PrePosition X="0.7537" Y="0.3100" />
                <PreSize X="0.0375" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" Y="134.5504" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1794" />
            <PreSize X="0.8996" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Role" ActionTag="-1409711839" VisibleForFrame="False" Tag="291" IconVisible="False" LeftMargin="1166.2566" RightMargin="60.7434" TopMargin="330.6693" BottomMargin="271.3307" ctype="SpriteObjectData">
            <Size X="107.0000" Y="148.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1219.7566" Y="345.3307" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9144" Y="0.4604" />
            <PreSize X="0.0802" Y="0.1973" />
            <FileData Type="Normal" Path="gameResult/bao_hun_girl_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_16" ActionTag="-1444165048" VisibleForFrame="False" Tag="292" IconVisible="False" LeftMargin="1166.9335" RightMargin="79.0665" TopMargin="459.6104" BottomMargin="175.3896" ctype="SpriteObjectData">
            <Size X="88.0000" Y="115.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1210.9335" Y="232.8896" />
            <Scale ScaleX="2.0000" ScaleY="2.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9077" Y="0.3105" />
            <PreSize X="0.0660" Y="0.1533" />
            <FileData Type="Normal" Path="gameResult/bao_hun_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_dis_huipai_bg" ActionTag="-1594738000" VisibleForFrame="False" Tag="128" IconVisible="False" LeftMargin="1188.2817" RightMargin="99.7183" TopMargin="516.8702" BottomMargin="187.1298" ctype="SpriteObjectData">
            <Size X="86.0000" Y="130.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1211.2817" Y="210.1298" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9080" Y="0.2802" />
            <PreSize X="0.0345" Y="0.0613" />
            <FileData Type="Normal" Path="game/mahjongCard_2/pai_bg_zheng.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_dis_huipai" ActionTag="1379325041" VisibleForFrame="False" Tag="165" IconVisible="False" LeftMargin="1188.2808" RightMargin="99.7192" TopMargin="531.5206" BottomMargin="172.4794" ctype="SpriteObjectData">
            <Size X="86.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1211.2808" Y="195.4794" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9080" Y="0.2606" />
            <PreSize X="0.0345" Y="0.0613" />
            <FileData Type="Normal" Path="game/mahjongCard_2/card_color_2_num_9.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="hun_zi_1" ActionTag="1900653740" VisibleForFrame="False" Tag="48" IconVisible="False" LeftMargin="1193.5530" RightMargin="103.4470" TopMargin="436.3482" BottomMargin="294.6518" ctype="SpriteObjectData">
            <Size X="37.0000" Y="19.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1212.0530" Y="304.1518" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9086" Y="0.4055" />
            <PreSize X="0.0277" Y="0.0253" />
            <FileData Type="Normal" Path="gameResult/hun_zi.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>