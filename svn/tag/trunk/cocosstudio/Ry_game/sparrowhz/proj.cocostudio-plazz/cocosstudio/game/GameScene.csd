<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="7e1f33ff-c024-4f0c-ba6c-680f612e7b5f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="165463859" Tag="71" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="440" RightEage="440" TopEage="247" BottomEage="247" Scale9OriginX="440" Scale9OriginY="247" Scale9Width="454" Scale9Height="256" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="game/background_sparrowHz_0.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="InfoLayout" ActionTag="-633510819" VisibleForFrame="False" Tag="340" IconVisible="False" RightMargin="1134.0000" BottomMargin="650.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Text_33" ActionTag="-1363361498" VisibleForFrame="False" Tag="341" IconVisible="False" LeftMargin="10.0000" RightMargin="57.0000" TopMargin="6.5000" BottomMargin="70.5000" FontSize="20" LabelText="房间号:123432" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="133.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.0000" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.8200" />
                <PreSize X="0.6650" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_33_0" ActionTag="545324910" Tag="342" IconVisible="False" LeftMargin="10.0000" RightMargin="127.0000" TopMargin="6.5000" BottomMargin="70.5000" FontSize="20" LabelText="剩16圈" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.0000" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.8200" />
                <PreSize X="0.3150" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="750.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="0.1499" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_chat" ActionTag="-1085812233" Tag="41" IconVisible="False" LeftMargin="1259.6279" RightMargin="16.3721" TopMargin="93.3665" BottomMargin="598.6335" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="58.0000" Y="58.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1288.6279" Y="627.6335" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9660" Y="0.8368" />
            <PreSize X="0.0435" Y="0.0773" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="btn_chat_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="btn_chat_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="btn_chat_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimeText" ActionTag="1793154896" Tag="111" IconVisible="False" LeftMargin="1171.1716" RightMargin="88.8284" TopMargin="42.9637" BottomMargin="670.0363" FontSize="30" LabelText="18:36" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="74.0000" Y="37.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1245.1716" Y="688.5363" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9334" Y="0.9180" />
            <PreSize X="0.0555" Y="0.0493" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_switch" ActionTag="2018638277" Tag="2" IconVisible="False" LeftMargin="1259.6279" RightMargin="16.3721" TopMargin="11.3354" BottomMargin="673.6646" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="43" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="58.0000" Y="65.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1288.6279" Y="706.1646" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9660" Y="0.9416" />
            <PreSize X="0.0435" Y="0.0867" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="btn_set_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="btn_set_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="btn_set_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_start" ActionTag="90896694" VisibleForFrame="False" Tag="3" IconVisible="False" LeftMargin="582.4951" RightMargin="582.5049" TopMargin="199.4052" BottomMargin="469.5948" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="169.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5314" ScaleY="0.5517" />
            <Position X="672.3017" Y="514.2825" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5040" Y="0.6857" />
            <PreSize X="0.1267" Y="0.1080" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="game_end_btn_start_game_press.png" Plist="game/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="game_end_btn_start_game_press.png" Plist="game/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="game_end_btn_start_game.png" Plist="game/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_voice" ActionTag="147989285" Tag="5" IconVisible="False" LeftMargin="1259.6279" RightMargin="16.3721" TopMargin="531.3533" BottomMargin="160.6467" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="58.0000" Y="58.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1288.6279" Y="189.6467" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9660" Y="0.2529" />
            <PreSize X="0.0435" Y="0.0773" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="btn_yuyin_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="btn_yuyin_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="btn_yuyin_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_clock" ActionTag="-4052651" Tag="9" IconVisible="False" LeftMargin="612.5000" RightMargin="608.5000" TopMargin="290.5000" BottomMargin="346.5000" ctype="SpriteObjectData">
            <Size X="113.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="Dong" ActionTag="-1198503849" VisibleForFrame="False" Tag="101" IconVisible="False" ctype="SpriteObjectData">
                <Size X="113.0000" Y="113.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.5000" Y="56.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="direction/dong.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Nan" ActionTag="-1357584307" VisibleForFrame="False" Tag="102" IconVisible="False" ctype="SpriteObjectData">
                <Size X="113.0000" Y="113.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.5000" Y="56.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="direction/nan.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Xi" ActionTag="-426494010" VisibleForFrame="False" Tag="103" IconVisible="False" ctype="SpriteObjectData">
                <Size X="113.0000" Y="113.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.5000" Y="56.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="direction/xi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Bei" ActionTag="-582795969" VisibleForFrame="False" Tag="104" IconVisible="False" ctype="SpriteObjectData">
                <Size X="113.0000" Y="113.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.5000" Y="56.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="direction/bei.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="669.0000" Y="403.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5015" Y="0.5373" />
            <PreSize X="0.0847" Y="0.1507" />
            <FileData Type="Normal" Path="direction/fengwei.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="AsLab_time" ActionTag="1664108806" Tag="1" IconVisible="False" LeftMargin="641.0001" RightMargin="650.9999" TopMargin="331.2308" BottomMargin="387.7692" CharWidth="21" CharHeight="31" LabelText="19" StartChar="0" ctype="TextAtlasObjectData">
            <Size X="42.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="662.0001" Y="403.2692" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4963" Y="0.5377" />
            <PreSize X="0.0315" Y="0.0413" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="game/num_clock.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_listenBg" ActionTag="337011487" Tag="10" IconVisible="False" LeftMargin="41.5389" RightMargin="964.4611" TopMargin="642.9748" BottomMargin="2.0252" ctype="SpriteObjectData">
            <Size X="328.0000" Y="105.0000" />
            <AnchorPoint />
            <Position X="41.5389" Y="2.0252" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0311" Y="0.0027" />
            <PreSize X="0.2459" Y="0.1400" />
            <FileData Type="Normal" Path="gamesceneplist/sp_listenBg_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3_mask" ActionTag="-1245017551" Alpha="90" Tag="106" IconVisible="False" LeftMargin="36.1575" RightMargin="1097.8425" TopMargin="509.0000" BottomMargin="145.0000" Scale9Enable="True" LeftEage="55" RightEage="47" TopEage="49" BottomEage="47" Scale9OriginX="55" Scale9OriginY="49" Scale9Width="7" Scale9Height="7" ctype="ImageViewObjectData">
            <Size X="200.0000" Y="96.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="36.1575" Y="193.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.0271" Y="0.2573" />
            <PreSize X="0.1499" Y="0.1280" />
            <FileData Type="Normal" Path="mask.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2_mask" ActionTag="-1716215364" Alpha="90" Tag="107" IconVisible="False" LeftMargin="38.0000" RightMargin="1200.0000" TopMargin="264.3661" BottomMargin="335.6339" Scale9Enable="True" LeftEage="55" RightEage="47" TopEage="49" BottomEage="47" Scale9OriginX="55" Scale9OriginY="49" Scale9Width="7" Scale9Height="7" ctype="ImageViewObjectData">
            <Size X="96.0000" Y="150.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="86.0000" Y="485.6339" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.0645" Y="0.6475" />
            <PreSize X="0.0720" Y="0.2000" />
            <FileData Type="Normal" Path="mask.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1_mask" ActionTag="-1159186645" Alpha="90" Tag="109" IconVisible="False" LeftMargin="290.0000" RightMargin="844.0000" TopMargin="26.0000" BottomMargin="628.0000" Scale9Enable="True" LeftEage="55" RightEage="47" TopEage="49" BottomEage="47" Scale9OriginX="55" Scale9OriginY="49" Scale9Width="7" Scale9Height="7" ctype="ImageViewObjectData">
            <Size X="200.0000" Y="96.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="290.0000" Y="676.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.2174" Y="0.9013" />
            <PreSize X="0.1499" Y="0.1280" />
            <FileData Type="Normal" Path="mask.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1_mask_1" ActionTag="1475278854" Alpha="90" Tag="108" IconVisible="False" LeftMargin="1200.7878" RightMargin="37.2122" TopMargin="264.9802" BottomMargin="335.0198" Scale9Enable="True" LeftEage="55" RightEage="47" TopEage="49" BottomEage="47" Scale9OriginX="55" Scale9OriginY="49" Scale9Width="7" Scale9Height="7" ctype="ImageViewObjectData">
            <Size X="96.0000" Y="150.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="1248.7878" Y="485.0198" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="26" G="26" B="26" />
            <PrePosition X="0.9361" Y="0.6467" />
            <PreSize X="0.0720" Y="0.2000" />
            <FileData Type="Normal" Path="mask.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="1072808184" Tag="11" IconVisible="True" LeftMargin="338.2484" RightMargin="995.7516" TopMargin="86.1948" BottomMargin="663.8052" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="338.2484" Y="663.8052" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2536" Y="0.8851" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="-651639449" Tag="12" IconVisible="True" LeftMargin="85.4838" RightMargin="1248.5162" TopMargin="324.2934" BottomMargin="425.7066" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="85.4838" Y="425.7066" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0641" Y="0.5676" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3" ActionTag="-66220346" Tag="13" IconVisible="True" LeftMargin="85.4838" RightMargin="1248.5162" TopMargin="567.1043" BottomMargin="182.8957" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="85.4838" Y="182.8957" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0641" Y="0.2439" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_4" ActionTag="-783811294" Tag="14" IconVisible="True" LeftMargin="1249.0000" RightMargin="85.0000" TopMargin="324.2934" BottomMargin="425.7066" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1249.0000" Y="425.7066" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9363" Y="0.5676" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_sparrowPlate" ActionTag="1285861256" Tag="19" IconVisible="False" LeftMargin="294.0666" RightMargin="916.9334" TopMargin="177.5784" BottomMargin="424.4216" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="123.0000" Y="148.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="355.5666" Y="498.4216" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2665" Y="0.6646" />
            <PreSize X="0.0922" Y="0.1973" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_25" ActionTag="1865648155" Tag="344" IconVisible="False" LeftMargin="80.0000" RightMargin="1194.0000" TopMargin="5.0000" BottomMargin="715.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="60.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_33" ActionTag="-738749475" Tag="840" IconVisible="False" LeftMargin="3.0000" RightMargin="37.0000" TopMargin="-1.0000" BottomMargin="-1.0000" ctype="SpriteObjectData">
                <Size X="20.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="13.0000" Y="15.0000" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2167" Y="0.5000" />
                <PreSize X="0.3333" Y="1.0667" />
                <FileData Type="Normal" Path="game/sheyuIcon.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="AtlasLabel_5" ActionTag="1527139439" Tag="839" IconVisible="False" LeftMargin="28.0000" RightMargin="3.0000" TopMargin="3.0000" BottomMargin="1.0000" FontSize="20" LabelText="112" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.0000" Y="14.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="235" G="236" B="21" />
                <PrePosition X="0.4667" Y="0.4667" />
                <PreSize X="0.4833" Y="0.8667" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="80.0000" Y="730.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0600" Y="0.9733" />
            <PreSize X="0.0450" Y="0.0400" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_laizicard_bg" ActionTag="1841668932" Tag="668999" IconVisible="False" LeftMargin="35.1180" RightMargin="1210.8820" TopMargin="65.0469" BottomMargin="548.9531" ctype="SpriteObjectData">
            <Size X="88.0000" Y="136.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_1" ActionTag="1214058931" Tag="227" IconVisible="False" LeftMargin="-13.3666" RightMargin="-12.6334" TopMargin="-12.0000" BottomMargin="-11.0000" ctype="SpriteObjectData">
                <Size X="114.0000" Y="159.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="43.6334" Y="68.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4958" Y="0.5037" />
                <PreSize X="1.2955" Y="1.1691" />
                <FileData Type="Normal" Path="game/pizipbgbig.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="79.1180" Y="616.9531" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0593" Y="0.8226" />
            <PreSize X="0.0660" Y="0.1813" />
            <FileData Type="Normal" Path="game/font_big/card_up.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_laizicard" ActionTag="-444506854" Tag="668" RotationSkewX="-0.7513" RotationSkewY="-0.7519" IconVisible="False" LeftMargin="40.1764" RightMargin="1215.8236" TopMargin="98.4700" BottomMargin="560.5300" ctype="SpriteObjectData">
            <Size X="78.0000" Y="91.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="79.1764" Y="606.0300" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0594" Y="0.8080" />
            <PreSize X="0.0585" Y="0.1213" />
            <FileData Type="Normal" Path="game/font_big/font_1_9.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="SpFlag" ActionTag="-1920171868" VisibleForFrame="False" Tag="1270" IconVisible="False" LeftMargin="35.1365" RightMargin="1210.8635" TopMargin="64.0000" BottomMargin="550.0000" ctype="SpriteObjectData">
            <Size X="88.0000" Y="136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="79.1365" Y="618.0000" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0593" Y="0.8240" />
            <PreSize X="0.0660" Y="0.1813" />
            <FileData Type="PlistSubImage" Path="huipaiflag.png" Plist="plist/pics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="RuleTxt" ActionTag="702173736" Tag="99929" IconVisible="False" LeftMargin="666.9994" RightMargin="667.0006" TopMargin="293.1887" BottomMargin="456.8113" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="666.9994" Y="456.8113" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6091" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="DianChiLayout" ActionTag="141390988" Tag="153" IconVisible="False" LeftMargin="1184.0000" RightMargin="100.0000" TopMargin="16.7587" BottomMargin="713.2413" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="50.0000" Y="20.0000" />
            <Children>
              <AbstractNodeData Name="ValueLayout" ActionTag="-1538140435" Tag="154" IconVisible="False" LeftMargin="1.0000" RightMargin="5.0000" TopMargin="1.0000" BottomMargin="1.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="7" Scale9Height="1" ctype="PanelObjectData">
                <Size X="44.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1.0000" Y="10.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0200" Y="0.5000" />
                <PreSize X="0.8800" Y="0.9000" />
                <FileData Type="Normal" Path="game/pj_dianliang.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpDianLiangBg" ActionTag="787194021" Tag="155" IconVisible="False" TopMargin="-0.5000" BottomMargin="-0.5000" ctype="SpriteObjectData">
                <Size X="50.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="25.0000" Y="10.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0500" />
                <FileData Type="Normal" Path="game/pj_dichi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1184.0000" Y="723.2413" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8876" Y="0.9643" />
            <PreSize X="0.0375" Y="0.0267" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_sparrowHuPlate" ActionTag="-240417479" Tag="1457" IconVisible="False" LeftMargin="304.0666" RightMargin="906.9334" TopMargin="187.5784" BottomMargin="414.4216" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="123.0000" Y="148.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="365.5666" Y="488.4216" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2740" Y="0.6512" />
            <PreSize X="0.0922" Y="0.1973" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnRule" ActionTag="1596553981" Tag="229" IconVisible="False" LeftMargin="1255.1279" RightMargin="11.8721" TopMargin="446.5688" BottomMargin="240.4312" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="37" Scale9Height="41" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="67.0000" Y="63.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1288.6279" Y="271.9312" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9660" Y="0.3626" />
            <PreSize X="0.0502" Y="0.0840" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="btn_rule_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="btn_rule_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="btn_rule_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="RuleDisplayLayout" ActionTag="791336001" VisibleForFrame="False" Tag="230" IconVisible="False" LeftMargin="1086.0000" RightMargin="108.0000" TopMargin="394.7715" BottomMargin="105.2285" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="57" RightEage="57" TopEage="27" BottomEage="27" Scale9OriginX="57" Scale9OriginY="27" Scale9Width="52" Scale9Height="20" ctype="PanelObjectData">
            <Size X="140.0000" Y="250.0000" />
            <Children>
              <AbstractNodeData Name="RuleTxt" ActionTag="1250354957" Tag="233" IconVisible="False" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0001" BottomMargin="4.9999" IsCustomSize="True" FontSize="25" LabelText="四清&#xA;会牌&#xA;清一色&#xA;穷胡加倍&#xA;封顶200&#xA;七对&#xA;天地胡" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="240.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="70.0000" Y="124.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.9286" Y="0.9600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="1086.0000" Y="355.2285" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8141" Y="0.4736" />
            <PreSize X="0.1049" Y="0.3333" />
            <FileData Type="PlistSubImage" Path="game_image_select_gang_back.png" Plist="game/plazaScene.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>