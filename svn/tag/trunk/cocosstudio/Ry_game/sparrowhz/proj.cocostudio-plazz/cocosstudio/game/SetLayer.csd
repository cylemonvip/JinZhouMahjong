<GameFile>
  <PropertyGroup Name="SetLayer" Type="Scene" ID="c88d9739-a13d-49cb-b8dd-cc80af33e71c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="60" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="sp_setLayer_bg" ActionTag="-1295503429" Tag="93" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="367.0000" RightMargin="367.0000" TopMargin="105.4482" BottomMargin="114.5518" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="174" RightEage="126" TopEage="91" BottomEage="66" Scale9OriginX="174" Scale9OriginY="91" Scale9Width="134" Scale9Height="89" ctype="PanelObjectData">
            <Size X="600.0000" Y="530.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" Y="114.5518" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1527" />
            <PreSize X="0.4498" Y="0.7067" />
            <FileData Type="Normal" Path="KFMJ_grxx_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="1633784408" Tag="100" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="527.5000" RightMargin="527.5000" TopMargin="84.4998" BottomMargin="608.5002" ctype="SpriteObjectData">
            <Size X="279.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="637.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8493" />
            <PreSize X="0.2091" Y="0.0760" />
            <FileData Type="Normal" Path="settile.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_close" ActionTag="-698393417" Tag="126" IconVisible="False" LeftMargin="916.1537" RightMargin="349.8463" TopMargin="87.7827" BottomMargin="594.2173" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="68.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="950.1537" Y="628.2173" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7123" Y="0.8376" />
            <PreSize X="0.0510" Y="0.0907" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="game/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="game/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="plaza_btn_close_normal.png" Plist="game/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="EffectSlider" ActionTag="-814203041" Tag="88" IconVisible="False" LeftMargin="505.4031" RightMargin="510.5969" TopMargin="218.8709" BottomMargin="513.1291" TouchEnable="True" PercentInfo="52" ctype="SliderObjectData">
            <Size X="318.0000" Y="18.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="664.4031" Y="522.1291" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4981" Y="0.6962" />
            <PreSize X="0.2384" Y="0.0240" />
            <BackGroundData Type="PlistSubImage" Path="setting_slide_bar_bg.png" Plist="game/setting.plist" />
            <ProgressBarData Type="PlistSubImage" Path="setting_slide_bar.png" Plist="game/setting.plist" />
            <BallNormalData Type="PlistSubImage" Path="setting_btn_slide_normal.png" Plist="game/setting.plist" />
            <BallPressedData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
            <BallDisabledData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="BgmSlider" ActionTag="-349379924" Tag="89" IconVisible="False" LeftMargin="505.4018" RightMargin="510.5982" TopMargin="296.9007" BottomMargin="435.0993" TouchEnable="True" PercentInfo="52" ctype="SliderObjectData">
            <Size X="318.0000" Y="18.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="664.4018" Y="444.0993" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4981" Y="0.5921" />
            <PreSize X="0.2384" Y="0.0240" />
            <BackGroundData Type="PlistSubImage" Path="setting_slide_bar_bg.png" Plist="game/setting.plist" />
            <ProgressBarData Type="PlistSubImage" Path="setting_slide_bar.png" Plist="game/setting.plist" />
            <BallNormalData Type="PlistSubImage" Path="setting_btn_slide_normal.png" Plist="game/setting.plist" />
            <BallPressedData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
            <BallDisabledData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Spfont" ActionTag="1387626291" Tag="90" IconVisible="False" LeftMargin="508.3436" RightMargin="720.6564" TopMargin="191.6763" BottomMargin="460.3237" ctype="SpriteObjectData">
            <Size X="105.0000" Y="98.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="560.8436" Y="509.3237" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4204" Y="0.6791" />
            <PreSize X="0.0787" Y="0.1307" />
            <FileData Type="PlistSubImage" Path="setting_image_front.png" Plist="game/setting.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnEffectMax" ActionTag="125717211" Tag="93" IconVisible="False" LeftMargin="854.0637" RightMargin="435.9363" TopMargin="206.1431" BottomMargin="500.8569" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="876.0637" Y="522.3569" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6567" Y="0.6965" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_effect_open_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_effect_open_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_effect_open_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnEffectMin" ActionTag="2089070453" Tag="94" IconVisible="False" LeftMargin="442.1110" RightMargin="847.8890" TopMargin="206.1431" BottomMargin="500.8569" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="464.1110" Y="522.3569" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3479" Y="0.6965" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_effect_close_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_effect_close_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_effect_close_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnBgmMax" ActionTag="172132052" Tag="95" IconVisible="False" LeftMargin="854.0637" RightMargin="435.9363" TopMargin="283.9492" BottomMargin="423.0508" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="876.0637" Y="444.5508" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6567" Y="0.5927" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_music_open_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_music_open_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_music_open_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnBgmMin" ActionTag="-536913857" Tag="96" IconVisible="False" LeftMargin="442.1110" RightMargin="847.8890" TopMargin="283.9492" BottomMargin="423.0508" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="464.1110" Y="444.5508" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3479" Y="0.5927" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_music_close_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_music_close_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_music_close_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnExit" ActionTag="1223485798" Tag="97" IconVisible="False" LeftMargin="713.2390" RightMargin="466.7610" TopMargin="519.1976" BottomMargin="168.8024" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="154.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="790.2390" Y="199.8024" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5924" Y="0.2664" />
            <PreSize X="0.1154" Y="0.0827" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_exit_unused.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_exit_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_exit_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnDismiss" ActionTag="-312824909" Tag="98" IconVisible="False" LeftMargin="495.2563" RightMargin="684.7437" TopMargin="519.1976" BottomMargin="168.8024" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="154.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="572.2563" Y="199.8024" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4290" Y="0.2664" />
            <PreSize X="0.1154" Y="0.0827" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_dismiss_unused.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_dismiss_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_dismiss_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_1" ActionTag="211869730" Tag="86" IconVisible="False" LeftMargin="505.4019" RightMargin="721.5981" TopMargin="384.8143" BottomMargin="342.1857" ctype="SpriteObjectData">
            <Size X="107.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="558.9019" Y="353.6857" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4190" Y="0.4716" />
            <PreSize X="0.0802" Y="0.0307" />
            <FileData Type="Normal" Path="game/pzys1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_2" ActionTag="-509137317" Tag="87" IconVisible="False" LeftMargin="505.4019" RightMargin="719.5981" TopMargin="437.7886" BottomMargin="289.2114" ctype="SpriteObjectData">
            <Size X="109.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="559.9019" Y="300.7114" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4197" Y="0.4009" />
            <PreSize X="0.0817" Y="0.0307" />
            <FileData Type="Normal" Path="game/pzys2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_4" ActionTag="803290412" VisibleForFrame="False" Tag="89" IconVisible="False" LeftMargin="758.9221" RightMargin="528.0779" TopMargin="358.5836" BottomMargin="366.4164" ctype="SpriteObjectData">
            <Size X="47.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="782.4221" Y="378.9164" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5865" Y="0.5052" />
            <PreSize X="0.0352" Y="0.0333" />
            <FileData Type="Normal" Path="game/pzys3.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_5" ActionTag="1682487701" Tag="90" IconVisible="False" LeftMargin="673.7923" RightMargin="611.2077" TopMargin="433.7841" BottomMargin="289.2159" ctype="SpriteObjectData">
            <Size X="49.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="698.2923" Y="302.7159" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5235" Y="0.4036" />
            <PreSize X="0.0367" Y="0.0360" />
            <FileData Type="Normal" Path="game/pzys5.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_6" ActionTag="855496041" Tag="91" IconVisible="False" LeftMargin="773.4020" RightMargin="511.5980" TopMargin="435.7864" BottomMargin="289.2136" ctype="SpriteObjectData">
            <Size X="49.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="797.9020" Y="301.7136" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5981" Y="0.4023" />
            <PreSize X="0.0367" Y="0.0333" />
            <FileData Type="Normal" Path="game/pzys6.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_lan" ActionTag="1257697101" Tag="78" IconVisible="False" LeftMargin="673.7892" RightMargin="610.2108" TopMargin="379.5855" BottomMargin="341.4145" FontSize="25" LabelText="蓝色" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="698.7892" Y="355.9145" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="35" G="124" B="85" />
            <PrePosition X="0.5238" Y="0.4746" />
            <PreSize X="0.0375" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_lv" ActionTag="1473101350" Tag="79" IconVisible="False" LeftMargin="773.4019" RightMargin="510.5981" TopMargin="379.6743" BottomMargin="341.3257" FontSize="25" LabelText="绿色" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="798.4019" Y="355.8257" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="35" G="124" B="85" />
            <PrePosition X="0.5985" Y="0.4744" />
            <PreSize X="0.0375" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_7" ActionTag="700644396" VisibleForFrame="False" Tag="92" IconVisible="False" LeftMargin="659.7909" RightMargin="625.2091" TopMargin="363.8111" BottomMargin="363.1889" ctype="SpriteObjectData">
            <Size X="49.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="684.2909" Y="374.6889" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5130" Y="0.4996" />
            <PreSize X="0.0367" Y="0.0307" />
            <FileData Type="Normal" Path="game/pzys4.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpzys_0" ActionTag="-1886142782" Tag="94" IconVisible="False" LeftMargin="636.0802" RightMargin="662.9198" TopMargin="377.8111" BottomMargin="342.1889" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="653.5802" Y="357.1889" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4899" Y="0.4763" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpzys_1" ActionTag="238463523" Tag="95" IconVisible="False" LeftMargin="737.0481" RightMargin="561.9519" TopMargin="377.8110" BottomMargin="342.1890" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="754.5481" Y="357.1890" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5656" Y="0.4763" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpmys_0" ActionTag="325230100" Tag="96" IconVisible="False" LeftMargin="636.0802" RightMargin="662.9198" TopMargin="430.7864" BottomMargin="289.2136" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="653.5802" Y="304.2136" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4899" Y="0.4056" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpmys_1" ActionTag="150233852" Tag="97" IconVisible="False" LeftMargin="737.0481" RightMargin="561.9519" TopMargin="430.7864" BottomMargin="289.2136" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="754.5481" Y="304.2136" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5656" Y="0.4056" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>