<GameFile>
  <PropertyGroup Name="nodeUser" Type="Node" ID="75eb8fb9-730f-4fa8-a115-7ee9072a6227" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="131" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="sp_headCover" ActionTag="886046402" Tag="1" IconVisible="False" LeftMargin="-48.0000" RightMargin="-48.0000" TopMargin="-48.0000" BottomMargin="-48.0000" ctype="SpriteObjectData">
            <Size X="96.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_nickname" ActionTag="374684462" Tag="2" IconVisible="False" LeftMargin="-47.7111" RightMargin="-46.2889" TopMargin="36.2712" BottomMargin="-62.2712" FontSize="20" LabelText="NickName" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="94.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.7111" Y="-49.2712" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="216" G="115" B="16" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_gangfen" ActionTag="-1387166548" Tag="97" IconVisible="False" LeftMargin="818.5200" RightMargin="-925.5200" TopMargin="-39.8359" BottomMargin="-16.1641" ctype="SpriteObjectData">
            <Size X="107.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="872.0200" Y="11.8359" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="r_ganfen.png" Plist="plist/pics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_hufen" ActionTag="-1486527979" Tag="98" IconVisible="False" LeftMargin="895.3200" RightMargin="-1002.3200" TopMargin="-39.8358" BottomMargin="-16.1642" ctype="SpriteObjectData">
            <Size X="107.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="948.8200" Y="11.8358" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="r_hufen.png" Plist="plist/pics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_zongfen" ActionTag="-421254349" Tag="99" IconVisible="False" LeftMargin="972.5865" RightMargin="-1079.5865" TopMargin="-38.9489" BottomMargin="-17.0511" ctype="SpriteObjectData">
            <Size X="107.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.4705" ScaleY="0.4863" />
            <Position X="1022.9300" Y="10.1817" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="r_zongfen.png" Plist="plist/pics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gangfen" ActionTag="43322944" Tag="224" IconVisible="False" LeftMargin="841.5200" RightMargin="-902.5200" TopMargin="13.0593" BottomMargin="-44.0593" FontSize="25" LabelText="+999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="61.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="872.0200" Y="-28.5593" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_hufen" ActionTag="-2038585406" Tag="225" IconVisible="False" LeftMargin="918.0800" RightMargin="-979.0800" TopMargin="13.0593" BottomMargin="-44.0593" FontSize="25" LabelText="+999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="61.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="948.5800" Y="-28.5593" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_zongfen" ActionTag="1559718384" Tag="226" IconVisible="False" LeftMargin="994.6400" RightMargin="-1055.6400" TopMargin="13.0593" BottomMargin="-44.0593" FontSize="25" LabelText="-999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="61.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1025.1400" Y="-28.5593" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="199" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="SpBanker" ActionTag="129675738" VisibleForFrame="False" Tag="71" IconVisible="False" LeftMargin="-55.0979" RightMargin="16.0979" TopMargin="-50.6531" BottomMargin="9.6531" ctype="SpriteObjectData">
            <Size X="38.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-35.5979" Y="30.1531" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/zhuang_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Description" ActionTag="-24913190" Tag="66" IconVisible="False" LeftMargin="600.0000" RightMargin="-674.0000" TopMargin="-40.5000" BottomMargin="11.5000" FontSize="25" LabelText="跟庄*1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="74.0000" Y="29.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="600.0000" Y="26.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>