<GameFile>
  <PropertyGroup Name="ApplyDetailPopupNode" Type="Node" ID="535e663f-81d0-4711-a369-e8b737736174" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="118" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="733592497" Tag="126" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="3568578" Tag="119" IconVisible="False" LeftMargin="-250.0000" RightMargin="-250.0000" TopMargin="-150.0000" BottomMargin="-150.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="234" RightEage="234" TopEage="112" BottomEage="112" Scale9OriginX="234" Scale9OriginY="112" Scale9Width="242" Scale9Height="116" ctype="PanelObjectData">
            <Size X="500.0000" Y="300.0000" />
            <Children>
              <AbstractNodeData Name="HeadImg" ActionTag="913810377" Tag="121" IconVisible="False" LeftMargin="199.9999" RightMargin="200.0001" TopMargin="24.6219" BottomMargin="175.3781" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="249.9999" Y="225.3781" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7513" />
                <PreSize X="0.2000" Y="0.3333" />
                <FileData Type="PlistSubImage" Path="Avatar107.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="1301733236" Tag="122" IconVisible="False" LeftMargin="181.5001" RightMargin="181.4999" TopMargin="137.8577" BottomMargin="128.1423" FontSize="30" LabelText="昵称: 张三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="137.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="250.0001" Y="145.1423" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.5000" Y="0.4838" />
                <PreSize X="0.2740" Y="0.1133" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDTxt" ActionTag="-1513709833" Tag="123" IconVisible="False" LeftMargin="176.4998" RightMargin="176.5002" TopMargin="185.0934" BottomMargin="80.9066" FontSize="30" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="147.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="249.9998" Y="97.9066" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.5000" Y="0.3264" />
                <PreSize X="0.2940" Y="0.1133" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="AgreeBtn" ActionTag="1549568760" Tag="124" IconVisible="False" LeftMargin="-4.0000" RightMargin="260.0000" TopMargin="214.8703" BottomMargin="-2.8703" TouchEnable="True" FontSize="20" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="214" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="244.0000" Y="88.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="118.0000" Y="41.1297" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2360" Y="0.1371" />
                <PreSize X="0.4880" Y="0.2933" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/dismiss_tongyi_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/dismiss_tongyi_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/dismiss_tongyi.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RefuseBtn" ActionTag="-1145714907" Tag="125" IconVisible="False" LeftMargin="252.9998" RightMargin="3.0002" TopMargin="214.8702" BottomMargin="-2.8702" TouchEnable="True" FontSize="20" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="214" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="244.0000" Y="88.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="374.9998" Y="41.1298" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.1371" />
                <PreSize X="0.4880" Y="0.2933" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/dismiss_jujue_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/dismiss_jujue_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/dismiss_jujue.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="709501914" Tag="127" IconVisible="False" LeftMargin="455.9454" RightMargin="-23.9454" TopMargin="-23.7370" BottomMargin="255.7370" TouchEnable="True" FontSize="20" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="489.9454" Y="289.7370" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9799" Y="0.9658" />
                <PreSize X="0.1360" Y="0.2267" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <PressedFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <NormalFileData Type="PlistSubImage" Path="common_btn_close_normal.png" Plist="common.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DeleteBtn" ActionTag="395452445" Tag="82" IconVisible="False" LeftMargin="128.0000" RightMargin="128.0000" TopMargin="214.8700" BottomMargin="-2.8700" TouchEnable="True" FontSize="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="214" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="244.0000" Y="88.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="250.0000" Y="41.1300" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1371" />
                <PreSize X="0.4880" Y="0.2933" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/club_delete_member_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/club_delete_member_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/club_delete_member.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="club/btn_pic/winning_tx.png" Plist="" />
            <SingleColor A="255" R="144" G="238" B="144" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>