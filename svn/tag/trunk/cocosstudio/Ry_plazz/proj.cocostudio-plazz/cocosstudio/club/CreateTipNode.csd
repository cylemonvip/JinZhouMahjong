<GameFile>
  <PropertyGroup Name="CreateTipNode" Type="Node" ID="7cce2400-1e90-4025-a3b6-d1183ebd6549" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="70" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="2139689200" Tag="71" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="ImageBg" ActionTag="1211866652" Tag="72" IconVisible="False" LeftMargin="350.0000" RightMargin="350.0000" TopMargin="202.0000" BottomMargin="202.0000" LeftEage="143" RightEage="143" TopEage="81" BottomEage="81" Scale9OriginX="143" Scale9OriginY="81" Scale9Width="148" Scale9Height="84" ctype="ImageViewObjectData">
                <Size X="634.0000" Y="346.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4753" Y="0.4613" />
                <FileData Type="Normal" Path="KFMJ_grxx_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Tip" ActionTag="-18677582" Tag="74" IconVisible="False" LeftMargin="396.9958" RightMargin="397.0042" TopMargin="279.0013" BottomMargin="366.9987" FontSize="45" LabelText="创建俱乐部，详情请咨询：&#xA;jiujiumj001" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="540.0000" Y="104.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="666.9958" Y="418.9987" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.5000" Y="0.5587" />
                <PreSize X="0.4048" Y="0.1387" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CopyBtn" ActionTag="197308796" Tag="645" IconVisible="False" LeftMargin="570.2089" RightMargin="562.7911" TopMargin="440.5255" BottomMargin="228.4745" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="171" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="201.0000" Y="81.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-2065053737" Tag="656" IconVisible="False" LeftMargin="27.4245" RightMargin="33.5755" TopMargin="20.4067" BottomMargin="20.5933" FontSize="35" LabelText="一键复制" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="97.4245" Y="40.5933" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="77" G="77" B="77" />
                    <PrePosition X="0.4847" Y="0.5012" />
                    <PreSize X="0.6965" Y="0.4938" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="670.7089" Y="268.9745" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5028" Y="0.3586" />
                <PreSize X="0.1507" Y="0.1080" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/btn_yellow_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/btn_yellow_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/btn_yellow.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="1483711126" Tag="73" IconVisible="False" LeftMargin="934.4769" RightMargin="331.5231" TopMargin="185.6896" BottomMargin="496.3104" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="968.4769" Y="530.3104" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7260" Y="0.7071" />
                <PreSize X="0.0510" Y="0.0907" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <PressedFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <NormalFileData Type="PlistSubImage" Path="common_btn_close_normal.png" Plist="common.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>