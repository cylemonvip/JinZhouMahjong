<GameFile>
  <PropertyGroup Name="InputClubNumNode" Type="Node" ID="284f750d-462b-4a0b-90a8-c05129d5f148" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="75" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="128075336" Tag="78" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="DisplayImageBg" ActionTag="1167908347" Tag="101" IconVisible="False" LeftMargin="191.0000" RightMargin="191.0000" TopMargin="120.0001" BottomMargin="119.9999" LeftEage="414" RightEage="293" TopEage="168" BottomEage="168" Scale9OriginX="414" Scale9OriginY="168" Scale9Width="245" Scale9Height="174" ctype="ImageViewObjectData">
                <Size X="952.0000" Y="510.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="374.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7136" Y="0.6800" />
                <FileData Type="Normal" Path="club/join_pic/join_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_8" ActionTag="2053754677" Tag="156" IconVisible="False" LeftMargin="469.8858" RightMargin="476.1142" TopMargin="86.3142" BottomMargin="586.6858" ctype="SpriteObjectData">
                <Size X="388.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="663.8858" Y="625.1858" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4977" Y="0.8336" />
                <PreSize X="0.2909" Y="0.1027" />
                <FileData Type="Normal" Path="club/join_pic/join_title.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_1" ActionTag="564829929" Tag="142" IconVisible="False" LeftMargin="688.8514" RightMargin="226.1487" TopMargin="167.0592" BottomMargin="542.9408" LeftEage="138" RightEage="138" TopEage="13" BottomEage="13" Scale9OriginX="138" Scale9OriginY="13" Scale9Width="143" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="419.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="898.3514" Y="562.9408" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6734" Y="0.7506" />
                <PreSize X="0.3141" Y="0.0533" />
                <FileData Type="Normal" Path="club/join_pic/join_sub_title.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="-1919680283" Tag="93" IconVisible="False" LeftMargin="1094.7468" RightMargin="171.2532" TopMargin="96.1183" BottomMargin="585.8817" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1128.7468" Y="619.8817" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8461" Y="0.8265" />
                <PreSize X="0.0510" Y="0.0907" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <PressedFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
                <NormalFileData Type="PlistSubImage" Path="common_btn_close_normal.png" Plist="common.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="InputImageBg" ActionTag="-1413914276" Tag="143" IconVisible="False" LeftMargin="691.2778" RightMargin="227.7222" TopMargin="201.1110" BottomMargin="158.8890" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="415.0000" Y="390.0000" />
                <Children>
                  <AbstractNodeData Name="NumBg" ActionTag="-1619275270" Tag="144" IconVisible="False" LeftMargin="-43.7627" RightMargin="-41.2373" TopMargin="11.2718" BottomMargin="308.7282" ctype="SpriteObjectData">
                    <Size X="500.0000" Y="70.0000" />
                    <Children>
                      <AbstractNodeData Name="Line_1" ActionTag="-329679136" Tag="145" IconVisible="False" LeftMargin="48.6440" RightMargin="402.3560" TopMargin="56.2085" BottomMargin="7.7915" ctype="SpriteObjectData">
                        <Size X="49.0000" Y="6.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="73.1440" Y="10.7915" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1463" Y="0.1542" />
                        <PreSize X="0.0980" Y="0.0857" />
                        <FileData Type="Normal" Path="club/join_pic/join_temp_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Line_2" ActionTag="-2015779440" Tag="146" IconVisible="False" LeftMargin="119.8939" RightMargin="331.1061" TopMargin="56.2085" BottomMargin="7.7915" ctype="SpriteObjectData">
                        <Size X="49.0000" Y="6.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="144.3939" Y="10.7915" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2888" Y="0.1542" />
                        <PreSize X="0.0980" Y="0.0857" />
                        <FileData Type="Normal" Path="club/join_pic/join_temp_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Line_4" ActionTag="1744452708" Tag="147" IconVisible="False" LeftMargin="262.3940" RightMargin="188.6060" TopMargin="56.2085" BottomMargin="7.7915" ctype="SpriteObjectData">
                        <Size X="49.0000" Y="6.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="286.8940" Y="10.7915" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5738" Y="0.1542" />
                        <PreSize X="0.0980" Y="0.0857" />
                        <FileData Type="Normal" Path="club/join_pic/join_temp_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Line_3" ActionTag="1663662684" Tag="148" IconVisible="False" LeftMargin="191.1437" RightMargin="259.8563" TopMargin="56.2085" BottomMargin="7.7915" ctype="SpriteObjectData">
                        <Size X="49.0000" Y="6.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="215.6437" Y="10.7915" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4313" Y="0.1542" />
                        <PreSize X="0.0980" Y="0.0857" />
                        <FileData Type="Normal" Path="club/join_pic/join_temp_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Line_5" ActionTag="209617195" Tag="149" IconVisible="False" LeftMargin="333.6438" RightMargin="117.3562" TopMargin="56.2085" BottomMargin="7.7915" ctype="SpriteObjectData">
                        <Size X="49.0000" Y="6.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="358.1438" Y="10.7915" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7163" Y="0.1542" />
                        <PreSize X="0.0980" Y="0.0857" />
                        <FileData Type="Normal" Path="club/join_pic/join_temp_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Line_6" ActionTag="652913485" Tag="150" IconVisible="False" LeftMargin="404.8938" RightMargin="46.1062" TopMargin="56.2085" BottomMargin="7.7915" ctype="SpriteObjectData">
                        <Size X="49.0000" Y="6.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="429.3938" Y="10.7915" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8588" Y="0.1542" />
                        <PreSize X="0.0980" Y="0.0857" />
                        <FileData Type="Normal" Path="club/join_pic/join_temp_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="206.2373" Y="343.7282" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4970" Y="0.8814" />
                    <PreSize X="1.2048" Y="0.1795" />
                    <FileData Type="Normal" Path="club/join_pic/WeChatKWX_Join_num_Bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_1" ActionTag="-764611186" Tag="80" IconVisible="False" LeftMargin="-6.1536" RightMargin="267.1536" TopMargin="72.2354" BottomMargin="223.7646" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="70.8464" Y="270.7646" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1707" Y="0.6943" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_1_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_1_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_1.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_2" ActionTag="475796680" Tag="81" IconVisible="False" LeftMargin="133.4718" RightMargin="127.5282" TopMargin="72.2355" BottomMargin="223.7645" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="210.4718" Y="270.7645" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5072" Y="0.6943" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_2_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_2_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_2.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_3" ActionTag="1139119820" Tag="82" IconVisible="False" LeftMargin="273.0971" RightMargin="-12.0971" TopMargin="72.2354" BottomMargin="223.7646" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="350.0971" Y="270.7646" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8436" Y="0.6943" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_3_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_3_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_3.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_4" ActionTag="-1952893388" Tag="83" IconVisible="False" LeftMargin="-6.1536" RightMargin="267.1536" TopMargin="149.8944" BottomMargin="146.1056" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="70.8464" Y="193.1056" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1707" Y="0.4951" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_4_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_4_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_4.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_5" ActionTag="1039266963" Tag="84" IconVisible="False" LeftMargin="133.4718" RightMargin="127.5282" TopMargin="149.8947" BottomMargin="146.1053" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="210.4718" Y="193.1053" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5072" Y="0.4951" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_5_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_5_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_5.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_6" ActionTag="1630375047" Tag="85" IconVisible="False" LeftMargin="273.0971" RightMargin="-12.0971" TopMargin="149.8947" BottomMargin="146.1053" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="350.0971" Y="193.1053" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8436" Y="0.4951" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_6_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_6_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_6.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_7" ActionTag="-1310144170" Tag="86" IconVisible="False" LeftMargin="-6.1536" RightMargin="267.1536" TopMargin="227.5538" BottomMargin="68.4462" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="70.8464" Y="115.4462" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1707" Y="0.2960" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_7_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_7_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_7.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_8" ActionTag="-1995749140" Tag="87" IconVisible="False" LeftMargin="133.4718" RightMargin="127.5282" TopMargin="227.5539" BottomMargin="68.4461" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="210.4718" Y="115.4461" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5072" Y="0.2960" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_8_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_8_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_8.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_9" ActionTag="835221992" Tag="88" IconVisible="False" LeftMargin="273.0971" RightMargin="-12.0971" TopMargin="227.5539" BottomMargin="68.4461" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="350.0971" Y="115.4461" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8436" Y="0.2960" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_9_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_9_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_9.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_again" ActionTag="-584517675" Tag="89" IconVisible="False" LeftMargin="-6.1536" RightMargin="267.1536" TopMargin="305.2131" BottomMargin="-9.2131" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="70.8464" Y="37.7869" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1707" Y="0.0969" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_RestBtn1_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_RestBtn1_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_RestBtn1.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_0" ActionTag="-532051923" Tag="90" IconVisible="False" LeftMargin="133.4718" RightMargin="127.5282" TopMargin="305.2132" BottomMargin="-9.2132" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="210.4718" Y="37.7868" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5072" Y="0.0969" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_0_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_0_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_button_0.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Btn_del" ActionTag="-284662638" Tag="91" IconVisible="False" LeftMargin="273.0971" RightMargin="-12.0971" TopMargin="305.2132" BottomMargin="-9.2132" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="154.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="350.0971" Y="37.7868" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8436" Y="0.0969" />
                    <PreSize X="0.3711" Y="0.2410" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/join_pic/KFMJ_tips_delete_button_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/join_pic/KFMJ_tips_delete_button_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/join_pic/KFMJ_tips_delete_button.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ClubNumTxt_1" ActionTag="1769526969" Tag="92" IconVisible="False" LeftMargin="53.2114" RightMargin="338.7886" TopMargin="23.7690" BottomMargin="321.2310" IsCustomSize="True" FontSize="40" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="23.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="64.7114" Y="343.7310" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.1559" Y="0.8814" />
                    <PreSize X="0.0554" Y="0.1154" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ClubNumTxt_2" ActionTag="-161335111" Tag="151" IconVisible="False" LeftMargin="110.2196" RightMargin="281.7804" TopMargin="23.7690" BottomMargin="321.2310" IsCustomSize="True" FontSize="40" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="23.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.7196" Y="343.7310" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2933" Y="0.8814" />
                    <PreSize X="0.0554" Y="0.1154" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ClubNumTxt_3" ActionTag="411082170" Tag="152" IconVisible="False" LeftMargin="167.2278" RightMargin="224.7722" TopMargin="23.7690" BottomMargin="321.2310" IsCustomSize="True" FontSize="40" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="23.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="178.7278" Y="343.7310" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.4307" Y="0.8814" />
                    <PreSize X="0.0554" Y="0.1154" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ClubNumTxt_4" ActionTag="-698099584" Tag="153" IconVisible="False" LeftMargin="224.2360" RightMargin="167.7640" TopMargin="23.7690" BottomMargin="321.2310" IsCustomSize="True" FontSize="40" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="23.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="235.7360" Y="343.7310" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.5680" Y="0.8814" />
                    <PreSize X="0.0554" Y="0.1154" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ClubNumTxt_5" ActionTag="1184533405" Tag="154" IconVisible="False" LeftMargin="281.2441" RightMargin="110.7559" TopMargin="23.7690" BottomMargin="321.2310" IsCustomSize="True" FontSize="40" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="23.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="292.7441" Y="343.7310" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.7054" Y="0.8814" />
                    <PreSize X="0.0554" Y="0.1154" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ClubNumTxt_6" ActionTag="593329914" Tag="155" IconVisible="False" LeftMargin="338.2523" RightMargin="53.7477" TopMargin="23.7690" BottomMargin="321.2310" IsCustomSize="True" FontSize="40" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="23.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="349.7523" Y="343.7310" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.8428" Y="0.8814" />
                    <PreSize X="0.0554" Y="0.1154" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="691.2778" Y="158.8890" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5182" Y="0.2119" />
                <PreSize X="0.3111" Y="0.5200" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="ClubInfoPanel" ActionTag="2000124997" VisibleForFrame="False" Tag="108" IconVisible="False" LeftMargin="0.0001" RightMargin="-0.0001" TopMargin="-0.0001" BottomMargin="0.0001" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="234" RightEage="234" TopEage="112" BottomEage="112" Scale9OriginX="-234" Scale9OriginY="-112" Scale9Width="468" Scale9Height="224" ctype="PanelObjectData">
                <Size X="1334.0000" Y="750.0000" />
                <Children>
                  <AbstractNodeData Name="ImageBg" ActionTag="715524034" Tag="115" IconVisible="False" LeftMargin="454.1881" RightMargin="456.8119" TopMargin="314.4925" BottomMargin="222.5075" LeftEage="139" RightEage="139" TopEage="70" BottomEage="70" Scale9OriginX="139" Scale9OriginY="70" Scale9Width="145" Scale9Height="73" ctype="ImageViewObjectData">
                    <Size X="423.0000" Y="213.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="665.6881" Y="329.0075" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4990" Y="0.4387" />
                    <PreSize X="0.3171" Y="0.2840" />
                    <FileData Type="Normal" Path="club/btn_pic/guild_main_deskbg.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="HeadImg" ActionTag="-116135607" Tag="109" IconVisible="False" LeftMargin="465.8033" RightMargin="788.1967" TopMargin="363.6554" BottomMargin="306.3446" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                    <Size X="80.0000" Y="80.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="505.8033" Y="346.3446" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3792" Y="0.4618" />
                    <PreSize X="0.0600" Y="0.1067" />
                    <FileData Type="PlistSubImage" Path="Avatar104.png" Plist="public/im_head.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="NameTxt" ActionTag="-419435538" Tag="110" IconVisible="False" LeftMargin="550.6818" RightMargin="743.3182" TopMargin="373.2800" BottomMargin="353.7200" FontSize="20" LabelText="张三" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="40.0000" Y="23.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="550.6818" Y="365.2200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.4128" Y="0.4870" />
                    <PreSize X="0.0300" Y="0.0307" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="QunZhuTxt" ActionTag="-1613974588" Tag="111" IconVisible="False" LeftMargin="550.6823" RightMargin="654.3177" TopMargin="408.0360" BottomMargin="318.9640" FontSize="20" LabelText="群主: oiqpoqre" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="129.0000" Y="23.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="550.6823" Y="330.4640" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.4128" Y="0.4406" />
                    <PreSize X="0.0967" Y="0.0307" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="IDTxt" ActionTag="1820299102" Tag="113" IconVisible="False" LeftMargin="750.3818" RightMargin="485.6182" TopMargin="373.2800" BottomMargin="353.7200" FontSize="20" LabelText="ID: 874889" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="98.0000" Y="23.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="750.3818" Y="365.2200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5625" Y="0.4870" />
                    <PreSize X="0.0735" Y="0.0307" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="CountTxt" ActionTag="-865481744" VisibleForFrame="False" Tag="114" IconVisible="False" LeftMargin="750.3817" RightMargin="481.6183" TopMargin="370.7186" BottomMargin="356.2814" FontSize="20" LabelText="人数: 1/200" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="102.0000" Y="23.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="750.3817" Y="367.7814" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.5625" Y="0.4904" />
                    <PreSize X="0.0765" Y="0.0307" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="JoinBtn" ActionTag="-458912889" Tag="115" IconVisible="False" LeftMargin="560.8925" RightMargin="564.1075" TopMargin="452.7531" BottomMargin="222.2469" TouchEnable="True" FontSize="30" LeftEage="92" RightEage="79" TopEage="11" BottomEage="11" Scale9OriginX="92" Scale9OriginY="11" Scale9Width="38" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="209.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="665.3925" Y="259.7469" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4988" Y="0.3463" />
                    <PreSize X="0.1567" Y="0.1000" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="club/btn_pic/club_join_btn_press.png" Plist="" />
                    <PressedFileData Type="Normal" Path="club/btn_pic/club_join_btn_press.png" Plist="" />
                    <NormalFileData Type="Normal" Path="club/btn_pic/club_join_btn_normal.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0001" Y="375.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>