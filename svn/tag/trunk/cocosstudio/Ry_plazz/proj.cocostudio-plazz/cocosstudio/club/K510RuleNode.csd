<GameFile>
  <PropertyGroup Name="K510RuleNode" Type="Node" ID="5e92c645-7c8e-4d07-80c6-82ecd7fffe2c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="115" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="479830707" Tag="116" IconVisible="False" LeftMargin="-537.1724" RightMargin="-542.8276" TopMargin="-273.5544" BottomMargin="-276.4456" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1080.0000" Y="550.0000" />
            <Children>
              <AbstractNodeData Name="RenShuTxt" ActionTag="-1115386228" Tag="117" IconVisible="False" LeftMargin="161.7713" RightMargin="841.2286" TopMargin="106.6519" BottomMargin="409.3481" FontSize="30" LabelText="人数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2713" Y="426.3481" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.7752" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CheckBox_1" ActionTag="1397130227" Tag="118" IconVisible="False" LeftMargin="282.2013" RightMargin="607.7987" TopMargin="96.6519" BottomMargin="399.3481" TouchEnable="True" CheckedState="True" DisplayState="False" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-389298761" Tag="119" IconVisible="False" LeftMargin="45.0000" RightMargin="85.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="四人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.3158" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="377.2013" Y="426.3481" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3493" Y="0.7752" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="JuShuTxt" ActionTag="-1998054984" Tag="120" IconVisible="False" LeftMargin="161.7713" RightMargin="841.2286" TopMargin="371.0919" BottomMargin="144.9081" FontSize="30" LabelText="局数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2713" Y="161.9081" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.2944" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_1" ActionTag="179211882" Tag="121" IconVisible="False" LeftMargin="282.2013" RightMargin="607.7987" TopMargin="361.6471" BottomMargin="134.3529" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-979448730" Tag="122" IconVisible="False" LeftMargin="45.0000" RightMargin="-19.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="164.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.8632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="377.2013" Y="161.3529" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3493" Y="0.2934" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_2" ActionTag="-903624622" Tag="125" IconVisible="False" LeftMargin="510.3326" RightMargin="379.6674" TopMargin="361.6471" BottomMargin="134.3529" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="562072499" Tag="126" IconVisible="False" LeftMargin="45.0000" RightMargin="-19.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="8圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="164.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.8632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="605.3326" Y="161.3529" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5605" Y="0.2934" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_3" ActionTag="2096717469" Tag="129" IconVisible="False" LeftMargin="738.4610" RightMargin="151.5390" TopMargin="361.6471" BottomMargin="134.3529" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-662769284" Tag="130" IconVisible="False" LeftMargin="45.0000" RightMargin="-36.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="10圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="181.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.9526" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="833.4610" Y="161.3529" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7717" Y="0.2934" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="SureRuleBtn" ActionTag="-1613167406" Tag="133" IconVisible="False" LeftMargin="800.0000" RightMargin="20.0000" TopMargin="450.0000" BottomMargin="10.0000" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="230" Scale9Height="68" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="260.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="930.0000" Y="55.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8611" Y="0.1000" />
                <PreSize X="0.2407" Y="0.1636" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/sure_rule_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/sure_rule_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/sure_rule.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ZuoWeiTxt" ActionTag="1076417895" Tag="210" IconVisible="False" LeftMargin="161.7713" RightMargin="841.2286" TopMargin="194.7982" BottomMargin="321.2018" FontSize="30" LabelText="座位: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2713" Y="338.2018" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.6149" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ZuoWeiCheckBox" ActionTag="-1053930811" Tag="211" IconVisible="False" LeftMargin="282.2013" RightMargin="607.7987" TopMargin="187.5068" BottomMargin="308.4932" TouchEnable="True" CheckedState="True" DisplayState="False" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1124543808" Tag="212" IconVisible="False" LeftMargin="45.0000" RightMargin="25.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="自助选座" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.6316" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="377.2013" Y="335.4932" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3493" Y="0.6100" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="ZuoWeiTxt_0" ActionTag="820207829" Tag="213" IconVisible="False" LeftMargin="146.7697" RightMargin="826.2303" TopMargin="282.9455" BottomMargin="233.0545" FontSize="30" LabelText="大小血: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="107.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.2697" Y="250.0545" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1854" Y="0.4546" />
                <PreSize X="0.0991" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigOrSmallCheckBox" ActionTag="41587945" Tag="214" IconVisible="False" LeftMargin="282.2019" RightMargin="657.7981" TopMargin="270.7925" BottomMargin="225.2075" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-395942572" Tag="215" IconVisible="False" LeftMargin="45.0000" RightMargin="5.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="大小血" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="0.6429" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="352.2019" Y="252.2075" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3261" Y="0.4586" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.8276" Y="-1.4456" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>