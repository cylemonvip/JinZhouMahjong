<GameFile>
  <PropertyGroup Name="LandRuleNode" Type="Node" ID="af21db4d-48a0-4d6e-ac7e-c6ff82899de6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="115" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="479830707" Tag="116" IconVisible="False" LeftMargin="-539.0033" RightMargin="-540.9967" TopMargin="-275.0000" BottomMargin="-275.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1080.0000" Y="550.0000" />
            <Children>
              <AbstractNodeData Name="RenShuTxt" ActionTag="-1115386228" Tag="117" IconVisible="False" LeftMargin="186.5028" RightMargin="816.4972" TopMargin="82.6544" BottomMargin="433.3456" FontSize="30" LabelText="人数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0028" Y="450.3456" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.2083" Y="0.8188" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CheckBox_1" ActionTag="1397130227" Tag="118" IconVisible="False" LeftMargin="299.2606" RightMargin="590.7394" TopMargin="78.1225" BottomMargin="417.8775" TouchEnable="True" CheckedState="True" DisplayState="False" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-389298761" Tag="119" IconVisible="False" LeftMargin="45.0000" RightMargin="85.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="三人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="60.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.3158" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="394.2606" Y="444.8775" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3651" Y="0.8089" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="JuShuTxt" ActionTag="-1998054984" Tag="120" IconVisible="False" LeftMargin="186.5028" RightMargin="816.4972" TopMargin="389.5887" BottomMargin="126.4113" FontSize="30" LabelText="局数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0028" Y="143.4113" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.2083" Y="0.2607" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_1" ActionTag="179211882" Tag="121" IconVisible="False" LeftMargin="299.2606" RightMargin="590.7394" TopMargin="379.5884" BottomMargin="116.4116" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-979448730" Tag="122" IconVisible="False" LeftMargin="45.0000" RightMargin="-19.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="164.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.8632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="394.2606" Y="143.4116" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3651" Y="0.2607" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_2" ActionTag="-903624622" Tag="125" IconVisible="False" LeftMargin="531.3503" RightMargin="358.6497" TopMargin="379.5884" BottomMargin="116.4116" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="562072499" Tag="126" IconVisible="False" LeftMargin="45.0000" RightMargin="-19.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="8圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="164.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.8632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="626.3503" Y="143.4116" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5800" Y="0.2607" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuCheckBox_3" ActionTag="2096717469" Tag="129" IconVisible="False" LeftMargin="763.4395" RightMargin="126.5605" TopMargin="379.5884" BottomMargin="116.4116" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-662769284" Tag="130" IconVisible="False" LeftMargin="45.0000" RightMargin="-36.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="10圈(房卡X2)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="181.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.9526" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="858.4395" Y="143.4116" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7949" Y="0.2607" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="SureRuleBtn" ActionTag="-1613167406" Tag="133" IconVisible="False" LeftMargin="800.0000" RightMargin="20.0000" TopMargin="450.0000" BottomMargin="10.0000" TouchEnable="True" FontSize="25" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="230" Scale9Height="68" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="260.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="930.0000" Y="55.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8611" Y="0.1000" />
                <PreSize X="0.2407" Y="0.1636" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/sure_rule_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/sure_rule_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/sure_rule.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleTxt" ActionTag="-1561035766" Tag="29" IconVisible="False" LeftMargin="186.5028" RightMargin="816.4972" TopMargin="184.9708" BottomMargin="331.0292" FontSize="30" LabelText="规则: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0028" Y="348.0292" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.2083" Y="0.6328" />
                <PreSize X="0.0713" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_1" ActionTag="1029789648" Tag="30" IconVisible="False" LeftMargin="299.2614" RightMargin="640.7386" TopMargin="178.6158" BottomMargin="317.3842" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-394023133" Tag="31" IconVisible="False" LeftMargin="45.0000" RightMargin="-55.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="农民可加倍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="1.0714" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="369.2614" Y="344.3842" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3419" Y="0.6262" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleCheckBox_2" ActionTag="485293617" Tag="32" IconVisible="False" LeftMargin="531.3514" RightMargin="408.6486" TopMargin="178.6160" BottomMargin="317.3840" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="140.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="-548600072" Tag="33" IconVisible="False" LeftMargin="45.0000" RightMargin="-209.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="双王或四个2必须叫3分" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="304.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.3214" Y="0.5000" />
                    <PreSize X="2.1714" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="601.3514" Y="344.3840" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5568" Y="0.6262" />
                <PreSize X="0.1296" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box5.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box6.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="RuleTxt_0" ActionTag="839061618" Tag="34" IconVisible="False" LeftMargin="126.5047" RightMargin="816.4952" TopMargin="287.2767" BottomMargin="228.7233" FontSize="30" LabelText="最大番数: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="137.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="195.0047" Y="245.7233" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="114" B="101" />
                <PrePosition X="0.1806" Y="0.4468" />
                <PreSize X="0.1269" Y="0.0618" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="FanCheckBox_1" ActionTag="2037628503" Tag="35" IconVisible="False" LeftMargin="299.2606" RightMargin="590.7394" TopMargin="279.0994" BottomMargin="216.9006" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1988637259" Tag="36" IconVisible="False" LeftMargin="45.0000" RightMargin="38.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="3炸封顶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="107.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.5632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="394.2606" Y="243.9006" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3651" Y="0.4435" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="FanCheckBox_2" ActionTag="1022410730" Tag="37" IconVisible="False" LeftMargin="531.3496" RightMargin="358.6504" TopMargin="279.1040" BottomMargin="216.8960" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1788760461" Tag="38" IconVisible="False" LeftMargin="45.0000" RightMargin="38.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="4炸封顶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="107.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.5632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.6845" ScaleY="0.6777" />
                <Position X="661.4046" Y="253.4918" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6124" Y="0.4609" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="FanCheckBox_3" ActionTag="-753617835" Tag="39" IconVisible="False" LeftMargin="763.4395" RightMargin="126.5605" TopMargin="279.0994" BottomMargin="216.9006" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="190.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="76872106" Tag="40" IconVisible="False" LeftMargin="45.0000" RightMargin="38.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="5炸封顶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="107.0000" Y="34.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="45.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="37" G="114" B="101" />
                    <PrePosition X="0.2368" Y="0.5000" />
                    <PreSize X="0.5632" Y="0.6296" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="858.4395" Y="243.9006" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7949" Y="0.4435" />
                <PreSize X="0.1759" Y="0.0982" />
                <NormalBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box3.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="club/btn_pic/KFMJ_select_box4.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.9967" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>