<GameFile>
  <PropertyGroup Name="PlayerNode" Type="Node" ID="9925509e-ef17-4c41-bc1a-a58c1317ec7e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="136" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="1472398733" Tag="137" IconVisible="False" LeftMargin="-50.0000" RightMargin="-50.0000" TopMargin="-50.0000" BottomMargin="-50.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="141" RightEage="141" TopEage="140" BottomEage="140" Scale9OriginX="141" Scale9OriginY="140" Scale9Width="148" Scale9Height="145" ctype="PanelObjectData">
            <Size X="100.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="HeadImg" ActionTag="-725786469" Tag="139" IconVisible="False" LeftMargin="15.0000" RightMargin="15.0000" TopMargin="-0.0001" BottomMargin="30.0001" LeftEage="31" RightEage="31" TopEage="31" BottomEage="31" Scale9OriginX="31" Scale9OriginY="31" Scale9Width="34" Scale9Height="34" ctype="ImageViewObjectData">
                <Size X="70.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.0000" Y="65.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6500" />
                <PreSize X="0.7000" Y="0.7000" />
                <FileData Type="PlistSubImage" Path="Avatar126.png" Plist="public/im_head.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="101685420" Tag="141" IconVisible="False" LeftMargin="12.5000" RightMargin="12.5000" TopMargin="73.9542" BottomMargin="5.0458" FontSize="15" LabelText="胡汉三大佬" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.0000" Y="15.5458" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.5000" Y="0.1555" />
                <PreSize X="0.7500" Y="0.2100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TipSp" ActionTag="1851875161" Tag="150" IconVisible="False" LeftMargin="-1.5925" RightMargin="74.5925" TopMargin="-1.0491" BottomMargin="74.0491" ctype="SpriteObjectData">
                <Size X="27.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="11.9075" Y="87.5491" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1191" Y="0.8755" />
                <PreSize X="0.2700" Y="0.2700" />
                <FileData Type="PlistSubImage" Path="sp_dot.png" Plist="public/public.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="club/pic/sub_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>