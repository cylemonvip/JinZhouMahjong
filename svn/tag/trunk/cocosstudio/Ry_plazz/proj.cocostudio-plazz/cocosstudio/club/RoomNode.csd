<GameFile>
  <PropertyGroup Name="RoomNode" Type="Node" ID="67de1e28-fd77-4815-918c-f150bb8e165f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="357" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1786043499" Tag="358" IconVisible="False" LeftMargin="-108.5000" RightMargin="-108.5000" TopMargin="-98.5000" BottomMargin="-98.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="71" RightEage="71" TopEage="65" BottomEage="65" Scale9OriginX="71" Scale9OriginY="65" Scale9Width="75" Scale9Height="67" ctype="PanelObjectData">
            <Size X="217.0000" Y="197.0000" />
            <Children>
              <AbstractNodeData Name="RoomCardTxt" ActionTag="805476692" Tag="359" IconVisible="False" LeftMargin="28.7599" RightMargin="30.2401" TopMargin="23.8376" BottomMargin="141.1624" FontSize="28" LabelText="房号:123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="158.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="107.7599" Y="157.1624" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4966" Y="0.7978" />
                <PreSize X="0.7281" Y="0.1624" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="PlayerCountTxt" ActionTag="537297802" Tag="360" IconVisible="False" LeftMargin="65.7804" RightMargin="66.2196" TopMargin="62.0583" BottomMargin="107.9417" FontSize="23" LabelText="人数:1/4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="85.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.2804" Y="121.4417" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4990" Y="0.6165" />
                <PreSize X="0.3917" Y="0.1371" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameNameTxt" ActionTag="-146164529" Tag="361" IconVisible="False" LeftMargin="35.7803" RightMargin="36.2197" TopMargin="93.2791" BottomMargin="76.7209" FontSize="23" LabelText="游戏:红中麻将" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="145.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.2803" Y="90.2209" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4990" Y="0.4580" />
                <PreSize X="0.6682" Y="0.1371" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="GameStatusTxt" ActionTag="1881888903" Tag="36" IconVisible="False" LeftMargin="47.2803" RightMargin="47.7197" TopMargin="124.4999" BottomMargin="45.5001" FontSize="23" LabelText="状态:已开始" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="122.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.2803" Y="59.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4990" Y="0.2995" />
                <PreSize X="0.5622" Y="0.1371" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RoundCountTxt" ActionTag="1599056151" VisibleForFrame="False" Tag="37" IconVisible="False" LeftMargin="71.2804" RightMargin="71.7196" TopMargin="84.6688" BottomMargin="89.3312" FontSize="20" LabelText="局数:1/4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.2804" Y="100.8312" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4990" Y="0.5118" />
                <PreSize X="0.3410" Y="0.1168" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ShareBtn" ActionTag="1988290579" Tag="438" IconVisible="False" LeftMargin="-75.0000" RightMargin="48.0000" TopMargin="139.1264" BottomMargin="-30.1264" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="214" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="244.0000" Y="88.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="13.8736" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2166" Y="0.0704" />
                <PreSize X="1.1244" Y="0.4467" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/share_btn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/share_btn.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/share_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JieSanBtn" ActionTag="80565423" Tag="439" IconVisible="False" LeftMargin="47.0001" RightMargin="-74.0001" TopMargin="139.1264" BottomMargin="-30.1264" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="214" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="244.0000" Y="88.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="169.0001" Y="13.8736" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7788" Y="0.0704" />
                <PreSize X="1.1244" Y="0.4467" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="club/btn_pic/dismiss_btn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="club/btn_pic/dismiss_btn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="club/btn_pic/dismiss_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="club/btn_pic/desk.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>