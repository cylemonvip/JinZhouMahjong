<GameFile>
  <PropertyGroup Name="LuckyDrawNode" Type="Node" ID="3fdb2975-e6e6-49da-9af3-083fcaab6b77" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="109" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="584055246" Tag="110" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="203" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="Bg" ActionTag="-1129364065" Tag="74" IconVisible="False" LeftMargin="193.4453" RightMargin="144.5547" TopMargin="73.6145" BottomMargin="100.3855" LeftEage="428" RightEage="428" TopEage="224" BottomEage="224" Scale9OriginX="428" Scale9OriginY="224" Scale9Width="140" Scale9Height="128" ctype="ImageViewObjectData">
                <Size X="996.0000" Y="576.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="691.4453" Y="388.3855" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5183" Y="0.5178" />
                <PreSize X="0.7466" Y="0.7680" />
                <FileData Type="Normal" Path="luckydraw/sp_background.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpBorder" ActionTag="-830069784" Tag="112" IconVisible="False" LeftMargin="657.2078" RightMargin="171.7922" TopMargin="101.0346" BottomMargin="138.9654" ctype="SpriteObjectData">
                <Size X="505.0000" Y="510.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="909.7078" Y="393.9654" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6819" Y="0.5253" />
                <PreSize X="0.3786" Y="0.6800" />
                <FileData Type="Normal" Path="luckydraw/faku_wheelbigd_bt.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Item" ActionTag="-1182621276" Tag="76" IconVisible="False" LeftMargin="654.8613" RightMargin="174.1387" TopMargin="106.0924" BottomMargin="133.9076" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="505.0000" Y="510.0000" />
                <Children>
                  <AbstractNodeData Name="fafu_3" ActionTag="1728179709" Tag="75" IconVisible="False" LeftMargin="44.1678" RightMargin="360.8322" TopMargin="357.9216" BottomMargin="53.0784" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="1363015962" Tag="42" IconVisible="False" LeftMargin="-75.2332" RightMargin="-80.7668" TopMargin="-81.7666" BottomMargin="-75.2334" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="52.7668" Y="52.7666" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5277" Y="0.5330" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="64949742" Tag="88" RotationSkewX="0.6356" RotationSkewY="0.6356" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="3.5000" BottomMargin="3.5000" ctype="SpriteObjectData">
                        <Size X="88.0000" Y="92.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.8800" Y="0.9293" />
                        <FileData Type="Normal" Path="luckydraw/faku_iphone8_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="94.1678" Y="102.5784" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1865" Y="0.2011" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wp_spe.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_4" ActionTag="106459455" Tag="77" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="151.3940" RightMargin="253.6060" TopMargin="356.9216" BottomMargin="54.0784" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="-1274507909" Tag="43" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="1197393727" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.0000" RightMargin="4.0000" TopMargin="21.4200" BottomMargin="5.5800" ctype="SpriteObjectData">
                        <Size X="92.0000" Y="72.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="41.5800" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4200" />
                        <PreSize X="0.9200" Y="0.7273" />
                        <FileData Type="Normal" Path="luckydraw/faku_card1_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="201.3940" Y="103.5784" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3988" Y="0.2031" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_5" ActionTag="665333878" Tag="78" IconVisible="False" LeftMargin="257.0339" RightMargin="147.9661" TopMargin="356.9216" BottomMargin="54.0784" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="1258612646" Tag="44" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="-510955507" Tag="89" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="17.5000" RightMargin="17.5000" TopMargin="9.5000" BottomMargin="9.5000" ctype="SpriteObjectData">
                        <Size X="65.0000" Y="80.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6500" Y="0.8081" />
                        <FileData Type="Normal" Path="luckydraw/once_again.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="307.0339" Y="103.5784" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6080" Y="0.2031" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_8" ActionTag="1743975144" Tag="79" IconVisible="False" LeftMargin="362.6604" RightMargin="42.3396" TopMargin="147.8016" BottomMargin="263.1984" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" ActionTag="-179792785" Tag="45" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" ActionTag="-1035507516" Tag="91" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="15.5000" RightMargin="15.5000" TopMargin="15.9800" BottomMargin="12.0200" ctype="SpriteObjectData">
                        <Size X="69.0000" Y="71.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="47.5200" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4800" />
                        <PreSize X="0.6900" Y="0.7172" />
                        <FileData Type="Normal" Path="luckydraw/faku_cash5_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="412.6604" Y="312.6984" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8171" Y="0.6131" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_9" ActionTag="2099782371" Tag="80" IconVisible="False" LeftMargin="362.6605" RightMargin="42.3395" TopMargin="43.2413" BottomMargin="367.7587" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" ActionTag="772179889" Tag="46" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" ActionTag="1489144655" Tag="92" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.0000" RightMargin="16.0000" TopMargin="15.9800" BottomMargin="12.0200" ctype="SpriteObjectData">
                        <Size X="68.0000" Y="71.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="47.5200" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4800" />
                        <PreSize X="0.6800" Y="0.7172" />
                        <FileData Type="Normal" Path="luckydraw/faku_cash1_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="412.6605" Y="417.2587" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8171" Y="0.8182" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_6" ActionTag="-190895396" Tag="81" IconVisible="False" LeftMargin="362.6606" RightMargin="42.3394" TopMargin="356.9216" BottomMargin="54.0784" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="-747782727" Tag="47" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="590708500" Tag="93" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.5000" RightMargin="12.5000" TopMargin="10.5000" BottomMargin="10.5000" ctype="SpriteObjectData">
                        <Size X="75.0000" Y="78.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.7500" Y="0.7879" />
                        <FileData Type="Normal" Path="luckydraw/faku_cash100_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="412.6606" Y="103.5784" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8171" Y="0.2031" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wp_spe.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_7" ActionTag="909143688" Tag="82" IconVisible="False" LeftMargin="362.6606" RightMargin="42.3394" TopMargin="252.3615" BottomMargin="158.6385" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" ActionTag="2078634041" Tag="48" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" ActionTag="-59726162" Tag="94" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.5000" RightMargin="2.5000" TopMargin="25.9200" BottomMargin="10.0800" ctype="SpriteObjectData">
                        <Size X="95.0000" Y="63.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="41.5800" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4200" />
                        <PreSize X="0.9500" Y="0.6364" />
                        <FileData Type="Normal" Path="luckydraw/faku_card3_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="412.6606" Y="208.1385" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8171" Y="0.4081" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_0" ActionTag="-777435118" Tag="83" IconVisible="False" LeftMargin="45.7806" RightMargin="359.2194" TopMargin="43.2413" BottomMargin="367.7587" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="-1550589743" Tag="49" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="1627073869" Tag="95" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="15.5000" RightMargin="15.5000" TopMargin="15.9800" BottomMargin="12.0200" ctype="SpriteObjectData">
                        <Size X="69.0000" Y="71.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="47.5200" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4800" />
                        <PreSize X="0.6900" Y="0.7172" />
                        <FileData Type="Normal" Path="luckydraw/faku_cash2_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="95.7806" Y="417.2587" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1897" Y="0.8182" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_11" ActionTag="1669982201" Tag="84" IconVisible="False" LeftMargin="151.4072" RightMargin="253.5928" TopMargin="43.2413" BottomMargin="367.7587" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="999111235" Tag="50" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="-1462850241" Tag="96" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="17.5000" RightMargin="17.5000" TopMargin="9.5000" BottomMargin="9.5000" ctype="SpriteObjectData">
                        <Size X="65.0000" Y="80.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6500" Y="0.8081" />
                        <FileData Type="Normal" Path="luckydraw/once_again.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="201.4072" Y="417.2587" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3988" Y="0.8182" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_10" ActionTag="-2093581540" Tag="85" IconVisible="False" LeftMargin="257.0338" RightMargin="147.9662" TopMargin="43.2413" BottomMargin="367.7587" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="1410712584" Tag="51" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="-1542082531" Tag="97" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.0000" RightMargin="4.0000" TopMargin="21.4200" BottomMargin="5.5800" ctype="SpriteObjectData">
                        <Size X="92.0000" Y="72.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="41.5800" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4200" />
                        <PreSize X="0.9200" Y="0.7273" />
                        <FileData Type="Normal" Path="luckydraw/faku_card1_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="307.0338" Y="417.2587" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6080" Y="0.8182" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_1" ActionTag="-1560245964" Tag="86" IconVisible="False" LeftMargin="45.7806" RightMargin="359.2194" TopMargin="148.1347" BottomMargin="262.8653" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="1113301734" Tag="52" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="1973927843" Tag="98" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="15.9800" BottomMargin="12.0200" ctype="SpriteObjectData">
                        <Size X="67.0000" Y="71.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="47.5200" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4800" />
                        <PreSize X="0.6700" Y="0.7172" />
                        <FileData Type="Normal" Path="luckydraw/faku_cash10_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="95.7806" Y="312.3653" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1897" Y="0.6125" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fafu_2" ActionTag="-97424817" Tag="87" IconVisible="False" LeftMargin="45.7806" RightMargin="359.2194" TopMargin="253.0282" BottomMargin="157.9718" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="99.0000" />
                    <Children>
                      <AbstractNodeData Name="guang" CanEdit="False" ActionTag="434541335" Tag="53" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-78.0000" RightMargin="-78.0000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                        <Size X="256.0000" Y="256.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="49.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.5600" Y="2.5859" />
                        <FileData Type="Normal" Path="luckydraw/guangyun.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="icon" CanEdit="False" ActionTag="-1887846975" Tag="99" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.5000" RightMargin="2.5000" TopMargin="22.4200" BottomMargin="6.5800" ctype="SpriteObjectData">
                        <Size X="95.0000" Y="70.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="41.5800" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4200" />
                        <PreSize X="0.9500" Y="0.7071" />
                        <FileData Type="Normal" Path="luckydraw/faku_card2_icon.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="95.7806" Y="207.4718" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1897" Y="0.4068" />
                    <PreSize X="0.1980" Y="0.1941" />
                    <FileData Type="Normal" Path="luckydraw/faku_wheelminid1_bt.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="654.8613" Y="133.9076" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4909" Y="0.1785" />
                <PreSize X="0.3786" Y="0.6800" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnStart" ActionTag="-840369668" Tag="117" IconVisible="False" LeftMargin="808.8846" RightMargin="325.1154" TopMargin="266.6855" BottomMargin="297.3145" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="164" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="200.0000" Y="186.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="908.8846" Y="390.3145" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6813" Y="0.5204" />
                <PreSize X="0.1499" Y="0.2480" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="luckydraw/faku_btn_p6.png" Plist="" />
                <PressedFileData Type="Normal" Path="luckydraw/faku_btn_p6.png" Plist="" />
                <NormalFileData Type="Normal" Path="luckydraw/faku_btn_p5.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnCopy" ActionTag="1480759754" Tag="103" IconVisible="False" LeftMargin="480.0316" RightMargin="759.9684" TopMargin="539.4650" BottomMargin="168.5350" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="64" Scale9Height="20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="94.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="527.0316" Y="189.5350" />
                <Scale ScaleX="1.1000" ScaleY="1.1000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3951" Y="0.2527" />
                <PreSize X="0.0705" Y="0.0560" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="luckydraw/shop_copy_btn_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="luckydraw/shop_copy_btn_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="luckydraw/shop_copy_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SelectWheel" ActionTag="-90736652" Tag="101" IconVisible="False" LeftMargin="688.5000" RightMargin="522.5000" TopMargin="139.0000" BottomMargin="489.0000" ctype="SpriteObjectData">
                <Size X="123.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="750.0000" Y="550.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5622" Y="0.7333" />
                <PreSize X="0.0922" Y="0.1627" />
                <FileData Type="Normal" Path="luckydraw/faku_wheel_selected.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>