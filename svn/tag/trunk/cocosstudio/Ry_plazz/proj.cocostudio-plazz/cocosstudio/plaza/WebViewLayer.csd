<GameFile>
  <PropertyGroup Name="WebViewLayer" Type="Layer" ID="301c6c2f-66d6-4993-969b-21cbb9382280" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="59" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="panel_mask" ActionTag="1343982046" Tag="60" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="GonggaoPanel" ActionTag="-2122683845" VisibleForFrame="False" Tag="77" IconVisible="False" LeftMargin="206.0000" RightMargin="206.0000" TopMargin="138.0000" BottomMargin="138.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="304" RightEage="304" TopEage="156" BottomEage="156" Scale9OriginX="304" Scale9OriginY="156" Scale9Width="314" Scale9Height="162" ctype="PanelObjectData">
            <Size X="922.0000" Y="474.0000" />
            <Children>
              <AbstractNodeData Name="CopyBtn" ActionTag="53519095" Tag="46" IconVisible="False" LeftMargin="740.1987" RightMargin="-19.1987" TopMargin="372.5499" BottomMargin="20.4501" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="171" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="201.0000" Y="81.0000" />
                <Children>
                  <AbstractNodeData Name="Txt" ActionTag="1351218634" Tag="47" IconVisible="False" LeftMargin="19.4747" RightMargin="19.5253" TopMargin="14.2537" BottomMargin="17.7463" FontSize="40" LabelText="一键复制" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="162.0000" Y="49.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.4747" Y="42.2463" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4999" Y="0.5216" />
                    <PreSize X="0.8060" Y="0.6049" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="77" G="77" B="77" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="840.6987" Y="60.9501" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9118" Y="0.1286" />
                <PreSize X="0.2180" Y="0.1709" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/btn_yellow_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/btn_yellow_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/btn_yellow.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6912" Y="0.6320" />
            <FileData Type="Normal" Path="plaza/gonggao_img.jpg" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="PageView" ActionTag="1910424891" Tag="75" IconVisible="False" LeftMargin="247.0000" RightMargin="247.0000" TopMargin="138.0000" BottomMargin="138.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="840.0000" Y="474.0000" />
            <Children>
              <AbstractNodeData Name="Panel_2" ActionTag="-37356142" Tag="78" IconVisible="False" RightMargin="1680.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="275" RightEage="275" TopEage="157" BottomEage="157" Scale9OriginX="275" Scale9OriginY="157" Scale9Width="286" Scale9Height="162" ctype="PanelObjectData">
                <Size X="840.0000" Y="474.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.3333" Y="1.0000" />
                <FileData Type="Normal" Path="plaza/club_1.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_3" ActionTag="1693640929" ZOrder="1" Tag="79" IconVisible="False" LeftMargin="840.0000" RightMargin="840.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="275" RightEage="275" TopEage="157" BottomEage="157" Scale9OriginX="275" Scale9OriginY="157" Scale9Width="285" Scale9Height="159" ctype="PanelObjectData">
                <Size X="840.0000" Y="474.0000" />
                <AnchorPoint />
                <Position X="840.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3333" />
                <PreSize X="0.3333" Y="1.0000" />
                <FileData Type="Normal" Path="plaza/club_2.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_4" ActionTag="-1099045283" ZOrder="2" Tag="80" IconVisible="False" LeftMargin="1680.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="275" RightEage="275" TopEage="157" BottomEage="157" Scale9OriginX="275" Scale9OriginY="157" Scale9Width="288" Scale9Height="156" ctype="PanelObjectData">
                <Size X="840.0000" Y="474.0000" />
                <AnchorPoint />
                <Position X="1680.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6667" />
                <PreSize X="0.3333" Y="1.0000" />
                <FileData Type="Normal" Path="plaza/club_3.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6297" Y="0.6320" />
            <SingleColor A="255" R="150" G="150" B="100" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_close" ActionTag="1926893349" Tag="81" IconVisible="False" LeftMargin="1049.5056" RightMargin="216.4944" TopMargin="104.4731" BottomMargin="577.5269" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="68.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1083.5056" Y="611.5269" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8122" Y="0.8154" />
            <PreSize X="0.0510" Y="0.0907" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
            <PressedFileData Type="PlistSubImage" Path="common_btn_close_press.png" Plist="common.plist" />
            <NormalFileData Type="PlistSubImage" Path="common_btn_close_normal.png" Plist="common.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Point_1" ActionTag="-560305321" Tag="82" IconVisible="False" LeftMargin="613.9999" RightMargin="694.0001" TopMargin="614.6441" BottomMargin="109.3559" ctype="SpriteObjectData">
            <Size X="26.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="626.9999" Y="122.3559" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4700" Y="0.1631" />
            <PreSize X="0.0195" Y="0.0347" />
            <FileData Type="Normal" Path="plaza/wx_cr_checkpoint.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Point_2" ActionTag="-1872117571" Tag="83" IconVisible="False" LeftMargin="653.9999" RightMargin="654.0001" TopMargin="614.6441" BottomMargin="109.3559" ctype="SpriteObjectData">
            <Size X="26.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="666.9999" Y="122.3559" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1631" />
            <PreSize X="0.0195" Y="0.0347" />
            <FileData Type="Normal" Path="plaza/wx_cr_checkboxbg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Point_3" ActionTag="-1176947875" Tag="84" IconVisible="False" LeftMargin="693.9999" RightMargin="614.0001" TopMargin="614.6441" BottomMargin="109.3559" ctype="SpriteObjectData">
            <Size X="26.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="706.9999" Y="122.3559" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5300" Y="0.1631" />
            <PreSize X="0.0195" Y="0.0347" />
            <FileData Type="Normal" Path="plaza/wx_cr_checkboxbg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>