<GameFile>
  <PropertyGroup Name="PrivateRoomCreateLayer" Type="Layer" ID="b1756db5-4ace-40f9-b7ef-c328f3f3e909" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="-2131718381" Tag="50" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Bg" ActionTag="-664636880" Tag="48" IconVisible="False" LeftMargin="171.0000" RightMargin="171.0000" TopMargin="75.5000" BottomMargin="75.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Enable="True" LeftEage="117" RightEage="117" TopEage="16" BottomEage="16" Scale9OriginX="117" Scale9OriginY="16" Scale9Width="758" Scale9Height="567" ctype="PanelObjectData">
            <Size X="992.0000" Y="599.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7436" Y="0.7987" />
            <FileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_popupbg.png" Plist="" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="titleSp" ActionTag="1990384855" Tag="45" IconVisible="False" LeftMargin="446.0000" RightMargin="446.0000" TopMargin="64.5000" BottomMargin="622.5000" ctype="SpriteObjectData">
            <Size X="442.0000" Y="63.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="654.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8720" />
            <PreSize X="0.0345" Y="0.0613" />
            <FileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_title.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CloseBtn" ActionTag="1519700553" Tag="499" IconVisible="False" LeftMargin="1113.7849" RightMargin="172.2151" TopMargin="77.9285" BottomMargin="623.0715" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="18" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="48.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1137.7849" Y="647.5715" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8529" Y="0.8634" />
            <PreSize X="0.0360" Y="0.0653" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_closebtn_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_closebtn_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_closebtn_normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="pri_sp_numbg" ActionTag="-29296447" Tag="49" IconVisible="False" LeftMargin="200.7820" RightMargin="983.2180" TopMargin="588.8326" BottomMargin="111.1674" Scale9Enable="True" LeftEage="82" RightEage="82" TopEage="16" BottomEage="16" Scale9OriginX="82" Scale9OriginY="16" Scale9Width="86" Scale9Height="18" ctype="ImageViewObjectData">
            <Size X="150.0000" Y="50.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="200.7820" Y="136.1674" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1505" Y="0.1816" />
            <PreSize X="0.1124" Y="0.0667" />
            <FileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_roomcardbg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="pri_sp_cardbg" Tag="91" RotationSkewX="-41.4443" RotationSkewY="-41.4443" IconVisible="False" LeftMargin="193.3026" RightMargin="1085.6974" TopMargin="592.7668" BottomMargin="119.2332" ctype="SpriteObjectData">
            <Size X="55.0000" Y="38.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="220.8026" Y="138.2332" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1655" Y="0.1843" />
            <PreSize X="0.0412" Y="0.0507" />
            <FileData Type="Normal" Path="room/pri510k_sp_cardbg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt_cardnum" Tag="92" IconVisible="False" LeftMargin="288.4846" RightMargin="996.5154" TopMargin="595.5001" BottomMargin="121.4999" FontSize="26" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="49.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="288.4846" Y="137.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2163" Y="0.1840" />
            <PreSize X="0.0367" Y="0.0440" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_createroom" Tag="17" IconVisible="False" LeftMargin="914.9530" RightMargin="201.0470" TopMargin="569.6108" BottomMargin="100.3892" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="188" Scale9Height="58" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="218.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1023.9530" Y="140.3892" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7676" Y="0.1872" />
            <PreSize X="0.1634" Y="0.1067" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_createtbn_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_createtbn_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_createtbn_normal.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayerCountTitle" ActionTag="1696477656" Tag="50" IconVisible="False" LeftMargin="259.3901" RightMargin="996.6099" TopMargin="200.1151" BottomMargin="518.8849" FontSize="26" LabelText="人数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="78.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.3901" Y="534.3849" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="139" G="105" B="20" />
            <PrePosition X="0.2237" Y="0.7125" />
            <PreSize X="0.0585" Y="0.0413" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="SitModeTitle" ActionTag="-306085953" Tag="51" IconVisible="False" LeftMargin="259.3901" RightMargin="996.6099" TopMargin="327.1570" BottomMargin="391.8430" FontSize="26" LabelText="座位：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="78.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.3901" Y="407.3430" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="139" G="105" B="20" />
            <PrePosition X="0.2237" Y="0.5431" />
            <PreSize X="0.0585" Y="0.0413" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayCountTitle" ActionTag="-1479597933" Tag="52" IconVisible="False" LeftMargin="259.3902" RightMargin="996.6098" TopMargin="473.4208" BottomMargin="245.5792" FontSize="26" LabelText="局数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="78.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.3902" Y="261.0792" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="139" G="105" B="20" />
            <PrePosition X="0.2237" Y="0.3481" />
            <PreSize X="0.0585" Y="0.0413" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayerCountCb_1" ActionTag="92586541" Tag="53" IconVisible="False" LeftMargin="355.5994" RightMargin="952.4006" TopMargin="202.7926" BottomMargin="521.2074" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-1960160580" Tag="54" IconVisible="False" LeftMargin="30.0000" RightMargin="-45.0000" TopMargin="-4.5000" BottomMargin="-0.5000" FontSize="26" LabelText="4人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="30.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="1.1538" Y="0.5769" />
                <PreSize X="1.5769" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="368.5994" Y="534.2074" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2763" Y="0.7123" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="SitCb_1" ActionTag="1979303319" Tag="57" IconVisible="False" LeftMargin="355.5998" RightMargin="952.4002" TopMargin="329.6570" BottomMargin="394.3430" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-167920315" Tag="58" IconVisible="False" LeftMargin="30.0000" RightMargin="-108.0000" TopMargin="-4.5000" BottomMargin="-0.5000" FontSize="26" LabelText="自主选座" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="104.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="30.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="1.1538" Y="0.5769" />
                <PreSize X="4.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="368.5998" Y="407.3430" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2763" Y="0.5431" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BigSmallBloodCb_1" ActionTag="61915151" Tag="61" IconVisible="False" LeftMargin="355.5999" RightMargin="952.4001" TopMargin="402.8075" BottomMargin="321.1925" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-593754153" Tag="62" IconVisible="False" LeftMargin="30.0000" RightMargin="-82.0000" TopMargin="-4.5000" BottomMargin="-0.5000" FontSize="26" LabelText="大小血" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="30.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="1.1538" Y="0.5769" />
                <PreSize X="3.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="368.5999" Y="334.1925" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2763" Y="0.4456" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayCountCb_1" ActionTag="738516636" Tag="63" IconVisible="False" LeftMargin="355.5995" RightMargin="952.4005" TopMargin="475.9208" BottomMargin="248.0792" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-281895418" Tag="64" IconVisible="False" LeftMargin="30.0000" RightMargin="-45.0000" TopMargin="-4.5000" BottomMargin="-0.5000" FontSize="26" LabelText="5局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="30.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="1.1538" Y="0.5769" />
                <PreSize X="1.5769" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardBg" ActionTag="1281637357" Tag="69" IconVisible="False" LeftMargin="85.0000" RightMargin="-159.0000" TopMargin="-4.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="5.1923" Y="0.5769" />
                <PreSize X="3.8462" Y="1.1538" />
                <FileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_costroombg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardNum" ActionTag="152475109" Tag="70" IconVisible="False" LeftMargin="104.0000" RightMargin="-140.0000" TopMargin="-0.5000" BottomMargin="3.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="5.1923" Y="0.5769" />
                <PreSize X="2.3846" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="368.5995" Y="261.0792" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2763" Y="0.3481" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayCountCb_2" ActionTag="80699808" Tag="65" IconVisible="False" LeftMargin="571.4465" RightMargin="736.5535" TopMargin="475.9208" BottomMargin="248.0792" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-1277631624" Tag="66" IconVisible="False" LeftMargin="30.0000" RightMargin="-59.0000" TopMargin="-4.5000" BottomMargin="-0.5000" FontSize="26" LabelText="10局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="30.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="1.1538" Y="0.5769" />
                <PreSize X="2.1154" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardBg" ActionTag="-1010384068" Tag="71" IconVisible="False" LeftMargin="85.0000" RightMargin="-159.0000" TopMargin="-4.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="5.1923" Y="0.5769" />
                <PreSize X="3.8462" Y="1.1538" />
                <FileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_costroombg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardNum" ActionTag="-1075714462" Tag="72" IconVisible="False" LeftMargin="104.0000" RightMargin="-140.0000" TopMargin="-0.5000" BottomMargin="3.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="5.1923" Y="0.5769" />
                <PreSize X="2.3846" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.4998" />
            <Position X="584.4465" Y="261.0740" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4381" Y="0.3481" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayCountCb_3" ActionTag="-1188862413" Tag="67" IconVisible="False" LeftMargin="787.2935" RightMargin="520.7065" TopMargin="475.9208" BottomMargin="248.0792" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-28058141" Tag="68" IconVisible="False" LeftMargin="30.0000" RightMargin="-59.0000" TopMargin="-4.5000" BottomMargin="-0.5000" FontSize="26" LabelText="15局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="30.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="105" B="20" />
                <PrePosition X="1.1538" Y="0.5769" />
                <PreSize X="2.1154" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardBg" ActionTag="2119597226" Tag="73" IconVisible="False" LeftMargin="85.0000" RightMargin="-159.0000" TopMargin="-4.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="5.1923" Y="0.5769" />
                <PreSize X="3.8462" Y="1.1538" />
                <FileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_costroombg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="CardNum" ActionTag="-169941585" Tag="74" IconVisible="False" LeftMargin="104.0000" RightMargin="-140.0000" TopMargin="-0.5000" BottomMargin="3.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="5.1923" Y="0.5769" />
                <PreSize X="2.3846" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="800.2935" Y="261.0792" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5999" Y="0.3481" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="pri_pic/pri_510k_wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="SpTips" ActionTag="-1361686521" Tag="115" IconVisible="False" LeftMargin="519.5000" RightMargin="519.5000" TopMargin="600.6752" BottomMargin="118.3248" LeftEage="97" RightEage="97" TopEage="10" BottomEage="10" Scale9OriginX="97" Scale9OriginY="10" Scale9Width="101" Scale9Height="11" ctype="ImageViewObjectData">
            <Size X="295.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="133.8248" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1784" />
            <PreSize X="0.2211" Y="0.0413" />
            <FileData Type="Normal" Path="room/pri510k_sp_card_tips.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BigSmallBloodTitle" ActionTag="586486930" Tag="31" IconVisible="False" LeftMargin="233.3901" RightMargin="996.6099" TopMargin="400.3075" BottomMargin="318.6925" FontSize="26" LabelText="大小血：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="104.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="285.3901" Y="334.1925" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="139" G="105" B="20" />
            <PrePosition X="0.2139" Y="0.4456" />
            <PreSize X="0.0780" Y="0.0413" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>