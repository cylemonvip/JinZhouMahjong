<GameFile>
  <PropertyGroup Name="PrivateGameEndLayer" Type="Layer" ID="3e72844a-3c30-4a64-b342-054ae059ae44" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="695348063" Tag="70" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="229" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_bg" ActionTag="-597713316" Tag="97" IconVisible="False" LeftMargin="52.3756" RightMargin="51.6244" TopMargin="92.5255" BottomMargin="107.4745" Scale9Enable="True" LeftEage="348" RightEage="348" TopEage="162" BottomEage="162" Scale9OriginX="348" Scale9OriginY="162" Scale9Width="361" Scale9Height="168" ctype="ImageViewObjectData">
            <Size X="1230.0000" Y="550.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_9" ActionTag="-1126258090" Tag="192" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="466.5000" RightMargin="466.5000" TopMargin="-27.5000" BottomMargin="522.5000" ctype="SpriteObjectData">
                <Size X="297.0000" Y="55.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="615.0000" Y="550.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="0.2415" Y="0.1000" />
                <FileData Type="Normal" Path="pri_end_res/game_iamge_end_title_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_8" ActionTag="1100926723" Tag="191" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="527.5000" RightMargin="527.5000" TopMargin="-29.5150" BottomMargin="528.5150" ctype="SpriteObjectData">
                <Size X="175.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="615.0000" Y="554.0150" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0073" />
                <PreSize X="0.1423" Y="0.0927" />
                <FileData Type="Normal" Path="pri_end_res/gameend_account_image_title.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_share" Tag="99" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="563.5010" RightMargin="563.4990" TopMargin="531.4237" BottomMargin="-21.4237" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="73" Scale9Height="18" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="103.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="615.0010" Y="-1.4237" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.0026" />
                <PreSize X="0.0837" Y="0.0727" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="pri_end_res/game_end_btn_share_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="pri_end_res/game_end_btn_share_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="pri_end_res/game_end_btn_share.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_quit" Tag="100" IconVisible="False" LeftMargin="1179.2808" RightMargin="-21.2808" TopMargin="-34.1699" BottomMargin="510.1699" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="42" Scale9Height="51" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="72.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1215.2808" Y="547.1699" />
                <Scale ScaleX="1.3000" ScaleY="1.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9880" Y="0.9949" />
                <PreSize X="0.0585" Y="0.1345" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="pri_end_res/gameend_account_btn_exit_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="pri_end_res/gameend_account_btn_exit_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="pri_end_res/gameend_account_btn_exit.png" Plist="" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.3756" Y="382.4745" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5003" Y="0.5100" />
            <PreSize X="0.9220" Y="0.7333" />
            <FileData Type="Normal" Path="pri_end_res/game_image_gameend_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="UserNode_1" ActionTag="-1350180335" Tag="116" IconVisible="False" LeftMargin="98.0591" RightMargin="989.9409" TopMargin="181.2675" BottomMargin="194.7325" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="81" RightEage="81" TopEage="123" BottomEage="123" Scale9OriginX="81" Scale9OriginY="123" Scale9Width="84" Scale9Height="128" ctype="PanelObjectData">
            <Size X="246.0000" Y="374.0000" />
            <Children>
              <AbstractNodeData Name="HostFlagImg" ActionTag="-918220102" Tag="70" IconVisible="False" LeftMargin="168.5000" RightMargin="4.5000" TopMargin="2.5000" BottomMargin="338.5000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="205.0000" Y="355.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8333" Y="0.9492" />
                <PreSize X="0.2967" Y="0.0882" />
                <FileData Type="Normal" Path="pri_end_res/gameend_account_bank.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadNode" ActionTag="1166828377" Tag="117" IconVisible="True" LeftMargin="50.4889" RightMargin="195.5111" TopMargin="50.8643" BottomMargin="323.1357" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="50.4889" Y="323.1357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2052" Y="0.8640" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="1063354799" Tag="118" IconVisible="False" LeftMargin="104.2071" RightMargin="21.7929" TopMargin="41.7739" BottomMargin="309.2261" FontSize="20" LabelText="名称可能很长" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2071" Y="320.7261" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4236" Y="0.8576" />
                <PreSize X="0.4878" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDTxt" ActionTag="-1305236858" Tag="119" IconVisible="False" LeftMargin="104.2071" RightMargin="43.7929" TopMargin="71.2808" BottomMargin="279.7192" FontSize="20" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2071" Y="291.2192" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.4236" Y="0.7787" />
                <PreSize X="0.3984" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleHuangShangCountTxt" ActionTag="615689991" Tag="120" IconVisible="False" LeftMargin="29.6361" RightMargin="116.3639" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="皇上次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6361" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.6484" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleNiangNiangCountTxt" ActionTag="1081135125" Tag="121" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="娘娘次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.5345" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleBigXueCountTxt" ActionTag="1524737729" Tag="122" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="大血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.4205" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleSmallXueCountTxt" ActionTag="-964841315" Tag="123" IconVisible="False" LeftMargin="29.6359" RightMargin="116.3641" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="小血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6359" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.3065" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HNFlag" ActionTag="-460009559" Tag="124" IconVisible="False" LeftMargin="148.0000" RightMargin="-10.0000" TopMargin="277.0000" BottomMargin="7.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="202.0000" Y="52.0000" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8211" Y="0.1390" />
                <PreSize X="0.4390" Y="0.2406" />
                <FileData Type="Normal" Path="pri_end_res/bg_label_hsjd.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HuangShangCountTxt" ActionTag="612256255" Tag="125" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.6484" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NiangNiangCountTxt" ActionTag="1130441899" Tag="126" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.5345" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigXueCountTxt" ActionTag="863803846" Tag="127" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.4205" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SmallXueCountTxt" ActionTag="-127515284" Tag="128" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.3065" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleTotalScore" ActionTag="1606078648" Tag="129" IconVisible="False" LeftMargin="91.4989" RightMargin="94.5011" TopMargin="292.5727" BottomMargin="58.4273" FontSize="20" LabelText="总结算" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="69.9273" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.1870" />
                <PreSize X="0.2439" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TotalScore" ActionTag="-1147581801" Tag="130" IconVisible="False" LeftMargin="115.4989" RightMargin="118.5011" TopMargin="328.5437" BottomMargin="22.4563" FontSize="20" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="33.9563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.0908" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="221.0591" Y="381.7325" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1657" Y="0.5090" />
            <PreSize X="0.1844" Y="0.4987" />
            <FileData Type="Normal" Path="pri_end_res/gameend_account_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="UserNode_2" ActionTag="-1028417585" Tag="131" IconVisible="False" LeftMargin="396.4447" RightMargin="691.5553" TopMargin="181.2676" BottomMargin="194.7325" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="81" RightEage="81" TopEage="123" BottomEage="123" Scale9OriginX="81" Scale9OriginY="123" Scale9Width="84" Scale9Height="128" ctype="PanelObjectData">
            <Size X="246.0000" Y="374.0000" />
            <Children>
              <AbstractNodeData Name="HostFlagImg" ActionTag="-815479601" Tag="71" IconVisible="False" LeftMargin="168.5000" RightMargin="4.5000" TopMargin="2.5000" BottomMargin="338.5000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="205.0000" Y="355.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8333" Y="0.9492" />
                <PreSize X="0.2967" Y="0.0882" />
                <FileData Type="Normal" Path="pri_end_res/gameend_account_bank.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadNode" ActionTag="246534692" Tag="132" IconVisible="True" LeftMargin="50.4889" RightMargin="195.5111" TopMargin="50.8643" BottomMargin="323.1357" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="50.4889" Y="323.1357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2052" Y="0.8640" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="734884302" Tag="133" IconVisible="False" LeftMargin="104.2072" RightMargin="21.7928" TopMargin="41.7739" BottomMargin="309.2261" FontSize="20" LabelText="名称可能很长" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2072" Y="320.7261" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4236" Y="0.8576" />
                <PreSize X="0.4878" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDTxt" ActionTag="608257078" Tag="134" IconVisible="False" LeftMargin="104.2072" RightMargin="43.7928" TopMargin="71.2809" BottomMargin="279.7191" FontSize="20" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2072" Y="291.2191" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.4236" Y="0.7787" />
                <PreSize X="0.3984" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleHuangShangCountTxt" ActionTag="323851796" Tag="135" IconVisible="False" LeftMargin="29.6361" RightMargin="116.3639" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="皇上次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6361" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.6484" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleNiangNiangCountTxt" ActionTag="-1568486997" Tag="136" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="娘娘次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.5345" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleBigXueCountTxt" ActionTag="498930265" Tag="137" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="大血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.4205" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleSmallXueCountTxt" ActionTag="-1120861341" Tag="138" IconVisible="False" LeftMargin="29.6359" RightMargin="116.3641" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="小血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6359" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.3065" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HNFlag" ActionTag="1981716463" Tag="139" IconVisible="False" LeftMargin="148.0000" RightMargin="-10.0000" TopMargin="277.0000" BottomMargin="7.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="202.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8211" Y="0.1390" />
                <PreSize X="0.4390" Y="0.2406" />
                <FileData Type="Normal" Path="pri_end_res/bg_label_hsjd.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HuangShangCountTxt" ActionTag="-2007886573" Tag="140" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.6484" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NiangNiangCountTxt" ActionTag="-2041663603" Tag="141" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.5345" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigXueCountTxt" ActionTag="-747392137" Tag="142" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.4205" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SmallXueCountTxt" ActionTag="701639658" Tag="143" IconVisible="False" LeftMargin="143.0659" RightMargin="90.9341" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0659" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.3065" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleTotalScore" ActionTag="1545921976" Tag="144" IconVisible="False" LeftMargin="91.4989" RightMargin="94.5011" TopMargin="292.5727" BottomMargin="58.4273" FontSize="20" LabelText="总结算" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="69.9273" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.1870" />
                <PreSize X="0.2439" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TotalScore" ActionTag="-2023197509" Tag="145" IconVisible="False" LeftMargin="115.4989" RightMargin="118.5011" TopMargin="328.5437" BottomMargin="22.4563" FontSize="20" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="33.9563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.0908" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="519.4447" Y="381.7325" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3894" Y="0.5090" />
            <PreSize X="0.1844" Y="0.4987" />
            <FileData Type="Normal" Path="pri_end_res/gameend_account_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="UserNode_3" ActionTag="-1015621557" Tag="146" IconVisible="False" LeftMargin="694.8297" RightMargin="393.1703" TopMargin="181.2676" BottomMargin="194.7325" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="81" RightEage="81" TopEage="123" BottomEage="123" Scale9OriginX="81" Scale9OriginY="123" Scale9Width="84" Scale9Height="128" ctype="PanelObjectData">
            <Size X="246.0000" Y="374.0000" />
            <Children>
              <AbstractNodeData Name="HostFlagImg" ActionTag="37979475" Tag="72" IconVisible="False" LeftMargin="168.5000" RightMargin="4.5000" TopMargin="2.5000" BottomMargin="338.5000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="205.0000" Y="355.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8333" Y="0.9492" />
                <PreSize X="0.2967" Y="0.0882" />
                <FileData Type="Normal" Path="pri_end_res/gameend_account_bank.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadNode" ActionTag="1747134026" Tag="147" IconVisible="True" LeftMargin="50.4889" RightMargin="195.5111" TopMargin="50.8643" BottomMargin="323.1357" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="50.4889" Y="323.1357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2052" Y="0.8640" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="-93225661" Tag="148" IconVisible="False" LeftMargin="104.2071" RightMargin="21.7929" TopMargin="41.7739" BottomMargin="309.2261" FontSize="20" LabelText="名称可能很长" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2071" Y="320.7261" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4236" Y="0.8576" />
                <PreSize X="0.4878" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDTxt" ActionTag="1902002651" Tag="149" IconVisible="False" LeftMargin="104.2072" RightMargin="43.7928" TopMargin="71.2809" BottomMargin="279.7191" FontSize="20" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2072" Y="291.2191" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.4236" Y="0.7787" />
                <PreSize X="0.3984" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleHuangShangCountTxt" ActionTag="-1577516218" Tag="150" IconVisible="False" LeftMargin="29.6362" RightMargin="116.3638" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="皇上次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6362" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.6484" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleNiangNiangCountTxt" ActionTag="-1269684203" Tag="151" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="娘娘次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.5345" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleBigXueCountTxt" ActionTag="724708108" Tag="152" IconVisible="False" LeftMargin="29.6363" RightMargin="116.3637" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="大血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6363" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.4205" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleSmallXueCountTxt" ActionTag="-1453832438" Tag="153" IconVisible="False" LeftMargin="29.6360" RightMargin="116.3640" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="小血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6360" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.3065" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HNFlag" ActionTag="1062072536" Tag="154" IconVisible="False" LeftMargin="148.0000" RightMargin="-10.0000" TopMargin="277.0000" BottomMargin="7.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="202.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8211" Y="0.1390" />
                <PreSize X="0.4390" Y="0.2406" />
                <FileData Type="Normal" Path="pri_end_res/bg_label_hsjd.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HuangShangCountTxt" ActionTag="-20437255" Tag="155" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.6484" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NiangNiangCountTxt" ActionTag="1542079892" Tag="156" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.5345" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigXueCountTxt" ActionTag="-1160422837" Tag="157" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.4205" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SmallXueCountTxt" ActionTag="1311426872" Tag="158" IconVisible="False" LeftMargin="143.0660" RightMargin="90.9340" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0660" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.3065" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleTotalScore" ActionTag="137491015" Tag="159" IconVisible="False" LeftMargin="91.4989" RightMargin="94.5011" TopMargin="292.5727" BottomMargin="58.4273" FontSize="20" LabelText="总结算" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="69.9273" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.1870" />
                <PreSize X="0.2439" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TotalScore" ActionTag="-1951204949" Tag="160" IconVisible="False" LeftMargin="115.4989" RightMargin="118.5011" TopMargin="328.5437" BottomMargin="22.4563" FontSize="20" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="33.9563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.0908" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="817.8297" Y="381.7325" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6131" Y="0.5090" />
            <PreSize X="0.1844" Y="0.4987" />
            <FileData Type="Normal" Path="pri_end_res/gameend_account_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="UserNode_4" ActionTag="698601202" Tag="161" IconVisible="False" LeftMargin="993.2157" RightMargin="94.7843" TopMargin="181.2676" BottomMargin="194.7325" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="81" RightEage="81" TopEage="123" BottomEage="123" Scale9OriginX="81" Scale9OriginY="123" Scale9Width="84" Scale9Height="128" ctype="PanelObjectData">
            <Size X="246.0000" Y="374.0000" />
            <Children>
              <AbstractNodeData Name="HostFlagImg" ActionTag="562374953" Tag="73" IconVisible="False" LeftMargin="168.5000" RightMargin="4.5000" TopMargin="2.5000" BottomMargin="338.5000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="205.0000" Y="355.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8333" Y="0.9492" />
                <PreSize X="0.2967" Y="0.0882" />
                <FileData Type="Normal" Path="pri_end_res/gameend_account_bank.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadNode" ActionTag="-1153824885" Tag="162" IconVisible="True" LeftMargin="50.4889" RightMargin="195.5111" TopMargin="50.8643" BottomMargin="323.1357" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="50.4889" Y="323.1357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2052" Y="0.8640" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameTxt" ActionTag="1109788457" Tag="163" IconVisible="False" LeftMargin="104.2068" RightMargin="21.7932" TopMargin="41.7739" BottomMargin="309.2261" FontSize="20" LabelText="名称可能很长" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2068" Y="320.7261" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4236" Y="0.8576" />
                <PreSize X="0.4878" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDTxt" ActionTag="893313042" Tag="164" IconVisible="False" LeftMargin="104.2068" RightMargin="43.7932" TopMargin="71.2809" BottomMargin="279.7191" FontSize="20" LabelText="ID: 123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="98.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="104.2068" Y="291.2191" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.4236" Y="0.7787" />
                <PreSize X="0.3984" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleHuangShangCountTxt" ActionTag="464971477" Tag="165" IconVisible="False" LeftMargin="29.6362" RightMargin="116.3638" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="皇上次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6362" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.6484" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleNiangNiangCountTxt" ActionTag="636219964" Tag="166" IconVisible="False" LeftMargin="29.6364" RightMargin="116.3636" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="娘娘次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6364" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.5345" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleBigXueCountTxt" ActionTag="1830147015" Tag="167" IconVisible="False" LeftMargin="29.6364" RightMargin="116.3636" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="大血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6364" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.4205" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleSmallXueCountTxt" ActionTag="1138096701" Tag="168" IconVisible="False" LeftMargin="29.6360" RightMargin="116.3640" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="小血次数：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="29.6360" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1205" Y="0.3065" />
                <PreSize X="0.4065" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="HNFlag" ActionTag="-1948334669" Tag="169" IconVisible="False" LeftMargin="148.0000" RightMargin="-10.0000" TopMargin="277.0000" BottomMargin="7.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="202.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8211" Y="0.1390" />
                <PreSize X="0.4390" Y="0.2406" />
                <FileData Type="Normal" Path="pri_end_res/bg_label_hsjd.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HuangShangCountTxt" ActionTag="-220573419" Tag="170" IconVisible="False" LeftMargin="143.0661" RightMargin="90.9339" TopMargin="119.9843" BottomMargin="231.0157" FontSize="20" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0661" Y="242.5157" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.6484" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NiangNiangCountTxt" ActionTag="-162324270" Tag="171" IconVisible="False" LeftMargin="143.0661" RightMargin="90.9339" TopMargin="162.6117" BottomMargin="188.3883" FontSize="20" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0661" Y="199.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.5345" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BigXueCountTxt" ActionTag="-1456007979" Tag="172" IconVisible="False" LeftMargin="143.0661" RightMargin="90.9339" TopMargin="205.2392" BottomMargin="145.7608" FontSize="20" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0661" Y="157.2608" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.4205" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SmallXueCountTxt" ActionTag="-1769791056" Tag="173" IconVisible="False" LeftMargin="143.0661" RightMargin="90.9339" TopMargin="247.8666" BottomMargin="103.1334" FontSize="20" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0661" Y="114.6334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5816" Y="0.3065" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TitleTotalScore" ActionTag="1948233499" Tag="174" IconVisible="False" LeftMargin="91.4989" RightMargin="94.5011" TopMargin="292.5727" BottomMargin="58.4273" FontSize="20" LabelText="总结算" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="69.9273" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.1870" />
                <PreSize X="0.2439" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TotalScore" ActionTag="-1959853446" Tag="175" IconVisible="False" LeftMargin="115.4989" RightMargin="118.5011" TopMargin="328.5437" BottomMargin="22.4563" FontSize="20" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="12.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.4989" Y="33.9563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="128" B="0" />
                <PrePosition X="0.4939" Y="0.0908" />
                <PreSize X="0.0488" Y="0.0615" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1116.2157" Y="381.7325" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8367" Y="0.5090" />
            <PreSize X="0.1844" Y="0.4987" />
            <FileData Type="Normal" Path="pri_end_res/gameend_account_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimeTxt" ActionTag="998930816" Tag="69" IconVisible="False" LeftMargin="998.8599" RightMargin="83.1401" TopMargin="599.6893" BottomMargin="121.3107" FontSize="25" LabelText="日期:2018.03.19 21:32" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="252.0000" Y="29.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1250.8599" Y="135.8107" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9377" Y="0.1811" />
            <PreSize X="0.1889" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>