<GameFile>
  <PropertyGroup Name="PrivateGameLayer" Type="Layer" ID="8249b305-deb5-4393-b983-37bb3afb4b80" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Image_bg" Tag="73" IconVisible="False" LeftMargin="-1.1650" RightMargin="1010.1650" TopMargin="2.7750" BottomMargin="617.2250" LeftEage="107" RightEage="107" TopEage="42" BottomEage="42" Scale9OriginX="107" Scale9OriginY="42" Scale9Width="111" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="325.0000" Y="130.0000" />
            <Children>
              <AbstractNodeData Name="room_id" Tag="81" IconVisible="False" LeftMargin="102.7800" RightMargin="120.2200" TopMargin="14.1700" BottomMargin="93.8300" CharWidth="17" CharHeight="22" LabelText="999999" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="102.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="153.7800" Y="104.8300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4732" Y="0.8064" />
                <PreSize X="0.3138" Y="0.1692" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/roomId_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="curRoundNum" ActionTag="-709008907" Tag="70" IconVisible="False" LeftMargin="126.4347" RightMargin="184.5653" TopMargin="42.8946" BottomMargin="58.1054" FontSize="25" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="133.4347" Y="72.6054" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4106" Y="0.5585" />
                <PreSize X="0.0431" Y="0.2231" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalRoundNum" ActionTag="1539506222" Tag="102" IconVisible="False" LeftMargin="163.6661" RightMargin="147.3339" TopMargin="42.8946" BottomMargin="58.1054" FontSize="25" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="170.6661" Y="72.6054" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5251" Y="0.5585" />
                <PreSize X="0.0431" Y="0.2231" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="img_sprit" Tag="31" IconVisible="False" LeftMargin="147.3677" RightMargin="167.6323" TopMargin="50.4032" BottomMargin="62.5968" ctype="SpriteObjectData">
                <Size X="10.0000" Y="17.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.3677" Y="71.0968" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4688" Y="0.5469" />
                <PreSize X="0.0308" Y="0.1308" />
                <FileData Type="Normal" Path="game/img_sprit.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="OtherScoreTxt" ActionTag="-872471852" Tag="44" IconVisible="False" LeftMargin="126.4347" RightMargin="184.5653" TopMargin="95.8776" BottomMargin="5.1224" FontSize="25" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="133.4347" Y="19.6224" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4106" Y="0.1509" />
                <PreSize X="0.0431" Y="0.2231" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="SelfScoreTxt" ActionTag="-1845768615" Tag="45" IconVisible="False" LeftMargin="126.4347" RightMargin="184.5653" TopMargin="69.5002" BottomMargin="31.4998" FontSize="25" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="133.4347" Y="45.9998" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4106" Y="0.3538" />
                <PreSize X="0.0431" Y="0.2231" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="DianChiLayout" ActionTag="-1628686632" Tag="49" IconVisible="False" LeftMargin="245.0272" RightMargin="29.9728" TopMargin="7.1143" BottomMargin="102.8857" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="50.0000" Y="20.0000" />
                <Children>
                  <AbstractNodeData Name="ValueLayout" ActionTag="-460408571" Tag="50" IconVisible="False" LeftMargin="1.0000" RightMargin="5.0000" TopMargin="1.0000" BottomMargin="1.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="4" BottomEage="4" Scale9OriginX="6" Scale9OriginY="4" Scale9Width="7" Scale9Height="5" ctype="PanelObjectData">
                    <Size X="44.0000" Y="18.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="1.0000" Y="10.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0200" Y="0.5000" />
                    <PreSize X="0.8800" Y="0.9000" />
                    <FileData Type="Normal" Path="game/pj_dianliang.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="SpDianLiangBg" ActionTag="-355477026" Tag="51" IconVisible="False" TopMargin="-0.5000" BottomMargin="-0.5000" ctype="SpriteObjectData">
                    <Size X="50.0000" Y="21.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="10.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0000" Y="1.0500" />
                    <FileData Type="Normal" Path="game/pj_dichi.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="245.0272" Y="112.8857" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7539" Y="0.8684" />
                <PreSize X="0.1538" Y="0.1538" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="TimeText" ActionTag="1339667576" Tag="52" IconVisible="False" LeftMargin="244.0900" RightMargin="29.9100" TopMargin="26.9275" BottomMargin="80.0725" FontSize="20" LabelText="00:00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="51.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="269.5900" Y="91.5725" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8295" Y="0.7044" />
                <PreSize X="0.1569" Y="0.1769" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="161.3350" Y="747.2250" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1209" Y="0.9963" />
            <PreSize X="0.2436" Y="0.1733" />
            <FileData Type="Normal" Path="game/left_bg_pri.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_invite" Tag="76" IconVisible="False" LeftMargin="469.0000" RightMargin="469.0000" TopMargin="330.5000" BottomMargin="330.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="366" Scale9Height="67" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="396.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.2969" Y="0.1187" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="game/bt_invite_0.png" Plist="" />
            <NormalFileData Type="Normal" Path="game/bt_invite_0.png" Plist="" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>