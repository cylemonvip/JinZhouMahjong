<GameFile>
  <PropertyGroup Name="RoomResultLayer" Type="Scene" ID="283709be-02a2-4d04-bd17-d2f2c4d92047" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="65" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-143788950" Tag="749" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_roomResult_bg" ActionTag="1782618034" Tag="66" IconVisible="False" LeftEage="264" RightEage="264" TopEage="158" BottomEage="158" Scale9OriginX="264" Scale9OriginY="158" Scale9Width="272" Scale9Height="164" ctype="ImageViewObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="plist/game_over_bg1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_leaveRoom" ActionTag="700991358" Tag="69" IconVisible="False" LeftMargin="1264.0477" RightMargin="28.9523" TopMargin="20.5041" BottomMargin="688.4959" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="11" Scale9Height="19" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="41.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1284.5477" Y="708.9959" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9629" Y="0.9453" />
            <PreSize X="0.0307" Y="0.0547" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="plist/back1.png" Plist="" />
            <PressedFileData Type="Normal" Path="plist/back1.png" Plist="" />
            <NormalFileData Type="Normal" Path="plist/back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_share" ActionTag="1873516002" Tag="70" IconVisible="False" LeftMargin="599.4999" RightMargin="599.5001" TopMargin="680.5001" BottomMargin="20.4999" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="105" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="135.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="666.9999" Y="44.9999" />
            <Scale ScaleX="1.6600" ScaleY="1.6600" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0600" />
            <PreSize X="0.1012" Y="0.0653" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="plist/share2.png" Plist="" />
            <PressedFileData Type="Normal" Path="plist/share2.png" Plist="" />
            <NormalFileData Type="Normal" Path="plist/share1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="-1084553291" Tag="73" IconVisible="True" LeftMargin="180.0000" RightMargin="1154.0000" TopMargin="369.9996" BottomMargin="380.0004" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="180.0000" Y="380.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1349" Y="0.5067" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserDataForHLD.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="2085352986" Tag="132" IconVisible="True" LeftMargin="505.0001" RightMargin="828.9999" TopMargin="368.9998" BottomMargin="381.0002" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="505.0001" Y="381.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3786" Y="0.5080" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserDataForHLD.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3" ActionTag="1742288563" Tag="145" IconVisible="True" LeftMargin="829.0001" RightMargin="504.9999" TopMargin="368.9998" BottomMargin="381.0002" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="829.0001" Y="381.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6214" Y="0.5080" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserDataForHLD.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_4" ActionTag="-1629111249" Tag="158" IconVisible="True" LeftMargin="1152.0000" RightMargin="182.0000" TopMargin="368.9998" BottomMargin="381.0002" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1152.0000" Y="381.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8636" Y="0.5080" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserDataForHLD.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimeTxt" ActionTag="1178491890" Tag="80" IconVisible="False" LeftMargin="1130.9957" RightMargin="5.0043" TopMargin="719.6103" BottomMargin="4.3897" FontSize="20" LabelText="日期: 2018.08.08 18:36" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="198.0000" Y="26.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1328.9957" Y="17.3897" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9962" Y="0.0232" />
            <PreSize X="0.1484" Y="0.0347" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>