<GameFile>
  <PropertyGroup Name="GameRoleItem" Type="Node" ID="417134c0-eb7f-4532-a80d-994d7b9909df" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="73" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="head_frame" ActionTag="750210547" Tag="172" IconVisible="False" LeftMargin="-30.0000" RightMargin="-30.0000" TopMargin="-30.0000" BottomMargin="-30.0000" LeftEage="19" RightEage="19" TopEage="19" BottomEage="19" Scale9OriginX="19" Scale9OriginY="19" Scale9Width="22" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="60.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bg" ActionTag="-988209702" Tag="71" IconVisible="False" LeftMargin="-261.0000" RightMargin="93.0000" TopMargin="-69.0000" BottomMargin="31.0000" Scale9Enable="True" LeftEage="20" RightEage="20" TopEage="13" BottomEage="13" Scale9OriginX="20" Scale9OriginY="13" Scale9Width="130" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="168.0000" Y="38.0000" />
            <Children>
              <AbstractNodeData Name="chat_face" ActionTag="1698780539" Tag="72" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="54.0000" RightMargin="54.0000" TopMargin="-14.8000" BottomMargin="-7.2000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="84.0000" Y="22.8000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6000" />
                <PreSize X="0.3571" Y="1.5789" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="-93.0000" Y="50.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="game_chat_0.png" Plist="game/game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_bg" ActionTag="211112326" Tag="43" IconVisible="False" LeftMargin="-56.0000" RightMargin="-56.0000" TopMargin="24.0000" BottomMargin="-80.0000" ctype="SpriteObjectData">
            <Size X="112.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-52.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="role_head_bg.png" Plist="game/game.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>