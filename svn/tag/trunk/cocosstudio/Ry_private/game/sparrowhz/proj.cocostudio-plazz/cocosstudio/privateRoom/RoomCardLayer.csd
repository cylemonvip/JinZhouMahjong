<GameFile>
  <PropertyGroup Name="RoomCardLayer" Type="Scene" ID="8cf7ce17-31cc-40dc-99df-ba76820ac081" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="4" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="-1799895735" Tag="217" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="440" RightEage="440" TopEage="247" BottomEage="247" Scale9OriginX="440" Scale9OriginY="247" Scale9Width="454" Scale9Height="256" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="ditu.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_JuShu_1" ActionTag="1157371440" Tag="70" IconVisible="False" LeftMargin="368.2360" RightMargin="906.7640" TopMargin="164.8103" BottomMargin="526.1897" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="397.7360" Y="555.6897" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2982" Y="0.7409" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_JuShu_2" ActionTag="1479050148" Tag="9" IconVisible="False" LeftMargin="640.7633" RightMargin="634.2367" TopMargin="164.8103" BottomMargin="526.1897" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="670.2633" Y="555.6897" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5024" Y="0.7409" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_JuShu_3" ActionTag="303849819" Tag="10" IconVisible="False" LeftMargin="914.9808" RightMargin="360.0192" TopMargin="164.8103" BottomMargin="526.1897" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="944.4808" Y="555.6897" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7080" Y="0.7409" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_4ren" ActionTag="1366292066" VisibleForFrame="False" Tag="11" IconVisible="False" LeftMargin="432.4921" RightMargin="817.5079" TopMargin="270.2747" BottomMargin="432.7253" ctype="SpriteObjectData">
            <Size X="84.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="474.4921" Y="456.2253" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3557" Y="0.6083" />
            <PreSize X="0.0630" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_siren.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_3ren" ActionTag="507391268" VisibleForFrame="False" Tag="12" IconVisible="False" LeftMargin="698.5242" RightMargin="543.4758" TopMargin="270.2747" BottomMargin="432.7253" ctype="SpriteObjectData">
            <Size X="92.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="744.5242" Y="456.2253" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5581" Y="0.6083" />
            <PreSize X="0.0690" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_sanren.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_RenShu_1" ActionTag="1042196030" Tag="13" IconVisible="False" LeftMargin="368.2360" RightMargin="906.7640" TopMargin="264.2747" BottomMargin="426.7253" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="397.7360" Y="456.2253" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2982" Y="0.6083" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_RenShu_2" ActionTag="698248547" Tag="14" IconVisible="False" LeftMargin="640.7633" RightMargin="634.2367" TopMargin="264.2747" BottomMargin="426.7253" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="670.2633" Y="456.2253" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5024" Y="0.6083" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_HuiPai" ActionTag="1377459124" VisibleForFrame="False" Tag="15" IconVisible="False" LeftMargin="425.8853" RightMargin="816.1147" TopMargin="369.3577" BottomMargin="333.6423" ctype="SpriteObjectData">
            <Size X="92.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="471.8853" Y="357.1423" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3537" Y="0.4762" />
            <PreSize X="0.0690" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_huipai.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_1" ActionTag="-719757651" Tag="16" IconVisible="False" LeftMargin="368.2360" RightMargin="906.7640" TopMargin="363.3577" BottomMargin="327.6423" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="397.7360" Y="357.1423" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2982" Y="0.4762" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_2" ActionTag="-1530266810" Tag="17" IconVisible="False" LeftMargin="587.9561" RightMargin="687.0439" TopMargin="363.3563" BottomMargin="327.6437" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5823" ScaleY="0.4012" />
            <Position X="622.3118" Y="351.3145" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4665" Y="0.4684" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_SanQing" ActionTag="94108590" VisibleForFrame="False" Tag="18" IconVisible="False" LeftMargin="647.5223" RightMargin="594.4777" TopMargin="369.3577" BottomMargin="333.6423" ctype="SpriteObjectData">
            <Size X="92.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="693.5223" Y="357.1423" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5199" Y="0.4762" />
            <PreSize X="0.0690" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_sanqing.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_QiongHu" ActionTag="1458167729" VisibleForFrame="False" Tag="19" IconVisible="False" LeftMargin="865.5411" RightMargin="302.4590" TopMargin="369.3577" BottomMargin="333.6423" ctype="SpriteObjectData">
            <Size X="166.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="948.5411" Y="357.1423" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7111" Y="0.4762" />
            <PreSize X="0.1244" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_qionghu.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_3" ActionTag="744135654" Tag="20" IconVisible="False" LeftMargin="1031.4327" RightMargin="243.5673" TopMargin="364.3444" BottomMargin="326.6556" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1060.9327" Y="356.1556" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7953" Y="0.4749" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_TianDiHu" ActionTag="1296118323" VisibleForFrame="False" Tag="21" IconVisible="False" LeftMargin="1085.7510" RightMargin="115.2490" TopMargin="370.3443" BottomMargin="332.6557" ctype="SpriteObjectData">
            <Size X="133.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1152.2510" Y="356.1557" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8638" Y="0.4749" />
            <PreSize X="0.0997" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_tiandi.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_4" ActionTag="1016970038" Tag="22" IconVisible="False" LeftMargin="810.4089" RightMargin="464.5911" TopMargin="364.3444" BottomMargin="326.6556" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="839.9089" Y="356.1556" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6296" Y="0.4749" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_FengDing" ActionTag="-241157759" VisibleForFrame="False" Tag="23" IconVisible="False" LeftMargin="857.0007" RightMargin="310.9993" TopMargin="459.4980" BottomMargin="243.5020" ctype="SpriteObjectData">
            <Size X="166.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="940.0007" Y="267.0020" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7046" Y="0.3560" />
            <PreSize X="0.1244" Y="0.0627" />
            <FileData Type="PlistSubImage" Path="cr_fengding.png" Plist="plist/creatroompics.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_fdybw" ActionTag="-1116826458" Tag="138" IconVisible="False" LeftMargin="868.9740" RightMargin="318.0260" TopMargin="460.4993" BottomMargin="244.5007" FontSize="40" LabelText="封顶150" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="147.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="868.9740" Y="267.0007" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.6514" Y="0.3560" />
            <PreSize X="0.1102" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_5" ActionTag="-1158803983" Tag="24" IconVisible="False" LeftMargin="810.4089" RightMargin="464.5911" TopMargin="453.2714" BottomMargin="237.7286" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="839.9089" Y="267.2286" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6296" Y="0.3563" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_6" ActionTag="-1408809665" Tag="93" IconVisible="False" LeftMargin="368.2360" RightMargin="906.7640" TopMargin="453.2714" BottomMargin="237.7286" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="397.7360" Y="267.2286" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2982" Y="0.3563" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_qd" ActionTag="-1538622087" Tag="94" IconVisible="False" LeftMargin="430.9514" RightMargin="823.0486" TopMargin="460.4993" BottomMargin="244.5007" FontSize="40" LabelText="七对" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="80.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="430.9514" Y="267.0007" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.3231" Y="0.3560" />
            <PreSize X="0.0600" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_CreatRoom" ActionTag="1451577220" Tag="25" IconVisible="False" LeftMargin="541.5000" RightMargin="541.5000" TopMargin="573.5070" BottomMargin="84.4930" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="221" Scale9Height="70" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="251.0000" Y="92.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="130.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1740" />
            <PreSize X="0.1882" Y="0.1227" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="cr_creatroom_p.png" Plist="plist/creatroompics.plist" />
            <PressedFileData Type="PlistSubImage" Path="cr_creatroom_p.png" Plist="plist/creatroompics.plist" />
            <NormalFileData Type="PlistSubImage" Path="cr_creatroom.png" Plist="plist/creatroompics.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_MyRoom" ActionTag="-1243900178" VisibleForFrame="False" Tag="26" IconVisible="False" LeftMargin="734.6952" RightMargin="343.3048" TopMargin="574.0070" BottomMargin="84.9930" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="226" Scale9Height="69" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="256.0000" Y="91.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="862.6952" Y="130.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6467" Y="0.1740" />
            <PreSize X="0.1919" Y="0.1213" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="cr_myroom_p.png" Plist="plist/creatroompics.plist" />
            <PressedFileData Type="PlistSubImage" Path="cr_myroom_p.png" Plist="plist/creatroompics.plist" />
            <NormalFileData Type="PlistSubImage" Path="cr_myroom.png" Plist="plist/creatroompics.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_Close" ActionTag="-215187022" Tag="25" IconVisible="False" LeftMargin="1254.7220" RightMargin="8.2780" TopMargin="48.8400" BottomMargin="631.1600" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="41" Scale9Height="48" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="71.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1290.2220" Y="666.1600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9672" Y="0.8882" />
            <PreSize X="0.0532" Y="0.0933" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="cr_close.png" Plist="plist/creatroompics.plist" />
            <PressedFileData Type="PlistSubImage" Path="cr_close.png" Plist="plist/creatroompics.plist" />
            <NormalFileData Type="PlistSubImage" Path="cr_close.png" Plist="plist/creatroompics.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_ChongZhi" ActionTag="1283950031" VisibleForFrame="False" Tag="36" IconVisible="False" LeftMargin="729.5000" RightMargin="489.5000" TopMargin="25.5000" BottomMargin="671.5000" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="85" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="115.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="787.0000" Y="698.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5900" Y="0.9307" />
            <PreSize X="0.0862" Y="0.0707" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="cr_chongzhi.png" Plist="plist/creatroompics.plist" />
            <PressedFileData Type="PlistSubImage" Path="cr_chongzhi.png" Plist="plist/creatroompics.plist" />
            <NormalFileData Type="PlistSubImage" Path="cr_chongzhi.png" Plist="plist/creatroompics.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_myRoomCardNum" ActionTag="-708188219" Tag="37" IconVisible="False" LeftMargin="626.2944" RightMargin="635.7056" TopMargin="27.2056" BottomMargin="673.7944" FontSize="40" LabelText="999" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="662.2944" Y="698.2944" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4965" Y="0.9311" />
            <PreSize X="0.0540" Y="0.0653" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_7" ActionTag="-1381022620" Tag="134" IconVisible="False" LeftMargin="587.9562" RightMargin="687.0438" TopMargin="453.2714" BottomMargin="237.7286" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="617.4562" Y="267.2286" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4629" Y="0.3563" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_qys" ActionTag="-2098695148" Tag="135" IconVisible="False" LeftMargin="647.9564" RightMargin="566.0436" TopMargin="460.4993" BottomMargin="244.5007" FontSize="40" LabelText="清一色" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="647.9564" Y="267.0007" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.4857" Y="0.3560" />
            <PreSize X="0.0900" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_8" ActionTag="-446987659" Tag="136" IconVisible="False" LeftMargin="1031.4327" RightMargin="243.5673" TopMargin="453.2726" BottomMargin="237.7274" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="59.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1060.9327" Y="267.2274" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7953" Y="0.3563" />
            <PreSize X="0.0442" Y="0.0787" />
            <NormalBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="cr_quan.png" Plist="plist/creatroompics.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="cr_quan_s.png" Plist="plist/creatroompics.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_fderb" ActionTag="-512133990" Tag="137" IconVisible="False" LeftMargin="1088.7310" RightMargin="98.2690" TopMargin="460.4993" BottomMargin="244.5007" FontSize="40" LabelText="封顶200" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="147.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1088.7310" Y="267.0007" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.8161" Y="0.3560" />
            <PreSize X="0.1102" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_tdh" ActionTag="1218649640" Tag="139" IconVisible="False" LeftMargin="868.9741" RightMargin="345.0259" TopMargin="375.1521" BottomMargin="329.8479" FontSize="40" LabelText="天地胡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="868.9741" Y="352.3479" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.6514" Y="0.4698" />
            <PreSize X="0.0900" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_qhjb" ActionTag="1180590825" Tag="140" IconVisible="False" LeftMargin="1088.7310" RightMargin="85.2690" TopMargin="375.1521" BottomMargin="329.8479" FontSize="40" LabelText="穷胡加倍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1088.7310" Y="352.3479" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.8161" Y="0.4698" />
            <PreSize X="0.1199" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_sq" ActionTag="1257413245" Tag="141" IconVisible="False" LeftMargin="647.9563" RightMargin="606.0437" TopMargin="375.1521" BottomMargin="329.8479" FontSize="40" LabelText="三清" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="80.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="647.9563" Y="352.3479" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.4857" Y="0.4698" />
            <PreSize X="0.0600" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_hp" ActionTag="-1625554026" Tag="142" IconVisible="False" LeftMargin="430.9514" RightMargin="823.0486" TopMargin="375.1521" BottomMargin="329.8479" FontSize="40" LabelText="会牌" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="80.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="430.9514" Y="352.3479" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.3231" Y="0.4698" />
            <PreSize X="0.0600" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_siren" ActionTag="801015102" Tag="143" IconVisible="False" LeftMargin="430.9514" RightMargin="823.0486" TopMargin="272.9562" BottomMargin="432.0438" FontSize="40" LabelText="四人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="80.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="430.9514" Y="454.5438" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.3231" Y="0.6061" />
            <PreSize X="0.0600" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_sanren" ActionTag="307289278" Tag="144" IconVisible="False" LeftMargin="700.7250" RightMargin="553.2750" TopMargin="272.9562" BottomMargin="432.0438" FontSize="40" LabelText="三人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="80.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="700.7250" Y="454.5438" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="59" B="6" />
            <PrePosition X="0.5253" Y="0.6061" />
            <PreSize X="0.0600" Y="0.0600" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>