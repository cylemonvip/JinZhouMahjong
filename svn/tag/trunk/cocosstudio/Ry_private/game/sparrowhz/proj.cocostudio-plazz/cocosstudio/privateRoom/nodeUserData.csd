<GameFile>
  <PropertyGroup Name="nodeUserData" Type="Node" ID="1ccb59f9-f3b4-46a4-96d9-f6db2eb04f2e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="71" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bgSp" ActionTag="819714919" Tag="236" IconVisible="False" LeftMargin="-123.0000" RightMargin="-123.0000" TopMargin="-154.0000" BottomMargin="-220.0000" ctype="SpriteObjectData">
            <Size X="246.0000" Y="374.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-33.0000" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="gameend_account_bg.png" Plist="plist/plazaScene.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="HeadNode" ActionTag="603900632" Tag="612" IconVisible="True" LeftMargin="-82.6100" RightMargin="82.6100" TopMargin="-124.0345" BottomMargin="124.0345" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-82.6100" Y="124.0345" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_account" ActionTag="-1674625780" Tag="209" IconVisible="False" LeftMargin="-25.9998" RightMargin="-73.0002" TopMargin="-122.8287" BottomMargin="94.8287" FontSize="24" LabelText="测试账号" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="99.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-25.9998" Y="108.8287" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="128" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_id" ActionTag="598437441" Tag="90" IconVisible="False" LeftMargin="-25.9998" RightMargin="-107.0002" TopMargin="-93.7883" BottomMargin="63.7883" FontSize="24" LabelText="ID：263598" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="133.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-25.9998" Y="78.7883" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="77" G="77" B="77" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_hu" ActionTag="-551613911" Tag="89" IconVisible="False" LeftMargin="-106.9991" RightMargin="-1.0009" TopMargin="-41.5002" BottomMargin="8.5002" FontSize="26" LabelText="胡牌次数" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9991" Y="25.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_huNum" ActionTag="1725115941" Tag="92" IconVisible="False" LeftMargin="34.0014" RightMargin="-52.0014" TopMargin="-41.5002" BottomMargin="8.5002" FontSize="26" LabelText="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="18.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0014" Y="25.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gongGang" ActionTag="-1625703066" Tag="86" IconVisible="False" LeftMargin="-106.9991" RightMargin="-1.0009" TopMargin="5.1665" BottomMargin="-38.1665" FontSize="26" LabelText="明杠次数" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9991" Y="-21.6665" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gongGangNum" ActionTag="-840589994" Tag="93" IconVisible="False" LeftMargin="34.0019" RightMargin="-52.0019" TopMargin="5.1665" BottomMargin="-38.1665" FontSize="26" LabelText="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="18.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0019" Y="-21.6665" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_anGang" ActionTag="1646956206" Tag="87" IconVisible="False" LeftMargin="-106.9991" RightMargin="-1.0009" TopMargin="51.8333" BottomMargin="-84.8333" FontSize="26" LabelText="暗杠次数" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9991" Y="-68.3333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_anGangNum" ActionTag="-1032092274" Tag="94" IconVisible="False" LeftMargin="33.5021" RightMargin="-52.5021" TopMargin="51.8333" BottomMargin="-84.8333" FontSize="26" LabelText="0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="19.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0021" Y="-68.3333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_ma" ActionTag="-68209943" Tag="88" IconVisible="False" LeftMargin="-106.9991" RightMargin="-1.0009" TopMargin="98.4999" BottomMargin="-131.4999" FontSize="26" LabelText="点炮次数" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.9991" Y="-114.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_maNum" ActionTag="-1087103918" Tag="95" IconVisible="False" LeftMargin="21.0009" RightMargin="-65.0009" TopMargin="98.4999" BottomMargin="-131.4999" FontSize="26" LabelText="100" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="44.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.0009" Y="-114.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gradeTotal" ActionTag="-1726386754" Tag="72" IconVisible="False" LeftMargin="-47.4999" RightMargin="-45.5001" TopMargin="158.4999" BottomMargin="-193.4999" FontSize="30" LabelText="总成绩" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="93.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.9999" Y="-175.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="128" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_gradeTotalNum" ActionTag="-251055276" Tag="91" IconVisible="False" LeftMargin="-42.0000" RightMargin="-40.0000" TopMargin="201.9118" BottomMargin="-236.9118" FontSize="30" LabelText="+1002" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="82.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0000" Y="-219.4118" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_roomHost" ActionTag="512729120" VisibleForFrame="False" Tag="211" IconVisible="False" LeftMargin="64.7536" RightMargin="-137.7536" TopMargin="-174.1844" BottomMargin="141.1844" ctype="SpriteObjectData">
            <Size X="73.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="101.2536" Y="157.6844" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="gameend_account_bank.png" Plist="plist/plazaScene.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_bigWinner" ActionTag="-1061795470" VisibleForFrame="False" Tag="213" IconVisible="False" LeftMargin="31.8352" RightMargin="-158.8352" TopMargin="126.4587" BottomMargin="-253.4587" ctype="SpriteObjectData">
            <Size X="127.0000" Y="127.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.3352" Y="-189.9587" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="sp_bigWinner.png" Plist="privateRoom/privateRoom.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>