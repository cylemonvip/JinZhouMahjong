<GameFile>
  <PropertyGroup Name="InputShareCodeNode" Type="Node" ID="fe30ae1c-d8d7-493a-b5cf-c18ccbde8616" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="70" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1528350461" Tag="71" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="ImageBg" ActionTag="396762535" Tag="73" IconVisible="False" LeftMargin="367.0000" RightMargin="367.0000" TopMargin="212.5000" BottomMargin="212.5000" LeftEage="143" RightEage="143" TopEage="81" BottomEage="81" Scale9OriginX="143" Scale9OriginY="81" Scale9Width="148" Scale9Height="84" ctype="ImageViewObjectData">
                <Size X="600.0000" Y="325.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4498" Y="0.4333" />
                <FileData Type="Normal" Path="KFMJ_grxx_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="EnterBtn" ActionTag="1886979830" Tag="74" IconVisible="False" LeftMargin="588.0814" RightMargin="589.9186" TopMargin="452.3433" BottomMargin="240.6567" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="126" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="156.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="666.0814" Y="269.1567" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4993" Y="0.3589" />
                <PreSize X="0.1169" Y="0.0760" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="enter_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="enter_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="enter.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ImgInputBg" ActionTag="1850179894" Tag="76" IconVisible="False" LeftMargin="422.0000" RightMargin="422.0000" TopMargin="306.0000" BottomMargin="386.0000" LeftEage="16" RightEage="16" TopEage="1" BottomEage="1" Scale9OriginX="16" Scale9OriginY="1" Scale9Width="458" Scale9Height="56" ctype="ImageViewObjectData">
                <Size X="490.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="415.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5533" />
                <PreSize X="0.3673" Y="0.0773" />
                <FileData Type="Normal" Path="room/recorddetail/guild_join_dt4.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="-1472997802" Tag="77" IconVisible="False" LeftMargin="916.8723" RightMargin="349.1277" TopMargin="195.4295" BottomMargin="486.5705" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="950.8723" Y="520.5705" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7128" Y="0.6941" />
                <PreSize X="0.0510" Y="0.0907" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza_btn_close_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza_btn_close_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza_btn_close_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Tips" ActionTag="592385513" Tag="43" IconVisible="False" LeftMargin="493.9999" RightMargin="494.0001" TopMargin="400.9183" BottomMargin="326.0817" FontSize="20" LabelText="您输入的分享码格式错误，请重新输入!" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="346.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="666.9999" Y="337.5817" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.5000" Y="0.4501" />
                <PreSize X="0.2594" Y="0.0307" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TextField" ActionTag="410938456" Tag="75" IconVisible="False" LeftMargin="457.0000" RightMargin="457.0000" TopMargin="310.0000" BottomMargin="390.0000" TouchEnable="True" FontSize="45" IsCustomSize="True" LabelText="" PlaceHolderText="请输入邀请码" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="420.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="415.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5533" />
                <PreSize X="0.3148" Y="0.0667" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>