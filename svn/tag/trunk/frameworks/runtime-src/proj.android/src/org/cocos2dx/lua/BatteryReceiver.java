package org.cocos2dx.lua;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BatteryReceiver extends BroadcastReceiver {
	private String power = "100";
    public BatteryReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int current = intent.getExtras().getInt("level");// 获得当前电量
        int total = intent.getExtras().getInt("scale");// 获得总电量
        int percent = current * 100 / total;
        this.power = "" + percent;
        System.out.println("this.power = " + this.power);
    }
    
    public String getPower()
    {
    	return this.power;
    }
}