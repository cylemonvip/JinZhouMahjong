#!/usr/bin/python
# coding=utf-8

import os
import sys
from optparse import OptionParser

def prRed(prt): print("\033[91m {}\033[00m" .format(prt))
def prGreen(prt): print("\033[92m {}\033[00m" .format(prt))
def prYellow(prt): print("\033[93m {}\033[00m" .format(prt))
def prLightPurple(prt): print("\033[94m {}\033[00m" .format(prt))
def prPurple(prt): print("\033[95m {}\033[00m" .format(prt))
def prCyan(prt): print("\033[96m {}\033[00m" .format(prt))
def prLightGray(prt): print("\033[97m {}\033[00m" .format(prt))
def prBlack(prt): print("\033[98m {}\033[00m" .format(prt))

PROJECT_ROOT = "/Users/chengyi/Documents/selfproj/svn/tag/trunk/frameworks/runtime-src/proj.android/"
so_path = PROJECT_ROOT + "obj/local/armeabi-v7a/libqpry_lua.so"
save_path = "x86-symbol.zip"
app_id = "56b9927219"
app_key = "8e61cbc9-c66c-45c5-a083-a549ae093587"
package = "com.mj.jiujiujinzhou"
channal = "Wechat"
version = "6.1"

if __name__ == "__main__":
	cmd = "buglySymbolAndroid/buglySymbolAndroid.sh -i %s -o %s -u -id -%s -key %s -package -%s -channal %s -version %s" % (so_path, save_path, app_id, app_key, package, channal, version)
	os.system(cmd)