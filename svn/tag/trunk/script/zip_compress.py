#!/usr/bin/python
# coding=utf-8
import os
import zipfile
import sys
from optparse import OptionParser

def compress(src, dst):
	path = src
	filename = dst
	try:
	    import zlib
	    compression = zipfile.ZIP_DEFLATED
	except:
	    compression = zipfile.ZIP_STORED
	start = path.rfind(os.sep) + 1
	z = zipfile.ZipFile(filename,mode = "w",compression = compression)
	try:
	    for dirpath,dirs,files in os.walk(path):
	        for file in files:
	            if file == filename or file == "zip.py":
	                continue
	            z_path = os.path.join(dirpath,file)
	            z.write(z_path,z_path[start:])
	    z.close()
	except:
	    if z:
	        z.close()


if __name__ == "__main__":
	parser = OptionParser()
	parser.add_option("-s", "--srcpath", dest="src_path",help='src path.')
	parser.add_option("-d", "--dstpath", dest="dst_path",help='dst path.')
	(opts, args) = parser.parse_args()
	srcPath = opts.src_path  #要进行压缩的文档目录  
	dstPath = opts.dst_path  #压缩后的文件名
	compress(srcPath, dstPath)