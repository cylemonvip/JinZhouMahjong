#!/usr/bin/env python
#-*-coding:utf-8-*-  
  
  
import os  
import time  
from ftplib import FTP    
from optparse import OptionParser
  
#服务器地址  
FTP_SERVER=''
USER=''  
PWD =''  
FTP_PATH =''  
OUT_PUT_PATH =""  

def ftp_down(filename = "filemd5List.json"): 
  ftp=FTP() 
  ftp.set_debuglevel(2) 
  ftp.connect(FTP_SERVER,'21') 
  ftp.login(USER,PWD) 
  print ftp.getwelcome()
  #显示ftp服务器欢迎信息 
  ftp.cwd(FTP_PATH)
  #选择操作目录 
  bufsize = 1024
  filename = 'filemd5List.json'#"filemd5List.json"
  fp = open(OUT_PUT_PATH,'wb')
  print("lp = %s" % os.path.basename(filename))
  #以写模式在本地打开文件 
  ftp.retrbinary('RETR %s' % os.path.basename(filename), fp.write, bufsize)
  #接收服务器上文件并写入本地文件 
  ftp.set_debuglevel(0) 
  fp.close()
  ftp.quit() 
  print "ftp down OK" 
  
if __name__=="__main__":  
    parser = OptionParser()
    parser.add_option("-i", "--ip", dest="ip_adress",help='ip.')
    parser.add_option("-u", "--username", dest="user_name",help='user name.')
    parser.add_option("-p", "--password", dest="password",help='password.')
    parser.add_option("-l", "--localdir", dest="local_dir",help='local dir path.')
    parser.add_option("-r", "--remotedir", dest="remote_dir",help='remote dir path.')
    (opts, args) = parser.parse_args()
    # #ip
    FTP_SERVER = opts.ip_adress
    # #user name
    USER = opts.user_name
    # #pwd
    PWD = opts.password
    
    # #remote path
    FTP_PATH = opts.remote_dir
    # #local wirte path
    OUT_PUT_PATH = opts.local_dir
    ftp_down()