#!/usr/bin/python
# coding=utf-8
__author__ = 'YCheng'  
__mail__ = 'cylemonVip@163.com'  
__date__ = '2017-08-25'  
__version = 1.0  

def prRed(prt): print("\033[91m {}\033[00m" .format(prt))
def prGreen(prt): print("\033[92m {}\033[00m" .format(prt))
def prYellow(prt): print("\033[93m {}\033[00m" .format(prt))
def prLightPurple(prt): print("\033[94m {}\033[00m" .format(prt))
def prPurple(prt): print("\033[95m {}\033[00m" .format(prt))
def prCyan(prt): print("\033[96m {}\033[00m" .format(prt))
def prLightGray(prt): print("\033[97m {}\033[00m" .format(prt))
def prBlack(prt): print("\033[98m {}\033[00m" .format(prt))
  
import sys  
from optparse import OptionParser
import os  
import json
from ftplib import FTP
import hashlib
import subprocess
reload(sys)
sys.setdefaultencoding('utf8')

  
_XFER_FILE = 'FILE'  
_XFER_DIR = 'DIR'  

#远程路径
REMOTE_DIR = ""
#本地路径
LOCAL_DIR = r""
#本地文件
LOCAL_FILE = r''

REMOTE_IP = ""
USER_NAME = ""
PASS_WORD = ""

MD5ARRAY = False

def CalcMD5(filepath):
    with open(filepath,'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        return md5obj.hexdigest()

def readMD5Json(md5filePath):
    with open(md5filePath) as json_file:
        data = json.load(json_file)
        listdata = data["listdata"]
        for item in listdata:
            if "allcount" in item:
                prLightPurple("md5 Json End %d" % (item["allcount"]))
            else:
                pass
                # prYellow("name: %s, md5: %s, path: %s" % (item["name"], item["md5"], item["path"]))
        return listdata

def checkFileChanged(name, fpath, md5Code):
    if MD5ARRAY == False:
        return True
    if name == "filemd5List.json":
        return False

    findName = False
    for item in MD5ARRAY:
        if "allcount" in item:
            pass
        else:
            fullPath = os.path.join(item["path"], name)
            if item["name"] == name and fullPath in fpath:
                findName = True
                if md5Code != item["md5"]:
                    # prLightPurple("Changed ======== >>>> fpath = %s" % fpath)
                    # prLightPurple("Changed ======== >>>> item[path] = %s" % item["path"])
                    # prRed("Changed ======== >>>> name : %s" % (name))
                    # prRed("Changed ======== >>>> bmd5 : %s" % (item["md5"]))
                    # prRed("Changed ======== >>>> cmd5 : %s" % (md5Code))
                    return True
                else:
                    return False
    if not findName:
        return True
    return False
    

class Xfer(object):  
    ''''' 
    @note: upload local file or dirs recursively to ftp server 
    '''  
    def __init__(self):  
        self.ftp = None  
      
    def __del__(self):  
        pass  
      
    def setFtpParams(self, ip, uname, pwd, port = 21, timeout = 60):          
        self.ip = ip  
        self.uname = uname  
        self.pwd = pwd  
        self.port = port  
        self.timeout = timeout  
      
    def initEnv(self):  
        if self.ftp is None:  
            self.ftp = FTP()  
            prPurple('### connect ftp server: %s ...'%self.ip)
            self.ftp.connect(self.ip, self.port, self.timeout)  
            self.ftp.login(self.uname, self.pwd)   
            prLightPurple(self.ftp.getwelcome())
      
    def clearEnv(self):  
        if self.ftp:  
            self.ftp.close()  
            prRed('### disconnect ftp server: %s!'%self.ip)
            self.ftp = None  
      
    def uploadDir(self, localdir, remotedir):  
        if not os.path.isdir(localdir):    
            return  
        self.ftp.cwd(remotedir)   
        for file in os.listdir(localdir):  
            src = os.path.join(localdir, file)  
            if os.path.isfile(src):  
                self.uploadFile(src, file)  
            elif os.path.isdir(src):  
                try:    
                    self.ftp.mkd(file)    
                except:    
                    sys.stderr.write('the dir is exists %s'%file)  
                self.uploadDir(src, file)  
        self.ftp.cwd('..')  
      
    def uploadFile(self, localpath, remotepath):
        if not os.path.isfile(localpath) or localpath.endswith(".DS_Store"):    
            return
        if not checkFileChanged(remotepath, localpath, CalcMD5(localpath)):
            return
        prGreen('+++ upload %s to %s  ==>>  %s'%(localpath, self.ip, remotepath))
        self.ftp.storbinary('STOR ' + remotepath, open(localpath, 'rb'))

    def __filetype(self, src):  
        if os.path.isfile(src):  
            index = src.rfind('\\')  
            if index == -1:  
                index = src.rfind('/')                  
            return _XFER_FILE, src[index+1:]  
        elif os.path.isdir(src):  
            return _XFER_DIR, ''          
      
    def upload(self, src):  
        filetype, filename = self.__filetype(src)  
          
        self.initEnv()  
        if filetype == _XFER_DIR:  
            self.srcDir = src              
            self.uploadDir(self.srcDir, REMOTE_DIR)  
        elif filetype == _XFER_FILE:  
            #如果是上传单个文件，则需要cd到目标目录
            if LOCAL_FILE:
                self.ftp.cwd(REMOTE_DIR)  
            self.uploadFile(src, filename)  
        self.clearEnv()   
                 
  
if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-i", "--ip", dest="ip_adress",help='ip.')
    parser.add_option("-u", "--username", dest="user_name",help='user name.')
    parser.add_option("-p", "--password", dest="password",help='password.')
    parser.add_option("-l", "--localdir", dest="local_dir",help='local dir path.')
    parser.add_option("-r", "--remotedir", dest="remote_dir",help='remote dir path.')
    parser.add_option("-f", "--file", dest="file",help='upload file.')
    parser.add_option("-m", "--md5filepath", dest="md5_file_path",help='md5 file path.')
    (opts, args) = parser.parse_args()
    REMOTE_IP = opts.ip_adress
    USER_NAME = opts.user_name
    PASS_WORD = opts.password
    LOCAL_DIR = opts.local_dir
    REMOTE_DIR = opts.remote_dir
    LOCAL_FILE = opts.file
    MD5_FILE_PATH = opts.md5_file_path
    
    prPurple("REMOTE_IP : %s" % REMOTE_IP)
    prPurple("USER_NAME : %s" % USER_NAME)
    prPurple("PASS_WORD : ******")
    prPurple("LOCAL_DIR : %s" % LOCAL_DIR)
    prPurple("REMOTE_DIR : %s" % REMOTE_DIR)
    prPurple("LOCAL_FILE : %s" % LOCAL_FILE)
    prPurple("MD5_FILE_PATH : %s" % MD5_FILE_PATH)
    
    if REMOTE_IP == None or USER_NAME == None or PASS_WORD == None or (LOCAL_FILE == None and LOCAL_DIR == None) or REMOTE_DIR == None:
        prRed("Please enter the full parameters")
        exit()

    xfer = Xfer()  
    xfer.setFtpParams(REMOTE_IP, USER_NAME, PASS_WORD)  
    if LOCAL_FILE:
        xfer.upload(LOCAL_FILE)
    else:
        MD5ARRAY = False
        if MD5_FILE_PATH:
            MD5ARRAY = readMD5Json(MD5_FILE_PATH)
        xfer.upload(LOCAL_DIR)