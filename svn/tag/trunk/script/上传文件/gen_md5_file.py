#!/usr/bin/env python
#coding:utf-8
import os
from optparse import OptionParser
import json
import hashlib
import subprocess

SRC_DIR = ""
DST_DIR = ""
PRE_DESC = ""
LAST_CHAR = ""

def prRed(prt): print("\033[91m {}\033[00m" .format(prt))
def prGreen(prt): print("\033[92m {}\033[00m" .format(prt))
def prYellow(prt): print("\033[93m {}\033[00m" .format(prt))
def prLightPurple(prt): print("\033[94m {}\033[00m" .format(prt))
def prPurple(prt): print("\033[95m {}\033[00m" .format(prt))
def prCyan(prt): print("\033[96m {}\033[00m" .format(prt))
def prLightGray(prt): print("\033[97m {}\033[00m" .format(prt))
def prBlack(prt): print("\033[98m {}\033[00m" .format(prt))

def CalcMD5(filepath):
    with open(filepath,'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        return md5obj.hexdigest()

def gen_json_files(path):
    project_str = {}
    dataDic = []
    for root , dirs, files in os.walk(path):
        for name in files:
            if name.endswith(".DS_Store") or name == "filemd5List.json":
                continue

            pathName = os.path.join(PRE_DESC, (root.replace(SRC_DIR + LAST_CHAR, "")))
            pathName = pathName+"/"
            mkMD5File = os.path.join(root,name)
            md5str = CalcMD5(mkMD5File)
            prGreen("path : %s, name : %s, md5 : %s" % (pathName, name, md5str))
            dataDic.append({"path" : pathName, "name" : name, "md5" : md5str})


    dataDic.append({"allcount" : len(dataDic)})
    prLightPurple("allcount = %s" % (len(dataDic) - 1))
    project_str.update({"listdata":dataDic})
    json_str = json.dumps(project_str, sort_keys = True, indent = 2)
    fo = open(DST_DIR,"w")
    fo.write(json_str)
    fo.close()

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-s", "--src", dest="src",help='compress file dir.')
    parser.add_option("-d", "--dst", dest="dst",help='out put file dir.')
    (opts, args) = parser.parse_args()
    SRC_DIR = opts.src
    DST_DIR = opts.dst

    prPurple("SRC_DIR : %s" % SRC_DIR)
    prPurple("DST_DIR : %s" % DST_DIR)

    if SRC_DIR == None or DST_DIR == None:
        prRed("Please enter the full parameters")
        exit()

    pathArray = SRC_DIR.split("/")
    if SRC_DIR[len(SRC_DIR)-1] == "/":
        PRE_DESC = pathArray[len(pathArray)-2]
        LAST_CHAR = ""
    else:
        LAST_CHAR = "/"
        PRE_DESC = pathArray[len(pathArray)-1]
    
    prLightPurple("PRE_DESC %s" % PRE_DESC)
    DST_DIR = os.path.join(DST_DIR, "filemd5List.json")
    gen_json_files(SRC_DIR)




