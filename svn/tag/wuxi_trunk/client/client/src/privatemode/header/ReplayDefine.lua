local ReplayDefine = {}


-- word = 2
-- dword = 4
-- score = 4
-- string = 2
-- byte = 1

--记录头
ReplayDefine.tagRecordHead = --size = 16
{
	{t = "word", k = "wFileVer"},
	{t = "word", k = "wFileKey"},
	{t = "word", k = "wGameCount"}, 					--总局数
	{t = "word", k = "wGameRount"},						--第几局
	{t = "dword", k = "dwRoomCardID"},					--房号
	{t = "dword", k = "dwContentSize"}					--本次实际下发内容
}

--用户属性
ReplayDefine.tagUserDataClient = --size = 81
{
	{t = "dword", k = "dwUserID"}, 								--UserID
	{t = "dword", k = "dwGameID"},								--GameID
	{t = "word", k = "wTableID"},								--桌子的ID
	{t = "word", k = "wChairID"},								--椅子ID
	{t = "int", k = "lScore"},									--分数
	{t = "byte", k = "cbGender"},								--性别
	{t = "string", k = "szNickName", s = yl.LEN_NICKNAME},		--昵称
	{t = "string", k = "szWeChatImgUrl", s = yl.LEN_USER_NOTE}	--头像URL
}

ReplayDefine.CMD_GameUserData = --size = 326
{
	{t = "dword", k = "wUsers"}, 								--用户人数
	{t = "table", k = "tUserData", d = ReplayDefine.tagUserDataClient, l = {4}}
}

ReplayDefine.CMD_GameData = --size = 8108
{
	{t = "word", k = "wMainCmdID"},								--消息主体
	{t = "word", k = "wSubCmdID"},								--消息操作码
	{t = "word", k = "wDataSize"},								--该数据长度
	{t = "word", k = "wChairID"},								--操作数据的椅子ID
	{t = "byte", k = "cbGameData", l = {8100}}					--消息数据
};


--操作数据


return ReplayDefine