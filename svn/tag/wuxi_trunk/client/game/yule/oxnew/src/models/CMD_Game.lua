local cmd =  {}

--游戏版本
cmd.VERSION 					= appdf.VersionValue(6,7,0,1)
--游戏标识
cmd.KIND_ID						= 27
	
--游戏人数
cmd.GAME_PLAYER					= 6

--视图位置
cmd.MY_VIEWID					= 3

cmd.VOICE_ANIMATION_KEY = "voice_ani_key"

cmd.DY_EXPRESSION_1_ANI_KEY = "dy_expression_1_ani_key"
cmd.DY_EXPRESSION_2_ANI_KEY = "dy_expression_2_ani_key"
cmd.DY_EXPRESSION_3_ANI_KEY = "dy_expression_3_ani_key"
cmd.DY_EXPRESSION_4_ANI_KEY = "dy_expression_4_ani_key"
cmd.DY_EXPRESSION_5_ANI_KEY = "dy_expression_5_ani_key"
cmd.DY_EXPRESSION_6_ANI_KEY = "dy_expression_6_ani_key"
cmd.DY_EXPRESSION_7_ANI_KEY = "dy_expression_7_ani_key"
cmd.DY_EXPRESSION_8_ANI_KEY = "dy_expression_8_ani_key"

--******************         游戏状态             ************--
--等待开始
cmd.GS_TK_FREE					= 0
--叫庄状态
cmd.GS_TK_CALL					= 100
--下注状态
cmd.GS_TK_SCORE					= 101
--游戏进行
cmd.GS_TK_PLAYING				= 102

--*********************      服务器命令结构       ************--
--游戏开始
cmd.SUB_S_GAME_START			= 100
--加注结果
cmd.SUB_S_ADD_SCORE				= 101
--用户强退
cmd.SUB_S_PLAYER_EXIT			= 102
--发牌消息
cmd.SUB_S_SEND_CARD				= 103
--游戏结束
cmd.SUB_S_GAME_END				= 104
--用户摊牌
cmd.SUB_S_OPEN_CARD				= 105
--用户叫庄
cmd.SUB_S_CALL_BANKER			= 106

--**********************    客户端命令结构        ************--
--用户叫庄
cmd.SUB_C_CALL_BANKER			= 1
--用户加注
cmd.SUB_C_ADD_SCORE				= 2
--用户摊牌
cmd.SUB_C_OPEN_CARD				= 3
--更新库存
cmd.SUB_C_STORAGE				= 6
--设置上限
cmd.SUB_C_STORAGEMAXMUL			= 7
--请求查询用户
cmd.SUB_C_REQUEST_QUERY_USER	= 8
--用户控制
cmd.SUB_C_USER_CONTROL			= 9

--********************       定时器标识         ***************--
--无效定时器
cmd.IDI_NULLITY					= 200
--开始定时器
cmd.IDI_START_GAME				= 201
--叫庄定时器
cmd.IDI_CALL_BANKER				= 202
--加注定时器
cmd.IDI_TIME_USER_ADD_SCORE		= 1
--摊牌定时器
cmd.IDI_TIME_OPEN_CARD			= 2
--摊牌定时器
cmd.IDI_TIME_NULLITY			= 3
--延时定时器
cmd.IDI_DELAY_TIME				= 4

--*******************        时间标识         *****************--
--叫庄定时器
cmd.TIME_USER_CALL_BANKER		= 30
--开始定时器
cmd.TIME_USER_START_GAME		= 30
--加注定时器
cmd.TIME_USER_ADD_SCORE			= 30
--摊牌定时器
cmd.TIME_USER_OPEN_CARD			= 30

--空闲状态
cmd.CMD_S_StatusFree = 
{
	{k = "wGameCount", t = "word"},								--游戏局数
	--基础积分
	{k = "lCellScore", t = "score"},
	{k = "lTurnScore", t = "score", l = {cmd.GAME_PLAYER}},
	{k = "lCollectScore", t = "score", l = {cmd.GAME_PLAYER}}
}

cmd.CMD_S_StatusCall = 
{
	{k = "cbDynamicJoin", t = "byte"}, 							--动态加入 
	{k = "wCallBanker", t = "word"},							--叫庄用户
	{k = "wGameCount", t = "word"},								--游戏局数
	{k = "cbPlayStatus", t = "byte", l = {cmd.GAME_PLAYER}},	--用户状态

	--历史积分
	{k = "lTurnScore", t = "score", l = {cmd.GAME_PLAYER}},		--积分信息
	{k = "lCollectScore", t = "score", l = {cmd.GAME_PLAYER}} 	--积分信息
}

--游戏下注状态        		GS_TK_SCORE
cmd.CMD_S_StatusScore = 
{
	--下注信息
	{k = "cbPlayStatus", t = "byte", l = {cmd.GAME_PLAYER}},	--用户状态
	{k = "cbDynamicJoin", t = "byte"},							--动态加入
	{k = "wGameCount", t = "word"},								--游戏局数
	{k = "wCallBanker", t = "word"},							--庄家用户
	{k = "lTurnMaxScore", t = "score"},							--最大下注
	{k = "lTableScore", t = "score", l = {cmd.GAME_PLAYER}},	--下注数目

	--历史积分
	{k = "lTurnScore", t = "score", l = {cmd.GAME_PLAYER}},		--积分信息
	{k = "lCollectScore", t = "score", l = {cmd.GAME_PLAYER}}	--积分信息		
};

--游戏摊牌状态             GS_TK_PLAYING
cmd.CMD_S_StatusPlay = 
{
	--状态信息
	{k = "bOpenCard", t = "bool", l = {cmd.GAME_PLAYER}},	--用户状态
	{k = "cbPlayStatus", t = "byte", l = {cmd.GAME_PLAYER}},	--用户状态
	{k = "cbDynamicJoin", t = "byte"},							--动态加入
	{k = "wGameCount", t = "word"},								--游戏局数
	{k = "wCallBanker", t = "word"},							--庄家用户
	{k = "lTurnMaxScore", t = "score"},							--最大下注
	{k = "lTableScore", t = "score", l = {cmd.GAME_PLAYER}},	--下注数目

	--扑克信息
	{k = "cbHandCardData", t = "byte", l = {5, 5, 5, 5, 5, 5}},		--桌面扑克
	{k = "bOxCard", t = "byte", l = {cmd.GAME_PLAYER}},			--牛牛数据

	--历史积分
	{k = "lTurnScore", t = "score", l = {cmd.GAME_PLAYER}},		--积分信息
	{k = "lCollectScore", t = "score", l = {cmd.GAME_PLAYER}}	--积分信息
};

--用户叫庄            			SUB_S_CALL_BANKER			106
cmd.CMD_S_CallBanker = 
{
	{k = "bFirstTimes", t = "bool"},							--首次叫庄
	{k = "wGameCount", t = "word"},								--游戏局数
	{k = "wCallBanker", t = "word"},							--叫庄用户
	{k = "cbPlayerStatus", t = "byte", l = {cmd.GAME_PLAYER}},	--玩家状态
};

--游戏开始	SUB_S_GAME_START			100
cmd.CMD_S_GameStart =  	
{
	--下注信息
	{k = "wCallBanker", t = "word"},							--庄家用户
	{k = "lTurnMaxScore", t = "score"},							--最大下注
};

--用户下注             			SUB_S_ADD_SCORE				101
cmd.CMD_S_AddScore = 
{
	{k = "wAddScoreUser", t = "word"},							--庄家用户
	{k = "lAddScoreCount", t = "score"},						--加注数目
};

--游戏结束               		SUB_S_GAME_END				104
cmd.CMD_S_GameEnd = 
{
	{k = "lGameTax", t = "score", l = {cmd.GAME_PLAYER}},		--游戏税收
	{k = "lGameScore", t = "score", l = {cmd.GAME_PLAYER}},		--游戏得分
	{k = "cbCardData", t = "byte", l = {cmd.GAME_PLAYER}},		--用户扑克
	{k = "cbDelayOverGame", t = "byte"},						--用户扑克
	{k = "bfiveKing", t = "bool", l = {cmd.GAME_PLAYER}},		--五花牛标识
};

--发牌数据包 SUB_S_SEND_CARD				103
cmd.CMD_S_SendCard =        	
{
	{k = "cbCardData", t = "byte", l = {5, 5, 5, 5, 5, 5}},			--用户扑克
};

--用户退出	SUB_S_PLAYER_EXIT			102
cmd.CMD_S_PlayerExit = 
{
	{k = "wPlayerID", t = "word"},								--退出用户
};

--用户摊牌                		SUB_S_OPEN_CARD				105
cmd.CMD_S_Open_Card = 
{
	{k = "wPlayerID", t = "word"},								--摊牌用户
	{k = "bOpen", t = "byte"},									--摊牌标志
};

return cmd