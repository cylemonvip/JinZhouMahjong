local GameModel = appdf.req(appdf.CLIENT_SRC.."gamemodel.GameModel")

local GameLayer = class("GameLayer", GameModel)

local cmd = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.CMD_Game")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.GameLogic")
local GameViewLayer = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.views.layer.GameViewLayer")
local ExternalFun =  appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local Utils = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.utils.Utils")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")
local UserItem = appdf.req(appdf.CLIENT_SRC.."plaza.models.ClientUserItem")

function GameLayer:ctor(frameEngine, scene, param)
    GameLayer.super.ctor(self, frameEngine, scene, param)
    --飘数
    self.m_piaoCount = {}
    self.m_huaCardArray = {}
    self._lastSendTime = os.time()

    if self._isReplay then

    	self._replayControlNode =  cc.CSLoader:createNode("client/src/privatemode/plaza/res/room/ReplayerControlNode.csb")
    	self._replayControlNode:setPosition(cc.p(667, 375))
    	self._replayControlNode:addTo(self, 20)

    	local rootNode = self._replayControlNode:getChildByName("RootNode")
    	if GameplayerManager:getInstance():isTest() then
    		rootNode:setTouchEnabled(false)
    	end
    	
    	local txtProgress = rootNode:getChildByName("TxtProgress")
    	local progress = GameplayerManager:getInstance():getProgress()
    	txtProgress:setString("进度: ".. (progress * 100) .. "%")
    	local txtRate = rootNode:getChildByName("TxtRate")
    	local rate = GameplayerManager:getInstance():getRate()
    	txtRate:setString("速率: x"..rate)

    	local btnSlow = rootNode:getChildByName("BtnSlow")
    	local btnQuick = rootNode:getChildByName("BtnQuick")
    	local btnPlay = rootNode:getChildByName("BtnPlay")
    	local btnClose = rootNode:getChildByName("BtnClose")

    	local btnCallback = function(ref, eventType)
			if eventType == ccui.TouchEventType.began then
				ref:setScale(1.2)
	        elseif eventType == ccui.TouchEventType.ended 
	        	or eventType == ccui.TouchEventType.canceled then
	            ref:setScale(1.0)
	            local name = ref:getName()
	            print("点击按钮 name = " .. name)
	            if name == "BtnSlow" then
	            	GameplayerManager:getInstance():slow()
	            	local rate = GameplayerManager:getInstance():getRate()
    				txtRate:setString("速率: x"..rate)
	            elseif name == "BtnQuick" then
	            	GameplayerManager:getInstance():quick()
	            	local rate = GameplayerManager:getInstance():getRate()
    				txtRate:setString("速率: x"..rate)
	            elseif name == "BtnPlay" then
	            	local isPause = GameplayerManager:getInstance():isPause()
	            	if isPause then
	            		GameplayerManager:getInstance():resume()
	            		--需要设置图片
	            		btnPlay:loadTextureNormal("client/src/privatemode/plaza/res/room/replayer/hist_rec_pause.png")
	            		btnPlay:loadTexturePressed("client/src/privatemode/plaza/res/room/replayer/hist_rec_pause.png")
	            		local action = cc.Sequence:create(cc.DelayTime:create(GameplayerManager:getInstance():getReplayDuration()),
	                          cc.CallFunc:create(replayerCallback))
	    				self:runAction(action)
	            	else
	            		GameplayerManager:getInstance():pause()
	            		--需要设置图片
	            		btnPlay:loadTextureNormal("client/src/privatemode/plaza/res/room/replayer/hist_rec_play.png")
	            		btnPlay:loadTexturePressed("client/src/privatemode/plaza/res/room/replayer/hist_rec_play.png")
	            		self:stopAllActions()
	            	end
	            elseif name == "BtnClose" then
	            	self:closeReplay()
	            end
	        end
		end

    	btnSlow:addTouchEventListener(btnCallback)
    	btnQuick:addTouchEventListener(btnCallback)
    	btnPlay:addTouchEventListener(btnCallback)
    	btnClose:addTouchEventListener(btnCallback)
    	
    	--重放的回调
    	function replayerCallback()
    		local subCmdID = nil
    		local dealData = function(cmd)
    			dump(cmd, "dealData cmd")
    			subCmdID = cmd.subCmdID
    			self._gameStartUserChairId = cmd.chairID
    			--数据处理
    			self:onEventGameMessage(cmd.subCmdID, cmd["t"])
    		end

    		print("提取重放数据 间隔时间：" .. GameplayerManager:getInstance():getReplayDuration() .. " 秒")

    		local isPause = GameplayerManager:getInstance():isPause()
    		if not isPause then
    			local replayCmd = GameplayerManager:getInstance():getNextCmd()
    			dealData(replayCmd)
    			local progress = GameplayerManager:getInstance():getProgress()
    			txtProgress:setString("进度: ".. (progress * 100) .. "%")
    			--游戏结束就终止
	    		if subCmdID ~= cmd.SUB_S_GAME_CONCLUDE then
	    			local action = cc.Sequence:create(cc.DelayTime:create(GameplayerManager:getInstance():getReplayDuration()),
	                          cc.CallFunc:create(replayerCallback))
	    			self:runAction(action)
	    		else
	    			self._replayControlNode:setVisible(false)
	    		end
	    	else
	    		local action = cc.Sequence:create(cc.DelayTime:create(GameplayerManager:getInstance():getReplayDuration()),
                          cc.CallFunc:create(replayerCallback))
    			self:runAction(action)
    		end
    	end
    	replayerCallback()
    end
end

function GameLayer:CreateView()
    return GameViewLayer:create(self):addTo(self)
end

function GameLayer:OnInitGameEngine()
    GameLayer.super.OnInitGameEngine(self)
    --保存当前玩家的座位id，防止最后一局被重置
    self.wMeChairID = 0
    self.isGameEnd = false
    -- self.userWithChairId = {}

	self.lCellScore = 0
	self.cbTimeOutCard = 0
	self.cbTimeOperateCard = 0
	self.cbTimeStartGame = 0
	self.wCurrentUser = yl.INVALID_CHAIR
	self.wBankerUser = yl.INVALID_CHAIR
	self.cbPlayStatus = {0, 0, 0, 0}
	self.cbGender = {0, 0, 0, 0}
	self.bTrustee = false
	self.nGameSceneLimit = 0
	self.cbAppearCardData = {} 		--已出现的牌
	self.bMoPaiStatus = false
	self.cbListenPromptOutCard = {}
	self.cbListenCardList = {}
	self.cbActionMask = nil
	self.cbActionCard = nil
	self.bSendCardFinsh = false
	self.cbPlayerCount = 4
	--当前局数
	self.cbQuanCount = 0
	self.lDetailScore = {}
	self.m_userRecord = {}

	self.cbMaCount = 0
	--房卡需要
	self.wRoomHostViewId = 0
	print("Hello Hello!")

	print("GlobalUserItem.dwLockServerID = " .. GlobalUserItem.dwLockServerID)
    print("GlobalUserItem.dwLockKindID = " .. GlobalUserItem.dwLockKindID)
    print("GlobalUserItem.nCurGameKind = " .. GlobalUserItem.nCurGameKind)    
end


function GameLayer:OnResetGameEngine()
    GameLayer.super.OnResetGameEngine(self)
    self._gameView:onResetData()
	self.nGameSceneLimit = 0
	self.bTrustee = false
	self.cbAppearCardData = {} 		--已出现的牌
	self.bMoPaiStatus = false
	self.cbActionMask = nil
	self.cbActionCard = nil
    self.m_huaCardArray = {}
end

--获取gamekind
function GameLayer:getGameKind()
    return cmd.KIND_ID
end

-- 房卡信息层zorder
function GameViewLayer:priGameLayerZorder()
    return 13
end

--关闭重放
function GameLayer:closeReplay()
 	--清除重放数据
 	self:stopAllActions()
    GameplayerManager:getInstance():clear()
 	self:onExitRoom()
 	local param = {}
	table.insert(param, true)
 	self._scene:onChangeShowMode(PriRoom.LAYTAG.LAYER_MYROOMRECORD, param)
end

function GameLayer:onExitRoom()
	print("GameLayer 调用退出房间")
	if not self._isReplay then
		self._gameFrame:onCloseSocket()
	end
    self:stopAllActions()
    self:KillGameClock()
    self:dismissPopWait()
    if not self._isReplay then
    	self._scene:onChangeShowMode(yl.SCENE_GAMELIST)
    end
    -- self._scene:onKeyBack()
end

-- 椅子号转视图位置,注意椅子号从0~nChairCount-1,返回的视图位置从1~nChairCount
function GameLayer:SwitchViewChairID(chair)
    local viewid = yl.INVALID_CHAIR
    local playerCount = nil
    local selfChair = nil

    if self._isReplay then
    	playerCount = GameplayerManager:getInstance():getPlayerCount()
    	selfChair = GameplayerManager:getInstance():getSelfChairID()
    end
    
    --当前桌子人数
    local nChairCount = playerCount or self._gameFrame:GetChairCount()
    --nChairCount = cmd.GAME_PLAYER
    --获取自己的椅子号
    local nChairID = selfChair or self:GetMeChairID()
    -- if chair ~= yl.INVALID_CHAIR and chair < nChairCount then
    --     viewid = math.mod(chair + math.floor(nChairCount * 3/2) - nChairID, nChairCount) + 1
    -- end

    if not chair then
    	print("chair = nil")
    	return nil
    end

	if chair ~= yl.INVALID_CHAIR and chair < cmd.GAME_PLAYER then
		--if GlobalUserItem.bPrivateRoom then 		--约战房
		    if nChairCount == 2 then
		    	if chair == 2 then -- 无用ID
		    		viewid = 2
		    	elseif chair == 3 then --无用ID
		    		viewid = 4
		    	elseif chair == nChairID then
		    		viewid = 3
		    	else
		    		viewid = 1
		    	end
			elseif nChairCount == 3 then
				if chair == 3 then -- 无用ID
					viewid = 1
				else
					
					local temp = math.mod(chair + (3 - 2 - nChairID), 3)
					print("chair : " .. chair .. "temp : " .. temp .. " nChairID : " .. nChairID)
					viewid = temp + 2
				end 
				-- viewid = 3 - (self._gameFrame._wChairID - chair)
			elseif nChairCount == 4 then
		        viewid = math.mod(chair + math.floor(nChairCount * 3/2) - nChairID, nChairCount) + 1
		    end
		-- else 				--普通房
		--     viewid = math.mod(chair + math.floor(cmd.GAME_PLAYER * 3/2) - nChairID, cmd.GAME_PLAYER) + 1
		-- end
    end
    -- print("返回椅子转view 原始椅子id = " .. chair .. " 桌子人数 = " .. nChairCount .. " 自己的椅子id = " .. nChairID .. " 转后的id = " .. viewid)
    return viewid
end

 function GameLayer:getRoomHostViewId()
 	return self.wRoomHostViewId
 end

function GameLayer:updateRoomHost()
	assert(GlobalUserItem.bPrivateRoom)
	if self.wRoomHostViewId and self.wRoomHostViewId >= 1 and self.wRoomHostViewId <= 4 then
		self._gameView:setRoomHost(self.wRoomHostViewId)
	end
end

function GameLayer:getUserInfoByChairID(chairId)
	local viewId = self:SwitchViewChairID(chairId)
	return self._gameView.m_sparrowUserItem[viewId]
end

function GameLayer:getMaCount()
	return self.cbMaCount
end

function GameLayer:onGetSitUserNum()
	local num = 0
	for i = 1, cmd.GAME_PLAYER do
		if nil ~= self._gameView.m_sparrowUserItem[i] then
			num = num + 1
		end
	end

    return num
end

-- function GameLayer:onEnterTransitionFinish()
--     self._scene:createVoiceBtn(cc.p(1250, 300))
--     GameLayer.super.onEnterTransitionFinish(self)
-- end

-- 计时器响应
function GameLayer:OnEventGameClockInfo(chair,time,clockId)
	-- print("GlobalUserItem.bPrivateRoom = " .. tostring(GlobalUserItem.bPrivateRoom) .. "   time = " .. time .. "  clockId = " .. clockId )
    -- body
    -- if GlobalUserItem.bPrivateRoom then
    -- 	return
    -- end
    local meChairId = self:GetMeChairID()
    if not self._isReplay then
    	if clockId == cmd.IDI_START_GAME then
	    	--托管
	    	if self.bTrustee and self._gameView.startBtnLayout:isVisible() then
	   --  		print("进去")
	   --  		self._gameView:onButtonClickedEvent(GameViewLayer.BT_START)
	   --  		--托管在上个函数被复原了，在下面重开
				-- self.bTrustee = true
				-- self._gameView.nodePlayer[cmd.MY_VIEWID]:getChildByTag(GameViewLayer.SP_TRUSTEE):setVisible(true)
				-- self._gameView.spTrusteeCover:setVisible(true)
	    	end
	    	-- if chair == meChairI then
	    		--如果游戏结束，那么把这个时间更新到结算界面
	    		if self.isGameEnd then
					self._gameView._resultLayer:onClockUpdate(time)
				end
	    		--超时
				if time <= 0 then
					print("OnEventGameClockInfo =================== 退出房间")
					self._gameFrame:setEnterAntiCheatRoom(false)--退出防作弊，如果有的话
					--如果游戏结束，并且时间到了，那么强制继续游戏，如果没有结束，那么就退出房间
					if self.isGameEnd then
						self._gameView._resultLayer:continueBtnCallback()
					else
						-- self:onExitTable()
					end
				elseif time <= 5 then
		    		self:PlaySound(cmd.RES_PATH.."sound/GAME_WARN.mp3")
				end
	    	-- end
	    elseif clockId == cmd.IDI_OUT_CARD then
	    	if chair == meChairId then
	    		--托管
	    		if self.bTrustee then
					--self._gameView._cardLayer:outCardAuto()
	    		end
	    		--超时
	    		if time <= 0 then
	    			print("OnEventGameClockInfo =================== 自动出牌 t GlobalUserItem.bPrivateRoom = " .. tostring(GlobalUserItem.bPrivateRoom))
	    			if not GlobalUserItem.bPrivateRoom then
	    				self._gameView._cardLayer:outCardAuto()
	    			end
					
	    		elseif time <= 5 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				self:PlaySound(cmd.RES_PATH.."sound/GAME_WARN.mp3")
	    			end
	    		end
	    	end
	    elseif clockId == cmd.IDI_OPERATE_CARD then
	    	if chair == meChairId then
	    		--托管
	    		if self.bTrustee then
	    -- 			if self._gameView._cardLayer:isUserMustWin() then
					-- 	self._gameView:onButtonClickedEvent(GameViewLayer.BT_WIN)
	    -- 			end
					-- self._gameView:onButtonClickedEvent(GameViewLayer.BT_PASS)
					-- self._gameView._cardLayer:outCardAuto()
	    		end
	    		--超时
	    		if time <= 0 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				if self._gameView._cardLayer:isUserMustWin() then
							self._gameView:onButtonClickedEvent(GameViewLayer.BT_WIN)
		    			end
						self._gameView:onButtonClickedEvent(GameViewLayer.BT_PASS)
						self._gameView._cardLayer:outCardAuto()
	    			end
	    		elseif time <= 5 then
	    			if not GlobalUserItem.bPrivateRoom then
	    				self:PlaySound(cmd.RES_PATH.."sound/GAME_WARN.mp3")
	    			end
	    		end
	    	end
	    end
    end
end

--用户聊天
function GameLayer:onUserChat(chat, wChairId)
    self._gameView:userChat(self:SwitchViewChairID(wChairId), chat.szChatString)
end

--用户表情
function GameLayer:onUserExpression(expression, wChairId, wTotChairID)
	dump("onUserExpression ---- c ", expression)
	for i,v in pairs(expression) do
		print("onUserExpression ---- c", i, v)
	end
	local viewToChair = nil
	if wTotChairID then
		viewToChair = self:SwitchViewChairID(wTotChairID)
	end
    self._gameView:userExpression(self:SwitchViewChairID(wChairId), expression.wItemIndex, viewToChair)
end

function GameLayer:IsValidViewID(viewId)
	local nChairCount = self._gameFrame:GetChairCount()
    if nChairCount == 3 then
        return (viewId >= 2) and (viewId <= 4)
    elseif nChairCount == 4 then
        return (viewId >= 1) and (viewId <= 4)    
    end
    return false
end

function GameLayer:onDyExpression(fromChairID, toChairID, index)
    local from = self:SwitchViewChairID(fromChairID)
    local to = self:SwitchViewChairID(toChairID)
    if self:IsValidViewID(from) and self:IsValidViewID(to) then
        self._gameView:onUserDyExpression(from, to, index)
    end
end

-- 语音播放开始
function GameLayer:onUserVoiceStart( useritem, filepath )
    self._gameView:onUserVoiceStart(self:SwitchViewChairID(useritem.wChairID))
end

-- 语音播放结束
function GameLayer:onUserVoiceEnded( useritem, filepath )
    self._gameView:onUserVoiceEnded(self:SwitchViewChairID(useritem.wChairID))
end

--用户状态发生变化
-- function GameLayer:onEventUserStatus(useritem,newstatus,oldstatus)
	
-- end

-- 场景消息
function GameLayer:onEventGameScene(cbGameStatus, dataBuffer)
	self.m_cbGameStatus = cbGameStatus
	self.nGameSceneLimit = self.nGameSceneLimit + 1
	if self.nGameSceneLimit > 1 then
		--限制只进入场景消息一次
		return true
	end
	
	print("GameLayer:onEventGameScene ------  断线重连 cbGameStatus = " .. cbGameStatus)
    
	local wTableId = self:GetMeTableID()
	local wMyChairId = self:GetMeChairID()
	self._gameView:setRoomInfo(wTableId, wMyChairId)
	--初始化用户信息
	local pcount = nil
	if self._isReplay then
		pcount = GameplayerManager:getInstance():getPlayerCount()
	end

	if not self._isReplay then
		local nChairCount = pcount or self._gameFrame:GetChairCount()
		for i = 1, nChairCount do --cmd.GAME_PLAYER
			local wViewChairId = self:SwitchViewChairID(i - 1)
			local userItem = self._gameFrame:getTableUserItem(wTableId, i - 1)
			self._gameView:OnUpdateUser(wViewChairId, userItem)
			if userItem then
				self.cbGender[wViewChairId] = userItem.cbGender
				if PriRoom and GlobalUserItem.bPrivateRoom then
					if userItem.dwUserID == PriRoom:getInstance().m_tabPriData.dwTableOwnerUserID then
						self.wRoomHostViewId = wViewChairId
						if self._gameView._priView.onRefreshInfo then
							--此处只要为了刷新房主
							self._gameView._priView:onRefreshInfo()
						end
						print("房主view id = " .. self.wRoomHostViewId)
					end
				end
			end
		end
	end

	if cbGameStatus == cmd.GAME_SCENE_FREE then
		if not self.isGameEnd then
			self._scene.m_gameAlreadyStart = true
		end
		print("空闲状态 ============ ", wMyChairId)

		local cmd_data = ExternalFun.read_netdata(cmd.CMD_S_StatusFree, dataBuffer)
		--dump(cmd_data, "空闲状态")

		self.lCellScore = cmd_data.lCellScore
		self.cbTimeOutCard = cmd_data.cbTimeOutCard
		self.cbTimeOperateCard = cmd_data.cbTimeOperateCard
		self.cbTimeStartGame = cmd_data.cbTimeStartGame
		self.cbPlayerCount = cmd_data.cbPlayerCount or 4

		for k,v in pairs(cmd_data.cbPiaoCount[1]) do
			print("free cbPiaoCount ======= >>> ", k, v)
			self._gameView:addPiaoTimes(k-1, v)
		end

		for k,v in pairs(cmd_data.tGameRule) do
			print("free tGameRule ======= >>> ", k, v)
		end
		local ruleTxtStr = ""
		if cmd_data.tGameRule.bCanPiao then
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "可飘"
			else
				ruleTxtStr = ruleTxtStr .. "、可飘"
			end
		else
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "不可飘"
			else
				ruleTxtStr = ruleTxtStr .. "、不可飘"
			end
		end

		if cmd_data.tGameRule.bShengJi then
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "升级"
			else
				ruleTxtStr = ruleTxtStr .. "、升级"
			end
		else
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "不升级"
			else
				ruleTxtStr = ruleTxtStr .. "、不升级"
			end
		end

		if cmd_data.tGameRule.bLeftCardHuang then
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "黄庄留八墩"
			else
				ruleTxtStr = ruleTxtStr .. "、黄庄留八墩"
			end
		else
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "摸完黄庄"
			else
				ruleTxtStr = ruleTxtStr .. "、摸完黄庄"
			end
		end

		if PriRoom:getInstance().m_playRuleList["SHENG_HUA"] then
            if PriRoom:getInstance().m_playRuleList["SHENG_HUA"] == 3 then
                if ruleTxtStr == "" then
                    ruleTxtStr = ruleTxtStr .. "3升花"
                else
                    ruleTxtStr = ruleTxtStr .. "、3升花"
                end
            elseif PriRoom:getInstance().m_playRuleList["SHENG_HUA"] == 4 then
                if ruleTxtStr == "" then
                    ruleTxtStr = ruleTxtStr .. "4升花"
                else
                    ruleTxtStr = ruleTxtStr .. "、4升花"
                end
            end
        end

		if ruleTxtStr == "" then
			ruleTxtStr = ruleTxtStr .. "无"
		end

		local ruleTxt = self._gameView:getChildByName("RuleTxt")
        ruleTxt:setString("玩法: " .. ruleTxtStr)

		PriRoom:getInstance().m_playRuleList["HUANG_ZHUANG_LIU_8_DUN"] = cmd_data.tGameRule.bLeftCardHuang
		PriRoom:getInstance().m_playRuleList["CAN_PIAO"] = cmd_data.tGameRule.bCanPiao
        PriRoom:getInstance().m_playRuleList["SHENG_JI"] = cmd_data.tGameRule.bShengJi
        PriRoom:getInstance().m_playRuleList["SHENG_HUA"] = cmd_data.tGameRule.wShengHua

		local btnNoPiao = self._gameView.startBtnLayout:getChildByName("BtnNoPiao")
		local btnPiao5 = self._gameView.startBtnLayout:getChildByName("BtnPiao5")
		local btnPiao10 = self._gameView.startBtnLayout:getChildByName("BtnPiao10")
		local btnPiao15 = self._gameView.startBtnLayout:getChildByName("BtnPiao15")
		local btnStart = self._gameView.startBtnLayout:getChildByName("BtnStart")
		
		--记录是否可以飘
		self.m_CanPiao = cmd_data.tGameRule.bCanPiao
		local showCanPiaoBtn = cmd_data.tGameRule.bCanPiao

		local myPiaoTimes = self:getPiaoCount(self:GetMeChairID())
		if myPiaoTimes and myPiaoTimes > 0 then
			showCanPiaoBtn = false
		end

		btnNoPiao:setVisible(showCanPiaoBtn)
		btnPiao5:setVisible(showCanPiaoBtn)
		btnPiao10:setVisible(showCanPiaoBtn)
		btnPiao15:setVisible(showCanPiaoBtn)
		btnStart:setVisible(not showCanPiaoBtn)

		--如果已经飘了，那就自动准备
		if not showCanPiaoBtn then
			self._gameView:sendReady(0)
			btnStart:setVisible(false)
		end

		self._gameFrame:SetChairCount(self.cbPlayerCount)
		-- self.cbMaCount = cmd_data.cbGameCount

		-- print("设置码数", self.cbMaCount)
		
		self._gameView.startBtnLayout:setVisible(true)

		-- if GlobalUserItem.bPrivateRoom then
		-- 	--self._gameView.spClock:setVisible(false)
		-- 	self._gameView.asLabTime:setString("0")
		-- else
		self:SetGameClock(wMyChairId, cmd.IDI_START_GAME, self.cbTimeStartGame) --第一次进入时调用
		self:OnResetGameEngine()
		-- self._gameView:initClockView(wMyChairId)
		--end

		--非房卡场，进入房间自动准备
		if not GlobalUserItem.bPrivateRoom then
	      	-- self._gameView:onButtonClickedEvent(self._gameView.BT_START,nil)
	      	self:sendGameStart()
		end

		self._gameView._cardLayer.nRemainCardNum = 144
		--设置剩余手牌
	    self._gameView:setRemainCardNum(self._gameView._cardLayer.nRemainCardNum)

		--恢复局数
		print("cmd_data.cbMaCount = " .. cmd_data.cbGameCount)
		-- 刷新房卡
    	if PriRoom and GlobalUserItem.bPrivateRoom then
	        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
	        	-- PriRoom:getInstance().m_tabPriData.dwPlayCount = cmd_data.cbGameCount
	        	print("恢复局数 cmd_data.wGameCount = " .. cmd_data.cbGameCount)
	            self._gameView._priView:setQuanData(cmd_data.cbGameCount)
	        end
	    end
	elseif cbGameStatus == cmd.GAME_SCENE_PLAY then
		print("self.isGameEnd = " .. tostring(self.isGameEnd))
		self._scene.m_gameAlreadyStart = true
		if not self.isGameEnd then
			self.isGameEnd = false
			--如果游戏开始，但结算界面仍然存在，那么将其删除
			self._gameView._resultLayer:hideLayer(false)
			self._gameView.startBtnLayout:setVisible(false)
		end

		self._scene.m_gameInResultLayer = false
		print("游戏状态 ============= ", wMyChairId)
		local cmd_data = ExternalFun.read_netdata(cmd.CMD_S_StatusPlay, dataBuffer)
		for k,v in pairs(cmd_data) do
			print("cmd_data -------->>>>>  ", k,v)
		end

		for k,v in pairs(cmd_data.cbPiaoCount[1]) do
			print("play cbPiaoCount ======= >>> ", k, v)
			--设置飘，座位号-飘数
			self._gameView:addPiaoTimes(k-1, v)
		end

		for i = 1, cmd.GAME_PLAYER do
			for j = 1, 8 do
				local hData = cmd_data.cbHuaCardData[i][j]
				self:addHuaCardArray(i-1, hData, true)
			end
		end

		--记录是否可以飘
		self.m_CanPiao = cmd_data.tGameRule.bCanPiao

		PriRoom:getInstance().m_playRuleList["HUANG_ZHUANG_LIU_8_DUN"] = cmd_data.tGameRule.bLeftCardHuang
		PriRoom:getInstance().m_playRuleList["CAN_PIAO"] = cmd_data.tGameRule.bCanPiao
        PriRoom:getInstance().m_playRuleList["SHENG_JI"] = cmd_data.tGameRule.bShengJi
        PriRoom:getInstance().m_playRuleList["SHENG_HUA"] = cmd_data.tGameRule.wShengHua

		local ruleTxtStr = ""
		if PriRoom:getInstance().m_playRuleList["CAN_PIAO"] then
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "可飘"
			else
				ruleTxtStr = ruleTxtStr .. "、可飘"
			end
		else
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "不可飘"
			else
				ruleTxtStr = ruleTxtStr .. "、不可飘"
			end
		end

		if PriRoom:getInstance().m_playRuleList["SHENG_JI"] then
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "升级"
			else
				ruleTxtStr = ruleTxtStr .. "、升级"
			end
		else
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "不升级"
			else
				ruleTxtStr = ruleTxtStr .. "、不升级"
			end
		end

		if PriRoom:getInstance().m_playRuleList["HUANG_ZHUANG_LIU_8_DUN"] then
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "黄庄留八墩"
			else
				ruleTxtStr = ruleTxtStr .. "、黄庄留八墩"
			end
		else
			if ruleTxtStr == "" then
				ruleTxtStr = ruleTxtStr .. "黄庄摸完"
			else
				ruleTxtStr = ruleTxtStr .. "、黄庄摸完"
			end
		end

		if PriRoom:getInstance().m_playRuleList["SHENG_HUA"] then
            if PriRoom:getInstance().m_playRuleList["SHENG_HUA"] == 3 then
                if ruleTxtStr == "" then
                    ruleTxtStr = ruleTxtStr .. "3升花"
                else
                    ruleTxtStr = ruleTxtStr .. "、3升花"
                end
            elseif PriRoom:getInstance().m_playRuleList["SHENG_HUA"] == 4 then
                if ruleTxtStr == "" then
                    ruleTxtStr = ruleTxtStr .. "4升花"
                else
                    ruleTxtStr = ruleTxtStr .. "、4升花"
                end
            end
        end

		if ruleTxtStr == "" then
			ruleTxtStr = ruleTxtStr .. "无"
		end

		local ruleTxt = self._gameView:getChildByName("RuleTxt")
        ruleTxt:setString("玩法: " .. ruleTxtStr)

		dump(cmd_data.cbHuCardData, "cbHuCardData")
		dump(cmd_data.wOutCardUser, "wOutCardUser")
		
		dump(cmd_data.cbOutCardDataEx, "cbOutCardDataEx")
		dump(cmd_data.cbHeapCardInfo, "cbHeapCardInfo")
 
 		-- showToast(self._scene, "海底捞参数" .. tostring(cmd_data.bHiDiLao), 2)
		--是否为海底捞
		self.m_isHaiDiLao = cmd_data.bHiDiLao

		self.lCellScore = cmd_data.lCellScore
		self.cbTimeOutCard = cmd_data.cbTimeOutCard
		self.cbTimeOperateCard = cmd_data.cbTimeOperateCard
		self.cbTimeStartGame = cmd_data.cbTimeStartGame
		self.wCurrentUser = cmd_data.wCurrentUser
		self.wBankerUser = cmd_data.wBankerUser
		print("游戏状态 恢复庄家 = " .. self.wBankerUser)
		self.cbPlayerCount = cmd_data.cbPlayerCount or 4
		if not self._isReplay then
			self._gameFrame:SetChairCount(self.cbPlayerCount)
		end
		
		self.cbQuanCount = cmd_data.cbMaCount

		if cmd_data.cbMagicIndex == 42 then
			GameLogic.MAGIC_DATA = -1
		else
			GameLogic.MAGIC_DATA = GameLogic.SwitchToCardData(cmd_data.cbMagicIndex)
		end 
	    
	    print("游戏状态 癞子牌索引 = " .. cmd_data.cbMagicIndex)
	    print("游戏状态 癞子牌实际点数 = " .. GameLogic.MAGIC_DATA)
	    GameLogic.MAGIC_INDEX = cmd_data.cbMagicIndex
		self._gameView:setLaiZi(cmd_data.cbMagicIndex)
		--庄家
		self._gameView:setBanker(self:SwitchViewChairID(self.wBankerUser))
		for i,v in ipairs(cmd_data.cbCardCount[1]) do
			print("cbCardCount --------  cbCardCount = ", i, v)
		end
		--设置手牌
		local viewCardCount = {}
		for i = 1, cmd.GAME_PLAYER do
			local viewId = self:SwitchViewChairID(i - 1)
			viewCardCount[viewId] = cmd_data.cbCardCount[1][i]
			if viewCardCount[viewId] > 0 then
				self.cbPlayStatus[viewId] = 1
			end
			print("重连客户端牌的数量 = " .. viewCardCount[viewId])
		end
		local cbHandCardData = {}
		for i = 1, cmd.MAX_COUNT do
			local data = cmd_data.cbCardData[1][i]
			if data > 0 then 				--去掉末尾的0
				cbHandCardData[i] = data
			else
				break
			end
		end
		GameLogic.SortCardList(cbHandCardData) 		--排序
		local cbSendCard = cmd_data.cbSendCardData
		if cbSendCard > 0 and self.wCurrentUser == wMyChairId then
			for i = 1, #cbHandCardData do
				if cbHandCardData[i] == cbSendCard then
					table.remove(cbHandCardData, i)				--把刚抓的牌放在最后
					break
				end
			end
			table.insert(cbHandCardData, cbSendCard)
		end
		for i = 1, cmd.GAME_PLAYER do
			self._gameView._cardLayer:setHandCard(i, viewCardCount[i], cbHandCardData)
		end
		self.bSendCardFinsh = true
		--记录已出现牌
		self:insertAppearCard(cbHandCardData)
		--组合牌
		for i = 1, cmd.GAME_PLAYER do
			local wViewChairId = self:SwitchViewChairID(i - 1)
			for j = 1, cmd_data.cbWeaveItemCount[1][i] do
				local cbOperateData = {}
				for v = 1, 4 do
					local data = cmd_data.WeaveItemArray[i][j].cbCardData[1][v]
					if data > 0 then
						table.insert(cbOperateData, data)
					end
				end
				local nShowStatus = GameLogic.SHOW_NULL
				local cbParam = cmd_data.WeaveItemArray[i][j].cbParam
				if cbParam == GameLogic.WIK_GANERAL then
					if cbOperateData[1] == cbOperateData[2] then 	--碰
						nShowStatus = GameLogic.SHOW_PENG
					else 											--吃
						nShowStatus = GameLogic.SHOW_CHI
						local tmp = {}
						tmp = {cbOperateData[2], cbOperateData[1], cbOperateData[3]}
						cbOperateData = tmp
					end
				elseif cbParam == GameLogic.WIK_MING_GANG then
					nShowStatus = GameLogic.SHOW_FANG_GANG
				elseif cbParam == GameLogic.WIK_FANG_GANG then
					nShowStatus = GameLogic.SHOW_FANG_GANG
				elseif cbParam == GameLogic.WIK_AN_GANG then
					nShowStatus = GameLogic.SHOW_AN_GANG
				end
				print("onEventGameScene 操作的椅子号 = " .. wViewChairId)
				dump(cbOperateData, "椅子：" .. wViewChairId .. " 对应的恢复牌数据")
				--dump(cmd_data.WeaveItemArray[i][j], "weaveItem")
				self._gameView._cardLayer:bumpOrBridgeCard(wViewChairId, cbOperateData, nShowStatus)
				--记录已出现牌
				self:insertAppearCard(cbOperateData)
			end
		end
		--设置牌堆
		local wViewHeapHead = self:SwitchViewChairID(cmd_data.wHeapHead)
		local wViewHeapTail = self:SwitchViewChairID(cmd_data.wHeapTail)
		for i = 1, cmd.GAME_PLAYER do
			local viewId = self:SwitchViewChairID(i - 1)
			for j = 1, cmd_data.cbDiscardCount[1][i] do
				--已出的牌
				self._gameView._cardLayer:discard(viewId, cmd_data.cbDiscardCard[i][j])
				--记录已出现牌
				local cbAppearCard = {cmd_data.cbDiscardCard[i][j]}
				self:insertAppearCard(cbAppearCard)
			end

			for k,v in pairs(cmd_data.cbHeapCardInfo) do
				print("第 " .. k .. " 人的牌堆信息")
				for l, m in pairs(v) do
					print(k,v)
				end
				print("===============================================")
			end

			--牌堆
			self._gameView._cardLayer:setTableCardByHeapInfo(viewId, cmd_data.cbHeapCardInfo[i], wViewHeapHead, wViewHeapTail)
			--托管
			self._gameView:setUserTrustee(viewId, cmd_data.bTrustee[1][i])
			if viewId == cmd.MY_VIEWID then
				self.bTrustee = cmd_data.bTrustee[1][i]
			end
		end

		--刚出的牌
		if cmd_data.cbOutCardData and cmd_data.cbOutCardData > 0 then
			print("cmd_data.wOutCardUser = " .. tostring(cmd_data.wOutCardUser))
			local wOutUserViewId = self:SwitchViewChairID(cmd_data.wOutCardUser)
			print("wOutUserViewId = " .. tostring(wOutUserViewId))
			print("cmd_data.cbOutCardData = " .. tostring(cmd_data.cbOutCardData))
			self._gameView:showCardPlate(wOutUserViewId, cmd_data.cbOutCardData)
		end
		--计时器
		self:SetGameClock(cmd_data.wCurrentUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard) --断线重连时调用

		--提示听牌数据
		self.cbListenPromptOutCard = {}
		self.cbListenCardList = {}
		for i = 1, cmd_data.cbOutCardCount do
			self.cbListenPromptOutCard[i] = cmd_data.cbOutCardDataEx[1][i]
			self.cbListenCardList[i] = {}
			for j = 1, cmd_data.cbHuCardCount[1][i] do
				self.cbListenCardList[i][j] = cmd_data.cbHuCardData[i][j]
			end
		end
		local cbPromptHuCard = self:getListenPromptHuCard(cmd_data.cbOutCardData)
		self._gameView:setListeningCard(cbPromptHuCard)
		for k,v in pairs(cmd_data) do
			print("cmd_data ---- ", k, v)
		end

		--65535 表明无人出牌，即自己摸牌
		local isOtherOp = false
		if cmd_data.wOutCardUser == 65535 then
			isOtherOp = false
		elseif self:GetMeChairID() ~= cmd_data.wOutCardUser then
			isOtherOp = true
		end
		--提示操作
		self._gameView:recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbActionCard, isOtherOp)
		if self.wCurrentUser == wMyChairId then
			self._gameView._cardLayer:promptListenOutCard(self.cbListenPromptOutCard)
		end

		self._gameView._cardLayer.nRemainCardNum = cmd_data.cbLeftCardCount
		print("self._gameView._cardLayer.nRemainCardNum = " .. self._gameView._cardLayer.nRemainCardNum)
		--设置剩余手牌
	    self._gameView:setRemainCardNum(self._gameView._cardLayer.nRemainCardNum)

		--恢复局数
		print("恢复局数 cmd_data.cbMaCount = " .. cmd_data.cbMaCount)
		-- 刷新房卡
    	if PriRoom and GlobalUserItem.bPrivateRoom then
	        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
	        	PriRoom:getInstance().m_tabPriData.dwPlayCount = cmd_data.cbMaCount

	        	print("恢复局数 cmd_data.wGameCount = " .. cmd_data.cbMaCount)
	            -- self._gameView._priView:onRefreshInfo()
	            self._gameView._priView:setQuanData(cmd_data.cbMaCount)

	        end
	    end
	else
		print("\ndefault\n")
		return false
	end

    -- 刷新房卡
    if PriRoom and GlobalUserItem.bPrivateRoom then
        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
            self._gameView._priView:onRefreshInfo()
        end
    end

	self:dismissPopWait()

	return true
end

function GameLayer:setPiaoCount(chairID, piaoCount)
    self.m_piaoCount["chairID_" .. tostring(chairID)] = piaoCount
    print("设置飘 座位 : " .. chairID .. " 飘数 : " .. piaoCount)
    dump(self.m_piaoCount, "m_piaoCount")
end

function GameLayer:getPiaoCount(chairID)
	dump(self.m_piaoCount, "m_piaoCount")
    local ret = self.m_piaoCount["chairID_" .. tostring(chairID)]
    print("ret = " .. tostring(ret) .. " chairID = " .. chairID)
    return ret
end

function GameLayer:addHuaCardArray(chairID, huaCardData, isReconnect)
	if huaCardData <= 0 then
		return
	end
    if not self.m_huaCardArray["bh_"..tostring(chairID)] then
    	self.m_huaCardArray["bh_"..tostring(chairID)] = {}
    end

    table.insert(self.m_huaCardArray["bh_"..tostring(chairID)], huaCardData)

    self._gameView:addHuaCard(chairID, self.m_huaCardArray["bh_"..tostring(chairID)], isReconnect)
end

function GameLayer:getHuaCardArray(chairID)
    return self.m_huaCardArray["bh_"..tostring(chairID)]
end

-- 游戏消息
function GameLayer:onEventGameMessage(sub, dataBuffer)
    -- body
	if sub == cmd.SUB_S_GAME_START then 					--游戏开始
		return self:onSubGameStart(dataBuffer)
	elseif sub == cmd.SUB_S_OUT_CARD then 					--用户出牌
		return self:onSubOutCard(dataBuffer)
	elseif sub == cmd.SUB_S_SEND_CARD then 					--发送扑克
		return self:onSubSendCard(dataBuffer)
	elseif sub == cmd.SUB_S_OPERATE_NOTIFY then 			--操作提示
		return self:onSubOperateNotify(dataBuffer)
	elseif sub == cmd.SUB_S_HU_CARD then 					--听牌提示
		return self:onSubListenNotify(dataBuffer)
	elseif sub == cmd.SUB_S_OPERATE_RESULT then 			--操作命令
		return self:onSubOperateResult(dataBuffer)
	elseif sub == cmd.SUB_S_LISTEN_CARD then 				--用户听牌
		return self:onSubListenCard(dataBuffer)
	elseif sub == cmd.SUB_S_GET_HUACARD then 				--获得花牌
		return self:onSubGetHuaCard(dataBuffer)
	elseif sub == cmd.SUB_S_GAME_CONCLUDE then 				--游戏结束
		return self:onSubGameConclude(dataBuffer)
	elseif sub == cmd.SUB_S_RECORD then 					--游戏记录
		return self:onSubGameRecord(dataBuffer)
	elseif sub == cmd.SUB_S_SET_PIAO then 					--设置飘
		return self:onSetPiaoCard(dataBuffer)
	elseif sub == yl.SUB_S_DETECT_SOCKET then 				--心跳
		self:gotHeartBeatTimer()
	else
		assert(false, "default")
	end

	return false
end

function GameLayer:onSetPiaoCard(dataBuffer)
	local cmd_data = ExternalFun.read_netdata(cmd.CMD_S_PiaoCard, dataBuffer)
	print("添加飘 cmd_data.wPiaoUser = " .. cmd_data.wPiaoUser .. " cmd_data.wPiaoTimes = " .. cmd_data.wPiaoTimes)
	self._gameView:addPiaoTimes(cmd_data.wPiaoUser, cmd_data.wPiaoTimes)
	return true
end

function GameLayer:setLaizi(args)
	return args
    -- if args == 9 then
    --   	return 1
    -- elseif args == 18 then
    --  	return 10
    -- elseif args == 27 then
    --  	return 19
    -- elseif args == 32 then
    --  	return 32
    -- else 
        
    -- end
end
function GameLayer:showLaizi(args)
	return args
    -- if args == 8 then
    --   	return 0
    -- elseif args == 17 then
    --  	return 19
    -- elseif args == 26 then
    --  	return 18
    -- elseif args == 33 then
    --  	return 27
    -- else 
        
    -- end
end
--游戏开始
function GameLayer:onSubGameStart(dataBuffer)
	if (self._isReplay and self._gameStartUserChairId == GameplayerManager:getInstance():getSelfChairID()) or not self._isReplay then
		if self._isReplay then
			if self._gameView._priView and self._gameView._priView.onRefreshInfo then
				self._gameView._priView:onRefreshInfo()
			end
		end
		self.m_isHaiDiLao = false
		print("游戏开始")
		self._scene.m_gameAlreadyStart = true
		self._scene.m_gameInResultLayer = false
		self.isGameEnd = false
		--如果游戏开始，但结算界面仍然存在，那么将其删除
		self._gameView._resultLayer:hideLayer(false)
		self._gameView.startBtnLayout:setVisible(false)

		self._gameView:playStartGameEffect()


	    self.m_cbGameStatus = cmd.GAME_SCENE_PLAY
	    local cmd_data = nil
	    if self._isReplay then
	    	cmd_data = dataBuffer
	    else
	    	cmd_data = ExternalFun.read_netdata(cmd.CMD_S_GameStart, dataBuffer)
	    end

		--dump(cmd_data, "CMD_S_GameStart")
		for i = 1, cmd.GAME_PLAYER do
			local viewId = self:SwitchViewChairID(i - 1)
			local head = cmd_data.cbHeapCardInfo[i][1]
			local tail = cmd_data.cbHeapCardInfo[i][2]
		end
		
		self.wBankerUser = cmd_data.wBankerUser
		print("游戏开始 self.wBankerUser = " .. self.wBankerUser)
		local wViewBankerUser = self:SwitchViewChairID(self.wBankerUser)
		self._gameView:setBanker(wViewBankerUser)
		local cbCardCount = {0, 0, 0, 0}

		if not self._isReplay then
			-- self.userWithChairId = {}
			for i = 1, cmd.GAME_PLAYER do
				local userItem = self._gameFrame:getTableUserItem(self:GetMeTableID(), i - 1)

				local wViewChairId = self:SwitchViewChairID(i - 1)
				self._gameView:OnUpdateUser(wViewChairId, userItem)
				if userItem then
					self.cbPlayStatus[wViewChairId] = 1
					cbCardCount[wViewChairId] = 13
					if wViewChairId == wViewBankerUser then
						cbCardCount[wViewChairId] = cbCardCount[wViewChairId] + 1
					end
				end
			end
		else --如果是重放，不需要更新头像
			for i = 1, cmd.GAME_PLAYER do
				local wViewChairId = self:SwitchViewChairID(i - 1)
				self.cbPlayStatus[wViewChairId] = 1
				cbCardCount[wViewChairId] = 13
				if wViewChairId == wViewBankerUser then
					cbCardCount[wViewChairId] = cbCardCount[wViewChairId] + 1
				end
				--如果是三人，则viewid = 1 没有牌
				if GameplayerManager:getInstance():getPlayerCount() == 3 then
					cbCardCount[1] = 0
				end
			end
		end

		if self.wBankerUser ~= self:GetMeChairID() then
			cmd_data.cbCardData[1][cmd.MAX_COUNT] = nil
		end

		--本局没有癞子 无锡34无效，无锡42无效
		if cmd_data.cbMagicIndex == 42 then
			GameLogic.MAGIC_DATA = -1
		else
			GameLogic.MAGIC_DATA = GameLogic.SwitchToCardData(cmd_data.cbMagicIndex)
		end

		GameLogic.MAGIC_INDEX = cmd_data.cbMagicIndex

		--剩余牌的数量
		self._gameView._cardLayer.nRemainCardNum = cmd_data.wCardCount
		print("self._gameView._cardLayer.nRemainCardNum = " .. self._gameView._cardLayer.nRemainCardNum)


	    print("游戏开始 癞子牌索引 = " .. cmd_data.cbMagicIndex)
		print("游戏开始 癞子牌实际点数 = " .. GameLogic.MAGIC_DATA)

		self._gameView:setLaiZi(cmd_data.cbMagicIndex)

		--筛子
		local cbSiceCount1 = math.mod(cmd_data.wSiceCount, 256)
		local cbSiceCount2 = math.floor(cmd_data.wSiceCount/256)
		--起始位置
		local wStartChairId = math.mod(self.wBankerUser + cbSiceCount1 + cbSiceCount2 - 1, cmd.GAME_PLAYER)
		local wStartViewId = self:SwitchViewChairID(wStartChairId)
		--起始位置数的起始牌
		local nStartCard = math.min(cbSiceCount1, cbSiceCount2)*2 + 1
		print("wStartViewId = " .. tostring(wStartViewId))
		print("nStartCard = " .. tostring(nStartCard))
		print("cmd_data.cbCardData[1] = " .. tostring(cmd_data.cbCardData[1]))
		dump(cbCardCount, "cbCardCount")
		print("cbSiceCount1 = " .. tostring(cbSiceCount1))
		print("cbSiceCount2 = " .. tostring(cbSiceCount2))
		--开始发牌
		self._gameView:gameStart(wStartViewId, nStartCard, cmd_data.cbCardData[1], cbCardCount, cbSiceCount1, cbSiceCount2)

		--记录已出现的牌
		self:insertAppearCard(cmd_data.cbCardData[1])

		self.wCurrentUser = cmd_data.wBankerUser
		self.cbActionMask = cmd_data.cbUserAction
		self.bMoPaiStatus = true
		self.bSendCardFinsh = false
		self:PlaySound(cmd.RES_PATH.."sound/GAME_START.mp3")
		-- 刷新房卡
	    if PriRoom and GlobalUserItem.bPrivateRoom then
	        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
	        	PriRoom:getInstance().m_tabPriData.dwPlayCount = cmd_data.wGameCount
	        	print("cmd_data.wGameCount = " .. cmd_data.wGameCount)
	            -- self._gameView._priView:onRefreshInfo()
	            self._gameView._priView:setQuanData(cmd_data.wGameCount)
	        end
	    end

	    --计时器
		self:SetGameClock(cmd_data.wBankerUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard) --开始游戏时调用，庄家先出牌
	end

	if self._isReplay and self._gameStartUserChairId ~= GameplayerManager:getInstance():getSelfChairID() then
		local cmd_data = dataBuffer

		local cbCardCount = 13
		if cmd_data.wBankerUser == self._gameStartUserChairId then
			cbCardCount = 14
		end
		--初始化其他玩家的手牌
		dump(cmd_data.cbCardData[1], "cmd_data.cbCardData")
		-- local wViewId = self:SwitchViewChairID(self._gameStartUserChairId)
		-- self._gameView._cardLayer:setHandCard(wViewId, cbCardCount, cmd_data.cbCardData[1])
	else
		--友盟统计玩过多少局
		MultiPlatform:getInstance():countEvent(yl.UMENG_KEY.PLAY_COUNT)
	end

	return true
end

--用户出牌
function GameLayer:onSubOutCard(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_OutCard, dataBuffer)
	end
	
	--dump(cmd_data, "CMD_S_OutCard")
	print("用户出牌", cmd_data.cbOutCardData)

	local wViewId = self:SwitchViewChairID(cmd_data.wOutCardUser)
	self._gameView:gameOutCard(wViewId, cmd_data.cbOutCardData)

	--记录已出现的牌
	if wViewId ~= cmd.MY_VIEWID then
		local cbAppearCard = {cmd_data.cbOutCardData}
		self:insertAppearCard(cbAppearCard)
	end

	self.bMoPaiStatus = false
	self:KillGameClock()
	self._gameView:HideGameBtn()
	self:PlaySound(cmd.RES_PATH.."sound/OUT_CARD.mp3")
	self:playCardDataSound(wViewId, cmd_data.cbOutCardData)
	--轮到下一个
	self.wCurrentUser = cmd_data.wOutCardUser
	local wTurnUser = nil
	if self.wCurrentUser >= self.cbPlayerCount - 1 then
		wTurnUser = 0
	else
		wTurnUser = self.wCurrentUser + 1
	end
	
	local wViewTurnUser = self:SwitchViewChairID(wTurnUser)
	--设置听牌
	self._gameView._cardLayer:promptListenOutCard(nil)
	if wViewId == cmd.MY_VIEWID then
		local cbPromptHuCard = self:getListenPromptHuCard(cmd_data.cbOutCardData)
		self._gameView:setListeningCard(cbPromptHuCard)
		--听牌数据置空
		self.cbListenPromptOutCard = {}
		self.cbListenCardList = {}
	end
	--设置时间
	self:SetGameClock(wTurnUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard)  --玩家出牌的时候调用
	return true
end

--发送扑克(抓牌)
function GameLayer:onSubSendCard(dataBuffer)
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_SendCard, dataBuffer)
	end
	
	dump(cmd_data, "CMD_S_SendCard")

	local sendCard = function()
		--判断是否为海底捞
		if tonumber(cmd_data.wSendCardUser) == 65535 then
			self.m_isHaiDiLao = true
		else
			self.m_isHaiDiLao = false
		end

		self.wCurrentUser = cmd_data.wCurrentUser
		local wCurrentViewId = self:SwitchViewChairID(self.wCurrentUser)
		--如果是皮子杠，就不加入手牌
		if cmd_data.bPiZiGang then

		else
			self._gameView:gameSendCard(wCurrentViewId, cmd_data.cbCardData, cmd_data.bTail)
		end

		self:SetGameClock(self.wCurrentUser, cmd.IDI_OUT_CARD, self.cbTimeOutCard) --给玩家发送牌的时候调用

		self._gameView:HideGameBtn()
		--如果当前用户是自己，那就提示
		if not cmd_data.bPiZiGang and self.wCurrentUser == self:GetMeChairID()  then
			self._gameView:recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbCardData, false)
			--自动胡牌
			if cmd_data.cbActionMask >= GameLogic.WIK_CHI_HU and self.bTrustee then
				self._gameView:onButtonClickedEvent(GameViewLayer.BT_WIN)
			end
		end

		--记录已出现的牌
		if not cmd_data.bPiZiGang and wCurrentViewId == cmd.MY_VIEWID then
			local cbAppearCard = {cmd_data.cbCardData}
			self:insertAppearCard(cbAppearCard)
		end

		self.bMoPaiStatus = true
		self:PlaySound(cmd.RES_PATH.."sound/PACK_CARD.mp3")
		if not cmd_data.bPiZiGang and cmd_data.bTail then
			self:playCardOperateSound(wOperateViewId, true, nil)
		end
	end

	--摸到此牌的前面存在N张花牌，则需要延迟N*0.9秒显示，如果是庄家，则延迟N*0.9+1.5秒
	local huaCount = cmd_data.cbHuaCount

	local action = cc.Sequence:create(
				cc.DelayTime:create(huaCount*0.9),
				cc.CallFunc:create(function()
					sendCard()
				end)
		)
	self:runAction(action)
	
	return true
end

--操作提示
function GameLayer:onSubOperateNotify(dataBuffer)
	print("操作提示 别人打的牌")
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_OperateNotify, dataBuffer)
	end
	dump(cmd_data, "CMD_S_OperateNotify")

	if self.bSendCardFinsh then 	--发牌完成
		self._gameView:recognizecbActionMask(cmd_data.cbActionMask, cmd_data.cbActionCard, true)
	else
		self.cbActionMask = cmd_data.cbActionMask
		self.cbActionCard = cmd_data.cbActionCard
	end
	return true
end

--听牌提示
function GameLayer:onSubListenNotify(dataBuffer)
	print("听牌提示")
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_Hu_Data, dataBuffer)
	end
	--dump(cmd_data, "CMD_S_Hu_Data")

	self.cbListenPromptOutCard = {}
	self.cbListenCardList = {}
	for i = 1, cmd_data.cbOutCardCount do
		self.cbListenPromptOutCard[i] = cmd_data.cbOutCardData[1][i]
		self.cbListenCardList[i] = {}
		for j = 1, cmd_data.cbHuCardCount[1][i] do
			self.cbListenCardList[i][j] = cmd_data.cbHuCardData[i][j]
		end
		print("self.cbListenCardList"..i, table.concat(self.cbListenCardList[i], ","))
	end
	print("self.cbListenPromptOutCard", table.concat(self.cbListenPromptOutCard, ","))

	return true
end

--操作结果
function GameLayer:onSubOperateResult(dataBuffer)
	print("操作结果")
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_OperateResult, dataBuffer)
	end
	--dump(cmd_data, "CMD_S_OperateResult")
	if cmd_data.cbOperateCode == GameLogic.WIK_NULL then
		assert(false, "没有操作也会进来？")
		return true
	end

	dump(cmd_data)

	local wOperateViewId = self:SwitchViewChairID(cmd_data.wOperateUser)
	if cmd_data.cbOperateCode < GameLogic.WIK_LISTEN then 		--并非听牌
		local nShowStatus = GameLogic.SHOW_NULL
		local data1 = cmd_data.cbOperateCard[1][1]
		local data2 = cmd_data.cbOperateCard[1][2]
		local data3 = cmd_data.cbOperateCard[1][3]

		print("操作结果 data1 = " .. data1)
		print("操作结果 data2 = " .. data2)
		print("操作结果 data3 = " .. data3)

		local cbOperateData = {}
		local cbRemoveData = {}
		if cmd_data.cbOperateCode == GameLogic.WIK_GANG then
			--皮子牌
			local piziData = GameLogic.getPiZiCardData()
			--如果杠牌是皮子牌，那么手上只移除两张
			if data1 == piziData then
				cbOperateData = {data1, data1, data1}
				cbRemoveData = {data1, data1}
			else
				cbOperateData = {data1, data1, data1, data1}
				cbRemoveData = {data1, data1, data1}
			end
			
			--获取操作的人的手牌数
			local cbCardCount = self._gameView._cardLayer.cbCardCount[wOperateViewId]
			print("获取操作的人的手牌数 = cbCardCount = " .. cbCardCount)
			--检查杠的类型
			if math.mod(cbCardCount - 2, 3) == 0 then
				if self._gameView._cardLayer:checkBumpOrBridgeCard(wOperateViewId, data1) then
					nShowStatus = GameLogic.SHOW_MING_GANG
				else
					nShowStatus = GameLogic.SHOW_AN_GANG
				end
			else
				nShowStatus = GameLogic.SHOW_FANG_GANG
			end
		elseif cmd_data.cbOperateCode == GameLogic.WIK_PENG then
			cbOperateData = {data1, data1, data1}
			cbRemoveData = {data1, data1}
			nShowStatus = GameLogic.SHOW_PENG
		elseif cmd_data.cbOperateCode == GameLogic.WIK_RIGHT then
			cbOperateData = {data2, data1, data3} --cmd_data.cbOperateCard[1]
			cbRemoveData = {data2, data3}
			nShowStatus = GameLogic.SHOW_CHI
		elseif cmd_data.cbOperateCode == GameLogic.WIK_CENTER then
			cbOperateData = {data2, data1, data3} --cmd_data.cbOperateCard[1]
			cbRemoveData = {data2, data3}
			nShowStatus = GameLogic.SHOW_CHI
		elseif cmd_data.cbOperateCode == GameLogic.WIK_LEFT then
			cbOperateData = {data2, data1, data3} -- cmd_data.cbOperateCard[1]
			
			cbRemoveData = {data2, data3}
			nShowStatus = GameLogic.SHOW_CHI
		end

		for k,v in pairs(cbOperateData) do
			print("吃牌 或 碰牌 ----- ", k , v)
		end

		local bAnGang = nShowStatus == GameLogic.SHOW_AN_GANG
		self._gameView._cardLayer:bumpOrBridgeCard(wOperateViewId, cbOperateData, nShowStatus)
		local bRemoveSuccess = false
		if nShowStatus == GameLogic.SHOW_AN_GANG then
			print("玩家是暗杠")
			self._gameView._cardLayer:removeHandCard(wOperateViewId, cbOperateData, false)
		elseif nShowStatus == GameLogic.SHOW_MING_GANG then
			print("玩家是明杠")
			self._gameView._cardLayer:removeHandCard(wOperateViewId, {data1}, false)
		else
			print("玩家是其他操作，吃或碰")
			self._gameView._cardLayer:removeHandCard(wOperateViewId, cbRemoveData, false)
			--self._gameView._cardLayer:recycleDiscard(self:SwitchViewChairID(cmd_data.wProvideUser))
			print("提供者不正常？", cmd_data.wProvideUser, self:GetMeChairID())
		end
		self:PlaySound(cmd.RES_PATH.."sound/SEND_CARD.mp3")
		self:playCardOperateSound(wOperateViewId, false, cmd_data.cbOperateCode)

		--记录已出现的牌
		if wOperateViewId ~= cmd.MY_VIEWID then
			if nShowStatus == GameLogic.SHOW_AN_GANG then
				self:insertAppearCard(cbOperateData)
			elseif nShowStatus == GameLogic.SHOW_MING_GANG then
				self:insertAppearCard({data1})
			else
				self:insertAppearCard(cbRemoveData)
			end
		end
		--提示听牌
		if wOperateViewId == cmd.MY_VIEWID and cmd_data.cbOperateCode == GameLogic.WIK_PENG then
			self._gameView._cardLayer:promptListenOutCard(self.cbListenPromptOutCard)
		end
	end
	self._gameView:showOperateFlag(wOperateViewId, cmd_data.cbOperateCode)

	local cbTime = self.cbTimeOutCard - self.cbTimeOperateCard
	self:SetGameClock(cmd_data.wOperateUser, cmd.IDI_OUT_CARD, cbTime > 0 and cbTime or self.cbTimeOutCard) --有碰、杠等操作的时候使用

	--当有两个玩家选择的时候，一个碰一个吃，碰家为先，别人碰了，吃家的操作提示关闭
	self._gameView:HideGameBtn()
	self._gameView:removeChoicesPopup()

	return true
end

--用户听牌
function GameLayer:onSubListenCard(dataBuffer)
	print("用户听牌")
	local cmd_data = ExternalFun.read_netdata(cmd.CMD_S_ListenCard, dataBuffer)
	dump(cmd_data, "CMD_S_ListenCard")
	return true
end

--用户托管
function GameLayer:onSubGetHuaCard(dataBuffer)
	print("获得花牌")
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_GET_HUACARD, dataBuffer)
	end

	dump(cmd_data, "CMD_S_GET_HUACARD")

	--补花的数目，根据此数目调整补花时间
	local huaCount = cmd_data.cbHuaCount

	local isStartGameAddHua = false
	--是否开局补花
	if not self.bSendCardFinsh or self._scene.m_gameAlreadyStart == false or self._scene.m_gameAlreadyStart == nil then
		isStartGameAddHua = true
	end

	--取第一个花
	local firstHuaData = cmd_data.cbHuaCard[1][1]

	--每次补花的间隔
	local spanTime = 0.9
	--如果是开局补花，再多延迟1.5秒
	local delayTime = 0
	if isStartGameAddHua then
		delayTime = 1.5
		--首次补花，一次性补，无需间隔
		spanTime = 0
	end
	
	--初始化补花开始索引
	local addIndex = 1

	function addHuaCallback()
		if addIndex <= 8 then
			local value = tonumber(cmd_data.cbHuaCard[1][addIndex])
	      	if value ~= 0 then
	      		if addIndex ~= 1 then
	      			delayTime = 0
	      		end
		      	--最多连续补8个
	      		local action = cc.Sequence:create(cc.DelayTime:create(delayTime),
	                      cc.CallFunc:create(function()
	                      	if isStartGameAddHua then
	                      		--减少一张牌
								self._gameView._cardLayer.nRemainCardNum = self._gameView._cardLayer.nRemainCardNum - huaCount
								self._gameView:setRemainCardNum(self._gameView._cardLayer.nRemainCardNum)
	                      	end
	                      	self:addHuaCardArray(cmd_data.wReplaceUser, value, false)	
	                      end),
	                      cc.DelayTime:create(spanTime),
	                      cc.CallFunc:create(function()
	                      	addHuaCallback()
	                      end))
				self:runAction(action)
				addIndex = addIndex + 1
	      	end
	    end
	end

	addHuaCallback()

	return true
end

--游戏结束
function GameLayer:onSubGameConclude(dataBuffer)
	self._gameView:HideGameBtn()
	self._gameView:removeChoicesPopup()
	
	self.isGameEnd = true
	--结束倒计时
	self:KillGameClock()
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_GameConclude, dataBuffer)
	end

	dump(cmd_data, "onSubGameConclude")

	local bMeWin = nil  	--nil：没人赢，false：有人赢但我没赢，true：我赢
	--剩余牌
	local cbTotalCardData = clone(GameLogic.TotalCardData)
	-- local cbRemainCard = GameLogic.RemoveCard(cbTotalCardData, self.cbAppearCardData)
	--提示胡牌标记
	for i = 1, cmd.GAME_PLAYER do
		local wViewChairId = self:SwitchViewChairID(i - 1)
		if cmd_data.cbChiHuKind[1][i] >= GameLogic.WIK_CHI_HU then
			bMeWin = false
			self:playCardOperateSound(wOperateViewId, false, GameLogic.WIK_CHI_HU, cmd_data.wProvideUser == cmd_data.wWinerUser)
			self._gameView:showOperateFlag(wViewChairId, GameLogic.WIK_CHI_HU, cmd_data.wProvideUser == cmd_data.wWinerUser)
			if wViewChairId == cmd.MY_VIEWID then
				bMeWin = true
			end
		end
	end
	--显示结算图层
	local resultList = {}
	local cbBpBgData = self._gameView._cardLayer:getBpBgCardData()
	for i = 1, cmd.GAME_PLAYER do
		local wViewChairId = self:SwitchViewChairID(i - 1)

		--总分
		local lScore = cmd_data.lGameScore[1][i]
		--杠的分
		local gScore = cmd_data.cbHuaCount[1][i]
		--飘的数目
		local piaoScore = cmd_data.cbPiaoCount[1][i]
		
		--胡牌的分
		local hScore = gScore

		local user = nil
		if self._isReplay then
			local userData = GameplayerManager:getInstance():getPlayerInfo()
			local aData = userData[i]
			if aData then
				--虚拟信息
				user = UserItem:create()
				user.szNickName = aData.szNickName
				user.dwUserID = aData.dwUserID
				user.cbUserStatus = aData.cbUserStatus
				user.wTableID = aData.wTableID
				user.wChairID = aData.wChairID
			end
		else
			user = self._gameFrame:getTableUserItem(self:GetMeTableID(), i - 1)
		end

		if user then
			local result = {}
			result.userItem = user
			result.lScore = lScore
			result.gScore = gScore
			result.hScore = hScore
			--飘数
			result.pScore = piaoScore
			--花牌数据
			result.huaArray = huaArray
			result.cbChHuKind = cmd_data.cbChiHuKind[1][i] --dwChiHuRight

			--没人胡，服务器传的65535
			if cmd_data.wWinerUser >= cmd.GAME_PLAYER then
				result.dwChiHuRight = 0
				result.cbCardData = {}
			else
				--TODO 不一定有人胡牌
				result.dwChiHuRight = cmd_data.dwChiHuRight[cmd_data.wWinerUser+1][1]
				result.cbCardData = {}
			end
			
			--手牌
			for j = 1, cmd_data.cbCardCount[1][i] do
				result.cbCardData[j] = cmd_data.cbHandCardData[i][j]
			end

			result.huaArray = {}
			--花牌
			for j = 1, 8 do
				result.huaArray[j] = cmd_data.cbHuaCardData[i][j]
			end

			--碰杠牌
			result.cbBpBgCardData = cbBpBgData[wViewChairId]
			--奖码
			result.cbAwardCard = {}

			--插入
			table.insert(resultList, result)
			--剩余牌里删掉对手的牌
			-- if wViewChairId ~= cmd.MY_VIEWID then
			-- 	cbRemainCard = GameLogic.RemoveCard(cbRemainCard, result.cbCardData)
			-- end
		end
	end

	local resultListTemp = Utils.deepCopy(resultList)
	local dtime = 1.0
	if cmd_data.wWinerUser == yl.INVALID_CHAIR then --流局
		self._gameView:playLiujuEffect()
		dtime = 1.5
	end
	--显示结算框
	self:runAction(cc.Sequence:create(cc.DelayTime:create(dtime), cc.CallFunc:create(function(ref)
		--游戏开始倒计时
		self:SetGameClock(self:GetMeChairID(), cmd.IDI_START_GAME, self.cbTimeStartGame) --游戏倒计时开始时，指针指向自己
		self._gameView._resultLayer:showLayer(resultListTemp, cbAwardCardTotal, nil, self.wBankerUser, cmd_data.cbProvideCard, cmd_data.wProvideUser, cmd_data.wWinerUser, self.wMeChairID) -- cmd_data.wWinerUser
	end)))
	--播放音效
	if bMeWin then
		self:PlaySound(cmd.RES_PATH.."sound/ZIMO_WIN.mp3")
	else
		self:PlaySound(cmd.RES_PATH.."sound/ZIMO_LOSE.mp3")
	end

	self.cbPlayStatus = {0, 0, 0, 0}
    self.bTrustee = false
    self.bSendCardFinsh = false
	self._gameView:gameConclude()

	self:SetGameClock(self:GetMeChairID(), cmd.IDI_START_GAME, self.cbTimeStartGame) --游戏倒计时结算时，指针指向自己
	--end

	self._gameView:setLaiziCardVisible(false)
	--局面结束时，隐藏操作按钮
	self._gameView:HideGameBtn()
	return true
end

--游戏记录（房卡）
function GameLayer:onSubGameRecord(dataBuffer)
	print("游戏记录")
	local cmd_data = nil
	if self._isReplay then
		cmd_data = dataBuffer
	else
		cmd_data = ExternalFun.read_netdata(cmd.CMD_S_Record, dataBuffer)
	end
	
	--dump(cmd_data, "CMD_S_Record")

	self.m_userRecord = {}
	local nInningsCount = cmd_data.nCount
	for i = 1, self.cbPlayerCount do
		self.m_userRecord[i] = {}
		self.m_userRecord[i].cbHuCount = cmd_data.cbHuCount[1][i]
		self.m_userRecord[i].cbMingGang = cmd_data.cbMingGang[1][i]
		self.m_userRecord[i].cbAnGang = cmd_data.cbAnGang[1][i]
		self.m_userRecord[i].cbMaCount = cmd_data.cbMaCount[1][i]
		self.m_userRecord[i].lDetailScore = {}
		for j = 1, nInningsCount do
			self.m_userRecord[i].lDetailScore[j] = cmd_data.lDetailScore[i][j]
		end
	end
	--dump(self.m_userRecord, "m_userRecord", 5)
end

--*****************************    普通函数     *********************************--
--发牌完成
function GameLayer:sendCardFinish()	
	--提示操作
	if self.cbActionMask then
		self._gameView:recognizecbActionMask(self.cbActionMask, self.cbActionCard, false)
	end

	--提示听牌
	if self.wBankerUser == self:GetMeChairID() then
		self._gameView._cardLayer:promptListenOutCard(self.cbListenPromptOutCard)
	else
		--非庄家
		local cbHuCardData = self.cbListenCardList[1]
		if cbHuCardData and #cbHuCardData > 0 then
			self._gameView:setListeningCard(cbHuCardData)
		end
	end

	self.bSendCardFinsh = true
end

--解析筛子
function GameLayer:analyseSice(wSiceCount)
	local cbSiceCount1 = math.mod(wSiceCount, 256)
	local cbSiceCount2 = math.floor(wSiceCount/256)
	return cbSiceCount1, cbSiceCount2
end

--设置操作时间
function GameLayer:SetGameOperateClock()
	self:SetGameClock(self:GetMeChairID(), cmd.IDI_OPERATE_CARD, self.cbTimeOperateCard) --设置当前操作者是自己
end

--播放麻将数据音效（哪张）
function GameLayer:playCardDataSound(viewId, cbCardData)
	local strGender = ""
	if self.cbGender[viewId] == 1 then
		strGender = "BOY"
	else
		strGender = "GIRL"
	end
	local color = {"W_", "S_", "T_", "F_"}
	local nCardColor = math.floor(cbCardData/16) + 1
	local nValue = math.mod(cbCardData, 16)
	if cbCardData == GameLogic.MAGIC_DATA then
		nValue = 5
	end
	local strFile = cmd.RES_PATH.."sound/"..strGender.."/"..color[nCardColor]..nValue..".mp3"
	self:PlaySound(strFile)
end
--播放麻将操作音效
function GameLayer:playCardOperateSound(viewId, bTail, operateCode, isZiMo)
	assert(operateCode ~= GameLogic.WIK_NULL)

	local strGender = ""
	if self.cbGender[viewId] == 1 then
		strGender = "BOY"
	else
		strGender = "GIRL"
	end
	local strName = ""
	if bTail then
		strName = "REPLACE.mp3"
	else
		if operateCode >= GameLogic.WIK_CHI_HU then
			local rNum = math.random(3)
			strName = "hu" .. rNum .. ".mp3"
			if isZiMo then
				rNum = math.random(2)
				strName = "zimo" .. rNum .. ".mp3"
			end

		elseif operateCode == GameLogic.WIK_LISTEN then
			strName = "TING.mp3"
		elseif operateCode == GameLogic.WIK_GANG then
			strName = "GANG.mp3"
		elseif operateCode == GameLogic.WIK_PENG then
			strName = "PENG.mp3"
		elseif operateCode <= GameLogic.WIK_RIGHT then
			strName = "CHI.mp3"
		end
	end
	local strFile = cmd.RES_PATH.."sound/"..strGender.."/"..strName
	self:PlaySound(strFile)
end
--播放随机聊天音效
function GameLayer:playRandomSound(viewId)
	local strGender = ""
	if self.cbGender[viewId] == 1 then
		strGender = "BOY"
	else
		strGender = "GIRL"
	end
	local nRand = math.random(25) - 1
	if nRand <= 6 then
		local num = 6603000 + nRand
		local strName = num..".mp3"
		local strFile = cmd.RES_PATH.."sound/PhraseVoice/"..strGender.."/"..strName
		-- self:PlaySound(strFile)
	end
end

--插入到已出现牌中
function GameLayer:insertAppearCard(cbCardData)
	assert(type(cbCardData) == "table")
	for i = 1, #cbCardData do
		table.insert(self.cbAppearCardData, cbCardData[i])
		--self._gameView:reduceListenCardNum(cbCardData[i])
	end
	table.sort(self.cbAppearCardData)
	local str = ""
	for i = 1, #self.cbAppearCardData do
		str = str..string.format("%x,", self.cbAppearCardData[i])
	end
	--print("已出现的牌:", str)
end

function GameLayer:getDetailScore()
	return self.m_userRecord
end

function GameLayer:getListenPromptOutCard()
	return self.cbListenPromptOutCard
end

function GameLayer:getListenPromptHuCard(cbOutCard)
	if not cbOutCard then
		return nil
	end

	for i = 1, #self.cbListenPromptOutCard do
		if self.cbListenPromptOutCard[i] == cbOutCard then
			assert(#self.cbListenCardList > 0 and self.cbListenCardList[i] and #self.cbListenCardList[i] > 0)
			return self.cbListenCardList[i]
		end
	end

	return nil
end

-- 刷新房卡数据
function GameLayer:updatePriRoom()
    if PriRoom and GlobalUserItem.bPrivateRoom then
        if nil ~= self._gameView._priView and nil ~= self._gameView._priView.onRefreshInfo then
            self._gameView._priView:onRefreshInfo()
        end
    end
end

--*****************************    发送消息     *********************************--
--开始游戏
function GameLayer:sendGameStart(dataBuffer)
	self:SendUserReady(dataBuffer)
	self:OnResetGameEngine()
end
--出牌
function GameLayer:sendOutCard(card)
	print("GameLogic.MAGIC_DATA = " .. GameLogic.MAGIC_DATA)
	-- body
	if card == GameLogic.MAGIC_DATA then
		return false
	end

	if true == self.m_isHaiDiLao then
		showToast(self._scene, "海底捞期间无法出牌，请等待其他玩家操作", 2)
		return
	end

	self._gameView:HideGameBtn()
	print("发送出牌", card)

	local cmd_data = ExternalFun.create_netdata(cmd.CMD_C_OutCard)
	cmd_data:pushbyte(card)
	return self:SendData(cmd.SUB_C_OUT_CARD, cmd_data)
end
--操作扑克
function GameLayer:sendOperateCard(cbOperateCode, cbOperateCard)
	print("发送操作提示：", cbOperateCode, table.concat(cbOperateCard, ","))
	assert(type(cbOperateCard) == "table")

	--听牌数据置空
	self.cbListenPromptOutCard = {}
	self.cbListenCardList = {}
	self._gameView:setListeningCard(nil)
	self._gameView._cardLayer:promptListenOutCard(nil)

	--发送操作
	--local cmd_data = ExternalFun.create_netdata(cmd.CMD_C_OperateCard)
    local cmd_data = CCmd_Data:create(4)
	cmd_data:pushbyte(cbOperateCode)
	for i = 1, 3 do
		cmd_data:pushbyte(cbOperateCard[i])
	end
	--dump(cmd_data, "operate")
	self:SendData(cmd.SUB_C_OPERATE_CARD, cmd_data)
end
--用户听牌
-- function GameLayer:sendUserListenCard(bListen)
-- 	local cmd_data = CCmd_Data:create(1)
-- 	cmd_data:pushbool(bListen)
-- 	self:SendData(cmd.SUB_C_LISTEN_CARD, cmd_data)
-- end
--用户托管
function GameLayer:sendUserTrustee(isTrustee)
	if not self.bSendCardFinsh then
		return
	end
	
	local cmd_data = CCmd_Data:create(1)
	--cmd_data:pushbool(not self.bTrustee)
	cmd_data:pushbool(isTrustee)
	self:SendData(cmd.SUB_C_TRUSTEE, cmd_data)
end
--用户补牌
-- function GameLayer:sendUserReplaceCard(card)
-- 	local cmd_data = ExternalFun.create_netdate(cmd.CMD_C_ReplaceCard)
-- 	cmd_data:pushbyte(card)
-- 	self:SendData(cmd.SUB_C_REPLACE_CARD, cmd_data)
-- end
--发送扑克
-- function GameLayer:sendControlCard(cbControlGameCount, cbCardCount, wBankerUser, cbCardData)
-- 	local cmd_data = ExternalFun.create_netdata(cmd.CMD_C_SendCard)
-- 	cmd_data:pushbyte(cbControlGameCount)
-- 	cmd_data:pushbyte(cbCardCount)
-- 	cmd_data:pushword(wBankerUser)
-- 	for i = 1, #cbCardData do
-- 		cmd_data:pushbyte(cbCardData[i])
-- 	end
-- 	self:SendData(cmd.SUB_C_SEND_CARD, cmd_data)
-- end

return GameLayer