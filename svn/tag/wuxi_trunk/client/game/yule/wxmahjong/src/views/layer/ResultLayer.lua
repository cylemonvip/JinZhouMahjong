local cmd = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.CMD_Game")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC.."ExternalFun")
local CardLayer = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.views.layer.CardLayer")
local PopupInfoHead = appdf.req("client.src.external.PopupInfoHead")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.GameLogic")
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

local ClipText = appdf.req("client.src.external.ClipText")

local ResultLayer = class("ResultLayer", function(scene)
	local resultLayer = cc.CSLoader:createNode(cmd.RES_PATH.."gameResult/GameResultLayer.csb")
	return resultLayer
end)

ResultLayer.TAG_NODE_USER_1					= 1
ResultLayer.TAG_NODE_USER_2					= 2
ResultLayer.TAG_NODE_USER_3					= 3
ResultLayer.TAG_NODE_USER_4					= 4
ResultLayer.TAG_SP_ROOMHOST					= 5
ResultLayer.TAG_SP_BANKER					= 6

ResultLayer.TAG_SP_HEADCOVER				= 1
ResultLayer.TAG_TEXT_NICKNAME				= 2
ResultLayer.TAG_HEAD 						= 4
ResultLayer.TAG_NODE_CARD					= 5

ResultLayer.WINNER_ORDER					= 1

local posBanker = {cc.p(100, 590), cc.p(100, 470), cc.p(100, 355), cc.p(100, 235)}

function ResultLayer:onInitData()
	--body
	self.winnerIndex = nil
	self.bShield = false
end

function ResultLayer:onResetData()
	--body
	self.winnerIndex = nil
	self.bShield = false
	self.nodeAwardCard:removeAllChildren()
	self.nodeRemainCard:removeAllChildren()
	for i = 1, cmd.GAME_PLAYER do
		self.nodeUser[i]:getChildByTag(ResultLayer.TAG_NODE_CARD):removeAllChildren()
	end
end

function ResultLayer:ctor(scene)
	self._scene = scene
	self:onInitData()
	ExternalFun.registerTouchEvent(self, true)

	local btRecodeShow = self:getChildByName("Button_recodeShow")
	btRecodeShow:addClickEventListener(function(ref)
		self:recodeShow()
	end)

	--继续按钮
	local btContinue = self:getChildByName("Button_continue")
	-- btContinue:setPosition(cc.p(667, 54))
	--btContinue:setPositionX(display.cx)
	btContinue:addClickEventListener(function(ref)
		self:continueBtnCallback(ref)
	end)

	self.nodeUser = {}
	for i = 1, cmd.GAME_PLAYER do
		self.nodeUser[i] = self:getChildByTag(ResultLayer.TAG_NODE_USER_1 + i - 1)
		self.nodeUser[i]:setLocalZOrder(1)
		self.nodeUser[i]:getChildByTag(ResultLayer.TAG_SP_HEADCOVER):setLocalZOrder(1)
		--个人麻将
		local nodeUserCard = cc.Node:create()
			:setTag(ResultLayer.TAG_NODE_CARD)
			:addTo(self.nodeUser[i])
	end
	--奖码
	self.nodeAwardCard = cc.Node:create():addTo(self)
	--剩余麻将
	self.nodeRemainCard = cc.Node:create():addTo(self)
	--庄标志
	self.spBanker = self:getChildByTag(ResultLayer.TAG_SP_BANKER):setLocalZOrder(1)

	self:getChildByName("Text_ResultDJS"):setVisible(false)
end

function ResultLayer:continueBtnCallback(ref)
	
	print("_gameFrame = " .. tostring(self._scene._scene._gameFrame))
	print("_isReplay = " .. tostring(self._scene._scene._isReplay))
	if self._scene._scene._isReplay or self._scene._scene._gameFrame == nil then
		self._scene._scene:closeReplay()
	else
		self:hideLayer()
		--重置花牌
		for i = 1, cmd.GAME_PLAYER do
			self._scene:setHuaCardZoomUI(i-1, 0)
		end
		--重置花数据
		self._scene._scene.m_huaCardArray = {}
		self._scene:onResetData()
		local myPiaoTimes = self._scene._scene:getPiaoCount(self._scene._scene:GetMeChairID())

		if not self._scene._scene.m_CanPiao then
			self._scene:sendReady(0)
		elseif myPiaoTimes and myPiaoTimes > 0 then --如果已经有飘了，直接准备
			self._scene:sendReady(myPiaoTimes)
		else
			local btnNoPiao = self._scene.startBtnLayout:getChildByName("BtnNoPiao")
			local btnPiao5 = self._scene.startBtnLayout:getChildByName("BtnPiao5")
			local btnPiao10 = self._scene.startBtnLayout:getChildByName("BtnPiao10")
			local btnPiao15 = self._scene.startBtnLayout:getChildByName("BtnPiao15")
			local btnStart = self._scene.startBtnLayout:getChildByName("BtnStart")

			btnNoPiao:setVisible(self._scene._scene.m_CanPiao)
			btnPiao5:setVisible(self._scene._scene.m_CanPiao)
			btnPiao10:setVisible(self._scene._scene.m_CanPiao)
			btnPiao15:setVisible(self._scene._scene.m_CanPiao)

			btnStart:setVisible(not self._scene._scene.m_CanPiao)
		end
	end
	
end

function ResultLayer:onTouchBegan(touch, event)
	local pos = touch:getLocation()
	--print(pos.x, pos.y)
	local rect = cc.rect(17, 25, 1330, 750)
	if not cc.rectContainsPoint(rect, pos) then
		self:hideLayer()
	end
	return self.bShield
end

function ResultLayer:onClockUpdate(time)
	self:getChildByName("Text_ResultDJS"):setString(time)
	self:getChildByName("Text_ResultDJS"):setVisible(true)
end

function ResultLayer:showLayer(resultList, cbAwardCard, cbRemainCard, wBankerChairId, cbHuCard, wProvideUser, wWinerUser, meCharid)
	assert(type(resultList) == "table") --and type(cbAwardCard) == "table"   and type(cbRemainCard) == "table"
	self:setPosition(cc.p(0, 750))
	cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
	self:getChildByName("Text_ResultDJS"):setVisible(false)

	local txtDate = self:getChildByName("TxtTime")
	local date=os.date("%Y.%m.%d %H:%M")
	txtDate:setString("日期: " .. tostring(date))

	local mgr = self._scene._scene._scene:getApp():getVersionMgr()
	local nVersion = mgr:getResVersion(cmd.KIND_ID) or "0"
	local strVersion = "版本:"..appdf.BASE_C_VERSION.."."..mgr:getResVersion()

	local txtVersion = self:getChildByName("TxtVersion")
	txtVersion:setString(strVersion)

	local width = 44
	local height = 67
	if self._scene._cardLayer._mjSkin == 1 then
		width = 37
		height = 59
	elseif self._scene._cardLayer._mjSkin == 3 then
		width = 37
		height = 59
	end
	for i = 1, #resultList do
		if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
			self.winnerIndex = i
			break
		end
	end

	local winBackground = self:getChildByName("win_background")
	local spHupaiType = winBackground:getChildByName("sp_hupaitype")
	spHupaiType:setVisible(false)

	local nBankerOrder = 1
	for i = 1, cmd.GAME_PLAYER do
		local order = self:switchToOrder(i)
		if i <= #resultList then
			self.nodeUser[i]:setVisible(true)
			
			--头像
			local head = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_HEAD)
			if head then
				head:updateHead(resultList[i].userItem)
			else
				if self._scene._scene._isReplay then
					head = PopupInfoHead:createReplayHead(resultList[i].userItem.dwUserID, nil, 86)
				else
					head = PopupInfoHead:createNormal(resultList[i].userItem, 86)
				end
				head:setPosition(0, 0)			--初始位置
				head:enableHeadFrame(false)
				head:enableInfoPop(false)
				head:setTag(ResultLayer.TAG_HEAD)
				self.nodeUser[i]:addChild(head)
			end

			local textZongFen = self.nodeUser[i]:getChildByName("Text_zongfen")
			local zongDataFen = math.abs(resultList[i].lScore)
			textZongFen:setString(zongDataFen)
			local textZongFenPosX = textZongFen:getPositionX()
			local textZongFenPosY = textZongFen:getPositionY()
			local textZongFenSize = textZongFen:getContentSize()

			--输赢积分
			local preImagePath = ""
			if resultList[i].lScore > 0 then
				preImagePath = "game/yule/wxmahjong/res/gameResult/add.png"
			else
				preImagePath = "game/yule/wxmahjong/res/gameResult/decr.png"
			end

			local preImageSp = textZongFen:getParent():getChildByName("PRE_IMAGE_SPRITE")
			if preImageSp then
				preImageSp:removeFromParent()
			end

			if tonumber(resultList[i].lScore) ~= 0 then
				local preImageSprite = cc.Sprite:create(preImagePath)
				preImageSprite:setName("PRE_IMAGE_SPRITE")
				preImageSprite:setPosition(cc.p(textZongFenPosX - textZongFenSize.width/2 - 8, textZongFenPosY))
				preImageSprite:addTo(textZongFen:getParent(), textZongFen:getLocalZOrder())
			end
			

			local spHua = self.nodeUser[i]:getChildByName("SpHua")
			local huaTxt = spHua:getChildByName("Txt")

			local spPiao = self.nodeUser[i]:getChildByName("SpPiao")
			local chairIDTemp = i - 1
			--local piaoNum = self._scene._scene:getPiaoCount(chairIDTemp)
			local piaoNum = resultList[i].pScore
			if piaoNum and (tostring(piaoNum) == "5" or tostring(piaoNum) == "10" or tostring(piaoNum) == "15") then
				spPiao:setTexture("game/yule/wxmahjong/res/game/piao/p" .. piaoNum ..".png")	
				spPiao:setVisible(true)
			else
				spPiao:setVisible(false)
			end

			--此人有胡牌
			if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
				local str = ""
				if wProvideUser == wWinerUser then
					str = "自摸 x2 "
				end
				str = str .. "花数 x " .. resultList[i].gScore
				huaTxt:setString(str)
			else
				huaTxt:setString("")
			end

			--昵称
			local textNickname = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_TEXT_NICKNAME)
			textNickname:setVisible(false)

			local nick =  ClipText:createClipText(cc.size(101, 23),resultList[i].userItem.szNickName,"fonts/round_body.ttf",19)
			nick:setAnchorPoint(cc.p(0.5,0.5))
			nick:setPosition(textNickname:getPosition())
			nick:setTextColor(cc.c3b(254, 255, 157))
			nick.m_text:enableOutline(cc.c4b(0,0,0,255), 1)
			self.nodeUser[i]:addChild(nick)

			--获取碰了的牌
			local userData = resultList[i].cbBpBgCardData

			local tempUserData = {}

			local tempOne = {}
			--按类型分开
			for u, v in ipairs(userData) do
				if u == 1 then
					tempOne = {}
				end
				table.insert(tempOne, v)
				if v == -1 or v == -2 then
					table.insert(tempUserData, tempOne)
					tempOne = {}
				end
			end

			local fontSmallFile = "font_small"
			if self._scene._cardLayer._mjSkin == 1 then
				fontSmallFile = "font_small_1"
			elseif self._scene._cardLayer._mjSkin == 2 then
				fontSmallFile = "font_small_2"
			elseif self._scene._cardLayer._mjSkin == 3 then
				fontSmallFile = "font_small_3"
			end

			local nodeUserCard = self.nodeUser[i]:getChildByTag(ResultLayer.TAG_NODE_CARD)
			local fX = 80
			local fY = -22
			for i,v in ipairs(tempUserData) do
				for l, m in ipairs(v) do
					if l ~= #v then
						local card = nil
						--暗杠
						if v[#v] == -2 then
							if l == #v - 1 then
								local fxx = fX - width
								if l == 3 then
									fxx = fX - width/2 - width
								elseif l == 4 then
									fxx = fX - width - width
								end
								
								fX = fX - width
								
								card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
									:move(fxx, fY + 16)
									:addTo(nodeUserCard)

								local cardSize = card:getContentSize()
								local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(m))
								display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
									:move(cardSize.width/2, cardSize.height/2 + 8)
									:addTo(card)
							else
								card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_back.png")
									:move(fX, fY)
									:addTo(nodeUserCard)
							end
						else
							card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
								:move(fX, fY)
								:addTo(nodeUserCard)

							local cardSize = card:getContentSize()
							local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(m))
							display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
								:move(cardSize.width/2, cardSize.height/2 + 8)
								:addTo(card)
						end
						fX = fX + width
					end
				end
				fX = fX + 10
			end

			local huipaiFlagDx = 0
			local huipaiFlagDy = 2
			local ddy = 6
			if self._scene._cardLayer._mjSkin == 1 then
				ddy = 7
				huipaiFlagDx = 3
				huipaiFlagDy = -3
			elseif self._scene._cardLayer._mjSkin == 3 then
				ddy = 7
				huipaiFlagDx = 3
				huipaiFlagDy = -3
			end
			
			for j = 1, #resultList[i].cbCardData do  											 	--剩余手牌
				--牌底
				local card = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
					:move(fX, fY)
					:addTo(nodeUserCard)

				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(resultList[i].cbCardData[j]))
				local cardSize = card:getContentSize()
				display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
					:move(cardSize.width/2, cardSize.height/2 + ddy)
					:addTo(card)

				card:removeChildByTag(666)

				if resultList[i].cbCardData[j] == GameLogic.MAGIC_DATA then
					local size = card:getContentSize()
					local sHuiPaiFlag = display.newSprite("#r_s_up_huipai.png")
					    :move(size.width/2 + huipaiFlagDx, size.height/2+huipaiFlagDy)
					    :setTag(666)
					    :addTo(card)    
					print("这一张是癞子牌 ------ GameLogic.MAGIC_DATA = " .. GameLogic.MAGIC_DATA .. " nValue = " .. nValue)
				end

				fX = fX + width
			end

			local resCard = 
			{
				cmd.RES_PATH.."game/font_small/card_down.png"
			}
			local resFont = 
			{
				cmd.RES_PATH.."game/font_small/"
			}

			if self._mjSkin == 1 then
				resCard = 
				{
					cmd.RES_PATH.."game/font_small_1/card_down.png"
				}

				resFont = 
				{
					cmd.RES_PATH.."game/font_small_1/"
				}
			elseif self._mjSkin == 2 then
				resCard = 
				{
					cmd.RES_PATH.."game/font_small_2/card_down.png"
				}

				resFont = 
				{
					cmd.RES_PATH.."game/font_small_2/"
				}
			elseif self._mjSkin == 3 then
				resCard = 
				{
					cmd.RES_PATH.."game/font_small_3/card_down.png"
				}

				resFont = 
				{
					cmd.RES_PATH.."game/font_small_3/"
				}
			end

			-- resultList[i].huaArray = {65,66,67,68,69,70,71,72}
			local vaidHuaCardCount = 0
			--遍历有效花牌数
			for p = 1, #resultList[i].huaArray do
				if tonumber(resultList[i].huaArray[p]) ~= 0 then
					vaidHuaCardCount = vaidHuaCardCount + 1
				end
			end

			local huaCardScaleRate = 0.7
			local huaCardStartY = -18
			--放花牌的位置和缩放比
			if vaidHuaCardCount <= 4 then
				huaCardScaleRate = 0.7
				huaCardStartY = -18
			else
				huaCardScaleRate = 0.6
				huaCardStartY = 0
			end

			for p = 1, 8 do
				local child = self.nodeUser[i]:getChildByName("HUA_CARD_"..p)
				if child then
					child:removeFromParent()
				end
			end
			
			--显示花牌
			for p = 1, #resultList[i].huaArray do
				if tonumber(resultList[i].huaArray[p]) ~= 0 then
					local card = cc.Sprite:create(resCard[1])
					local size = card:getContentSize()
					card:setScale(huaCardScaleRate)
					local offsetHuaY = 0
					local xIndex = p
					if p > 4 then
						offsetHuaY = size.height * huaCardScaleRate-10
						xIndex = xIndex - 4
					end
					card:move(760 + (xIndex-1) * size.width*huaCardScaleRate, huaCardStartY - offsetHuaY)
					card:addTo(self.nodeUser[i])

					card:setName("HUA_CARD_"..p)

					local size = card:getContentSize()
					local rotate = 0
					local offsetX = 0
					local offsetY = 13

					local strFile = "game/yule/wxmahjong/res/game/hua_card/hua_card_" .. resultList[i].huaArray[p] ..".png"
					local font = cc.Sprite:create(strFile)
					font:setPosition(cc.p(size.width/2 + offsetX, size.height/2 + offsetY))
					font:setScale(0.4)
					font:setRotation(rotate)
					card:addChild(font)
				end
			end
			

			local cNode = self.nodeUser[i]:getChildByName("PROVIDEFLAG")
			if cNode then
				cNode:removeFromParent()
			end

			print("resultList[i].userItem.wChairID = " .. tostring(resultList[i].userItem.wChairID))
			print("wProvideUser = " .. wProvideUser)
			print("wWinerUser = " .. wWinerUser)
			print("self.winnerIndex = " .. tostring(self.winnerIndex))

			--先判断是否有赢家
			if self.winnerIndex then
				--如果当前遍历的玩家是提供者
				if resultList[i].userItem.wChairID and resultList[i].userItem.wChairID == wProvideUser then
					local isFangpao = false
					--自摸
					if wProvideUser == wWinerUser then
						hupaiFlagPath = "game/yule/wxmahjong/res/pics/sp_ziMo.png"
					else --放炮
						hupaiFlagPath = "game/yule/wxmahjong/res/pics/r_fangpao.png"
						isFangpao = true
					end
					print("放炮，自摸创建成果")
					local hupaiSp = display.newSprite(hupaiFlagPath)
						:setName("PROVIDEFLAG")
						:addTo(self.nodeUser[i], 20)

						hupaiSp:setPosition(1060, -7)
				end
			end

			--胡的那张牌
			if resultList[i].cbChHuKind >= GameLogic.WIK_CHI_HU then
				fX = fX + 20
				--牌底
				--local rectX = CardLayer:switchToCardRectX(cbHuCard)
				local huCard = display.newSprite(cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_down.png")
					--:setTextureRect(cc.rect(width*rectX, 0, width, height))
					:move(fX, fY)
					:addTo(nodeUserCard)
				--字体
				local huCardSize = huCard:getContentSize()
				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(cbHuCard))
				display.newSprite("game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png")
					:move(huCardSize.width/2, huCardSize.height/2 + 8)
					:addTo(huCard)

				if cbHuCard == GameLogic.MAGIC_DATA then
					local size = huCard:getContentSize()
					local sHuiPaiFlag = display.newSprite("#r_s_up_huipai.png")
					    :move(size.width/2 + huipaiFlagDx, size.height/2+huipaiFlagDy)
					    :setTag(666)
					    :addTo(huCard)
				end

				local hupaiFlagPath = ""
				-- print("meCharid = " .. meCharid)
				-- print("wProvideUser = " .. wProvideUser)
				-- print("wWinerUser = " .. wWinerUser)
				-- print("resultList[i].userItem.wChairID = " .. resultList[i].userItem.wChairID)

				nick:setTextColor(cc.c3b(216, 115,16))

				local winBackground = self:getChildByName("win_background")
				winBackground:setPositionY(self.nodeUser[i]:getPositionY() - 10)

				local spHupaiType = winBackground:getChildByName("sp_hupaitype")
				

				local htpType = GameLogic.getHPType(resultList[i].dwChiHuRight)
				local typePath = ""
				if htpType == GameLogic.HTP_QIDUI_HU then
					typePath = "r_hppinghu.png"
					if hasQiDuiRule then
						typePath = "r_hpqidui.png"
					end
				elseif htpType == GameLogic.HTP_HZ_HU then
					typePath = "r_hplou.png"
				elseif htpType == GameLogic.HTP_PIAO_HU then
					typePath = "r_hppiaohu.png"
				elseif htpType == GameLogic.HTP_DAN_DIAN then
					typePath = "r_hpdanza.png"
				elseif htpType == GameLogic.HTP_JIA_HU then
					typePath = "r_hpjiahu.png"
				elseif htpType == GameLogic.HTP_PING_HU then
					typePath = "r_hppinghu.png"
				elseif htpType == GameLogic.HTP_QING_YI_SE_HU then
					typePath = "r_hpqingyise.png"
				end

				if typePath ~= "" then
					typePath = "game/yule/wxmahjong/res/pics/r_hppinghu.png"
					spHupaiType:setTexture(typePath)
					spHupaiType:setVisible(true)
				else
					spHupaiType:setVisible(false)
				end
			end
			--麻将
			fX = 909-(width+3)*(self._scene._scene.cbMaCount/2) 
				
			--庄家
			if wBankerChairId == resultList[i].userItem.wChairID then
				print("庄家椅子号  wBankerChairId = " .. wBankerChairId)
				print("对比的玩家椅子号  wBankerChairId = " .. resultList[i].userItem.wChairID)
				print("i = " .. i)
				nBankerOrder = i
				print("nBankerOrder = " .. nBankerOrder)
			end
		else
			self.nodeUser[i]:setVisible(false)
		end
	end

	--庄家
	self:setBanker(nBankerOrder)

	self.bShield = true
	self:setVisible(true)
	self:setLocalZOrder(yl.MAX_INT)

	self:runAction(cc.Sequence:create(
		cc.MoveTo:create(0.3, cc.p(0, 0))
		))
end

function ResultLayer:hideLayer()
	if not self:isVisible() then
		return
	end
	self:onResetData()
	self:setVisible(false)
	self._scene.startBtnLayout:setVisible(true)
	self._scene._scene:KillGameClock()
end

--1~4转换到1~4
function ResultLayer:switchToOrder(index)
	assert(index >=1 and index <= cmd.GAME_PLAYER)
	print("self.winnerIndex = " .. tostring(self.winnerIndex))
	if self.winnerIndex == nil then
		return index
	end
	local nDifference = ResultLayer.WINNER_ORDER - self.winnerIndex - 1
	local order = math.mod(index + nDifference, cmd.GAME_PLAYER) + 1
	return order
end

function ResultLayer:setBanker(order)
	assert(order ~= 0)
	print("setBanker order = " .. order)
	self.spBanker:move(posBanker[order])
	self.spBanker:setVisible(true)
end

function ResultLayer:recodeShow()
	print("战绩炫耀")
	if not PriRoom then
		return
	end

    --GlobalUserItem.bAutoConnect = false
    PriRoom:getInstance():getPlazaScene():popTargetShare(function(target, bMyFriend)
        bMyFriend = bMyFriend or false
        local function sharecall( isok )
            if type(isok) == "string" and isok == "true" then
                showToast(self, "战绩炫耀成功", 2)
            end
            --GlobalUserItem.bAutoConnect = true
            yl.removeErWeiMa()
        end

        local url = yl.WXShareURL or GlobalUserItem.szWXSpreaderURL or yl.HTTP_URL
        -- 截图分享
        local framesize = cc.Director:getInstance():getOpenGLView():getFrameSize()
        local area = cc.rect(0, 0, framesize.width, framesize.height)
        local imagename = "grade_share.jpg"
        if bMyFriend then
            imagename = "grade_share_" .. os.time() .. ".jpg"
        end
        ExternalFun.popupTouchFilter(0, false)

        yl.addErWeiMa()

        captureScreenWithArea(area, imagename, function(ok, savepath)
            ExternalFun.dismissTouchFilter()
            if ok then
                if bMyFriend then
                    PriRoom:getInstance():getTagLayer(PriRoom.LAYTAG.LAYER_FRIENDLIST, function( frienddata )
                        PriRoom:getInstance():imageShareToFriend(frienddata, savepath, "分享我的战绩")
                    end)
                elseif nil ~= target then
                    MultiPlatform:getInstance():shareToTarget(target, sharecall, "我的战绩", "分享我的战绩", url, savepath, "true")
                end            
            end
        end)
    end)
end

return ResultLayer