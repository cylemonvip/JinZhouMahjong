local ChatNode = class("ChatNode", cc.Node)
local ChatItem = class("ChatItem", cc.Node)

local ExternalFun = require(appdf.EXTERNAL_SRC .. "ExternalFun")
local ClipText = appdf.req(appdf.EXTERNAL_SRC .. "ClipText")

local RES_CSB = "client/res/Chat/ChatNode.csb"

local GAMECHAT_LISTENER = "chatnode_listener"
local CHAT_ITEM_NAME = "game_chat_node_item"

local LEN_TABLEVIEW_WIDTH = 300

local SIZE_CHAT_LABEL = 25

local LEN_CHAT_WIDTH = 300

--聊天记录
local chatRecord = {}
--文本聊天内容
local textChatConfig = {}

local EmojFrameCount = {2, 4, 9, 3, 2, 7, 8, 8, 6, 6, 5, 3, 4, 2, 2, 7, 4, 7}

function ChatNode.playEmojAnimation(parent, pos, emojIndex)
	print("表情编号 emojIndex = ".. emojIndex)
	cc.SpriteFrameCache:getInstance():addSpriteFrames("client/res/Chat/femoj.plist")

	local frameName =string.format("femoj_%d_1.png", emojIndex)
	local sprite = cc.Sprite:createWithSpriteFrameName(frameName)  
   sprite:setPosition(pos)
   parent:addChild(sprite)

    local animation =cc.Animation:create()
    for i=1, EmojFrameCount[emojIndex] do  
        local frameName =string.format("femoj_%d_%d.png", emojIndex, i)
        print("frameName = " .. frameName)  
        local spriteFrame = cc.SpriteFrameCache:getInstance():getSpriteFrameByName(frameName)
       animation:addSpriteFrame(spriteFrame)
    end  

   animation:setDelayPerUnit(0.25)          --设置两个帧播放时间
   animation:setRestoreOriginalFrame(true)    --动画执行后还原初始状态

    local action =cc.Animate:create(animation)
    sprite:runAction(
    	cc.Sequence:create(
    		cc.Repeat:create(action, 3),
    		cc.RemoveSelf:create()
    		)
    	) 
end

-- 默认短语对比
function ChatNode.compareWithText(str)
    for k,v in pairs(textChatConfig) do
        if v.strChat == str then
            return (k - 1)
        end
    end
    return nil
end

function ChatNode.getRecordConfig()
	return 
	{
		bBrow = false,
		strChat = "",
		nIdx = 0,
		szNick = ""
	}
end

--添加聊天记录
function ChatNode.addChatRecord(rec)
	rec = rec or ChatNode.getRecordConfig()
	table.insert(chatRecord, 1, rec)

	--通知
	local eventListener = cc.EventCustom:new(GAMECHAT_LISTENER)
    cc.Director:getInstance():getEventDispatcher():dispatchEvent(eventListener)
end

function ChatNode.addChatRecordWith(cmdtable, bBrow)
	local rec = ChatNode.getRecordConfig()
	rec.bBrow = bBrow
	rec.szNick = cmdtable.szNick
	if bBrow then
		rec.nIdx = cmdtable.wItemIndex
	else
		rec.strChat = cmdtable.szChatString
	end

	ChatNode.addChatRecord(rec)
end

function ChatNode.loadTextChat()
	local str = cc.FileUtils:getInstance():getStringFromFile("public/chat_text.json")
	local ok, datatable = pcall(function()
			       return cjson.decode(str)
			    end)
	if true == ok and type(datatable) == "table" then
		for k,v in pairs(datatable) do
			local record = {}
			record.bBrow = false
			record.strChat = v
			textChatConfig[k] = record
		end
	else
		print("load text chat error!")
	end

	cc.SpriteFrameCache:getInstance():addSpriteFrames("public/brow.plist")
	--[[cc.SpriteFrameCache:getInstance():addSpriteFrames("public/brow.plist")

	--缓存表情
	local dict = cc.FileUtils:getInstance():getValueMapFromFile("public/brow.plist")
	local framesDict = dict["frames"]
	if nil ~= framesDict and type(framesDict) == "table" then
		for k,v in pairs(framesDict) do
			local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(k)
			if nil ~= frame then
				frame:retain()
			end
		end
	end]]
end
ChatNode.loadTextChat()


function ChatNode:ctor(netframe)
	if nil == netframe then
		print("net frame can't be nil")
		return
	end
	self.m_netframe = netframe
	self.m_recordTableView = nil
	self.m_isPopupIME = false

	self:registerScriptHandler(function(eventType)
		if eventType == "enterTransitionFinish" then	-- 进入场景而且过渡动画结束时候触发。			
						
		elseif eventType == "exitTransitionStart" then	-- 退出场景而且开始过渡动画时候触发。
			
		elseif eventType == "exit" then
			self:onExit()
		end
	end)

	self:initCSB()
	self:initTabLayout()
	self:onSelectTab(1)
	-- self:initRecord()

 --    self.m_listener = cc.EventListenerCustom:create(GAMECHAT_LISTENER,handler(self, function(target,event)
	-- 	self.m_recordTableView:reloadData()
 --        self:moveRecordToLastRow()
	-- end))
    -- cc.Director:getInstance():getEventDispatcher():addEventListenerWithSceneGraphPriority(self.m_listener, self)

    self.touchShild:setVisible(false)
end

function ChatNode:initRecord()
	if nil == self.m_recordTableView then
		local px, py = self.msgRecordLayout:getPosition()
		self.m_recordTableView = self:getDataTableView(self.msgRecordLayout:getContentSize(), cc.p(px, py))
		self.voiceLayout:addChild(self.m_recordTableView)
	end
	self.m_recordTableView:reloadData()
    self:moveRecordToLastRow()
end

function ChatNode:onExit()
	chatRecord = {}
	if nil ~= self.m_listener then
    	cc.Director:getInstance():getEventDispatcher():removeEventListener(self.m_listener)
    end
end

function ChatNode:getDataTableView(size, pos)
	--tableview
	local m_tableView = cc.TableView:create(size)
	m_tableView:setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL)
	-- m_tableView:setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN)
	m_tableView:setPosition(pos)
	m_tableView:setDelegate()
	m_tableView:registerScriptHandler(handler(self, self.cellSizeForTable), cc.TABLECELL_SIZE_FOR_INDEX)
	m_tableView:registerScriptHandler(handler(self, self.tableCellAtIndex), cc.TABLECELL_SIZE_AT_INDEX)
	m_tableView:registerScriptHandler(handler(self, self.tableCellTouched), cc.TABLECELL_TOUCHED)
	m_tableView:registerScriptHandler(handler(self, self.numberOfCellsInTableView), cc.NUMBER_OF_CELLS_IN_TABLEVIEW)
	return m_tableView
end

function ChatNode:cellSizeForTable( view, idx )
	local record = chatRecord[idx + 1]
	if nil ~= record then
		local wi, he, si = self:getTextChatSize(record.strChat)
		print("wi = " .. wi .. " hi = " .. (he + si))
		return wi, he + si
	end
	return 0, 0
end

function ChatNode:numberOfCellsInTableView( view )			
	return #chatRecord
end

function ChatNode:tableCellAtIndex(view, idx)	
	local cell = view:dequeueCell()
	local dataitem = nil

	if nil == cell then
		cell = cc.TableViewCell:new()
		dataitem = self:getDataItemAt(view,idx)
		if nil ~= dataitem then
			dataitem:setName(CHAT_ITEM_NAME)
			cell:addChild(dataitem)
		end
	else
		dataitem = cell:getChildByName(CHAT_ITEM_NAME)
	end
	if nil ~= dataitem then
		self:refreshDataItemAt(view, dataitem, idx)
	end

	cell:setTag(idx)
	return cell
end

function ChatNode:tableCellTouched(view, cell)
	
end


function ChatNode:refreshDataItemAt(view, item, idx)	
	if nil == item then
		return 
	end

	local record = chatRecord[idx + 1]
	if nil ~= record then
		item:refreshRecordItem(record.strChat, record.szNick)
	end

	local size = item:getContentSize()
	item:setPosition(size.width * 0.5, 0)
end

function ChatNode:getDataItemAt(view, idx)
	local chatitem = ChatItem:create(true)
	chatitem:setName(CHAT_ITEM_NAME)
	return chatitem
end

function ChatNode:getTextChatSize(str)
	print("getTextChatSize str = " .. str)
	local tmp = cc.LabelTTF:create(str, "fonts/round_body.ttf", SIZE_CHAT_LABEL, cc.size(LEN_CHAT_WIDTH,0))
	local tmpsize = tmp:getContentSize()
	tmp:setString("网")
	return LEN_TABLEVIEW_WIDTH, tmpsize.height + 15, tmp:getContentSize().height
end

function ChatNode:initCSB()
	local rootLayer, csbNode = ExternalFun.loadRootCSB(RES_CSB, self)

	local function btncallback(ref, tType)
		print("tType = " .. tostring(tType) .. " ref = "  .. tostring(ref))
        if tType == ccui.TouchEventType.ended then
         	self:onButtonCallback(ref)
        end
    end

    self.rootNode = csbNode:getChildByName("RootNode")
    self.touchShild = csbNode:getChildByName("TouchShild")
    self.emojItem = csbNode:getChildByName("EmojItemLayout")
    self.msgItemLayout = csbNode:getChildByName("MsgItemLayout")
    

    self.emojScrollView = self.rootNode:getChildByName("EmojScrollView")
    for i=1,18 do
    	local emojBtn = self.emojScrollView:getChildByName("Emoj_" .. i)
    	emojBtn.index = i
    	emojBtn:addTouchEventListener(function(ref, ttype)
	        self:onEmojTouched(ref, ttype)
    	end)
    end
	self.msgLayout = self.rootNode:getChildByName("MsgLayout")
	self.voiceLayout = self.rootNode:getChildByName("VoiceLayout")


    self.touchShild:addTouchEventListener(btncallback)
    

	local btnSend = self.voiceLayout:getChildByName("BtnSend")
	btnSend:addTouchEventListener(btncallback)

	self.tabEmoj = self.rootNode:getChildByName("TabEmoj")
	self.tabEmoj:addTouchEventListener(btncallback)
	self.tabEmoj.tabIndex = 1

	self.tabMsg = self.rootNode:getChildByName("TabMsg")
	self.tabMsg:addTouchEventListener(btncallback)
	self.tabMsg.tabIndex = 2

	self.tabVoice = self.rootNode:getChildByName("TabVoice")
	self.tabVoice:addTouchEventListener(btncallback)
	self.tabVoice.tabIndex = 3

	self.inputMsgField = self.voiceLayout:getChildByName("InputMsgField")
	self.inputMsgField:addEventListener(function(ref, ttype)
		if ttype == ccui.TextFiledEventType.attach_with_ime then
			print("吊起输入法")
			self:attachWithIme()
		elseif ttype == ccui.TextFiledEventType.detach_with_ime then
			print("关闭输入法")
			self:detachWithIme()
		elseif ttype == ccui.TextFiledEventType.insert_text then
			print("输入字符")
		elseif ttype == ccui.TextFiledEventType.delete_backward then
			print("删除按钮")
		end
	end)
	--消息记录
	self.msgRecordLayout = self.voiceLayout:getChildByName("MsgRecordLayout")
end

function ChatNode:attachWithIme()
	self.m_isPopupIME = true
	self.rootNode:stopAllActions()
	self.rootNode:runAction(cc.MoveTo:create(0.2, cc.p(0, 150)))
end
function ChatNode:detachWithIme()
	
	self.rootNode:stopAllActions()
	self.rootNode:runAction(
		cc.Sequence:create(
			cc.MoveTo:create(0.2, cc.p(0, 0)),
			cc.CallFunc:create(function()
				self.m_isPopupIME = false
			end)
			)
		)
end

function ChatNode:initTabLayout()
	-- --表情
	-- local itemSize = self.emojItem:getContentSize()

	-- local startX = 40
	-- local startY = 425
	-- local dis = 95
	-- --4行
	-- local row = 5
	-- --5列
	-- local col = 4

	-- --18个表情
	-- local MAXCOUNT = 18
	-- local emojScrollViewHeight = 475

	-- self.emojScrollView:setInnerContainerSize(cc.size(376, emojScrollViewHeight))

	
	-- for i = 1, row do
	-- 	for j = 1, col do
	-- 		local index = (i-1) * col + j
	-- 		print("emoj index = " .. index)
	-- 		if index <= MAXCOUNT and index > 0 then
	-- 			local emojItem = self.emojItem:clone()
	-- 			local fileName = "client/res/Chat/ltb/face_" .. index .. ".png"
	-- 			local sp = cc.Sprite:create(fileName)
	-- 			sp:setPosition(cc.p(itemSize.width/2, itemSize.height/2))
	-- 			emojItem:addChild(sp)
	-- 			emojItem.index = index
	-- 			emojItem:addTouchEventListener(function(ref, tType)
	-- 				self:onEmojTouched(ref, tType)
	-- 			end)
	-- 			local px = startX + (j-1) * dis
	-- 			local py = startY - (i-1) * dis
	-- 			emojItem:setPosition(cc.p(px, py))
	-- 			print("px = " .. px .. " py = " .. py)
	-- 			self.emojScrollView:addChild(emojItem)
	-- 		end
	-- 	end
	-- end
	

	--文字语音
	local startX = 577/2
	local msgLayoutHeight = (50) * #textChatConfig
	self.msgLayout:setInnerContainerSize(cc.size(577, msgLayoutHeight))

	for i, record in ipairs(textChatConfig) do
		local msgItem = self.msgItemLayout:clone()
		local txt = msgItem:getChildByName("Text")
		msgItem:setPosition(cc.p(startX, msgLayoutHeight - (i-1) * 50))
		txt:setString(record.strChat)
		msgItem.txt = record.strChat
		self.msgLayout:addChild(msgItem)
		msgItem:addTouchEventListener(function(ref, tType)
			self:onMsgTouched(ref, tType, i-1)
		end)
	end
end

function ChatNode:moveRecordToLastRow()    
    local container = self.m_recordTableView:getContainer()
    local needToLastRow = false
    if nil ~= container then
        needToLastRow = (container:getContentSize().height >= self.m_recordTableView:getViewSize().height)
        --self.m_recordTableView:setTouchEnabled(needToLastRow)
    end
    if needToLastRow then
        self.m_recordTableView:setContentOffset(cc.p(0,0),false)
    end
end

function ChatNode:onMsgTouched(ref, tType, index)
	if tType == ccui.TouchEventType.ended then
		self:sendTextChat(ref.txt, index)
		self:hide()
	end
end

function ChatNode:onEmojTouched(ref, tType)
	if tType == ccui.TouchEventType.ended then
     	print("emoj touch index = " .. tostring(ref.index))
     	self.m_netframe:sendBrowChat(ref.index)
     	self:hide()
    end
end

function ChatNode:onSelectTab(index)
	local tabEmojFlag = false
	local tabMsgFlag = false
	local tabVoiceFlag = false

	if index == 1 then 
		tabEmojFlag = true
	elseif index == 2 then 
		tabMsgFlag = true
	elseif index == 3 then
		tabVoiceFlag = true
	end

	print("tabEmojFlag = " .. tostring(tabEmojFlag))
	print("tabMsgFlag = " .. tostring(tabMsgFlag))
	print("tabVoiceFlag = " .. tostring(tabVoiceFlag))

	self.tabEmoj:setBright(not tabEmojFlag)
	self.tabMsg:setBright(not tabMsgFlag)
	self.tabVoice:setBright(not tabVoiceFlag)

	self.emojScrollView:setVisible(tabEmojFlag)
	self.msgLayout:setVisible(tabMsgFlag)
	self.voiceLayout:setVisible(tabVoiceFlag)
end

--发送文本聊
function ChatNode:sendTextChat(msg, index)
	print("sendTextChat ==== " .. msg)
	if nil ~= self.m_netframe and nil ~= self.m_netframe.sendTextChat then
		return self.m_netframe:sendTextChat(msg, nil, index)
	end
	return false, ""
end

function ChatNode:onButtonCallback(ref)
	local name = ref:getName()
	print("name = " .. name)
	if name == "BtnVoice" then
		
	elseif name == "BtnSend" then
		local chatstr = self.inputMsgField:getString()
		if "" ~= chatstr then
			cc.Director:getInstance():getOpenGLView():setIMEKeyboardState(false);
			self:detachWithIme()
			local valid, msg = self:sendTextChat(chatstr)
			if false == valid and type(msg) == "string" and "" ~= msg then
				showToast(cc.Director:getInstance():getRunningScene(), msg, 2)
			else
				self.inputMsgField:setString("")
			end
			self:hide()
		end
	elseif name == "TabEmoj" then
		self:onSelectTab(ref.tabIndex)
	elseif name == "TabMsg" then
		self:onSelectTab(ref.tabIndex)
	elseif name == "TabVoice" then
		self:onSelectTab(ref.tabIndex)
	elseif name == "TouchShild" then
		if self.m_isPopupIME then
			cc.Director:getInstance():getOpenGLView():setIMEKeyboardState(false)
			self:detachWithIme()
		else
			self:hide()
		end
		
	end
end

function ChatNode:show()
	self:setVisible(true)
	self.touchShild:setVisible(true)
	-- self.rootNode:runAction(
	-- 	cc.Sequence:create(
	-- 		cc.ScaleTo:create(0.3, 1.1),
	-- 		cc.ScaleTo:create(0.1, 1.0),
	-- 		cc.CallFunc:create(function()
	-- 			self.touchShild:setTouchEnabled(true)
	-- 		end)
	-- 		))
end

function ChatNode:hide()
	print("chatnode === >>> " .. debug.traceback())
	self.touchShild:setVisible(false)
	self:setVisible(false)
end

function ChatItem:ctor( bRecord )
    if not bRecord then
        local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame("chat_item_framebg.png")
        if nil ~= frame then
            self.m_spLine = cc.Sprite:createWithSpriteFrame(frame)
            self.m_spLine:setPosition(0, self.m_spLine:getContentSize().height * 0.5)
            self:addChild(self.m_spLine)
        end
    end

	self.m_labelChat = nil
	self.m_clipUserName = nil
end

function ChatItem:refreshTextItem(str,sendusernick)
	if nil == self.m_labelChat then
		self.m_labelChat = cc.LabelTTF:create(str, "fonts/round_body.ttf", SIZE_CHAT_LABEL, cc.size(LEN_CHAT_WIDTH,0), cc.TEXT_ALIGNMENT_LEFT)
		self.m_labelChat:setPosition(2, self.m_spLine:getContentSize().height * 0.5)

		self:addChild(self.m_labelChat)
	else
		self.m_labelChat:setString(str)
	end
	local labSize = self.m_labelChat:getContentSize()
	self:setContentSize(cc.size(LEN_TABLEVIEW_WIDTH, labSize.height + 15))
end

function ChatItem:refreshRecordItem(str,sendusernick)
    if nil == self.m_labelChat then
        self.m_labelChat = cc.LabelTTF:create(str, "fonts/round_body.ttf", 20, cc.size(LEN_CHAT_WIDTH,0), cc.TEXT_ALIGNMENT_LEFT)
        self.m_labelChat:setAnchorPoint(cc.p(0.5, 0))
        self.m_labelChat:setPositionY(5)

        self:addChild(self.m_labelChat)
    else
        self.m_labelChat:setString(str)
    end
    local labSize = self.m_labelChat:getContentSize()
    self:addSendUser(labSize.height + 10, sendusernick)
    if nil ~= sendusernick and type(sendusernick) == "string" then
        labSize.height = labSize.height + self.m_clipUserName:getContentSize().height + 5
    end
    self:setContentSize(cc.size(LEN_TABLEVIEW_WIDTH, labSize.height + 15))
end

function ChatItem:addSendUser(posHeight,sendusernick)
	if nil ~= sendusernick and type(sendusernick) == "string" then
		if nil == self.m_clipUserName then
			self.m_clipUserName = ClipText:createClipText(cc.size(200,20), sendusernick .. ":", "fonts/round_body.ttf", 20)
			self.m_clipUserName:setTextColor(cc.c3b(36, 236, 255))
			self.m_clipUserName:setAnchorPoint(cc.p(0, 0.5))
			self.m_clipUserName:setPositionX(-LEN_CHAT_WIDTH * 0.5)
			self:addChild(self.m_clipUserName)
		else
			self.m_clipUserName:setString(sendusernick .. ":")
		end

		self.m_clipUserName:setPositionY(posHeight + self.m_clipUserName:getContentSize().height * 0.5)		
	else
		if nil ~= self.m_clipUserName then
			self.m_clipUserName:setVisible(false)
		end
	end
end


return ChatNode