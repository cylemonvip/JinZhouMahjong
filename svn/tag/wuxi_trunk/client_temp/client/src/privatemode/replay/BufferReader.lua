local BufferReader = class("BufferReader")

function BufferReader:ctor()
	self._buffer = nil
	self._currIndex = 1
end

function BufferReader:setBuffer(buffer)
	--记录buffer
	self._buffer = buffer
	self._currIndex = 1

end

function BufferReader:readWord(alignSize)
	alignSize = alignSize or 2
	local ret = ""
	for i = 1, alignSize do
		ret = ret .. string.char(self._buffer[self._currIndex+(i-1)])
	end
	self._currIndex = self._currIndex + alignSize
	return ret
end

function BufferReader:readDword(alignSize)
	alignSize = alignSize or 4
	local ret = ""
	for i = 1, alignSize do
		ret = ret .. string.char(self._buffer[self._currIndex+(i-1)])
	end
	self._currIndex = self._currIndex + alignSize
	return ret
end

function BufferReader:readLong(alignSize)
	alignSize = alignSize or 4
	local ret = ""
	for i = 1, alignSize do
		ret = ret .. string.char(self._buffer[self._currIndex+(i-1)])
	end
	self._currIndex = self._currIndex + alignSize
	return ret
end

function BufferReader:readByte(alignSize)
	alignSize = alignSize or 1
	local ret = ""
	for i = 1, alignSize do
		ret = ret .. string.char(self._buffer[self._currIndex+(i-1)])
	end
	self._currIndex = self._currIndex + alignSize
	return ret
end

function BufferReader:readString(size)
	local ret = ""
	for i = 1, size * 2 do
		ret = ret .. string.char(self._buffer[self._currIndex+(i-1)])
	end
	self._currIndex = self._currIndex + size * 2
	return ret
end

return BufferReader