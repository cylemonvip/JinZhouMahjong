GameplayerManager = GameplayerManager or class("GameplayerManager")
local BufferReader = appdf.req("client/src/privatemode/replay/BufferReader")
local ExternalFun = appdf.req(appdf.EXTERNAL_SRC .. "ExternalFun")
local ReplayDefine = appdf.req("client/src/privatemode/header/ReplayDefine")
local SPARROWHZ_CMD = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.CMD_Game")
 
function GameplayerManager:ctor()
	--原始数据
	self._buffer = nil
	--将原始数据解析成包含单个cmd的数组
	self._cmdArray = {}
	--播放索引
	self._currIndex = 0
	--用户数据
	self._userData = {}
	--主场景
	self._clientScene = nil
	--游戏场景
	self._gameScene = nil
	--人数
	self._playerCount = 0
	--是否暂停
	self._isPause = false
	--获取数据的间隔时间
	self._replayDuration = yl.REPLAY_DURATION
		--房间号
	self._roomID = 0
	--自己的userid
	self._userID = GlobalUserItem.dwUserID
	--是否为测试
	self._isTest = false
	self:setTestUserID(yl.rpUserID)
end

GameplayerManager._instance = nil
function GameplayerManager:getInstance()  
    if nil == GameplayerManager._instance then  
    	print("new GameplayerManager instance")
    	print(debug.traceback())
        GameplayerManager._instance = GameplayerManager:create()
    end  
    return GameplayerManager._instance  
end 

--清楚数据
function GameplayerManager:clear()
	if self._buffer then
		self._buffer:release()
	end
	--原始数据
	self._buffer = nil
	--将原始数据解析成包含单个cmd的数组
	self._cmdArray = {}
	--播放索引
	self._currIndex = 0
	--用户数据
	self._userData = {}
	--主场景
	self._clientScene = nil
	--游戏场景
	self._gameScene = nil
	--人数
	self._playerCount = 0
	--是否暂停
	self._isPause = false
	--获取数据的间隔时间
	self._replayDuration = yl.REPLAY_DURATION
	--房间号
	self._roomID = 0
	--自己的userid
	self._userID = GlobalUserItem.dwUserID
	self.instance = nil
	--是否为测试
	self._isTest = false
	self:setTestUserID(yl.rpUserID)
end

function GameplayerManager:getBuffer()
	return self._buffer
end

--设置播放的原始数据
function GameplayerManager:setOriginData(buffer, clientScene, roomID)
	self:clear()
	self._clientScene = clientScene
	self._roomID = roomID
	self._buffer = buffer
	self._buffer:retain()

	local analysisRet = true	
	analysisRet, self._userData = self:analysisOriginData()
	--如果这两个都有数据，那么说明成功，可以进入房间，并且初始化
	if self._userData and analysisRet then
		--处理初始手牌
		self:deleteInvailHandCard()
		self._gameScene = self._clientScene:onChangeShowMode(yl.SCENE_GAME, {isReplay = true})
	end
	return analysisRet
end

function GameplayerManager:getRoomID()
	return self._roomID
end

--解析原始数据
function GameplayerManager:analysisOriginData()
	--解析结果
	local ret = true
	print("开始解析文件")
	--获取玩家人数	
	self._playerCount = self._buffer:readword()
	print("playerCount = " .. tostring(self._playerCount))
	--获取用户具体数据
	local userData = ExternalFun.readTableHelper({dTable = ReplayDefine.tagUserDataClient, lentable = {self._playerCount}, buffer = self._buffer, strkey = "tUserData"})
	dump(userData[1], "userData")
	local invaidBtyeTotal = 0
	--提取所有关键的cmd,并将解析的结构体插入到cmd中
	function analysisCmd()
		local subCmdID = nil
		local dealData = function(cmd, firstLen)
			subCmdID = cmd.subCmdID
			--数据处理
			print("开始解析 cmd.subCmdID = " .. cmd.subCmdID)
			cmd["t"] = self:onEventGameMessage(cmd.subCmdID, self:getBuffer())
			local lastLen = self:getBuffer():getcurlen()
			local ds = lastLen - firstLen
			local invaidBtyeCount = cmd.dataSize - ds
			-- if invaidBtyeCount ~= 0 then
				print("mainID = " .. cmd.mainCmdID .. " subID = " .. cmd.subCmdID .. " 原定义长度 = " .. cmd.dataSize .. " 实际读取长度 = " .. ds ..  " 无效字节 invaid byte = " .. (invaidBtyeCount))
			-- end
			
			invaidBtyeTotal = invaidBtyeTotal + invaidBtyeCount
			for i = 1, invaidBtyeCount do
				self:getBuffer():readbyte()
			end

			local lastLen = self:getBuffer():getcurlen()
			local ds = lastLen - firstLen
			-- print("firstLen = " .. tostring(firstLen) .. " lastLen = " .. tostring(lastLen) .. " ds = " .. ds )
			assert(ds == cmd.dataSize, "byte read failed")
			dump(cmd, "cmdItem")
			if cmd.t.cbOperateCard then
				dump(cmd.t.cbOperateCard, "cbOperateCard")
			end
			table.insert(self._cmdArray, cmd)
			return subCmdID ~= SPARROWHZ_CMD.SUB_S_GAME_CONCLUDE
		end

		while true do 
    		local replayCmd = self:analysisNextCmd()
    		-- dump(replayCmd, "replayCmd")
    		if next(replayCmd) == nil then
    			ret = false
    			break
    		end
    		local firstLen = self:getBuffer():getcurlen()

    		if replayCmd.mainCmdID == yl.MDM_GF_GAME then
    			if replayCmd.subCmdID == SPARROWHZ_CMD.SUB_S_GAME_START then --如果是游戏开始，则只收自己开始的数据，其他三家的数据过滤掉
    				if not dealData(replayCmd, firstLen) then
    					break
    				end
    			elseif replayCmd.subCmdID == yl.SUB_S_DETECT_SOCKET then --过滤心跳
    				-- print("过滤心跳")
    				for i = 1, replayCmd.dataSize do
						self:getBuffer():readbyte()
					end
				elseif not self._isTest and replayCmd.subCmdID == SPARROWHZ_CMD.SUB_S_OPERATE_NOTIFY then --如果不是测试，就过滤提示碰，吃，杠等等，如果是测试，就解析
					for i = 1, replayCmd.dataSize do
						self:getBuffer():readbyte()
					end
				else
					if not dealData(replayCmd, firstLen) then
						break
					end
    			end
    		else
    			print("收到非 yl.MDM_GF_GAME 数据，mainCmdID = " .. mainCmdID)
    		end
		end
	end

    analysisCmd()

    print("总的无效字节 invaidBtyeTotal = " .. invaidBtyeTotal)

    dump(self._cmdArray, "_cmdArray")
    dump(userData[1], "userData")
	return ret, userData[1]
end


-- 游戏消息
function GameplayerManager:onEventGameMessage(sub, dataBuffer)
    -- body
	if sub == SPARROWHZ_CMD.SUB_S_GAME_START then 					--游戏开始
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_GameStart, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_OUT_CARD then 					--用户出牌
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_OutCard, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_SEND_CARD then 					--发送扑克
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_SendCard, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_OPERATE_NOTIFY then 			--操作提示
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_OperateNotify, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_HU_CARD then 					--听牌提示
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_Hu_Data, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_OPERATE_RESULT then 			--操作命令
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_OperateResult, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_LISTEN_CARD then 				--用户听牌
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_ListenCard, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_GET_HUACARD then 					--用户托管
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_GET_HUACARD, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_GAME_CONCLUDE then 				--游戏结束
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_GameConclude, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_RECORD then 					--游戏记录
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_Record, dataBuffer)
	elseif sub == SPARROWHZ_CMD.SUB_S_SET_PIAO then 				--设置票数
		return ExternalFun.read_netdata(SPARROWHZ_CMD.CMD_S_PiaoCard, dataBuffer)
	elseif sub == yl.SUB_S_DETECT_SOCKET then 				--心跳
		
	else
		assert(false, "default")
	end

	return false
end

function GameplayerManager:getNextCmd()
	if self._currIndex >= #self._cmdArray then
		print("已经是最后一个记录，无法再播放")
		return nil
	end
	self._currIndex = self._currIndex + 1
	local retCmd = self._cmdArray[self._currIndex]
	if retCmd.mainCmdID == yl.MDM_GF_GAME  and retCmd.subCmdID == SPARROWHZ_CMD.SUB_S_GAME_START and retCmd.chairID ~= self:getSelfChairID() then
		retCmd = self:getNextCmd()
	end
	return retCmd
end

function GameplayerManager:getPreCmd()
	if self._currIndex <= 0 then
		print("已经是第 " .. self._currIndex .. " 个记录，此前无记录")
		return nil
	end
	self._currIndex = self._currIndex - 1
	local retCmd = self._cmdArray[self._currIndex]
	return retCmd
end


function GameplayerManager:analysisNextCmd()
	--解析操作数据
	--MainCMD
	local mainCmdID = self._buffer:readword()
	local subCmdID = self._buffer:readword()
	--消息数据实际大小
	local wDataSize = self._buffer:readword()
	--发出该消息的椅子号
	local wChairID = self._buffer:readword()
	--消息主体(结构体)
	return {mainCmdID=mainCmdID, subCmdID=subCmdID, dataSize=wDataSize, chairID=wChairID}
end

--viewId 视图号
--return cmd 消息
function GameplayerManager:getGameStartData(viewId)
	print("原始 viewId = " .. viewId)

	local startOpIndex = 1
	--针对未开始前就把花牌发下来的处理
	for k, cmd in pairs(self._cmdArray) do
		if cmd.subCmdID == SPARROWHZ_CMD.SUB_S_GAME_START then
			startOpIndex = k
			break
		end
	end
	
	--人数
	local pCount = self:getPlayerCount()
	for i = startOpIndex, startOpIndex+pCount-1 do
		local cmd = self._cmdArray[i]
		--把chairID转成viewId再进行比较
		local vId = self._gameScene:SwitchViewChairID(cmd.chairID)
		print("转换后的viewId = " .. tostring(vId))
		if vId == viewId then
			return cmd
		end
	end
	return nil
end

--处理初始手牌，由于所有玩家起手都14张牌，非庄家的第14张牌为0，因为把为0的牌删掉
function GameplayerManager:deleteInvailHandCard()
	local startOpIndex = 1
	--针对未开始前就把花牌发下来的处理
	for k,cmd in pairs(self._cmdArray) do
		if cmd.subCmdID == SPARROWHZ_CMD.SUB_S_GAME_START then
			startOpIndex = k
			break
		end
	end
	local pCount = self:getPlayerCount()
	for i = startOpIndex, startOpIndex+pCount-1 do
		local cmd = self._cmdArray[i]
		dump(cmd, "deleteInvailHandCard cmd")
		for i = #cmd["t"].cbCardData[1], 1, -1 do
			if cmd["t"].cbCardData[1][i] == 0 then
				table.remove(cmd["t"].cbCardData[1], i)
			end
		end
	end
end


function GameplayerManager:getPlayerHandCard(viewId)
	local cmd = self:getGameStartData(viewId)
	assert(cmd, "无效的CMD")
	return cmd["t"].cbCardData[1]
end

function GameplayerManager:setPlayerHandCard(viewId, t)
	local cmd = self:getGameStartData(viewId)
	cmd["t"].cbCardData[1] = t
end

function GameplayerManager.GetPreciseDecimal(nNum, n)
    if type(nNum) ~= "number" then
        return nNum;
    end
    
    n = n or 0;
    n = math.floor(n)
    local fmt = '%.' .. n .. 'f'
    local nRet = tonumber(string.format(fmt, nNum))

    return nRet;
end

--获取播放进度
function GameplayerManager:getProgress()
	return GameplayerManager.GetPreciseDecimal(self._currIndex / #self._cmdArray, 2)
end

--获取播放速率
function GameplayerManager:getRate()
	return GameplayerManager.GetPreciseDecimal(yl.REPLAY_DURATION /self._replayDuration, 2)
end

--开始播放
function GameplayerManager:startPlay()
	
end

--停止播放
function GameplayerManager:stopPlay()
	
end

--暂停
function GameplayerManager:pause()
	self._isPause = true
end

function GameplayerManager:isPause()
	return self._isPause
end

--恢复
function GameplayerManager:resume()
	self._isPause = false
end

--快进
function GameplayerManager:quick()
	self._replayDuration = self._replayDuration - 0.5
	if self._replayDuration <= 0.5 then
		self._replayDuration = 0.5
	end
end

--慢放
function GameplayerManager:slow()
	self._replayDuration = self._replayDuration + 0.5

	if self._replayDuration >= 3.5 then
		self._replayDuration = 3.5
	end
end

--快退
function GameplayerManager:back()
	
end

function GameplayerManager:getReplayDuration()
	return self._replayDuration
end

--重新开始
function GameplayerManager:backoff()
	
end

--获取自己的椅子号
function GameplayerManager:getSelfChairID()
	local ret = nil
	if type(self._userData) == 'table' then
		for k,v in pairs(self._userData) do
			if tonumber(v.dwUserID) == tonumber(self._userID) then
				ret = v.wChairID
				break
			end
		end
	end
	return ret
end

--获取桌子号
function GameplayerManager:getTableID()
	local ret = nil
	for k,v in pairs(self._userData) do
		if tonumber(v.dwUserID) == tonumber(self._userID) then
			ret = v.wTableID
			break
		end
	end
	return ret
end

--人数
function GameplayerManager:getPlayerCount()
	return self._playerCount
end

--用户数据
function GameplayerManager:getPlayerInfo()
	return self._userData
end

function GameplayerManager:setTestUserID(userID)
	if userID ~= nil then
		self._isTest = true
		self._userID = userID
	else
		self._isTest = false
	end
end

function GameplayerManager:isTest()
	return self._isTest
end

return GameplayerManager