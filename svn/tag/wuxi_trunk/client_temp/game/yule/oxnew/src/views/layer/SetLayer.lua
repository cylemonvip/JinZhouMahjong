--
-- Author: tom
-- Date: 2017-02-27 17:26:42
--
local SetLayer = class("SetLayer", function(scene)
	local setLayer = display.newLayer()
	return setLayer
end)

local cmd = appdf.req(appdf.GAME_SRC.."yule.oxnew.src.models.CMD_Game")

local TAG_BT_MUSICON = 1
local TAG_BT_MUSICOFF = 2
local TAG_BT_EFFECTON = 3
local TAG_BT_EFFECTOFF = 4
local TAG_BT_EXIT = 5

local TAG_MaskPanel = 20

function SetLayer:onInitData()
end

function SetLayer:onResetData()
end

local this
function SetLayer:ctor(scene)
	cc.FileUtils:getInstance():addSearchPath(device.writablePath.."game/yule/oxnew/res/game/", true)
	this = self
	self._scene = scene
	self:onInitData()

	local funCallback = function(ref)
		this:onButtonCallback(ref:getTag(), ref)
	end
	--UI
	self._csbNode = cc.CSLoader:createNode(self._scene.RES_PATH.."game/SetLayer.csb")
		:addTo(self, 1)

	local children = self._csbNode:getChildren()
	for k,v in pairs(children) do
		print(k, v:getName())
	end

	self.maskPanel = self._csbNode:getChildByName("MaskPanel")
	self.maskPanel:setTag(TAG_MaskPanel)
	self.maskPanel:addClickEventListener(funCallback)

	self.btMusicOn = self._csbNode:getChildByName("bt_music_on")
		:setTag(TAG_BT_MUSICON)
	self.btMusicOn:addClickEventListener(funCallback)
	self.btMusicOff = self._csbNode:getChildByName("bt_music_off")
		:setTag(TAG_BT_MUSICOFF)
	self.btMusicOff:addClickEventListener(funCallback)
	self.btEffectOn = self._csbNode:getChildByName("bt_effect_on")
		:setTag(TAG_BT_EFFECTON)
	self.btEffectOn:addClickEventListener(funCallback)
	self.btEffectOff = self._csbNode:getChildByName("bt_effect_off")
		:setTag(TAG_BT_EFFECTOFF)
	self.btEffectOff:addClickEventListener(funCallback)
	local btnClose = self._csbNode:getChildByName("bt_close")
		:setTag(TAG_BT_EXIT)
	btnClose:addClickEventListener(funCallback)
	self.sp_layerBg = self._csbNode:getChildByName("sp_setLayer_bg")
	self.sp_layerBg:setTexture("game/yule/oxnew/res/game/sp_setLayer_bg.png")--ccui.TextureResType.localType
	--声音
	self.btMusicOn:setVisible(GlobalUserItem.bVoiceAble)
	self.btMusicOff:setVisible(not GlobalUserItem.bVoiceAble)
	self.btEffectOn:setVisible(GlobalUserItem.bSoundAble)
	self.btEffectOff:setVisible(not GlobalUserItem.bSoundAble)
	if GlobalUserItem.bVoiceAble then
		AudioEngine.playMusic("game/yule/oxnew/res/sound/backMusic.mp3", true)
	end
	--版本号
	local textVersion = self._csbNode:getChildByName("Text_version")
	local mgr = self._scene._scene._scene:getApp():getVersionMgr()
	local nVersion = mgr:getResVersion(cmd.KIND_ID) or "0"
	local strVersion = "游戏版本："..appdf.BASE_C_VERSION.."."..nVersion
	textVersion:setString(strVersion)

	self:setVisible(false)
end

function SetLayer:onButtonCallback(tag, ref)
	if tag == TAG_BT_MUSICON then
		print("音乐状态本开")
		GlobalUserItem.setVoiceAble(false)
		self.btMusicOn:setVisible(false)
		self.btMusicOff:setVisible(true)
	elseif tag == TAG_BT_MUSICOFF then
		print("音乐状态本关")
		GlobalUserItem.setVoiceAble(true)
		self.btMusicOn:setVisible(true)
		self.btMusicOff:setVisible(false)
		AudioEngine.playMusic("game/yule/oxnew/res/sound/backMusic.mp3", true)
	elseif tag == TAG_BT_EFFECTON then
		print("音效状态本开")
		GlobalUserItem.setSoundAble(false)
		self.btEffectOn:setVisible(false)
		self.btEffectOff:setVisible(true)
	elseif tag == TAG_BT_EFFECTOFF then
		print("音效状态本关")
		GlobalUserItem.setSoundAble(true)
		self.btEffectOn:setVisible(true)
		self.btEffectOff:setVisible(false)
	elseif tag == TAG_BT_EXIT then
		self:hideLayer()
		-- if GlobalUserItem.bPrivateRoom then
		-- 	self._scene._scene._gameFrame:StandUp(false)
		-- end
	elseif tag == TAG_MaskPanel then
		self:hideLayer()
	end
end

function SetLayer:showLayer()
	self.maskPanel:setVisible(true)
	self:setVisible(true)
end

function SetLayer:hideLayer()
	self.maskPanel:setVisible(false)
	self:setVisible(false)
	self:onResetData()
end

return SetLayer