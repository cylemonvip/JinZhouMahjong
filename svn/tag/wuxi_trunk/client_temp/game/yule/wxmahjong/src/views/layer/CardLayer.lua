local CardLayer = class("CardLayer", function(scene)
	local cardLayer = display.newLayer()
	return cardLayer
end)

local ExternalFun = appdf.req(appdf.EXTERNAL_SRC.."ExternalFun")
local GameLogic = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.GameLogic")
local cmd = appdf.req(appdf.GAME_SRC.."yule.wxmahjong.src.models.CMD_Game")
local GameplayerManager = appdf.req("client/src/privatemode/replay/GameplayerManager")

local posTableCard = {cc.p(393, 546), cc.p(292, 179), cc.p(940, 187), cc.p(1044, 593)}
local posHandCard = {cc.p(1080, 660), cc.p(180, 220), cc.p(1270, 8), cc.p(1150, 700)}
local posHandDownCard = {cc.p(495, 665), cc.p(180, 670), cc.p(295, 70), cc.p(1155, 670)}
local posDiscard = {cc.p(922, 505), cc.p(374, 535), cc.p(436, 258), cc.p(986, 204)}
local posBpBgCard = {cc.p(390, 680), cc.p(190, 735), cc.p(0, 65), cc.p(1150, 164)}
local anchorPointHandCard = {cc.p(1, 0), cc.p(0, 0), cc.p(1, 0), cc.p(0, 1)}
local multipleTableCard = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}}
local multipleDownCard = {{1, 0}, {0, -1}, {1, 0}, {0, -1}}
local multipleBpBgCard = {{1, 0}, {0, -1}, {1, 0}, {0, 1}}

local huaCardDisplayPos = {cc.p(1000, 620), cc.p(250, 300), cc.p(280, 185), cc.p(1100, 250)}

local CARD_HEIGHT = 127
local CARD_WIDTH = 86

--local cbCardData = {1, 5, 7, 6, 34, 12, 32, 25, 18, 19, 27, 22, 33, 33}

CardLayer.TAG_BUMPORBRIDGE = 1
CardLayer.TAG_CARD_FONT = 1
CardLayer.TAG_LISTEN_FLAG = 2

CardLayer.ENUM_CARD_NORMAL = nil
CardLayer.ENUM_CARD_POPUP = 1
CardLayer.ENUM_CARD_MOVING = 2
CardLayer.ENUM_CARD_OUT = 3

CardLayer.Z_ORDER_TOP = 50

function CardLayer:onInitData()
	--body
	math.randomseed(os.time())
	self.cbCardData = {}
	--该Table用于储存其他三家的手牌，顺序为viewId -> 1 -> 2 - > 4
	self.otherCbCardData = {}
	self.cbCardCount = {cmd.MAX_COUNT, cmd.MAX_COUNT, cmd.MAX_COUNT, cmd.MAX_COUNT}
	self.nDiscardCount = {0, 0, 0, 0}
	self.nBpBgCount = {0, 0, 0, 0}
	self.bCardGray = {}
	self.cbCardStatus = {}
	self.nCurrentTouchCardTag = 0
	self.currentTag = 0
	self.cbListenList = {}
	self.bWin = false
	self.currentOutCard = 0
	self.nTableTailCardTag = 112
	self.nRemainCardNum = 144
	self.posTemp = cc.p(0, 0)
	self.nZOrderTemp = 0
	self.nMovingIndex = 0
	self.bSpreadCardEnabled = false
	self.bSendOutCardEnabled = false
	self.cbBpBgCardData = {{}, {}, {}, {}}
	self.bpBgCardArray = {}

	self._huaCardArray = {}
end

function CardLayer:onResetData()
	
	--body
	for i = 1, cmd.GAME_PLAYER do
		self.cbCardCount[i] = cmd.MAX_COUNT
		self.nDiscardCount[i] = 0
		self.nBpBgCount[i] = 0
		self.nodeDiscard[i]:removeAllChildren()
		self.nodeBpBgCard[i]:removeAllChildren()

		for j = 1, cmd.MAX_COUNT do
			local card = self.nodeHandCard[i]:getChildByTag(j)
			
			
			card:setColor(cc.c3b(255, 255, 255))
			card:setVisible(false)

			if i == cmd.MY_VIEWID then
				local width = CARD_WIDTH
				local height = CARD_HEIGHT
				local fSpacing = width
				local widthTotal = fSpacing*cmd.MAX_COUNT
				local posX = widthTotal - width/2 - fSpacing*(j - 1)
				local posY = height/2
				
				card:setPosition(posX, posY)
				if j == 1 then
					card:setPosition(posX + 20, posY)						--每次抓的牌
				end
			end
			self.nodeHandDownCard[i]:getChildByTag(j):setVisible(false)
		end
	end

	self.bpBgCardArray = {}
	self.cbCardData = {}
	self.bCardGray = {}
	self.cbCardStatus = {}
	self.nCurrentTouchCardTag = 0
	self.nMovingIndex = 0
	self.nZOrderTemp = 0
	self.currentTag = 0
	self.cbListenList = {}
	self.bWin = false
	self.currentOutCard = 0
	self.nTableTailCardTag = 112
	self.nRemainCardNum = 144
	self.posTemp = cc.p(0, 0)
	self:promptListenOutCard(nil)
	self.bSpreadCardEnabled = false
	self.bSendOutCardEnabled = false
	self.cbBpBgCardData = {{}, {}, {}, {}}
	self.beginPoint = nil
end

function CardLayer:ctor(scene)
	self._scene = scene
	self._mjSkin = tonumber(cc.UserDefault:getInstance():getStringForKey("PM_INDEX", 0))
	print("CardLayer self._mjSkin = " .. self._mjSkin)
	if self._mjSkin == "1" then
		print("皮肤111111111111111111") 
	else
		print("皮肤000000000000000000")
	end

	local yspmIndex = cc.UserDefault:getInstance():getStringForKey("PM_INIT_INDEX_1")
	print("yspmIndex = " .. tostring(yspmIndex))
	if not yspmIndex or yspmIndex == "" then
		self._mjSkin = 3
		cc.UserDefault:getInstance():setStringForKey("PM_INIT_INDEX_1", "1")
		cc.UserDefault:getInstance():setStringForKey("PM_INDEX", 3)
	end

	if self._mjSkin == 1 then
		CARD_HEIGHT = 127
		CARD_WIDTH = 86
		posHandCard = {cc.p(980, 660), cc.p(180, 220), cc.p(1240, 8), cc.p(1150, 700)}
		posDiscard = {cc.p(922, 505), cc.p(374, 535), cc.p(436, 258), cc.p(986, 204)}
	elseif self._mjSkin == 3 then
		CARD_HEIGHT = 122
		CARD_WIDTH = 80
		posHandCard = {cc.p(980, 660), cc.p(180, 220), cc.p(1240, 8), cc.p(1150, 700)}
		posDiscard = {cc.p(922, 505), cc.p(374, 535), cc.p(436, 258), cc.p(986, 204)}
	else
		posDiscard = {cc.p(900, 505), cc.p(374, 535), cc.p(460, 258), cc.p(986, 230)}
		posHandCard = {cc.p(1080, 660), cc.p(180, 220), cc.p(1270, 8), cc.p(1150, 700)}
		CARD_HEIGHT = 136
		CARD_WIDTH = 88
	end
	self:onInitData()

	self.bpBgCardArray = {}

	ExternalFun.registerTouchEvent(self, true)
	--桌牌
	--self.nodeTableCard = self:createTableCard()
	--手牌
	self.nodeHandCard = self:createHandCard()
	--铺着的手牌
	self.nodeHandDownCard = self:createHandDownCard()
	--弃牌
	self.nodeDiscard = self:createDiscard()
	--碰或杠牌
	self.nodeBpBgCard = self:createBpBgCard()

	--花牌展示
	self.huaCardZoom = self:createHuaCardZoom()
end

--给viewId加花
function CardLayer:addHuaCard(viewId, huaCardArray)
	dump(huaCardArray, "GameViewLayer addHuaCard")
	local dx = 0
	local dy = 0

	if viewId == 1 then
		dx = -30
		dy = 0
		if self._mjSkin == 3 then
			dx = -26
		end
	elseif viewId == 2 then
		dx = 0
		dy = 26
		if self._mjSkin == 3 then
			dy = 22
		end
	elseif viewId == 3 then
		dx = 30
		dy = 0
		if self._mjSkin == 3 then
			dx = 26
		end
	elseif viewId == 4 then
		dx = 0
		dy = 26
		if self._mjSkin == 3 then
			dy = 22
		end
	end

	local resCard = 
	{
		cmd.RES_PATH.."game/font_small/card_down.png",
		cmd.RES_PATH.."game/font_small_side/card_down.png", 
		cmd.RES_PATH.."game/font_small/card_down.png",
		cmd.RES_PATH.."game/font_small_side/card_down.png"
	}
	local resFont = 
	{
		cmd.RES_PATH.."game/font_small/",
		cmd.RES_PATH.."game/font_small_side/", 
		cmd.RES_PATH.."game/font_small/",
		cmd.RES_PATH.."game/font_small_side/"
	}

	if self._mjSkin == 1 then
		resCard = 
		{
			cmd.RES_PATH.."game/font_small_1/card_down.png",
			cmd.RES_PATH.."game/font_small_side_1/card_down.png", 
			cmd.RES_PATH.."game/font_small_1/card_down.png",
			cmd.RES_PATH.."game/font_small_side_1/card_down.png"
		}

		resFont = 
		{
			cmd.RES_PATH.."game/font_small_1/",
			cmd.RES_PATH.."game/font_small_side_1/", 
			cmd.RES_PATH.."game/font_small_1/",
			cmd.RES_PATH.."game/font_small_side_1/"
		}
	elseif self._mjSkin == 2 then
		resCard = 
		{
			cmd.RES_PATH.."game/font_small_2/card_down.png",
			cmd.RES_PATH.."game/font_small_side_2/card_down.png", 
			cmd.RES_PATH.."game/font_small_2/card_down.png",
			cmd.RES_PATH.."game/font_small_side_2/card_down.png"
		}

		resFont = 
		{
			cmd.RES_PATH.."game/font_small_2/",
			cmd.RES_PATH.."game/font_small_side_2/", 
			cmd.RES_PATH.."game/font_small_2/",
			cmd.RES_PATH.."game/font_small_side_2/"
		}
	elseif self._mjSkin == 3 then
		resCard = 
		{
			cmd.RES_PATH.."game/font_small_3/card_down.png",
			cmd.RES_PATH.."game/font_small_side_3/card_down.png", 
			cmd.RES_PATH.."game/font_small_3/card_down.png",
			cmd.RES_PATH.."game/font_small_side_3/card_down.png"
		}

		resFont = 
		{
			cmd.RES_PATH.."game/font_small_3/",
			cmd.RES_PATH.."game/font_small_side_3/", 
			cmd.RES_PATH.."game/font_small_3/",
			cmd.RES_PATH.."game/font_small_side_3/"
		}
	end

	self.huaCardZoom[viewId]:removeAllChildren()

	for i = 1, #huaCardArray do
		local card = cc.Sprite:create(resCard[viewId])
		card:setScale(0.7)
		card:move((i-1) * dx, (i-1) * dy)
		card:addTo(self.huaCardZoom[viewId], 100 - i)

		local size = card:getContentSize()
		local rotate = 0
		local offsetX = 0
		local offsetY = 0
		if viewId == 1 then
			rotate = 180
			offsetX = 0
			offsetY = 6
		elseif viewId == 2 then
			rotate = 90
			offsetY = 8
			offsetX = -1
		elseif viewId == 3 then
			rotate = 0
			offsetY = 10
		elseif viewId == 4 then
			rotate = 270
			offsetY = 8
			offsetX = 1
		end
		--
		local strFile = "game/yule/wxmahjong/res/game/hua_card/hua_card_" .. huaCardArray[i] ..".png"
		local font = cc.Sprite:create(strFile)
		font:setPosition(cc.p(size.width/2 + offsetX, size.height/2 + offsetY))
		font:setScale(0.4)
		font:setRotation(rotate)
		card:addChild(font)
	end
end

--创建立着的手牌
function CardLayer:createHandCard()
	local res = 
	{
		cmd.RES_PATH.."game/card_back_up.png",
		cmd.RES_PATH.."game/card_left.png", 
		cmd.RES_PATH.."game/font_big/card_up.png",
		cmd.RES_PATH.."game/card_right.png"
	}

	print("createHandCard self._mjSkin = " .. self._mjSkin)
	if self._mjSkin == 1 then
		res = 
		{
			cmd.RES_PATH.."game/font_small_1/card_back_up.png",
			cmd.RES_PATH.."game/card_left_1.png", 
			cmd.RES_PATH.."game/font_big_1/card_up.png",
			cmd.RES_PATH.."game/card_right_1.png"
		}
	elseif self._mjSkin == 2 then
		res = 
		{
			cmd.RES_PATH.."game/font_small_2/card_back_up.png",
			cmd.RES_PATH.."game/card_left.png", 
			cmd.RES_PATH.."game/font_big_2/card_up.png",
			cmd.RES_PATH.."game/card_right.png"
		}
	elseif self._mjSkin == 3 then
		res = 
		{
			cmd.RES_PATH.."game/font_small_3/card_back_up.png",
			cmd.RES_PATH.."game/card_left_3.png", 
			cmd.RES_PATH.."game/font_big_3/card_up.png",
			cmd.RES_PATH.."game/card_right_3.png"
		}
	end

	if self._scene._scene._isReplay then
		res = 
		{
			cmd.RES_PATH.."game/font_small/card_down.png",
			cmd.RES_PATH.."game/font_small_side/card_down.png", 
			cmd.RES_PATH.."game/font_big/card_up.png",
			cmd.RES_PATH.."game/font_small_side/card_down.png"
		}
		if self._mjSkin == 1 then
			res = 
			{
				cmd.RES_PATH.."game/font_small_1/card_down.png",
				cmd.RES_PATH.."game/font_small_side_1/card_down.png", 
				cmd.RES_PATH.."game/font_big_1/card_up.png",
				cmd.RES_PATH.."game/font_small_side_1/card_down.png"
			}
		elseif self._mjSkin == 2 then
			res = 
			{
				cmd.RES_PATH.."game/font_small_2/card_down.png",
				cmd.RES_PATH.."game/font_small_side_2/card_down.png", 
				cmd.RES_PATH.."game/font_big_2/card_up.png",
				cmd.RES_PATH.."game/font_small_side_2/card_down.png"
			}
		elseif self._mjSkin == 3 then
			res = 
			{
				cmd.RES_PATH.."game/font_small_3/card_down.png",
				cmd.RES_PATH.."game/font_small_side_3/card_down.png", 
				cmd.RES_PATH.."game/font_big_3/card_up.png",
				cmd.RES_PATH.."game/font_small_side_3/card_down.png"
			}
		end
	end
	
	local nodeCard = {}

	if self._scene._scene._isReplay then
		posHandCard = {cc.p(1080, 645), cc.p(165, 220), cc.p(1270, 8), cc.p(1150, 700)}
		if self._mjSkin == 1 then
			posHandCard = {cc.p(980, 645), cc.p(165, 220), cc.p(1270, 8), cc.p(1150, 700)}
		elseif self._mjSkin == 3 then
			posHandCard = {cc.p(980, 645), cc.p(165, 220), cc.p(1270, 8), cc.p(1150, 700)}
		end
	end

	for i = 1, cmd.GAME_PLAYER do
		local bVert = i == 1 or i == cmd.MY_VIEWID
		local width = 0
		local height = 0
		local fSpacing = 0
		if i == 1 then
			width = 44
			height = 67
			if self._mjSkin == 1 then
				width = 36
				height = 59
			elseif self._mjSkin == 3 then
				width = 36
				height = 59
			end
			fSpacing = width
		elseif i == cmd.MY_VIEWID then
			width = CARD_WIDTH
			height = CARD_HEIGHT
			fSpacing = width
		elseif i == 2 then
			width = 28
			height = 75
			fSpacing = 31
			if self._mjSkin == 1 then
				width = 27
				height = 67
				fSpacing = 28
			elseif self._mjSkin == 3 then
				width = 27
				height = 67
				fSpacing = 28
			end
		elseif i == 4 then
			width = 28
			height = 75
			fSpacing = 31
			if self._mjSkin == 1 then
				width = 27
				height = 67
				fSpacing = 28
			elseif self._mjSkin == 3 then
				width = 27
				height = 67
				fSpacing = 28
			end
		end

		if self._scene._scene._isReplay and (i == 2 or i == 4) then
			width = 55
			height = 47
			fSpacing = 32
		end

		local widthTotal = fSpacing*cmd.MAX_COUNT
		local heightTotal = height + fSpacing*(cmd.MAX_COUNT - 1)
		nodeCard[i] = cc.Node:create()
			:move(posHandCard[i])
			:setContentSize(bVert and cc.size(widthTotal, height) or cc.size(width, heightTotal))
			:setAnchorPoint(anchorPointHandCard[i])
			:addTo(self, 1)

		
		for j = 1, cmd.MAX_COUNT do
			if i == 1 then
				pos = cc.p(widthTotal - fSpacing*(j - 1), height/2)
			elseif i == 2 then
				pos = cc.p(width/2, height/2 + fSpacing*(j - 1))
			elseif i == cmd.MY_VIEWID then
				pos = cc.p(widthTotal - width/2 - fSpacing*(j - 1), height/2)
			elseif i == 4 then
				pos = cc.p(width/2, heightTotal - height/2 - fSpacing*(j - 1))
			end

			local card = display.newSprite(res[i])
				:move(pos)
				:setTag(j)
				:setVisible(false)
				:addTo(nodeCard[i])

			if i == cmd.MY_VIEWID or self._scene._scene._isReplay then
				local cardData = GameLogic.MAGIC_DATA
				card.cardData = cardData
				print("cardData = " .. cardData)
				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(cardData))
				print("cardData nValue = " .. nValue .. " nColor = " .. nColor)
				local fontBigFile = "font_big"
				local fontSmallSideFile = "font_small_side"
				local fontSmallFile = "font_small"
				local dy = -15
				if self._mjSkin == 1 then
					fontBigFile = "font_big_1"
					fontSmallSideFile = "font_small_side_1"
					fontSmallFile = "font_small_1"
				elseif self._mjSkin == 2 then
					fontBigFile = "font_big_2"
					fontSmallSideFile = "font_small_side_2"
					fontSmallFile = "font_small_2"
				elseif self._mjSkin == 3 then
					fontBigFile = "font_big_3"
					fontSmallSideFile = "font_small_side_3"
					fontSmallFile = "font_small_3"
					dy = -12
				end
				local strFile = cmd.RES_PATH.."game/" .. fontBigFile .. "/font_"..nColor.."_"..nValue..".png"

				if self._scene._scene._isReplay then
					if i == 2 or i == 4 then
						strFile = cmd.RES_PATH.."game/" .. fontSmallSideFile .. "/font_"..nColor.."_"..nValue..".png"
					elseif i == 1 then
						strFile = cmd.RES_PATH.."game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png"
					end
					
				end

				if self._scene._scene._isReplay and (i == 2) then
					card:setLocalZOrder(30 - j)	--修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
				end

				local font = display.newSprite(strFile)
					:move(width/2, height/2 + dy)
					:setTag(1)
					:addTo(card)
				if cardData == 0x35 then
				    --local lai = cc.Sprite:create(cmd.RES_PATH.."game/lai.png"):move(70.5,112):addTo(card)
				end

				if self._scene._scene._isReplay and (i == 1 or i == 2 or i == 4) then
					local csize = card:getContentSize()
					if i == 1 then
						font:setPosition(cc.p(csize.width/2, csize.height/2 + 8))
					elseif i == 2 then
						if self._mjSkin == 1 then
							font:setPosition(cc.p(csize.width/2, csize.height/2 + 7))
						elseif self._mjSkin == 3 then
							font:setPosition(cc.p(csize.width/2, csize.height/2 + 7))
						else
							font:setPosition(cc.p(csize.width/2, csize.height/2 + 5))
						end
					elseif i == 4 then
						if self._mjSkin == 1 then
							font:setPosition(cc.p(csize.width/2, csize.height/2 + 7))
						elseif self._mjSkin == 3 then
							font:setPosition(cc.p(csize.width/2, csize.height/2 + 7))
						else
							font:setPosition(cc.p(csize.width/2, csize.height/2 + 8))
						end
						
						font:setRotation(180)
					end
				end

				--card:setTextureRect(cc.rect(0, 0, 69, CARD_HEIGHT))
				if j == 1 then
					local x, y = card:getPosition()
					if i == 1 or i == 3 then
						card:setPositionX(x + 20)						--每次抓的牌
					elseif i == 2 then
						card:setPositionY(y - 20)						--每次抓的牌
					elseif i == 4 then
						card:setPositionY(y + 20)						--每次抓的牌
					end
				end

				--提示听牌的小标记
				cc.Sprite:create("game/yule/wxmahjong/res/game/listen/sp_listenPromptFlag.png")
					:move(69/2, CARD_HEIGHT + 25)
					:setTag(CardLayer.TAG_LISTEN_FLAG)
					:setVisible(false)
					:addTo(card)
			elseif i == 2 then
				card:setLocalZOrder(30 - j)	--修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
			end
		end
	end
	nodeCard[cmd.MY_VIEWID]:setLocalZOrder(2)

	return nodeCard
end

--给会牌添加标记
function CardLayer:initLaiziCard()
	
end

--创建铺着的手牌
function CardLayer:createHandDownCard()
	local fSpacing = {44, 32, CARD_WIDTH, 32}
	local fontBigFile = "font_big"
	local fontSmallFile = "font_small"
	local fontSmallSideFile = "font_small_side"
	if self._mjSkin == 1 then
		fontBigFile = "font_big_1"
		fontSmallFile = "font_small_1"
		fontSmallSideFile = "font_small_side_1"
		fSpacing = {36, 28, CARD_WIDTH, 28}
	elseif self._mjSkin == 2 then
		fontBigFile = "font_big_2"
		fontSmallFile = "font_small_2"
		fontSmallSideFile = "font_small_side_2"
	elseif self._mjSkin == 3 then
		fontBigFile = "font_big_3"
		fontSmallFile = "font_small_3"
		fontSmallSideFile = "font_small_side_3"
		fSpacing = {38, 28, CARD_WIDTH, 28}
	end
	local res = 
	{
		cmd.RES_PATH.."game/" .. fontSmallFile .. "/card_back.png",
		cmd.RES_PATH.."game/" .. fontSmallSideFile .. "/card_back.png",
		cmd.RES_PATH.."game/" .. fontBigFile .. "/card_back.png",
		cmd.RES_PATH.."game/" .. fontSmallSideFile .. "/card_back.png"
	}

	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
			:move(posHandDownCard[i])
			:setAnchorPoint(cc.p(0.5, 0.5))
			:addTo(self, 2)
		for j = 1, cmd.MAX_COUNT do
			local card = display.newSprite(res[i])
				:setVisible(false)
				:setTag(j)
				:addTo(nodeCard[i])
			if i == 1 or i == 3 then
				card:move(fSpacing[i]*j, 0)
			else
				card:move(0, -fSpacing[i]*j)
			end
			if i == 2 then
				--card:setLocalZOrder(30 - j)
			end
		end
	end

	return nodeCard
end

function CardLayer:createHuaCardZoom()
	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
			:move(huaCardDisplayPos[i])
			:addTo(self)
	end
	--nodeCard[1]:setLocalZOrder(2)

	return nodeCard
end

--创建弃牌
function CardLayer:createDiscard()
	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
			:move(posDiscard[i])
			:addTo(self)
	end
	--nodeCard[1]:setLocalZOrder(2)

	return nodeCard
end
--创建碰或杠牌
function CardLayer:createBpBgCard()
	if self._scene._scene._isReplay then
		posBpBgCard = {cc.p(390, 680), cc.p(190, 735), cc.p(0, 65), cc.p(1160, 164)}
	end
	local nodeCard = {}
	for i = 1, cmd.GAME_PLAYER do
		nodeCard[i] = cc.Node:create()
			:move(posBpBgCard[i])
			:addTo(self)
	end
	nodeCard[cmd.MY_VIEWID]:setLocalZOrder(2)
	nodeCard[4]:setLocalZOrder(1)

	return nodeCard
end
function CardLayer:onTouchBegan(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end

	local bCanOutCard = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2
	if not bCanOutCard then
		return false
	end

	if self._scene.isChoicesPopuped then
		return false
	end

	local pos = touch:getLocation()
	local parentPosX, parentPosY = self.nodeHandCard[cmd.MY_VIEWID]:getPosition()
	local parentSize = self.nodeHandCard[cmd.MY_VIEWID]:getContentSize()
	local posRelativeCard = cc.p(0, 0)
	posRelativeCard.x = pos.x - (parentPosX - parentSize.width)
	posRelativeCard.y = pos.y - parentPosY

	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		local cardRect = card:getBoundingBox()
		if cc.rectContainsPoint(cardRect, posRelativeCard) and card:isVisible() and not self.bCardGray[i] then
			--print("touch begin!", pos.x, pos.y)
			self.nCurrentTouchCardTag = i
			self.beginPoint = pos
			--缓存
			self.posTemp.x, self.posTemp.y = card:getPosition()
			self.nZOrderTemp = card:getLocalZOrder()
			--将牌补满(ui与值的对齐方式)
			local nCount = 0
			local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
			print("self.cbCardCount[cmd.MY_VIEWID] = " .. self.cbCardCount[cmd.MY_VIEWID])
			print("num = " .. num)
			if num == 2 then
				nCount = self.cbCardCount[cmd.MY_VIEWID]
			elseif num == 1 then
				nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
			else
				assert(false)
			end
			local index = nCount - i + 1

			return true
		end
	end

	return false
end
function CardLayer:onTouchMoved(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end

	local bCanOutCard = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2
	if not bCanOutCard then
		return false
	end

	local pos = touch:getLocation()
	--print("touch move!", pos.x, pos.y)

	local parentPosX, parentPosY = self.nodeHandCard[cmd.MY_VIEWID]:getPosition()
	local parentSize = self.nodeHandCard[cmd.MY_VIEWID]:getContentSize()
	local posRelativeCard = cc.p(0, 0)
	posRelativeCard.x = pos.x - (parentPosX - parentSize.width)
	posRelativeCard.y = pos.y - parentPosY

	if self.beginPoint and math.pow(pos.x - self.beginPoint.x,2) + math.pow(pos.y - self.beginPoint.y,2)  < 15*15 then
		
	   return true
	end

	--移动
	if self.nCurrentTouchCardTag ~= 0 then
		--将牌补满(ui与值的对齐方式)
		local nCount = 0
		local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
		if num == 2 then
			nCount = self.cbCardCount[cmd.MY_VIEWID]
		elseif num == 1 then
			nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
		else
			assert(false)
		end
		local index = nCount - self.nCurrentTouchCardTag + 1

		self.cbCardStatus[index] = CardLayer.ENUM_CARD_MOVING
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(self.nCurrentTouchCardTag)
		card:setPosition(posRelativeCard)
		card:setLocalZOrder(CardLayer.Z_ORDER_TOP)
		--有则提示听牌
		local cbPromptHuCard = self._scene._scene:getListenPromptHuCard(self.cbCardData[index])
		if self.nMovingIndex ~= index and math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 then
			self._scene:setListeningCard(cbPromptHuCard)
		end
		self.nMovingIndex = index
	end

	return true
end
function CardLayer:onTouchEnded(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end

	local bCanOutCard = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2
	if not bCanOutCard then
		return false
	end
	
	if self.nCurrentTouchCardTag == 0 then
		return true
	end

	local pos = touch:getLocation()
	--print("touch end!", pos.x, pos.y)
	local parentPosX, parentPosY = self.nodeHandCard[cmd.MY_VIEWID]:getPosition()
	local parentSize = self.nodeHandCard[cmd.MY_VIEWID]:getContentSize()
	local posRelativeCard = cc.p(0, 0)
	posRelativeCard.x = pos.x - (parentPosX - parentSize.width)
	posRelativeCard.y = pos.y - parentPosY

	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		local cardRect = card:getBoundingBox()
		if cc.rectContainsPoint(cardRect, posRelativeCard) and card:isVisible() and not self.bCardGray[i] then
			--将牌补满(ui与值的对齐方式)
			local nCount = 0
			local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
			if num == 2 then
				nCount = self.cbCardCount[cmd.MY_VIEWID]
			elseif num == 1 then
				nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
			else
				assert(false)
			end
			local index = nCount - i + 1		--算出这张牌对应牌值在self.cbCardData里的下标(cbCardData与cbCardStatus保持一致)
			if self.nCurrentTouchCardTag == i then
				if self.cbCardStatus[index] == CardLayer.ENUM_CARD_NORMAL then 		--原始状态
					--恢复
					self.cbCardStatus = {}
					for v = 1, cmd.MAX_COUNT do
						local cardTemp = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(v)
						cardTemp:setPositionY(CARD_HEIGHT/2)
					end
					--弹出
					self.cbCardStatus[index] = CardLayer.ENUM_CARD_POPUP
					card:setPositionY(CARD_HEIGHT/2 + 30)

					self:findSameCardFromDiscard(card.cardData)

					--有则提示听牌
					if math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 then
						local cbPromptHuCard = self._scene._scene:getListenPromptHuCard(self.cbCardData[index])
						self._scene:setListeningCard(cbPromptHuCard)
					end
				elseif self.cbCardStatus[index] == CardLayer.ENUM_CARD_POPUP then 		--弹出状态
					if math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 then
						--出牌"
						if self:touchSendOutCard(self.cbCardData[index]) then
							card:setVisible(false)
							self:removeHandCard(cmd.MY_VIEWID, {self.cbCardData[index]}, true)
						end
					else
						--弹回
					end
					self.cbCardStatus[index] = CardLayer.ENUM_CARD_NORMAL
					card:setPositionY(CARD_HEIGHT/2)
				elseif self.cbCardStatus[index] == CardLayer.ENUM_CARD_MOVING then 		--移动状态
					--恢复
					self.cbCardStatus = {}
					for v = 1, cmd.MAX_COUNT do
						local cardTemp = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(v)
						cardTemp:setPositionY(CARD_HEIGHT/2)
					end
					self.cbCardStatus[index] = CardLayer.ENUM_CARD_POPUP
					card:setPosition(self.posTemp.x, CARD_HEIGHT/2 + 30)
					card:setLocalZOrder(self.nZOrderTemp)
					--判断
					--local rectDiscardPool = cc.rect(324, 218, 686, 283)
					if math.mod(self.cbCardCount[cmd.MY_VIEWID], 3) == 2 and pos.y >= 190 then
						--出牌"
						if self:touchSendOutCard(self.cbCardData[index]) then
							card:setVisible(false)
							card:setPosition(self.posTemp.x, CARD_HEIGHT/2)
							self:removeHandCard(cmd.MY_VIEWID, {self.cbCardData[index]}, true)
						end
					end
				elseif self.cbCardStatus[index] == CardLayer.ENUM_CARD_OUT then 		--已出牌状态
					assert(false)
				end
				break
			end
		end
		--规避没点到牌的情况
		if i == cmd.MAX_COUNT then
			local cardTemp = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(self.nCurrentTouchCardTag)
			cardTemp:setPosition(self.posTemp)
			cardTemp:setLocalZOrder(self.nZOrderTemp)
			self.cbCardStatus = {}
		end
	end
	self.nCurrentTouchCardTag = 0
	self.nZOrderTemp = 0
	return true
end

function CardLayer:onTouchCancelled(touch, event)
	--如果是重放就不能点击
	if self._scene._scene._isReplay then
		return false
	end
	
	for j = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(j)
		width = CARD_WIDTH
		height = CARD_HEIGHT
		fSpacing = width
		local widthTotal = fSpacing*cmd.MAX_COUNT
		local posX = widthTotal - width/2 - fSpacing*(j - 1)
		local posY = height/2
		card:setPosition(posX, posY)
		if j == 1 then
			card:setPosition(posX + 20, posY)						--每次抓的牌
		end
	end
end

--弹出指定的牌
function CardLayer:popupCard(cards, op)
	print("操作 op = " .. op)
	for i,v in ipairs(cards) do
		print("被弹出 --- ", i, v)
	end
	--先收齐点出的牌
	self:unpopupAllCard()

	for i,aCard in ipairs(cards) do
		print("弹出 aCard = " .. aCard)
		for j, aCardData in ipairs(self.cbCardData) do
			if aCard == aCardData then
				print("手牌 aCardData = " .. aCardData)
				local bPopup = false
				--弹出
				for k = 1, cmd.MAX_COUNT do
					--将牌补满(ui与值的对齐方式)
					local nCount = 0
					local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
					if num == 2 then
						nCount = self.cbCardCount[cmd.MY_VIEWID]
					elseif num == 1 then
						nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
					else
						assert(false)
					end
					local index = nCount - k + 1
					if index == j then
						local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(k)
						self.cbCardStatus[j] = CardLayer.ENUM_CARD_POPUP
						card:setPositionY(CARD_HEIGHT/2 + 30)
						bPopup = true
					end
				end
				if bPopup and (op == GameLogic.WIK_LEFT or op == GameLogic.WIK_CENTER or op == GameLogic.WIK_RIGHT) then
					break
				end
			end
		end
	end
end

--收入所有牌
function CardLayer:unpopupAllCard()
	for j = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(j)
		width = CARD_WIDTH
		height = CARD_HEIGHT
		fSpacing = width
		local widthTotal = fSpacing*cmd.MAX_COUNT
		local posX = widthTotal - width/2 - fSpacing*(j - 1)
		local posY = height/2
		card:setPosition(posX, posY)

		local nCount = 0
		local num = math.mod(self.cbCardCount[cmd.MY_VIEWID], 3)
		if num == 2 then
			nCount = self.cbCardCount[cmd.MY_VIEWID]
		elseif num == 1 then
			nCount = self.cbCardCount[cmd.MY_VIEWID] + 1
		else
			assert(false)
		end

		local index = nCount - j + 1
		self.cbCardStatus[index] = CardLayer.ENUM_CARD_NORMAL
		if j == 1 then
			card:setPosition(posX + 20, posY)						--每次抓的牌
		end
	end


end

--发牌
function CardLayer:sendCard(meData, cardCount, otherCbCardData)
	assert(type(meData) == "table" and type(cardCount) == "table")
	print("开始发牌")
	self.bSpreadCardEnabled = true
	--其他三家的牌
	self.otherCbCardData = otherCbCardData
	self.cbCardData = meData
	self.cbCardCount = cardCount
	dump(meData)
	dump(cardCount)
	local fDelayTime = 0.05
	for i = 1, cmd.GAME_PLAYER do
		if cardCount[i] > 0 then
			self:spreadCard(i, fDelayTime, 1)
		end
	end

	local fDelayTimeMax = fDelayTime*cmd.MAX_COUNT + 0.3
	self:runAction(cc.Sequence:create(
		cc.DelayTime:create(fDelayTimeMax),
		cc.CallFunc:create(function(ref)
			self._scene._scene:PlaySound(cmd.RES_PATH.."sound/OUT_CARD.mp3")
			self:spreadCardFinish()
		end)))
end

--伸展牌
function CardLayer:spreadCard(viewId, fDelayTime, nCurrentCount)
	print("伸展牌 bSpreadCardEnabled = " .. tostring(self.bSpreadCardEnabled))
	if not self.bSpreadCardEnabled then
		return false
	end
	
	local nodeParent = self.nodeHandDownCard[viewId]
	if nCurrentCount <= self.cbCardCount[viewId] then
		--local posX, posY = nodeParent:getPosition()
		local downCard = nodeParent:getChildByTag(nCurrentCount)
		local downCardSize = downCard:getContentSize()
		for i = 1, nCurrentCount do
			local downCardTemp = nodeParent:getChildByTag(i)
			downCardTemp:setVisible(true)
		end

		if viewId == 1 then
			nodeParent:setPositionX(display.cx - downCardSize.width/2*nCurrentCount - 57 + 135)
		elseif viewId == 3 then
			nodeParent:setPositionX(display.cx - downCardSize.width/2*nCurrentCount - 57)
		else
			nodeParent:setPositionY(display.cy + downCardSize.height/2*nCurrentCount)
		end
		
		self:runAction(cc.Sequence:create(
			cc.DelayTime:create(fDelayTime),
			cc.CallFunc:create(function(ref)
				return self:spreadCard(viewId, fDelayTime, nCurrentCount + 1)
			end)))
	else
		self:runAction(cc.Sequence:create(
			cc.DelayTime:create(0.5),
			cc.CallFunc:create(function()
				print("牌伸展完，消失牌面")
				for i = 1, nCurrentCount do
					local downCardTemp = nodeParent:getChildByTag(i)
					if downCardTemp then
						downCardTemp:setVisible(false)
					end
				end
			end)))
		
		return true
	end
end

--发完牌
function CardLayer:spreadCardFinish()
	GameLogic.SortCardList(self.cbCardData)
	for i = 1, cmd.GAME_PLAYER do
		self.nRemainCardNum = self.nRemainCardNum - self.cbCardCount[i]
		for j = 1, cmd.MAX_COUNT do
			self.nodeHandDownCard[i]:getChildByTag(j):setVisible(false)
		end
		if self._scene._scene._isReplay then
			local startCmd = GameplayerManager:getInstance():getGameStartData(i)
			if startCmd then
				self:setHandCard(i, self.cbCardCount[i], startCmd["t"].cbCardData[1])
			end
		else
			self:setHandCard(i, self.cbCardCount[i], self.cbCardData)
		end
		
	end

	self._scene:setRemainCardNum(self.nRemainCardNum)
	self._scene:sendCardFinish()
end

--抓牌
function CardLayer:catchCard(viewId, cardData, bTail)
	-- assert(math.mod(self.cbCardCount[viewId], 3) == 1 or bTail, "Can't catch card!")
	
	self._scene._scene:playRandomSound(viewId)

	local HandCard = self.nodeHandCard[viewId]:getChildByTag(1)
	HandCard.cardData = cardData
	HandCard:setVisible(true)
	self.cbCardCount[viewId] = self.cbCardCount[viewId] + 1
	self.nRemainCardNum = self.nRemainCardNum - 1
	print("抓牌 self.nRemainCardNum = " .. self.nRemainCardNum)
	self._scene:setRemainCardNum(self.nRemainCardNum)

	--抓牌的数据
	local cbCardDataTemp = nil
	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		cbCardDataTemp = GameplayerManager:getInstance():getPlayerHandCard(viewId)
	end

	if viewId == cmd.MY_VIEWID then
		cbCardDataTemp = self.cbCardData
	end

	if viewId == cmd.MY_VIEWID or self._scene._scene._isReplay then
		if not self._scene._scene._isReplay then
			self:findSameCardFromDiscard(HandCard.cardData)
		end
		

		table.insert(cbCardDataTemp, cardData)
		--设置纹理
		--local rectX = self:switchToCardRectX(cardData)
		
		local nValue = math.mod(cardData, 16)
		local nColor = math.floor(cardData/16)
		print("抓牌 viewId = " .. viewId .. " nColor = " .. nColor .. " nValue = " .. nValue)
		local strFile = cmd.RES_PATH.."game/font_big/font_"..nColor.."_"..nValue..".png"
		if self._mjSkin == 1 then
			strFile = cmd.RES_PATH.."game/font_big_1/font_"..nColor.."_"..nValue..".png"
		elseif self._mjSkin == 2 then
			strFile = cmd.RES_PATH.."game/font_big_2/font_"..nColor.."_"..nValue..".png"
		elseif self._mjSkin == 3 then
			strFile = cmd.RES_PATH.."game/font_big_3/font_"..nColor.."_"..nValue..".png"
		end
		
		if self._scene._scene._isReplay then
			local fontSmallFile = "font_small"
			local fontSmallSideFile = "font_small_side"
			if self._mjSkin == 1 then
				fontSmallFile = "font_small_1"
				fontSmallSideFile = "font_small_side_1"
			elseif self._mjSkin == 2 then
				fontSmallFile = "font_small_2"
				fontSmallSideFile = "font_small_side_2"
			elseif self._mjSkin == 3 then
				fontSmallFile = "font_small_3"
				fontSmallSideFile = "font_small_side_3"
			end
			if viewId == 1 then
				strFile = cmd.RES_PATH.."game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png"
			elseif viewId == 2 or viewId == 4 then
				strFile = cmd.RES_PATH.."game/" .. fontSmallSideFile .. "/font_"..nColor.."_"..nValue..".png"
			end
		end

		local cardFont = HandCard:getChildByTag(CardLayer.TAG_CARD_FONT)
		cardFont:setTexture(strFile)
		HandCard:removeChildByTag(666)

		local huipaFilePath = "#huipaiflag.png"
		local hpscale = 1.0
		local dx = 0
		local dy = 0
		local rotate = 0
		local flpx = false

		if self._mjSkin == 3 then
			hpscale = 0.9
			dx = -1
			dy = -1
		end
		if self._scene._scene._isReplay then
			if viewId == 1 then
				huipaFilePath = "#r_m_huipai.png"
				hpscale = 0.6
				dx = 1
				if self._mjSkin == 1 then
					dx = 5
					dy = -5
				elseif self._mjSkin == 3 then
					dx = 5
					dy = -5
				end
			elseif viewId == 2 then
				huipaFilePath = "#r_s_up_huipai.png"
				hpscale = 0.9
				rotate = 180
				flpx = true
				dx = 8
				dy = -5
				if self._mjSkin == 1 then
					dx = -5
					dy = 5
				elseif self._mjSkin == 3 then
					dx = 5
					dy = -5
				end
			elseif viewId == 4 then
				huipaFilePath = "#r_s_up_huipai.png"
				hpscale = 0.9
				rotate = 0
				flpx = true
				dx = -7
				dy = 18
				if self._mjSkin == 1 then
					dx = -5
					dy = 20
				elseif self._mjSkin == 3 then
					dx = -5
					dy = 20
				end
			end
		end

		if cardData == GameLogic.MAGIC_DATA then
			local size = HandCard:getContentSize()
			cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
			display.newSprite(huipaFilePath)
			    :move(size.width/2+dx, size.height/2+dy)
			    :setTag(666)
			    :setScale(hpscale)
			    :setRotation(rotate)
			    :setFlippedY(flpx)
			    :addTo(HandCard)
		end

		--假如可以听牌
		local cbPromptCardData = self._scene._scene:getListenPromptOutCard()
		if #cbPromptCardData > 0 then
			self:promptListenOutCard(cbPromptCardData)
		end
	end

	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		GameplayerManager:getInstance():setPlayerHandCard(viewId, cbCardDataTemp)
	end

	if viewId == cmd.MY_VIEWID then
		self.cbCardData = cbCardDataTemp
	end

end

-- 遍历数组
function CardLayer:isInTable(value, tb)
	for k,v in ipairs(tb) do
	  if v == value then
	  	return true;
	  end
	end
	return false;
end

--获取某个元素在table中出现的次数
function CardLayer:getCountOfTab(value, tb)
	local count = 0
	for k,v in ipairs(tb) do
	  if v == value then
	  	count = count + 1
	  end
	end
	return count;
end

--获取可以用来吃的癞子牌
function CardLayer:getEatCardType(index)
	for k,v in pairs(self.cbCardData) do
		print("cbCardData ---- ", k, v)
	end

	--癞子牌不能用来吃

	--如果是右吃
	local isRightEat = self:isInTable(index-1, self.cbCardData)
	if isRightEat == true and (index-1) ~= GameLogic.MAGIC_DATA  then
		isRightEat = self:isInTable(index-2, self.cbCardData)
		if isRightEat == true and (index-2) ~= GameLogic.MAGIC_DATA then
			return GameLogic.WIK_RIGHT
		end
	end

	--如果是左吃
	local isLeftEat = self:isInTable(index+1, self.cbCardData)
	if isLeftEat == true and (index+1) ~= GameLogic.MAGIC_DATA then
		isLeftEat = self:isInTable(index+2, self.cbCardData)
		if isLeftEat == true and (index+2) ~= GameLogic.MAGIC_DATA then
			return GameLogic.WIK_LEFT
		end
	end

	--如果是中吃
	local isCenterEat = self:isInTable(index-1, self.cbCardData)
	if isCenterEat == true and (index-1) ~= GameLogic.MAGIC_DATA then
		isCenterEat = self:isInTable(index+1, self.cbCardData)
		if isCenterEat == true and (index+1) ~= GameLogic.MAGIC_DATA then
			return GameLogic.WIK_CENTER
		end
	end

	return nil
end

--设置本方牌值
function CardLayer:setHandCard(viewId, cardCount, meData)
	print("更新手牌 ================================================= begin viewId = " .. tostring(viewId) .. " cardCount = " .. tostring(cardCount))
	assert(type(meData) == "table")
	if viewId == cmd.MY_VIEWID then
		self.cbCardData = meData
	end
	self.cbCardCount[viewId] = cardCount
	self.bSendOutCardEnabled = true

	--先全部隐藏
	for j = 1, cmd.MAX_COUNT do
		self.nodeHandCard[viewId]:getChildByTag(j):setVisible(false)
		self.nodeHandDownCard[viewId]:getChildByTag(j):setVisible(false)
	end
	--再显示
	if self.cbCardCount[viewId] ~= 0 then
		local nCount = 0
		local num = math.mod(self.cbCardCount[viewId], 3)
		print("self.cbCardCount[viewId] = " .. self.cbCardCount[viewId])
		print("setHandCard num = " .. num)
		if num == 2 then
			nCount = self.cbCardCount[viewId]
		elseif num == 1 then
			nCount = self.cbCardCount[viewId] + 1
		else
			assert(false)
		end

		print("nCount = " .. nCount)
		dump(meData, "setHandCard meData")
		cc.SpriteFrameCache:getInstance():addSpriteFrames("plist/pics.plist")
		for j = 1, self.cbCardCount[viewId] do
			local card = self.nodeHandCard[viewId]:getChildByTag(nCount - j + 1)
			card.cardData = meData[j]
			card:setVisible(true)
			if viewId == cmd.MY_VIEWID or self._scene._scene._isReplay then
				local cardFont = card:getChildByTag(CardLayer.TAG_CARD_FONT)

				local nValue, nColor = GameLogic.GetNValueAndNColorByCardIndex(GameLogic.SwitchToCardIndex(meData[j]))
				if nValue ~= nil and nColor ~= nil then
					local fontBigFile = "font_big"
					local fontSmallFile = "font_small"
					local fontSmallSideFile = "font_small_side"
					if self._mjSkin == 1 then
						fontBigFile = "font_big_1"
						fontSmallFile = "font_small_1"
						fontSmallSideFile = "font_small_side_1"
					elseif self._mjSkin == 2 then
						fontBigFile = "font_big_2"
						fontSmallFile = "font_small_2"
						fontSmallSideFile = "font_small_side_2"
					elseif self._mjSkin == 3 then
						fontBigFile = "font_big_3"
						fontSmallFile = "font_small_3"
						fontSmallSideFile = "font_small_side_3"
					end
					local strFile = "game/" .. fontBigFile .. "/font_"..nColor.."_"..nValue..".png"
					if self._scene._scene._isReplay then
						if viewId == 1 then
							strFile = "game/" .. fontSmallFile .. "/font_"..nColor.."_"..nValue..".png"
						elseif viewId == 2 or viewId == 4 then
							strFile = "game/" .. fontSmallSideFile .. "/font_"..nColor.."_"..nValue..".png"
						end
					end

					card:removeChildByTag(666)

					-- print("self.cbCardData[j] = " .. meData[j])
					-- print("GameLogic.MAGIC_DATA = " .. GameLogic.MAGIC_DATA)

					local huipaFilePath = "#huipaiflag.png"
					local hpscale = 1.0
					local dx = 0
					local dy = 0
					local rotate = 0
					local flpx = false
					if self._scene._scene._isReplay then
						if viewId == 1 then
							huipaFilePath = "#r_m_huipai.png"
							hpscale = 0.6
							dx = 1
							if self._mjSkin == 1 then
								dx = 5
								dy = -5
							elseif self._mjSkin == 3 then
								dx = 5
								dy = -5
							end
						elseif viewId == 2 then
							huipaFilePath = "#r_s_up_huipai.png"
							hpscale = 0.9
							rotate = 180
							flpx = true
							dx = 8
							dy = -5
							if self._mjSkin == 1 then
								dy = -5
								dx = 5
							elseif self._mjSkin == 3 then
								dy = -5
								dx = 5
							end
						elseif viewId == 4 then
							huipaFilePath = "#r_s_up_huipai.png"
							hpscale = 0.9
							rotate = 0
							flpx = true
							dx = -7
							dy = 18
							if self._mjSkin == 1 then
								dx = -5
								dy = 20
							elseif self._mjSkin == 3 then
								dx = -5
								dy = 20
							end
						end
					end

					if viewId == 3 then
						if self._mjSkin == 1 then
							dy = -4
						elseif self._mjSkin == 3 then
							hpscale = 0.9
							dx = -1
							dy = -1
						end
						
					end

					if meData[j] == GameLogic.MAGIC_DATA then
						local size = card:getContentSize()
						display.newSprite(huipaFilePath)
						    :move(size.width/2 + dx, size.height/2 + dy)
						    :setTag(666)
						    :setScale(hpscale)
						    :setRotation(rotate)
						    :setFlippedY(flpx)
						    :addTo(card)			    
						print("这一张是癞子牌 ------ GameLogic.MAGIC_DATA = " .. GameLogic.MAGIC_DATA .. " nValue = " .. nValue)
					end
					
					cardFont:setTexture(strFile)
				end
			end
		end
	end
	print("更新手牌 ================================================= end")
end

--删除手上的牌
function CardLayer:removeHandCard(viewId, cardData, bOutCard)
	assert(type(cardData) == "table")
	for k,v in pairs(cardData) do
		print("removeHandCard cardData ---- ", k, v)
	end
	local cbRemainCount = self.cbCardCount[viewId] - #cardData
	print("删除牌后 ，剩余的牌数 = " .. cbRemainCount)
	if bOutCard and math.mod(cbRemainCount, 3) ~= 1 then
		return false
	end
	self.cbCardCount[viewId] = cbRemainCount

	local cbCardDataTemp = self.cbCardData

	if viewId == cmd.MY_VIEWID then
		cbCardDataTemp = self.cbCardData
	end

	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		--获取某个视图的手牌
		cbCardDataTemp = GameplayerManager:getInstance():getPlayerHandCard(viewId)
	end

	dump(cbCardDataTemp, "出牌前的队列")

	--如果删除手牌的人是自己
	if viewId == cmd.MY_VIEWID or self._scene._scene._isReplay then
		for i = 1, #cardData do
			for j = 1, #cbCardDataTemp do
				if cbCardDataTemp[j] == cardData[i] then
					print("删除 打出的牌 " .. cbCardDataTemp[j])
					table.remove(cbCardDataTemp, j)
					break
				end
				assert(j ~= #cbCardDataTemp, "WithOut this card to remove!")
			end
		end
		GameLogic.SortCardList(cbCardDataTemp)
	end

	if viewId == cmd.MY_VIEWID then 
		self.cbCardData = cbCardDataTemp
	end

	if self._scene._scene._isReplay and viewId ~= cmd.MY_VIEWID then
		--获取某个视图的手牌
		GameplayerManager:getInstance():setPlayerHandCard(viewId, cbCardDataTemp)
	end

	dump(cbCardDataTemp, "出牌后的队列")

	self:setHandCard(viewId, self.cbCardCount[viewId], cbCardDataTemp)

	return true
end

--牌打到弃牌堆
function CardLayer:discard(viewId, cardData)
	local fontSmallFile = "font_small"
	local fontSmallSideFile = "font_small_side"
	if self._mjSkin == 1 then
		fontSmallFile = "font_small_1"
		fontSmallSideFile = "font_small_side_1"
	elseif self._mjSkin == 2 then
		fontSmallFile = "font_small_2"
		fontSmallSideFile = "font_small_side_2"
	elseif self._mjSkin == 3 then
		fontSmallFile = "font_small_3"
		fontSmallSideFile = "font_small_side_3"
	end

	local res1 = 
	{
		cmd.RES_PATH.."game/" .. fontSmallFile .. "/",
		cmd.RES_PATH.."game/" .. fontSmallSideFile .. "/", 
		cmd.RES_PATH.."game/" .. fontSmallFile .. "/",
		cmd.RES_PATH.."game/" .. fontSmallSideFile .. "/"
	}
	local res = cmd.RES_PATH..string.format("game/cardDown_%d.png", viewId)
	local width = 0
	local height = 0
	local fSpacing = 0
	local posX = 0
	local posY = 0
	local pos = cc.p(0, 0)
	local nLimit = 0
	local fBase = 0
	local countTemp = self.nDiscardCount[viewId]
	local bVert = viewId == 1 or viewId == cmd.MY_VIEWID
	if bVert then
		width = 44
		height = 54
		local LineSpan = 45
		nLimit = 12
		if self._mjSkin == 1 then
			width = 36
			height = 44
		elseif self._mjSkin == 3 then
			width = 36
			height = 44
			LineSpan = 51
		else
			nLimit = 11
		end

		fSpacing = width
		

		while countTemp >= nLimit*2 do   	--超过一层
			fBase = fBase + height - LineSpan
			countTemp = countTemp - nLimit*2
		end

		while countTemp >= nLimit do 		--超过一行
			posY = posY - height
			countTemp = countTemp - nLimit
		end

		local posX = fSpacing*countTemp
		pos = viewId == cmd.MY_VIEWID and cc.p(posX, posY + fBase) or cc.p(-posX, -posY + fBase)
	else
		width = 55
		height = 47
		fSpacing = 35
		--一行 nLimit 个
		nLimit = 11
		if self._mjSkin == 1 then
			width = 49
			height = 33
			fSpacing = 33
		elseif self._mjSkin == 3 then
			width = 50
			height = 31
			fSpacing = 31
		else
			nLimit = 9
		end
		
		

		while countTemp >= nLimit*2 do 		--超过一层
			fBase = fBase + 14
			countTemp = countTemp - nLimit*2
		end

		while countTemp >= nLimit do 		--超过一行
			posX = posX - width
			countTemp = countTemp - nLimit
		end

		local posY = fSpacing*countTemp
		pos = viewId == 4 and cc.p(-posX, posY + fBase) or cc.p(posX, -posY + fBase)
	end

	--local rectX = self:switchToCardRectX(cardData)
	local nValue = math.mod(cardData, 16)
	local nColor = math.floor(cardData/16)
	--牌底
	local card = display.newSprite(res1[viewId].."card_down.png")
	card:move(pos)
	card:setTag(self.nDiscardCount[viewId] + 1)
	card.cardData = cardData
	card:addTo(self.nodeDiscard[viewId])
	--字体
	local strFile = res1[viewId].."font_"..nColor.."_"..nValue..".png"
	local ddy = 0
	if self._mjSkin == 1 then
		ddy = 8
	elseif self._mjSkin == 3 then
		ddy = 8
	else
		ddy = 6.5
	end
	local cardSize = card:getContentSize()
	local cardFont = display.newSprite(strFile)
		:move(cardSize.width/2, cardSize.height/2 + ddy)
		:addTo(card)
	cardFont:setTag(CardLayer.TAG_CARD_FONT)
	if viewId == 1 or viewId == 4 then
		cardFont:setRotation(180)
	end
	--修改了Z轴顺序，设置重叠效果   **** 需注意处理 ******
	if viewId == 1 or viewId == 4 then
		local nOrder = 0
		if self.nDiscardCount[viewId] >= nLimit*6 then
			assert(false)
		elseif self.nDiscardCount[viewId] >= nLimit*4 then
			nOrder = 80 - (self.nDiscardCount[viewId] - nLimit*4)
		elseif self.nDiscardCount[viewId] >= nLimit*2 then
			nOrder = 80 - (self.nDiscardCount[viewId] - nLimit*2)*2
		else
			nOrder = 80 - self.nDiscardCount[viewId]*3
		end
		card:setLocalZOrder(nOrder)
	end
	--计数
	self.nDiscardCount[viewId] = self.nDiscardCount[viewId] + 1
end

--从弃牌堆回收牌（有人要这张牌，碰或杠等）
function CardLayer:recycleDiscard(viewId)
	self.nodeDiscard[viewId]:getChildByTag(self.nDiscardCount[viewId]):removeFromParent()
	self.nDiscardCount[viewId] = self.nDiscardCount[viewId] - 1
end

function CardLayer:stopSameCard()
	for i = 1, 4 do
		local children = self.nodeDiscard[i]:getChildren()
		for j = 1, #children do
			children[j]:stopAllActions()
			children[j]:setColor(cc.c3b(255,255,255))
			local cardFont = children[j]:getChildByTag(CardLayer.TAG_CARD_FONT)
			if cardFont then
				cardFont:stopAllActions()
				cardFont:setColor(cc.c3b(255,255,255))
			end
		end
	end

	for i = 1, #self.bpBgCardArray do
		if self.bpBgCardArray[i] and self.bpBgCardArray[i].cardData then
			self.bpBgCardArray[i]:stopAllActions()
			self.bpBgCardArray[i]:setColor(cc.c3b(255,255,255))
			local cardFont = self.bpBgCardArray[i]:getChildByTag(CardLayer.TAG_CARD_FONT)
			if cardFont then
				cardFont:stopAllActions()
				cardFont:setColor(cc.c3b(255,255,255))
			end
		end
	end
end

--cardData从牌堆里查找相同点数的牌
function CardLayer:findSameCardFromDiscard(cardData)
	self:stopSameCard()
	local sameCardArray = {}
	for i = 1, 4 do
		local children = self.nodeDiscard[i]:getChildren()
		for j = 1, #children do
			if children[j].cardData and children[j].cardData == cardData then
				table.insert(sameCardArray, children[j])
			end
		end
	end

	for i = 1, #self.bpBgCardArray do
		if self.bpBgCardArray[i] and self.bpBgCardArray[i].cardData and self.bpBgCardArray[i].cardData == cardData then
			table.insert(sameCardArray, self.bpBgCardArray[i])
		end
	end
	
	for k,v in pairs(sameCardArray) do
		print("已出现的牌", k, v.cardData)
		-- 用持续时间和颜色创建动作，第一个参数为持续时间，后面三个为颜色值  
	  	local action1 = cc.TintTo:create(0.5, 255,228,150)
	  	local action2 = cc.TintTo:create(0.5, 255,255,255)
		v:runAction(cc.RepeatForever:create(cc.Sequence:create(action1:clone(), action2:clone())))
		local cardFont = v:getChildByTag(CardLayer.TAG_CARD_FONT)
		if cardFont then
			cardFont:runAction(cc.RepeatForever:create(cc.Sequence:create(action1:clone(), action2:clone())))
		end
	end
end

--碰或杠
function CardLayer:bumpOrBridgeCard(viewId, cbCardData, nShowStatus)
	print("碰牌的人 viewId = " .. viewId)
	assert(type(cbCardData) == "table")
	-- table.sort(cbCardData)
	local resCard = 
	{
		cmd.RES_PATH.."game/font_small/card_down.png",
		cmd.RES_PATH.."game/font_small_side/card_down.png", 
		cmd.RES_PATH.."game/font_middle/card_down.png",
		cmd.RES_PATH.."game/font_small_side/card_down.png"
	}
	local resFont = 
	{
		cmd.RES_PATH.."game/font_small/",
		cmd.RES_PATH.."game/font_small_side/", 
		cmd.RES_PATH.."game/font_middle/",
		cmd.RES_PATH.."game/font_small_side/"
	}

	local scaleRate = {
		1, 1, 1, 1
	}

	if self._mjSkin == 1 then
		resCard = 
		{
			cmd.RES_PATH.."game/font_small_1/card_down.png",
			cmd.RES_PATH.."game/font_small_side_1/card_down.png", 
			cmd.RES_PATH.."game/font_big_1/card_down.png",
			cmd.RES_PATH.."game/font_small_side_1/card_down.png"
		}

		resFont = 
		{
			cmd.RES_PATH.."game/font_small_1/",
			cmd.RES_PATH.."game/font_small_side_1/", 
			cmd.RES_PATH.."game/font_big_1/",
			cmd.RES_PATH.."game/font_small_side_1/"
		}

		scaleRate = {
			1, 1, 0.9, 1
		}
	elseif self._mjSkin == 2 then
		resCard = 
		{
			cmd.RES_PATH.."game/font_small_2/card_down.png",
			cmd.RES_PATH.."game/font_small_side_2/card_down.png", 
			cmd.RES_PATH.."game/font_middle_2/card_down.png",
			cmd.RES_PATH.."game/font_small_side_2/card_down.png"
		}

		resFont = 
		{
			cmd.RES_PATH.."game/font_small_2/",
			cmd.RES_PATH.."game/font_small_side_2/", 
			cmd.RES_PATH.."game/font_middle_2/",
			cmd.RES_PATH.."game/font_small_side_2/"
		}
	elseif self._mjSkin == 3 then
		resCard = 
		{
			cmd.RES_PATH.."game/font_small_3/card_down.png",
			cmd.RES_PATH.."game/font_small_side_3/card_down.png", 
			cmd.RES_PATH.."game/font_middle_3/card_down.png",
			cmd.RES_PATH.."game/font_small_side_3/card_down.png"
		}

		resFont = 
		{
			cmd.RES_PATH.."game/font_small_3/",
			cmd.RES_PATH.."game/font_small_side_3/", 
			cmd.RES_PATH.."game/font_middle_3/",
			cmd.RES_PATH.."game/font_small_side_3/"
		}
	end

	for k,v in pairs(scaleRate) do
		print("scaleRate  ",k,v)
	end
	
	local width = 0
	local height = 0
	local widthTotal = 0
	local heightTotal = 0
	local fSpacing = 0
	if viewId == 1 then
		width = 44 * scaleRate[1]
		height = 67 * scaleRate[1]
		if self._mjSkin == 1 then
			width = 36 * scaleRate[1]
			height = 59 * scaleRate[1]
		elseif self._mjSkin == 3 then
			width = 36 * scaleRate[1]
			height = 59 * scaleRate[1]
		end
		fSpacing = width
	elseif viewId == 3 then
		if self._mjSkin == 1 then
			width = CARD_WIDTH * scaleRate[3]
			height = CARD_HEIGHT * scaleRate[3]
		elseif self._mjSkin == 3 then
			width = 72 * scaleRate[3]
			height = 112 * scaleRate[3]
		else
			width = 80
			height = 116
		end
		
		fSpacing = width
	else
		width = 55 * scaleRate[viewId]
		height = 47
		fSpacing = 35
		if self._mjSkin == 3 then
			fSpacing = 32
		end
	end

	local fN = {15, 15, 15, 15}
	local fParentSpacing = fSpacing*3 + fN[viewId]
	local nodeParent = cc.Node:create()
		:move(self.nBpBgCount[viewId]*fParentSpacing*multipleBpBgCard[viewId][1], 
				self.nBpBgCount[viewId]*fParentSpacing*multipleBpBgCard[viewId][2])
		:addTo(self.nodeBpBgCard[viewId])

	if nShowStatus ~= GameLogic.SHOW_CHI then
		--明杠
		if nShowStatus == GameLogic.SHOW_MING_GANG then
			nodeParentMG = self.nodeBpBgCard[viewId]:getChildByTag(cbCardData[1])
			--assert(nodeParentMG, "None of this bump card!")
			if nodeParentMG then
				self.nBpBgCount[viewId] = self.nBpBgCount[viewId] - 1
				nodeParent:removeFromParent()
				nodeParentMG:removeAllChildren()
				nodeParent = nodeParentMG
			end
		end
		nodeParent:setTag(cbCardData[1])
	end

	--个个牌形之间的间隔
	local isInsertPos = false
	for i = 1, #cbCardData do
		--local rectX = self:switchToCardRectX(cbCardData[i])
		--牌底
		local card = display.newSprite(resCard[viewId])
			:move(i*fSpacing*multipleBpBgCard[viewId][1], i*fSpacing*multipleBpBgCard[viewId][2])
			--:setTextureRect(cc.rect(width*rectX, 0, width, height))
			:addTo(nodeParent)
		card.cardData = cbCardData[i]
		table.insert(self.bpBgCardArray, card)
		--字体
		local cardSize = card:getContentSize()
		local nValue = math.mod(cbCardData[i], 16)
		local nColor = math.floor(cbCardData[i]/16)
		local strFile = resFont[viewId].."font_"..nColor.."_"..nValue..".png"
		local ddy = 0
		if viewId == 3 then
			ddy = 8
			if self._mjSkin == 3 then
				ddy = 10
			end
		elseif viewId == 2 then
			ddy = 7
		elseif viewId == 4 then
			ddy = 7
		elseif viewId == 1 then
			ddy = 8
		end
		local cardFont = display.newSprite(strFile)
			:move(cardSize.width/2, cardSize.height/2 + ddy)
			:setTag(1)
			:addTo(card)
		if viewId == 1 or viewId == 4 then
			cardFont:setRotation(180)
		end

		if viewId == 4 then
			card:setLocalZOrder(5 - i)
		end

		card:setScale(scaleRate[viewId])

		if i == 4  then 		--杠
			local moveUp = {17, 14, 23, 14}
			card:move(2*fSpacing*multipleBpBgCard[viewId][1], 2*fSpacing*multipleBpBgCard[viewId][2] + moveUp[viewId])
			card:setLocalZOrder(5)
		end

		--皮子牌
		local piziCardData = GameLogic.getPiZiCardData()

		print("aaa nShowStatus = " .. nShowStatus)
		print("aaa cbCardData[i]  = " .. cbCardData[i] )
		print("aaa piziCardData = " .. piziCardData)
		--皮子牌第三张移动位置
		if (nShowStatus == GameLogic.SHOW_AN_GANG or nShowStatus == GameLogic.SHOW_FANG_GANG) and i == 3 and cbCardData[i] == piziCardData then
			local moveUp = {17, 14, 23, 14}
			--3是自己
			local dx = 0
			local dy = 0
			if viewId == 1 then
				dx = - card:getContentSize().width/2
				dy = 0
			elseif viewId == 3 then
				dx = - card:getContentSize().width/2
				dy = 0
			elseif viewId == 4 then
				dx = 0
				dy = - card:getContentSize().height/2
			elseif viewId == 2 then
				dx = 0
				dy = card:getContentSize().height/2
			end
			card:move(2*fSpacing*multipleBpBgCard[viewId][1] + dx, 2*fSpacing*multipleBpBgCard[viewId][2] + moveUp[viewId] + dy)
			card:setLocalZOrder(5)
		end

		if nShowStatus == GameLogic.SHOW_AN_GANG and viewId ~= cmd.MY_VIEWID then 		--暗杠
			card:setTexture(resFont[viewId].."card_back.png")
			card:removeChildByTag(1)
		end

		-- if (nShowStatus == GameLogic.SHOW_AN_GANG or nShowStatus == GameLogic.SHOW_MING_GANG) and i == 3 and cbCardData[i] == piziCardData then

		-- end

		

		if nShowStatus == GameLogic.SHOW_AN_GANG then --and viewId == cmd.MY_VIEWID 
			--如果不是皮子，那么前三张都盖住
			if i ~= 4 and cbCardData[i] ~= piziCardData then
				card:setTexture(resFont[viewId].."card_back.png")
				card:removeChildByTag(1)
			end

			--如果是皮子杠，那么第三张显示
			if i ~= 3 and cbCardData[i] == piziCardData then
				card:setTexture(resFont[viewId].."card_back.png")
				card:removeChildByTag(1)
			end
		end

		--添加牌到记录里
		if nShowStatus ~= GameLogic.SHOW_MING_GANG or i == 4 then
			--如果插入的只有一个，那就是碰后的杠
			if nShowStatus == GameLogic.SHOW_MING_GANG then
				local pos = 1
				while pos <= #self.cbBpBgCardData[viewId] do 
					print("self.cbBpBgCardData[viewId][pos] = " .. self.cbBpBgCardData[viewId][pos] .. "   cbCardData[i] = " .. cbCardData[i])
					if self.cbBpBgCardData[viewId][pos] == cbCardData[i] then
						table.insert(self.cbBpBgCardData[viewId], pos, cbCardData[i])
						break
					end
					pos = pos + 1
				end
			else
				table.insert(self.cbBpBgCardData[viewId], cbCardData[i])
			end
			print("碰，杠插入值 椅子：" .. (viewId-1) .. " 插入的数据："..cbCardData[i])
		end
	end

	if isInsertPos == false then
		print("插入前判断最后一个牌 = " .. self.cbBpBgCardData[viewId][#self.cbBpBgCardData[viewId]])
		if self.cbBpBgCardData[viewId][#self.cbBpBgCardData[viewId]] ~= -2 and self.cbBpBgCardData[viewId][#self.cbBpBgCardData[viewId]] ~= -1 then
			if GameLogic.SHOW_AN_GANG == nShowStatus then
				table.insert(self.cbBpBgCardData[viewId], -2)
			else
				table.insert(self.cbBpBgCardData[viewId], -1)
				print("插入 -1")
			end
		else
			print("前面是-1, 或-2暂时不能插")
		end
	end

	for k,v in pairs(self.cbBpBgCardData[viewId]) do
		print("self.cbBpBgCardData[viewId] = ", viewId, k, v)
	end
	
	self.nBpBgCount[viewId] = self.nBpBgCount[viewId] + 1
end

--检查碰、杠牌里是否有这张牌
function CardLayer:checkBumpOrBridgeCard(viewId, cbCardData)
	local card = self.nodeBpBgCard[viewId]:getChildByTag(cbCardData)
	if card then
		return true
	else
		return false
	end
end

function CardLayer:getBpBgCardData()
	return self.cbBpBgCardData
end

function CardLayer:gameEnded()
	self.bSendOutCardEnabled = false
end

function CardLayer:switchToCardRectX(data)
	assert(data, "this card is nil")
	local cardIndex = GameLogic.SwitchToCardIndex(data)
	local rectX = cardIndex == GameLogic.MAGIC_INDEX and 32 or cardIndex
	return rectX
end

--使部分牌变灰（参数为不变灰的）
function CardLayer:makeCardGray(cbOutCardData)
	--assert(#self.cbListenList > 0 and math.mod(#self.cbCardData - 2, 3) == 0)
	local cbCardCount = #self.cbCardData
	--先全部变灰
	for i = cmd.MAX_COUNT, 1, -1 do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		card:setColor(cc.c3b(100, 100, 100))
		self.bCardGray[i] = true
	end
	--将牌补满(ui与值的对齐方式)
	local nCount = 0
	local num = math.mod(cbCardCount, 3)
	if num == 2 then
		nCount = cbCardCount
	elseif num == 1 then
		nCount = cbCardCount + 1
	else
		assert(false)
	end
	--再恢复可打出的牌
	for i = 1, #cbOutCardData do
		for j = 1, nCount do
			if cbOutCardData[i] == self.cbCardData[j] then
				--assert(cbOutCardData[i] ~= GameLogic.MAGIC_DATA, "The magic card can't out!")
				local nTag = nCount - j + 1
				local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(nTag)
				card:setColor(cc.c3b(255, 255, 255))
				self.bCardGray[nTag] = false
				break
			end
		end
	end
end

function CardLayer:promptListeningCard(cardData)
	--assert(#self.cbListenList > 0)

	if nil == cardData then
		assert(self.currentOutCard > 0)
		cardData = self.currentOutCard
	end

	for i = 1, #self.cbListenList do
		if self.cbListenList[i].cbOutCard == cardData then
			self._scene:setListeningCard(self.cbListenList[i].cbListenCard)
			break
		end
	end
end

function CardLayer:startListenCard()
	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		if i == 1 then
			card:setColor(cc.c3b(255, 255, 255))
			self.bCardGray[i] = false
		else
			card:setColor(cc.c3b(100, 100, 100))
			self.bCardGray[i] = true
		end
	end
end

function CardLayer:promptListenOutCard(cbPromptOutData)
	--还原
	local cbCardCount = #self.cbCardData
	for i = 1, cmd.MAX_COUNT do
		local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(i)
		card:getChildByTag(CardLayer.TAG_LISTEN_FLAG):setVisible(false)
	end
	if cbPromptOutData == nil then
		return 
	end
	--校验
	assert(type(cbPromptOutData) == "table")
	-- assert(math.mod(cbCardCount - 2, 3) == 0, "You can't out card now!")
	for i = 1, #cbPromptOutData do
		if cbPromptOutData[i] ~= GameLogic.MAGIC_DATA then
			for j = 1, cbCardCount do
				if cbPromptOutData[i] == self.cbCardData[j] then
					local nTag = cbCardCount - j + 1
					local card = self.nodeHandCard[cmd.MY_VIEWID]:getChildByTag(nTag)
					if card then
						card:getChildByTag(CardLayer.TAG_LISTEN_FLAG):setVisible(false)
					end
				end
			end
		end
	end
end

function CardLayer:setTableCardByHeapInfo(viewId, cbHeapCardInfo, wViewHead, wViewTail)
	local nViewMinTag = (cmd.GAME_PLAYER - viewId)*28 + 1
	local nViewMaxTag = (cmd.GAME_PLAYER - viewId)*28 + 28
	--从右数隐藏几张牌
	local nTagStart1 = nViewMinTag
	local nTagEnd1 = nViewMinTag + cbHeapCardInfo[1] - 1
	for i = nTagStart1, nTagEnd1 do
		--self.nodeTableCard:getChildByTag(i):setVisible(false)
	end
	--从左数隐藏几张牌
	local nTagStart2 = nViewMaxTag - cbHeapCardInfo[2] + 1
	local nTagEnd2 = nViewMaxTag
	for i = nTagStart2, nTagEnd2 do
		--self.nodeTableCard:getChildByTag(i):setVisible(false)
	end

	if viewId == wViewHead then
		self.currentTag = (cmd.GAME_PLAYER - viewId)*28 + cbHeapCardInfo[1] + 1
	elseif viewId == wViewTail then
		self.nTableTailCardTag = (cmd.GAME_PLAYER - viewId)*28 + (28 - cbHeapCardInfo[2])
	end
	--牌堆总共剩余多少牌
	-- self.nRemainCardNum = self.nRemainCardNum - cbHeapCardInfo[1] - cbHeapCardInfo[2]
	self._scene:setRemainCardNum(self.nRemainCardNum)
end

function CardLayer:touchSendOutCard(cbCardData)
	if not self.bSendOutCardEnabled then
		return false
	end

	self:stopSameCard()
	self.currentOutCard = cbCardData
	self._scene:setListeningCard(nil)
	--发送消息
	return self._scene._scene:sendOutCard(cbCardData)
end

function CardLayer:outCardAuto()
	local nCount = #self.cbCardData
	if math.mod(nCount - 2, 3) ~= 0 then
		return false
	end
	for i = nCount, 1, -1 do
		local nTag = nCount - i + 1
		local bOk = not self.bCardGray[nTag]
		if self.cbCardData[i] ~= GameLogic.MAGIC_DATA and bOk then
			self:touchSendOutCard(self.cbCardData[i])
			break
		end
	end

	return true
end

function CardLayer:isMagicData(temp)
	return GameLogic.MAGIC_DATA == temp
end

--获取能吃此牌的手牌
--return a table
function CardLayer:getCanChiCombin(currCard)
	local ret = {}

	--如果是右吃
	local isRightEat = self:isInTable(currCard-1, self.cbCardData)
	if isRightEat == true and self:isMagicData(currCard-1) == false then
		isRightEat = self:isInTable(currCard-2, self.cbCardData)
		if isRightEat == true and self:isMagicData(currCard-2) == false then
			table.insert(ret, {GameLogic.WIK_RIGHT, currCard, currCard - 2, currCard - 1})
		end
	end

	--如果是中吃
	local isCenterEat = self:isInTable(currCard-1, self.cbCardData)
	if isCenterEat == true and self:isMagicData(currCard-1) == false then
		isCenterEat = self:isInTable(currCard+1, self.cbCardData)
		if isCenterEat == true and self:isMagicData(currCard+1) == false then
			table.insert(ret, {GameLogic.WIK_CENTER, currCard, currCard - 1, currCard + 1})
		end
	end

	--如果是左吃
	local isLeftEat = self:isInTable(currCard+1, self.cbCardData)
	if isLeftEat == true and self:isMagicData(currCard+1) == false then
		isLeftEat = self:isInTable(currCard+2, self.cbCardData)
		if isLeftEat == true and self:isMagicData(currCard+2) == false then
			table.insert(ret, {GameLogic.WIK_LEFT, currCard, currCard + 1, currCard + 2})
		end
	end

	return ret
end

--获取能吃此牌的手牌
--return a table
function CardLayer:getCanGangCombin(data, chairId, isOtherOp)
	print("isOtherOp = " .. tostring(isOtherOp))
	local ret = {}
	local cbCardCount = #self.cbCardData

	--储存每张牌对应几张
	local cbCardIndex = GameLogic.DataToCardIndex(self.cbCardData)

	--计算出皮子牌
	local piziCardData = GameLogic.getPiZiCardData()
	print("皮子牌是 ： " .. piziCardData)

	--如果判断摸起来的牌，能不能和手牌凑成4张
	if data and data > 0 then
		local index = GameLogic.SwitchToCardIndex(data)
		--皮子牌3张可以暗杠，2张可以明杠--摸起来的这张牌是皮子牌
		if (cbCardIndex[index] == 2 and data == piziCardData) or (cbCardIndex[index] == 3 and isOtherOp and data ~= piziCardData ) then
			print("手上有3张 可以杠 data = " .. data)
			if data ~= GameLogic.MAGIC_DATA then
				table.insert(ret, {GameLogic.WIK_GANG, data, data, data, data})
			end
		end
	end

	if isOtherOp then
		print("别人打的牌，不可以暗杠")
		return ret
	end

	print("自己摸起来的牌，可以暗杠")
	--手牌有4张 暗杠
	for i = 1, GameLogic.NORMAL_INDEX_MAX do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		--普通牌四张可以杠，皮子牌三张可以暗杠
		if cbCardIndex[i] == 4 or (piziCardData == dataTemp and cbCardIndex[i] == 3) then
			if dataTemp ~= GameLogic.MAGIC_DATA then
				print("手上有4张 普通牌 可以杠 data = " .. dataTemp)
				table.insert(ret, {GameLogic.WIK_GANG, dataTemp, dataTemp, dataTemp, dataTemp})
			end
			
		end
	end

	--特殊牌红中等
	for i = 28, 34 do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		if cbCardIndex[i] == 4 then
			if dataTemp ~= GameLogic.MAGIC_DATA then
				print("手上有4张 特殊牌 可以杠 data = " .. dataTemp)
				table.insert(ret, {GameLogic.WIK_GANG, dataTemp, dataTemp, dataTemp, dataTemp})
			end
		end
	end

	print("data = " .. data .. " chairId = " .. chairId)
	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId]

	for k,v in pairs(bpbgData) do
		print("self.bpbgData -- ")
		for i,j in pairs(v) do
			print("self.bpbgData -- ", i, j)
		end
		print("self.bpbgData -- ")
	end

	--碰了之后，手牌里面还有能杠的
	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId]

	local tempUserData = {}
	
	local tempOne = {}
	--按类型分开
	for i, v in ipairs(userData) do
		if i == 1 then
			tempOne = {}
		end
		if v == -1 or v == -2 then
			table.insert(tempUserData, tempOne)
			tempOne = {}
		else
			table.insert(tempOne, v)
		end
	end

	for k,v in pairs(tempUserData) do
		print("-=-------------------------------- bump")
		for a,b in ipairs(v) do
			print(a, b)
		end
		print("-=-------------------------------- bump")
	end

	for k,v in pairs(tempUserData) do
		--碰过的牌
		if v[1] and v[1] == v[2] and v[1] == v[3] and v[4] == nil then
			print("此牌为碰 " .. v[1] .. " 检测 self.cbCardData = " .. tostring(self.cbCardData))
			for a,b in pairs(self.cbCardData) do
				print(" =========== ", a,b)
			end
			if self:isInTable(v[1], self.cbCardData) then
				table.insert(ret, {GameLogic.WIK_GANG, v[1], v[1], v[1], v[1]})
			end
		end
	end

	print("摸起一张碰了的牌 可以杠 data = " .. data)
	--摸到已经碰了的牌，杠
	return ret
end

--检查手里的杠
function CardLayer:getGangCard(data, chairId)
	local cbCardCount = #self.cbCardData

	--储存每张牌对应几张
	local cbCardIndex = GameLogic.DataToCardIndex(self.cbCardData)

	--如果判断摸起来的牌，能不能和手牌凑成4张
	if data and data > 0 then
		local index = GameLogic.SwitchToCardIndex(data)
		if cbCardIndex[index] == 3 then
			print("手上有3张 可以杠 data = " .. data)
			return data
		end
	end

	--手牌有4张 暗杠
	for i = 1, GameLogic.NORMAL_INDEX_MAX do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		if cbCardIndex[i] == 4 then
			data = dataTemp
			print("手上有4张 可以杠 data = " .. data)
			return data
		end
	end

	--特殊牌红中等
	for i = 28, 34 do
		local dataTemp = GameLogic.SwitchToCardData(i-1)
		if cbCardIndex[i] == 4 then
			data = dataTemp
			print("手上有4张 可以杠 data = " .. data)
			return data
		end
	end

	print("data = " .. data .. " chairId = " .. chairId)
	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId+1]

	

	for k,v in pairs(bpbgData) do
		print("self.bpbgData -- ")
		for i,j in pairs(v) do
			print("self.bpbgData -- ", i, j)
		end
		print("self.bpbgData -- ")
	end

	--碰了之后，手牌里面还有能杠的
	--获取碰了的牌
	local bpbgData = self:getBpBgCardData()
	local userData = bpbgData[chairId+1]

	for k,v in pairs(userData) do
		print("v = ", v)
		if self:isInTable(v, self.cbCardData) then
			data = v
			print("碰了次牌 可以杠 data = " .. data)
			return data
		end
	end

	print("摸起一张碰了的牌 可以杠 data = " .. data)
	--摸到已经碰了的牌，杠
	return data
end

--用户必须点胡牌
function CardLayer:isUserMustWin()
	if #self.cbCardData == 2 then
		if self.cbCardData[1] == self.cbCardData[2] and 
			self.cbCardData[2] == GameLogic.MAGIC_DATA then
			return true
		end
	end

	return false
end

return CardLayer