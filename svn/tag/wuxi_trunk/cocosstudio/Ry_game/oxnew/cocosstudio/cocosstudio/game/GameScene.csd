<GameFile>
  <PropertyGroup Name="GameScene" Type="Layer" ID="8ecbc710-2b16-4f4d-84a6-70956f2487f6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskPanel" ActionTag="293546565" Tag="24" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_buttonBg" Tag="8" IconVisible="False" LeftMargin="1207.5258" RightMargin="3.4742" TopMargin="1.6653" BottomMargin="173.3347" ctype="SpriteObjectData">
            <Size X="123.0000" Y="575.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="1269.0258" Y="748.3347" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9513" Y="0.9978" />
            <PreSize X="0.0922" Y="0.7667" />
            <FileData Type="PlistSubImage" Path="sp_buttonBg.png" Plist="gameScene_oxnew.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_exit" Tag="13" IconVisible="False" LeftMargin="1210.0000" RightMargin="8.0000" TopMargin="461.0000" BottomMargin="175.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="86" Scale9Height="92" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="116.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1268.0000" Y="232.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9505" Y="0.3093" />
            <PreSize X="0.0870" Y="0.1520" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_exit_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_exit_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_exit_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_howPlay" Tag="12" IconVisible="False" LeftMargin="1210.0000" RightMargin="8.0000" TopMargin="347.0000" BottomMargin="289.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="86" Scale9Height="92" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="116.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1268.0000" Y="346.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9505" Y="0.4613" />
            <PreSize X="0.0870" Y="0.1520" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_howPlay_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_howPlay_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_howPlay_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_chat" Tag="11" IconVisible="False" LeftMargin="1210.0000" RightMargin="8.0000" TopMargin="233.0000" BottomMargin="403.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="86" Scale9Height="92" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="116.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1268.0000" Y="460.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9505" Y="0.6133" />
            <PreSize X="0.0870" Y="0.1520" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_chat_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_chat_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_chat_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_set" Tag="10" IconVisible="False" LeftMargin="1210.0000" RightMargin="8.0000" TopMargin="119.0000" BottomMargin="517.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="86" Scale9Height="92" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="116.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1268.0000" Y="574.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9505" Y="0.7653" />
            <PreSize X="0.0870" Y="0.1520" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_set_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_set_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_set_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_switch" Tag="9" IconVisible="False" LeftMargin="1210.0000" RightMargin="8.0000" TopMargin="5.0000" BottomMargin="631.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="86" Scale9Height="92" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="116.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1268.0000" Y="688.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9505" Y="0.9173" />
            <PreSize X="0.0870" Y="0.1520" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_switch_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_switch_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_switch_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_callBanker" Tag="15" IconVisible="False" LeftMargin="478.9797" RightMargin="710.0203" TopMargin="477.9995" BottomMargin="194.0005" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="115" Scale9Height="56" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="145.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="551.4797" Y="233.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4134" Y="0.3107" />
            <PreSize X="0.1087" Y="0.1040" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_callBanker_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_callBanker_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_callBanker_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_cancel" Tag="14" IconVisible="False" LeftMargin="702.3975" RightMargin="486.6025" TopMargin="477.9995" BottomMargin="194.0005" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="115" Scale9Height="56" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="145.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="774.8975" Y="233.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5809" Y="0.3107" />
            <PreSize X="0.1087" Y="0.1040" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_cancel_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_cancel_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_cancel_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_showCard" Tag="16" IconVisible="False" LeftMargin="1052.0663" RightMargin="136.9337" TopMargin="552.5606" BottomMargin="119.4394" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="115" Scale9Height="56" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="145.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1124.5663" Y="158.4394" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8430" Y="0.2113" />
            <PreSize X="0.1087" Y="0.1040" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_showCard_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_showCard_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_showCard_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_prompt" Tag="17" IconVisible="False" LeftMargin="1050.1718" RightMargin="138.8282" TopMargin="652.9083" BottomMargin="19.0917" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="115" Scale9Height="56" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="145.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1122.6718" Y="58.0917" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8416" Y="0.0775" />
            <PreSize X="0.1087" Y="0.1040" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_prompt_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_prompt_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_prompt_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_start" Tag="18" IconVisible="False" LeftMargin="582.5000" RightMargin="582.5000" TopMargin="329.0000" BottomMargin="329.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="70" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="169.0000" Y="92.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.1267" Y="0.1227" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_start_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_start_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_start_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimeText" ActionTag="285554669" Tag="33" IconVisible="False" LeftMargin="1074.7324" RightMargin="183.2676" TopMargin="40.5344" BottomMargin="675.4656" FontSize="30" LabelText="18:36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="76.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1112.7324" Y="692.4656" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8341" Y="0.9233" />
            <PreSize X="0.0570" Y="0.0453" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_Position" CanEdit="False" ActionTag="1415871073" Tag="29" IconVisible="False" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="Player_pos_1" ActionTag="1939651770" Tag="23" IconVisible="True" LeftMargin="665.0001" RightMargin="668.9999" TopMargin="93.3563" BottomMargin="656.6437" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="665.0001" Y="656.6437" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4985" Y="0.8755" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Player_pos_2" ActionTag="427336992" Tag="28" IconVisible="True" LeftMargin="93.3314" RightMargin="1240.6686" TopMargin="341.2068" BottomMargin="408.7932" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="93.3314" Y="408.7932" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0700" Y="0.5451" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Player_pos_3" ActionTag="582909786" Tag="27" IconVisible="True" LeftMargin="167.4441" RightMargin="1166.5559" TopMargin="601.3851" BottomMargin="148.6149" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="167.4441" Y="148.6149" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1255" Y="0.1982" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Player_pos_4" ActionTag="-543741697" Tag="26" IconVisible="True" LeftMargin="1202.4709" RightMargin="131.5291" TopMargin="336.3562" BottomMargin="413.6438" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="1202.4709" Y="413.6438" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9014" Y="0.5515" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Player_pos_5" ActionTag="1730859311" Tag="25" IconVisible="True" LeftMargin="1200.1171" RightMargin="133.8829" TopMargin="214.3505" BottomMargin="535.6495" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="1200.1171" Y="535.6495" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8996" Y="0.7142" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Player_pos_6" ActionTag="-2100723968" Tag="24" IconVisible="True" LeftMargin="846.4611" RightMargin="487.5389" TopMargin="93.3563" BottomMargin="656.6437" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="846.4611" Y="656.6437" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6345" Y="0.8755" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Card_pos_1" ActionTag="-681430090" Tag="30" IconVisible="True" LeftMargin="665.0029" RightMargin="668.9971" TopMargin="251.3569" BottomMargin="498.6431" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="665.0029" Y="498.6431" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4985" Y="0.6649" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Card_pos_2" ActionTag="135017275" Tag="31" IconVisible="True" LeftMargin="232.3299" RightMargin="1101.6702" TopMargin="341.2100" BottomMargin="408.7900" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="232.3299" Y="408.7900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1742" Y="0.5451" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Card_pos_3" ActionTag="329798597" Tag="32" IconVisible="True" LeftMargin="667.0000" RightMargin="667.0000" TopMargin="636.1285" BottomMargin="113.8715" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="667.0000" Y="113.8715" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1518" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Card_pos_4" ActionTag="1209013186" Tag="33" IconVisible="True" LeftMargin="1058.1219" RightMargin="275.8781" TopMargin="336.3562" BottomMargin="413.6438" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="1058.1219" Y="413.6438" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7932" Y="0.5515" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Card_pos_5" ActionTag="63496562" Tag="34" IconVisible="True" LeftMargin="1058.1221" RightMargin="275.8779" TopMargin="214.3503" BottomMargin="535.6497" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="1058.1221" Y="535.6497" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7932" Y="0.7142" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Card_pos_6" ActionTag="928431866" Tag="35" IconVisible="True" LeftMargin="846.4618" RightMargin="487.5382" TopMargin="251.3580" BottomMargin="498.6420" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="846.4618" Y="498.6420" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6345" Y="0.6649" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="OpenCard_pos_1" ActionTag="-233499059" Tag="27" IconVisible="True" LeftMargin="665.0029" RightMargin="668.9971" TopMargin="321.3599" BottomMargin="428.6401" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="665.0029" Y="428.6401" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4985" Y="0.5715" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="OpenCard_pos_2" ActionTag="309275892" Tag="28" IconVisible="True" LeftMargin="232.3299" RightMargin="1101.6702" TopMargin="411.2100" BottomMargin="338.7900" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="232.3299" Y="338.7900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1742" Y="0.4517" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="OpenCard_pos_3" ActionTag="-834670739" Tag="29" IconVisible="True" LeftMargin="667.0000" RightMargin="667.0000" TopMargin="716.1300" BottomMargin="33.8700" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="667.0000" Y="33.8700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0452" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="OpenCard_pos_4" ActionTag="-183230822" Tag="30" IconVisible="True" LeftMargin="1058.1200" RightMargin="275.8800" TopMargin="406.3600" BottomMargin="343.6400" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="1058.1200" Y="343.6400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7932" Y="0.4582" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="OpenCard_pos_5" ActionTag="-64514500" Tag="31" IconVisible="True" LeftMargin="1058.1200" RightMargin="275.8800" TopMargin="284.3500" BottomMargin="465.6500" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="1058.1200" Y="465.6500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7932" Y="0.6209" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="OpenCard_pos_6" ActionTag="495654197" Tag="32" IconVisible="True" LeftMargin="846.4600" RightMargin="487.5400" TopMargin="321.3600" BottomMargin="428.6400" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="846.4600" Y="428.6400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6345" Y="0.5715" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="DianChiLayout" ActionTag="-167825895" Tag="344" IconVisible="False" LeftMargin="1156.5728" RightMargin="127.4272" TopMargin="46.5344" BottomMargin="683.4656" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="50.0000" Y="20.0000" />
            <Children>
              <AbstractNodeData Name="ValueLayout" ActionTag="1514272352" Tag="35" IconVisible="False" LeftMargin="1.0000" RightMargin="5.0000" TopMargin="1.0000" BottomMargin="-1.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="4" BottomEage="4" Scale9OriginX="6" Scale9OriginY="4" Scale9Width="7" Scale9Height="5" ctype="PanelObjectData">
                <Size X="44.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1.0000" Y="9.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0200" Y="0.4500" />
                <PreSize X="0.8800" Y="1.0000" />
                <FileData Type="Normal" Path="nn_dianliang.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpDianLiangBg" ActionTag="486345817" Tag="36" IconVisible="False" TopMargin="0.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="50.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="25.0000" Y="9.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4500" />
                <PreSize X="1.0000" Y="1.0500" />
                <FileData Type="Normal" Path="nn_dichi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1156.5728" Y="693.4656" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8670" Y="0.9246" />
            <PreSize X="0.0375" Y="0.0267" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>