<GameFile>
  <PropertyGroup Name="SetLayer" Type="Layer" ID="c1bba4d9-ab59-4c0f-aa37-d6c4173557b0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskPanel" ActionTag="2015309446" Tag="71" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_setLayer_bg" Tag="93" IconVisible="False" LeftMargin="424.5000" RightMargin="424.5000" TopMargin="224.5000" BottomMargin="224.5000" ctype="SpriteObjectData">
            <Size X="485.0000" Y="301.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.3636" Y="0.4013" />
            <FileData Type="Normal" Path="game/sp_setLayer_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_close" Tag="126" IconVisible="False" LeftMargin="835.5232" RightMargin="411.4768" TopMargin="195.5952" BottomMargin="467.4048" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="57" Scale9Height="65" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="87.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="879.0232" Y="510.9048" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6589" Y="0.6812" />
            <PreSize X="0.0652" Y="0.1160" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_exitSetLayer_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_exitSetLayer_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_exitSetLayer_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_effect_on" Tag="66" IconVisible="False" LeftMargin="675.5000" RightMargin="489.5000" TopMargin="272.4209" BottomMargin="388.5791" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="67" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="169.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="760.0000" Y="433.0791" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5697" Y="0.5774" />
            <PreSize X="0.1267" Y="0.1187" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_on_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_on_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_on_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_effect_off" Tag="68" IconVisible="False" LeftMargin="675.5000" RightMargin="489.5000" TopMargin="272.4209" BottomMargin="388.5791" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="67" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="169.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="760.0000" Y="433.0791" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5697" Y="0.5774" />
            <PreSize X="0.1267" Y="0.1187" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_off_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_off_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_off_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_music_on" Tag="67" IconVisible="False" LeftMargin="676.1900" RightMargin="488.8100" TopMargin="372.6510" BottomMargin="288.3490" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="67" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="169.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="760.6900" Y="332.8490" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5702" Y="0.4438" />
            <PreSize X="0.1267" Y="0.1187" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_on_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_on_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_on_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_music_off" Tag="69" IconVisible="False" LeftMargin="676.1900" RightMargin="488.8100" TopMargin="372.6510" BottomMargin="288.3490" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="67" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="169.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="760.6900" Y="332.8490" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5702" Y="0.4438" />
            <PreSize X="0.1267" Y="0.1187" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="bt_off_0.png" Plist="gameScene_oxnew.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_off_1.png" Plist="gameScene_oxnew.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_off_0.png" Plist="gameScene_oxnew.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_version" Tag="20" IconVisible="False" LeftMargin="732.9978" RightMargin="457.0022" TopMargin="456.5000" BottomMargin="272.5000" FontSize="18" LabelText="游戏版本：19.112" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="21.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="876.9978" Y="283.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.6574" Y="0.3773" />
            <PreSize X="0.1079" Y="0.0280" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>