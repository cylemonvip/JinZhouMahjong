<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="7e1f33ff-c024-4f0c-ba6c-680f612e7b5f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="background" CanEdit="False" ActionTag="165463859" Tag="71" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="440" RightEage="440" TopEage="247" BottomEage="247" Scale9OriginX="440" Scale9OriginY="247" Scale9Width="454" Scale9Height="256" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="game/background_sparrowHz_0.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="InfoLayout" ActionTag="-633510819" VisibleForFrame="False" Tag="340" IconVisible="False" RightMargin="1134.0000" BottomMargin="650.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Text_33" ActionTag="-1363361498" VisibleForFrame="False" Tag="341" IconVisible="False" LeftMargin="10.0000" RightMargin="57.0000" TopMargin="6.5000" BottomMargin="70.5000" FontSize="20" LabelText="房间号:123432" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="133.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.0000" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.8200" />
                <PreSize X="0.6650" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_33_0" ActionTag="545324910" Tag="342" IconVisible="False" LeftMargin="10.0000" RightMargin="127.0000" TopMargin="6.5000" BottomMargin="70.5000" FontSize="20" LabelText="剩16圈" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.0000" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.8200" />
                <PreSize X="0.3150" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="750.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="0.1499" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_chat" ActionTag="-1085812233" Tag="41" IconVisible="False" LeftMargin="1240.7542" RightMargin="6.2458" TopMargin="501.6626" BottomMargin="161.3374" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="57" Scale9Height="65" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="87.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1284.2542" Y="204.8374" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9627" Y="0.2731" />
            <PreSize X="0.0652" Y="0.1160" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="menu_btn_chat_press.png" Plist="game/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="menu_btn_chat_press.png" Plist="game/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="menu_btn_chat_normal.png" Plist="game/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_switch" ActionTag="2018638277" Tag="2" IconVisible="False" LeftMargin="1249.0000" RightMargin="3.0000" TopMargin="2.8353" BottomMargin="665.1647" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="52" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="82.0000" Y="82.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1290.0000" Y="706.1647" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9670" Y="0.9416" />
            <PreSize X="0.0615" Y="0.1093" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="menu_btn_setting_press.png" Plist="game/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="menu_btn_setting_press.png" Plist="game/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="menu_btn_setting_normal.png" Plist="game/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="StartBtnLayout" ActionTag="-1048837035" Tag="128" IconVisible="False" LeftMargin="317.0000" RightMargin="317.0000" TopMargin="197.0000" BottomMargin="453.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="700.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="BtnStart" ActionTag="90896694" Tag="3" IconVisible="False" LeftMargin="265.5000" RightMargin="265.5000" TopMargin="14.5000" BottomMargin="4.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="139" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="169.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="350.0000" Y="45.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4500" />
                <PreSize X="0.2414" Y="0.8100" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_end_btn_start_game_press.png" Plist="game/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="game_end_btn_start_game_press.png" Plist="game/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_end_btn_start_game.png" Plist="game/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnNoPiao" ActionTag="1342435522" Tag="129" IconVisible="False" LeftMargin="0.2187" RightMargin="539.7813" TopMargin="13.0000" BottomMargin="13.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="130" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.2187" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1146" Y="0.5000" />
                <PreSize X="0.2286" Y="0.7400" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game/piao/btn_bp_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game/piao/btn_bp_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game/piao/btn_bp_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnPiao5" ActionTag="-791762379" Tag="131" IconVisible="False" LeftMargin="181.2838" RightMargin="358.7162" TopMargin="12.9999" BottomMargin="13.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="130" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="261.2838" Y="50.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3733" Y="0.5000" />
                <PreSize X="0.2286" Y="0.7400" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game/piao/btn_p5_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game/piao/btn_p5_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game/piao/btn_p5_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnPiao10" ActionTag="672762515" Tag="133" IconVisible="False" LeftMargin="361.3489" RightMargin="178.6511" TopMargin="12.9999" BottomMargin="13.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="130" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="441.3489" Y="50.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6305" Y="0.5000" />
                <PreSize X="0.2286" Y="0.7400" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game/piao/btn_p10_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game/piao/btn_p10_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game/piao/btn_p10_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnPiao15" ActionTag="-1290357529" Tag="135" IconVisible="False" LeftMargin="541.4140" RightMargin="-1.4140" TopMargin="12.9999" BottomMargin="13.0001" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="130" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="621.4140" Y="50.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8877" Y="0.5000" />
                <PreSize X="0.2286" Y="0.7400" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game/piao/btn_p15_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="game/piao/btn_p15_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="game/piao/btn_p15_normal.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="503.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6707" />
            <PreSize X="0.5247" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_voice" ActionTag="147989285" Tag="5" IconVisible="False" LeftMargin="1247.2540" RightMargin="12.7460" TopMargin="423.9993" BottomMargin="252.0007" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="44" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="74.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1284.2540" Y="289.0007" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9627" Y="0.3853" />
            <PreSize X="0.0555" Y="0.0987" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="btn_sound_record_pressed.png" Plist="game/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="btn_sound_record_pressed.png" Plist="game/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="btn_sound_record_normal.png" Plist="game/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_clock" ActionTag="-4052651" Tag="9" IconVisible="False" LeftMargin="597.5000" RightMargin="597.5000" TopMargin="315.5000" BottomMargin="295.5000" ctype="SpriteObjectData">
            <Size X="139.0000" Y="139.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="365.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4867" />
            <PreSize X="0.1042" Y="0.1853" />
            <FileData Type="Normal" Path="direction/direction4_01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="AsLab_time" ActionTag="1664108806" Tag="1" IconVisible="False" LeftMargin="658.4998" RightMargin="654.5002" TopMargin="370.6161" BottomMargin="348.3839" CharWidth="21" CharHeight="31" LabelText="0" StartChar="0" ctype="TextAtlasObjectData">
            <Size X="21.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="668.9998" Y="363.8839" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5015" Y="0.4852" />
            <PreSize X="0.0157" Y="0.0413" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="game/num_clock.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_listenBg" ActionTag="337011487" VisibleForFrame="False" Tag="10" IconVisible="False" LeftMargin="41.5389" RightMargin="1246.4611" TopMargin="701.9748" BottomMargin="2.0252" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint />
            <Position X="41.5389" Y="2.0252" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0311" Y="0.0027" />
            <PreSize X="0.0345" Y="0.0613" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="1072808184" Tag="11" IconVisible="True" LeftMargin="338.2484" RightMargin="995.7516" TopMargin="95.1943" BottomMargin="654.8057" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="338.2484" Y="654.8057" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2536" Y="0.8731" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="-651639449" Tag="12" IconVisible="True" LeftMargin="85.4838" RightMargin="1248.5162" TopMargin="324.2934" BottomMargin="425.7066" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="85.4838" Y="425.7066" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0641" Y="0.5676" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3" ActionTag="-66220346" Tag="13" IconVisible="True" LeftMargin="85.4838" RightMargin="1248.5162" TopMargin="567.1043" BottomMargin="182.8957" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="85.4838" Y="182.8957" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0641" Y="0.2439" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_4" ActionTag="-783811294" Tag="14" IconVisible="True" LeftMargin="1249.0000" RightMargin="85.0000" TopMargin="324.2934" BottomMargin="425.7066" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1249.0000" Y="425.7066" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9363" Y="0.5676" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/Player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_sparrowPlate" CanEdit="False" ActionTag="1285861256" Tag="19" IconVisible="False" LeftMargin="294.0666" RightMargin="916.9334" TopMargin="177.5784" BottomMargin="424.4216" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="123.0000" Y="148.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="355.5666" Y="498.4216" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2665" Y="0.6646" />
            <PreSize X="0.0922" Y="0.1973" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_25" ActionTag="1865648155" Tag="344" IconVisible="False" LeftMargin="80.0000" RightMargin="1194.0000" TopMargin="5.0000" BottomMargin="715.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="60.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_33" ActionTag="-738749475" Tag="840" IconVisible="False" LeftMargin="3.0000" RightMargin="37.0000" TopMargin="-1.0000" BottomMargin="-1.0000" ctype="SpriteObjectData">
                <Size X="20.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="13.0000" Y="15.0000" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2167" Y="0.5000" />
                <PreSize X="0.3333" Y="1.0667" />
                <FileData Type="Normal" Path="game/sheyuIcon.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="AtlasLabel_5" ActionTag="1527139439" Tag="839" IconVisible="False" LeftMargin="28.0000" RightMargin="3.0000" TopMargin="3.0000" BottomMargin="1.0000" FontSize="20" LabelText="112" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.0000" Y="14.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="235" G="236" B="21" />
                <PrePosition X="0.4667" Y="0.4667" />
                <PreSize X="0.4833" Y="0.8667" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="80.0000" Y="730.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0600" Y="0.9733" />
            <PreSize X="0.0450" Y="0.0400" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RuleTxt" ActionTag="-763692192" Tag="246" IconVisible="False" LeftMargin="667.0000" RightMargin="667.0000" TopMargin="303.0000" BottomMargin="447.0000" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="447.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5960" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="HuaCardZoom" CanEdit="False" ActionTag="1856954149" Tag="54678" IconVisible="False" LeftMargin="0.0002" RightMargin="-0.0002" TopMargin="-0.0001" BottomMargin="0.0001" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="SpUser_1" ActionTag="1178757825" Tag="140" IconVisible="False" LeftMargin="529.2300" RightMargin="760.7700" TopMargin="99.2990" BottomMargin="583.7010" ctype="SpriteObjectData">
                <Size X="44.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="SpHuaFont" ActionTag="1812789937" Tag="202" IconVisible="False" LeftMargin="-18.3857" RightMargin="-19.6143" TopMargin="-35.4993" BottomMargin="-16.5007" ctype="SpriteObjectData">
                    <Size X="82.0000" Y="119.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="22.6143" Y="42.9993" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5140" Y="0.6418" />
                    <PreSize X="1.8636" Y="1.7761" />
                    <FileData Type="Normal" Path="game/hua_card/hua_card_65.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Txt" ActionTag="-783119370" Tag="141" IconVisible="False" LeftMargin="53.0709" RightMargin="-60.0709" TopMargin="7.5463" BottomMargin="10.4537" FontSize="40" LabelText="X0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="51.0000" Y="49.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="53.0709" Y="34.9537" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.2062" Y="0.5217" />
                    <PreSize X="1.1591" Y="0.7313" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="26" G="26" B="26" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="551.2300" Y="617.2010" />
                <Scale ScaleX="0.3000" ScaleY="0.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4132" Y="0.8229" />
                <PreSize X="0.0330" Y="0.0893" />
                <FileData Type="Normal" Path="game/font_small/card_down.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpUser_2" ActionTag="-955194596" Tag="142" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" LeftMargin="214.3087" RightMargin="1075.6913" TopMargin="359.2342" BottomMargin="323.7658" ctype="SpriteObjectData">
                <Size X="44.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="SpHuaFont" ActionTag="-591229212" Tag="203" IconVisible="False" LeftMargin="-20.1002" RightMargin="-17.8998" TopMargin="-34.8964" BottomMargin="-17.1036" ctype="SpriteObjectData">
                    <Size X="82.0000" Y="119.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="20.8998" Y="42.3964" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4750" Y="0.6328" />
                    <PreSize X="1.8636" Y="1.7761" />
                    <FileData Type="Normal" Path="game/hua_card/hua_card_65.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Txt" ActionTag="332382091" Tag="143" IconVisible="False" LeftMargin="50.9719" RightMargin="-57.9719" TopMargin="5.1334" BottomMargin="12.8666" FontSize="40" LabelText="X0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="51.0000" Y="49.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="50.9719" Y="37.3666" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1585" Y="0.5577" />
                    <PreSize X="1.1591" Y="0.7313" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="26" G="26" B="26" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5591" ScaleY="0.5000" />
                <Position X="238.9091" Y="357.2658" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1791" Y="0.4764" />
                <PreSize X="0.0330" Y="0.0893" />
                <FileData Type="Normal" Path="game/font_small/card_down.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpUser_3" ActionTag="1893325071" Tag="144" IconVisible="False" LeftMargin="250.6400" RightMargin="1039.3600" TopMargin="535.5000" BottomMargin="147.5000" ctype="SpriteObjectData">
                <Size X="44.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="SpHuaFont" ActionTag="-60162354" Tag="200" IconVisible="False" LeftMargin="-17.4999" RightMargin="-20.5001" TopMargin="-33.9043" BottomMargin="-18.0957" ctype="SpriteObjectData">
                    <Size X="82.0000" Y="119.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="23.5001" Y="41.4043" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5341" Y="0.6180" />
                    <PreSize X="1.8636" Y="1.7761" />
                    <FileData Type="Normal" Path="game/hua_card/hua_card_65.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Txt" ActionTag="-994607373" Tag="145" IconVisible="False" LeftMargin="49.9095" RightMargin="-56.9095" TopMargin="7.5875" BottomMargin="10.4125" FontSize="40" LabelText="X0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="51.0000" Y="49.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="49.9095" Y="34.9125" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1343" Y="0.5211" />
                    <PreSize X="1.1591" Y="0.7313" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="26" G="26" B="26" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="272.6400" Y="181.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2044" Y="0.2413" />
                <PreSize X="0.0330" Y="0.0893" />
                <FileData Type="Normal" Path="game/font_small/card_down.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpUser_4" ActionTag="796129012" Tag="146" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="1051.0211" RightMargin="238.9789" TopMargin="437.5000" BottomMargin="245.5000" ctype="SpriteObjectData">
                <Size X="44.0000" Y="67.0000" />
                <Children>
                  <AbstractNodeData Name="SpHuaFont" ActionTag="148702706" Tag="201" IconVisible="False" LeftMargin="-18.5444" RightMargin="-19.4556" TopMargin="-33.6172" BottomMargin="-18.3828" ctype="SpriteObjectData">
                    <Size X="82.0000" Y="119.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="22.4556" Y="41.1172" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5104" Y="0.6137" />
                    <PreSize X="1.8636" Y="1.7761" />
                    <FileData Type="Normal" Path="game/hua_card/hua_card_65.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Txt" ActionTag="283985178" Tag="147" IconVisible="False" LeftMargin="51.8778" RightMargin="-58.8778" TopMargin="5.8260" BottomMargin="12.1740" FontSize="40" LabelText="X0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="51.0000" Y="49.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="51.8778" Y="36.6740" />
                    <Scale ScaleX="2.0000" ScaleY="2.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1790" Y="0.5474" />
                    <PreSize X="1.1591" Y="0.7313" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="26" G="26" B="26" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1073.0211" Y="279.0000" />
                <Scale ScaleX="0.3000" ScaleY="0.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8044" Y="0.3720" />
                <PreSize X="0.0330" Y="0.0893" />
                <FileData Type="Normal" Path="game/font_small/card_down.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0002" Y="375.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimeText" ActionTag="1793154896" Tag="111" IconVisible="False" LeftMargin="1116.9707" RightMargin="143.0293" TopMargin="0.8257" BottomMargin="712.1743" FontSize="30" LabelText="18:36" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="74.0000" Y="37.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1190.9707" Y="730.6743" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8928" Y="0.9742" />
            <PreSize X="0.0555" Y="0.0493" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="DianChiLayout" ActionTag="1849714734" Tag="146" IconVisible="False" LeftMargin="1196.0000" RightMargin="88.0000" TopMargin="10.3456" BottomMargin="719.6544" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="50.0000" Y="20.0000" />
            <Children>
              <AbstractNodeData Name="ValueLayout" ActionTag="-89172311" Tag="145" IconVisible="False" LeftMargin="1.0000" RightMargin="5.0000" TopMargin="1.0000" BottomMargin="1.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="6" RightEage="6" TopEage="4" BottomEage="4" Scale9OriginX="6" Scale9OriginY="4" Scale9Width="7" Scale9Height="5" ctype="PanelObjectData">
                <Size X="44.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1.0000" Y="10.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0200" Y="0.5000" />
                <PreSize X="0.8800" Y="0.9000" />
                <FileData Type="Normal" Path="pics/pj_dianliang.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpDianLiangBg" ActionTag="-735213391" Tag="144" IconVisible="False" TopMargin="-0.5000" BottomMargin="-0.5000" ctype="SpriteObjectData">
                <Size X="50.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="25.0000" Y="10.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0500" />
                <FileData Type="Normal" Path="pics/pj_dichi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1196.0000" Y="729.6544" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8966" Y="0.9729" />
            <PreSize X="0.0375" Y="0.0267" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>