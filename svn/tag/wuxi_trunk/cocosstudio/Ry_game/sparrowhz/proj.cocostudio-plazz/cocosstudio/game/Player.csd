<GameFile>
  <PropertyGroup Name="Player" Type="Layer" ID="5f0fb4aa-b0a2-41c0-89d9-a0626f565963" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="32" ctype="GameLayerObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="sp_headCover" ActionTag="-1631372913" Tag="2" IconVisible="False" LeftMargin="-48.0000" RightMargin="-48.0000" TopMargin="-59.3640" BottomMargin="-36.6360" ctype="SpriteObjectData">
            <Size X="96.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="11.3640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_nickname" ActionTag="-1714128219" Tag="3" IconVisible="False" LeftMargin="-53.1388" RightMargin="-49.8612" TopMargin="36.5133" BottomMargin="-62.5133" FontSize="20" LabelText="身后有尾巴" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="103.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.6388" Y="-49.5133" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Aslab_score" ActionTag="967830280" Tag="4" IconVisible="False" LeftMargin="-9.7333" RightMargin="-6.2667" TopMargin="60.9313" BottomMargin="-86.9313" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="16.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.7333" Y="-73.9313" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="242" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_ready" ActionTag="-729668461" Tag="5" IconVisible="False" LeftMargin="72.7932" RightMargin="-169.7932" TopMargin="-27.8023" BottomMargin="-23.1977" ctype="SpriteObjectData">
            <Size X="97.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="121.2932" Y="2.3023" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="pics/sp_ready.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_trustee" ActionTag="1234088309" Tag="6" IconVisible="False" HorizontalEdge="RightEdge" LeftMargin="-82.5540" RightMargin="49.5540" TopMargin="23.5000" BottomMargin="-58.5000" ctype="SpriteObjectData">
            <Size X="33.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-66.0540" Y="-41.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="pics/sp_trustee.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_banker" ActionTag="2048453043" Tag="7" IconVisible="False" LeftMargin="17.0002" RightMargin="-79.0002" TopMargin="-78.7697" BottomMargin="21.7697" ctype="SpriteObjectData">
            <Size X="62.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="48.0002" Y="50.2697" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="pics/r_zhuang.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_host" ActionTag="-1344979282" Tag="8" IconVisible="False" LeftMargin="-93.1984" RightMargin="53.1984" TopMargin="-21.0215" BottomMargin="-54.9785" ctype="SpriteObjectData">
            <Size X="40.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-73.1984" Y="-16.9785" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="pics/sp_roomHost.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ipTxt" ActionTag="103235362" Tag="209" IconVisible="False" TopMargin="-8.0000" BottomMargin="8.0000" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="8.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="spPiao" ActionTag="1539522282" VisibleForFrame="False" Tag="144" IconVisible="False" LeftMargin="-54.5000" RightMargin="-54.5000" TopMargin="-114.0001" BottomMargin="62.0001" ctype="SpriteObjectData">
            <Size X="109.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="0.0000" Y="62.0001" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/piao/p10.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>