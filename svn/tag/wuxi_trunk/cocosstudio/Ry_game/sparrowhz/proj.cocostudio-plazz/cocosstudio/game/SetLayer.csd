<GameFile>
  <PropertyGroup Name="SetLayer" Type="Scene" ID="c88d9739-a13d-49cb-b8dd-cc80af33e71c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="60" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="sp_setLayer_bg" ActionTag="-1295503429" Tag="93" IconVisible="False" LeftMargin="402.9987" RightMargin="371.0013" TopMargin="155.4482" BottomMargin="114.5518" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" TopEage="121" BottomEage="113" Scale9OriginY="121" Scale9Width="530" Scale9Height="111" ctype="PanelObjectData">
            <Size X="560.0000" Y="480.0000" />
            <AnchorPoint />
            <Position X="402.9987" Y="114.5518" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3021" Y="0.1527" />
            <PreSize X="0.4198" Y="0.6400" />
            <FileData Type="PlistSubImage" Path="setting_image_bg.png" Plist="game/setting.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_close" ActionTag="-698393417" Tag="126" IconVisible="False" LeftMargin="909.6502" RightMargin="356.3498" TopMargin="152.7026" BottomMargin="529.2974" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="68.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="943.6502" Y="563.2974" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7074" Y="0.7511" />
            <PreSize X="0.0510" Y="0.0907" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="game/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="game/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="plaza_btn_close_normal.png" Plist="game/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="EffectSlider" ActionTag="-814203041" Tag="88" IconVisible="False" LeftMargin="520.5975" RightMargin="495.4025" TopMargin="279.0483" BottomMargin="452.9517" TouchEnable="True" PercentInfo="52" ctype="SliderObjectData">
            <Size X="318.0000" Y="18.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="679.5975" Y="461.9517" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5094" Y="0.6159" />
            <PreSize X="0.2384" Y="0.0240" />
            <BackGroundData Type="PlistSubImage" Path="setting_slide_bar_bg.png" Plist="game/setting.plist" />
            <ProgressBarData Type="PlistSubImage" Path="setting_slide_bar.png" Plist="game/setting.plist" />
            <BallNormalData Type="PlistSubImage" Path="setting_btn_slide_normal.png" Plist="game/setting.plist" />
            <BallPressedData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
            <BallDisabledData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="BgmSlider" ActionTag="-349379924" Tag="89" IconVisible="False" LeftMargin="520.5970" RightMargin="495.4030" TopMargin="357.0777" BottomMargin="374.9223" TouchEnable="True" PercentInfo="52" ctype="SliderObjectData">
            <Size X="318.0000" Y="18.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="679.5970" Y="383.9223" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5094" Y="0.5119" />
            <PreSize X="0.2384" Y="0.0240" />
            <BackGroundData Type="PlistSubImage" Path="setting_slide_bar_bg.png" Plist="game/setting.plist" />
            <ProgressBarData Type="PlistSubImage" Path="setting_slide_bar.png" Plist="game/setting.plist" />
            <BallNormalData Type="PlistSubImage" Path="setting_btn_slide_normal.png" Plist="game/setting.plist" />
            <BallPressedData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
            <BallDisabledData Type="PlistSubImage" Path="setting_btn_slide_press.png" Plist="game/setting.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Spfont" ActionTag="1387626291" Tag="90" IconVisible="False" LeftMargin="523.5370" RightMargin="705.4630" TopMargin="251.8536" BottomMargin="400.1464" ctype="SpriteObjectData">
            <Size X="105.0000" Y="98.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="576.0370" Y="449.1464" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4318" Y="0.5989" />
            <PreSize X="0.0787" Y="0.1307" />
            <FileData Type="PlistSubImage" Path="setting_image_front.png" Plist="game/setting.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnEffectMax" ActionTag="125717211" Tag="93" IconVisible="False" LeftMargin="869.2546" RightMargin="420.7454" TopMargin="266.3207" BottomMargin="440.6793" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="891.2546" Y="462.1793" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6681" Y="0.6162" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_effect_open_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_effect_open_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_effect_open_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnEffectMin" ActionTag="2089070453" Tag="94" IconVisible="False" LeftMargin="457.3057" RightMargin="832.6943" TopMargin="266.3207" BottomMargin="440.6793" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="479.3057" Y="462.1793" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3593" Y="0.6162" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_effect_close_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_effect_close_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_effect_close_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnBgmMax" ActionTag="172132052" Tag="95" IconVisible="False" LeftMargin="869.2546" RightMargin="420.7454" TopMargin="344.1275" BottomMargin="362.8725" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="891.2546" Y="384.3725" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6681" Y="0.5125" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_music_open_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_music_open_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_music_open_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnBgmMin" ActionTag="-536913857" Tag="96" IconVisible="False" LeftMargin="457.3057" RightMargin="832.6943" TopMargin="344.1275" BottomMargin="362.8725" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="14" Scale9Height="21" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="44.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="479.3057" Y="384.3725" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3593" Y="0.5125" />
            <PreSize X="0.0330" Y="0.0573" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_music_close_press.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_music_close_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_music_close_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnExit" ActionTag="1223485798" Tag="97" IconVisible="False" LeftMargin="728.4313" RightMargin="451.5687" TopMargin="530.3745" BottomMargin="157.6255" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="154.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="805.4313" Y="188.6255" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6038" Y="0.2515" />
            <PreSize X="0.1154" Y="0.0827" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_exit_unused.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_exit_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_exit_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnDismiss" ActionTag="-312824909" Tag="98" IconVisible="False" LeftMargin="510.4515" RightMargin="669.5485" TopMargin="531.6981" BottomMargin="156.3019" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="124" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="154.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="587.4515" Y="187.3019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4404" Y="0.2497" />
            <PreSize X="0.1154" Y="0.0827" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="setting_btn_dismiss_unused.png" Plist="game/setting.plist" />
            <PressedFileData Type="PlistSubImage" Path="setting_btn_dismiss_press.png" Plist="game/setting.plist" />
            <NormalFileData Type="PlistSubImage" Path="setting_btn_dismiss_normal.png" Plist="game/setting.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_1" ActionTag="211869730" Tag="86" IconVisible="False" LeftMargin="470.6443" RightMargin="756.3557" TopMargin="423.9875" BottomMargin="303.0125" ctype="SpriteObjectData">
            <Size X="107.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="524.1443" Y="314.5125" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3929" Y="0.4194" />
            <PreSize X="0.0802" Y="0.0307" />
            <FileData Type="Normal" Path="game/pzys1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pzys_2" ActionTag="-509137317" Tag="87" IconVisible="False" LeftMargin="468.6443" RightMargin="756.3557" TopMargin="476.9612" BottomMargin="250.0388" ctype="SpriteObjectData">
            <Size X="109.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="523.1443" Y="261.5388" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3922" Y="0.3487" />
            <PreSize X="0.0817" Y="0.0307" />
            <FileData Type="Normal" Path="game/pzys2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_lan" ActionTag="1785496682" Tag="79" IconVisible="False" LeftMargin="643.9860" RightMargin="640.0140" TopMargin="418.5000" BottomMargin="302.5000" FontSize="25" LabelText="蓝色" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="668.9860" Y="317.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="21" G="119" B="67" />
            <PrePosition X="0.5015" Y="0.4227" />
            <PreSize X="0.0375" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_lv" ActionTag="1611736563" Tag="80" IconVisible="False" LeftMargin="741.1171" RightMargin="542.8829" TopMargin="418.5000" BottomMargin="302.5000" FontSize="25" LabelText="绿色" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="766.1171" Y="317.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="21" G="119" B="67" />
            <PrePosition X="0.5743" Y="0.4227" />
            <PreSize X="0.0375" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpzys_0" ActionTag="-1886142782" Tag="94" IconVisible="False" LeftMargin="606.2761" RightMargin="692.7239" TopMargin="416.9876" BottomMargin="303.0124" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7761" Y="318.0124" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.4240" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpzys_1" ActionTag="238463523" Tag="95" IconVisible="False" LeftMargin="703.2461" RightMargin="595.7539" TopMargin="416.9877" BottomMargin="303.0123" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="720.7461" Y="318.0123" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5403" Y="0.4240" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpmys_0" ActionTag="325230100" Tag="96" IconVisible="False" LeftMargin="606.2761" RightMargin="692.7239" TopMargin="469.9612" BottomMargin="250.0388" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7761" Y="265.0388" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.3534" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cbpmys_1" ActionTag="150233852" Tag="97" IconVisible="False" LeftMargin="703.2461" RightMargin="595.7539" TopMargin="469.9612" BottomMargin="250.0388" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="35.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="720.7461" Y="265.0388" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5403" Y="0.3534" />
            <PreSize X="0.0262" Y="0.0400" />
            <NormalBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="game/login_image_point.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="game/login_image_point_front.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_lan_0" ActionTag="-1250605878" Tag="264" IconVisible="False" LeftMargin="643.9860" RightMargin="640.0140" TopMargin="471.9531" BottomMargin="249.0469" FontSize="25" LabelText="经典" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="668.9860" Y="263.5469" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="21" G="119" B="67" />
            <PrePosition X="0.5015" Y="0.3514" />
            <PreSize X="0.0375" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_lan_0_0" ActionTag="1706796056" Tag="265" IconVisible="False" LeftMargin="741.1171" RightMargin="542.8829" TopMargin="471.9531" BottomMargin="249.0469" FontSize="25" LabelText="传统" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="766.1171" Y="263.5469" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="21" G="119" B="67" />
            <PrePosition X="0.5743" Y="0.3514" />
            <PreSize X="0.0375" Y="0.0387" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>