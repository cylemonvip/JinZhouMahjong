<GameFile>
  <PropertyGroup Name="GameResultLayer" Type="Scene" ID="5383186a-ba16-4f1b-aa57-a64d26f3f633" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="3" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-1414793136" Tag="215" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="result_background_1" ActionTag="1185920085" Tag="108" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="gameResult/result_background_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="result_background" ActionTag="-1691730088" Tag="109" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="gameResult/result_background.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="win_background" ActionTag="-1509099719" Tag="110" IconVisible="False" LeftMargin="79.4235" RightMargin="75.5764" TopMargin="148.2400" BottomMargin="491.7600" ctype="SpriteObjectData">
            <Size X="1179.0000" Y="110.0000" />
            <Children>
              <AbstractNodeData Name="sp_hupaitype" ActionTag="1369981340" Tag="688" IconVisible="False" LeftMargin="1068.8121" RightMargin="7.1879" TopMargin="23.6286" BottomMargin="17.3714" ctype="SpriteObjectData">
                <Size X="103.0000" Y="69.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1120.3121" Y="51.8714" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9502" Y="0.4716" />
                <PreSize X="0.0874" Y="0.6273" />
                <FileData Type="Normal" Path="pics/r_fangpao.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="668.9235" Y="546.7600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5014" Y="0.7290" />
            <PreSize X="0.8838" Y="0.1467" />
            <FileData Type="Normal" Path="gameResult/win_background.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="-1952599429" Tag="1" IconVisible="True" LeftMargin="134.7382" RightMargin="1199.2618" TopMargin="192.0115" BottomMargin="557.9885" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="134.7382" Y="557.9885" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1010" Y="0.7440" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gameResult/nodeUser.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="1294721634" Tag="2" IconVisible="True" LeftMargin="134.7382" RightMargin="1199.2618" TopMargin="310.5119" BottomMargin="439.4881" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="134.7382" Y="439.4881" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1010" Y="0.5860" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gameResult/nodeUser.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3" ActionTag="885959186" Tag="3" IconVisible="True" LeftMargin="134.7382" RightMargin="1199.2618" TopMargin="429.0123" BottomMargin="320.9877" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="134.7382" Y="320.9877" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1010" Y="0.4280" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gameResult/nodeUser.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_4" ActionTag="-57070223" Tag="4" IconVisible="True" LeftMargin="134.7382" RightMargin="1199.2618" TopMargin="547.5127" BottomMargin="202.4873" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="134.7382" Y="202.4873" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1010" Y="0.2700" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gameResult/nodeUser.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_banker" ActionTag="-1007326614" Tag="6" IconVisible="False" LeftMargin="69.0000" RightMargin="1203.0000" TopMargin="131.5000" BottomMargin="561.5000" ctype="SpriteObjectData">
            <Size X="62.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="100.0000" Y="590.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0750" Y="0.7867" />
            <PreSize X="0.0465" Y="0.0760" />
            <FileData Type="Normal" Path="pics/r_zhuang.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_recodeShow" ActionTag="-1909096868" Tag="8" IconVisible="False" LeftMargin="745.5000" RightMargin="291.5000" TopMargin="610.1828" BottomMargin="60.8172" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="267" Scale9Height="57" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="297.0000" Y="79.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="894.0000" Y="100.3172" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6702" Y="0.1338" />
            <PreSize X="0.2226" Y="0.1053" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="gameResult/btn_show_pressed.png" Plist="" />
            <PressedFileData Type="Normal" Path="gameResult/btn_show_pressed.png" Plist="" />
            <NormalFileData Type="Normal" Path="gameResult/btn_show_normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_continue" ActionTag="680418892" Tag="9" IconVisible="False" LeftMargin="291.5000" RightMargin="745.5000" TopMargin="610.1828" BottomMargin="60.8172" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="267" Scale9Height="57" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="297.0000" Y="79.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="440.0000" Y="100.3172" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3298" Y="0.1338" />
            <PreSize X="0.2226" Y="0.1053" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="gameResult/goon2.png" Plist="" />
            <PressedFileData Type="Normal" Path="gameResult/goon2.png" Plist="" />
            <NormalFileData Type="Normal" Path="gameResult/goon1.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_ResultDJS" ActionTag="659169747" Tag="132" IconVisible="False" LeftMargin="1075.6947" RightMargin="182.3053" TopMargin="93.2166" BottomMargin="601.7834" CharWidth="38" CharHeight="55" LabelText="20" StartChar="0" ctype="TextAtlasObjectData">
            <Size X="76.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1113.6947" Y="629.2834" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8349" Y="0.8390" />
            <PreSize X="0.0570" Y="0.0733" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="gameResult/font_djsatlas.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TxtTime" ActionTag="935667894" Tag="133" IconVisible="False" LeftMargin="1054.6919" RightMargin="85.3081" TopMargin="654.9164" BottomMargin="69.0835" FontSize="20" LabelText="日期: 2018.01.01 20:45" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="194.0000" Y="26.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1248.6919" Y="82.0835" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9361" Y="0.1094" />
            <PreSize X="0.1454" Y="0.0347" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="TxtVersion" ActionTag="-1892309569" Tag="134" IconVisible="False" LeftMargin="1166.3655" RightMargin="86.6345" TopMargin="629.8250" BottomMargin="94.1750" FontSize="20" LabelText="版本: 4.5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="81.0000" Y="26.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="1247.3655" Y="107.1750" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9351" Y="0.1429" />
            <PreSize X="0.0607" Y="0.0347" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>