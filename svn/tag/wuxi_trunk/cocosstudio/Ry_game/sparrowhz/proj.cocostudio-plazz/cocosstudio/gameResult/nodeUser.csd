<GameFile>
  <PropertyGroup Name="nodeUser" Type="Node" ID="75eb8fb9-730f-4fa8-a115-7ee9072a6227" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="131" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="sp_headCover" ActionTag="886046402" Tag="1" IconVisible="False" LeftMargin="-48.0000" RightMargin="-48.0000" TopMargin="-48.0000" BottomMargin="-48.0000" ctype="SpriteObjectData">
            <Size X="96.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/headbg_kk.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_nickname" ActionTag="374684462" Tag="2" IconVisible="False" LeftMargin="-25.5813" RightMargin="-25.4187" TopMargin="42.0000" BottomMargin="-68.0000" FontSize="20" LabelText="游客1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="51.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0813" Y="-55.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="216" G="115" B="16" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_zongfen" ActionTag="-421254349" Tag="99" IconVisible="False" LeftMargin="946.3953" RightMargin="-1011.3953" TopMargin="-39.7022" BottomMargin="1.7022" ctype="SpriteObjectData">
            <Size X="65.0000" Y="38.0000" />
            <AnchorPoint ScaleX="0.4705" ScaleY="0.4863" />
            <Position X="976.9778" Y="20.1816" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="pics/r_zongfen.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_zongfen" ActionTag="403942270" Tag="83" IconVisible="False" LeftMargin="966.9407" RightMargin="-993.9407" TopMargin="6.5818" BottomMargin="-42.5818" CharWidth="27" CharHeight="36" LabelText="0" StartChar="0" ctype="TextAtlasObjectData">
            <Size X="27.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="980.4407" Y="-24.5818" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="gameResult/font_atlas.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="SpPiao" ActionTag="1092403298" Tag="212" IconVisible="False" LeftMargin="844.8837" RightMargin="-953.8837" TopMargin="-15.8205" BottomMargin="-36.1795" ctype="SpriteObjectData">
            <Size X="109.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="899.3837" Y="-10.1795" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game/piao/p10.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="SpHua" ActionTag="-1942775480" Tag="82" IconVisible="False" LeftMargin="36.0717" RightMargin="-236.0717" TopMargin="-28.7070" BottomMargin="8.7070" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="20.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-1913224272" Tag="710" IconVisible="False" LeftMargin="27.0900" RightMargin="33.9100" TopMargin="-10.5001" BottomMargin="7.5001" FontSize="20" LabelText="自摸 x2 花牌 x3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="139.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="27.0900" Y="19.0001" />
                <Scale ScaleX="1.0000" ScaleY="0.9426" />
                <CColor A="255" R="121" G="68" B="44" />
                <PrePosition X="0.1355" Y="0.9500" />
                <PreSize X="0.6950" Y="1.1500" />
                <FontResource Type="Default" Path="" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="36.0717" Y="18.7070" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>