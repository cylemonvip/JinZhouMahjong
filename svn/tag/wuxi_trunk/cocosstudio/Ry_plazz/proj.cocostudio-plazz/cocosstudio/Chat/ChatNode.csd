<GameFile>
  <PropertyGroup Name="ChatNode" Type="Node" ID="6fbf5e0f-014c-4870-87f7-414c62639ef1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="10" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="TouchShild" ActionTag="-675846124" Tag="42" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="-631331141" Tag="11" IconVisible="False" LeftMargin="-309.0000" RightMargin="-309.0000" TopMargin="-168.0000" BottomMargin="-168.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="149" RightEage="149" TopEage="150" BottomEage="150" Scale9OriginX="149" Scale9OriginY="150" Scale9Width="320" Scale9Height="36" ctype="PanelObjectData">
            <Size X="618.0000" Y="336.0000" />
            <Children>
              <AbstractNodeData Name="TabEmoj" ActionTag="-1273486902" Tag="13" IconVisible="False" LeftMargin="55.7412" RightMargin="420.2588" TopMargin="-37.0000" BottomMargin="325.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="112" Scale9Height="26" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="142.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="126.7412" Y="349.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2051" Y="1.0387" />
                <PreSize X="0.2298" Y="0.1429" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq_press.png" Plist="Chat/chat.plist" />
                <PressedFileData Type="PlistSubImage" Path="chat_btn_bq_press.png" Plist="Chat/chat.plist" />
                <NormalFileData Type="PlistSubImage" Path="chat_btn_bq_normal.png" Plist="Chat/chat.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TabMsg" ActionTag="1271261845" Tag="14" IconVisible="False" LeftMargin="195.7044" RightMargin="280.2956" TopMargin="-37.0000" BottomMargin="325.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="112" Scale9Height="26" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="142.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="266.7044" Y="349.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4316" Y="1.0387" />
                <PreSize X="0.2298" Y="0.1429" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="chat_btn_cyy_press.png" Plist="Chat/chat.plist" />
                <PressedFileData Type="PlistSubImage" Path="chat_btn_cyy_press.png" Plist="Chat/chat.plist" />
                <NormalFileData Type="PlistSubImage" Path="chat_btn_cyy_normal.png" Plist="Chat/chat.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TabVoice" ActionTag="-1736880556" Tag="15" IconVisible="False" LeftMargin="330.4838" RightMargin="145.5162" TopMargin="-37.0000" BottomMargin="325.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="112" Scale9Height="26" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="142.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="401.4838" Y="349.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6497" Y="1.0387" />
                <PreSize X="0.2298" Y="0.1429" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="chat_btn_lt_press.png" Plist="Chat/chat.plist" />
                <PressedFileData Type="PlistSubImage" Path="chat_btn_lt_press.png" Plist="Chat/chat.plist" />
                <NormalFileData Type="PlistSubImage" Path="chat_btn_lt_normal.png" Plist="Chat/chat.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="EmojScrollView" ActionTag="915503119" Tag="39" IconVisible="False" LeftMargin="20.0000" RightMargin="21.0000" TopMargin="17.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="577.0000" Y="299.0000" />
                <Children>
                  <AbstractNodeData Name="Emoj_1" ActionTag="821019491" Tag="20" IconVisible="False" LeftMargin="-2.5278" RightMargin="481.5278" TopMargin="-0.3049" BottomMargin="197.3049" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.4722" Y="248.3049" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0805" Y="0.8305" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq1.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq1.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq1.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_2" ActionTag="111252220" Tag="21" IconVisible="False" LeftMargin="94.9022" RightMargin="384.0978" TopMargin="-0.3048" BottomMargin="197.3048" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="143.9022" Y="248.3048" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2494" Y="0.8305" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq2.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq2.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq2.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_3" ActionTag="267699175" Tag="22" IconVisible="False" LeftMargin="192.8045" RightMargin="286.1955" TopMargin="-0.3049" BottomMargin="197.3049" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="241.8045" Y="248.3049" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4191" Y="0.8305" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq3.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq3.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq3.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_4" ActionTag="381633460" Tag="23" IconVisible="False" LeftMargin="290.7080" RightMargin="188.2920" TopMargin="-0.3049" BottomMargin="197.3049" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="339.7080" Y="248.3049" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5887" Y="0.8305" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq4.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq4.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq4.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_5" ActionTag="1865410643" Tag="24" IconVisible="False" LeftMargin="387.6112" RightMargin="91.3888" TopMargin="-0.3054" BottomMargin="197.3054" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="436.6112" Y="248.3054" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7567" Y="0.8305" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq5.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq5.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq5.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_6" ActionTag="149570839" Tag="25" IconVisible="False" LeftMargin="484.5133" RightMargin="-5.5133" TopMargin="-0.3054" BottomMargin="197.3054" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="533.5133" Y="248.3054" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9246" Y="0.8305" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq6.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq6.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq6.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_7" ActionTag="1739128893" Tag="26" IconVisible="False" LeftMargin="-2.5278" RightMargin="481.5278" TopMargin="102.0027" BottomMargin="94.9973" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.4722" Y="145.9973" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0805" Y="0.4883" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq7.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq7.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq7.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_8" ActionTag="1730625694" Tag="27" IconVisible="False" LeftMargin="94.9022" RightMargin="384.0978" TopMargin="102.0020" BottomMargin="94.9980" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="143.9022" Y="145.9980" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2494" Y="0.4883" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq8.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq8.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq8.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_9" ActionTag="762035023" Tag="28" IconVisible="False" LeftMargin="192.8045" RightMargin="286.1955" TopMargin="102.0022" BottomMargin="94.9978" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="241.8045" Y="145.9978" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4191" Y="0.4883" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq9.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq9.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq9.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_10" ActionTag="-1223795716" Tag="29" IconVisible="False" LeftMargin="290.7080" RightMargin="188.2920" TopMargin="102.0022" BottomMargin="94.9978" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="339.7080" Y="145.9978" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5887" Y="0.4883" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq10.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq10.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq10.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_11" ActionTag="-672427753" Tag="30" IconVisible="False" LeftMargin="387.6112" RightMargin="91.3888" TopMargin="102.0019" BottomMargin="94.9981" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="436.6112" Y="145.9981" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7567" Y="0.4883" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq11.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq11.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq11.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_12" ActionTag="-1047692105" Tag="31" IconVisible="False" LeftMargin="484.5133" RightMargin="-5.5133" TopMargin="102.0025" BottomMargin="94.9975" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="533.5133" Y="145.9975" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9246" Y="0.4883" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq12.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq12.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq12.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_13" ActionTag="1108907844" Tag="32" IconVisible="False" LeftMargin="-2.5278" RightMargin="481.5278" TopMargin="203.5474" BottomMargin="-6.5474" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="46.4722" Y="44.4526" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0805" Y="0.1487" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq13.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq13.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq13.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_14" ActionTag="1881985249" Tag="33" IconVisible="False" LeftMargin="94.9022" RightMargin="384.0978" TopMargin="203.5472" BottomMargin="-6.5472" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="143.9022" Y="44.4528" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2494" Y="0.1487" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq14.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq14.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq14.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_15" ActionTag="1830306970" Tag="34" IconVisible="False" LeftMargin="192.8045" RightMargin="286.1955" TopMargin="203.5472" BottomMargin="-6.5472" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="241.8045" Y="44.4528" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4191" Y="0.1487" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq15.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq15.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq15.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_16" ActionTag="-447006102" Tag="35" IconVisible="False" LeftMargin="290.7080" RightMargin="188.2920" TopMargin="203.5472" BottomMargin="-6.5472" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="339.7080" Y="44.4528" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5887" Y="0.1487" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq16.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq16.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq16.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_17" ActionTag="351631022" Tag="36" IconVisible="False" LeftMargin="387.6112" RightMargin="91.3888" TopMargin="203.5472" BottomMargin="-6.5472" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="436.6112" Y="44.4528" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7567" Y="0.1487" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq17.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq17.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq17.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Emoj_18" ActionTag="-122251811" Tag="37" IconVisible="False" LeftMargin="484.5133" RightMargin="-5.5133" TopMargin="203.5474" BottomMargin="-6.5474" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="68" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="98.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="533.5133" Y="44.4526" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9246" Y="0.1487" />
                    <PreSize X="0.1698" Y="0.3411" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_bq18.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_bq18.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_bq18.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="20.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0324" Y="0.0595" />
                <PreSize X="0.9337" Y="0.8899" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="577" Height="299" />
              </AbstractNodeData>
              <AbstractNodeData Name="MsgLayout" Visible="False" ActionTag="1646909161" Tag="20" IconVisible="False" LeftMargin="20.0000" RightMargin="21.0000" TopMargin="17.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="577.0000" Y="299.0000" />
                <AnchorPoint />
                <Position X="20.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0324" Y="0.0595" />
                <PreSize X="0.9337" Y="0.8899" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="577" Height="430" />
              </AbstractNodeData>
              <AbstractNodeData Name="VoiceLayout" Visible="False" ActionTag="861430927" Tag="24" IconVisible="False" LeftMargin="20.0000" RightMargin="21.0000" TopMargin="17.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="577.0000" Y="299.0000" />
                <Children>
                  <AbstractNodeData Name="SpBottom" ActionTag="-709768196" Tag="12" IconVisible="False" LeftMargin="33.2597" RightMargin="23.7403" TopMargin="64.3108" BottomMargin="172.6892" ctype="SpriteObjectData">
                    <Size X="520.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="293.2597" Y="203.6892" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5082" Y="0.6812" />
                    <PreSize X="0.9012" Y="0.2074" />
                    <FileData Type="PlistSubImage" Path="chat_word_bg.png" Plist="Chat/chat.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="InputMsgField" ActionTag="2032323586" Tag="19" IconVisible="False" LeftMargin="39.1072" RightMargin="35.8928" TopMargin="70.4176" BottomMargin="178.5824" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="输入您想说的话..." MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="502.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="39.1072" Y="228.5824" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.0678" Y="0.7645" />
                    <PreSize X="0.8700" Y="0.1672" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="BtnSend" ActionTag="1108477908" Tag="17" IconVisible="False" LeftMargin="205.5000" RightMargin="206.5000" TopMargin="184.9831" BottomMargin="45.0169" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="135" Scale9Height="47" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="165.0000" Y="69.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="288.0000" Y="79.5169" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4991" Y="0.2659" />
                    <PreSize X="0.2860" Y="0.2308" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="PlistSubImage" Path="chat_btn_send_press.png" Plist="Chat/chat.plist" />
                    <PressedFileData Type="PlistSubImage" Path="chat_btn_send_press.png" Plist="Chat/chat.plist" />
                    <NormalFileData Type="PlistSubImage" Path="chat_btn_send_normal.png" Plist="Chat/chat.plist" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="20.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0324" Y="0.0595" />
                <PreSize X="0.9337" Y="0.8899" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.1700" ScaleY="1.1700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="chat_bg_only.png" Plist="Chat/chat.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="EmojItemLayout" ActionTag="875538908" Tag="15" IconVisible="False" LeftMargin="1139.6812" RightMargin="-1214.6812" TopMargin="78.9246" BottomMargin="-153.9246" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="75.0000" Y="75.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1177.1812" Y="-116.4246" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="MsgItemLayout" ActionTag="-302938984" Tag="18" IconVisible="False" LeftMargin="-262.4846" RightMargin="-241.5154" TopMargin="806.6512" BottomMargin="-852.6512" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="166" RightEage="166" TopEage="15" BottomEage="15" Scale9OriginX="166" Scale9OriginY="15" Scale9Width="172" Scale9Height="16" ctype="PanelObjectData">
            <Size X="504.0000" Y="46.0000" />
            <Children>
              <AbstractNodeData Name="Text" ActionTag="-1769272078" Tag="19" IconVisible="False" IsCustomSize="True" FontSize="20" LabelText="这是一句话，可能很长很长" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="504.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="252.0000" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="-10.4846" Y="-806.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="PlistSubImage" Path="chat_image_cyy_bg.png" Plist="Chat/chat.plist" />
            <SingleColor A="255" R="255" G="165" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>