<GameFile>
  <PropertyGroup Name="PlazzLayer" Type="Layer" ID="0f3c9af1-552a-4db5-9077-d05cf9f63dde" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="24" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="648245720" VisibleForFrame="False" Tag="56" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="plaza/hzmj/hzmjbj.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="top_bg" ActionTag="-2084056997" VisibleForFrame="False" Tag="4" IconVisible="False" LeftMargin="21.9993" RightMargin="22.0007" TopMargin="18.0005" BottomMargin="636.9995" ctype="SpriteObjectData">
            <Size X="1290.0000" Y="95.0000" />
            <Children>
              <AbstractNodeData Name="bt_person" ActionTag="765164914" Tag="93" IconVisible="False" LeftMargin="27.6903" RightMargin="1042.3097" TopMargin="11.6012" BottomMargin="8.3988" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="32" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="220.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="137.6903" Y="45.8988" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1067" Y="0.4831" />
                <PreSize X="0.1705" Y="0.7895" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <PressedFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <NormalFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_set" ActionTag="-170204292" Tag="5" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="1191.4363" RightMargin="22.5637" TopMargin="6.5000" BottomMargin="6.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="76.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1229.4363" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9531" Y="0.5000" />
                <PreSize X="0.0589" Y="0.8632" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_plaza_set_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_plaza_set_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_plaza_set_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_return" ActionTag="-1210570609" Tag="59" IconVisible="False" LeftMargin="20.1427" RightMargin="1193.8573" TopMargin="10.4343" BottomMargin="2.5657" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="64" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="76.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.1427" Y="43.5657" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0451" Y="0.4586" />
                <PreSize X="0.0589" Y="0.8632" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_roomlist_return.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_roomlist_return.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_roomlist_return.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_num_bg_2_1" ActionTag="-533084635" Tag="8" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="263.5000" RightMargin="733.5000" TopMargin="16.0000" BottomMargin="16.0000" ctype="SpriteObjectData">
                <Size X="293.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="410.0000" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3178" Y="0.5000" />
                <PreSize X="0.2271" Y="0.6632" />
                <FileData Type="PlistSubImage" Path="sp_num_bg.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_num_bg_2" ActionTag="-351723861" Tag="6" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="576.8281" RightMargin="420.1719" TopMargin="16.0000" BottomMargin="16.0000" ctype="SpriteObjectData">
                <Size X="293.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="723.3281" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5607" Y="0.5000" />
                <PreSize X="0.2271" Y="0.6632" />
                <FileData Type="PlistSubImage" Path="sp_num_bg.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_num_bg_2_0" ActionTag="1956230274" Tag="7" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="888.9747" RightMargin="108.0253" TopMargin="16.0000" BottomMargin="16.0000" ctype="SpriteObjectData">
                <Size X="293.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1035.4747" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8027" Y="0.5000" />
                <PreSize X="0.2271" Y="0.6632" />
                <FileData Type="PlistSubImage" Path="sp_num_bg.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_progress_bg_5" ActionTag="849012228" Tag="10" IconVisible="False" LeftMargin="113.5000" RightMargin="1023.5000" TopMargin="54.5000" BottomMargin="11.5000" ctype="SpriteObjectData">
                <Size X="153.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="190.0000" Y="26.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1473" Y="0.2737" />
                <PreSize X="0.1186" Y="0.3053" />
                <FileData Type="PlistSubImage" Path="sp_progress_bg.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bar_progress" ActionTag="576274012" Tag="9" IconVisible="False" LeftMargin="122.5000" RightMargin="1032.5000" TopMargin="62.5000" BottomMargin="19.5000" ctype="LoadingBarObjectData">
                <Size X="135.0000" Y="13.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="190.0000" Y="26.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1473" Y="0.2737" />
                <PreSize X="0.1047" Y="0.1368" />
                <ImageFileData Type="PlistSubImage" Path="sp_progress_bar.png" Plist="plaza/plaza.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_lv_7" ActionTag="1939102076" Tag="12" IconVisible="False" LeftMargin="179.6759" RightMargin="1071.3241" TopMargin="23.5928" BottomMargin="46.4072" ctype="SpriteObjectData">
                <Size X="39.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="199.1759" Y="58.9072" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1544" Y="0.6201" />
                <PreSize X="0.0302" Y="0.2632" />
                <FileData Type="PlistSubImage" Path="sp_lv.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_levels" ActionTag="-582711419" Tag="14" IconVisible="False" LeftMargin="242.1112" RightMargin="1047.8888" TopMargin="36.0940" BottomMargin="58.9060" CharWidth="16" CharHeight="20" LabelText="" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="242.1112" Y="58.9060" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1877" Y="0.6201" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="plaza/atlas_level.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_8" ActionTag="1539303593" Tag="13" IconVisible="False" LeftMargin="134.7492" RightMargin="1112.2507" TopMargin="17.5909" BottomMargin="37.4091" ctype="SpriteObjectData">
                <Size X="43.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="156.2492" Y="57.4091" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1211" Y="0.6043" />
                <PreSize X="0.0333" Y="0.4211" />
                <FileData Type="PlistSubImage" Path="sp_crown.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_9" ActionTag="-1417200711" Tag="15" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="274.0164" RightMargin="971.9836" TopMargin="25.5000" BottomMargin="25.5000" ctype="SpriteObjectData">
                <Size X="44.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="296.0164" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2295" Y="0.5000" />
                <PreSize X="0.0341" Y="0.4632" />
                <FileData Type="PlistSubImage" Path="sp_coin.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_9_0" ActionTag="-1941655254" Tag="16" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="587.8598" RightMargin="663.1402" TopMargin="27.5000" BottomMargin="27.5000" ctype="SpriteObjectData">
                <Size X="39.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="607.3598" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4708" Y="0.5000" />
                <PreSize X="0.0302" Y="0.4211" />
                <FileData Type="PlistSubImage" Path="sp_bean.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_9_0_0" ActionTag="782425198" Tag="17" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="899.8617" RightMargin="345.1383" TopMargin="29.5000" BottomMargin="29.5000" ctype="SpriteObjectData">
                <Size X="45.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="922.3617" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7150" Y="0.5000" />
                <PreSize X="0.0349" Y="0.3789" />
                <FileData Type="PlistSubImage" Path="sp_ingot.png" Plist="plaza/plaza.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_coin" ActionTag="282711696" Tag="78" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="402.0000" RightMargin="888.0000" TopMargin="46.4075" BottomMargin="48.5925" CharWidth="18" CharHeight="25" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="402.0000" Y="48.5925" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5115" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="plaza/altas_plazz_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_take" ActionTag="-1107056064" Tag="19" IconVisible="False" LeftMargin="493.1187" RightMargin="752.8813" TopMargin="25.0488" BottomMargin="27.9512" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="11" BottomEage="11" Scale9OriginX="14" Scale9OriginY="11" Scale9Width="20" Scale9Height="24" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="44.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="515.1187" Y="48.9512" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3993" Y="0.5153" />
                <PreSize X="0.0341" Y="0.4421" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_take_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_take_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_take_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_bean" ActionTag="-1069321369" Tag="79" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="715.6900" RightMargin="574.3100" TopMargin="46.4075" BottomMargin="48.5925" CharWidth="18" CharHeight="25" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="715.6900" Y="48.5925" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5548" Y="0.5115" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="plaza/altas_plazz_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_charge" ActionTag="-1090451147" Tag="21" IconVisible="False" LeftMargin="808.4587" RightMargin="437.5413" TopMargin="24.0500" BottomMargin="26.9500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="11" BottomEage="11" Scale9OriginX="14" Scale9OriginY="11" Scale9Width="34" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="44.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="830.4587" Y="48.9500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6438" Y="0.5153" />
                <PreSize X="0.0341" Y="0.4632" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <PressedFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <NormalFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_ingot" ActionTag="709504390" Tag="80" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="1031.6899" RightMargin="258.3101" TopMargin="46.4075" BottomMargin="48.5925" CharWidth="18" CharHeight="25" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1031.6899" Y="48.5925" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7998" Y="0.5115" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="plaza/altas_plazz_num.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_exchange" ActionTag="-1585548012" Tag="23" IconVisible="False" LeftMargin="1121.1233" RightMargin="124.8767" TopMargin="25.0500" BottomMargin="27.9500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="14" RightEage="14" TopEage="11" BottomEage="11" Scale9OriginX="14" Scale9OriginY="11" Scale9Width="20" Scale9Height="24" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="44.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1143.1233" Y="48.9500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8861" Y="0.5153" />
                <PreSize X="0.0341" Y="0.4421" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_dui_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_dui_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_dui_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_box" ActionTag="-245503490" Tag="92" IconVisible="False" LeftMargin="25.0000" RightMargin="1205.0000" TopMargin="115.9999" BottomMargin="-80.9999" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="32" Scale9Height="40" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="-50.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0426" Y="-0.5368" />
                <PreSize X="0.0465" Y="0.6316" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <PressedFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <NormalFileData Type="PlistSubImage" Path="blank.png" Plist="public/public.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="666.9993" Y="731.9995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9760" />
            <PreSize X="0.9670" Y="0.1267" />
            <FileData Type="PlistSubImage" Path="plaza_top_bg.png" Plist="plaza/plaza.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bottom_bg" ActionTag="-1399102176" VisibleForFrame="False" Tag="26" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-2.0000" RightMargin="-2.0000" TopMargin="631.0000" ctype="SpriteObjectData">
            <Size X="1338.0000" Y="119.0000" />
            <Children>
              <AbstractNodeData Name="btn_shop" ActionTag="-2002924590" Tag="24" IconVisible="False" LeftMargin="2.0000" RightMargin="1066.0000" TopMargin="21.3523" BottomMargin="0.6477" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="240" Scale9Height="75" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="270.0000" Y="97.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="137.0000" Y="49.1477" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1024" Y="0.4130" />
                <PreSize X="0.2018" Y="0.8151" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_shop_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_shop_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_shop_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_friend" ActionTag="-406888020" Tag="25" IconVisible="False" LeftMargin="834.6921" RightMargin="230.3079" TopMargin="17.0818" BottomMargin="2.9182" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="247" Scale9Height="81" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="273.0000" Y="99.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="971.1921" Y="52.4182" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7259" Y="0.4405" />
                <PreSize X="0.2040" Y="0.8319" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_friend_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_friend_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_friend_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_task" ActionTag="-1871722783" Tag="26" IconVisible="False" LeftMargin="237.8771" RightMargin="827.1229" TopMargin="14.6516" BottomMargin="4.3484" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="247" Scale9Height="82" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="273.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="374.3771" Y="54.3484" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2798" Y="0.4567" />
                <PreSize X="0.2040" Y="0.8403" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_task_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_task_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_task_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_rank" ActionTag="-1742577587" Tag="27" IconVisible="False" LeftMargin="1065.0000" RightMargin="9.0000" TopMargin="23.3500" BottomMargin="2.6500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="238" Scale9Height="75" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="264.0000" Y="93.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1197.0000" Y="49.1500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8946" Y="0.4130" />
                <PreSize X="0.1973" Y="0.7815" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_rank_0.png" Plist="plaza/plaza.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_rank_1.png" Plist="plaza/plaza.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_rank_0.png" Plist="plaza/plaza.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="image_start" ActionTag="221555060" Tag="31" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="490.0000" RightMargin="490.0000" TopMargin="-59.0000" BottomMargin="2.0000" Scale9Width="362" Scale9Height="180" ctype="ImageViewObjectData">
                <Size X="358.0000" Y="176.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="669.0000" Y="90.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7563" />
                <PreSize X="0.2676" Y="1.4790" />
                <FileData Type="PlistSubImage" Path="ani_start_0.png" Plist="plaza/plaza.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="1.0030" Y="0.1587" />
            <FileData Type="PlistSubImage" Path="sp_bottom_bg.png" Plist="plaza/plaza.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnMajiang" ActionTag="1431714915" VisibleForFrame="False" Tag="153" IconVisible="False" LeftMargin="118.9316" RightMargin="856.0684" TopMargin="169.8213" BottomMargin="429.1787" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="322" Scale9Height="108" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="359.0000" Y="151.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.4316" Y="504.6787" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2237" Y="0.6729" />
            <PreSize X="0.2691" Y="0.2013" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_mj_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="plaza/hzmj/btn_mj_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="plaza/hzmj/btn_mj_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnLand" ActionTag="484491322" VisibleForFrame="False" Tag="154" IconVisible="False" LeftMargin="116.9316" RightMargin="856.0684" TopMargin="373.0800" BottomMargin="227.9200" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="321" Scale9Height="105" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="361.0000" Y="149.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="297.4316" Y="302.4200" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2230" Y="0.4032" />
            <PreSize X="0.2706" Y="0.1987" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_land_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="plaza/hzmj/btn_land_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="plaza/hzmj/btn_land_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="GameOpListLayout" ActionTag="1765251116" Tag="155" IconVisible="False" LeftMargin="915.0000" RightMargin="69.0000" TopMargin="105.0000" BottomMargin="145.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="350.0000" Y="500.0000" />
            <Children>
              <AbstractNodeData Name="BtnMjCr" ActionTag="247849960" Tag="156" IconVisible="False" LeftMargin="6.0000" RightMargin="4.0000" TopMargin="18.0000" BottomMargin="348.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="311" Scale9Height="104" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="340.0000" Y="134.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="176.0000" Y="415.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5029" Y="0.8300" />
                <PreSize X="0.9714" Y="0.2680" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_mj_cr_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_mj_cr_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_mj_cr_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnMjJbc" ActionTag="-2088636448" Tag="161" IconVisible="False" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="321.5000" BottomMargin="31.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="308" Scale9Height="110" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="340.0000" Y="147.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="175.0000" Y="105.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2100" />
                <PreSize X="0.9714" Y="0.2940" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_mj_jbc_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_mj_jbc_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_mj_jbc_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnMjJoin" ActionTag="-1386101062" Tag="162" IconVisible="False" LeftMargin="3.5000" RightMargin="3.5000" TopMargin="173.0000" BottomMargin="193.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="306" Scale9Height="109" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="343.0000" Y="134.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="175.0000" Y="260.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5200" />
                <PreSize X="0.9800" Y="0.2680" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_mj_join_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_mj_join_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_mj_join_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnLandCr" ActionTag="-1521039567" Tag="163" IconVisible="False" LeftMargin="436.0699" RightMargin="-427.0699" TopMargin="3.0000" BottomMargin="333.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="311" Scale9Height="131" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="341.0000" Y="164.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="606.5699" Y="415.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.7331" Y="0.8300" />
                <PreSize X="0.9743" Y="0.3280" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_land_cr_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_land_cr_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_land_cr_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnLandJbc" ActionTag="-1345120140" Tag="164" IconVisible="False" LeftMargin="436.8353" RightMargin="-427.8353" TopMargin="319.5000" BottomMargin="29.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="307" Scale9Height="125" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="341.0000" Y="151.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="607.3353" Y="105.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.7352" Y="0.2100" />
                <PreSize X="0.9743" Y="0.3020" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_land_jbc_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_land_jbc_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_land_jbc_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnLandJoin" ActionTag="-1545729863" Tag="165" IconVisible="False" LeftMargin="435.8381" RightMargin="-427.8381" TopMargin="168.5000" BottomMargin="188.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="309" Scale9Height="126" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="342.0000" Y="143.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="606.8381" Y="260.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.7338" Y="0.5200" />
                <PreSize X="0.9771" Y="0.2860" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_land_join_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_land_join_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_land_join_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1090.0000" Y="395.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8171" Y="0.5267" />
            <PreSize X="0.2624" Y="0.6667" />
            <SingleColor A="255" R="255" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BottomLayout" ActionTag="-162361516" Tag="53" IconVisible="False" LeftMargin="167.0000" RightMargin="167.0000" TopMargin="646.7306" BottomMargin="3.2694" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1000.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="BtnHuoDong" ActionTag="-334330599" Tag="169" IconVisible="False" LeftMargin="-114.6018" RightMargin="974.6018" TopMargin="32.2675" BottomMargin="7.7325" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="110" Scale9Height="38" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="140.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-44.6018" Y="37.7325" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.0446" Y="0.3773" />
                <PreSize X="0.1400" Y="0.6000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_activety_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_activety_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_activety_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnZhanJi" ActionTag="374463298" Tag="170" IconVisible="False" LeftMargin="112.5956" RightMargin="749.4044" TopMargin="35.7675" BottomMargin="11.2325" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="108" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="138.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="181.5956" Y="37.7325" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1816" Y="0.3773" />
                <PreSize X="0.1380" Y="0.5300" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_zhanji_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_zhanji_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_zhanji_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnFriends" ActionTag="1712976484" Tag="171" IconVisible="False" LeftMargin="327.7927" RightMargin="527.2073" TopMargin="35.7675" BottomMargin="11.2325" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="115" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="145.0000" Y="53.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.2927" Y="37.7325" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4003" Y="0.3773" />
                <PreSize X="0.1450" Y="0.5300" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_friend_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_friend_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_friend_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnShare" ActionTag="-452483963" Tag="172" IconVisible="False" LeftMargin="533.9904" RightMargin="322.0096" TopMargin="32.7675" BottomMargin="8.2325" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="114" Scale9Height="37" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="144.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="605.9904" Y="37.7325" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6060" Y="0.3773" />
                <PreSize X="0.1440" Y="0.5900" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_share_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_share_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_share_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnKeFu" ActionTag="2025535822" Tag="173" IconVisible="False" LeftMargin="738.1880" RightMargin="113.8120" TopMargin="30.7675" BottomMargin="6.2325" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="118" Scale9Height="41" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="148.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="812.1880" Y="37.7325" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8122" Y="0.3773" />
                <PreSize X="0.1480" Y="0.6300" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_kefu_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_kefu_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_kefu_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnMyRoom" ActionTag="1166827296" Tag="57" IconVisible="False" LeftMargin="960.8853" RightMargin="-107.8853" TopMargin="42.2675" BottomMargin="17.7325" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="117" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="147.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1034.3853" Y="37.7325" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0344" Y="0.3773" />
                <PreSize X="0.1470" Y="0.4000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/wodefangjian2.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/wodefangjian2.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/wodefangjian.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="667.0000" Y="3.2694" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0044" />
            <PreSize X="0.7496" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="TopLayout" ActionTag="1193812059" Tag="54" IconVisible="False" BottomMargin="630.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="120.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_2" ActionTag="-1388367470" Tag="136" IconVisible="False" LeftMargin="97.9970" RightMargin="956.0030" TopMargin="20.2915" BottomMargin="-0.2915" ctype="SpriteObjectData">
                <Size X="280.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="237.9970" Y="49.7085" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1784" Y="0.4142" />
                <PreSize X="0.2099" Y="0.8333" />
                <FileData Type="Normal" Path="plaza/hzmj/touxiangkuang.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3" ActionTag="-1610776473" Tag="137" IconVisible="False" LeftMargin="643.0421" RightMargin="523.9579" TopMargin="26.3568" BottomMargin="39.6432" ctype="SpriteObjectData">
                <Size X="167.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="726.5421" Y="66.6432" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5446" Y="0.5554" />
                <PreSize X="0.1252" Y="0.4500" />
                <FileData Type="Normal" Path="plaza/hzmj/fangka.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_4" ActionTag="-2131821640" Tag="138" IconVisible="False" LeftMargin="471.6792" RightMargin="696.3208" TopMargin="26.3568" BottomMargin="39.6432" ctype="SpriteObjectData">
                <Size X="166.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="554.6792" Y="66.6432" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4158" Y="0.5554" />
                <PreSize X="0.1244" Y="0.4500" />
                <FileData Type="Normal" Path="plaza/hzmj/jinbi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadNode" ActionTag="81995023" Tag="174" IconVisible="True" LeftMargin="147.0000" RightMargin="1187.0000" TopMargin="68.0000" BottomMargin="52.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="147.0000" Y="52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1102" Y="0.4333" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="HeadClickLayout" ActionTag="1871203956" Tag="56" IconVisible="False" LeftMargin="101.9927" RightMargin="967.0073" TopMargin="24.7645" BottomMargin="5.2355" TouchEnable="True" ClipAble="False" BackColorAlpha="148" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="265.0000" Y="90.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="101.9927" Y="50.2355" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0765" Y="0.4186" />
                <PreSize X="0.1987" Y="0.7500" />
                <SingleColor A="255" R="255" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickNameText" ActionTag="-1868529123" Tag="178" IconVisible="False" LeftMargin="215.0000" RightMargin="1119.0000" TopMargin="49.4996" BottomMargin="70.5004" FontSize="30" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="215.0000" Y="70.5004" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1612" Y="0.5875" />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickIDText" ActionTag="-1971259145" Tag="179" IconVisible="False" LeftMargin="215.0000" RightMargin="1119.0000" TopMargin="91.8912" BottomMargin="28.1088" FontSize="30" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="215.0000" Y="28.1088" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1612" Y="0.2342" />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TextJinBi" ActionTag="2145712415" Tag="180" IconVisible="False" LeftMargin="541.5865" RightMargin="713.4135" TopMargin="37.4447" BottomMargin="48.5553" FontSize="27" LabelText="99999" TouchScaleChangeAble="True" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="79.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="581.0865" Y="65.5553" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4356" Y="0.5463" />
                <PreSize X="0.0592" Y="0.2833" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TextFangKa" ActionTag="-803743742" Tag="181" IconVisible="False" LeftMargin="728.2745" RightMargin="572.7255" TopMargin="39.4447" BottomMargin="48.5553" FontSize="27" LabelText="99" TouchScaleChangeAble="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="33.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="744.7745" Y="64.5553" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5583" Y="0.5380" />
                <PreSize X="0.0247" Y="0.2667" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnSetting" ActionTag="686390321" Tag="168" IconVisible="False" LeftMargin="1159.6396" RightMargin="37.3604" TopMargin="24.8787" BottomMargin="35.1213" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="107" Scale9Height="38" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="137.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1228.1396" Y="65.1213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9206" Y="0.5427" />
                <PreSize X="0.1027" Y="0.5000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_setting_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_setting_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_setting_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnAddFK" ActionTag="-1212376721" VisibleForFrame="False" Tag="58" IconVisible="False" LeftMargin="550.9268" RightMargin="751.0732" TopMargin="32.1276" BottomMargin="51.8724" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="2" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="32.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="566.9268" Y="69.8724" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4250" Y="0.5823" />
                <PreSize X="0.0240" Y="0.3000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_add_fk_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/btn_add_fk_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/btn_add_fk_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RefreshRoomCardBtn" ActionTag="18842464" Tag="75" IconVisible="False" LeftMargin="814.2708" RightMargin="405.7292" TopMargin="38.4928" BottomMargin="43.5072" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="84" Scale9Height="16" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="114.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="871.2708" Y="62.5072" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6531" Y="0.5209" />
                <PreSize X="0.0855" Y="0.3167" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/plaza_btn_refresh_cardcnt_press.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/plaza_btn_refresh_cardcnt_press.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/plaza_btn_refresh_cardcnt.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="667.0000" Y="750.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="1.0000" Y="0.1600" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnReturn" ActionTag="986864260" Tag="55" IconVisible="False" LeftMargin="24.0000" RightMargin="1252.0000" TopMargin="40.5000" BottomMargin="650.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="37" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="58.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="53.0000" Y="680.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0397" Y="0.9067" />
            <PreSize X="0.0435" Y="0.0787" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="plaza/hzmj/btn_back_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="plaza/hzmj/btn_back_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="plaza/hzmj/btn_back_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="InvitedShowLayout" ActionTag="-1712015618" Tag="137" IconVisible="False" LeftMargin="777.1628" RightMargin="446.8372" TopMargin="172.3652" BottomMargin="467.6348" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="110.0000" Y="110.0000" />
            <Children>
              <AbstractNodeData Name="SpLight" ActionTag="210004657" Tag="140" IconVisible="False" LeftMargin="5.5000" RightMargin="5.5000" TopMargin="4.5000" BottomMargin="4.5000" ctype="SpriteObjectData">
                <Size X="99.0000" Y="101.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="55.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.9000" Y="0.9182" />
                <FileData Type="Normal" Path="plaza/invited_plaza/invited_plaza_14.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpIcon" ActionTag="929257353" Tag="139" IconVisible="False" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="17.5000" BottomMargin="17.5000" ctype="SpriteObjectData">
                <Size X="77.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.2000" />
                <Position X="55.0000" Y="32.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2955" />
                <PreSize X="0.7000" Y="0.6818" />
                <FileData Type="Normal" Path="plaza/invited_plaza/invited_plaza_13.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="SpDescr" ActionTag="-947908592" Tag="138" IconVisible="False" TopMargin="72.1861" BottomMargin="8.8139" ctype="SpriteObjectData">
                <Size X="110.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="23.3139" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2119" />
                <PreSize X="1.0000" Y="0.2636" />
                <FileData Type="Normal" Path="plaza/invited_plaza/invited_plaza_10.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="832.1628" Y="522.6348" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6238" Y="0.6968" />
            <PreSize X="0.0825" Y="0.1467" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_trumpet_bg" ActionTag="1470052724" Tag="86" IconVisible="False" LeftMargin="443.4999" RightMargin="443.5001" TopMargin="111.4399" BottomMargin="611.5601" ctype="SpriteObjectData">
            <Size X="447.0000" Y="27.0000" />
            <Children>
              <AbstractNodeData Name="btn_trumpet" ActionTag="765506216" Tag="44" IconVisible="False" LeftMargin="29.1608" RightMargin="394.8392" TopMargin="2.3311" BottomMargin="2.6689" TouchEnable="True" FontSize="14" LeftEage="7" RightEage="7" TopEage="11" BottomEage="11" Scale9OriginX="7" Scale9OriginY="11" Scale9Width="9" Scale9Height="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="23.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="40.6608" Y="13.6689" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0910" Y="0.5063" />
                <PreSize X="0.0515" Y="0.8148" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/hzmj/lb.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/hzmj/lb.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/hzmj/lb.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="666.9999" Y="625.0601" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8334" />
            <PreSize X="0.3351" Y="0.0360" />
            <FileData Type="Normal" Path="plaza/hzmj/noticebanner.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>