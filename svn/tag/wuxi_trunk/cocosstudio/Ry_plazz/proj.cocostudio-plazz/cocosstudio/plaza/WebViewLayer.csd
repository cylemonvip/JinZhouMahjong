<GameFile>
  <PropertyGroup Name="WebViewLayer" Type="Layer" ID="301c6c2f-66d6-4993-969b-21cbb9382280" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="59" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="panel_mask" ActionTag="1343982046" Tag="60" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_bg" ActionTag="166812761" Tag="6" IconVisible="False" LeftMargin="182.0000" RightMargin="182.0000" TopMargin="79.9200" BottomMargin="66.0800" Scale9Enable="True" LeftEage="139" RightEage="316" TopEage="148" BottomEage="144" Scale9OriginX="139" Scale9OriginY="148" Scale9Width="315" Scale9Height="212" ctype="ImageViewObjectData">
            <Size X="970.0000" Y="604.0000" />
            <Children>
              <AbstractNodeData Name="check_switch" ActionTag="-2051607491" Tag="48" IconVisible="False" LeftMargin="16.6352" RightMargin="883.3648" TopMargin="-254.6209" BottomMargin="788.6209" TouchEnable="True" ctype="CheckBoxObjectData">
                <Size X="70.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="51.6352" Y="823.6209" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0532" Y="1.3636" />
                <PreSize X="0.0722" Y="0.1159" />
                <NormalBackFileData Type="Normal" Path="plaza/ad_pub_switch_noshow_02.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="plaza/ad_pub_switch_noshow_02.png" Plist="" />
                <DisableBackFileData Type="Normal" Path="plaza/ad_pub_switch_noshow_02.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="plaza/ad_pub_switch_noshow_01.png" Plist="" />
                <NodeDisableFileData Type="Normal" Path="plaza/ad_pub_switch_noshow_01.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt" ActionTag="-1050923216" VisibleForFrame="False" Tag="75" IconVisible="False" LeftMargin="414.0000" RightMargin="434.0000" TopMargin="5.4582" BottomMargin="525.5418" FontSize="60" LabelText="公告" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="122.0000" Y="73.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="475.0000" Y="562.0418" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4897" Y="0.9305" />
                <PreSize X="0.1258" Y="0.1209" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ad_pub_noshowtips_1" ActionTag="2095696474" Tag="49" IconVisible="False" LeftMargin="86.0018" RightMargin="774.9982" TopMargin="-229.6097" BottomMargin="809.6097" ctype="SpriteObjectData">
                <Size X="109.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="140.5018" Y="821.6097" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1448" Y="1.3603" />
                <PreSize X="0.1124" Y="0.0397" />
                <FileData Type="Normal" Path="plaza/ad_pub_noshowtips.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="content" ActionTag="-2048825347" Tag="64" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="17.9850" RightMargin="17.0150" TopMargin="89.0000" BottomMargin="15.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="935.0000" Y="500.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="485.4850" Y="265.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5005" Y="0.4387" />
                <PreSize X="0.9639" Y="0.8278" />
                <SingleColor A="255" R="255" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_close" ActionTag="9458911" Tag="63" IconVisible="False" LeftMargin="893.2132" RightMargin="8.7868" TopMargin="6.4298" BottomMargin="529.5702" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="927.2132" Y="563.5702" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9559" Y="0.9331" />
                <PreSize X="0.0701" Y="0.1126" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="plaza/login_tips_btn_close.png" Plist="" />
                <PressedFileData Type="Normal" Path="plaza/login_tips_btn_close.png" Plist="" />
                <NormalFileData Type="Normal" Path="plaza/login_tips_btn_close.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="368.0800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4908" />
            <PreSize X="0.7271" Y="0.8053" />
            <FileData Type="Normal" Path="plaza/img_bg_describe.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>