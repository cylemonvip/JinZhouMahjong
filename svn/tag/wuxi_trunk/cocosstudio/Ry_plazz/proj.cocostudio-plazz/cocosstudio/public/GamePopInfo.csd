<GameFile>
  <PropertyGroup Name="GamePopInfo" Type="Layer" ID="7cfa1444-79cb-4145-a5d7-8ba1ea1cbad2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="192" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="484548114" VisibleForFrame="False" Tag="193" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="0.9413" RightMargin="-0.9413" TopMargin="1.7827" BottomMargin="-1.7827" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position X="0.9413" Y="-1.7827" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0007" Y="-0.0024" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_kuang" ActionTag="1668047194" Tag="155" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="342.0000" RightMargin="342.0000" TopMargin="150.0000" BottomMargin="150.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="203" RightEage="203" TopEage="110" BottomEage="110" Scale9OriginX="203" Scale9OriginY="110" Scale9Width="212" Scale9Height="116" ctype="PanelObjectData">
            <Size X="650.0000" Y="450.0000" />
            <Children>
              <AbstractNodeData Name="head_bg" ActionTag="-1292145578" Tag="228" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="56.6719" RightMargin="479.3281" TopMargin="37.5001" BottomMargin="298.4999" ctype="SpriteObjectData">
                <Size X="114.0000" Y="114.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.6719" Y="355.4999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1749" Y="0.7900" />
                <PreSize X="0.1754" Y="0.2533" />
                <FileData Type="PlistSubImage" Path="player_vip_game.png" Plist="public/public.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_front" ActionTag="39678740" Tag="49" IconVisible="False" LeftMargin="-16.8600" RightMargin="405.8600" TopMargin="-15.5000" BottomMargin="276.5000" LeftEage="42" RightEage="42" TopEage="42" BottomEage="42" Scale9OriginX="42" Scale9OriginY="42" Scale9Width="177" Scale9Height="105" ctype="ImageViewObjectData">
                <Size X="261.0000" Y="189.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.6400" Y="371.0000" />
                <Scale ScaleX="0.8700" ScaleY="0.8700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1748" Y="0.8244" />
                <PreSize X="0.4015" Y="0.4200" />
                <FileData Type="Normal" Path="public/head_sp_frame_0_0.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="NickName_title" ActionTag="-196107259" Tag="117" IconVisible="False" LeftMargin="210.8942" RightMargin="390.1058" TopMargin="65.7866" BottomMargin="358.2134" FontSize="20" LabelText="昵称:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="210.8942" Y="371.2134" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3245" Y="0.8249" />
                <PreSize X="0.0754" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IP_title" ActionTag="-1910023925" Tag="118" IconVisible="False" LeftMargin="410.4707" RightMargin="214.5293" TopMargin="106.0837" BottomMargin="317.9163" FontSize="20" LabelText="IP:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="25.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="410.4707" Y="330.9163" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6315" Y="0.7354" />
                <PreSize X="0.0385" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ID_title" ActionTag="673454716" Tag="119" IconVisible="False" LeftMargin="210.8942" RightMargin="412.1058" TopMargin="106.0837" BottomMargin="317.9163" FontSize="20" LabelText="ID:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="27.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="210.8942" Y="330.9163" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3245" Y="0.7354" />
                <PreSize X="0.0415" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sign_title" ActionTag="-154695679" VisibleForFrame="False" Tag="120" IconVisible="False" LeftMargin="210.1364" RightMargin="390.8636" TopMargin="120.2760" BottomMargin="303.7240" FontSize="20" LabelText="签名:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="210.1364" Y="316.7240" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3233" Y="0.7038" />
                <PreSize X="0.0754" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sex_txt" ActionTag="-950303558" Tag="122" IconVisible="False" LeftMargin="462.1580" RightMargin="164.8420" TopMargin="65.7866" BottomMargin="358.2134" FontSize="20" LabelText="男" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="23.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="462.1580" Y="371.2134" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="224" B="170" />
                <PrePosition X="0.7110" Y="0.8249" />
                <PreSize X="0.0354" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sex_title" ActionTag="845062206" Tag="121" IconVisible="False" LeftMargin="410.4707" RightMargin="190.5293" TopMargin="65.7866" BottomMargin="358.2134" FontSize="20" LabelText="性别:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="410.4707" Y="371.2134" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6315" Y="0.8249" />
                <PreSize X="0.0754" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sign_text" ActionTag="1526070798" VisibleForFrame="False" Tag="31" IconVisible="False" LeftMargin="261.0634" RightMargin="43.9366" TopMargin="121.2761" BottomMargin="303.7239" IsCustomSize="True" FontSize="16" LabelText="此人没签名" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="345.0000" Y="25.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="261.0634" Y="328.7239" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="232" B="170" />
                <PrePosition X="0.4016" Y="0.7305" />
                <PreSize X="0.5308" Y="0.0556" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nick_text" ActionTag="940244423" Tag="145" IconVisible="False" LeftMargin="261.1161" RightMargin="246.8839" TopMargin="66.7863" BottomMargin="358.2137" IsCustomSize="True" FontSize="20" LabelText="张三" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="142.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="261.1161" Y="370.7137" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="224" B="170" />
                <PrePosition X="0.4017" Y="0.8238" />
                <PreSize X="0.2185" Y="0.0556" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="id_text" ActionTag="720358670" Tag="234" IconVisible="False" LeftMargin="240.6767" RightMargin="343.3233" TopMargin="105.5837" BottomMargin="318.4163" FontSize="20" LabelText="123456" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="240.6767" Y="331.4163" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="232" B="170" />
                <PrePosition X="0.3703" Y="0.7365" />
                <PreSize X="0.1015" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Gold_title" ActionTag="-397697514" VisibleForFrame="False" Tag="153" IconVisible="False" LeftMargin="43.0632" RightMargin="557.9368" TopMargin="272.3143" BottomMargin="151.6857" FontSize="20" LabelText="金币:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="67.5632" Y="164.6857" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1039" Y="0.3660" />
                <PreSize X="0.0754" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_text" ActionTag="-1688199708" VisibleForFrame="False" CallBackType="Click" Tag="235" IconVisible="False" LeftMargin="100.2010" RightMargin="500.7990" TopMargin="272.5219" BottomMargin="151.4781" FontSize="20" LabelText="5000" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="100.2010" Y="164.4781" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1542" Y="0.3655" />
                <PreSize X="0.0754" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bean_text" ActionTag="1766717664" VisibleForFrame="False" Tag="236" IconVisible="False" LeftMargin="100.2011" RightMargin="533.7989" TopMargin="305.9474" BottomMargin="118.0526" FontSize="20" LabelText="0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="16.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="100.2011" Y="131.0526" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1542" Y="0.2912" />
                <PreSize X="0.0246" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ip_text" ActionTag="1687288741" Tag="54" IconVisible="False" LeftMargin="443.1313" RightMargin="111.8687" TopMargin="105.5837" BottomMargin="318.4163" FontSize="20" LabelText="192.168.0.1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="95.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="443.1313" Y="331.4163" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="232" B="170" />
                <PrePosition X="0.6817" Y="0.7365" />
                <PreSize X="0.1462" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="addfriend_btn" ActionTag="675523784" Tag="160" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="213.1550" RightMargin="214.8450" TopMargin="358.3698" BottomMargin="17.6302" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="10" RightEage="10" TopEage="10" BottomEage="10" Scale9OriginX="10" Scale9OriginY="10" Scale9Width="202" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="222.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="324.1550" Y="54.6302" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4987" Y="0.1214" />
                <PreSize X="0.3415" Y="0.1644" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="public/yixinFriend.png" Plist="" />
                <PressedFileData Type="Normal" Path="public/yixinFriend.png" Plist="" />
                <NormalFileData Type="Normal" Path="public/yixinFriend.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Dy_Expression" ActionTag="1994341197" Tag="156" IconVisible="False" LeftMargin="125.7144" RightMargin="77.2856" TopMargin="160.0000" BottomMargin="90.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="447.0000" Y="200.0000" />
                <Children>
                  <AbstractNodeData Name="Dy_Expression_1" ActionTag="685455067" Tag="123" IconVisible="False" LeftMargin="-17.3030" RightMargin="374.3030" TopMargin="3.9168" BottomMargin="106.0832" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="32" BottomEage="26" Scale9OriginX="36" Scale9OriginY="32" Scale9Width="40" Scale9Height="54" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="-2090251579" Tag="124" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="27.6970" Y="151.0832" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0620" Y="0.7554" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_2" ActionTag="-1628707821" Tag="125" IconVisible="False" LeftMargin="97.4887" RightMargin="259.5113" TopMargin="3.9168" BottomMargin="106.0832" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="910054181" Tag="126" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_2.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="142.4887" Y="151.0832" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3188" Y="0.7554" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_3" ActionTag="-754648413" Tag="127" IconVisible="False" LeftMargin="212.2804" RightMargin="144.7196" TopMargin="3.9168" BottomMargin="106.0832" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="-1768388672" Tag="128" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_3.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="257.2804" Y="151.0832" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5756" Y="0.7554" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_4" ActionTag="1320456445" Tag="129" IconVisible="False" LeftMargin="327.0728" RightMargin="29.9272" TopMargin="3.9168" BottomMargin="106.0832" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="-1116081455" Tag="130" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_4.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="372.0728" Y="151.0832" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8324" Y="0.7554" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_5" ActionTag="197222492" Tag="131" IconVisible="False" LeftMargin="-17.3030" RightMargin="374.3030" TopMargin="105.8488" BottomMargin="4.1512" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="-1437583319" Tag="132" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_5.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="27.6970" Y="49.1512" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0620" Y="0.2458" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_6" ActionTag="1407577203" Tag="133" IconVisible="False" LeftMargin="97.4887" RightMargin="259.5113" TopMargin="105.8488" BottomMargin="4.1512" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="-1032002829" Tag="134" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_6.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="142.4887" Y="49.1512" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3188" Y="0.2458" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_7" ActionTag="-536384380" Tag="135" IconVisible="False" LeftMargin="212.2804" RightMargin="144.7196" TopMargin="105.8488" BottomMargin="4.1512" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="2089248514" Tag="136" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_7.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="257.2804" Y="49.1512" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5756" Y="0.2458" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Dy_Expression_8" ActionTag="1160243009" Tag="137" IconVisible="False" LeftMargin="327.0728" RightMargin="29.9272" TopMargin="105.8487" BottomMargin="4.1513" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="36" RightEage="36" TopEage="36" BottomEage="36" Scale9OriginX="36" Scale9OriginY="36" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite" CanEdit="False" ActionTag="438014925" Tag="138" IconVisible="False" LeftMargin="-22.0000" RightMargin="-22.0000" TopMargin="-22.0000" BottomMargin="-22.0000" ctype="SpriteObjectData">
                        <Size X="134.0000" Y="134.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="45.0000" Y="45.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4889" Y="1.4889" />
                        <FileData Type="Normal" Path="public/dy_expression/pic/expression_icon_8.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="372.0728" Y="49.1513" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8324" Y="0.2458" />
                    <PreSize X="0.2013" Y="0.4500" />
                    <FileData Type="Normal" Path="public/dy_expression/record_9scale_bg.png" Plist="" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="349.2144" Y="190.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5373" Y="0.4222" />
                <PreSize X="0.6877" Y="0.4444" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Bean_title" ActionTag="-1163307046" VisibleForFrame="False" Tag="154" IconVisible="False" LeftMargin="43.0632" RightMargin="557.9368" TopMargin="305.9474" BottomMargin="118.0526" FontSize="20" LabelText="豆豆:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="67.5632" Y="131.0526" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1039" Y="0.2912" />
                <PreSize X="0.0754" Y="0.0578" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.4873" Y="0.6000" />
            <FileData Type="PlistSubImage" Path="chat_bg_only.png" Plist="Chat/chat.plist" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>