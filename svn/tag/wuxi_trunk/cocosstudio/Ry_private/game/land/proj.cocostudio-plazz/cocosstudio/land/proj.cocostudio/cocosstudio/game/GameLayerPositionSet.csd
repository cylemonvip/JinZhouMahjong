<GameFile>
  <PropertyGroup Name="GameLayerPositionSet" Type="Layer" ID="79acf30e-2fa8-4dd4-b9df-7c2e05a0e98b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="47" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="game_bg_0_1" ActionTag="46041988" Tag="65" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ctype="SpriteObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="PlistSubImage" Path="game_bg_0.png" Plist="public_res/public_res.plist" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_layout" ActionTag="25663319" Tag="77" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="game_topMenu_1" ActionTag="376140498" Tag="277" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="332.0000" RightMargin="332.0000" BottomMargin="642.0000" ctype="SpriteObjectData">
                <Size X="670.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="667.0000" Y="750.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="0.5022" Y="0.1440" />
                <FileData Type="PlistSubImage" Path="game_topMenu.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-1586993847" Tag="278" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="597.0000" RightMargin="667.0000" TopMargin="57.5000" BottomMargin="657.5000" FontSize="30" LabelText="叫分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="35.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="667.0000" Y="675.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9000" />
                <PreSize X="0.0525" Y="0.0467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="gamecall_text" ActionTag="1987961410" Tag="279" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="671.6690" RightMargin="662.3310" TopMargin="75.0000" BottomMargin="675.0000" FontSize="30" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="671.6690" Y="675.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.5035" Y="0.9000" />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="back_btn" ActionTag="-2078719511" Tag="67" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="876.1942" RightMargin="365.8058" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="922.1942" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6913" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="game_leave.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="game_leave_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="game_leave.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="set_btn" ActionTag="-1459776986" Tag="68" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="766.2726" RightMargin="475.7274" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="812.2726" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6089" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_setting_0.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_setting_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_setting_0.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="tru_btn" ActionTag="1067060848" Tag="69" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="471.0584" RightMargin="770.9416" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="517.0584" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3876" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_trust_0.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_trust_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_trust_0.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="chat_btn" ActionTag="1056259407" Tag="70" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="362.8710" RightMargin="879.1290" TopMargin="3.0000" BottomMargin="663.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="62" Scale9Height="62" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="92.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="408.8710" Y="705.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3065" Y="0.9400" />
                <PreSize X="0.0690" Y="0.1120" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="btn_chat_0.png" Plist="public_res/public_res.plist" />
                <PressedFileData Type="PlistSubImage" Path="btn_chat_1.png" Plist="public_res/public_res.plist" />
                <NormalFileData Type="PlistSubImage" Path="btn_chat_0.png" Plist="public_res/public_res.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ready_btn" ActionTag="-512199298" Tag="75" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="551.0000" RightMargin="551.0000" TopMargin="421.7500" BottomMargin="249.2500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="202" Scale9Height="57" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="232.0000" Y="79.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3850" />
                <PreSize X="0.1739" Y="0.1053" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="btn_ready_0.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="userstate_control" ActionTag="-589800948" Tag="75" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="ready1" ActionTag="-955878366" VisibleForFrame="False" Tag="93" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="279.5000" RightMargin="946.5000" TopMargin="199.0000" BottomMargin="499.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="333.5000" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2500" Y="0.7000" />
                <PreSize X="0.0810" Y="0.0693" />
                <FileData Type="PlistSubImage" Path="txt_ready.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ready2" ActionTag="-1040522555" VisibleForFrame="False" Tag="91" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="613.0000" RightMargin="613.0000" TopMargin="566.5000" BottomMargin="131.5000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="157.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2100" />
                <PreSize X="0.0810" Y="0.0693" />
                <FileData Type="PlistSubImage" Path="txt_ready.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ready3" ActionTag="1872747879" VisibleForFrame="False" Tag="92" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="946.5000" RightMargin="279.5000" TopMargin="199.0000" BottomMargin="499.0000" ctype="SpriteObjectData">
                <Size X="108.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1000.5000" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.7000" />
                <PreSize X="0.0810" Y="0.0693" />
                <FileData Type="PlistSubImage" Path="txt_ready.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="state_sp1" ActionTag="1890529561" Tag="118" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="316.8400" RightMargin="957.1600" TopMargin="195.0000" BottomMargin="495.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="346.8400" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2600" Y="0.7000" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="state_sp2" ActionTag="-439710818" Tag="76" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="156.7600" RightMargin="1117.2400" TopMargin="459.7500" BottomMargin="230.2500" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="186.7600" Y="260.2500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1400" Y="0.3470" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="state_sp3" ActionTag="-1357661018" Tag="117" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="983.8400" RightMargin="290.1600" TopMargin="195.0000" BottomMargin="495.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1013.8400" Y="525.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7600" Y="0.7000" />
                <PreSize X="0.0450" Y="0.0800" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="callscore_control" ActionTag="708489431" Tag="86" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="score_btn1" ActionTag="-2125142203" Tag="87" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="365.2300" RightMargin="778.7700" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="460.2300" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3450" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_pass.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_btn2" ActionTag="1686241839" Tag="88" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="592.0100" RightMargin="551.9900" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="687.0100" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5150" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_one.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_btn3" ActionTag="-390430517" Tag="89" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="818.7900" RightMargin="325.2100" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="913.7900" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6850" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_two.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_btn4" ActionTag="-1919289208" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1045.5701" RightMargin="98.4299" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1140.5701" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8550" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_score_three.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ongame_control" ActionTag="1351370147" VisibleForFrame="False" Tag="71" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="pass_btn" ActionTag="-1669409434" Tag="368" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="231.8300" RightMargin="912.1700" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="326.8300" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2450" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_pass.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="suggest_btn" ActionTag="1317905002" Tag="73" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="685.3900" RightMargin="458.6100" TopMargin="422.2500" BottomMargin="249.7500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="780.3900" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5850" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_tip.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="outcard_btn" ActionTag="337477917" Tag="72" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="910.1180" RightMargin="233.8821" TopMargin="420.0504" BottomMargin="251.9496" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5108" ScaleY="0.4718" />
                <Position X="1007.1700" Y="288.7500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7550" Y="0.3850" />
                <PreSize X="0.1424" Y="0.1040" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="PlistSubImage" Path="game_bt_out.png" Plist="game/game.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="outcards_control" Visible="False" ActionTag="495270638" Tag="67" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="info" ActionTag="1529881482" Tag="426" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="600.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_9" ActionTag="2009002679" Tag="427" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="33.0000" ctype="SpriteObjectData">
                <Size X="1334.0000" Y="117.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="667.0000" />
                <Scale ScaleX="1.0000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.7800" />
                <FileData Type="PlistSubImage" Path="back_tip.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tmpname_text" ActionTag="-1030113918" Tag="479" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.7400" RightMargin="1088.2600" TopMargin="101.1350" BottomMargin="13.8650" FontSize="30" LabelText="1234567891011" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="198.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="146.7400" Y="31.3650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1100" Y="0.2091" />
                <PreSize X="0.1484" Y="0.2333" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="game_gold_1" ActionTag="-1367521202" Tag="518" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="302.5702" RightMargin="955.4298" TopMargin="78.8350" BottomMargin="-4.8350" ctype="SpriteObjectData">
                <Size X="76.0000" Y="76.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="340.5702" Y="33.1650" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2553" Y="0.2211" />
                <PreSize X="0.0570" Y="0.5067" />
                <FileData Type="PlistSubImage" Path="game_gold.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_atlas" ActionTag="384441280" Tag="577" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="549.8748" RightMargin="784.1252" TopMargin="118.5000" BottomMargin="31.5000" CharWidth="23" CharHeight="30" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="549.8748" Y="31.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4122" Y="0.2100" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/shuzi.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="game_cellScoreIMG_1" ActionTag="1914105072" Tag="619" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="765.4772" RightMargin="456.5228" TopMargin="91.8350" BottomMargin="8.1650" ctype="SpriteObjectData">
                <Size X="112.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="821.4772" Y="33.1650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6158" Y="0.2211" />
                <PreSize X="0.0840" Y="0.3333" />
                <FileData Type="PlistSubImage" Path="game_cellScoreIMG.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dizhu_atlas" ActionTag="1385333979" Tag="621" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1079.4728" RightMargin="254.5272" TopMargin="118.1700" BottomMargin="31.8300" CharWidth="23" CharHeight="30" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1079.4728" Y="31.8300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8092" Y="0.2122" />
                <PreSize X="0.0000" Y="0.0000" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="game/shuzi.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="info_tip" ActionTag="-556129195" Tag="156" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="637.0000" RightMargin="637.0000" TopMargin="-40.0000" BottomMargin="130.0000" ctype="SpriteObjectData">
                <Size X="60.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="160.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0667" />
                <PreSize X="0.0450" Y="0.4000" />
                <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg_clock_1" ActionTag="2078204638" Tag="153" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="617.0000" RightMargin="617.0000" TopMargin="-485.0000" BottomMargin="525.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="110.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="580.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="3.8667" />
                <PreSize X="0.0750" Y="0.7333" />
                <FileData Type="PlistSubImage" Path="bg_clock.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg_clock_1_0" ActionTag="-1624074500" Tag="154" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="1083.9000" RightMargin="150.1000" TopMargin="-485.0000" BottomMargin="525.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="110.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1133.9000" Y="580.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8500" Y="3.8667" />
                <PreSize X="0.0750" Y="0.7333" />
                <FileData Type="PlistSubImage" Path="bg_clock.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg_clock_1_0_0" ActionTag="-444390106" Tag="155" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="150.1000" RightMargin="1083.9000" TopMargin="-485.0000" BottomMargin="525.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="110.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.1000" Y="580.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1500" Y="3.8667" />
                <PreSize X="0.0750" Y="0.7333" />
                <FileData Type="PlistSubImage" Path="bg_clock.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.2000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tru_control" ActionTag="-945568001" VisibleForFrame="False" Tag="88" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="Text_11" ActionTag="1644917598" Tag="81" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="547.0000" RightMargin="547.0000" TopMargin="285.0000" BottomMargin="435.0000" FontSize="30" LabelText="点击屏幕取消托管" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="240.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.0000" Y="450.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6000" />
                <PreSize X="0.1799" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="-497045577" Tag="134" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="972.0000" TopMargin="414.0000" ctype="SpriteObjectData">
                <Size X="362.0000" Y="336.0000" />
                <AnchorPoint ScaleX="1.0000" />
                <Position X="1334.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" />
                <PreSize X="0.2714" Y="0.4480" />
                <FileData Type="PlistSubImage" Path="game_robot-hd.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="-1654415265" Tag="252" IconVisible="True" LeftMargin="122.0000" RightMargin="1212.0000" TopMargin="572.0000" BottomMargin="178.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="head_frame" ActionTag="903478272" Tag="251" IconVisible="False" LeftMargin="-121.0000" RightMargin="-121.0000" TopMargin="-110.5000" BottomMargin="-110.5000" ctype="SpriteObjectData">
                <Size X="242.0000" Y="221.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="game_role21.png" Plist="game/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_bg" ActionTag="1409582488" Tag="246" IconVisible="False" LeftMargin="-56.0000" RightMargin="-56.0000" TopMargin="45.9606" BottomMargin="-101.9606" ctype="SpriteObjectData">
                <Size X="112.0000" Y="56.0000" />
                <Children>
                  <AbstractNodeData Name="name_text" ActionTag="-1662820060" Tag="247" IconVisible="False" LeftMargin="7.0001" RightMargin="4.9999" TopMargin="6.0000" BottomMargin="30.0000" IsCustomSize="True" FontSize="20" LabelText="123345" TouchScaleChangeAble="True" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="57.0001" Y="40.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="0.5089" Y="0.7143" />
                    <PreSize X="0.8929" Y="0.3571" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="coin_text" ActionTag="-1414680151" Tag="248" IconVisible="False" LeftMargin="24.4178" RightMargin="7.5822" TopMargin="28.5274" BottomMargin="7.4726" IsCustomSize="True" FontSize="12" LabelText="999999999" TouchScaleChangeAble="True" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="80.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="64.4178" Y="17.4726" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.5752" Y="0.3120" />
                    <PreSize X="0.7143" Y="0.3571" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-73.9606" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="role_head_bg.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chat_bg" ActionTag="-1896767672" Tag="249" IconVisible="False" LeftMargin="93.0000" RightMargin="-263.0000" TopMargin="-70.0000" BottomMargin="30.0000" Scale9Enable="True" LeftEage="12" RightEage="55" TopEage="13" BottomEage="13" Scale9OriginX="12" Scale9OriginY="13" Scale9Width="103" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="170.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="chat_face" ActionTag="-1567744267" Tag="250" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="55.0000" TopMargin="-14.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                    <Size X="60.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="85.0000" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6000" />
                    <PreSize X="0.3529" Y="1.5000" />
                    <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="93.0000" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="game_chat_0.png" Plist="game/game.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="122.0000" Y="178.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0915" Y="0.2373" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1_0" ActionTag="1971110075" Tag="253" IconVisible="True" LeftMargin="207.0000" RightMargin="1127.0000" TopMargin="334.0000" BottomMargin="416.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="head_frame" ActionTag="133989252" Tag="259" IconVisible="False" LeftMargin="-82.5000" RightMargin="-82.5000" TopMargin="-92.5000" BottomMargin="-92.5000" ctype="SpriteObjectData">
                <Size X="165.0000" Y="185.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="game_role12.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_bg" ActionTag="1712924920" Tag="254" IconVisible="False" LeftMargin="-56.0000" RightMargin="-56.0000" TopMargin="24.0000" BottomMargin="-80.0000" ctype="SpriteObjectData">
                <Size X="112.0000" Y="56.0000" />
                <Children>
                  <AbstractNodeData Name="name_text" ActionTag="941094275" Tag="255" IconVisible="False" LeftMargin="6.3788" RightMargin="5.6212" TopMargin="6.3953" BottomMargin="29.6047" IsCustomSize="True" FontSize="20" LabelText="" TouchScaleChangeAble="True" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="20.0000" />
                    <AnchorPoint />
                    <Position X="6.3788" Y="29.6047" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="0.0570" Y="0.5287" />
                    <PreSize X="0.8929" Y="0.3571" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="coin_text" ActionTag="351367282" Tag="256" IconVisible="False" LeftMargin="23.0687" RightMargin="8.9313" TopMargin="29.0134" BottomMargin="6.9866" IsCustomSize="True" FontSize="12" LabelText="123456" TouchScaleChangeAble="True" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="80.0000" Y="20.0000" />
                    <AnchorPoint />
                    <Position X="23.0687" Y="6.9866" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.2060" Y="0.1248" />
                    <PreSize X="0.7143" Y="0.3571" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="-52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="role_head_bg.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chat_bg" ActionTag="-11449188" Tag="257" IconVisible="False" LeftMargin="93.0000" RightMargin="-261.0000" TopMargin="-79.0000" BottomMargin="41.0000" Scale9Enable="True" LeftEage="12" RightEage="55" TopEage="13" BottomEage="13" Scale9OriginX="12" Scale9OriginY="13" Scale9Width="103" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="168.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="chat_face" ActionTag="-574141302" Tag="258" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="54.0000" RightMargin="54.0000" TopMargin="-14.8000" BottomMargin="-7.2000" ctype="SpriteObjectData">
                    <Size X="60.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="84.0000" Y="22.8000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6000" />
                    <PreSize X="0.3571" Y="1.5789" />
                    <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="93.0000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="game_chat_0.png" Plist="game/game.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="207.0000" Y="416.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1552" Y="0.5547" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1_0_0" ActionTag="-376292456" Tag="260" IconVisible="True" LeftMargin="1150.0000" RightMargin="184.0000" TopMargin="334.0000" BottomMargin="416.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="head_frame" ActionTag="1948565397" Tag="266" IconVisible="False" LeftMargin="-82.5000" RightMargin="-82.5000" TopMargin="-92.5000" BottomMargin="-92.5000" ctype="SpriteObjectData">
                <Size X="165.0000" Y="185.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="game_role32.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_bg" ActionTag="1492462570" Tag="261" IconVisible="False" LeftMargin="-55.9999" RightMargin="-56.0001" TopMargin="24.0000" BottomMargin="-80.0000" ctype="SpriteObjectData">
                <Size X="112.0000" Y="56.0000" />
                <Children>
                  <AbstractNodeData Name="name_text" ActionTag="198794399" Tag="262" IconVisible="False" LeftMargin="5.0387" RightMargin="6.9613" TopMargin="6.8918" BottomMargin="29.1082" IsCustomSize="True" FontSize="20" LabelText="" TouchScaleChangeAble="True" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="20.0000" />
                    <AnchorPoint />
                    <Position X="5.0387" Y="29.1082" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="105" B="20" />
                    <PrePosition X="0.0450" Y="0.5198" />
                    <PreSize X="0.8929" Y="0.3571" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="coin_text" ActionTag="-7559397" Tag="263" IconVisible="False" LeftMargin="20.5736" RightMargin="11.4264" TopMargin="29.4490" BottomMargin="6.5510" IsCustomSize="True" FontSize="12" LabelText="123456" TouchScaleChangeAble="True" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="80.0000" Y="20.0000" />
                    <AnchorPoint />
                    <Position X="20.5736" Y="6.5510" />
                    <Scale ScaleX="1.0950" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.1837" Y="0.1170" />
                    <PreSize X="0.7143" Y="0.3571" />
                    <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="0.0001" Y="-52.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="role_head_bg.png" Plist="game/game.plist" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chat_bg" ActionTag="1995350364" Tag="264" IconVisible="False" LeftMargin="-263.0000" RightMargin="93.0000" TopMargin="-80.0000" BottomMargin="40.0000" Scale9Enable="True" LeftEage="12" RightEage="55" TopEage="13" BottomEage="13" Scale9OriginX="12" Scale9OriginY="13" Scale9Width="103" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="170.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="chat_face" ActionTag="-1915690876" Tag="265" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="55.0000" TopMargin="-14.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                    <Size X="60.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="85.0000" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6000" />
                    <PreSize X="0.3529" Y="1.5000" />
                    <FileData Type="PlistSubImage" Path="land_blank.png" Plist="public_res/public_res.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="-93.0000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="PlistSubImage" Path="game_chat_1.png" Plist="game/game.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="1150.0000" Y="416.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8621" Y="0.5547" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_2" ActionTag="186357453" Tag="100" IconVisible="True" LeftMargin="725.0000" RightMargin="609.0000" TopMargin="600.0000" BottomMargin="150.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Panel_1_0" ActionTag="-135890548" Tag="111" IconVisible="False" LeftMargin="-472.5000" RightMargin="327.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-400.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_2_0" ActionTag="-1482740577" Tag="112" IconVisible="False" LeftMargin="-432.5000" RightMargin="287.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-360.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_3_0" ActionTag="-77871763" Tag="113" IconVisible="False" LeftMargin="-392.5000" RightMargin="247.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-320.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_4_0" ActionTag="-1979256025" Tag="114" IconVisible="False" LeftMargin="-352.5000" RightMargin="207.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-280.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_5_0" ActionTag="274621960" Tag="115" IconVisible="False" LeftMargin="-312.5000" RightMargin="167.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-240.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_6_0" ActionTag="1668374771" Tag="116" IconVisible="False" LeftMargin="-272.5000" RightMargin="127.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_7_0" ActionTag="219013353" Tag="117" IconVisible="False" LeftMargin="-232.5000" RightMargin="87.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-160.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_8_0" ActionTag="368834102" Tag="118" IconVisible="False" LeftMargin="-192.5000" RightMargin="47.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-120.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_9_0" ActionTag="824227248" Tag="119" IconVisible="False" LeftMargin="-152.5000" RightMargin="7.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-80.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_10_0" ActionTag="-1854428880" Tag="120" IconVisible="False" LeftMargin="-112.5000" RightMargin="-32.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_1" ActionTag="-914336490" Tag="101" IconVisible="False" LeftMargin="-72.5000" RightMargin="-72.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_2" ActionTag="1602986300" Tag="102" IconVisible="False" LeftMargin="-32.5000" RightMargin="-112.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_3" ActionTag="-1716220945" Tag="103" IconVisible="False" LeftMargin="7.5000" RightMargin="-152.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_4" ActionTag="-1751459825" Tag="104" IconVisible="False" LeftMargin="47.5000" RightMargin="-192.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_5" ActionTag="-115568037" Tag="105" IconVisible="False" LeftMargin="87.5000" RightMargin="-232.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_6" ActionTag="1176796257" Tag="106" IconVisible="False" LeftMargin="127.5000" RightMargin="-272.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_7" ActionTag="-1648572667" Tag="107" IconVisible="False" LeftMargin="167.5000" RightMargin="-312.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="240.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_8" ActionTag="-1813044054" Tag="108" IconVisible="False" LeftMargin="207.5000" RightMargin="-352.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="280.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_9" ActionTag="1683847968" Tag="109" IconVisible="False" LeftMargin="247.5000" RightMargin="-392.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_10" ActionTag="-840051673" Tag="110" IconVisible="False" LeftMargin="287.5000" RightMargin="-432.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="360.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="725.0000" Y="150.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5435" Y="0.2000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_3" ActionTag="1301052909" Tag="121" IconVisible="True" LeftMargin="320.0000" RightMargin="1014.0000" TopMargin="315.0000" BottomMargin="435.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Panel_11" ActionTag="-1409765025" Tag="139" IconVisible="False" LeftMargin="-72.5000" RightMargin="-72.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="320.0000" Y="435.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2399" Y="0.5800" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_3_0" ActionTag="-1773663946" Tag="122" IconVisible="True" PositionPercentYEnabled="True" LeftMargin="1014.0000" RightMargin="320.0000" TopMargin="315.0000" BottomMargin="435.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Panel_11_0" ActionTag="-1133556623" Tag="142" IconVisible="False" LeftMargin="-72.5000" RightMargin="-72.5000" TopMargin="-80.5000" BottomMargin="-80.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="145.0000" Y="161.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="1014.0000" Y="435.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7601" Y="0.5800" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_5" ActionTag="-1168983860" Tag="161" IconVisible="True" LeftMargin="667.0000" RightMargin="667.0000" TopMargin="30.0000" BottomMargin="720.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Panel_28" ActionTag="-322182374" Tag="162" IconVisible="False" LeftMargin="-20.0000" RightMargin="-20.0000" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="40.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_28_0" ActionTag="765654675" Tag="163" IconVisible="False" LeftMargin="30.0000" RightMargin="-70.0000" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="40.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_28_0_0" ActionTag="1581972104" Tag="164" IconVisible="False" LeftMargin="-70.0000" RightMargin="30.0000" TopMargin="-25.0000" BottomMargin="-25.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="40.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="667.0000" Y="720.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9600" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="PageView_1" ActionTag="290514781" Tag="128" IconVisible="False" LeftMargin="750.0000" RightMargin="186.0000" TopMargin="140.0000" BottomMargin="410.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="398.0000" Y="200.0000" />
            <AnchorPoint />
            <Position X="750.0000" Y="410.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5622" Y="0.5467" />
            <PreSize X="0.2984" Y="0.2667" />
            <SingleColor A="255" R="150" G="150" B="100" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="PageView_1_0" ActionTag="-1566862261" Tag="129" IconVisible="False" LeftMargin="115.8456" RightMargin="820.1544" TopMargin="370.9009" BottomMargin="179.0991" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="398.0000" Y="200.0000" />
            <AnchorPoint />
            <Position X="115.8456" Y="179.0991" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0868" Y="0.2388" />
            <PreSize X="0.2984" Y="0.2667" />
            <SingleColor A="255" R="150" G="150" B="100" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="PageView_1_1" ActionTag="1268192982" Tag="130" IconVisible="False" LeftMargin="210.0000" RightMargin="726.0000" TopMargin="140.0000" BottomMargin="410.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="398.0000" Y="200.0000" />
            <AnchorPoint />
            <Position X="210.0000" Y="410.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1574" Y="0.5467" />
            <PreSize X="0.2984" Y="0.2667" />
            <SingleColor A="255" R="150" G="150" B="100" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="game_alarm_01_1" ActionTag="-85047860" Tag="138" IconVisible="False" LeftMargin="262.5000" RightMargin="996.5000" TopMargin="369.0000" BottomMargin="319.0000" ctype="SpriteObjectData">
            <Size X="75.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="300.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2249" Y="0.4667" />
            <PreSize X="0.0562" Y="0.0827" />
            <FileData Type="PlistSubImage" Path="game_alarm_01.png" Plist="game/animation.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="game_alarm_01_1_0" ActionTag="909498428" Tag="140" IconVisible="False" LeftMargin="996.5000" RightMargin="262.5000" TopMargin="369.0000" BottomMargin="319.0000" ctype="SpriteObjectData">
            <Size X="75.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1034.0000" Y="350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7751" Y="0.4667" />
            <PreSize X="0.0562" Y="0.0827" />
            <FileData Type="PlistSubImage" Path="game_alarm_01.png" Plist="game/animation.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="game_alarm_01_1_0_0" ActionTag="1696412037" Tag="141" IconVisible="False" LeftMargin="182.5000" RightMargin="1076.5000" TopMargin="619.0000" BottomMargin="69.0000" ctype="SpriteObjectData">
            <Size X="75.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="220.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1649" Y="0.1333" />
            <PreSize X="0.0562" Y="0.0827" />
            <FileData Type="PlistSubImage" Path="game_alarm_01.png" Plist="game/animation.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>