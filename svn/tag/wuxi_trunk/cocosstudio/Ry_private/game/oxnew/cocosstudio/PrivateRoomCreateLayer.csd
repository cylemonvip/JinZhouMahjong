<GameFile>
  <PropertyGroup Name="PrivateRoomCreateLayer" Type="Layer" ID="f22823ea-44dc-480d-b5f4-e0afa3b8751b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskPanel" ActionTag="1157537826" Tag="130" IconVisible="False" TopMargin="2.1749" BottomMargin="-2.1749" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position Y="-2.1749" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="-0.0029" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BgImage" ActionTag="373231197" Tag="64" IconVisible="False" LeftMargin="121.3699" RightMargin="112.6301" TopMargin="93.7050" BottomMargin="106.2950" Scale9Enable="True" LeftEage="242" RightEage="242" TopEage="131" BottomEage="131" Scale9OriginX="242" Scale9OriginY="131" Scale9Width="250" Scale9Height="137" ctype="ImageViewObjectData">
            <Size X="1100.0000" Y="550.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="671.3699" Y="381.2950" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5033" Y="0.5084" />
            <PreSize X="0.8246" Y="0.7333" />
            <FileData Type="Normal" Path="room/27_plaza_common_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_help" Tag="88" IconVisible="False" LeftMargin="153.3907" RightMargin="1098.6093" TopMargin="125.1123" BottomMargin="542.8877" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="52" Scale9Height="60" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="82.0000" Y="82.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="194.3907" Y="583.8877" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1457" Y="0.7785" />
            <PreSize X="0.0615" Y="0.1093" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_help_0.png" Plist="room/27_pri_room.plist" />
            <PressedFileData Type="PlistSubImage" Path="27_pri_btn_help_1.png" Plist="room/27_pri_room.plist" />
            <NormalFileData Type="PlistSubImage" Path="27_pri_btn_help_0.png" Plist="room/27_pri_room.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="pri_sp_numbg" Tag="89" IconVisible="False" LeftMargin="473.6171" RightMargin="505.3829" TopMargin="128.5626" BottomMargin="570.4374" ctype="SpriteObjectData">
            <Size X="355.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="651.1171" Y="595.9374" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4881" Y="0.7946" />
            <PreSize X="0.2661" Y="0.0680" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_numbg.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pri_sp_cardbg_3" Tag="91" IconVisible="False" LeftMargin="484.5660" RightMargin="794.4340" TopMargin="135.0602" BottomMargin="576.9398" ctype="SpriteObjectData">
            <Size X="55.0000" Y="38.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="512.0660" Y="595.9398" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3839" Y="0.7946" />
            <PreSize X="0.0412" Y="0.0507" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_cardbg.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt_cardnum" Tag="92" IconVisible="False" LeftMargin="651.4628" RightMargin="682.5372" TopMargin="154.4734" BottomMargin="595.5266" FontSize="24" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="651.4628" Y="595.5266" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4884" Y="0.7940" />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_myroom" Tag="93" IconVisible="False" LeftMargin="845.2346" RightMargin="336.7654" TopMargin="131.8685" BottomMargin="576.1315" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="122" Scale9Height="20" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="152.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="921.2346" Y="597.1315" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6906" Y="0.7962" />
            <PreSize X="0.1139" Y="0.0560" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_myroom.png" Plist="room/27_pri_room.plist" />
            <NormalFileData Type="PlistSubImage" Path="27_pri_btn_myroom.png" Plist="room/27_pri_room.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="BankerModeLabel" Tag="95" IconVisible="False" LeftMargin="213.0489" RightMargin="1009.9511" TopMargin="273.8477" BottomMargin="447.1523" ctype="SpriteObjectData">
            <Size X="111.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="268.5489" Y="461.6523" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2013" Y="0.6155" />
            <PreSize X="0.0832" Y="0.0387" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_bankermod.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="modecheck_1" Tag="63" IconVisible="False" LeftMargin="339.5500" RightMargin="856.4500" TopMargin="271.3477" BottomMargin="444.6523" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="28" IconVisible="False" LeftMargin="43.0000" RightMargin="32.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="霸王庄" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.4565" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="339.5500" Y="461.6523" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2545" Y="0.6155" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="modecheck_2" Tag="64" IconVisible="False" LeftMargin="499.5481" RightMargin="696.4519" TopMargin="271.3477" BottomMargin="444.6523" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="29" IconVisible="False" LeftMargin="43.0000" RightMargin="12.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="传统叫庄" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="83.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.6014" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="568.5481" Y="461.6523" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4262" Y="0.6155" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="modecheck_3" Tag="65" IconVisible="False" LeftMargin="659.5488" RightMargin="536.4512" TopMargin="271.3477" BottomMargin="444.6523" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="30" IconVisible="False" LeftMargin="43.0000" RightMargin="32.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="随机庄" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="63.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.4565" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="728.5488" Y="461.6523" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5461" Y="0.6155" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="PersonLabel" Tag="43" IconVisible="False" LeftMargin="213.0486" RightMargin="1009.9514" TopMargin="343.8488" BottomMargin="377.1512" ctype="SpriteObjectData">
            <Size X="111.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="268.5486" Y="391.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2013" Y="0.5222" />
            <PreSize X="0.0832" Y="0.0387" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_player_count.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_check_1" Tag="75" IconVisible="False" LeftMargin="339.5488" RightMargin="856.4512" TopMargin="341.3488" BottomMargin="374.6512" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="83" IconVisible="False" LeftMargin="43.0000" RightMargin="61.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="2人" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="34.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.2464" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="408.5488" Y="391.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3063" Y="0.5222" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_check_2" Tag="76" IconVisible="False" LeftMargin="499.5481" RightMargin="696.4519" TopMargin="341.3488" BottomMargin="374.6512" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="84" IconVisible="False" LeftMargin="43.0000" RightMargin="61.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="3人" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="34.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.2464" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="568.5481" Y="391.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4262" Y="0.5222" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_check_3" Tag="77" IconVisible="False" LeftMargin="659.5488" RightMargin="536.4512" TopMargin="341.3488" BottomMargin="374.6512" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="85" IconVisible="False" LeftMargin="43.0000" RightMargin="61.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="4人" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="34.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.2464" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="728.5488" Y="391.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5461" Y="0.5222" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_check_4" Tag="78" IconVisible="False" LeftMargin="819.5483" RightMargin="376.4517" TopMargin="341.3488" BottomMargin="374.6512" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <Children>
              <AbstractNodeData Name="Txt" Tag="86" IconVisible="False" LeftMargin="43.0000" RightMargin="38.0000" TopMargin="3.0000" BottomMargin="5.0000" FontSize="20" LabelText="2-4人" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="57.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="43.0000" Y="18.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3116" Y="0.5294" />
                <PreSize X="0.4130" Y="0.7647" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="888.5483" Y="391.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6661" Y="0.5222" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="GameCountLabel" Tag="94" IconVisible="False" LeftMargin="213.0486" RightMargin="1009.9514" TopMargin="413.8491" BottomMargin="307.1509" ctype="SpriteObjectData">
            <Size X="111.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="268.5486" Y="321.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2013" Y="0.4289" />
            <PreSize X="0.0832" Y="0.0387" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_count.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt_selcount" Tag="100" IconVisible="False" LeftMargin="1166.5502" RightMargin="167.4498" TopMargin="303.3485" BottomMargin="446.6515" FontSize="30" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1166.5502" Y="446.6515" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8745" Y="0.5955" />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="check_1" VisibleForFrame="False" Tag="58" IconVisible="False" LeftMargin="339.5488" RightMargin="856.4512" TopMargin="411.3491" BottomMargin="304.6509" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="408.5488" Y="321.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3063" Y="0.4289" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="count_1" Tag="63" IconVisible="False" LeftMargin="388.5486" RightMargin="945.4514" TopMargin="428.3488" BottomMargin="321.6512" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="388.5486" Y="321.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2913" Y="0.4289" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="check_2" VisibleForFrame="False" Tag="59" IconVisible="False" LeftMargin="499.5481" RightMargin="696.4519" TopMargin="411.3491" BottomMargin="304.6509" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="568.5481" Y="321.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4262" Y="0.4289" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="count_2" Tag="64" IconVisible="False" LeftMargin="548.5492" RightMargin="785.4508" TopMargin="428.3488" BottomMargin="321.6512" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="548.5492" Y="321.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4112" Y="0.4289" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="check_3" VisibleForFrame="False" Tag="60" IconVisible="False" LeftMargin="659.5488" RightMargin="536.4512" TopMargin="411.3491" BottomMargin="304.6509" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="728.5488" Y="321.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5461" Y="0.4289" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="count_3" Tag="65" IconVisible="False" LeftMargin="708.5492" RightMargin="625.4508" TopMargin="428.3488" BottomMargin="321.6512" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="708.5492" Y="321.6512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5311" Y="0.4289" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="check_4" VisibleForFrame="False" Tag="61" IconVisible="False" LeftMargin="819.5483" RightMargin="376.4517" TopMargin="411.3491" BottomMargin="304.6509" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="888.5483" Y="321.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6661" Y="0.4289" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="count_4" Tag="66" IconVisible="False" LeftMargin="868.5490" RightMargin="465.4510" TopMargin="428.3491" BottomMargin="321.6509" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="868.5490" Y="321.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6511" Y="0.4289" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="check_5" VisibleForFrame="False" Tag="62" IconVisible="False" LeftMargin="979.5518" RightMargin="216.4482" TopMargin="411.3492" BottomMargin="304.6508" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="138.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1048.5518" Y="321.6508" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7860" Y="0.4289" />
            <PreSize X="0.1034" Y="0.0453" />
            <NormalBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <PressedBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <DisableBackFileData Type="PlistSubImage" Path="27_pri_check_dot_1.png" Plist="room/27_pri_room.plist" />
            <NodeNormalFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
            <NodeDisableFileData Type="PlistSubImage" Path="27_pri_check_dot_0.png" Plist="room/27_pri_room.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="count_5" Tag="67" IconVisible="False" LeftMargin="1028.5474" RightMargin="305.4526" TopMargin="428.3492" BottomMargin="321.6508" FontSize="20" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1028.5474" Y="321.6508" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7710" Y="0.4289" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="PayCountLabel" Tag="96" IconVisible="False" LeftMargin="212.5487" RightMargin="1009.4513" TopMargin="483.8491" BottomMargin="237.1509" ctype="SpriteObjectData">
            <Size X="112.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="268.5487" Y="251.6509" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2013" Y="0.3355" />
            <PreSize X="0.0840" Y="0.0387" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_fee.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt_fee" Tag="22" IconVisible="False" LeftMargin="355.2234" RightMargin="978.7766" TopMargin="500.2016" BottomMargin="249.7984" FontSize="30" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="355.2234" Y="249.7984" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.2663" Y="0.3331" />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_createroom" Tag="17" IconVisible="False" LeftMargin="549.7956" RightMargin="581.2044" TopMargin="535.1064" BottomMargin="144.8936" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="173" Scale9Height="48" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="203.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="651.2956" Y="179.8936" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4882" Y="0.2399" />
            <PreSize X="0.1522" Y="0.0933" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_createroom_0.png" Plist="room/27_pri_room.plist" />
            <PressedFileData Type="PlistSubImage" Path="27_pri_btn_createroom_1.png" Plist="room/27_pri_room.plist" />
            <NormalFileData Type="PlistSubImage" Path="27_pri_btn_createroom_0.png" Plist="room/27_pri_room.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="priland_sp_card_tips" VisibleForFrame="False" Tag="46" IconVisible="False" LeftMargin="506.6808" RightMargin="532.3192" TopMargin="505.7864" BottomMargin="213.2136" ctype="SpriteObjectData">
            <Size X="295.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="654.1808" Y="228.7136" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4904" Y="0.3050" />
            <PreSize X="0.2211" Y="0.0413" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_card_tips.png" Plist="room/27_pri_room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ExitBtn" ActionTag="-1372136826" Tag="129" IconVisible="False" LeftMargin="1168.4590" RightMargin="94.5410" TopMargin="69.9864" BottomMargin="610.0136" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="41" Scale9Height="48" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="71.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1203.9590" Y="645.0136" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9025" Y="0.8600" />
            <PreSize X="0.0532" Y="0.0933" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="room/27_cr_close.png" Plist="" />
            <PressedFileData Type="Normal" Path="room/27_cr_close.png" Plist="" />
            <NormalFileData Type="Normal" Path="room/27_cr_close.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>