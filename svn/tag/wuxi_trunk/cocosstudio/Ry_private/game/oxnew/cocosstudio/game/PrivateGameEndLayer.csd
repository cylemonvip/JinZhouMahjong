<GameFile>
  <PropertyGroup Name="PrivateGameEndLayer" Type="Layer" ID="3d8df46d-766a-4973-be79-f320717176f8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" Tag="98" IconVisible="False" LeftMargin="-1.4636" RightMargin="1.4636" TopMargin="2.3978" BottomMargin="-2.3978" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="0.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position X="-1.4636" Y="-2.3978" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0011" Y="-0.0032" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleX="1.0000" ScaleY="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_bg" Tag="97" IconVisible="False" LeftMargin="356.5000" RightMargin="356.5000" TopMargin="47.0000" BottomMargin="47.0000" ctype="SpriteObjectData">
            <Size X="621.0000" Y="656.0000" />
            <Children>
              <AbstractNodeData Name="im_score_cell_1" Tag="123" IconVisible="False" LeftMargin="58.5004" RightMargin="18.4996" TopMargin="137.4998" BottomMargin="461.5002" ctype="SpriteObjectData">
                <Size X="544.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="sp_gameres" Tag="131" IconVisible="False" LeftMargin="409.5000" RightMargin="33.5000" TopMargin="5.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                    <Size X="101.0000" Y="47.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="460.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8456" Y="0.5000" />
                    <PreSize X="0.1857" Y="0.8246" />
                    <FileData Type="PlistSubImage" Path="27_pri_txt_win.png" Plist="game/27_pri_game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="atlas_score" Tag="86" IconVisible="False" LeftMargin="315.0000" RightMargin="229.0000" TopMargin="28.5000" BottomMargin="28.5000" CharWidth="15" CharHeight="23" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="315.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5790" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/27_pri_atlas_addscore.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5004" Y="490.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5322" Y="0.7470" />
                <PreSize X="0.8760" Y="0.0869" />
                <FileData Type="PlistSubImage" Path="27_pri_sp_gray.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="im_score_cell_2" Tag="124" IconVisible="False" LeftMargin="58.5004" RightMargin="18.4996" TopMargin="201.4998" BottomMargin="397.5002" ctype="SpriteObjectData">
                <Size X="544.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="sp_gameres" VisibleForFrame="False" Tag="214" IconVisible="False" LeftMargin="410.0000" RightMargin="34.0000" TopMargin="5.5000" BottomMargin="5.5000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="460.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8456" Y="0.5000" />
                    <PreSize X="0.1838" Y="0.8070" />
                    <FileData Type="PlistSubImage" Path="27_pri_txt_failed.png" Plist="game/27_pri_game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="atlas_score" Tag="85" IconVisible="False" LeftMargin="315.0000" RightMargin="229.0000" TopMargin="28.5000" BottomMargin="28.5000" CharWidth="15" CharHeight="23" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="315.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5790" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/27_pri_atlas_subscore.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5004" Y="426.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5322" Y="0.6494" />
                <PreSize X="0.8760" Y="0.0869" />
                <FileData Type="PlistSubImage" Path="27_pri_sp_gray.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="im_score_cell_3" Tag="125" IconVisible="False" LeftMargin="58.5004" RightMargin="18.4996" TopMargin="265.4999" BottomMargin="333.5001" ctype="SpriteObjectData">
                <Size X="544.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="sp_gameres" VisibleForFrame="False" Tag="212" IconVisible="False" LeftMargin="410.0000" RightMargin="34.0000" TopMargin="5.5000" BottomMargin="5.5000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="460.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8456" Y="0.5000" />
                    <PreSize X="0.1838" Y="0.8070" />
                    <FileData Type="PlistSubImage" Path="27_pri_txt_failed.png" Plist="game/27_pri_game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="atlas_score" Tag="84" IconVisible="False" LeftMargin="315.0000" RightMargin="229.0000" TopMargin="28.5000" BottomMargin="28.5000" CharWidth="15" CharHeight="23" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="315.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5790" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/27_pri_atlas_addscore.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5004" Y="362.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5322" Y="0.5518" />
                <PreSize X="0.8760" Y="0.0869" />
                <FileData Type="PlistSubImage" Path="27_pri_sp_gray.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="im_score_cell_4" Tag="80" IconVisible="False" LeftMargin="58.5004" RightMargin="18.4996" TopMargin="329.4999" BottomMargin="269.5001" ctype="SpriteObjectData">
                <Size X="544.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="sp_gameres" VisibleForFrame="False" Tag="82" IconVisible="False" LeftMargin="410.0000" RightMargin="34.0000" TopMargin="5.5000" BottomMargin="5.5000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="460.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8456" Y="0.5000" />
                    <PreSize X="0.1838" Y="0.8070" />
                    <FileData Type="PlistSubImage" Path="27_pri_txt_failed.png" Plist="game/27_pri_game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="atlas_score" Tag="83" IconVisible="False" LeftMargin="315.0000" RightMargin="229.0000" TopMargin="28.5000" BottomMargin="28.5000" CharWidth="15" CharHeight="23" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="315.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5790" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/27_pri_atlas_addscore.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5004" Y="298.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5322" Y="0.4543" />
                <PreSize X="0.8760" Y="0.0869" />
                <FileData Type="PlistSubImage" Path="27_pri_sp_gray.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="im_score_cell_5" ActionTag="668562509" Tag="18" IconVisible="False" LeftMargin="58.5004" RightMargin="18.4996" TopMargin="393.4999" BottomMargin="205.5001" ctype="SpriteObjectData">
                <Size X="544.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="sp_gameres" ActionTag="-663941539" VisibleForFrame="False" Tag="19" IconVisible="False" LeftMargin="410.0000" RightMargin="34.0000" TopMargin="5.5000" BottomMargin="5.5000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="460.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8456" Y="0.5000" />
                    <PreSize X="0.1838" Y="0.8070" />
                    <FileData Type="PlistSubImage" Path="27_pri_txt_failed.png" Plist="game/27_pri_game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="atlas_score" ActionTag="-466678691" Tag="20" IconVisible="False" LeftMargin="315.0000" RightMargin="229.0000" TopMargin="28.5000" BottomMargin="28.5000" CharWidth="15" CharHeight="23" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="315.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5790" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/27_pri_atlas_addscore.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5004" Y="234.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5322" Y="0.3567" />
                <PreSize X="0.8760" Y="0.0869" />
                <FileData Type="PlistSubImage" Path="27_pri_sp_gray.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="im_score_cell_6" ActionTag="191225177" Tag="21" IconVisible="False" LeftMargin="58.5002" RightMargin="18.4998" TopMargin="457.4999" BottomMargin="141.5001" ctype="SpriteObjectData">
                <Size X="544.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="sp_gameres" ActionTag="-741733752" VisibleForFrame="False" Tag="22" IconVisible="False" LeftMargin="410.0000" RightMargin="34.0000" TopMargin="5.5000" BottomMargin="5.5000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="460.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8456" Y="0.5000" />
                    <PreSize X="0.1838" Y="0.8070" />
                    <FileData Type="PlistSubImage" Path="27_pri_txt_failed.png" Plist="game/27_pri_game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="atlas_score" ActionTag="-982869640" Tag="23" IconVisible="False" LeftMargin="315.0000" RightMargin="229.0000" TopMargin="28.5000" BottomMargin="28.5000" CharWidth="15" CharHeight="23" LabelText="" StartChar="." ctype="TextAtlasObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="315.0000" Y="28.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5790" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <LabelAtlasFileImage_CNB Type="Normal" Path="game/27_pri_atlas_addscore.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5002" Y="170.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5322" Y="0.2591" />
                <PreSize X="0.8760" Y="0.0869" />
                <FileData Type="PlistSubImage" Path="27_pri_sp_gray.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_share" Tag="99" IconVisible="False" LeftMargin="70.5364" RightMargin="358.4636" TopMargin="521.0000" BottomMargin="57.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="56" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="192.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="166.5364" Y="96.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2682" Y="0.1463" />
                <PreSize X="0.3092" Y="0.1189" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_share_0.png" Plist="game/27_pri_game.plist" />
                <PressedFileData Type="PlistSubImage" Path="27_pri_btn_share_1.png" Plist="game/27_pri_game.plist" />
                <NormalFileData Type="PlistSubImage" Path="27_pri_btn_share_0.png" Plist="game/27_pri_game.plist" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_quit" Tag="100" IconVisible="False" LeftMargin="328.2651" RightMargin="100.7349" TopMargin="521.0000" BottomMargin="57.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="56" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="192.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="424.2651" Y="96.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6832" Y="0.1463" />
                <PreSize X="0.3092" Y="0.1189" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_quit_0.png" Plist="game/27_pri_game.plist" />
                <PressedFileData Type="PlistSubImage" Path="27_pri_btn_quit_1.png" Plist="game/27_pri_game.plist" />
                <NormalFileData Type="PlistSubImage" Path="27_pri_btn_quit_0.png" Plist="game/27_pri_game.plist" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.4655" Y="0.8747" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_end.png" Plist="game/27_pri_game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>