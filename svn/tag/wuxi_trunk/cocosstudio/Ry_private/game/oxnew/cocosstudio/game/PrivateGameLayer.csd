<GameFile>
  <PropertyGroup Name="PrivateGameLayer" Type="Layer" ID="cb74177e-38e7-4105-87af-0269cd5d274d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Image_bg" Tag="73" IconVisible="False" LeftMargin="-6.0000" RightMargin="1072.0000" BottomMargin="598.0000" Scale9Width="268" Scale9Height="152" ctype="ImageViewObjectData">
            <Size X="268.0000" Y="152.0000" />
            <Children>
              <AbstractNodeData Name="im_room_icon" Tag="79" IconVisible="False" LeftMargin="70.5000" RightMargin="176.5000" TopMargin="36.5000" BottomMargin="94.5000" ctype="SpriteObjectData">
                <Size X="21.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="81.0000" Y="105.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3022" Y="0.6908" />
                <PreSize X="0.0784" Y="0.1382" />
                <FileData Type="PlistSubImage" Path="27_pri_im_room_icon.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_room" Tag="80" IconVisible="False" LeftMargin="178.9999" RightMargin="53.0001" TopMargin="36.5001" BottomMargin="94.4999" ctype="SpriteObjectData">
                <Size X="36.0000" Y="21.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="196.9999" Y="104.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7351" Y="0.6908" />
                <PreSize X="0.1343" Y="0.1382" />
                <FileData Type="PlistSubImage" Path="27_pri_txt_room.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_count" Tag="78" IconVisible="False" LeftMargin="25.8072" RightMargin="194.1928" TopMargin="75.4997" BottomMargin="55.5003" ctype="SpriteObjectData">
                <Size X="48.0000" Y="21.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="73.8072" Y="66.0003" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2754" Y="0.4342" />
                <PreSize X="0.1791" Y="0.1382" />
                <FileData Type="PlistSubImage" Path="27_pri_txt_count.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_count_0" Tag="82" IconVisible="False" LeftMargin="129.2028" RightMargin="118.7972" TopMargin="75.9996" BottomMargin="56.0004" ctype="SpriteObjectData">
                <Size X="20.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="129.2028" Y="66.0004" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4821" Y="0.4342" />
                <PreSize X="0.0746" Y="0.1316" />
                <FileData Type="PlistSubImage" Path="27_pri_txt_count_0.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sp_bankermode" Tag="64" IconVisible="False" LeftMargin="162.5344" RightMargin="30.4656" TopMargin="74.5000" BottomMargin="54.5000" ctype="SpriteObjectData">
                <Size X="75.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="200.0344" Y="66.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7464" Y="0.4342" />
                <PreSize X="0.2799" Y="0.1513" />
                <FileData Type="PlistSubImage" Path="27_pri_txt_mode12.png" Plist="game/27_pri_game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="num_roomID" ActionTag="661436223" Tag="81" IconVisible="False" LeftMargin="101.5827" RightMargin="95.4173" TopMargin="34.2835" BottomMargin="91.7165" FontSize="20" LabelText="000000" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="71.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="137.0827" Y="104.7165" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="236" G="217" B="142" />
                <PrePosition X="0.5115" Y="0.6889" />
                <PreSize X="0.2649" Y="0.1711" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="num_count" ActionTag="661685038" Tag="54" IconVisible="False" LeftMargin="85.9459" RightMargin="147.0541" TopMargin="72.3625" BottomMargin="53.6375" FontSize="20" LabelText="0/0" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="35.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.4459" Y="66.6375" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="226" G="202" B="122" />
                <PrePosition X="0.3860" Y="0.4384" />
                <PreSize X="0.1306" Y="0.1711" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_dismiss" Tag="77" IconVisible="False" LeftMargin="37.4000" RightMargin="144.6000" TopMargin="105.0000" BottomMargin="13.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="56" Scale9Height="12" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="86.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.4000" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3000" Y="0.1974" />
                <PreSize X="0.3209" Y="0.2237" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_dismiss_0.png" Plist="game/27_pri_game.plist" />
                <PressedFileData Type="PlistSubImage" Path="27_pri_btn_dismiss_1.png" Plist="game/27_pri_game.plist" />
                <NormalFileData Type="PlistSubImage" Path="27_pri_btn_dismiss_0.png" Plist="game/27_pri_game.plist" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="bt_zanli" Tag="98" IconVisible="False" LeftMargin="144.6000" RightMargin="37.4000" TopMargin="105.0000" BottomMargin="13.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="56" Scale9Height="12" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="86.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="187.6000" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7000" Y="0.1974" />
                <PreSize X="0.3209" Y="0.2237" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_leave_0.png" Plist="game/27_pri_game.plist" />
                <PressedFileData Type="PlistSubImage" Path="27_pri_btn_leave_1.png" Plist="game/27_pri_game.plist" />
                <NormalFileData Type="PlistSubImage" Path="27_pri_btn_leave_0.png" Plist="game/27_pri_game.plist" />
                <OutlineColor A="255" R="0" G="63" B="198" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="128.0000" Y="750.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0960" Y="1.0000" />
            <PreSize X="0.2009" Y="0.2027" />
            <FileData Type="PlistSubImage" Path="27_pri_sp_bg.png" Plist="game/27_pri_game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_invite" Tag="76" IconVisible="False" LeftMargin="469.0000" RightMargin="469.0000" TopMargin="330.5000" BottomMargin="330.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="366" Scale9Height="67" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
            <Size X="396.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.2969" Y="0.1187" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="27_pri_btn_invite_0.png" Plist="game/27_pri_game.plist" />
            <PressedFileData Type="PlistSubImage" Path="27_pri_btn_invite_1.png" Plist="game/27_pri_game.plist" />
            <NormalFileData Type="PlistSubImage" Path="27_pri_btn_invite_0.png" Plist="game/27_pri_game.plist" />
            <OutlineColor A="255" R="0" G="63" B="198" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>