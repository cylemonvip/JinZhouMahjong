<GameFile>
  <PropertyGroup Name="RoomResultLayer" Type="Scene" ID="283709be-02a2-4d04-bd17-d2f2c4d92047" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="65" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-143788950" Tag="749" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_roomResult_bg" ActionTag="-1930470121" Tag="66" IconVisible="False" LeftMargin="138.5000" RightMargin="138.5000" TopMargin="122.0000" BottomMargin="136.0000" ctype="SpriteObjectData">
            <Size X="1057.0000" Y="492.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="382.0000" />
            <Scale ScaleX="1.2000" ScaleY="1.1700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5093" />
            <PreSize X="0.7924" Y="0.6560" />
            <FileData Type="Normal" Path="plist/game_image_gameend_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TitleSp" ActionTag="1518445823" Tag="234" IconVisible="False" LeftMargin="518.5000" RightMargin="518.5000" TopMargin="49.6705" BottomMargin="645.3295" ctype="SpriteObjectData">
            <Size X="297.0000" Y="55.0000" />
            <Children>
              <AbstractNodeData Name="TitleTxt" ActionTag="1769504935" Tag="235" IconVisible="False" LeftMargin="61.0000" RightMargin="61.0000" TopMargin="-1.0000" BottomMargin="4.0000" ctype="SpriteObjectData">
                <Size X="175.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="148.5000" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5455" />
                <PreSize X="0.5892" Y="0.9455" />
                <FileData Type="PlistSubImage" Path="gameend_account_image_title.png" Plist="plist/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="672.8295" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8971" />
            <PreSize X="0.2226" Y="0.0733" />
            <FileData Type="PlistSubImage" Path="game_iamge_end_title_bg.png" Plist="plist/plazaScene.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_leaveRoom" ActionTag="700991358" Tag="69" IconVisible="False" LeftMargin="1240.5608" RightMargin="21.4392" TopMargin="4.0037" BottomMargin="671.9963" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="42" Scale9Height="52" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="72.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1276.5608" Y="708.9963" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9569" Y="0.9453" />
            <PreSize X="0.0540" Y="0.0987" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="gameend_account_btn_exit_press.png" Plist="plist/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="gameend_account_btn_exit_press.png" Plist="plist/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="gameend_account_btn_exit.png" Plist="plist/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_share" ActionTag="1873516002" Tag="70" IconVisible="False" LeftMargin="615.4999" RightMargin="615.5001" TopMargin="678.0001" BottomMargin="31.9999" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="73" Scale9Height="18" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="103.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="666.9999" Y="51.9999" />
            <Scale ScaleX="1.8000" ScaleY="1.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0693" />
            <PreSize X="0.0772" Y="0.0533" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="game_end_btn_share_press.png" Plist="plist/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="game_end_btn_share_press.png" Plist="plist/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="game_end_btn_share.png" Plist="plist/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_1" ActionTag="-1084553291" Tag="73" IconVisible="True" LeftMargin="211.9971" RightMargin="1122.0029" TopMargin="339.9964" BottomMargin="410.0036" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="211.9971" Y="410.0036" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1589" Y="0.5467" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserData.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_2" ActionTag="2085352986" Tag="132" IconVisible="True" LeftMargin="514.6422" RightMargin="819.3578" TopMargin="339.9961" BottomMargin="410.0039" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="514.6422" Y="410.0039" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3858" Y="0.5467" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserData.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_3" ActionTag="1742288563" Tag="145" IconVisible="True" LeftMargin="817.2876" RightMargin="516.7124" TopMargin="339.9961" BottomMargin="410.0039" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="817.2876" Y="410.0039" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6127" Y="0.5467" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserData.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="FileNode_4" ActionTag="-1629111249" Tag="158" IconVisible="True" LeftMargin="1119.9326" RightMargin="214.0674" TopMargin="339.9964" BottomMargin="410.0036" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1119.9326" Y="410.0036" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8395" Y="0.5467" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="privateRoom/nodeUserData.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>