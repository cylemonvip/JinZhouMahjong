<GameFile>
  <PropertyGroup Name="RoomCardLayer" Type="Scene" ID="8cf7ce17-31cc-40dc-99df-ba76820ac081" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="4" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Mask" ActionTag="-1799895735" Tag="217" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="440" RightEage="440" TopEage="247" BottomEage="247" Scale9OriginX="-440" Scale9OriginY="-247" Scale9Width="880" Scale9Height="494" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_6" ActionTag="595002173" Tag="125" IconVisible="False" LeftMargin="171.0000" RightMargin="171.0000" TopMargin="75.5000" BottomMargin="75.5000" ctype="SpriteObjectData">
            <Size X="992.0000" Y="599.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7436" Y="0.7987" />
            <FileData Type="Normal" Path="wxpri_pic/wx_cr_popupbg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_JuShu_1" ActionTag="1157371440" Tag="70" IconVisible="False" LeftMargin="400.7430" RightMargin="907.2571" TopMargin="202.3127" BottomMargin="521.6873" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Bg" ActionTag="-603391506" Tag="116" IconVisible="False" LeftMargin="70.0000" RightMargin="-144.0000" TopMargin="-2.0000" BottomMargin="-2.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="70.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.6923" Y="0.5000" />
                <PreSize X="3.8462" Y="1.1538" />
                <FileData Type="Normal" Path="wxpri_pic/wx_cr_costroombg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="CostTxt" ActionTag="1786805401" Tag="119" IconVisible="False" LeftMargin="89.0001" RightMargin="-125.0001" TopMargin="1.5000" BottomMargin="1.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.0001" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="4.6154" Y="0.5000" />
                <PreSize X="2.3846" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_Ju" ActionTag="-460425027" Tag="122" IconVisible="False" LeftMargin="27.5000" RightMargin="-44.5000" TopMargin="1.5000" BottomMargin="1.5000" FontSize="20" LabelText="99局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="43.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.8846" Y="0.5000" />
                <PreSize X="1.6538" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="413.7430" Y="534.6873" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3102" Y="0.7129" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_JuShu_2" ActionTag="1479050148" Tag="9" IconVisible="False" LeftMargin="610.7170" RightMargin="697.2830" TopMargin="202.3090" BottomMargin="521.6910" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Bg" ActionTag="1357774558" Tag="117" IconVisible="False" LeftMargin="70.0000" RightMargin="-144.0000" TopMargin="-2.0000" BottomMargin="-2.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="70.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.6923" Y="0.5000" />
                <PreSize X="3.8462" Y="1.1538" />
                <FileData Type="Normal" Path="wxpri_pic/wx_cr_costroombg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="CostTxt" ActionTag="-1053207319" Tag="120" IconVisible="False" LeftMargin="89.0000" RightMargin="-125.0000" TopMargin="1.5000" BottomMargin="1.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="4.6154" Y="0.5000" />
                <PreSize X="2.3846" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_Ju" ActionTag="-969513046" Tag="123" IconVisible="False" LeftMargin="27.5000" RightMargin="-44.5000" TopMargin="1.5000" BottomMargin="1.5000" FontSize="20" LabelText="99局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="43.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.8846" Y="0.5000" />
                <PreSize X="1.6538" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7170" Y="534.6910" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.7129" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_JuShu_3" ActionTag="303849819" Tag="10" IconVisible="False" LeftMargin="820.6926" RightMargin="487.3074" TopMargin="202.3127" BottomMargin="521.6873" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Bg" ActionTag="-428527411" Tag="118" IconVisible="False" LeftMargin="70.0000" RightMargin="-144.0000" TopMargin="-2.0000" BottomMargin="-2.0000" ctype="SpriteObjectData">
                <Size X="100.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="70.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.6923" Y="0.5000" />
                <PreSize X="3.8462" Y="1.1538" />
                <FileData Type="Normal" Path="wxpri_pic/wx_cr_costroombg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="CostTxt" ActionTag="-1475470532" Tag="121" IconVisible="False" LeftMargin="89.0000" RightMargin="-125.0000" TopMargin="1.5000" BottomMargin="1.5000" FontSize="20" LabelText="房卡x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="4.6154" Y="0.5000" />
                <PreSize X="2.3846" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Txt_Ju" ActionTag="846786179" Tag="124" IconVisible="False" LeftMargin="27.5000" RightMargin="-44.5000" TopMargin="1.5000" BottomMargin="1.5000" FontSize="20" LabelText="99局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="43.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.8846" Y="0.5000" />
                <PreSize X="1.6538" Y="0.8846" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="833.6926" Y="534.6873" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6250" Y="0.7129" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_RenShu_1" ActionTag="1042196030" Tag="13" IconVisible="False" LeftMargin="400.7430" RightMargin="907.2570" TopMargin="314.8263" BottomMargin="409.1737" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Text_siren" ActionTag="801015102" Tag="143" IconVisible="False" LeftMargin="40.0000" RightMargin="-55.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="4人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="1.5769" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="413.7430" Y="422.1737" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3102" Y="0.5629" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_RenShu_2" ActionTag="698248547" Tag="14" IconVisible="False" LeftMargin="610.7167" RightMargin="697.2833" TopMargin="314.8263" BottomMargin="409.1737" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Text_sanren" ActionTag="307289278" Tag="144" IconVisible="False" LeftMargin="40.0000" RightMargin="-55.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="3人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="1.5769" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7167" Y="422.1737" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.5629" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_1" ActionTag="-719757651" Tag="16" IconVisible="False" LeftMargin="400.7431" RightMargin="907.2570" TopMargin="374.8445" BottomMargin="349.1555" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-1625554026" Tag="142" IconVisible="False" LeftMargin="40.0000" RightMargin="-66.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="可飘" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="52.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="2.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="413.7431" Y="362.1555" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3102" Y="0.4829" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_2" ActionTag="-1530266810" Tag="17" IconVisible="False" LeftMargin="400.7431" RightMargin="907.2569" TopMargin="455.3397" BottomMargin="268.6603" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="1257413245" Tag="141" IconVisible="False" LeftMargin="40.0000" RightMargin="-92.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="不可飘" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="3.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5198" ScaleY="0.4395" />
            <Position X="414.2589" Y="280.0874" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3105" Y="0.3734" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_3" ActionTag="1016970038" Tag="22" IconVisible="False" LeftMargin="820.6927" RightMargin="487.3073" TopMargin="390.8450" BottomMargin="333.1550" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="1218649640" Tag="139" IconVisible="False" LeftMargin="40.0000" RightMargin="-118.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="摸完黄庄" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="104.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="4.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="833.6927" Y="346.1550" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6250" Y="0.4615" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_4" ActionTag="744135654" Tag="20" IconVisible="False" LeftMargin="820.6927" RightMargin="487.3073" TopMargin="455.3398" BottomMargin="268.6602" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="1180590825" Tag="140" IconVisible="False" LeftMargin="40.0000" RightMargin="-144.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="剩八墩黄庄" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="5.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="833.6927" Y="281.6602" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6250" Y="0.3755" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_5" ActionTag="-1158803983" Tag="24" IconVisible="False" LeftMargin="610.7167" RightMargin="697.2833" TopMargin="390.8450" BottomMargin="333.1550" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-1116826458" Tag="138" IconVisible="False" LeftMargin="40.0000" RightMargin="-66.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="升级" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="52.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="2.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7167" Y="346.1550" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.4615" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_6" ActionTag="-446987659" Tag="136" IconVisible="False" LeftMargin="610.7167" RightMargin="697.2833" TopMargin="455.3398" BottomMargin="268.6602" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-512133990" Tag="137" IconVisible="False" LeftMargin="40.0000" RightMargin="-92.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="不升级" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="3.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7167" Y="281.6602" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.3755" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_8" ActionTag="-1496270662" Tag="142" IconVisible="False" LeftMargin="610.7167" RightMargin="697.2833" TopMargin="511.3655" BottomMargin="212.6345" TouchEnable="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-761800183" Tag="143" IconVisible="False" LeftMargin="40.0000" RightMargin="-92.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="四花升" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="3.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.7167" Y="225.6345" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4676" Y="0.3008" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="CB_WanFa_7" ActionTag="1795628264" Tag="140" IconVisible="False" LeftMargin="400.7430" RightMargin="907.2570" TopMargin="511.3655" BottomMargin="212.6345" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="26.0000" Y="26.0000" />
            <Children>
              <AbstractNodeData Name="Txt" ActionTag="-1614383120" Tag="128" IconVisible="False" LeftMargin="40.0000" RightMargin="-92.0000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="26" LabelText="三花升" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="78.0000" Y="31.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.0000" Y="13.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="110" B="16" />
                <PrePosition X="1.5385" Y="0.5000" />
                <PreSize X="3.0000" Y="1.1923" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="413.7430" Y="225.6345" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3102" Y="0.3008" />
            <PreSize X="0.0195" Y="0.0347" />
            <NormalBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <PressedBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <DisableBackFileData Type="Normal" Path="wxpri_pic/wx_cr_checkboxbg.png" Plist="" />
            <NodeNormalFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
            <NodeDisableFileData Type="Normal" Path="wxpri_pic/wx_cr_checkpoint.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_CreatRoom" ActionTag="1451577220" Tag="25" IconVisible="False" LeftMargin="912.7369" RightMargin="203.2632" TopMargin="572.9682" BottomMargin="97.0318" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="188" Scale9Height="58" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="218.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1021.7369" Y="137.0318" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7659" Y="0.1827" />
            <PreSize X="0.1634" Y="0.1067" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="wxpri_pic/wx_cr_createtbn_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="wxpri_pic/wx_cr_createtbn_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="wxpri_pic/wx_cr_createtbn_normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_MyRoom" ActionTag="-1243900178" VisibleForFrame="False" Tag="26" IconVisible="False" LeftMargin="734.6952" RightMargin="343.3048" TopMargin="574.0070" BottomMargin="84.9930" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="226" Scale9Height="69" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="256.0000" Y="91.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="862.6952" Y="130.4930" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6467" Y="0.1740" />
            <PreSize X="0.1919" Y="0.1213" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="cr_myroom_p.png" Plist="plist/creatroompics.plist" />
            <PressedFileData Type="PlistSubImage" Path="cr_myroom_p.png" Plist="plist/creatroompics.plist" />
            <NormalFileData Type="PlistSubImage" Path="cr_myroom.png" Plist="plist/creatroompics.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_Close" ActionTag="-215187022" Tag="25" IconVisible="False" LeftMargin="1107.0586" RightMargin="178.9414" TopMargin="79.4021" BottomMargin="621.5979" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="18" Scale9Height="27" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="48.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1131.0586" Y="646.0979" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8479" Y="0.8615" />
            <PreSize X="0.0360" Y="0.0653" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="wxpri_pic/wx_cr_closebtn_press.png" Plist="" />
            <PressedFileData Type="Normal" Path="wxpri_pic/wx_cr_closebtn_press.png" Plist="" />
            <NormalFileData Type="Normal" Path="wxpri_pic/wx_cr_closebtn_normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Btn_ChongZhi" ActionTag="1283950031" VisibleForFrame="False" Tag="36" IconVisible="False" LeftMargin="729.5000" RightMargin="489.5000" TopMargin="25.5000" BottomMargin="671.5000" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="85" Scale9Height="31" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="115.0000" Y="53.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="787.0000" Y="698.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5900" Y="0.9307" />
            <PreSize X="0.0862" Y="0.0707" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="PlistSubImage" Path="cr_chongzhi.png" Plist="plist/creatroompics.plist" />
            <PressedFileData Type="PlistSubImage" Path="cr_chongzhi.png" Plist="plist/creatroompics.plist" />
            <NormalFileData Type="PlistSubImage" Path="cr_chongzhi.png" Plist="plist/creatroompics.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="RoomCard_bg" ActionTag="-1765769879" Tag="114" IconVisible="False" LeftMargin="195.9031" RightMargin="888.0969" TopMargin="586.5712" BottomMargin="113.4288" ctype="SpriteObjectData">
            <Size X="250.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.9031" Y="138.4288" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2406" Y="0.1846" />
            <PreSize X="0.1874" Y="0.0667" />
            <FileData Type="Normal" Path="wxpri_pic/wx_cr_roomcardbg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_myRoomCardNum" ActionTag="-708188219" Tag="37" IconVisible="False" LeftMargin="348.9353" RightMargin="912.0647" TopMargin="586.2158" BottomMargin="112.7842" FontSize="40" LabelText="999" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="73.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="385.4353" Y="138.2842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2889" Y="0.1844" />
            <PreSize X="0.0547" Y="0.0680" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <OutlineColor A="255" R="174" G="110" B="16" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="PeopleCount" ActionTag="2058276598" Tag="136" IconVisible="False" LeftMargin="285.1539" RightMargin="988.8461" TopMargin="312.1107" BottomMargin="406.8893" FontSize="26" LabelText="人数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="315.1539" Y="422.3893" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="144" G="93" B="12" />
            <PrePosition X="0.2362" Y="0.5632" />
            <PreSize X="0.0450" Y="0.0413" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="144" G="93" B="12" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="JuCount" ActionTag="-94862227" Tag="137" IconVisible="False" LeftMargin="285.1538" RightMargin="988.8462" TopMargin="203.1494" BottomMargin="515.8506" FontSize="26" LabelText="局数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="315.1538" Y="531.3506" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="144" G="93" B="12" />
            <PrePosition X="0.2362" Y="0.7085" />
            <PreSize X="0.0450" Y="0.0413" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="139" G="105" B="20" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Playway" ActionTag="-1597937528" Tag="138" IconVisible="False" LeftMargin="285.1539" RightMargin="988.8461" TopMargin="385.0650" BottomMargin="333.9350" FontSize="26" LabelText="玩法:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="315.1539" Y="349.4350" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="144" G="93" B="12" />
            <PrePosition X="0.2362" Y="0.4659" />
            <PreSize X="0.0450" Y="0.0413" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="144" G="93" B="12" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="HuaHuaSheng" ActionTag="868935268" Tag="139" IconVisible="False" LeftMargin="259.1540" RightMargin="988.8459" TopMargin="507.0926" BottomMargin="211.9074" FontSize="26" LabelText="花花升:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="86.0000" Y="31.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="302.1540" Y="227.4074" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="144" G="93" B="12" />
            <PrePosition X="0.2265" Y="0.3032" />
            <PreSize X="0.0645" Y="0.0413" />
            <FontResource Type="Default" Path="" Plist="" />
            <OutlineColor A="255" R="144" G="93" B="12" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sp_Title" ActionTag="1359034653" Tag="115" IconVisible="False" LeftMargin="446.0000" RightMargin="446.0000" TopMargin="65.2600" BottomMargin="621.7400" ctype="SpriteObjectData">
            <Size X="442.0000" Y="63.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="653.2400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8710" />
            <PreSize X="0.3313" Y="0.0840" />
            <FileData Type="Normal" Path="wxpri_pic/wx_cr_title.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>