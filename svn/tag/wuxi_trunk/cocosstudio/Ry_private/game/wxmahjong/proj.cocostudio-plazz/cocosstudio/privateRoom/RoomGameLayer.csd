<GameFile>
  <PropertyGroup Name="RoomGameLayer" Type="Scene" ID="c975d689-1c5f-4847-ad61-99f4c4bfdcf0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="312" ctype="GameNodeObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="sp_roomInfoBg" ActionTag="-1862146133" Tag="79" IconVisible="False" RightMargin="1134.0000" BottomMargin="650.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Text_roomNum" ActionTag="-1157423479" Tag="104" IconVisible="False" LeftMargin="10.0000" RightMargin="61.0000" TopMargin="33.0000" BottomMargin="41.0000" FontSize="20" LabelText="房号：1008611" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="129.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.0000" Y="54.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.5400" />
                <PreSize X="0.6450" Y="0.2600" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_roomName" ActionTag="1932044364" VisibleForFrame="False" Tag="103" IconVisible="False" LeftMargin="29.5000" RightMargin="27.5000" TopMargin="33.9631" BottomMargin="42.0369" FontSize="20" LabelText="女儿村雀神争霸" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="143.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="101.0000" Y="54.0369" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5050" Y="0.5404" />
                <PreSize X="0.7150" Y="0.2400" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_index" ActionTag="-1693890023" VisibleForFrame="False" Tag="102" IconVisible="False" LeftMargin="34.0195" RightMargin="117.9805" TopMargin="33.7524" BottomMargin="42.2476" FontSize="20" LabelText="1/8局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="48.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.0195" Y="54.2476" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2901" Y="0.5425" />
                <PreSize X="0.2400" Y="0.2400" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_innings" ActionTag="-315237078" Tag="101" IconVisible="False" LeftMargin="10.0000" RightMargin="129.0000" TopMargin="5.0000" BottomMargin="69.0000" FontSize="20" LabelText="剩21局" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="61.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.0000" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.8200" />
                <PreSize X="0.3050" Y="0.2600" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="750.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="0.1499" Y="0.1333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_invite" ActionTag="645617496" Tag="138" IconVisible="False" LeftMargin="521.5000" RightMargin="521.5000" TopMargin="462.0000" BottomMargin="182.0000" TouchEnable="True" FontSize="28" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="261" Scale9Height="84" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="291.0000" Y="106.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="235.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3133" />
            <PreSize X="0.2181" Y="0.1413" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="77" />
            <DisabledFileData Type="PlistSubImage" Path="game_btn_ivite_wx_press.png" Plist="plist/plazaScene.plist" />
            <PressedFileData Type="PlistSubImage" Path="game_btn_ivite_wx_press.png" Plist="plist/plazaScene.plist" />
            <NormalFileData Type="PlistSubImage" Path="game_btn_ivite_wx.png" Plist="plist/plazaScene.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_dismiss" ActionTag="848647569" VisibleForFrame="False" Tag="137" IconVisible="False" LeftMargin="17.9915" RightMargin="1236.0085" TopMargin="81.1779" BottomMargin="635.8221" TouchEnable="True" FontSize="28" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="50" Scale9Height="11" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="80.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="57.9915" Y="652.3221" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0435" Y="0.8698" />
            <PreSize X="0.0600" Y="0.0440" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="77" />
            <DisabledFileData Type="PlistSubImage" Path="bt_dismiss_1.png" Plist="privateRoom/privateRoom.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_dismiss_2.png" Plist="privateRoom/privateRoom.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_dismiss_1.png" Plist="privateRoom/privateRoom.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bt_zanli" ActionTag="-912571242" VisibleForFrame="False" Tag="267" IconVisible="False" LeftMargin="102.8982" RightMargin="1150.1018" TopMargin="81.1779" BottomMargin="635.8221" TouchEnable="True" FontSize="28" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="51" Scale9Height="11" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="81.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="143.3982" Y="652.3221" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1075" Y="0.8698" />
            <PreSize X="0.0607" Y="0.0440" />
            <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="77" />
            <DisabledFileData Type="PlistSubImage" Path="bt_zanLi_1.png" Plist="privateRoom/privateRoom.plist" />
            <PressedFileData Type="PlistSubImage" Path="bt_zanLi_2.png" Plist="privateRoom/privateRoom.plist" />
            <NormalFileData Type="PlistSubImage" Path="bt_zanLi_1.png" Plist="privateRoom/privateRoom.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_roomHost" ActionTag="-1681766355" Tag="68" IconVisible="False" LeftMargin="5.5805" RightMargin="1288.4196" TopMargin="479.2333" BottomMargin="194.7667" ctype="SpriteObjectData">
            <Size X="40.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="25.5805" Y="232.7667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0192" Y="0.3104" />
            <PreSize X="0.0300" Y="0.1013" />
            <FileData Type="PlistSubImage" Path="sp_roomHost.png" Plist="privateRoom/privateRoom.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>