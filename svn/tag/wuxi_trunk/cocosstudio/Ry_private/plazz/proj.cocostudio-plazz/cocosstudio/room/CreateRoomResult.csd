<GameFile>
  <PropertyGroup Name="CreateRoomResult" Type="Layer" ID="66121fbc-f7f3-452d-b18c-b5f990415394" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="18" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="panel_mask" ActionTag="-1795147967" Tag="32" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_bg" ActionTag="-485898702" Tag="22" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="238.0000" RightMargin="238.0000" TopMargin="145.2505" BottomMargin="135.7495" TouchEnable="True" Scale9Enable="True" LeftEage="385" RightEage="349" TopEage="148" BottomEage="148" Scale9OriginX="385" Scale9OriginY="148" Scale9Width="1" Scale9Height="103" ctype="ImageViewObjectData">
            <Size X="858.0000" Y="469.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_2" ActionTag="-1378335273" Tag="77" IconVisible="False" LeftMargin="255.4441" RightMargin="254.5559" TopMargin="-47.5004" BottomMargin="452.5004" ctype="SpriteObjectData">
                <Size X="348.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="429.4441" Y="484.5004" />
                <Scale ScaleX="1.1700" ScaleY="1.1700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5005" Y="1.0330" />
                <PreSize X="0.0536" Y="0.0981" />
                <FileData Type="PlistSubImage" Path="plaza_image_title_bg.png" Plist="room/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="title_general_4" ActionTag="-1053955951" Tag="23" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="363.5713" RightMargin="358.4287" TopMargin="-27.4741" BottomMargin="461.4741" ctype="SpriteObjectData">
                <Size X="136.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="431.5713" Y="478.9741" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5030" Y="1.0213" />
                <PreSize X="0.1585" Y="0.0746" />
                <FileData Type="Normal" Path="General/title_general.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_tips" ActionTag="-1811399833" Tag="33" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="384.0731" RightMargin="380.9269" TopMargin="181.3931" BottomMargin="250.6069" FontSize="30" LabelText="房间号" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="93.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="430.5731" Y="269.1069" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5018" Y="0.5738" />
                <PreSize X="0.1084" Y="0.0789" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_join" ActionTag="275932609" Tag="24" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="170.9788" RightMargin="497.0212" TopMargin="348.0019" BottomMargin="56.9981" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="265.9788" Y="88.9981" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3100" Y="0.1898" />
                <PreSize X="0.2214" Y="0.1365" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="pri_btn_joingame_0.png" Plist="room/room.plist" />
                <PressedFileData Type="PlistSubImage" Path="pri_btn_joingame_1.png" Plist="room/room.plist" />
                <NormalFileData Type="PlistSubImage" Path="pri_btn_joingame_0.png" Plist="room/room.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_invite" ActionTag="-1527452884" Tag="25" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="497.0180" RightMargin="170.9820" TopMargin="348.0019" BottomMargin="56.9981" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="42" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="190.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="592.0180" Y="88.9981" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6900" Y="0.1898" />
                <PreSize X="0.2214" Y="0.1365" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="pri_btn_invite_0.png" Plist="room/room.plist" />
                <PressedFileData Type="PlistSubImage" Path="pri_btn_invite_1.png" Plist="room/room.plist" />
                <NormalFileData Type="PlistSubImage" Path="pri_btn_invite_0.png" Plist="room/room.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="370.2495" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4937" />
            <PreSize X="0.6432" Y="0.6253" />
            <FileData Type="PlistSubImage" Path="plaza_common_bg.png" Plist="room/plazaScene.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>