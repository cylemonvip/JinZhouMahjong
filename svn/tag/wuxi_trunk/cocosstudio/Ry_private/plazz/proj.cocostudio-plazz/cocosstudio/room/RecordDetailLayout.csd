<GameFile>
  <PropertyGroup Name="RecordDetailLayout" Type="Node" ID="d0aa2f65-9114-466a-85bf-068a4f40008f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="49" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="MaskLayout" ActionTag="-11559639" Tag="53" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="RootNode" ActionTag="-1406309269" Tag="50" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="249" RightEage="249" TopEage="148" BottomEage="148" Scale9OriginX="249" Scale9OriginY="148" Scale9Width="836" Scale9Height="454" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="NameText1" ActionTag="275486224" Tag="33" IconVisible="False" LeftMargin="376.4500" RightMargin="774.5500" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三拉" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="183.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="376.4500" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2822" Y="0.7989" />
                <PreSize X="0.1372" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText2" ActionTag="1836024632" Tag="34" IconVisible="False" LeftMargin="559.9254" RightMargin="591.0746" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="183.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="559.9254" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4197" Y="0.7989" />
                <PreSize X="0.1372" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText3" ActionTag="-1290192448" Tag="35" IconVisible="False" LeftMargin="744.4007" RightMargin="406.5993" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="183.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="744.4007" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5580" Y="0.7989" />
                <PreSize X="0.1372" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText4" ActionTag="1159738768" Tag="36" IconVisible="False" LeftMargin="928.3760" RightMargin="239.6240" TopMargin="132.3511" BottomMargin="580.6489" IsCustomSize="True" FontSize="30" LabelText="张三打" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="166.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="928.3760" Y="599.1489" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6959" Y="0.7989" />
                <PreSize X="0.1244" Y="0.0493" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnBack" ActionTag="-918638689" Tag="17" IconVisible="False" LeftMargin="23.0067" RightMargin="1252.9933" TopMargin="21.8690" BottomMargin="669.1310" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="37" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="58.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.0067" Y="698.6310" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0390" Y="0.9315" />
                <PreSize X="0.0435" Y="0.0787" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/btn_back_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/btn_back_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/btn_back_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="room/recorddetail/recordbg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayout" ActionTag="-426199384" Tag="57" IconVisible="False" LeftMargin="-570.2499" RightMargin="-579.7501" TopMargin="500.2107" BottomMargin="-620.2107" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="381" RightEage="381" TopEage="37" BottomEage="37" Scale9OriginX="381" Scale9OriginY="37" Scale9Width="394" Scale9Height="41" ctype="PanelObjectData">
            <Size X="1150.0000" Y="120.0000" />
            <Children>
              <AbstractNodeData Name="FangIDText" ActionTag="2016835688" Tag="58" IconVisible="False" LeftMargin="51.2274" RightMargin="1075.7726" TopMargin="15.0114" BottomMargin="31.9886" FontSize="60" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="23.0000" Y="73.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.7274" Y="68.4886" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0545" Y="0.5707" />
                <PreSize X="0.0200" Y="0.6083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TimeText" ActionTag="-1058833150" Tag="60" IconVisible="False" LeftMargin="114.3290" RightMargin="890.6710" TopMargin="11.0115" BottomMargin="27.9885" IsCustomSize="True" FontSize="22" LabelText="2017-08-29&#xA;18：16：09" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="145.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="186.8290" Y="68.4885" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1625" Y="0.5707" />
                <PreSize X="0.1261" Y="0.6750" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score1" ActionTag="-1500048984" Tag="59" IconVisible="False" LeftMargin="340.5993" RightMargin="749.4007" TopMargin="21.0126" BottomMargin="37.9874" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="370.5993" Y="68.4874" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3223" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score2" ActionTag="-1652902996" Tag="37" IconVisible="False" LeftMargin="528.5731" RightMargin="561.4269" TopMargin="21.0128" BottomMargin="37.9872" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="558.5731" Y="68.4872" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4857" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score3" ActionTag="-1324174194" Tag="38" IconVisible="False" LeftMargin="711.5496" RightMargin="378.4504" TopMargin="21.0126" BottomMargin="37.9874" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="741.5496" Y="68.4874" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6448" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score4" ActionTag="-852027379" Tag="39" IconVisible="False" LeftMargin="887.5254" RightMargin="202.4746" TopMargin="21.0120" BottomMargin="37.9880" FontSize="50" LabelText="99" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="60.0000" Y="61.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="917.5254" Y="68.4880" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7978" Y="0.5707" />
                <PreSize X="0.0522" Y="0.5083" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnReplay" ActionTag="1355914935" Tag="40" IconVisible="False" LeftMargin="984.8922" RightMargin="31.1078" TopMargin="22.5937" BottomMargin="37.4063" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="104" Scale9Height="38" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="134.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1051.8922" Y="67.4063" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9147" Y="0.5617" />
                <PreSize X="0.1165" Y="0.5000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/recorddetail/btnplayback_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/recorddetail/btnplayback_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/recorddetail/btnplayback_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-570.2499" Y="-620.2107" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="room/recorddetail/rcditembg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>