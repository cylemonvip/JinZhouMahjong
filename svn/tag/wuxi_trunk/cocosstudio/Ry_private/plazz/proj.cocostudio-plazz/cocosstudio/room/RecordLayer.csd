<GameFile>
  <PropertyGroup Name="RecordLayer" Type="Layer" ID="ac60071d-8c48-437a-8feb-1423e4610ded" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="32" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="1875718627" Tag="108" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="440" RightEage="440" TopEage="247" BottomEage="247" Scale9OriginX="440" Scale9OriginY="247" Scale9Width="454" Scale9Height="256" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="room/recordpic/wdzjbg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="MyRoomBgSprite" ActionTag="-1207903328" Tag="103" IconVisible="False" ctype="SpriteObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="room/recordpic/wdfjpic.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="1169492687" Tag="53" IconVisible="False" LeftMargin="93.0429" RightMargin="90.9572" TopMargin="171.9836" BottomMargin="33.0164" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1150.0000" Y="545.0000" />
            <AnchorPoint />
            <Position X="93.0429" Y="33.0164" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0697" Y="0.0440" />
            <PreSize X="0.8621" Y="0.7267" />
            <SingleColor A="255" R="255" G="165" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="BtnBack" ActionTag="1854734583" Tag="132" IconVisible="False" LeftMargin="35.0001" RightMargin="1240.9999" TopMargin="18.4980" BottomMargin="672.5020" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="28" Scale9Height="37" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="58.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="64.0001" Y="702.0020" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0480" Y="0.9360" />
            <PreSize X="0.0435" Y="0.0787" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="room/btn_back_s.png" Plist="" />
            <PressedFileData Type="Normal" Path="room/btn_back_s.png" Plist="" />
            <NormalFileData Type="Normal" Path="room/btn_back_n.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayout" ActionTag="881123514" Tag="40" IconVisible="False" LeftMargin="93.4975" RightMargin="90.5024" TopMargin="812.6736" BottomMargin="-212.6736" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="381" RightEage="381" TopEage="47" BottomEage="47" Scale9OriginX="381" Scale9OriginY="47" Scale9Width="394" Scale9Height="49" ctype="PanelObjectData">
            <Size X="1150.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="IndexText" ActionTag="1852120442" Tag="41" IconVisible="False" LeftMargin="125.9993" RightMargin="1010.0007" TopMargin="9.1033" BottomMargin="103.8967" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="132.9993" Y="122.3967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1157" Y="0.8160" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDText" ActionTag="-1233787837" Tag="42" IconVisible="False" LeftMargin="300.0000" RightMargin="812.0000" TopMargin="9.1033" BottomMargin="103.8967" FontSize="30" LabelText="ID:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="38.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="300.0000" Y="122.3967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2609" Y="0.8160" />
                <PreSize X="0.0330" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TimeText" ActionTag="-1336141793" Tag="43" IconVisible="False" LeftMargin="1044.3022" RightMargin="35.6978" TopMargin="9.1033" BottomMargin="103.8967" FontSize="30" LabelText="DZSJ" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="37.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="1114.3022" Y="122.3967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9690" Y="0.8160" />
                <PreSize X="0.0609" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText1" ActionTag="-453586589" Tag="44" IconVisible="False" LeftMargin="30.9993" RightMargin="915.0007" TopMargin="44.4761" BottomMargin="73.5239" IsCustomSize="True" FontSize="25" LabelText="香水有毒收发" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="204.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="132.9993" Y="89.5239" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1157" Y="0.5968" />
                <PreSize X="0.1774" Y="0.2133" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText2" ActionTag="1476581412" Tag="45" IconVisible="False" LeftMargin="283.6677" RightMargin="662.3323" TopMargin="45.5000" BottomMargin="71.5000" IsCustomSize="True" FontSize="25" LabelText="下你家的说法" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="204.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="385.6677" Y="88.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3354" Y="0.5867" />
                <PreSize X="0.1774" Y="0.2200" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText3" ActionTag="984518883" Tag="46" IconVisible="False" LeftMargin="536.4401" RightMargin="409.5599" TopMargin="45.5000" BottomMargin="71.5000" IsCustomSize="True" FontSize="25" LabelText="加的精神分裂就" HorizontalAlignmentType="HT_Center" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="204.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="638.4401" Y="88.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5552" Y="0.5867" />
                <PreSize X="0.1774" Y="0.2200" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText4" ActionTag="2096156258" Tag="47" IconVisible="False" LeftMargin="788.5045" RightMargin="156.4955" TopMargin="45.5000" BottomMargin="71.5000" IsCustomSize="True" FontSize="25" LabelText="发记得是开发都是" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="205.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="891.0045" Y="88.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7748" Y="0.5867" />
                <PreSize X="0.1783" Y="0.2200" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreText1" ActionTag="-2051869947" Tag="49" IconVisible="False" LeftMargin="128.6011" RightMargin="1007.3989" TopMargin="84.5678" BottomMargin="28.4322" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.6011" Y="46.9322" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1179" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreText2" ActionTag="1644233815" Tag="50" IconVisible="False" LeftMargin="380.4023" RightMargin="755.5977" TopMargin="84.5677" BottomMargin="28.4323" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="387.4023" Y="46.9323" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3369" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreText3" ActionTag="-1079176404" Tag="51" IconVisible="False" LeftMargin="632.2036" RightMargin="503.7964" TopMargin="84.5661" BottomMargin="28.4339" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.7091" ScaleY="0.4279" />
                <Position X="642.1310" Y="44.2662" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5584" Y="0.2951" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ScoreText4" ActionTag="1164819342" Tag="52" IconVisible="False" LeftMargin="884.0045" RightMargin="251.9955" TopMargin="84.5677" BottomMargin="28.4323" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="891.0045" Y="46.9323" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7748" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnMore" ActionTag="-116525834" Tag="54" IconVisible="False" LeftMargin="993.4761" RightMargin="24.5239" TopMargin="57.4712" BottomMargin="35.5288" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="102" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="132.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1059.4761" Y="64.0288" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9213" Y="0.4269" />
                <PreSize X="0.1148" Y="0.3800" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/recordpic/btnmore_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/recordpic/btnmore_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/recordpic/btnmore_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="93.4975" Y="-212.6736" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0701" Y="-0.2836" />
            <PreSize X="0.8621" Y="0.2000" />
            <FileData Type="Normal" Path="room/recordpic/itemttbg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ItemLayout_create" ActionTag="1869039201" Tag="55" IconVisible="False" LeftMargin="95.4948" RightMargin="88.5052" TopMargin="1014.7578" BottomMargin="-414.7578" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="381" RightEage="381" TopEage="47" BottomEage="47" Scale9OriginX="381" Scale9OriginY="47" Scale9Width="394" Scale9Height="49" ctype="PanelObjectData">
            <Size X="1150.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="IndexText" ActionTag="2043271233" Tag="56" IconVisible="False" LeftMargin="95.9997" RightMargin="1040.0002" TopMargin="9.1033" BottomMargin="103.8967" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.9997" Y="122.3967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0896" Y="0.8160" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="IDText" ActionTag="-1641118478" Tag="57" IconVisible="False" LeftMargin="213.9980" RightMargin="898.0020" TopMargin="9.1033" BottomMargin="103.8967" FontSize="30" LabelText="ID:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="38.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="213.9980" Y="122.3967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1861" Y="0.8160" />
                <PreSize X="0.0330" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TimeText" ActionTag="-1211616587" Tag="58" IconVisible="False" LeftMargin="1044.3022" RightMargin="35.6978" TopMargin="9.1033" BottomMargin="103.8967" FontSize="30" LabelText="DZSJ" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="37.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="1114.3022" Y="122.3967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9690" Y="0.8160" />
                <PreSize X="0.0609" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="RoomQuanCount" ActionTag="1282554300" Tag="59" IconVisible="False" LeftMargin="76.0000" RightMargin="1020.0000" TopMargin="44.5000" BottomMargin="74.5000" FontSize="25" LabelText="圈数" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="54.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.0000" Y="90.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0896" Y="0.6000" />
                <PreSize X="0.0470" Y="0.2067" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText2" ActionTag="-475468578" Tag="60" IconVisible="False" LeftMargin="300.4965" RightMargin="796.5035" TopMargin="44.5000" BottomMargin="74.5000" FontSize="25" LabelText="奖励" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="53.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="326.9965" Y="90.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2843" Y="0.6000" />
                <PreSize X="0.0461" Y="0.2067" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText3" ActionTag="1173413881" Tag="61" IconVisible="False" LeftMargin="563.2977" RightMargin="483.7023" TopMargin="44.5000" BottomMargin="74.5000" FontSize="25" LabelText="解散时间" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="103.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="614.7977" Y="90.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5346" Y="0.6000" />
                <PreSize X="0.0896" Y="0.2067" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="NameText4" ActionTag="-593189986" Tag="62" IconVisible="False" LeftMargin="880.0038" RightMargin="215.9962" TopMargin="43.9753" BottomMargin="75.0247" FontSize="25" LabelText="状态" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="54.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="907.0038" Y="90.5247" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7887" Y="0.6035" />
                <PreSize X="0.0470" Y="0.2067" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="QuanShuText" ActionTag="1160399589" Tag="63" IconVisible="False" LeftMargin="98.6014" RightMargin="1037.3986" TopMargin="84.5678" BottomMargin="28.4322" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="105.6014" Y="46.9322" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0918" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JiangLiText" ActionTag="-740909772" Tag="64" IconVisible="False" LeftMargin="319.9963" RightMargin="816.0037" TopMargin="84.5677" BottomMargin="28.4323" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="326.9963" Y="46.9323" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2843" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="JieSanTimeText" ActionTag="-1460771674" Tag="65" IconVisible="False" LeftMargin="607.7977" RightMargin="528.2023" TopMargin="84.5676" BottomMargin="28.4324" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="614.7977" Y="46.9324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5346" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="StatusText" ActionTag="667392647" Tag="66" IconVisible="False" LeftMargin="900.0037" RightMargin="235.9963" TopMargin="84.5677" BottomMargin="28.4323" FontSize="30" LabelText="1" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="907.0037" Y="46.9323" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7887" Y="0.3129" />
                <PreSize X="0.0122" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnMore" ActionTag="1614884317" Tag="67" IconVisible="False" LeftMargin="993.4761" RightMargin="24.5239" TopMargin="57.4712" BottomMargin="35.5288" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="102" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="132.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1059.4761" Y="64.0288" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9213" Y="0.4269" />
                <PreSize X="0.1148" Y="0.3800" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/recordpic/btnmore_s.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/recordpic/btnmore_s.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/recordpic/btnmore_n.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CostText" ActionTag="-354587757" Tag="69" IconVisible="False" LeftMargin="455.9949" RightMargin="652.0051" TopMargin="9.1000" BottomMargin="103.9000" FontSize="30" LabelText="XH" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="455.9949" Y="122.4000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3965" Y="0.8160" />
                <PreSize X="0.0365" Y="0.2467" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="95.4948" Y="-414.7578" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0716" Y="-0.5530" />
            <PreSize X="0.8621" Y="0.2000" />
            <FileData Type="Normal" Path="room/recordpic/itemttbg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>