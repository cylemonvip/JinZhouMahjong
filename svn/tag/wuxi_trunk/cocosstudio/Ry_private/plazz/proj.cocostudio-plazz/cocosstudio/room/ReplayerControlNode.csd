<GameFile>
  <PropertyGroup Name="ReplayerControlNode" Type="Node" ID="b8fc393d-90db-408b-832c-12180a972fd4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="RootNode" ActionTag="-1369842914" Tag="2" IconVisible="False" LeftMargin="-667.0000" RightMargin="-667.0000" TopMargin="-375.0000" BottomMargin="-375.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <Children>
              <AbstractNodeData Name="BgSp" ActionTag="664755235" Tag="3" IconVisible="False" LeftMargin="431.0001" RightMargin="432.9999" TopMargin="322.0000" BottomMargin="322.0000" ctype="SpriteObjectData">
                <Size X="470.0000" Y="106.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="666.0001" Y="375.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4993" Y="0.5000" />
                <PreSize X="0.3523" Y="0.1413" />
                <FileData Type="Normal" Path="room/replayer/hist_rec_play_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnQuick" ActionTag="-1851908006" Tag="4" IconVisible="False" LeftMargin="704.2130" RightMargin="578.7870" TopMargin="355.2671" BottomMargin="359.7329" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="21" Scale9Height="13" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="51.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="729.7130" Y="377.2329" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5470" Y="0.5030" />
                <PreSize X="0.0382" Y="0.0467" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/replayer/hist_rec_forward.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/replayer/hist_rec_forward.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/replayer/hist_rec_forward.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnSlow" ActionTag="-1414302896" Tag="5" IconVisible="False" LeftMargin="479.1931" RightMargin="803.8069" TopMargin="355.2671" BottomMargin="359.7329" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="21" Scale9Height="13" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="51.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="504.6931" Y="377.2329" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3783" Y="0.5030" />
                <PreSize X="0.0382" Y="0.0467" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/replayer/hist_rec_backward.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/replayer/hist_rec_backward.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/replayer/hist_rec_backward.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnPlay" ActionTag="1142950248" Tag="6" IconVisible="False" LeftMargin="599.1036" RightMargin="700.8964" TopMargin="353.7671" BottomMargin="358.2329" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="4" Scale9Height="16" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="34.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="616.1036" Y="377.2329" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4618" Y="0.5030" />
                <PreSize X="0.0255" Y="0.0507" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/replayer/hist_rec_pause.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/replayer/hist_rec_pause.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/replayer/hist_rec_pause.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="BtnClose" ActionTag="1130044012" Tag="7" IconVisible="False" LeftMargin="811.2607" RightMargin="472.7393" TopMargin="355.2671" BottomMargin="359.7329" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="20" Scale9Height="13" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="50.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="836.2607" Y="377.2329" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6269" Y="0.5030" />
                <PreSize X="0.0375" Y="0.0467" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="room/replayer/hist_rec_exit.png" Plist="" />
                <PressedFileData Type="Normal" Path="room/replayer/hist_rec_exit.png" Plist="" />
                <NormalFileData Type="Normal" Path="room/replayer/hist_rec_exit.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TxtProgress" ActionTag="1908485768" Tag="8" IconVisible="False" LeftMargin="618.1295" RightMargin="615.8705" TopMargin="286.4107" BottomMargin="432.5893" FontSize="25" LabelText="进度: 5%" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="668.1295" Y="448.0893" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5008" Y="0.5975" />
                <PreSize X="0.0750" Y="0.0413" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="TxtRate" ActionTag="-1182644816" Tag="9" IconVisible="False" LeftMargin="619.4998" RightMargin="619.5002" TopMargin="427.1077" BottomMargin="291.8923" FontSize="25" LabelText="速率: x2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="95.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="666.9998" Y="307.3923" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4099" />
                <PreSize X="0.0712" Y="0.0413" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>