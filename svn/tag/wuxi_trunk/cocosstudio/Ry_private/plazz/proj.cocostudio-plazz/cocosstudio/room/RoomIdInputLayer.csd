<GameFile>
  <PropertyGroup Name="RoomIdInputLayer" Type="Layer" ID="01cc8968-0646-4944-866d-94f5edeb9136" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="16" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="pri_sp_ideditbg" ActionTag="226186103" Tag="18" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="300.0000" RightMargin="300.0000" TopMargin="175.5000" BottomMargin="175.5000" ctype="SpriteObjectData">
            <Size X="734.0000" Y="399.0000" />
            <Children>
              <AbstractNodeData Name="TitleSp" ActionTag="150585054" Tag="49" IconVisible="False" LeftMargin="193.0000" RightMargin="193.0000" TopMargin="-43.8298" BottomMargin="378.8298" ctype="SpriteObjectData">
                <Size X="348.0000" Y="64.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="367.0000" Y="410.8298" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0296" />
                <PreSize X="0.4741" Y="0.1604" />
                <FileData Type="PlistSubImage" Path="plaza_image_title_bg.png" Plist="room/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="NumShowBg" ActionTag="1409027042" Tag="48" IconVisible="False" LeftMargin="65.4998" RightMargin="65.5002" TopMargin="16.6491" BottomMargin="237.3509" ctype="SpriteObjectData">
                <Size X="603.0000" Y="145.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="366.9998" Y="309.8509" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7766" />
                <PreSize X="0.8215" Y="0.3634" />
                <FileData Type="PlistSubImage" Path="plaza_enter_room_back.png" Plist="room/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num1" ActionTag="-195158273" Tag="20" IconVisible="False" LeftMargin="99.1540" RightMargin="466.8460" TopMargin="111.1819" BottomMargin="221.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="183.1540" Y="254.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2495" Y="0.6386" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_01.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_01.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_01.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num2" ActionTag="1966220425" Tag="21" IconVisible="False" LeftMargin="280.1537" RightMargin="285.8463" TopMargin="111.1818" BottomMargin="221.8182" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="364.1537" Y="254.8182" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4961" Y="0.6386" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_02.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_02.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_02.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num3" ActionTag="-569130389" Tag="22" IconVisible="False" LeftMargin="461.1535" RightMargin="104.8465" TopMargin="111.1819" BottomMargin="221.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="545.1535" Y="254.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7427" Y="0.6386" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_03.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_03.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_03.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num4" ActionTag="-604225920" Tag="23" IconVisible="False" LeftMargin="99.1540" RightMargin="466.8460" TopMargin="180.1819" BottomMargin="152.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="183.1540" Y="185.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2495" Y="0.4657" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_04.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_04.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_04.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num5" ActionTag="81640117" Tag="24" IconVisible="False" LeftMargin="280.1537" RightMargin="285.8463" TopMargin="180.1819" BottomMargin="152.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="364.1537" Y="185.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4961" Y="0.4657" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_05.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_05.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_05.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num6" ActionTag="1619923935" Tag="25" IconVisible="False" LeftMargin="461.1535" RightMargin="104.8465" TopMargin="180.1819" BottomMargin="152.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="545.1535" Y="185.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7427" Y="0.4657" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_06.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_06.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_06.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num7" ActionTag="2030805213" Tag="26" IconVisible="False" LeftMargin="99.1540" RightMargin="466.8460" TopMargin="249.1819" BottomMargin="83.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="183.1540" Y="116.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2495" Y="0.2928" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_07.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_07.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_07.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num8" ActionTag="-1843842339" Tag="27" IconVisible="False" LeftMargin="280.1537" RightMargin="285.8463" TopMargin="249.1819" BottomMargin="83.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="364.1537" Y="116.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4961" Y="0.2928" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_08.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_08.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_08.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num9" ActionTag="1852254127" Tag="28" IconVisible="False" LeftMargin="461.1535" RightMargin="104.8465" TopMargin="249.1819" BottomMargin="83.8181" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="545.1535" Y="116.8181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7427" Y="0.2928" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_09.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_09.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_09.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_num0" ActionTag="540766664" Tag="29" IconVisible="False" LeftMargin="280.1537" RightMargin="285.8463" TopMargin="318.1822" BottomMargin="14.8178" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="364.1537" Y="47.8178" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4961" Y="0.1198" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_0.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_0.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_0.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_clear" ActionTag="532765255" Tag="30" IconVisible="False" LeftMargin="99.1540" RightMargin="466.8460" TopMargin="318.1822" BottomMargin="14.8178" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="183.1540" Y="47.8178" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2495" Y="0.1198" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_again.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_again.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_again.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_del" ActionTag="835022667" Tag="31" IconVisible="False" LeftMargin="461.1535" RightMargin="104.8465" TopMargin="318.1822" BottomMargin="14.8178" TouchEnable="True" FontSize="14" ButtonText=" " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="138" Scale9Height="44" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="168.0000" Y="66.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="545.1535" Y="47.8178" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7427" Y="0.1198" />
                <PreSize X="0.2289" Y="0.1654" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="shuzi_btn_press_delete.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="shuzi_btn_press_delete.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="shuzi_btn_delete.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_0" ActionTag="1620666935" Tag="57" IconVisible="False" LeftMargin="142.2400" RightMargin="562.7600" TopMargin="49.1766" BottomMargin="290.8234" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="156.7400" Y="320.3234" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="169" G="117" B="21" />
                <PrePosition X="0.2135" Y="0.8028" />
                <PreSize X="0.0395" Y="0.1479" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid" ActionTag="-159371142" Tag="56" IconVisible="False" LeftMargin="331.2867" RightMargin="350.7133" TopMargin="518.5485" BottomMargin="-160.5485" CharWidth="52" CharHeight="41" LabelText="1" StartChar="0" ctype="TextAtlasObjectData">
                <Size X="52.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="357.2867" Y="-140.0485" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4868" Y="-0.3510" />
                <PreSize X="0.0708" Y="0.1028" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="room/atlas_roomid.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="CloseBtn" ActionTag="-1406303764" Tag="50" IconVisible="False" LeftMargin="686.2384" RightMargin="-20.2384" TopMargin="-21.8579" BottomMargin="352.8579" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="38" Scale9Height="46" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="720.2384" Y="386.8579" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9813" Y="0.9696" />
                <PreSize X="0.0926" Y="0.1704" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="room/plazaScene.plist" />
                <PressedFileData Type="PlistSubImage" Path="plaza_btn_close_press.png" Plist="room/plazaScene.plist" />
                <NormalFileData Type="PlistSubImage" Path="plaza_btn_close_normal.png" Plist="room/plazaScene.plist" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_2" ActionTag="-1018522487" Tag="58" IconVisible="False" LeftMargin="308.4719" RightMargin="396.5281" TopMargin="49.1765" BottomMargin="290.8235" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="322.9719" Y="320.3235" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="169" G="117" B="21" />
                <PrePosition X="0.4400" Y="0.8028" />
                <PreSize X="0.0395" Y="0.1479" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_1" ActionTag="-384215604" Tag="59" IconVisible="False" LeftMargin="225.5101" RightMargin="479.4899" TopMargin="49.1766" BottomMargin="290.8234" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="240.0101" Y="320.3234" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="169" G="117" B="21" />
                <PrePosition X="0.3270" Y="0.8028" />
                <PreSize X="0.0395" Y="0.1479" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_3" ActionTag="-988490495" Tag="60" IconVisible="False" LeftMargin="391.7025" RightMargin="313.2975" TopMargin="49.1763" BottomMargin="290.8237" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="406.2025" Y="320.3237" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="169" G="117" B="21" />
                <PrePosition X="0.5534" Y="0.8028" />
                <PreSize X="0.0395" Y="0.1479" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_4" ActionTag="-1738655502" Tag="61" IconVisible="False" LeftMargin="476.1633" RightMargin="228.8367" TopMargin="49.1765" BottomMargin="290.8235" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="490.6633" Y="320.3235" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="169" G="117" B="21" />
                <PrePosition X="0.6685" Y="0.8028" />
                <PreSize X="0.0395" Y="0.1479" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="atlas_roomid_5" ActionTag="1373596364" Tag="62" IconVisible="False" LeftMargin="560.2403" RightMargin="144.7597" TopMargin="49.1765" BottomMargin="290.8235" FontSize="50" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="29.0000" Y="59.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="574.7403" Y="320.3235" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="169" G="117" B="21" />
                <PrePosition X="0.7830" Y="0.8028" />
                <PreSize X="0.0395" Y="0.1479" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="375.0000" />
            <Scale ScaleX="1.3000" ScaleY="1.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5502" Y="0.5320" />
            <FileData Type="PlistSubImage" Path="plaza_common_bg.png" Plist="room/plazaScene.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>