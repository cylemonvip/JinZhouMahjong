<GameFile>
  <PropertyGroup Name="RoomInfoLayer" Type="Layer" ID="f384e5ea-1a7a-428d-a0d2-48d0e5519ef9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="1334.0000" Y="750.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-376297899" Tag="4" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1334.0000" Y="750.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_bg" ActionTag="592919944" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="300.0000" RightMargin="300.0000" TopMargin="213.0000" BottomMargin="138.0000" LeftEage="249" RightEage="249" TopEage="148" BottomEage="148" Scale9OriginX="249" Scale9OriginY="148" Scale9Width="236" Scale9Height="103" ctype="ImageViewObjectData">
            <Size X="734.0000" Y="399.0000" />
            <Children>
              <AbstractNodeData Name="TitleBg" ActionTag="2137465536" Tag="113" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="193.0000" RightMargin="193.0000" TopMargin="-43.9700" BottomMargin="378.9700" ctype="SpriteObjectData">
                <Size X="348.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="pri_sp_systemtip_3" ActionTag="-1937601451" Tag="6" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="104.1094" RightMargin="105.8906" TopMargin="18.1227" BottomMargin="10.8773" ctype="SpriteObjectData">
                    <Size X="138.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="173.1094" Y="28.3773" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4974" Y="0.4434" />
                    <PreSize X="0.3966" Y="0.5469" />
                    <FileData Type="PlistSubImage" Path="pri_sp_roominfotitle.png" Plist="room/room.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="367.0000" Y="410.9700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0300" />
                <PreSize X="0.4741" Y="0.1604" />
                <FileData Type="PlistSubImage" Path="plaza_image_title_bg.png" Plist="room/plazaScene.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_roomid_0" ActionTag="-283544076" Tag="7" IconVisible="False" LeftMargin="61.8378" RightMargin="592.1622" TopMargin="60.0000" BottomMargin="309.0000" FontSize="24" LabelText="房间ID:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="61.8378" Y="324.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0842" Y="0.8120" />
                <PreSize X="0.1090" Y="0.0752" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_roomid" ActionTag="-962898531" Tag="14" IconVisible="False" LeftMargin="149.1146" RightMargin="584.8854" TopMargin="75.0000" BottomMargin="324.0000" FontSize="24" LabelText="" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="149.1146" Y="324.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2032" Y="0.8120" />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_roomid_1" ActionTag="-1287556366" Tag="15" IconVisible="False" LeftMargin="61.8378" RightMargin="566.1622" TopMargin="114.0000" BottomMargin="255.0000" FontSize="24" LabelText="房间战绩:" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="106.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="61.8378" Y="270.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0842" Y="0.6767" />
                <PreSize X="0.1444" Y="0.0752" />
                <FontResource Type="Normal" Path="fonts/round_body.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="667.0000" Y="337.5000" />
            <Scale ScaleX="1.1700" ScaleY="1.1700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4500" />
            <PreSize X="0.5502" Y="0.5320" />
            <FileData Type="PlistSubImage" Path="plaza_common_bg.png" Plist="room/plazaScene.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>