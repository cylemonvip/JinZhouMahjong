//
//  HTTPManager.cpp
//  HTTPEx
//
//  Created by 程祎 on 2017/7/4.
//
//

#include "HTTPManager.h"

HTTPManager* HTTPManager::_instance = nullptr;

HTTPManager* HTTPManager::getInstance()
{
    if(!_instance)
    {
        _instance = new (std::nothrow)HTTPManager();
    }
    
    return _instance;
}

HTTPManager::HTTPManager()
{
    
}

HTTPManager::~HTTPManager()
{
    
}

void HTTPManager::downloadPicture(std::string url, const ccHttpRequestCallback& callback)
{
    HttpRequest *requese = new HttpRequest();
    requese->setRequestType(cocos2d::network::HttpRequest::Type::GET);
    requese->setTag("HeadPicRequest");
    requese->setUrl(url.c_str());
    requese->setResponseCallback(callback);
    HttpClient::getInstance()->sendImmediate(requese);
    requese->release();
}

void HTTPManager::onDPRequestComplete(network::HttpClient *sender, network::HttpResponse *response)
{
    if(response != nullptr)
    {
        if(response->isSucceed())
        {
            std::string fileName = FileUtils::getInstance()->getWritablePath() + std::string("wx_head.png");
            std::vector<char>* data = response->getResponseData();
            std::string buffer(data->begin(), data->end());
            
            FILE *fp = fopen(fileName.c_str(), "wb");
            fwrite(buffer.c_str(), 1, buffer.size(), fp);
            fclose(fp);
        }else
        {
            CCLOG("response fail error : %s", response->getErrorBuffer());
        }
    }else
    {
        CCLOG("response == nullptr");
    }
}

void HTTPManager::postHTTPRequest()
{
    HttpRequest *request = new (std::nothrow) HttpRequest();
    if(request)
    {
        request->setUrl("http://httpbin.org/post");
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(CC_CALLBACK_2(HTTPManager::onRequestComplete, this));
        
        std::string data = getJsonData();
        std::vector<std::string> headers;
        
        headers.push_back("Content-Type: application/json; charset=utf-8");
        request->setHeaders(headers);
        
        request->setRequestData(data.c_str(), data.size());
        request->setTag("POST TEST");
        HttpClient::getInstance()->send(request);
        
        request->release();
    }else
    {
        request = nullptr;
    }
}

void HTTPManager::onRequestComplete(network::HttpClient *sender, network::HttpResponse *response)
{
    CCLOG("onRequestComplete ------------- onRequestComplete");
    
    if(!response)
    {
        return;
    }
    
    CCLOG("Tag : %s, response code : %ld", response->getHttpRequest()->getTag(), response->getResponseCode());
    
    if(!response->isSucceed())
    {
        CCLOG("HTTP Request Error");
        return;
    }
    
    std::vector<char> *responseData = response->getResponseData();
    std::string responseStr = std::string(responseData->begin(), responseData->end());
    
    CCLOG("responseStr : %s", responseStr.c_str());
    
    rapidjson::Document document;
    document.Parse<0>(responseStr.c_str());
    
    CCASSERT(!document.HasParseError(), "parsing to document failed");
    
    if(document.IsObject())
    {
        if(document.HasMember("json")){
            const rapidjson::Value& val_form = document["json"];
            if(val_form.IsObject()){
                log("json:{");
                if(val_form.HasMember("ID")){
                    log("   ID: %d", val_form["ID"].GetInt());
                }
                
                if(val_form.HasMember("info")){
                    const rapidjson::Value& info = val_form["info"];
                    CC_ASSERT(info.IsArray());
                    log("   info: { ");
                    for(unsigned int i = 0; i < info.Size(); ++i){
                        // 获得一条记录对象
                        const rapidjson::Value& record = info[i];
                        CC_ASSERT(record.HasMember("no"));
                        log("       no: %d", record["no"].GetInt());
                        
                        CC_ASSERT(record.HasMember("content"));
                        log("       content: %s", record["content"].GetString());
                        
                    }
                }
                
                log("   } \n");
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

std::string HTTPManager::getJsonData()
{
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
    document.AddMember("ID", 123, allocator);
    
    rapidjson::Value array(rapidjson::kArrayType);
    rapidjson::Value object(rapidjson::kObjectType);
    object.AddMember("no", 1, allocator);
    object.AddMember("content", "hello", allocator);
    object.AddMember("state", true, allocator);
    array.PushBack(object, allocator);
    
    document.AddMember("info", array, allocator);
    
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    document.Accept(writer);
    
    return buffer.GetString();
}
