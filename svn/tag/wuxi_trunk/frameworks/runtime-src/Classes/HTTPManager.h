//
//  HTTPManager.h
//  HTTPEx
//
//  Created by 程祎 on 2017/7/4.
//
//

#ifndef HTTPManager_h
#define HTTPManager_h

#include <stdio.h>
#include "cocos2d.h"
#include <network/HttpClient.h>
#include <json/rapidjson.h>
#include <json/writer.h>
#include <json/document.h>
#include <json/stringbuffer.h>

using namespace cocos2d;
using namespace network;

class HTTPManager {
public:
    HTTPManager();
    ~HTTPManager();
    
    std::string getJsonData();
    void postHTTPRequest();
    void onRequestComplete(HttpClient *sender, HttpResponse *response);
    void downloadPicture(std::string url, const ccHttpRequestCallback& callback);
    void onDPRequestComplete(network::HttpClient *sender, network::HttpResponse *response);
private:
    static HTTPManager *_instance;
    
public:
    static HTTPManager* getInstance();
    
};

#endif /* HTTPManager_h */
