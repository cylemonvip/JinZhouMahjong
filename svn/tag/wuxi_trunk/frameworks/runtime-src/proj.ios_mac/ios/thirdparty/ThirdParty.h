//
//  ThirdParty.h
//  GloryProject
//
//  Created by zhong on 16/9/8.
//
//

#ifndef ThirdParty_h
#define ThirdParty_h

#import <Foundation/Foundation.h>
#import "ThirdDefine.h"
#import "ThirdProtocol.h"
#import "ThirdData.h"

#import "UMSocial.h"
#import "WXApi.h"
//#import <JftMergeSDK/JfPay.h> 11

// 高德定位服务
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <MAMapKit/MAMapKit.h>
#import <LinkedME_iOS/LMDeepLinkingController.h>

#import <LinkedME_iOS/LinkedME.h>
#import <LinkedME_iOS/LMUniversalObject.h>
#import <LinkedME_iOS/LMLinkProperties.h>

@interface ThirdParty: NSObject <UMSocialUIDelegate, WXApiDelegate, AMapLocationManagerDelegate> // JftSDKPayDelegate, 11
{
    //平台
    NSMutableArray* _platformArray;
    //友盟平台
    NSDictionary* _umDict;
    //友盟分享平台
    NSArray* _umSharePlat;
    //分享配置
    ShareConfig *_share;
    //微信配置
    WeChatConfig *_wechat;
    //支付宝配置
    AliPayConfig *_alipay;
    //竣付通配置
    JftPayConfig *_jftpay;
    
    //支付平台
    NSString* _payPlat;
    
    BOOL m_bConfigAmap;
    //通过打开分享得到的房间号
    BOOL _isShared;
    NSString* _gameID;
    NSString* _roomID;
    NSString* _inviterUserID;
    NSString* _channelID;

    NSString* LINKEDME_SHORT_URL;
    id<ShareDelegate> _shareDelegate;
    NSDictionary* _shareDict;
}
@property (nonatomic, assign) id <ShareDelegate> shareDelegate;
@property (nonatomic, assign) id <PayDelegate> payDelegate;
@property (nonatomic, assign) id <LocationDelegate> locationDelegate;
@property (nonatomic, strong) AMapLocationManager *locationMgr;
@property (nonatomic, copy) AMapLocatingCompletionBlock completionBlock;

@property (strong, nonatomic) LMUniversalObject *linkedUniversalObject;

+ (ThirdParty*) getInstance;
+ (void) destroy;

//初始化
- (void) defaultInit;

// ======================== UMeng 统计 ================================= begin
//初始化UMeng统计
- (void) initUMengAnalyicsWithAppID:(NSString*) appid withChannel:(NSString*) channel;

//页面访问统计
- (void) logPageViewWithName:(NSString*) name isEnter:(BOOL) isEnter;

//计数统计
- (void) countEvent:(NSString*) eventName withAttributes:(NSDictionary*) attributes;

//计算统计
- (void) calculateEvent:(NSString*) eventName withAttributes:(NSDictionary*) attributes withCounter:(NSString*) counter;

// ======================== UMeng 统计 ================================= end


// ======================== LinkedMe ================================== begin

// ======================== LinkedMe ================================== end


//获取平台
- (NSString*) getPlatform: (int) nidx;

//获取分享默认配置
- (ShareConfig*) getDefaultShareConfig;

//获取链接参数
- (NSString *) paramValueOfUrl:(NSString *) url withParam:(NSString *) param;
//打开链接
- (BOOL) openURL:(NSURL *)url;

//获取游戏号
- (NSString*) getSharedGameID;
//获取房间号
- (NSString*) getSharedRoomID;

- (NSString*) getInviterUserID;

//渠道号
- (NSString*) getChannelID;

- (void) onMWEvent:(NSDictionary*) dict;
- (void) onEventUseRoomCard:(NSDictionary*) dict;

//
- (void) willEnterForegound;

//
- (void) didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

//
- (BOOL) continueUserActivity :(NSUserActivity *)userActivity ;

//配置第三方平台数据
- (void) configThirdParty:(NSString*)plat platConfig:(NSDictionary*)config;

//配置分享数据
- (void) configSocialShare:(NSDictionary*) config;

//第三方登陆
- (void) thirdPartyLogin:(NSString*)plat delegate:(id<LoginDelegate>) delegate;

//分享
- (void) openShare:(id<ShareDelegate>) delegate share:(tagShareParam) param;

//指定平台分享
- (void) targetShare:(id<ShareDelegate>) delegate share:(tagShareParam) param dict:(NSDictionary*) dict;

//支付
- (void) thirdPartyPay:(NSString*)plat delegate:(id<PayDelegate>)delegate payparam:(NSDictionary*)payparam;

//获取竣付通支付列表
- (void) getPayList:(NSString*)token delegate:(id<PayDelegate>)delegate;

// 判断是否安装
- (BOOL) isPlatformInstalled:(NSString *)plat;

// 请求单次定位
- (void) requestLocation: (id<LocationDelegate>)delegate;

// 根据经纬度计算距离
- (NSString*) metersBetweenLocation: (NSDictionary*)loParam;
@end
#endif /* ThirdParty_h */
