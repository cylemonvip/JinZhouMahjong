//
//  ThirdParty.cpp
//  GloryProject
//
//  Created by zhong on 16/9/8.
//
//

#import "ThirdParty.h"

#import "AppController.h"
#import "RootViewController.h"

//友盟
#import "UMSocialWechatHandler.h"
#import "UMMobClick/MobClick.h"

#include "cocos2d.h"
#include "json/document.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

#import <LinkedME_iOS/LinkedME.h>
#import <LinkedME_iOS/LMUniversalObject.h>
#import <LinkedME_iOS/LMLinkProperties.h>

@implementation ThirdParty
static ThirdParty* s_instance = nil;
+ (ThirdParty*) getInstance
{
    if (nil == s_instance)
    {
        s_instance = [ThirdParty alloc];
        [s_instance defaultInit];
    }
    return s_instance;
}

+ (void) destroy
{
    if (nil != s_instance)
    {
        [s_instance->_platformArray release];
        [s_instance->_umDict release];
        [s_instance->_umSharePlat release];
        [s_instance->_share release];
        [s_instance->_wechat release];
        [s_instance->_alipay release];
        [s_instance->_jftpay release];
        s_instance->_completionBlock = nil;
        
        [s_instance release];
    }
}
@synthesize shareDelegate = _shareDelegate;

#pragma mark UMSocial
-(void)didCloseUIViewController:(UMSViewControllerType)fromViewControllerType
{
    
}

-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    if (nil == _shareDelegate)
    {
        return;
    }
    
    if (response.responseCode == UMSResponseCodeCancel)
    {
        [_shareDelegate onCancel:INVALIDPLAT];
    }
    else if (response.responseCode == UMSResponseCodeSuccess)
    {
        [_shareDelegate onComplete:INVALIDPLAT backCode:response.responseCode backMsg:response.message];
    }
    else
    {
        [_shareDelegate onError:INVALIDPLAT backMsg:response.message];
    }
    [self setShareDelegate:nil];
}
#pragma mark -

#pragma mark -
#pragma mark WXApiDelegate

-(void) onReq:(BaseReq*)req
{
    
}
-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[PayResp class]])
    {
        //支付返回结果，实际支付结果需要去微信服务器端查询
        //NSString *strMsg,*strTitle = [NSString stringWithFormat:@"充值结果"];
        
        switch (resp.errCode)
        {
            case WXSuccess:
                NSLog(@"充值成功");
                [self onPayResult:TRUE msg:@""];
                break;
                
            default:
                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
                [self onPayResult:FALSE msg:resp.errStr];
                break;
        }
    }
}
#pragma mark -

#pragma mark -
#pragma mark JftSDKPayDelegate
//获取支付列表成功
- (void)getPayTypeListSuccess:(NSArray *)list
{
    rapidjson::Document doc;
    doc.SetArray();
    rapidjson::Document::AllocatorType &alloc = doc.GetAllocator();
    for (NSDictionary *dic : list)
    {
        NSString *typeID = [dic objectForKey:@"typeid"];
        doc.PushBack(rapidjson::Value([typeID UTF8String], alloc), alloc);
    }
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    doc.Accept(writer);
    if (nil != _payDelegate)
    {
        [_payDelegate onGetPayList:TRUE backMsg:[NSString stringWithCString:buffer.GetString() encoding:NSUTF8StringEncoding]];
    }
}

//获取支付列表失败
- (void)getPayTypeListFailure:(NSString *)message
{
    if (nil != _payDelegate)
    {
        [_payDelegate onGetPayList:FALSE backMsg:@"获取支付列表失败"];
    }
    [self setPayDelegate:nil];
}

//支付成功
- (void)jftPaySuccess
{
    //充值成功
    [self onPayResult:TRUE msg:@""];
}

//支付失败
- (void)jftPayFailure:(NSString *)message
{
    //充值失败
    [self onPayResult:FALSE msg:@""];
}

//支付结果
- (void)jftPayResult:(NSString *)result
{
    NSLog(@"pay result:%@",result);
}

//应用打开成功
- (void)thirdAppOpenSucceed;
{
    
}

//应用打开失败
- (void)thirdAppOpenFailure
{
    if (nil != _payDelegate)
    {
        [_payDelegate onPayNotify:@"" backMsg:@"第三方应用打开失败"];
    }
}

//pcsoama
- (void)openWebSuccessed
{
    
}

- (void)openWebFailer:(NSString *)failer
{
    if (nil != _payDelegate)
    {
        [_payDelegate onPayNotify:@"" backMsg:failer];
    }
}
#pragma mark -

- (void) defaultInit
{
    //bugly
//    [BuglyAgent class];
//    [Bugly sdkVersion];
    
    _shareDelegate = nil;
    _payDelegate = nil;
    _payPlat = INVALIDPLAT;
    
    //配置第三方平台
    _platformArray = [NSMutableArray arrayWithCapacity:20];
    [_platformArray insertObject:WECHAT atIndex:0];
    [_platformArray insertObject:WECHAT_CIRCLE atIndex:1];
    [_platformArray insertObject:ALIPAY atIndex:2];
    [_platformArray insertObject:JFT atIndex:3];
    [_platformArray insertObject:AMAP atIndex:4];
    [_platformArray insertObject:IOSIAP atIndex:5];
    [_platformArray insertObject:SMS atIndex:6];
    [_platformArray retain];
    
    //配置友盟平台
    _umDict = [NSDictionary dictionaryWithObjectsAndKeys:
               UMShareToWechatSession, WECHAT,
               UMShareToWechatTimeline, WECHAT_CIRCLE,
               UMShareToAlipaySession, ALIPAY,
               UMShareToSms, SMS,
               nil];
    [_umDict retain];
    
    //友盟分享平台
    _umSharePlat = [NSArray arrayWithObjects:UMShareToWechatSession, UMShareToWechatTimeline, nil];
    [_umSharePlat retain];
    
    _share = [ShareConfig alloc];
    [_share retain];
    _wechat = [WeChatConfig alloc];
    [_wechat retain];
    _alipay = [AliPayConfig alloc];
    [_alipay retain];
    _jftpay = [JftPayConfig alloc];
    [_jftpay retain];
    
    _locationDelegate = nil;
    m_bConfigAmap = FALSE;
    _completionBlock = nil;
    
    _gameID = 0;
    _roomID = 0;
    
}

// ======================== UMeng 统计 ================================= begin
//初始化UMeng统计
- (void) initUMengAnalyicsWithAppID:(NSString*) appid withChannel:(NSString*) channel
{
    //友盟统计
    UMConfigInstance.appKey = appid;
    UMConfigInstance.channelId = channel;
    [MobClick startWithConfigure:UMConfigInstance];
}

//页面访问统计
- (void) logPageViewWithName:(NSString*) name isEnter:(BOOL) isEnter
{
    if(isEnter)
    {
        [MobClick beginLogPageView:name];
    }else
    {
        [MobClick endLogPageView:name];
    }
}

//计数统计
- (void) countEvent:(NSString*) eventName withAttributes:(NSDictionary*) attributes
{
    if(attributes.count > 0)
    {
        [MobClick event:eventName attributes:attributes];
    }else{
        [MobClick event:eventName];
    }
}

//计算统计
- (void) calculateEvent:(NSString*) eventName withAttributes:(NSDictionary*) attributes withCounter:(NSString*) counter
{
    [MobClick event:eventName attributes:attributes counter:[counter intValue]];
}
// ======================== UMeng 统计 ================================= end

- (void)didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //初始化及实例
    LinkedME* linkedme = [LinkedME getInstance];
    //打印日志
    [linkedme setDebug];
        
    NSString* testID = [LinkedME getTestID];
    NSLog(@"testID = %@", testID);
    
    //获取跳转参数
    [linkedme initSessionWithLaunchOptions:launchOptions automaticallyDisplayDeepLinkController:NO deepLinkHandler:^(NSDictionary* params, NSError* error) {
        if (!error) {
            //防止传递参数出错取不到数据,导致App崩溃这里一定要用try catch
            @try {
                NSLog(@"LinkedME finished init with params = %@",[params description]);
                //获取详情页类型(如新闻客户端,有图片类型,视频类型,文字类型等)
                //NSString *title = [params objectForKey:@"$og_title"];
//                {"$control":{"GameID":"0","RoomID":"000000"}}
                NSString *gameID = params[@"$control"][@"gameID"];
                NSString *roomID = params[@"$control"][@"roomID"];
                NSString *inviterUserID = params[@"$control"][@"inviterUserID"];
                NSString *channelID = params[@"$control"][@"channelID"];
                
                if (gameID && gameID.length > 0 && roomID && roomID.length > 0) {
                    _gameID = gameID;
                    _roomID = roomID;
                }
                if(inviterUserID)
                {
                    _inviterUserID = inviterUserID;
                }
                if(channelID)
                {
                    _channelID = channelID;
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
            
        } else {
            NSLog(@"LinkedME failed init: %@", error);
        }
    }];
}

- (NSString*) getChannelID
{
    if (_channelID && [_channelID compare:@"0"] != NSOrderedSame && [_channelID compare:@""] != NSOrderedSame) {
        return _channelID;
    }
    return @"0";
}

//获取游戏号
- (NSString*) getSharedGameID
{
    NSString* ret = _gameID;
    _gameID = nil;
    if([ret compare:@"0"] == NSOrderedSame )
    {
        return @"";
    }
    return ret;
}

//获取房间号
- (NSString*) getSharedRoomID
{
    NSString* ret = _roomID;
    _roomID = nil;
    if([ret compare:@"0"] == NSOrderedSame )
    {
        return @"";
    }
    return ret;
}

- (NSString*) getInviterUserID
{
    NSString* ret = _inviterUserID;
    if (ret && [ret compare:@"0"] != NSOrderedSame && [ret compare:@""] != NSOrderedSame) {
        return ret;
    }
    return @"0";
}

-(void) onMWEvent:(NSDictionary*) dict
{

}

- (void) onEventUseRoomCard:(NSDictionary*) dict
{
}

- (NSString*) getPlatform:(int)nidx
{
    if(nidx > [_platformArray count])
    {
        return INVALIDPLAT;
    }
    return [_platformArray objectAtIndex:nidx];
}

- (ShareConfig*) getDefaultShareConfig
{
    return _share;
}

- (NSString *) paramValueOfUrl:(NSString *) url withParam:(NSString *) param{
    
    NSError *error;
    NSString *regTags=[[NSString alloc] initWithFormat:@"(^|&|\\?)+%@=+([^&]*)(&|$)",param];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regTags
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    // 执行匹配的过程
    NSArray *matches = [regex matchesInString:url
                                      options:0
                                        range:NSMakeRange(0, [url length])];
    for (NSTextCheckingResult *match in matches) {
        NSString *tagValue = [url substringWithRange:[match rangeAtIndex:2]];  // 分组2所对应的串
        return tagValue;
    }
    return nil;
}

- (BOOL) openURL:(NSURL *)url
{
    BOOL bRes = [UMSocialSnsService handleOpenURL:url];
    if (FALSE == bRes)
    {
        NSString *pstr = [[NSString alloc] initWithFormat:@"%@://pay", _wechat.WeChatAppID];
        if ([[url absoluteString] rangeOfString:pstr].location == 0)
        {
            bRes = [WXApi handleOpenURL:url delegate:self];
        }
    }
    
    if(FALSE == bRes)
    {
        //判断是否是通过LinkedME的UrlScheme唤起App
        if ([[url description] rangeOfString:@"click_id"].location != NSNotFound) {
            bRes = [[LinkedME getInstance] handleDeepLink:url];
        }
    }
    
    return bRes;
}

-(NSString *) getParamFromUrl:(NSString *)paramName webaddress:(NSString *)url
{
    NSError *error;
    NSString *regTags=[[NSString alloc] initWithFormat:@"(^|&|\\?)+%@=+([^&]*)(&|$)", paramName];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regTags
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    // 执行匹配的过程
    NSArray *matches = [regex matchesInString:url
                                      options:0
                                        range:NSMakeRange(0, [url length])];
    for (NSTextCheckingResult *match in matches) {
        NSString *tagValue = [url substringWithRange:[match rangeAtIndex:2]];  // 分组2所对应的串
        return tagValue;
    }
    return @"";
}

- (BOOL) continueUserActivity:(NSUserActivity *)userActivity
{
    //判断是否是通过LinkedME的Universal Links唤起App
    if ([[userActivity.webpageURL description] rangeOfString:@"lkme.cc"].location != NSNotFound) {
        return  [[LinkedME getInstance] continueUserActivity:userActivity];
    }
    return false;
}

- (void) willEnterForegound
{

}

- (void) configThirdParty:(NSString*)plat platConfig:(NSDictionary*)config
{
    BOOL bRes = FALSE;
    if ([plat isEqualToString:WECHAT])
    {
        [_wechat setWeChatAppID:[config objectForKey:@"AppID"]];
        bRes = (_wechat.WeChatAppID != nil);
        [_wechat setWeChatAppSecret:[config objectForKey:@"AppSecret"]];
        bRes = (_wechat.WeChatAppSecret != nil);
        [_wechat setWeChatPartnerID:[config objectForKey:@"PartnerID"]];
        bRes = (_wechat.WeChatPartnerID != nil);
        [_wechat setWeChatPayKey:[config objectForKey:@"PayKey"]];
        bRes = (_wechat.WeChatPayKey != nil);
        [_wechat setWeChatURL:[config objectForKey:@"URL"]];
        bRes = (_wechat.WeChatURL != nil);
        [_wechat setConfiged:bRes];
        
        if (TRUE == _wechat.Configed)
        {
            [UMSocialWechatHandler setWXAppId:_wechat.WeChatAppID appSecret:_wechat.WeChatAppSecret url:_wechat.WeChatURL];
        }
    }
    else if ([plat isEqualToString:ALIPAY])
    {
        [_alipay setAlipayPartnerID:[config objectForKey:@"PartnerID"]];
        bRes = (_alipay.AlipayPartnerID != nil);
        [_alipay setAlipaySeller:[config objectForKey:@"SellerID"]];
        bRes = (_alipay.AlipaySeller != nil);
        [_alipay setAlipayPrivate:[config objectForKey:@"RsaKey"]];
        bRes = (_alipay.AlipayPrivate != nil);
        [_alipay setAlipayNotifyUrl:[config objectForKey:@"NotifyURL"]];
        bRes = (_alipay.AlipayNotifyUrl != nil);
        [_alipay setAliPaySchemes:[config objectForKey:@"AliSchemes"]];
        bRes = (_alipay.AliPaySchemes != nil);
        [_alipay setConfiged:bRes];
    }
    else if ([plat isEqualToString:JFT])
    {
        [_jftpay setJftPayKey:[config objectForKey:@"PayKey"]];
        bRes = (_jftpay.JftPayKey != nil);
        [_jftpay setJftPayPartnerID:[config objectForKey:@"PartnerID"]];
        bRes = (_jftpay.JftPayPartnerID != nil);
        [_jftpay setJftAppID:[config objectForKey:@"JftAppID"]];
        bRes = (_jftpay.JftAppID != nil);
        [_jftpay setJftPayAesKey:[config objectForKey:@"JftAesKey"]];
        bRes = (_jftpay.JftPayAesKey != nil);
        [_jftpay setJftPayAesVec:[config objectForKey:@"JftAesVec"]];
        bRes = (_jftpay.JftPayAesVec != nil);
        [_jftpay setConfiged:bRes];
        
//        [JfPay setLogEnable:YES]; 11
    }
    else if ([plat isEqualToString:AMAP])
    {
        NSString *k = [config objectForKey:@"AmapKeyIOS"];
        if (nil != k)
        {
            m_bConfigAmap = TRUE;
            [[AMapServices sharedServices] setEnableHTTPS:TRUE];
            [AMapServices sharedServices].apiKey = k;
            
            self.locationMgr = [[AMapLocationManager alloc] init];
            [self.locationMgr setDelegate:self];
            //设置期望定位精度
            [self.locationMgr setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
            //设置不允许系统暂停定位
            [self.locationMgr setPausesLocationUpdatesAutomatically:NO];
            //设置允许在后台定位
            [self.locationMgr setAllowsBackgroundLocationUpdates:YES];
            //设置定位超时时间
            [self.locationMgr setLocationTimeout:6];
            //设置逆地理超时时间
            [self.locationMgr setReGeocodeTimeout:3];
            
            /*
            // 定位回调
            self.completionBlock = ^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error)
            {
                if (error)
                {
                    NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
//                    error.localizedDescription = ;
                    //如果为定位失败的error，则不进行annotation的添加
                    if (error.code == AMapLocationErrorLocateFailed)
                    {
                        if (nil != _locationDelegate)
                        {
                            //TODO 定位失败报错暂时屏蔽
//                            [_locationDelegate onLocationResult:FALSE backCode:(int)error.code backMsg: @"定位失败，请打开定位服务"];
                        }
                        return;
                    }
                }
                
                //得到定位信息，添加annotation
                if (location)
                {
                    MAPointAnnotation *annotation = [[MAPointAnnotation alloc] init];
                    [annotation setCoordinate:location.coordinate];
                    
                    if (regeocode)
                    {
                        [annotation setTitle:[NSString stringWithFormat:@"%@", regeocode.formattedAddress]];
                        [annotation setSubtitle:[NSString stringWithFormat:@"%@-%@-%.2fm", regeocode.citycode, regeocode.adcode, location.horizontalAccuracy]];
                    }
                    else
                    {
                        [annotation setTitle:[NSString stringWithFormat:@"lat:%f;lon:%f;", location.coordinate.latitude, location.coordinate.longitude]];
                        [annotation setSubtitle:[NSString stringWithFormat:@"accuracy:%.2fm", location.horizontalAccuracy]];
                    }
                    if (nil != _locationDelegate)
                    {
                        rapidjson::Document doc;
                        doc.SetObject();
                        rapidjson::Document::AllocatorType &alloc = doc.GetAllocator();
                        rapidjson::Value("",alloc);
                        doc.AddMember("latitude", rapidjson::Value(location.coordinate.latitude), alloc);
                        doc.AddMember("longitude", rapidjson::Value(location.coordinate.longitude), alloc);
                        doc.AddMember("accuracy", rapidjson::Value(location.horizontalAccuracy), alloc);
                        rapidjson::StringBuffer buffer;
                        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
                        doc.Accept(writer);
                        
                        [_locationDelegate onLocationResult:TRUE backCode:(int)error.code backMsg:[NSString stringWithCString:buffer.GetString() encoding:NSUTF8StringEncoding]];
                    }
                }
            };*/
        }
    }
}

- (void) configSocialShare:(NSDictionary *)config
{
    BOOL bConfiged = FALSE;
    [_share setShareTitle:[config objectForKey:@"title"]];
    bConfiged = (_share.ShareTitle != nil);
    [_share setShareContent:[config objectForKey:@"content"]];
    bConfiged = (_share.ShareContent != nil);
    [_share setShareUrl:[config objectForKey:@"url"]];
    bConfiged = (_share.ShareUrl != nil);
    [_share setAppKey:[config objectForKey:@"AppKey"]];
    bConfiged = (_share.AppKey != nil);
    [_share setConfiged:bConfiged];
    
    if (_share.Configed)
    {
        [UMSocialData setAppKey:_share.AppKey];
        [UMSocialConfig setSupportedInterfaceOrientations:UIInterfaceOrientationMaskLandscape];
    }
}

- (void) thirdPartyLogin:(NSString*)plat delegate:(id<LoginDelegate>) delegate
{
    //判断友盟平台
    NSString *platstr = [_umDict objectForKey:plat];
    if (nil != platstr)
    {
        //判断是否配置
        if (FALSE == _wechat.Configed || FALSE == _share.Configed)
        {
            [delegate onLoginFail:platstr backMsg:@"did not config platform"];
            return;
        }
        
        //判断是否安装微信
        if (FALSE == [WXApi isWXAppInstalled])
        {
            [delegate onLoginFail:platstr backMsg:@"微信客户端未安装,无法进行授权登陆!"];
            return;
        }
        
        //是否授权(微信暂时需要重新授权)
        if ([UMSocialAccountManager isOauthWithPlatform:platstr] && FALSE == [WECHAT isEqualToString:plat])
        {
            [self getAuthorizedUserInfo:platstr delegate:delegate];
            return;
        }
        
        UMSocialSnsPlatform *snsplat = nil;
        //微信登陆
        if ([WECHAT isEqualToString:plat])
        {
            snsplat = [UMSocialSnsPlatformManager getSocialPlatformWithName:platstr];
        }
        
        //账户信息
        if (nil != snsplat)
        {
            AppController *pApp = (AppController*)[[UIApplication sharedApplication] delegate];
            [UMSocialConfig setSupportedInterfaceOrientations:UIInterfaceOrientationMaskAll];
            snsplat.loginClickHandler(pApp->viewController, [UMSocialControllerService defaultControllerService], YES, ^(UMSocialResponseEntity *response)
                                      {
                                          [self parseAuthorizeData:platstr response:response delegate:delegate];
                                      });
        }
    }
    else
    {
        [delegate onLoginSuccess:INVALIDPLAT backMsg:@""];
    }
}

- (void) getAuthorizedUserInfo:(NSString *)platstr delegate:(id<LoginDelegate>) delegate
{
    [[UMSocialDataService defaultDataService] requestSocialAccountWithCompletion:^(UMSocialResponseEntity *response)
     {
         [self parseAuthorizeData:platstr response:response delegate:delegate];
     }];
}

- (void) parseAuthorizeData:(NSString *)platstr response:(UMSocialResponseEntity *)response delegate:(id<LoginDelegate>) delegate
{
    if (response.responseCode == UMSResponseCodeSuccess)
    {
        //添加同安卓一致信息
        [response.thirdPlatformUserProfile setValue:[response.thirdPlatformUserProfile objectForKey:@"nickname"] forKey:@"screen_name"];
        [response.thirdPlatformUserProfile setValue:[response.thirdPlatformUserProfile objectForKey:@"headimgurl"] forKey:@"profile_image_url"];
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:response.thirdPlatformUserProfile
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        if (nil != jsonData)
        {
            NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            //NSLog(@"json : %@",jsonString);
            [delegate onLoginSuccess:platstr backMsg:jsonString];
            [jsonString release];
        }
        else
        {
            NSLog(@"to json error:%@", error);
            [delegate onLoginFail:platstr backMsg:@""];
        }
    }
    else if(response.responseCode == UMSResponseCodeCancel)
    {
        [delegate onLoginCancel:platstr backMsg:@""];
    }
    else
    {
        [delegate onLoginFail:platstr backMsg:@""];
    }
}

- (void) openShare:(id<ShareDelegate>)delegate share:(tagShareParam)param
{
    [self setShareDelegate:delegate];
    AppController *pApp = (AppController*)[[UIApplication sharedApplication] delegate];
    
    if (TRUE == param.bImageOnly)
    {
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeImage;
    }
    else
    {
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeNone;
        [UMSocialWechatHandler setWXAppId:_wechat.WeChatAppID appSecret:_wechat.WeChatAppSecret url: param.sTargetURL];
    }
    [UMSocialData defaultData].extConfig.wechatSessionData.title = param.sTitle;
    
    [UMSocialSnsService presentSnsIconSheetView:pApp->viewController
                                         appKey:_share.AppKey
                                      shareText:param.sContent
                                     shareImage:[self getShareImage:param]
                                shareToSnsNames:[NSArray arrayWithArray:_umSharePlat]
                                       delegate:self];
}

+ (tagShareParam) getShareParam:(NSDictionary *)dict
{
    ShareConfig* share = [[ThirdParty getInstance] getDefaultShareConfig];
    NSString* title = [dict objectForKey:@"title"];
    if (nil == title)
    {
        title = share.ShareTitle;
    }
    NSString* content = [dict objectForKey:@"content"];
    if (nil == content)
    {
        content = share.ShareContent;
    }
    NSString* url = [dict objectForKey:@"url"];
    if (nil == url)
    {
        url = share.ShareUrl;
    }
    NSString* img = [dict objectForKey:@"img"];
    if (nil == img)
    {
        img = share.ShareMediaPath;
    }
    NSString* imageOnly = [dict objectForKey:@"imageOnly"];
    BOOL bImageOnly = FALSE;
    if (nil != imageOnly && [imageOnly isEqualToString:@"true"])
    {
        bImageOnly = TRUE;
    }
    
    //标签
    NSString* tag = [dict objectForKey:@"tag"];
    if (nil == tag)
    {
        tag = @"";
    }
    //游戏ID
    NSString* gameID = [dict objectForKey:@"gameID"];
    if (nil == gameID)
    {
        gameID = @"";
    }
    
    //房间ID
    NSString* roomID = [dict objectForKey:@"roomID"];
    if (nil == roomID)
    {
        roomID = @"";
    }
    
    NSString* targetPlatfm = [dict objectForKey:@"target"];
    if (nil == targetPlatfm || [targetPlatfm intValue] < 0 || [targetPlatfm intValue] > 10 )
    {
        targetPlatfm = @"0";
    }
    
    struct tagShareParam param = {[targetPlatfm intValue], title, content, url, img, bImageOnly, tag, gameID, roomID};
    return param;
}

- (void) shareTest:(id<ShareDelegate>)delegate{
    AppController *pApp = (AppController*)[[UIApplication sharedApplication] delegate];
//    _shareDict
    struct tagShareParam param = [ThirdParty getShareParam:_shareDict];
    param.sTargetURL = LINKEDME_SHORT_URL;
    
    NSString *plat = [self getPlatform:param.nTarget];
    NSString *platstr = [_umDict objectForKey:plat];
    [self setShareDelegate:delegate];
    

    if (TRUE == param.bImageOnly)
    {
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeImage;
    }
    else
    {
        [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeNone;
        [UMSocialWechatHandler setWXAppId:_wechat.WeChatAppID appSecret:_wechat.WeChatAppSecret url: param.sTargetURL];
    }
    [UMSocialData defaultData].extConfig.wechatSessionData.title = param.sTitle;

    [[UMSocialDataService defaultDataService] postSNSWithTypes:@[platstr]
                                                       content:param.sContent
                                                         image:[self getShareImage:param]
                                                      location:nil
                                                   urlResource:nil
                                           presentedController:pApp->viewController
                                                    completion:^(UMSocialResponseEntity *response)
    {
            if (response.responseCode == UMSResponseCodeCancel)
            {
                [_shareDelegate onCancel:INVALIDPLAT];
            }
            else if (response.responseCode == UMSResponseCodeSuccess)
            {
                [_shareDelegate onComplete:INVALIDPLAT backCode:response.responseCode backMsg:response.message];
            }
            else
            {
                [_shareDelegate onError:INVALIDPLAT backMsg:response.message];
            }
            [self setShareDelegate:nil];
        }];
}

- (void) targetShare:(id<ShareDelegate>)delegate share:(tagShareParam)param dict:(NSDictionary*) dict
{
    //生成短连接前，保存参数
    _shareDelegate = delegate;
    _shareDict = dict;
    [_shareDict retain];
    
    self.linkedUniversalObject = [[LMUniversalObject alloc] init];
    self.linkedUniversalObject.title = [NSString stringWithFormat:@"%@-%@", param.channelID, param.tag];//标题
    LMLinkProperties *linkProperties = [[LMLinkProperties alloc] init];
    linkProperties.channel = param.channelID;//渠道(微信,微博,QQ,等...)
    linkProperties.feature = @"Share";//特点
    NSString* tag = [NSString stringWithFormat:@"%@", param.tag];
    linkProperties.tags=@[param.channelID, tag];//标签
    linkProperties.stage = @"Live";//阶段
    
    NSString* gameID = param.gameID;
    NSString* roomID = param.roomID;
    NSString* inviterUserID = param.tag;
    NSString* channelID = param.channelID;
    
    int iGameID = [gameID intValue];
    int iRoomID = [roomID intValue];
    int iInviterUserID = [inviterUserID intValue];
    
    NSString *newGameID = [NSString stringWithFormat:@"%d",iGameID];
    NSString *newRoomID = [NSString stringWithFormat:@"%d",iRoomID];
    NSString *newInviterUserID = [NSString stringWithFormat:@"%d",iInviterUserID];
    
    [linkProperties addControlParam:@"gameID" withValue: newGameID];//页面唯一标识 param.gameID
    [linkProperties addControlParam:@"roomID" withValue: newRoomID];//Demo标识 param.roomID
    [linkProperties addControlParam:@"inviterUserID" withValue: newInviterUserID];
    [linkProperties addControlParam:@"channelID" withValue: channelID];
    
    
    NSLog(@"targetShare linkedme = %@" , param.sTargetURL);
    
    //如果有内置分享链接，则直接分享指定链接
    if([param.sTargetURL compare:@""] != NSOrderedSame)
    {
        [[ThirdParty getInstance] shareTest:_shareDelegate];
    }else
    {
        //开始请求短链
        [self.linkedUniversalObject getShortUrlWithLinkProperties:linkProperties andCallback:^(NSString *url, NSError *err) {
            NSString* H5_LIVE_URL = @"";
            LINKEDME_SHORT_URL = @"";
            if (url) {
                NSLog(@"[LinkedME Info] SDK creates the url is:%@", url);
                if([param.sTargetURL compare:@""] != NSOrderedSame)
                {
                    H5_LIVE_URL = [param.sTargetURL stringByAppendingString:@"linkedme="];
                }else
                {
                    H5_LIVE_URL = param.sTargetURL;
                }
                //拼接连接 https://www.linkedme.cc/h5/feature?linkedme= 中间承接h5
                //前面是Html5页面,后面拼上深度链接https://xxxxx.xxx (html5 页面地址) ?linkedme=(深度链接)    https://lkme.cc/iFD/cSRpYfxhI
                //https://www.linkedme.cc/h5/feature?linkedme=https://lkme.cc/AfC/mj9H87tk7
                LINKEDME_SHORT_URL = [H5_LIVE_URL stringByAppendingString:url];
            } else {
                LINKEDME_SHORT_URL = H5_LIVE_URL;
            }
            
            if([LINKEDME_SHORT_URL compare:@""] != NSOrderedSame)
            {
                [[ThirdParty getInstance] shareTest:_shareDelegate];
            }
            NSLog(@"LINKEDME_SHORT_URL = %@", LINKEDME_SHORT_URL);
        }];
    }
}

- (id) getShareImage:(tagShareParam)param
{
    id image = nil;
    NSString *imageString = param.sMedia;
    if ([imageString hasPrefix:@"http://"] || [imageString hasPrefix:@"https://"])
    {
        [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:imageString];
    }
    else
    {
        if (FALSE == [param.sMedia isEqualToString:@""])
        {
            NSString *imageString = param.sMedia;
            if ([imageString.lowercaseString hasSuffix:@".gif"])
            {
                NSString *path = [[NSBundle mainBundle] pathForResource:[[imageString componentsSeparatedByString:@"."] objectAtIndex:0]
                                                                 ofType:@"gif"];
                image = [NSData dataWithContentsOfFile:path];
            }
            else if ([imageString rangeOfString:@"/"].length > 0)
            {
                image = [NSData dataWithContentsOfFile:imageString];
            }
            else
            {
                image = [UIImage imageNamed:imageString];
            }
            [UMSocialData defaultData].urlResource.resourceType = UMSocialUrlResourceTypeDefault;
        }
        else
        {
            image = [UIImage imageNamed:@"Icon-72.png"];
        }
    }
    return image;
}

- (void) thirdPartyPay:(NSString *)plat delegate:(id<PayDelegate>)delegate payparam:(NSDictionary *)payparam
{
    
    _payPlat = plat;
    [self setPayDelegate:delegate];
    
    if ([WECHAT isEqualToString:plat])
    {
        [self doWeChatPay:[payparam objectForKey:@"info"]];
    }
    else if ([ALIPAY isEqualToString:plat])
    {
        //解析支付参数
        BOOL bRes = FALSE;
        NSString *sOrderId = [payparam objectForKey:@"orderid"];
        bRes = (sOrderId != nil);
        NSString *sPrice = [payparam objectForKey:@"price"];
        bRes = (sPrice != nil);
        NSString *sProducName = [payparam objectForKey:@"name"];
        bRes = (sProducName != nil);
        if (FALSE == bRes)
        {
            [delegate onPayFail:plat backMsg:@"订单数据解析失败"];
            return;
        }
        tagPayParam param = {sOrderId, sProducName, [sPrice floatValue], 1};
        [self doAliPay:param];
    }
    else if ([JFT isEqualToString:plat])
    {
        [self doJftPay:payparam];
    }
    else if ([IOSIAP isEqualToString:plat])
    {
        [self doIapPay:payparam];
    }
}

- (void) getPayList:(NSString *)token delegate:(id<PayDelegate>)delegate
{
    if (FALSE == _jftpay.Configed)
    {
        [delegate onGetPayList:FALSE backMsg:@"竣付通配置异常"];
        return;
    }
    [self setPayDelegate:delegate];
//    [JfPay getPayTypeList:token key:_jftpay.JftPayAesKey iv:_jftpay.JftPayAesVec serviceType:@"jft" appId:_jftpay.JftAppID delegate:self]; 11
}

- (BOOL) isPlatformInstalled:(NSString *)plat
{
    if ([WECHAT isEqualToString:plat])
    {
        return [WXApi isWXAppInstalled];
    }
    else if ([ALIPAY isEqualToString:plat])
    {
        NSURL *alipayUrl = [NSURL URLWithString:@"alipay:"];
        return [[UIApplication sharedApplication] canOpenURL:alipayUrl];
    }
    return FALSE;
}

- (void) requestLocation:(id<LocationDelegate>)delegate
{
    [self setLocationDelegate:delegate];
    if (FALSE == m_bConfigAmap)
    {
        [delegate onLocationResult:FALSE backCode:-1 backMsg:@"定位服务未配置!"];
        return;
    }
    [self.locationMgr requestLocationWithReGeocode:NO completionBlock:self.completionBlock];
}

- (NSString*) metersBetweenLocation:(NSDictionary *)loParam
{
    // 自己坐标
    double myLatitude = [[loParam objectForKey:@"myLatitude"] doubleValue];
    double myLongitude = [[loParam objectForKey:@"myLongitude"] doubleValue];
    
    // 对方坐标
    double otherLatitude = [[loParam objectForKey:@"otherLatitude"] doubleValue];
    double otherLongitude = [[loParam objectForKey:@"otherLongitude"] doubleValue];
    
    CLLocationCoordinate2D my2d = CLLocationCoordinate2DMake(myLatitude, myLongitude);
    CLLocationCoordinate2D o2d = CLLocationCoordinate2DMake(otherLatitude, otherLongitude);
    MAMapPoint mP = MAMapPointForCoordinate(my2d);
    MAMapPoint oP = MAMapPointForCoordinate(o2d);
    CLLocationDistance distance = MAMetersBetweenMapPoints(mP, oP);
    
    return [NSString stringWithFormat:@"%.2f", distance];
}

#pragma mark -
- (void) doWeChatPay:(NSString*) infostr
{
    if (FALSE == _wechat.Configed)
    {
        [self onPayResult:FALSE msg:@"初始化失败"];
        return;
    }
    NSData *data = [infostr dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data
                                                    options:NSJSONReadingAllowFragments
                                                      error:&error];
    if (FALSE == [jsonObject isKindOfClass:[NSDictionary class]])
    {
        [self onPayResult:FALSE msg:@"订单数据解析失败"];
        return;
    }
    NSDictionary *info = (NSDictionary*)jsonObject;
    BOOL bRes = FALSE;
    NSString *appid = [info objectForKey:@"appid"];
    bRes = (appid != nil);
    NSString *partnerId = [info objectForKey:@"partnerid"];
    bRes = (partnerId != nil);
    NSString *prepayId = [info objectForKey:@"prepayid"];
    bRes = (prepayId != nil);
    NSString *packageValue = [info objectForKey:@"package"];
    bRes = (packageValue != nil);
    NSString *nonceStr = [info objectForKey:@"noncestr"];
    bRes = (nonceStr != nil);
    NSString *timeStamp = [info objectForKey:@"timestamp"];
    bRes = (timeStamp != nil);
    NSString *sign = [info objectForKey:@"sign"];
    bRes = (sign != nil);
    if (FALSE == bRes)
    {
        [self onPayResult:FALSE msg:@"订单数据解析失败"];
        return;
    }
    
    //调起微信支付
    PayReq* req = [[[PayReq alloc] init] autorelease];
    req.partnerId = partnerId;
    req.prepayId = prepayId;
    req.nonceStr = nonceStr;
    req.package = packageValue;
    req.timeStamp = [timeStamp intValue];
    req.sign = sign;
    [WXApi sendReq:req];
}

- (void) doAliPay:(tagPayParam) param
{
    
}

- (void) doJftPay:(NSDictionary *)payparam
{

}

- (void) doIapPay:(NSDictionary *)payparam
{
}

- (void) onPayResult:(BOOL)res msg:(NSString*)msg
{
    if (nil != _payDelegate)
    {
        if (TRUE == res)
        {
            [_payDelegate onPaySuccess:_payPlat backMsg:msg];
        }
        else
        {
            [_payDelegate onPayFail:_payPlat backMsg:msg];
        }
    }
    [self setPayDelegate:nil];
}
@end

