#!/usr/bin/python
# coding=utf-8

# src = '../client'
# outPath = '../ciphercode'
# srcTemp = '../client_temp'
# innerOutPath = src + '/ciphercode'

src = '/Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/client'
outPath = '/Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/ciphercode'
srcTemp = '/Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/client_temp'
innerOutPath = src + '/ciphercode'

import os
import sys
import time
import datetime

def prRed(prt): print("\033[91m {}\033[00m" .format(prt))
def prGreen(prt): print("\033[92m {}\033[00m" .format(prt))
def prYellow(prt): print("\033[93m {}\033[00m" .format(prt))
def prLightPurple(prt): print("\033[94m {}\033[00m" .format(prt))
def prPurple(prt): print("\033[95m {}\033[00m" .format(prt))
def prCyan(prt): print("\033[96m {}\033[00m" .format(prt))
def prLightGray(prt): print("\033[97m {}\033[00m" .format(prt))
def prBlack(prt): print("\033[98m {}\033[00m" .format(prt))

def back_up_src():
    if os.path.exists(innerOutPath+'/'):
        os.system('sudo rm -r -f ' + innerOutPath)

    if os.path.exists(srcTemp):
	    prGreen(srcTemp + ' 文件夹授权')
	    os.system('sudo chmod -R 777 ' + srcTemp)
	    prRed('删除 ' + srcTemp)
	    os.system('sudo rm -r -f ' + srcTemp)
    prPurple('创建备份文件夹 ' + srcTemp)
    os.mkdir(srcTemp)
    prYellow('备份源代码')
    os.system('sudo cp -r -f ' + src + '/ ' + srcTemp +'/')

def cp_files():
    if os.path.exists(outPath):
        prGreen(outPath + ' 文件夹授权')
        os.system('sudo chmod -R 777 ' + outPath)
        prRed('删除 ' + outPath)
        os.system('sudo rm -r -f ' + outPath + '/')
    prPurple('创建输出文件夹 ' + outPath)
    os.mkdir(outPath)
   
    prYellow('拷贝所以资源与源代码到输出文件')
    os.system('sudo cp -r -f ' + srcTemp + '/ ' + outPath +'/')

def del_lua_files(path):
    for root , dirs, files in os.walk(path):
        for name in files:
            if name.endswith(".lua"):
                os.remove(os.path.join(root, name))
                prRed("Delete File: " + os.path.join(root, name))

def del_base_files(path):
    prRed('删除 ' + path)
    os.system('sudo rm -r -f ' + path)

def compile_lua():
    prGreen('编译luac')
    command = '/Users/chengyi/Documents/env/cocos2d-x-3.12/tools/cocos2d-console/bin/cocos luacompile -s ' + src +'/ -d ' + outPath + '/' + ' -e -k RY_QP_MBCLIENT_!2016 -b RY_QP_2016 --disable-compile'
    if os.system(command) != 0:
         raise Exception("Build dynamic library for project [ " + command + " ] fails!")

if __name__ == "__main__":
    #The start time 
    print "开始执行时间 == 》》" + time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    #是否输出日志
    is_out_put_log = False
    if is_out_put_log:
        stdout_backup = sys.stdout
        log_file = open("/Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/script/message.log", "w")
        sys.stdout = log_file

    starttime = datetime.datetime.now()

    back_up_src()
    cp_files()
    del_lua_files(outPath)
    print("compile lua start")
    compile_lua()
    print("compile lua end")
    del_base_files(innerOutPath + '/base/')
    endtime = datetime.datetime.now()
    
    if is_out_put_log:
        log_file.close()
        sys.stdout = stdout_backup
    print "执行结束时间 == 》》" + time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    print ("执行时长 == 》》 %d 秒" % ((endtime - starttime).seconds))