#!/bin/bash
######################################################################################
#功能：ftp上传/下载文件
#用法：第一个参数put(上传)还是get(下载)文件，第二个参数为FTP服务器IP，第三、四个参数分别是用户名和密码
#      第五个参数是FTP上的工作目录，第六个是本地的目录，第七个是操作的文件名
#例子：testFtptool.sh put|get ip_address ftp_user ftp_password ftp_dir local_dir filename
#版本：0.1    #作者：crazyMyWay     #日期：
#说明：建立初版
######################################################################################
E_NOTROOT=67

#输出帮助信息,用法：./testFtptool.sh -h
if [ $# -eq 1 -a "$1" = "-h" ]
then
  echo "Usage: $0 put|get ip_address ftp_user ftp_password ftp_dir local_dir" #filename
  echo "Example:
        $0 put|get ftp服务ip ftp用户名 ftp密码 ftp目录 本地目录" #文件名
  exit $E_NOTROOT
fi

#如果参数不等于7
#if [ $# != 6 ]
#then
  #echo "Param error: Usage: $0 put|get ip_address ftp_user ftp_password ftp_dir local_dir" #filename
  #exit $E_NOTROOT
#fi

#!/bin/bash
cd /Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/ciphercode/client/
for dir in $(ls client*)
do
  [ -d $dir ] && echo $dir #先判断是否是目录，然后再输出
done

read -p "Press any key to continue." var

opType="mput"
ip="60.205.47.103"
userName="bxu2713950036"
password="c649605210"
opDir=("htdocs/upload/views", "layer/friend", "layer/game", "layer/logon" , "../../models", )
localDir1="/Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/ciphercode/client/src/plaza/views/"
localDir2="/Users/chengyi/Documents/selfproj/svn/tag/wuxi_trunk/ciphercode/client/src/plaza/models/"
ftp -v -n <<!
open $ip 21
user $userName $password
prompt
epsv4 off
cd $opDir[1]
bin
lcd $localDir1
$opType *
cd $opDir[2]
bin
lcd $localDir2
$opType *
bye
!