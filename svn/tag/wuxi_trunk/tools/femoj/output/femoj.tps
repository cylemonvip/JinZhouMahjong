<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>1</int>
        <key>variation</key>
        <string>main</string>
        <key>verbose</key>
        <false/>
        <key>autoSDSettings</key>
        <array/>
        <key>allowRotation</key>
        <true/>
        <key>quiet</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d</string>
        <key>textureFileName</key>
        <filename>femoj.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>javaFileName</key>
            <filename>femoj.java</filename>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileName</key>
        <filename>femoj.plist</filename>
        <key>multiPack</key>
        <false/>
        <key>mainExtension</key>
        <string></string>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../image/femoj/femoj_1_1.png</filename>
            <filename>../image/femoj/femoj_1_2.png</filename>
            <filename>../image/femoj/femoj_2_1.png</filename>
            <filename>../image/femoj/femoj_2_2.png</filename>
            <filename>../image/femoj/femoj_2_3.png</filename>
            <filename>../image/femoj/femoj_2_4.png</filename>
            <filename>../image/femoj/femoj_3_1.png</filename>
            <filename>../image/femoj/femoj_3_2.png</filename>
            <filename>../image/femoj/femoj_3_3.png</filename>
            <filename>../image/femoj/femoj_3_4.png</filename>
            <filename>../image/femoj/femoj_3_5.png</filename>
            <filename>../image/femoj/femoj_3_6.png</filename>
            <filename>../image/femoj/femoj_3_7.png</filename>
            <filename>../image/femoj/femoj_3_8.png</filename>
            <filename>../image/femoj/femoj_3_9.png</filename>
            <filename>../image/femoj/femoj_4_1.png</filename>
            <filename>../image/femoj/femoj_4_2.png</filename>
            <filename>../image/femoj/femoj_4_3.png</filename>
            <filename>../image/femoj/femoj_5_1.png</filename>
            <filename>../image/femoj/femoj_5_2.png</filename>
            <filename>../image/femoj/femoj_6_1.png</filename>
            <filename>../image/femoj/femoj_6_2.png</filename>
            <filename>../image/femoj/femoj_6_3.png</filename>
            <filename>../image/femoj/femoj_6_4.png</filename>
            <filename>../image/femoj/femoj_6_5.png</filename>
            <filename>../image/femoj/femoj_6_6.png</filename>
            <filename>../image/femoj/femoj_6_7.png</filename>
            <filename>../image/femoj/femoj_7_1.png</filename>
            <filename>../image/femoj/femoj_7_2.png</filename>
            <filename>../image/femoj/femoj_7_3.png</filename>
            <filename>../image/femoj/femoj_7_4.png</filename>
            <filename>../image/femoj/femoj_7_5.png</filename>
            <filename>../image/femoj/femoj_7_6.png</filename>
            <filename>../image/femoj/femoj_7_7.png</filename>
            <filename>../image/femoj/femoj_7_8.png</filename>
            <filename>../image/femoj/femoj_8_1.png</filename>
            <filename>../image/femoj/femoj_8_2.png</filename>
            <filename>../image/femoj/femoj_8_3.png</filename>
            <filename>../image/femoj/femoj_8_4.png</filename>
            <filename>../image/femoj/femoj_8_5.png</filename>
            <filename>../image/femoj/femoj_8_6.png</filename>
            <filename>../image/femoj/femoj_8_7.png</filename>
            <filename>../image/femoj/femoj_8_8.png</filename>
            <filename>../image/femoj/femoj_9_1.png</filename>
            <filename>../image/femoj/femoj_9_2.png</filename>
            <filename>../image/femoj/femoj_9_3.png</filename>
            <filename>../image/femoj/femoj_9_4.png</filename>
            <filename>../image/femoj/femoj_9_5.png</filename>
            <filename>../image/femoj/femoj_9_6.png</filename>
            <filename>../image/femoj/femoj_10_1.png</filename>
            <filename>../image/femoj/femoj_10_2.png</filename>
            <filename>../image/femoj/femoj_10_3.png</filename>
            <filename>../image/femoj/femoj_10_4.png</filename>
            <filename>../image/femoj/femoj_10_5.png</filename>
            <filename>../image/femoj/femoj_10_6.png</filename>
            <filename>../image/femoj/femoj_11_1.png</filename>
            <filename>../image/femoj/femoj_11_2.png</filename>
            <filename>../image/femoj/femoj_11_3.png</filename>
            <filename>../image/femoj/femoj_11_4.png</filename>
            <filename>../image/femoj/femoj_11_5.png</filename>
            <filename>../image/femoj/femoj_12_1.png</filename>
            <filename>../image/femoj/femoj_12_2.png</filename>
            <filename>../image/femoj/femoj_12_3.png</filename>
            <filename>../image/femoj/femoj_13_1.png</filename>
            <filename>../image/femoj/femoj_13_2.png</filename>
            <filename>../image/femoj/femoj_13_3.png</filename>
            <filename>../image/femoj/femoj_13_4.png</filename>
            <filename>../image/femoj/femoj_14_1.png</filename>
            <filename>../image/femoj/femoj_14_2.png</filename>
            <filename>../image/femoj/femoj_15_1.png</filename>
            <filename>../image/femoj/femoj_15_2.png</filename>
            <filename>../image/femoj/femoj_16_1.png</filename>
            <filename>../image/femoj/femoj_16_2.png</filename>
            <filename>../image/femoj/femoj_16_3.png</filename>
            <filename>../image/femoj/femoj_16_4.png</filename>
            <filename>../image/femoj/femoj_16_5.png</filename>
            <filename>../image/femoj/femoj_16_6.png</filename>
            <filename>../image/femoj/femoj_16_7.png</filename>
            <filename>../image/femoj/femoj_17_1.png</filename>
            <filename>../image/femoj/femoj_17_2.png</filename>
            <filename>../image/femoj/femoj_17_3.png</filename>
            <filename>../image/femoj/femoj_17_4.png</filename>
            <filename>../image/femoj/femoj_18_1.png</filename>
            <filename>../image/femoj/femoj_18_2.png</filename>
            <filename>../image/femoj/femoj_18_3.png</filename>
            <filename>../image/femoj/femoj_18_4.png</filename>
            <filename>../image/femoj/femoj_18_5.png</filename>
            <filename>../image/femoj/femoj_18_6.png</filename>
            <filename>../image/femoj/femoj_18_7.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
