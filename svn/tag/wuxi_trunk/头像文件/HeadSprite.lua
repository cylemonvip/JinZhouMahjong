--
-- Author: zhong
-- Date: 2016-07-25 10:19:18
--
--游戏头像
local HeadSprite = class("HeadSprite", cc.Sprite)
local MultiPlatform = appdf.req(appdf.EXTERNAL_SRC .. "MultiPlatform")

--自定义头像存储规则
-- path/face/userid/custom_+customid.ry
--头像缓存规则
-- uid_custom_cusomid

local headArray = {}

local HEADDOWNLOAD_LISTENER = "head_notify_down"
local FACEDOWNLOAD_LISTENER = "face_notify_down"
local FACERESIZE_LISTENER = "face_resize_notify"
--全局通知函数
cc.exports.g_FaceDownloadListener = function (ncode, msg, filename)
	print("g_FaceDownloadListener -------- g_FaceDownloadListener ncode = " .. ncode .. " msg = " .. " filename = " .. filename)
	local event = cc.EventCustom:new(FACEDOWNLOAD_LISTENER)
	event.code = ncode
	event.msg = msg
	event.filename = filename

	cc.Director:getInstance():getEventDispatcher():dispatchEvent(event)
end

cc.exports.g_HeadDownloadListener = function (ncode, msg, filename)
	print("g_HeadDownloadListener -------- g_HeadDownloadListener ncode = " .. ncode .. " msg = " .. " filename = " .. filename .. " HEADDOWNLOAD_LISTENER = " .. HEADDOWNLOAD_LISTENER)
	HeadSprite.headDownLoadCallback(filename)
end

cc.exports.g_FaceResizeListener = function(oldpath, newpath)
	local event = cc.EventCustom:new(FACERESIZE_LISTENER)
	event.oldpath = oldpath
	event.newpath = newpath

	cc.Director:getInstance():getEventDispatcher():dispatchEvent(event)
end

local SYS_HEADSIZE = 96
function HeadSprite.checkData(useritem)
	useritem = useritem or {}
	useritem.dwUserID = useritem.dwUserID or 0
	useritem.dwCustomID = useritem.dwCustomID or 0
	useritem.wFaceID = useritem.wFaceID or 0
	if useritem.wFaceID > 199 then
		useritem.wFaceID = 0
	end
	useritem.cbMemberOrder = useritem.cbMemberOrder or 0

	return useritem
end

function HeadSprite:ctor( )
	self.m_spRender = nil
	self.m_downListener = nil
	self.m_resizeListener = nil

	--注册事件
	local function onEvent( event )
		if event == "exit" then
			self:onExit()
		elseif event == "enterTransitionFinish" then
			self:onEnterTransitionFinish()
        end
	end
	self:registerScriptHandler(onEvent)

	self.m_headSize = 96
	self.m_useritem = nil
	self.listener = nil
	self.m_bEnable = false
	--是否头像
	self.m_bFrameEnable = false
	--头像配置
	self.m_tabFrameParam = {}
end

--创建普通玩家头像
function HeadSprite:createNormal( useritem ,headSize)
	if nil == useritem then
		return
	end
	useritem = HeadSprite.checkData(useritem)
	local sp = HeadSprite.new()
	sp.m_headSize = headSize
	local spRender = sp:initHeadSprite(useritem)
	if nil ~= spRender then
		sp:addChild(spRender)
		local selfSize = sp:getContentSize()
		spRender:setPosition(cc.p(selfSize.width * 0.5, selfSize.height * 0.5))
		return sp
	end
	
	return nil
end

--创建裁剪玩家头像
function HeadSprite:createClipHead( useritem, headSize, clippingfile)
	if nil == useritem then
		--return
	end
	useritem = HeadSprite.checkData(useritem)

	local sp = HeadSprite.new()
	sp.m_headSize = headSize
	local spRender = sp:initHeadSprite(useritem)
	if nil == spRender then
		return nil
	end 

	--创建裁剪
	local strClip = "head_mask.png"
	if nil ~= clippingfile then
		strClip = clippingfile
	end
	local clipSp = nil
	local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(strClip)
	if nil ~= frame then
		clipSp = cc.Sprite:createWithSpriteFrame(frame)
	else
		clipSp = cc.Sprite:create(strClip)
	end
	if nil ~= clipSp then
		--裁剪
		local clip = cc.ClippingNode:create()
		clip:setStencil(clipSp)
		clip:setAlphaThreshold(0)
		clip:addChild(spRender)
		local selfSize = sp:getContentSize()
		clip:setPosition(cc.p(selfSize.width * 0.5, selfSize.height * 0.5))
		sp:addChild(clip)
		return sp
	end

	return nil
end

function HeadSprite:updateHead( useritem )
	if nil == useritem then
		return
	end
	self.m_useritem = useritem

	--判断是否进入防作弊房间
	local bAntiCheat = GlobalUserItem.isAntiCheatValid(useritem.dwUserID)

	--更新头像框
	if self.m_bFrameEnable and false == bAntiCheat then
		local vipIdx = self.m_useritem.cbMemberOrder or 0

		--根据会员等级配置
		local vipIdx = self.m_useritem.cbMemberOrder or 0
		local framestr = string.format("sp_frame_%d_0.png", vipIdx)
		local deScale = 0.72

		local framefile = self.m_tabFrameParam._framefile or framestr
		local scaleRate = self.m_tabFrameParam._scaleRate or deScale
		local zorder = self.m_tabFrameParam._zorder or 1
		self:updateHeadFrame(framefile, scaleRate):setLocalZOrder(zorder)
	end

	if nil ~= useritem.dwCustomID and 0 ~= useritem.dwCustomID and false == bAntiCheat then
		--判断是否有缓存
		local framename = useritem.dwUserID .. "_wx_head.png"
		local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(framename)
		if nil ~= frame then
			self:updateHeadByFrame(frame)
			return		
		end		
	end
	--系统头像
	local faceid = useritem.wFaceID or 0
	if true == bAntiCheat then
		faceid = 0
	end
	local str = string.format("Avatar%d.png", faceid)
	local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(str)
	if nil ~= frame then
		self.m_spRender:setSpriteFrame(frame)
		-- self:setContentSize(self.m_spRender:getContentSize())	
		self:setContentSize(cc.size(SYS_HEADSIZE, SYS_HEADSIZE))
	end

	local fwidth = SYS_HEADSIZE
	if self.m_spRender:getContentSize().width > SYS_HEADSIZE then
		fwidth = self.m_spRender:getContentSize().width
		self.m_spRender:setScale(SYS_HEADSIZE/fwidth)
	else
		self.m_spRender:setScale(1)
	end
	self.m_fScale = self.m_headSize / SYS_HEADSIZE
	self:setScale(self.m_fScale)

	return sp
end

function HeadSprite:updateHeadByFrame(frame)
	if nil == self.m_spRender then
		self.m_spRender = cc.Sprite:createWithSpriteFrame(frame)
	else
		self.m_spRender:setSpriteFrame(frame)
	end

	local fwidth = SYS_HEADSIZE
	local spWidth = self.m_spRender:getContentSize().width
	if tonumber(spWidth) > fwidth then
		fwidth = tonumber(spWidth)
		self.m_spRender:setScale(SYS_HEADSIZE/fwidth)
	else
		self.m_spRender:setScale(1)
	end

	self:setContentSize(cc.size(SYS_HEADSIZE, SYS_HEADSIZE))
	self.m_fScale = self.m_headSize / SYS_HEADSIZE
	self:setScale(self.m_fScale)
end

--允许个人信息弹窗/点击头像触摸事件
function HeadSprite:registerInfoPop( bEnable, fun )
	self.m_bEnable = bEnable
	self.m_fun = fun

	if bEnable then
		--触摸事件
		self:registerTouch()
	else
		self:onExit()
	end
end

--头像框
--[[
frameparam = 
{
	--框文件
	_framefile 
	--缩放值
	_scaleRate
	--位置比例
	_posPer{}
	-- z顺序
	_zorder
}
]]
function HeadSprite:enableHeadFrame( bEnable, frameparam )
	if nil == self.m_useritem then
		print("不知为何 返回")
		return
	end
	self.m_bFrameEnable = bEnable
	local bAntiCheat = GlobalUserItem.isAntiCheatValid(self.m_useritem.dwUserID)

	if false == bEnable then -- bAntiCheat 原本防作弊场，不显示头像框
		if nil ~= self.m_spFrame then
			self.m_spFrame:removeFromParent()
			self.m_spFrame = nil
		end
		print("不知为何 返回 2")
		return
	end	
	local vipIdx = self.m_useritem.cbMemberOrder or 0

	--根据会员等级配置
	local vipIdx = self.m_useritem.cbMemberOrder or 0
	local framestr = string.format("sp_frame_0_0.png", vipIdx)
	local deScale = 0.72

	frameparam = frameparam or {}
	self.m_tabFrameParam = frameparam
	local framefile = frameparam._framefile or framestr
	local scaleRate = frameparam._scaleRate or deScale
	local zorder = frameparam._zorder or 1
	self:updateHeadFrame(framefile, scaleRate):setLocalZOrder(zorder)
end

--更新头像框
function HeadSprite:updateHeadFrame(framefile, scaleRate)
	local spFrame = nil	
	local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(framefile)
	if nil == frame then
		spFrame = cc.Sprite:create(framefile)
		frame = (spFrame ~= nil) and spFrame:getSpriteFrame() or nil		
	end
	if nil == frame then
		print("不知为何 返回 3")
		return nil
	end

	if nil == self.m_spFrame then
		local selfSize = self:getContentSize()
		self.m_spFrame = cc.Sprite:createWithSpriteFrame(frame)
		local positionRate = self.m_tabFrameParam._posPer or cc.p(0.5, 0.64)
		self.m_spFrame:setPosition(selfSize.width * positionRate.x, selfSize.height * positionRate.y)
		self:addChild(self.m_spFrame, 20)
	else
		self.m_spFrame:setSpriteFrame(frame)
	end
	self.m_spFrame:setScale(scaleRate)
	return self.m_spFrame
end

function HeadSprite.headDownLoadCallback(downloadfile)
	local path = device.writablePath
	local filepath = path .. downloadfile
	print("下载完成后filepath = " .. filepath)
	print("downloadfile = " .. downloadfile)
	local isUpdateHeadSuccess = false
	local useritem = nil 
	for k,v in pairs(headArray) do
		if k == downloadfile then
			local sp = cc.Sprite:create(filepath)
			if nil ~= sp then
				local customframe = sp:getSpriteFrame()
				cc.SpriteFrameCache:getInstance():addSpriteFrame(customframe, downloadfile)
				customframe:retain()
				v:updateHeadByFrame(customframe)
				useritem = v.m_useritem
				isUpdateHeadSuccess = true
				break
			end
		end
	end

	-- local that = this	
	local function eventReSizeListener(event)
		if nil == event.oldpath or nil == event.newpath then
			return
		end

		--是否是自己文件
		--发送上传头像
		local url = yl.HTTP_URL .. "/WS/Account.ashx?action=uploadface"
		local uploader = CurlAsset:createUploader(url,filepath)
		if nil == uploader then
			return
		end
		local nres = uploader:addToFileForm("file", filepath, "image/png")
		--用户标示
		nres = uploader:addToForm("userID", useritem.dwUserID or "thrid")
		--登陆时间差
		local delta = tonumber(currentTime()) - tonumber(GlobalUserItem.LogonTime)
		nres = uploader:addToForm("time", delta .. "")
		--客户端ip
		local ip = MultiPlatform:getInstance():getClientIpAdress() or "192.168.1.1"
		nres = uploader:addToForm("clientIP", ip)
		--机器码
		local machine = useritem.szMachine or "A501164B366ECFC9E249163873094D50"
		nres = uploader:addToForm("machineID", machine)
		--会话签名
		nres = uploader:addToForm("signature", GlobalUserItem:getSignature(delta))
		if 0 ~= nres then
			return
		end
		uploader:uploadFile(function(sender, ncode, msg)
			print("上传成功 msg = " .. msg)
		end)					
	end
	--如果头像更新成功了，才开始上传
	--判断这个头像是不是自己的
	local isSelfHeadPic = false
	if downloadfile == GlobalUserItem.dwUserID .. "_wx_head.png" then
		print("是自己的头像，需要上传")
		isSelfHeadPic = true
	end
	if isUpdateHeadSuccess and useritem ~= nil and isSelfHeadPic then
		eventReSizeListener({oldpath = filepath, newpath = filepath })
	end
	
end

function HeadSprite:initHeadSprite( useritem )
	for k,v in pairs(useritem) do
		print("初始化头像 ======= >>> ", k, v)
	end
	self.m_useritem = useritem
	local isThirdParty = useritem.bThirdPartyLogin or false	
	if isThirdParty then
		print("szThirdPartyUrl = " .. tostring(useritem.szThirdPartyUrl))
	end

	print("isThirdParty = " .. tostring(isThirdParty))
	print("useritem.dwCustomID = " .. tostring(useritem.dwCustomID))
	--系统头像
	local faceid = useritem.wFaceID or 0
	--判断是否进入防作弊房间
	local bAntiCheat = GlobalUserItem.isAntiCheatValid(useritem.dwUserID)
	if bAntiCheat then
		--直接使用系统头像
		faceid = 0
	elseif isThirdParty and nil ~= useritem.szThirdPartyUrl and string.len(useritem.szThirdPartyUrl) > 0 then
		local filename = useritem.dwUserID .. "_wx_head.png"
		self.m_fileName = filename
		print("ThirdParty Head filename = " .. filename)
		--判断是否有缓存或者本地文件
		local framename = filename
		local path = device.writablePath
		local filepath = path .. filename
		print("filepath = " .. filepath)
		local bHave, spRender = self:haveCacheOrLocalFile(framename, filepath, false) 
		-- bHave = false
		if bHave then
			print("存在本地文件")
			return spRender
		else
			print("调用下载头像方法")
			--网络请求
			local url = useritem.szThirdPartyUrl
			local this = self
			print("头像下载链接：" .. url)
			self:downloadHeadFile(url, path, filename)			
		end
	elseif nil ~= useritem.dwCustomID and 0 ~= useritem.dwCustomID then
		--判断是否有缓存或者本地文件
		local framename = useritem.dwUserID .. "_wx_head.png"
		local filepath = device.writablePath .. framename

		local bHave, spRender = self:haveCacheOrLocalFile(framename, filepath, true)
		if bHave then
			return spRender
		else
			--网络请求
			local url = yl.HTTP_URL .. "/WS/UserFace.ashx?customid=" .. useritem.dwCustomID
			print("从自己服务器下载 url = " .. url)
			local path = device.writablePath
			local filename = useritem.dwUserID .. "_wx_head.png"

			self:downloadHeadFile(url, path, filename)
		end
	end
	
	local str = string.format("Avatar%d.png", faceid)
	local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(str)
	if nil ~= frame then
		self.m_spRender = cc.Sprite:createWithSpriteFrame(frame)
		self:setContentSize(cc.size(SYS_HEADSIZE, SYS_HEADSIZE))
	end	

	local fwidth = SYS_HEADSIZE
	if self.m_spRender:getContentSize().width > SYS_HEADSIZE then
		fwidth = self.m_spRender:getContentSize().width
		self.m_spRender:setScale(SYS_HEADSIZE/fwidth)
	else
		self.m_spRender:setScale(1)
	end
	
	self.m_fScale = self.m_headSize / SYS_HEADSIZE
	self:setScale(self.m_fScale)

	return self.m_spRender
end

function HeadSprite:haveCacheOrLocalFile(framename, filepath, bmpfile)
	--判断是否有缓存
	local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(framename)
	if nil ~= frame then
		print("有本地缓存 spriteFrame，直接使用 =============>>>>>>")
		self:updateHeadByFrame(frame)
		return true, self.m_spRender
	else
		print("查看是否存在本地文件 =============>>>>>>")
		--判断是否有本地文件
		local path = filepath
		if cc.FileUtils:getInstance():isFileExist(path) then
			print("存在本地文件 ================>>>>>>")
			local customframe = nil
			print("创建精灵帧")
			local sp = cc.Sprite:create(path)
			if nil ~= sp then
				customframe = sp:getSpriteFrame()
			end
			if nil ~= customframe then
				--缓存帧
				local framename = self.m_useritem.dwUserID .. "_wx_head.png"    
				print("插入精灵帧，key = " ..  framename .. "  value = xxxxx")										
				cc.SpriteFrameCache:getInstance():addSpriteFrame(customframe, framename)

				customframe:retain()
				print("调用更新头像的方法")
				self:updateHeadByFrame(customframe)
				return true, self.m_spRender
			end
		end
	end
	return false
end

--下载头像
function HeadSprite:downloadHeadFile(url, path, filename)
	local downloader = CurlAsset:createDownloader("g_HeadDownloadListener", url)
	if false == cc.FileUtils:getInstance():isDirectoryExist(path) then
		cc.FileUtils:getInstance():createDirectory(path)
	end			
	headArray[filename] = self
	downloader:downloadHeadFile(url, filename)
end

--下载头像
function HeadSprite:downloadFace(url, path, filename, onDownLoadSuccess)
	local downloader = CurlAsset:createDownloader("g_FaceDownloadListener",url)			
	if false == cc.FileUtils:getInstance():isDirectoryExist(path) then
		cc.FileUtils:getInstance():createDirectory(path)
	end			

	local function eventCustomListener(event)
        if nil ~= event.filename then -- and 0 == event.code
        	if nil ~= onDownLoadSuccess 
        		and type(onDownLoadSuccess) == "function" 
        		and nil ~= event.filename 
        		and type(event.filename) == "string" then
        		onDownLoadSuccess(event.filename)
        	end        	
        end
	end
	self.m_downListener = cc.EventListenerCustom:create(FACEDOWNLOAD_LISTENER,eventCustomListener)
	self:getEventDispatcher():addEventListenerWithFixedPriority(self.m_downListener, 1)
	downloader:downloadFile(path, filename)
end

function HeadSprite:registerTouch()
	local function onTouchBegan( touch, event )
		return self:isVisible() and self:isAncestorVisible(self) and self.m_bEnable
	end

	local function onTouchEnded( touch, event )
		local pos = touch:getLocation()
        pos = self:convertToNodeSpace(pos)
        print("")
        local swidth = self:getContentSize().width
        local sheight = self:getContentSize().height
        local rec = cc.rect(0, 0, swidth, sheight)
        if true == cc.rectContainsPoint(rec, pos) then
            if nil ~= self.m_fun then
            	self.m_fun()
            end
        end        
	end

	local listener = cc.EventListenerTouchOneByOne:create()
	self.listener = listener
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self)
end

function HeadSprite:isAncestorVisible( child )
	if nil == child then
		return true
	end
	local parent = child:getParent()
	if nil ~= parent and false == parent:isVisible() then
		return false
	end
	return self:isAncestorVisible(parent)
end

function HeadSprite:onExit()

	local arrayIndex  = 1
	for k,v in pairs(headArray) do
		if k == self.m_fileName then
			headArray[k] = nil
			table.remove(headArray, arrayIndex)
		end
		arrayIndex = arrayIndex + 1
	end

	print("HeadSprite->onExit " .. debug.traceback())
	if nil ~= self.listener then
		local eventDispatcher = self:getEventDispatcher()
		eventDispatcher:removeEventListener(self.listener)
		self.listener = nil
	end

	if nil ~= self.m_downListener then
		self:getEventDispatcher():removeEventListener(self.m_downListener)
		self.m_downListener = nil
	end

	if nil ~= self.m_resizeListener then
		self:getEventDispatcher():removeEventListener(self.m_resizeListener)
		self.m_resizeListener = nil
	end
end

function HeadSprite:onEnterTransitionFinish()
	if self.m_bEnable and nil == self.listener then
		self:registerTouch()
	end
end

--缓存头像
function HeadSprite.loadAllHeadFrame(  )
	if false == cc.SpriteFrameCache:getInstance():isSpriteFramesWithFileLoaded("public/im_head.plist") then
		--缓存头像
		cc.SpriteFrameCache:getInstance():addSpriteFrames("public/im_head.plist")
		--
		--手动retain所有的头像帧缓存、防止被释放
		local dict = cc.FileUtils:getInstance():getValueMapFromFile("public/im_head.plist")
		local framesDict = dict["frames"]
		if nil ~= framesDict and type(framesDict) == "table" then
			for k,v in pairs(framesDict) do
				local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(k)
				if nil ~= frame then
					frame:retain()
				end
			end
		end

		--缓存头像框
		cc.SpriteFrameCache:getInstance():addSpriteFrames("public/im_head_frame.plist")
		dict = cc.FileUtils:getInstance():getValueMapFromFile("public/im_head_frame.plist")
		framesDict = dict["frames"]
		if nil ~= framesDict and type(framesDict) == "table" then
			for k,v in pairs(framesDict) do
				local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(k)
				if nil ~= frame then
					frame:retain()
				end
			end
		end
	end
end

function HeadSprite.unloadAllHead(  )
	local dict = cc.FileUtils:getInstance():getValueMapFromFile("public/im_head_frame.plist")
	local framesDict = dict["frames"]
	if nil ~= framesDict and type(framesDict) == "table" then
		for k,v in pairs(framesDict) do
			local frame = cc.SpriteFrameCache:getInstance():getSpriteFrame(k)
			if nil ~= frame then
				frame:release()
			end
		end
	end

	cc.Director:getInstance():getTextureCache():removeUnusedTextures()
	cc.SpriteFrameCache:getInstance():removeUnusedSpriteFrames()
end

--获取系统头像数量
function HeadSprite.getSysHeadCount(  )
	return 200
end

--缓存头像
HeadSprite.loadAllHeadFrame()

return HeadSprite